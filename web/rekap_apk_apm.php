<?php

require_once "../app/config.php";

$ta_berjalan = 2015;
$param_limit = '';

$start = microtime(true);

$today = date('Y-m-d');

echo "Inisiasi rekap apk apm tanggal ".$today."<br>";

function initdb($tipe){

	if($tipe == 'utama'){
		$serverName = DATABASEHOST; //serverName\instanceName
    	$connectionInfo = array( "Database"=>DATABASENAME, "UID"=>DATABASEUSER, "PWD"=>DATABASEPASSWORD,'ReturnDatesAsStrings'=>true);
	}else{
		$serverName = DATABASEHOST_REKAP; //serverName\instanceName
    	$connectionInfo = array( "Database"=>DATABASENAME_REKAP, "UID"=>DATABASEUSER_REKAP, "PWD"=>DATABASEPASSWORD_REKAP,'ReturnDatesAsStrings'=>true);
	}

    $conn = sqlsrv_connect( $serverName, $connectionInfo);

    return $conn;
}

$con_rekap = initdb('rekap');
$con_dapodik = initdb('utama');

$sql = "select {$param_limit} * from apk_apm";

$stmt = sqlsrv_query( $con_rekap, $sql);

while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){

	echo "memulai rekap untuk kecamatan ".$row['kecamatan']."<br>";

	$sql_pd = "SELECT
				COUNT (DISTINCT(pd.peserta_didik_id)) as jumlah
			FROM
				peserta_didik pd
			JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
			JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
			JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
			JOIN ref.semester sm ON sm.semester_id = rb.semester_id
			JOIN sekolah s on s.sekolah_id = rpd.sekolah_id
			JOIN ref.mst_wilayah kec on kec.kode_wilayah = LEFT(s.kode_wilayah,6)
			WHERE
				rpd.Soft_delete = 0
			AND pd.Soft_delete = 0
			AND (rpd.jenis_keluar_id is null
				OR rpd.tanggal_keluar > sm.tanggal_selesai
				OR rpd.jenis_keluar_id = '1')
			AND sm.tahun_ajaran_id like '{$ta_berjalan}%'
			AND rb.soft_delete = 0
			AND ar.soft_delete = 0
			AND rb.jenis_rombel = 1
			AND kec.kode_wilayah = '{$row['kode_wilayah_kecamatan']}'
			AND s.bentuk_pendidikan_id in (13,15)";

	$stmt_pd = sqlsrv_query( $con_dapodik, $sql_pd);

	$row_pd = sqlsrv_fetch_array( $stmt_pd, SQLSRV_FETCH_ASSOC);

	$sql_pd_sma_16 = "SELECT
				COUNT (DISTINCT(pd.peserta_didik_id)) as jumlah
			FROM
				peserta_didik pd
			JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
			JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
			JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
			JOIN ref.semester sm ON sm.semester_id = rb.semester_id
			JOIN sekolah s on s.sekolah_id = rpd.sekolah_id
			JOIN ref.mst_wilayah kec on kec.kode_wilayah = LEFT(s.kode_wilayah,6)
			WHERE
				rpd.Soft_delete = 0
			AND pd.Soft_delete = 0
			AND (rpd.jenis_keluar_id is null
				OR rpd.tanggal_keluar > sm.tanggal_selesai
				OR rpd.jenis_keluar_id = '1')
			AND sm.tahun_ajaran_id like '{$ta_berjalan}%'
			AND rb.soft_delete = 0
			AND ar.soft_delete = 0
			AND rb.jenis_rombel = 1
			AND kec.kode_wilayah = '{$row['kode_wilayah_kecamatan']}'
			AND s.bentuk_pendidikan_id in (13,15)
			AND (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 16 and (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) <= 19";

	$stmt_pd_sma_16 = sqlsrv_query( $con_dapodik, $sql_pd_sma_16);

	$row_pd_sma_16 = sqlsrv_fetch_array( $stmt_pd_sma_16, SQLSRV_FETCH_ASSOC);

	// echo $row_pd['jumlah']."<br>";

	$sql_update = "UPDATE apk_apm
					SET pd_sma = {$row_pd['jumlah']},
					pd_sma_16_19_tahun = {$row_pd_sma_16['jumlah']}
					WHERE
						kode_wilayah_kecamatan = '{$row['kode_wilayah_kecamatan']}'";

	$stmt_update = sqlsrv_query( $con_rekap, $sql_update);

	if($stmt_update){
		echo "berhasil merepak apk apm<br>";
	}


}

$time_elapsed_secs = microtime(true) - $start;

echo $time_elapsed_secs;
