Ext.define('Admin.store.KualitasDataSekolahSp', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.KualitasDataSekolahSp',
    model: 'Admin.model.KualitasDataSekolahSp',
    pageSize: 50,
    autoLoad: false
});