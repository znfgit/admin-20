Ext.define('Admin.store.customQuery', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.customQuery',
    model: 'Admin.model.customQuery',
    pageSize: 50,
    autoLoad: false    
});