Ext.define('Admin.store.JenisPtk', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.JenisPtk',
    model: 'Admin.model.JenisPtk',
    pageSize: 50,
    autoLoad: false    
});