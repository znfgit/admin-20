Ext.define('admin.store.SpBentukPendidikan', {
    extend: 'Ext.data.Store',
    requires: 'admin.model.SpBentukPendidikan',
    model: 'admin.model.SpBentukPendidikan',
    pageSize: 50,
    autoLoad: false
});