Ext.define('Admin.store.SatuanPendidikanRingkasanDetail', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.SatuanPendidikanRingkasanDetail',
    model: 'Admin.model.SatuanPendidikanRingkasanDetail',
    pageSize: 50,
    autoLoad: false,
    listeners: {
        write: function(store, operation){
            var record = operation.getRecords()[0],
            name = Ext.String.capitalize(operation.action),
            verb;

            var msg = "";
            switch (name) {
                case 'Create':
                    msg = 'dibuat';
                    break;
                case 'Update':
                    msg = 'diperbarui';
                    break;
                case 'Destroy':
                    msg = 'dihapus';
                    break;
            }
            if (operation.wasSuccessful()) {
                Xond.msg('Info', 'Data SatuanPendidikanRingkasanDetail berhasil ' + msg);
            } else {
                var errorMsg = operation.getError();
                //Xond.msg('Info', 'Gagal menyimpan data Sekolah ('+ errorMsg +')');
                Ext.Msg.alert('Error', 'Gagal menyimpan data Sekolah ('+ errorMsg +')');
            }
        }
    }
});