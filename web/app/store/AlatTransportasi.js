Ext.define('Admin.store.AlatTransportasi', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.AlatTransportasi',
    model: 'Admin.model.AlatTransportasi',
    pageSize: 50,
    autoLoad: false    
});