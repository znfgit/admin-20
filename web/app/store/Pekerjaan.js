Ext.define('Admin.store.Pekerjaan', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.Pekerjaan',
    model: 'Admin.model.Pekerjaan',
    pageSize: 50,
    autoLoad: false    
});