Ext.define('Admin.store.MstWilayah', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.MstWilayah',
    model: 'Admin.model.MstWilayah',
    pageSize: 50,
    autoLoad: false
});