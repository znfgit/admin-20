Ext.define('Admin.store.PdPerJurusan', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.PdPerJurusan',
    model: 'Admin.model.PdPerJurusan',
    pageSize: 500000,
    autoLoad: false    
});