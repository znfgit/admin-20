Ext.define('admin.store.SarprasKerusakan', {
    extend: 'Ext.data.Store',
    requires: 'admin.model.SarprasKerusakan',
    model: 'admin.model.SarprasKerusakan',
    pageSize: 50,
    autoLoad: false
});