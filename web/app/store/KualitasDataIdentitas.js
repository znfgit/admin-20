Ext.define('Admin.store.KualitasDataIdentitas', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.KualitasDataIdentitas',
    model: 'Admin.model.KualitasDataIdentitas',
    pageSize: 50,
    autoLoad: false
});