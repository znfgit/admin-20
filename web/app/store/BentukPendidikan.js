Ext.define('Admin.store.BentukPendidikan', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.BentukPendidikan',
    model: 'Admin.model.BentukPendidikan',
    pageSize: 50,
    autoLoad: false
});