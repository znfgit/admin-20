Ext.define('Admin.store.StatusKeaktifanPegawai', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.StatusKeaktifanPegawai',
    model: 'Admin.model.StatusKeaktifanPegawai',
    pageSize: 50,
    autoLoad: false    
});