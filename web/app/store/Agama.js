Ext.define('Admin.store.Agama', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.Agama',
    model: 'Admin.model.Agama',
    pageSize: 50,
    autoLoad: false    
});