Ext.define('Admin.store.JenisSurat', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.JenisSurat',
    model: 'Admin.model.JenisSurat',
    pageSize: 50,
    autoLoad: false    
});