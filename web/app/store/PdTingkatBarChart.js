Ext.define('Admin.store.PdTingkatBarChart', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.PdTingkatBarChart',
    model: 'Admin.model.PdTingkatBarChart',
    pageSize: 50,
    autoLoad: false
});