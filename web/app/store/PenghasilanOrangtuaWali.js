Ext.define('Admin.store.PenghasilanOrangtuaWali', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.PenghasilanOrangtuaWali',
    model: 'Admin.model.PenghasilanOrangtuaWali',
    pageSize: 50,
    autoLoad: false    
});