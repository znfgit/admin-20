Ext.define('Admin.store.KualitasDataSekolah', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.KualitasDataSekolah',
    model: 'Admin.model.KualitasDataSekolah',
    pageSize: 50,
    autoLoad: false
});