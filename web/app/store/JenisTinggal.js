Ext.define('Admin.store.JenisTinggal', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.JenisTinggal',
    model: 'Admin.model.JenisTinggal',
    pageSize: 50,
    autoLoad: false    
});