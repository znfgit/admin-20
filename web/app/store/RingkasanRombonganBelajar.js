Ext.define('Admin.store.RingkasanRombonganBelajar', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.RingkasanRombonganBelajar',
    model: 'Admin.model.RingkasanRombonganBelajar',
    pageSize: 50,
    autoLoad: false
});