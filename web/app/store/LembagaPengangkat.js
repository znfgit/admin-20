Ext.define('Admin.store.LembagaPengangkat', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.LembagaPengangkat',
    model: 'Admin.model.LembagaPengangkat',
    pageSize: 50,
    autoLoad: false    
});