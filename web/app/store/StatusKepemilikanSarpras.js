Ext.define('Admin.store.StatusKepemilikanSarpras', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.StatusKepemilikanSarpras',
    model: 'Admin.model.StatusKepemilikanSarpras',
    pageSize: 50,
    autoLoad: false    
});