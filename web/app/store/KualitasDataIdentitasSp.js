Ext.define('Admin.store.KualitasDataIdentitasSp', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.KualitasDataIdentitasSp',
    model: 'Admin.model.KualitasDataIdentitasSp',
    pageSize: 50,
    autoLoad: false
});