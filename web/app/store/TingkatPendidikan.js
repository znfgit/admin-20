Ext.define('Admin.store.TingkatPendidikan', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.TingkatPendidikan',
    model: 'Admin.model.TingkatPendidikan',
    pageSize: 50,
    autoLoad: false    
});