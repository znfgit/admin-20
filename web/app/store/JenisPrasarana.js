Ext.define('Admin.store.JenisPrasarana', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.JenisPrasarana',
    model: 'Admin.model.JenisPrasarana',
    pageSize: 50,
    autoLoad: false    
});