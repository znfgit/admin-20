Ext.define('Admin.store.PengirimanSp', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.PengirimanSp',
    model: 'Admin.model.PengirimanSp',
    pageSize: 50,
    autoLoad: false
});