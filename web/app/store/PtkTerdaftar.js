Ext.define('Admin.store.PtkTerdaftar', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.PtkTerdaftar',
    model: 'Admin.model.PtkTerdaftar',
    pageSize: 50,
    autoLoad: false
});