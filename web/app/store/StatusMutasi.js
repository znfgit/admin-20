Ext.define('Admin.store.StatusMutasi', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.StatusMutasi',
    model: 'Admin.model.StatusMutasi',
    pageSize: 50,
    autoLoad: false    
});