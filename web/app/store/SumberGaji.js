Ext.define('Admin.store.SumberGaji', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.SumberGaji',
    model: 'Admin.model.SumberGaji',
    pageSize: 50,
    autoLoad: false    
});