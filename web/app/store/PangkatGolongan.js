Ext.define('Admin.store.PangkatGolongan', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.PangkatGolongan',
    model: 'Admin.model.PangkatGolongan',
    pageSize: 50,
    autoLoad: false    
});