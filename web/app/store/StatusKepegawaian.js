Ext.define('Admin.store.StatusKepegawaian', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.StatusKepegawaian',
    model: 'Admin.model.StatusKepegawaian',
    pageSize: 50,
    autoLoad: false    
});