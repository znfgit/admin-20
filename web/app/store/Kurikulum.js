Ext.define('Admin.store.Kurikulum', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.Kurikulum',
    model: 'Admin.model.Kurikulum',
    pageSize: 500000,
    autoLoad: false    
});