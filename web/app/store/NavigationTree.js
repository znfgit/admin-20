Ext.define('Admin.store.NavigationTree', {
    extend: 'Ext.data.TreeStore',

    storeId: 'NavigationTree',
    root: {
        expanded: true,
        children: [
            {
                text:   'Beranda',
                view:   'dashboard.Dashboard',
                leaf:   true,
                iconCls: 'right-icon x-fa fa-desktop',
                routeId: 'dashboard'
            },
            {
                text:   'Profil Anda',
                view:   'Pengguna.ProfilPengguna',
                leaf:   true,
                iconCls: 'right-icon x-fa fa-user',
                routeId: 'profilPengguna',
                id: 'pages-profil-pengguna'
            }
            // ,{
            //     text:   'Progres Pengiriman',
            //     view:   'progres.Pengiriman',
            //     iconCls: 'right-icon x-fa fa-send ',
            //     leaf:   true,
            //     routeId: 'Pengiriman'

            // }
            // ,{
            //     text: 'Daftar Pengguna',
            //     view: 'Pengguna.Pengguna',
            //     leaf: true,
            //     iconCls: 'x-fa fa-list-ul',
            //     id: 'pages-pengguna',
            //     routeId:'Pengguna'
            // }
            ,{
                text: 'Progres',
                expanded: false,
                selectable: false,
                iconCls: 'x-fa fa-simplybuilt',
                routeId : 'pages-parent-administrasi',
                id:       'pages-parent-administrasi',
                children: [
                    {
                        text:   'Pengiriman',
                        view:   'progres.Pengiriman',
                        iconCls: 'right-icon x-fa fa-send ',
                        leaf:   true,
                        routeId: 'Pengiriman'

                    }
                    ,{
                        text:   'Kualitas Data',
                        view:   'progres.KualitasDataSekolah',
                        iconCls: 'right-icon x-fa fa-line-chart ',
                        leaf:   true,
                        id: 'pages-kualitas-data',
                        routeId: 'KualitasData'

                    }
                    ,{
                        text:   'Pencapaian Kualitas Bulanan',
                        view:   'progres.Pencapaian',
                        iconCls: 'right-icon x-fa fa-line-chart ',
                        leaf:   true,
                        id: 'pages-kualitas-bulanan',
                        routeId: 'PencapaianKualitasBulanan'

                    }
                ]
            }
            ,{
                text: 'Data Pokok',
                expanded: false,
                selectable: false,
                iconCls: 'x-fa fa-cloud',
                routeId : 'pages-parent-data-pokok',
                id:       'pages-parent-data-pokok',
                children: [
                    {
                        text: 'Satuan Pendidikan',
                        view: 'SatuanPendidikan.SatuanPendidikan',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'SatuanPendidikan'
                    },
                    // {
                    //     text: 'PTK',
                    //     view: 'Ptk.Ptk',
                    //     leaf: true,
                    //     iconCls: 'x-fa fa-bar-chart-o',
                    //     routeId:'PTK'
                    // }
                ]
            }
            ,{
                text: 'Tabulasi SP',
                expanded: false,
                selectable: false,
                iconCls: 'x-fa fa-building',
                routeId : 'pages-parent',
                id:       'pages-parent',
                children: [
                    {
                        text: 'Ringkasan',
                        view: 'SatuanPendidikan.SpRingkasan',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'SpRingkasan'
                    },
                    {
                        text: 'Waktu Penyelenggaraan',
                        view: 'SatuanPendidikan.SpWaktuPenyelenggaraan',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'SpWaktuPenyelenggaraan'
                    }
                    ,{
                        text: 'Kurikulum',
                        view: 'SatuanPendidikan.SpKurikulum',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'SpKurikulum'
                    }
                ]
            }
            ,{
                text: 'Tabulasi GTK',
                expanded: false,
                selectable: false,
                iconCls: 'x-fa fa-graduation-cap',
                routeId : 'pages-parent-tabulasi-ptk',
                id:       'pages-parent-tabulasi-ptk',
                children: [
                    {
                        text: 'Ringkasan',
                        view: 'Ptk.PtkRingkasan',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'PtkRingkasan'
                    },
                    {
                        text: 'Agama',
                        view: 'Ptk.PtkAgama',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'PtkAgama'
                    },{
                        text: 'Status Kepegawaian',
                        view: 'Ptk.PtkStatusPegawai',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'PtkStatusPegawai'
                    },{
                        text: 'Kualifikasi',
                        view: 'Ptk.PtkKualifikasi',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'PtkKualifikasi'
                    }
                ]
            }
            ,{
                text: 'Tabulasi PD',
                expanded: false,
                selectable: false,
                iconCls: 'x-fa fa-child',
                routeId : 'pages-parent-tabulasi-pd',
                id:       'pages-parent-tabulasi-pd',
                children: [
                    {
                        text: 'Ringkasan',
                        view: 'PesertaDidik.PdRingkasan',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'PdRingkasan'
                    },
                    {
                        text: 'Tingkat',
                        view: 'PesertaDidik.PdTingkat',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'PdTingkat'
                    },
                    {
                        text: 'Agama',
                        view: 'PesertaDidik.PdAgama',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'PdAgama'
                    },
                    {
                        text: 'Usia',
                        view: 'PesertaDidik.PdUsia',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'PdUsia'
                    },
                    {
                        text: 'Angka Putus Sekolah',
                        view: 'PesertaDidik.PdAngkaPutusSekolah',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'PdAngkaPutusSekolah'
                    },
                    {
                        text: 'Kelulusan',
                        view: 'PesertaDidik.PdAngkaKelulusan',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'PdAngkaKelulusan'
                    },
                    {
                        text: 'Angka Mengulang',
                        view: 'PesertaDidik.PdAngkaMengulang',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'PdAngkaMengulang'
                    }
                ]
            }
            ,{
                text: 'Tabulasi Sarpras',
                expanded: false,
                selectable: false,
                iconCls: 'x-fa fa-briefcase',
                routeId : 'pages-parent-tabulasi-sarpras',
                id:       'pages-parent-tabulasi-sarpras',
                children: [
                    {
                        text: 'Kerusakan',
                        view: 'Sarpras.SarprasTingkatKerusakan',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId:'SarprasTingkatKerusakan'
                    },
                    {
                        text: 'RKB',
                        view: 'Sarpras.SarprasRkb',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart',
                        routeId:'RKB'
                    }
                ]
            },
            {
                text: 'Tabulasi Rombel',
                expanded: false,
                selectable: false,
                iconCls: 'x-fa fa-users',
                routeId : 'pages-parent-tabulasi-sarpras',
                id:       'pages-parent-tabulasi-sarpras',
                children: [
                    {
                        text: 'Ringkasan',
                        view: 'RombonganBelajar.RingkasanRombonganBelajar',
                        leaf: true,
                        iconCls: 'x-fa fa-bar-chart-o',
                        routeId: 'RombelRingkasan'
                    }
                ]
            }
            ,{
                text: 'Custom Query (Beta)',
                expanded: false,
                selectable: false,
                iconCls: 'x-fa fa-sliders',
                routeId : 'pages-parent-custom-query',
                id:       'pages-parent-custom-query',
                children: [
                    {
                        text:   'Satuan Pendidikan',
                        view:   'customQuery.customQuery',
                        leaf:   true,
                        iconCls: 'right-icon x-fa fa-sliders',
                        routeId: 'CustomQuery'
                    }   
                ]
            }
            ,{
                text:   'Peta',
                view:   'Peta.Peta',
                leaf:   true,
                iconCls: 'right-icon x-fa fa-map-marker',
                routeId: 'peta'
            }
            // {
            //     text:   'Tentang',
            //     view:   'Tentang.Tentang',
            //     leaf:   true,
            //     iconCls: 'right-icon x-fa fa-comments-o',
            //     id: 'page-tentang',
            //     routeId: 'tentang'
            // }
        ]
    },
    fields: [
        {
            name: 'text'
        }
    ]
});
