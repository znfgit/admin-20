Ext.define('Admin.store.BidangStudi', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.BidangStudi',
    model: 'Admin.model.BidangStudi',
    pageSize: 50,
    autoLoad: false
});