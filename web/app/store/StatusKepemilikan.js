Ext.define('Admin.store.StatusKepemilikan', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.StatusKepemilikan',
    model: 'Admin.model.StatusKepemilikan',
    pageSize: 50,
    autoLoad: false    
});