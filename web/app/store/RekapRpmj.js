Ext.define('Admin.store.RekapRpmj', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.RekapRpmj',
    model: 'Admin.model.RekapRpmj',
    pageSize: 50,
    autoLoad: false    
});