Ext.define('Admin.store.Peran', {
    extend: 'Ext.data.Store',
    requires: 'Admin.model.Peran',
    model: 'Admin.model.Peran',
    pageSize: 50,
    autoLoad: false    
});