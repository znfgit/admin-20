Ext.define('Admin.model.StatusKepemilikan', {
    extend: 'Ext.data.Model',
    idProperty: 'status_kepemilikan_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'status_kepemilikan_id', type: 'float'  },
        { name: 'nama', type: 'string'  },
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'}
    ],
    proxy: {
        type: 'rest',
        url : 'rest/StatusKepemilikan',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data StatusKepemilikan ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});