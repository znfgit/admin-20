Ext.define('Admin.model.JenisPtk', {
    extend: 'Ext.data.Model',
    idProperty: 'jenis_ptk_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'jenis_ptk_id', type: 'float'  },
        { name: 'jenis_ptk', type: 'string'  },
        { name: 'guru_kelas', type: 'float'  },
        { name: 'guru_matpel', type: 'float'  },
        { name: 'guru_bk', type: 'float'  },
        { name: 'guru_inklusi', type: 'float'  },
        { name: 'pengawas_satdik', type: 'float'  },
        { name: 'pengawas_plb', type: 'float'  },
        { name: 'pengawas_matpel', type: 'float'  },
        { name: 'pengawas_bidang', type: 'float'  },
        { name: 'tas', type: 'float'  },
        { name: 'formal', type: 'float'  },
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'}
    ],
    proxy: {
        type: 'rest',
        url : 'CustomRest/JenisPtk',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data JenisPtk ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});