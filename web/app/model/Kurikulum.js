Ext.define('Admin.model.Kurikulum', {
    extend: 'Ext.data.Model',
    idProperty: 'kurikulum_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'kurikulum_id', type: 'int'  },
        { name: 'jurusan_id', type: 'string', useNull: true },
        { name: 'jurusan_id_str', type: 'string'  },
        { name: 'jenjang_pendidikan_id', type: 'float', useNull: true },
        { name: 'jenjang_pendidikan_id_str', type: 'string'  },
        { name: 'nama_kurikulum', type: 'string'  },
        { name: 'mulai_berlaku', type: 'date',  dateFormat: 'Y-m-d'},
        { name: 'sistem_sks', type: 'float'  },
        { name: 'total_sks', type: 'float'  },
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'}
    ],
    proxy: {
        type: 'rest',
        url : 'rest/Kurikulum',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Kurikulum ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});