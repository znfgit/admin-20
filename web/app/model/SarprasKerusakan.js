Ext.define('admin.model.SarprasKerusakan', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'desk', type: 'string'  }, 
        { name: 'jumlah', type: 'float'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'sarpras/SarprasKerusakan',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});