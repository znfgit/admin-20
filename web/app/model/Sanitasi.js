Ext.define('Admin.model.Sanitasi', {
    extend: 'Ext.data.Model',
    idProperty: 'sanitasi_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'sanitasi_id', type: 'string'  },
        { name: 'sekolah_id', type: 'string', useNull: true },
        { name: 'sekolah_id_str', type: 'string'  },
        { name: 'semester_id', type: 'string', useNull: true },
        { name: 'semester_id_str', type: 'string'  },
        { name: 'sumber_air_id', type: 'float', useNull: true },
        { name: 'sumber_air_id_str', type: 'string'  },
        { name: 'ketersediaan_air', type: 'float'  },
        { name: 'kecukupan_air', type: 'float'  },
        { name: 'minum_siswa', type: 'float'  },
        { name: 'memproses_air', type: 'float'  },
        { name: 'siswa_bawa_air', type: 'float'  },
        { name: 'toilet_siswa_laki', type: 'float'  },
        { name: 'toilet_siswa_perempuan', type: 'float'  },
        { name: 'toilet_siswa_kk', type: 'float'  },
        { name: 'toilet_siswa_kecil', type: 'float'  },
        { name: 'tempat_cuci_tangan', type: 'float'  },
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'soft_delete', type: 'float'  },
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'updater_id', type: 'string'  }
    ],
    proxy: {
        type: 'rest',
        url : 'rest/Sanitasi',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Sanitasi ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});