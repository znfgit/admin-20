Ext.define('Admin.model.ManajemenPengguna', {
    extend: 'Ext.data.Model',
    idProperty: 'pengguna_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'pengguna_id', type: 'string'  },
        { name: 'sekolah_id', type: 'string', useNull: true },
        { name: 'sekolah_id_str', type: 'string'  },
        { name: 'lembaga_id', type: 'string', useNull: true },
        { name: 'lembaga_id_str', type: 'string'  },
        { name: 'yayasan_id', type: 'string'  },
        { name: 'la_id', type: 'string'  },
        { name: 'peran_id', type: 'int', useNull: true },
        { name: 'peran_id_str', type: 'string'  },
        { name: 'username', type: 'string'  },
        { name: 'password', type: 'string'  },
        { name: 'nama', type: 'string'  },
        { name: 'nip_nim', type: 'string'  },
        { name: 'jabatan_lembaga', type: 'string'  },
        { name: 'ym', type: 'string'  },
        { name: 'skype', type: 'string'  },
        { name: 'alamat', type: 'string'  },
        { name: 'kode_wilayah', type: 'string', useNull: true },
        { name: 'kode_wilayah_str', type: 'string'  },
        { name: 'no_telepon', type: 'string'  },
        { name: 'no_hp', type: 'string'  },
        { name: 'aktif', type: 'float'  },
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'soft_delete', type: 'float'  },
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'updater_id', type: 'string'  }
    ],
    proxy: {
        type: 'rest',
        url : 'Administrasi/ManajemenPengguna',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Pengguna ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});