Ext.define('Admin.model.PdPerJurusan', {
    extend: 'Ext.data.Model',
    idProperty: 'nama_jurusan_sp',
    clientIdProperty: 'ext_client_id',
    proxy: {
        type: 'rest',
        url : 'PesertaDidik/PdPerJurusan',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Kurikulum ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});