Ext.define('Admin.model.SarprasRkbSp', {
    extend: 'Ext.data.Model',
    idProperty: 'tingkat_kerusakan_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'kode_wilayah', type: 'string'  },
        { name: 'nama', type: 'string'  },
        { name: 'r_kelas_baik', type: 'float'  },
        { name: 'r_kelas_rusak_ringan', type: 'float'  },
        { name: 'r_kelas_rusak_sedang', type: 'float'  },
        { name: 'r_kelas_rusak_berat', type: 'float'  },
        { name: 'bengkel_baik', type: 'float'  },
        { name: 'bengkel_rusak_ringan', type: 'float'  },
        { name: 'bengkel_rusak_sedang', type: 'float'  },
        { name: 'bengkel_rusak_berat', type: 'float'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'sarpras/RkbSp',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Sekolah ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});