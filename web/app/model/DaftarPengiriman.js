Ext.define('Admin.model.DaftarPengiriman', {
    extend: 'Ext.data.Model',
    idProperty: 'daftar_pengiriman_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'kode_wilayah', type: 'string'  },
        { name: 'nama', type: 'string'  },
        { name: 'tk', type: 'float'  },
        { name: 'kb', type: 'float'  },
        { name: 'tpa', type: 'float'  },
        { name: 'sps', type: 'float'  },
        { name: 'kirim_tk', type: 'float'  },
        { name: 'kirim_kb', type: 'float'  },
        { name: 'kirim_tpa', type: 'float'  },
        { name: 'kirim_sps', type: 'float'  },
        { name: 'sisa_tk', type: 'float'  },
        { name: 'sisa_kb', type: 'float'  },
        { name: 'sisa_tpa', type: 'float'  },
        { name: 'sisa_sps', type: 'float'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'monitoring/getMonitoring',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Sekolah ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});