Ext.define('Admin.model.RombonganBelajar', {
    extend: 'Ext.data.Model',
    idProperty: 'rombongan_belajar_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'rombongan_belajar_id', type: 'string'  },
        { name: 'semester_id', type: 'string', useNull: true },
        { name: 'semester_id_str', type: 'string'  },
        { name: 'sekolah_id', type: 'string', useNull: true },
        { name: 'sekolah_id_str', type: 'string'  },
        { name: 'tingkat_pendidikan_id', type: 'float', useNull: true },
        { name: 'tingkat_pendidikan_id_str', type: 'string'  },
        { name: 'jurusan_sp_id', type: 'string', useNull: true },
        { name: 'jurusan_sp_id_str', type: 'string'  },
        { name: 'kurikulum_id', type: 'int', useNull: true },
        { name: 'kurikulum_id_str', type: 'string'  },
        { name: 'nama', type: 'string'  },
        { name: 'ptk_id', type: 'string', useNull: true },
        { name: 'ptk_id_str', type: 'string'  },
        { name: 'prasarana_id', type: 'string', useNull: true },
        { name: 'prasarana_id_str', type: 'string'  },
        { name: 'moving_class', type: 'float'  },
        { name: 'jenis_rombel', type: 'float'  },
        { name: 'sks', type: 'float'  },
        { name: 'kebutuhan_khusus_id', type: 'int', useNull: true },
        { name: 'kebutuhan_khusus_id_str', type: 'string'  },
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'soft_delete', type: 'float'  },
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'updater_id', type: 'string'  }
    ],
    proxy: {
        type: 'rest',
        url : 'RombonganBelajar/RombonganBelajar',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data RombonganBelajar ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});