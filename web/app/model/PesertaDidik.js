Ext.define('Admin.model.PesertaDidik', {
    extend: 'Ext.data.Model',
    idProperty: 'peserta_didik_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'peserta_didik_id', type: 'string'  },
        { name: 'nama', type: 'string'  },
        { name: 'jenis_kelamin', type: 'string'  },
        { name: 'nisn', type: 'string'  },
        { name: 'nik', type: 'string'  },
        { name: 'tempat_lahir', type: 'string'  },
        { name: 'tanggal_lahir', type: 'date',  dateFormat: 'Y-m-d'},
        { name: 'agama_id', type: 'int', useNull: true },
        { name: 'agama_id_str', type: 'string'  },
        { name: 'kebutuhan_khusus_id', type: 'int', useNull: true },
        { name: 'kebutuhan_khusus_id_str', type: 'string'  },
        { name: 'sekolah_id', type: 'string', useNull: true },
        { name: 'sekolah_id_str', type: 'string'  },
        { name: 'alamat_jalan', type: 'string'  },
        { name: 'rt', type: 'float'  },
        { name: 'rw', type: 'float'  },
        { name: 'nama_dusun', type: 'string'  },
        { name: 'desa_kelurahan', type: 'string'  },
        { name: 'kode_wilayah', type: 'string', useNull: true },
        { name: 'kode_wilayah_str', type: 'string'  },
        { name: 'kode_pos', type: 'string'  },
        { name: 'jenis_tinggal_id', type: 'float', useNull: true },
        { name: 'jenis_tinggal_id_str', type: 'string'  },
        { name: 'alat_transportasi_id', type: 'float', useNull: true },
        { name: 'alat_transportasi_id_str', type: 'string'  },
        { name: 'nomor_telepon_rumah', type: 'string'  },
        { name: 'nomor_telepon_seluler', type: 'string'  },
        { name: 'email', type: 'string'  },
        { name: 'penerima_kps', type: 'float'  },
        { name: 'no_kps', type: 'string'  },
        { name: 'status_data', type: 'int'  },
        { name: 'nama_ayah', type: 'string'  },
        { name: 'tahun_lahir_ayah', type: 'float'  },
        { name: 'jenjang_pendidikan_ayah', type: 'float', useNull: true },
        { name: 'jenjang_pendidikan_ayah_str', type: 'string'  },
        { name: 'pekerjaan_id_ayah', type: 'int', useNull: true },
        { name: 'pekerjaan_id_ayah_str', type: 'string'  },
        { name: 'penghasilan_id_ayah', type: 'int', useNull: true },
        { name: 'penghasilan_id_ayah_str', type: 'string'  },
        { name: 'kebutuhan_khusus_id_ayah', type: 'int', useNull: true },
        { name: 'kebutuhan_khusus_id_ayah_str', type: 'string'  },
        { name: 'nama_ibu_kandung', type: 'string'  },
        { name: 'tahun_lahir_ibu', type: 'float'  },
        { name: 'jenjang_pendidikan_ibu', type: 'float', useNull: true },
        { name: 'jenjang_pendidikan_ibu_str', type: 'string'  },
        { name: 'penghasilan_id_ibu', type: 'int', useNull: true },
        { name: 'penghasilan_id_ibu_str', type: 'string'  },
        { name: 'pekerjaan_id_ibu', type: 'int', useNull: true },
        { name: 'pekerjaan_id_ibu_str', type: 'string'  },
        { name: 'kebutuhan_khusus_id_ibu', type: 'int', useNull: true },
        { name: 'kebutuhan_khusus_id_ibu_str', type: 'string'  },
        { name: 'nama_wali', type: 'string'  },
        { name: 'tahun_lahir_wali', type: 'float'  },
        { name: 'jenjang_pendidikan_wali', type: 'float', useNull: true },
        { name: 'jenjang_pendidikan_wali_str', type: 'string'  },
        { name: 'pekerjaan_id_wali', type: 'int', useNull: true },
        { name: 'pekerjaan_id_wali_str', type: 'string'  },
        { name: 'penghasilan_id_wali', type: 'int', useNull: true },
        { name: 'penghasilan_id_wali_str', type: 'string'  },
        { name: 'kewarganegaraan', type: 'string', useNull: true },
        { name: 'kewarganegaraan_str', type: 'string'  },
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'soft_delete', type: 'float'  },
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'updater_id', type: 'string'  }
    ],
    proxy: {
        type: 'rest',
        url : 'PesertaDidik/PesertaDidik',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data PesertaDidik ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});