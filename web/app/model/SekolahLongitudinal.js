Ext.define('Admin.model.SekolahLongitudinal', {
    extend: 'Ext.data.Model',
    idProperty: 'sekolah_longitudinal_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'sekolah_longitudinal_id', type: 'string'  },
        { name: 'sekolah_id', type: 'string', useNull: true },
        { name: 'sekolah_id_str', type: 'string'  },
        { name: 'semester_id', type: 'string', useNull: true },
        { name: 'semester_id_str', type: 'string'  },
        { name: 'waktu_penyelenggaraan_id', type: 'float', useNull: true },
        { name: 'waktu_penyelenggaraan_id_str', type: 'string'  },
        { name: 'wilayah_terpencil', type: 'float'  },
        { name: 'wilayah_perbatasan', type: 'float'  },
        { name: 'wilayah_transmigrasi', type: 'float'  },
        { name: 'wilayah_adat_terpencil', type: 'float'  },
        { name: 'wilayah_bencana_alam', type: 'float'  },
        { name: 'wilayah_bencana_sosial', type: 'float'  },
        { name: 'partisipasi_bos', type: 'float'  },
        { name: 'sertifikasi_iso_id', type: 'int', useNull: true },
        { name: 'sertifikasi_iso_id_str', type: 'string'  },
        { name: 'sumber_listrik_id', type: 'float', useNull: true },
        { name: 'sumber_listrik_id_str', type: 'string'  },
        { name: 'daya_listrik', type: 'float'  },
        { name: 'akses_internet_id', type: 'int', useNull: true },
        { name: 'akses_internet_id_str', type: 'string'  },
        { name: 'akses_internet_2_id', type: 'int', useNull: true },
        { name: 'akses_internet_2_id_str', type: 'string'  },
        { name: 'blob_id', type: 'string'  },
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'soft_delete', type: 'float'  },
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'updater_id', type: 'string'  }
    ],
    proxy: {
        type: 'rest',
        url : 'rest/SekolahLongitudinal',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data SekolahLongitudinal ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});