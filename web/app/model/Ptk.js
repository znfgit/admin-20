Ext.define('Admin.model.Ptk', {
    extend: 'Ext.data.Model',
    idProperty: 'ptk_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'ptk_id', type: 'string'  },
        { name: 'nama', type: 'string'  },
        { name: 'nip', type: 'string'  },
        { name: 'jenis_kelamin', type: 'string'  },
        { name: 'tempat_lahir', type: 'string'  },
        { name: 'tanggal_lahir', type: 'date',  dateFormat: 'Y-m-d'},
        { name: 'nik', type: 'string'  },
        { name: 'niy_nigk', type: 'string'  },
        { name: 'nuptk', type: 'string'  },
        { name: 'status_kepegawaian_id', type: 'int', useNull: true },
        { name: 'status_kepegawaian_id_str', type: 'string'  },
        { name: 'jenis_ptk_id', type: 'float', useNull: true },
        { name: 'jenis_ptk_id_str', type: 'string'  },
        { name: 'pengawas_bidang_studi_id', type: 'int', useNull: true },
        { name: 'pengawas_bidang_studi_id_str', type: 'string'  },
        { name: 'agama_id', type: 'int', useNull: true },
        { name: 'agama_id_str', type: 'string'  },
        { name: 'alamat_jalan', type: 'string'  },
        { name: 'rt', type: 'float'  },
        { name: 'rw', type: 'float'  },
        { name: 'nama_dusun', type: 'string'  },
        { name: 'desa_kelurahan', type: 'string'  },
        { name: 'kode_wilayah', type: 'string', useNull: true },
        { name: 'kode_wilayah_str', type: 'string'  },
        { name: 'kode_pos', type: 'string'  },
        { name: 'no_telepon_rumah', type: 'string'  },
        { name: 'no_hp', type: 'string'  },
        { name: 'email', type: 'string'  },
        { name: 'entry_sekolah_id', type: 'string', useNull: true },
        { name: 'entry_sekolah_id_str', type: 'string'  },
        { name: 'status_keaktifan_id', type: 'float', useNull: true },
        { name: 'status_keaktifan_id_str', type: 'string'  },
        { name: 'sk_cpns', type: 'string'  },
        { name: 'tgl_cpns', type: 'date',  dateFormat: 'Y-m-d'},
        { name: 'sk_pengangkatan', type: 'string'  },
        { name: 'tmt_pengangkatan', type: 'date',  dateFormat: 'Y-m-d'},
        { name: 'lembaga_pengangkat_id', type: 'float', useNull: true },
        { name: 'lembaga_pengangkat_id_str', type: 'string'  },
        { name: 'pangkat_golongan_id', type: 'float', useNull: true },
        { name: 'pangkat_golongan_id_str', type: 'string'  },
        { name: 'keahlian_laboratorium_id', type: 'int', useNull: true },
        { name: 'keahlian_laboratorium_id_str', type: 'string'  },
        { name: 'sumber_gaji_id', type: 'float', useNull: true },
        { name: 'sumber_gaji_id_str', type: 'string'  },
        { name: 'nama_ibu_kandung', type: 'string'  },
        { name: 'status_perkawinan', type: 'float'  },
        { name: 'nama_suami_istri', type: 'string'  },
        { name: 'nip_suami_istri', type: 'string'  },
        { name: 'pekerjaan_suami_istri', type: 'int', useNull: true },
        { name: 'pekerjaan_suami_istri_str', type: 'string'  },
        { name: 'tmt_pns', type: 'date',  dateFormat: 'Y-m-d'},
        { name: 'sudah_lisensi_kepala_sekolah', type: 'float'  },
        { name: 'jumlah_sekolah_binaan', type: 'int'  },
        { name: 'pernah_diklat_kepengawasan', type: 'float'  },
        { name: 'status_data', type: 'int'  },
        { name: 'mampu_handle_kk', type: 'int', useNull: true },
        { name: 'mampu_handle_kk_str', type: 'string'  },
        { name: 'keahlian_braille', type: 'float'  },
        { name: 'keahlian_bhs_isyarat', type: 'float'  },
        { name: 'npwp', type: 'string'  },
        { name: 'kewarganegaraan', type: 'string', useNull: true },
        { name: 'kewarganegaraan_str', type: 'string'  },
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'soft_delete', type: 'float'  },
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'updater_id', type: 'string'  }
    ],
    proxy: {
        type: 'rest',
        url : 'Ptk/Ptk',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Ptk ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});