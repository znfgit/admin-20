Ext.define('Admin.model.JurusanSp', {
    extend: 'Ext.data.Model',
    idProperty: 'jurusan_sp_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'jurusan_sp_id', type: 'string'  },
        { name: 'sekolah_id', type: 'string', useNull: true },
        { name: 'sekolah_id_str', type: 'string'  },
        { name: 'kebutuhan_khusus_id', type: 'int', useNull: true },
        { name: 'kebutuhan_khusus_id_str', type: 'string'  },
        { name: 'jurusan_id', type: 'string', useNull: true },
        { name: 'jurusan_id_str', type: 'string'  },
        { name: 'nama_jurusan_sp', type: 'string'  },
        { name: 'sk_izin', type: 'string'  },
        { name: 'tanggal_sk_izin', type: 'date',  dateFormat: 'Y-m-d'},
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'soft_delete', type: 'float'  },
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'updater_id', type: 'string'  }
    ],
    proxy: {
        type: 'rest',
        url : 'rest/JurusanSp',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data JurusanSp ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});