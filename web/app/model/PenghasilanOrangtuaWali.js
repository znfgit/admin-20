Ext.define('Admin.model.PenghasilanOrangtuaWali', {
    extend: 'Ext.data.Model',
    idProperty: 'penghasilan_orangtua_wali_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'penghasilan_orangtua_wali_id', type: 'int'  },
        { name: 'nama', type: 'string'  },
        { name: 'batas_bawah', type: 'int'  },
        { name: 'batas_atas', type: 'int'  },
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'}
    ],
    proxy: {
        type: 'rest',
        url : 'rest/PenghasilanOrangtuaWali',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data PenghasilanOrangtuaWali ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});