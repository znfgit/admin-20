Ext.define('Admin.model.BentukPendidikan', {
    extend: 'Ext.data.Model',
    idProperty: 'bentuk_pendidikan_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'bentuk_pendidikan_id', type: 'int'  },
        { name: 'nama', type: 'string'  },
        { name: 'jenjang_paud', type: 'float'  },
        { name: 'jenjang_tk', type: 'float'  },
        { name: 'jenjang_sd', type: 'float'  },
        { name: 'jenjang_smp', type: 'float'  },
        { name: 'jenjang_sma', type: 'float'  },
        { name: 'jenjang_tinggi', type: 'float'  },
        { name: 'direktorat_pembinaan', type: 'string'  },
        { name: 'aktif', type: 'float'  },
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'}
    ],
    proxy: {
        type: 'rest',
        url : 'rest/BentukPendidikan',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data BentukPendidikan ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});