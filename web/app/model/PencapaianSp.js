Ext.define('Admin.model.PencapaianSp', {
    extend: 'Ext.data.Model',
    idProperty: 'rekap_sekolah_id',
    clientIdProperty: 'ext_client_id',
    proxy: {
        type: 'rest',
        url : 'progres/pencapaian/PencapaianSp',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data PencapaianSp ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});