Ext.define('Admin.model.PdAngkaPutusSekolahSp', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    clientIdProperty: 'ext_client_id',
    // fields: [
    //     { name: 'kode_wilayah', type: 'string'  },
    //     { name: 'nama', type: 'string'  }
        
    // ],
    proxy: {
        type: 'rest',
        url : 'PesertaDidik/PdAngkaPutusSekolahSp',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Sekolah ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});