Ext.define('Admin.model.PdTingkatBarChart', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    proxy: {
        type: 'rest',
        url : 'PesertaDidik/PdTingkatBarChart',
        timeout : 1200000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});