Ext.define('Admin.model.Blockgrant', {
    extend: 'Ext.data.Model',
    idProperty: 'blockgrant_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'blockgrant_id', type: 'string'  },
        { name: 'sekolah_id', type: 'string', useNull: true },
        { name: 'sekolah_id_str', type: 'string'  },
        { name: 'nama', type: 'string'  },
        { name: 'tahun', type: 'float'  },
        { name: 'jenis_bantuan_id', type: 'int', useNull: true },
        { name: 'jenis_bantuan_id_str', type: 'string'  },
        { name: 'sumber_dana_id', type: 'float', useNull: true },
        { name: 'sumber_dana_id_str', type: 'string'  },
        { name: 'besar_bantuan', type: 'float'  },
        { name: 'dana_pendamping', type: 'float'  },
        { name: 'peruntukan_dana', type: 'string'  },
        { name: 'asal_data', type: 'string'  },
        { name: 'last_update', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'soft_delete', type: 'float'  },
        { name: 'last_sync', type: 'date',  dateFormat: 'Y-m-d H:i:s'},
        { name: 'updater_id', type: 'string'  }
    ],
    proxy: {
        type: 'rest',
        url : 'rest/Blockgrant',
        timeout: 120000,
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Blockgrant ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});