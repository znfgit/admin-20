Ext.require([
    'Ext.window.MessageBox',
    'Ext.tip.*',
    'Ext.Component',
    'Admin.view.Sarpras.SarprasTingkatKerusakan',
    'Admin.view.Sarpras.SarprasTingkatKerusakanGrid'
]);

Ext.define('Admin.controller.Sarpras', {
    extend: 'Ext.app.Controller',
    init: function() {
        this.control({
            'sarpraskerusakanprasaranagrid': {
                afterrender: this.SarprasKerusakanPrasaranaGridSetup
            }
        });
        
        this.callParent(arguments);
    },
    SarprasKerusakanPrasaranaGridSetup: function(grid){
        grid.getStore().load();
    }
});