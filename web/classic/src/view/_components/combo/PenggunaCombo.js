Ext.define('Admin.view._components.combo.PenggunaCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    // pageSize: 20,
    valueField: 'pengguna_id',
    displayField: 'nama',
    label: 'Ref.pengguna',    
    alias: 'widget.combopengguna',
    emptyText:'Cari Pengguna',
    //hideTrigger: false,    
    //tpl: '<div class="search-item">{nama} - {nama_kabupaten}</div>',
    listConfig: {
        loadingText: 'Mencari...',
        emptyText: 'Pengguna tidak ditemukan'

		/*
        // Custom rendering template for each item
        getInnerTpl: function() {
            return '<div class="search-item">{nama} - {nama_kabupaten}</div>';
        }
		*/
    },
    listeners: {
        change: function(combo, options) {
            
        }
    },
    initComponent: function() {
        this.store = Ext.create('Admin.store.Pengguna');
        this.callParent(arguments); 
    }
});
