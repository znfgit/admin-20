Ext.define('Admin.view._components.combo.StatusMutasiCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    // pageSize: 20,
    valueField: 'status_mutasi_id',
    displayField: 'nama',
    label: 'Ref.status_mutasi',    
    alias: 'widget.combostatusmutasi',
    emptyText:'Cari Status Mutasi',
    //hideTrigger: false,    
    //tpl: '<div class="search-item">{nama} - {nama_kabupaten}</div>',
    listConfig: {
        loadingText: 'Mencari...',
        emptyText: 'Status Mutasi tidak ditemukan'

		/*
        // Custom rendering template for each item
        getInnerTpl: function() {
            return '<div class="search-item">{nama} - {nama_kabupaten}</div>';
        }
		*/
    },
    listeners: {
        change: function(combo, options) {
            
        }
    },
    initComponent: function() {
        this.store = Ext.create('Admin.store.StatusMutasi');
        this.callParent(arguments); 
    }
});
