Ext.define('Admin.view._components.combo.BidangStudiCombo', {
    extend: 'Ext.form.field.ComboBox',
    valueField: 'bidang_studi_id',
    displayField: 'bidang_studi',
    alias: 'widget.BidangStudicombo',
    fieldLabel: 'Bidang Studi',
    initComponent: function() {
        this.store = Ext.create('Admin.store.BidangStudi', {
            model: 'Admin.model.BidangStudi',
            sorters: ['bentuk_pendidikan_id']
        });

        this.callParent(arguments);
    }
});