Ext.define('Admin.view._components.combo.PeranCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'local',
    alias: 'widget.perancombo',
    valueField: 'peran_id',
    displayField: 'nama',
    label: 'Ref.peran',
	// editable: false,
    initComponent: function() {
        this.store = Ext.create('Admin.store.Peran', {
            model: 'Admin.model.Peran',
            sorters: ['peran_id'],
            autoLoad: true
        });
        this.callParent(arguments); 
    }
});