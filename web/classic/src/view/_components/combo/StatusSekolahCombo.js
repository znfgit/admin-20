var pilihStatusStore = Ext.create('Ext.data.Store', {
    fields: ['id', 'nama'],
    data : [
        {"id":"1", "nama":"Negeri"},
        {"id":"2", "nama":"Swasta"},
        {"id":"999", "nama":"Semua"}
    ]
});

Ext.define('Admin.view._components.combo.StatusSekolahCombo', {
    extend: 'Ext.form.field.ComboBox',
    valueField: 'id',
    displayField: 'nama',
    alias: 'widget.StatusSekolahCombo',
    initComponent: function() {
        this.store = pilihStatusStore;

        this.callParent(arguments);
    }
});