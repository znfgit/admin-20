Ext.define('Admin.view._components.combo.MstWilayahCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    // pageSize: 20,
    valueField: 'kode_wilayah',
    displayField: 'nama',
    label: 'Ref.mst wilayah',    
    alias: 'widget.mstwilayahcombo',
    //hideTrigger: false,
    listeners: {
        beforerender: function(combo, options) {
            // this.store.load();
        }
    },
    initComponent: function() {
        this.store = Ext.create('Admin.store.MstWilayah', {
            model: 'Admin.model.MstWilayah',
            sorters: ['mst_wilayah_id']
        });
        // this.store.load();
        
        this.callParent(arguments); 
    }
});