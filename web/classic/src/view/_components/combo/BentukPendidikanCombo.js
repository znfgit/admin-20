Ext.define('Admin.view._components.combo.BentukPendidikanCombo', {
    extend: 'Ext.form.field.ComboBox',
    valueField: 'bentuk_pendidikan_id',
    displayField: 'nama',
    alias: 'widget.bentukpendidikancombo',
    listeners: {
        afterrender: 'BentukPendidikanComboSetup'
        // afterrender: function(combo, options) {
        //     Ext.Ajax.request({
        //         url: 'session',
        //         success: function(response){
        //             var text = response.responseText;

        //             var res = Ext.JSON.decode(response.responseText);

        //             if(res.bp == 'SMA'){
        //                 combo.setValue(13);
        //                 combo.hide();

        //             }else if(res.bp == 'SMK'){
        //                 comboBp.setValue(15);
        //                 comboBp.hide();

        //             }else if(res.bp == 'SMLB'){
        //                 comboBp.setValue(14);
        //                 comboBp.hide();

        //             }
        //         }
        //     });
        // }
    },
    initComponent: function() {
        this.store = Ext.create('Admin.store.BentukPendidikan', {
            model: 'Admin.model.BentukPendidikan',
            sorters: ['bentuk_pendidikan_id']
        });

        this.callParent(arguments);
    }
});