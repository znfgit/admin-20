Ext.define('Admin.view._components.combo.StatusKepemilikanCombo', {
    extend: 'Ext.form.field.ComboBox',
    valueField: 'status_kepemilikan_id',
    displayField: 'nama',
    alias: 'widget.StatusKepemilikanCombo',
    initComponent: function() {
        this.store = Ext.create('Admin.store.StatusKepemilikan', {
            model: 'Admin.model.StatusKepemilikan',
            sorters: ['status_kepemilikan_id']
        });

        this.callParent(arguments);
    }
});