Ext.define('Admin.view._components.combo.JenisPtkCombo', {
    extend: 'Ext.form.field.ComboBox',
    valueField: 'jenis_ptk_id',
    displayField: 'jenis_ptk',
    alias: 'widget.jenisptkcombo',
    listeners: {
        afterrender: 'JenisPtkComboSetup'
        // afterrender: function(combo, options) {
        //     Ext.Ajax.request({
        //         url: 'session',
        //         success: function(response){
        //             var text = response.responseText;
                    
        //             var res = Ext.JSON.decode(response.responseText);

        //             if(res.bp == 'SMA'){
        //                 combo.setValue(13);
        //                 combo.hide();
                        
        //             }else if(res.bp == 'SMK'){
        //                 comboBp.setValue(15);
        //                 comboBp.hide();
                        
        //             }else if(res.bp == 'SMLB'){
        //                 comboBp.setValue(14);
        //                 comboBp.hide();
                        
        //             }
        //         }
        //     });
        // }
    },
    initComponent: function() {
        this.store = Ext.create('Admin.store.JenisPtk', {
            model: 'Admin.model.JenisPtk',
            sorters: ['jenis_ptk_id']
        });
        
        this.callParent(arguments); 
    }
});