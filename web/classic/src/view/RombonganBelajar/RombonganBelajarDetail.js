Ext.define('Admin.view.RombonganBelajar.RombonganBelajarDetail', {
    extend: 'Ext.container.Container',
    xtype: 'rombonganbelajardetail',

    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'Admin.view.RombonganBelajar.RombonganBelajarDetailGrid'
    ],

    controller: 'rombonganbelajardetail',

    cls: 'rombonganbelajardetail-container',

    layout: 'responsivecolumn',
    // layout: 'border',
    scrollable: 'y',
    items: [{
        xtype: 'rombonganbelajardetailgrid',
        title: 'Rekap Rombongan Belajar Detail',
        responsiveCls: 'big-100',
        minHeight:'100%'
    }]
});
