Ext.define('Admin.view.RombonganBelajar.RombonganBelajarDetailGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.rombonganbelajardetailgrid',
    title: '',
    selType: 'rowmodel',
    listeners:{
        afterrender:'RombonganBelajarDetailGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.RombonganBelajarDetail');
    },
    createDockedItems: function() {
        var panel = btn.up('sekolahform').up('panel').up('tabpanel').up('profilsatuanpendidikan');
        console.log(panel.sekolah_id);

        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                href:'#ProfilSatuanPendidikan/'+ panel.sekolah_id,
                hrefTarget: '_self'
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a,
            c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {

        var grid = this;

        this.store = this.createStore();

        Ext.apply(this, this.initialConfig);


        this.columns = [{
            header: 'No',
            width: 40,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            locked: true,
            renderer: function(v,p,r){
                // console.log(v);
                // console.log(p.recordIndex);
                // console.log(r);
                return (p.recordIndex + 1);
            }
        },
        {
            header: 'Nama Satuan Pendidikan',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            locked: true,
            renderer: function(v,p,r){
                return '<a style="color:#228ab7;text-decoration:none" href="#ProfilSatuanPendidikan/'+r.data.sekolah_id+'">'+v+'</a>';
            }
        }
        ,{
            text: 'Status Sekolah',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'negeri',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'swasta',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        }
        ,{
            text: 'Guru',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'guru_negeri',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'guru_swasta',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        }
        ,{
            text: 'Tendik',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'pegawai_negeri',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'pegawai_swasta',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        }
        ,{
            text: 'Peserta Didik',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'pd_negeri',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'pd_swasta',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        }
        ,{
            text: 'Rombel',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'rombel_negeri',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'rombel_swasta',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        }
        ];

        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();

        this.callParent(arguments);

        // this.store.load();
    }
});