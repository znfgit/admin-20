Ext.define('Admin.view.Ptk.RombonganBelajarGridController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.rombonganbelajargrid',
    RombonganBelajarGridSetup: function(grid){

        var sekolah_id = grid.up('tabpanel').up('profilsatuanpendidikan').sekolah_id;

        console.log(sekolah_id);

        grid.getStore().on('beforeload', function(store){
            Ext.apply(store.proxy.extraParams, {
                Soft_delete: 0,
                sekolah_id: sekolah_id
            });
        });
        
    	grid.getStore().load();
    }
});
