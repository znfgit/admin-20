Ext.define('Admin.view.RombonganBelajar.RombonganBelajarDetailController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.rombonganbelajardetail',
    RombonganBelajarDetailGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var panel = grid.up('rombonganbelajardetail');
            // console.log(panel);
            // console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: panel.id_level_wilayah,
                kode_wilayah: panel.kode_wilayah
            });


        });

        grid.getStore().on('load', function(store){

            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

            // var data = store.data;

            // var guru_negeri = 0;
            // var guru_swasta = 0;
            // var negeri = 0;
            // var swasta = 0;
            // var pegawai_negeri = 0;
            // var pegawai_swasta = 0;
            // var rombel_negeri = 0;
            // var rombel_swasta = 0;
            // var pd_negeri = 0;
            // var pd_swasta = 0;

            // data.each(function(record){
            //     // console.log(record.data);

            //     var dataRecord = record.data;
            //     // console.log(dataRecord);

            //     guru_negeri = guru_negeri+dataRecord.guru_negeri;
            //     guru_swasta = guru_swasta+dataRecord.guru_swasta;

            //     negeri = negeri+dataRecord.negeri;
            //     swasta = swasta+dataRecord.swasta;

            //     pegawai_negeri = pegawai_negeri+dataRecord.pegawai_negeri;
            //     pegawai_swasta = pegawai_swasta+dataRecord.pegawai_swasta;

            //     rombel_negeri = rombel_negeri+dataRecord.rombel_negeri;
            //     rombel_swasta = rombel_swasta+dataRecord.rombel_swasta;

            //     pd_negeri = pd_negeri+dataRecord.pd_negeri;
            //     pd_swasta = pd_swasta+dataRecord.pd_swasta;
            // });

            // // console.log(guru_negeri);

            // var recordConfig = {
            //     no : '',
            //     nama : '<b>Total<b>',
            //     negeri : negeri,
            //     swasta : swasta,
            //     guru_negeri : guru_negeri,
            //     guru_swasta : guru_swasta,
            //     pegawai_negeri: pegawai_negeri,
            //     pegawai_swasta: pegawai_swasta,
            //     rombel_negeri: rombel_negeri,
            //     rombel_swasta: rombel_swasta,
            //     pd_negeri: pd_negeri,
            //     pd_swasta: pd_swasta
            // };

            // var r = new Admin.model.SatuanPendidikanRingkasan(recordConfig);

            // grid.store.add(r);

        });

    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
    	combo.up('gridpanel').getStore().reload();
    }
    // TODO - Add control logic or remove if not needed
});
