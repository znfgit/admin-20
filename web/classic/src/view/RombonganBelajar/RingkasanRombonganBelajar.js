Ext.define('Admin.view.RombonganBelajar.RingkasanRombonganBelajar', {
    extend: 'Ext.container.Container',
    xtype: 'ringkasanrombonganbelajar',

    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'Admin.view.RombonganBelajar.RingkasanRombonganBelajarGrid'
    ],

    controller: 'ringkasanrombonganbelajar',

    cls: 'ringkasanrombonganbelajar-container',

    layout: 'responsivecolumn',
    // layout: 'border',
    scrollable: 'y',
    items: [{
        xtype: 'ringkasanrombonganbelajargrid',
        title: 'Tabel Rekap Rombongan Belajar ',
        responsiveCls: 'big-100',
        minHeight: '100%',
        createStore: function() {
            return Ext.create('Admin.store.RingkasanRombonganBelajar');
        }
    },{
        xtype: 'ringkasanrombonganbelajarbarchart',
        title: 'Rekap Rombongan Belajar Berdasarkan Tingkat',
        responsiveCls: 'big-100',
        minHeight:200,
        createStore: function() {
            return Ext.create('Admin.store.RingkasanRombonganBelajar');
        }
    }]
});
