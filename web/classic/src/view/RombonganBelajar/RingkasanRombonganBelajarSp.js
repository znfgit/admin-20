Ext.define('Admin.view.RombonganBelajar.RingkasanRombonganBelajarSp', {
    extend: 'Ext.container.Container',
    xtype: 'ringkasanrombonganbelajarsp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'ringkasanrombonganbelajarsp',
    
    cls: 'ringkasanrombonganbelajarsp-container',

    layout: 'responsivecolumn',
    // layout: 'border',
    scrollable: 'y',

    items: [
        {
            xtype: 'ringkasanrombonganbelajarspgrid',
            title: 'Ringkasan Rekap PTK',
            // Always 100% of container
            responsiveCls: 'big-100'
        }
    ]
});
