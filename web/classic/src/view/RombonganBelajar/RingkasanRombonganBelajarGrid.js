Ext.define('Admin.view.RombonganBelajar.RingkasanRombonganBelajarGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ringkasanrombonganbelajargrid',
    title: '',
    selType: 'rowmodel',
    listeners:{
        afterrender:'RingkasanRombonganBelajarGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.RingkasanRombonganBelajar');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                href:'#SpRingkasan',
                hrefTarget: '_self'
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'RombonganBelajar',
                paramMethod: 'RingkasanRombonganBelajar',
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a,
            c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {

        var grid = this;

        this.store = this.createStore();

        Ext.apply(this, this.initialConfig);


        this.columns = [{
            header: 'No',
            width: 40,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            locked: true,
            renderer: function(v,p,r){
                // console.log(v);
                // console.log(p.recordIndex);
                // console.log(r);
                return (p.recordIndex + 1);
            }
        }
        ,{
            header: 'Wilayah',
            width: 170,
            sortable: true,
            dataIndex: 'nama',
            locked: true,
            hideable: false,
            renderer: function(v,p,r){

                if(r.data.id_level_wilayah){
                    return '<a style="color:#228ab7;text-decoration:none" href="#RingkasanRombonganBelajarSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah+'">'+v+'</a>';
                }else{
                    return v;
                }

            }
        },{
            header: 'Tk.1',
            align:'right',
            dataIndex: 'pd_kelas_1',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        },{
            header: 'Tk.1',
            align:'right',
            dataIndex: 'pd_kelas_2',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        },{
            header: 'Tk.3',
            align:'right',
            dataIndex: 'pd_kelas_3',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        },{
            header: 'Tk.4',
            align:'right',
            dataIndex: 'pd_kelas_4',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        },{
            header: 'Tk.5',
            align:'right',
            dataIndex: 'pd_kelas_5',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        },{
            header: 'Tk.6',
            align:'right',
            dataIndex: 'pd_kelas_6',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        },{
            header: 'Tk.7',
            align:'right',
            dataIndex: 'pd_kelas_7',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        },{
            header: 'Tk.8',
            align:'right',
            dataIndex: 'pd_kelas_8',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        },{
            header: 'Tk.9',
            align:'right',
            dataIndex: 'pd_kelas_9',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        },{
            header: 'Tk.10',
            align:'right',
            dataIndex: 'pd_kelas_10',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        },{
            header: 'Tk.11',
            align:'right',
            dataIndex: 'pd_kelas_11',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        },{
            header: 'Tk.12',
            align:'right',
            dataIndex: 'pd_kelas_12',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        },{
            header: 'Tk.13',
            align:'right',
            dataIndex: 'pd_kelas_13',
            width: 80,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return grid.formatMoney(v);
            }
        }
        ];

        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();

        this.callParent(arguments);

        // this.store.load();
    }
});