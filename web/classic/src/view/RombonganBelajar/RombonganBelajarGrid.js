Ext.define('Admin.view.RombonganBelajar.RombonganBelajarGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.rombonganbelajargrid',
    title: '',
    selType: 'rowmodel',
    controller: 'rombonganbelajargrid',
    listeners:{
        afterrender:'RombonganBelajarGridSetup',
        itemdblclick: function(dv, record, item, index, e) {

        }
    },
    createStore: function() {
        return Ext.create('Admin.store.RombonganBelajar');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [/*{
                xtype: 'button',
                text: 'Lihat Detail Rombel',
                ui: 'soft-green',
                iconCls: 'x-fa fa-users',
                paramClass: 'RombelDetal',
                paramMethod: 'RombelDetail',
                listeners:{
                    click: 'tombolRombelDetail'
                }
            }*/]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a,
            c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {

        var grid = this;

        this.store = this.createStore();

        Ext.apply(this, this.initialConfig);

        this.lookupStoreTingkatPendidikan = new Admin.store.TingkatPendidikan();
        // this.lookupStoreJurusanSp = new Admin.store.JurusanSp();
        this.lookupStoreKurikulum = new Admin.store.Kurikulum();

        this.lookupStoreTingkatPendidikan.load();
        // this.lookupStoreJurusanSp.load();
        this.lookupStoreKurikulum.load();

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 80,
            sortable: true,
            dataIndex: 'rombongan_belajar_id',
            hideable: false,
            hidden: true
        },{
            header: 'Tingkat Pendidikan',
            width: 160,
            sortable: true,
            dataIndex: 'tingkat_pendidikan_id',
            hideable: false,
            // renderer: function(v,p,r){

            //     var record = this.lookupStoreTingkatPendidikan.getById(v);
            //     if(record) {
            //         return '<a style="color:#228ab7;text-decoration:none" href="#ProfilSpRombelDetail/'+r.data.rombongan_belajar_id+'">'+record.get('nama')+'</a>';
            //         // return record.get('nama');
            //         // if(r.data.rombongan_belajar_id){
            //         // }else{
            //         //     return v;
            //         // }
            //     } else {
            //         return v;
            //     }

            // }
            renderer: function(v,p,r) {
                var record = this.lookupStoreTingkatPendidikan.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }
            }
        },{
            header: 'Kurikulum',
            width: 200,
            sortable: true,
            dataIndex: 'kurikulum_id',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStoreKurikulum.getById(v);
                if(record) {
                    return record.get('nama_kurikulum');
                } else {
                    return v;
                }
            }
        },{
            header: 'Jurusan Sat.Pendidikan',
            width: 140,
            sortable: true,
            dataIndex: 'jurusan_sp_id',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.jurusan_sp_id_str;
            }
        },{
            header: 'Nama Rombel',
            width: 120,
            sortable: true,
            dataIndex: 'nama',
            hideable: false
        },{
            header: 'Wali/Guru Kelas',
            width: 170,
            sortable: true,
            dataIndex: 'ptk_id',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.ptk_id_str;
            }
        },{
            header: 'Prasarana',
            width: 150,
            sortable: true,
            dataIndex: 'prasarana_id',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.prasarana_id_str;
            }
        },{

            header: 'Moving Class',
            width: 100,
            sortable: true,
            dataIndex: 'moving_class',
            hideable: false,
            renderer: function(v,p,r) {
                switch (v) {
                    case 1 : return 'Ya'; break;
                    case 2 : return 'Tidak'; break;
                    default : return '-'; break;
                }
            }
        },{
            header: 'Jenis Rombel',
            width: 360,
            sortable: true,
            dataIndex: 'jenis_rombel',
            hideable: false,
            hidden: true
        }];

        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();

        this.callParent(arguments);

        // this.store.load();
    }
});