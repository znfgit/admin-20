Ext.define('Admin.view.RombonganBelajar.RingkasanRombonganBelajarController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.ringkasanrombonganbelajar',
    RingkasanRombonganBelajarGridSetup: function(grid){

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
        var panel = grid.up('ringkasanrombonganbelajar');
        var container = grid.up('ringkasanrombonganbelajar');
        var barchart = container.down('ringkasanrombonganbelajarbarchart');
        var cartesian = barchart.down('cartesian');
        var cartesianstore = cartesian.getStore();
        // console.log(container);
        // console.log(barchart);
        // console.log(cartesian);
        // console.log(cartesianstore);
        // console.log(session.tingkat);
        // console.log(grid.columns[1]); // nama
        // if (session.tingkat == "DIKMEN") {
        //     grid.columns[2].setVisible(false);
        //     grid.columns[3].setVisible(false);
        //     grid.columns[4].setVisible(false);
        //     grid.columns[5].setVisible(false);
        //     grid.columns[6].setVisible(false);
        //     grid.columns[7].setVisible(false);
        //     grid.columns[8].setVisible(false);
        //     grid.columns[9].setVisible(false);
        //     grid.columns[10].setVisible(false);
        // }

        grid.getStore().on('beforeload', function(store){

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: panel.id_level_wilayah,
                kode_wilayah: panel.kode_wilayah
            });

            cartesianstore.on('beforeload', function(store) {
                Ext.apply(store.proxy.extraParams, {
                    bentuk_pendidikan_id: bpCombo.getValue(),
                    id_level_wilayah: panel.id_level_wilayah,
                    kode_wilayah: panel.kode_wilayah
                });
            });
        });

        grid.getStore().on('load', function(store){
            // console.log(store.lastOptions.params);

            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

            var data = store.data;

            var pd_kelas_1 = 0;
            var pd_kelas_2 = 0;
            var pd_kelas_3 = 0;
            var pd_kelas_4 = 0;
            var pd_kelas_5 = 0;
            var pd_kelas_6 = 0;
            var pd_kelas_7 = 0;
            var pd_kelas_8 = 0;
            var pd_kelas_9 = 0;
            var pd_kelas_10 = 0;
            var pd_kelas_11 = 0;
            var pd_kelas_12 = 0;
            var pd_kelas_13 = 0;

            data.each(function(record){
                // console.log(record.data);

                var dataRecord = record.data;
                // console.log(dataRecord);
                pd_kelas_1 += dataRecord.pd_kelas_1;
                pd_kelas_2 += dataRecord.pd_kelas_2;
                pd_kelas_3 += dataRecord.pd_kelas_3;
                pd_kelas_4 += dataRecord.pd_kelas_4;
                pd_kelas_5 += dataRecord.pd_kelas_5;
                pd_kelas_6 += dataRecord.pd_kelas_6;
                pd_kelas_7 += dataRecord.pd_kelas_7;
                pd_kelas_8 += dataRecord.pd_kelas_8;
                pd_kelas_9 += dataRecord.pd_kelas_9;
                pd_kelas_10 += dataRecord.pd_kelas_10;
                pd_kelas_11 += dataRecord.pd_kelas_11;
                pd_kelas_12 += dataRecord.pd_kelas_12;
                pd_kelas_13 += dataRecord.pd_kelas_13;
            });

            // console.log(guru_negeri);

            var recordConfig = {
                no : '',
                nama : '<b>Total<b>',
                pd_kelas_1: pd_kelas_1,
                pd_kelas_2: pd_kelas_2,
                pd_kelas_3: pd_kelas_3,
                pd_kelas_4: pd_kelas_4,
                pd_kelas_5: pd_kelas_5,
                pd_kelas_6: pd_kelas_6,
                pd_kelas_7: pd_kelas_7,
                pd_kelas_8: pd_kelas_8,
                pd_kelas_9: pd_kelas_9,
                pd_kelas_10: pd_kelas_10,
                pd_kelas_11: pd_kelas_11,
                pd_kelas_12: pd_kelas_12,
                pd_kelas_13: pd_kelas_13
            };

            var r = new Admin.model.RingkasanRombonganBelajar(recordConfig);

            grid.store.add(r);

            cartesianstore.reload();
        });

        grid.getStore().load();
        cartesianstore.load();
    },
    bentukPendidikanComboChange: function(combo){
    	combo.up('gridpanel').getStore().reload();
    }
    // TODO - Add control logic or remove if not needed
});
