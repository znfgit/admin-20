Ext.define('Admin.view.RekapRpmj.RekapRpmj', {
    extend: 'Ext.container.Container',
    xtype: 'rekaprpmj',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    // controller: 'rekaprpmj',
    
    cls: 'rekaprpmj-container',

    layout: 'responsivecolumn',
    // layout: 'border',

    items: [
        {
            // xtype: 'rekaprpmjgrid',
            xtype: 'rekaprpmjgrid',
            dockedItems:[{
                xtype: 'toolbar',
                dock: 'top',
                items:[{
                    text: 'Muat Ulang',
                    listeners:{
                        click: function(btn){
                            btn.up('gridpanel').getStore().reload();
                        }
                    }
                }]
            }],
            title: 'Tabel Rekap RPMJ Pendidikan',
            // Always 100% of container
            responsiveCls: 'big-100',
            minHeight:500,
            listeners:{
                afterrender: function(grid){
                    grid.getStore().load();
                }
            }
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});
