Ext.define('Admin.view.RekapRpmj.RekapRpmjGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.rekaprpmjgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    // controller: 'mutasi',
    // listeners:{
    //     afterrender:'setupMutasiGrid'
    // },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('Admin.store.RekapRpmj');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        // this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'No',
            width:50,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            locked: true,
            hidden: false
        },{
            header: 'ARAHAN STRUKTUR<br>PEMBAHASAN MENURUT<br>PERMENDAGRI NO.54/2010',
            width:200,
            sortable: true,
            dataIndex: 'nama',
            locked: true,
            hideable: false,
            hidden: false
        },{
            text: '2009',
            columns:[{
                header: 'Target',
                width:100,
                sortable: true,
                dataIndex: 'target_2009',
                hideable: false,
                hidden: true,
                align: 'right'
            },{
                header: 'Realisasi',
                width:100,
                sortable: true,
                dataIndex: 'realisasi_2009',
                hideable: false,
                hidden: false,
                align: 'right'
            },{
                header: 'SPM',
                width:100,
                sortable: true,
                dataIndex: 'spm_2009',
                hideable: false,
                hidden: true,
                align: 'right'
            }]
        },{
            text: '2010',
            columns:[{
                header: 'Target',
                width:100,
                sortable: true,
                dataIndex: 'target_2010',
                hideable: false,
                hidden: true,
                align: 'right'
            },{
                header: 'Realisasi',
                width:100,
                sortable: true,
                dataIndex: 'realisasi_2010',
                hideable: false,
                hidden: false,
                align: 'right'
            },{
                header: 'SPM',
                width:100,
                sortable: true,
                dataIndex: 'spm_2010',
                hideable: false,
                hidden: true,
                align: 'right'
            }]
        },{
            text: '2011',
            columns:[{
                header: 'Target',
                width:100,
                sortable: true,
                dataIndex: 'target_2011',
                hideable: false,
                hidden: true,
                align: 'right'
            },{
                header: 'Realisasi',
                width:100,
                sortable: true,
                dataIndex: 'realisasi_2011',
                hideable: false,
                hidden: false,
                align: 'right'
            },{
                header: 'SPM',
                width:100,
                sortable: true,
                dataIndex: 'spm_2011',
                hideable: false,
                hidden: true,
                align: 'right'
            }]
        },{
            text: '2012',
            columns:[{
                header: 'Target',
                width:100,
                sortable: true,
                dataIndex: 'target_2012',
                hideable: false,
                hidden: true,
                align: 'right'
            },{
                header: 'Realisasi',
                width:100,
                sortable: true,
                dataIndex: 'realisasi_2012',
                hideable: false,
                hidden: false,
                align: 'right'
            },{
                header: 'SPM',
                width:100,
                sortable: true,
                dataIndex: 'spm_2012',
                hideable: false,
                hidden: true,
                align: 'right'
            }]
        },{
            text: '2013',
            columns:[{
                header: 'Target',
                width:100,
                sortable: true,
                dataIndex: 'target_2013',
                hideable: false,
                hidden: true,
                align: 'right'
            },{
                header: 'Realisasi',
                width:100,
                sortable: true,
                dataIndex: 'realisasi_2013',
                hideable: false,
                hidden: false,
                align: 'right'
            },{
                header: 'SPM',
                width:100,
                sortable: true,
                dataIndex: 'spm_2013',
                hideable: false,
                hidden: true,
                align: 'right'
            }]
        },{
            text: '2014',
            columns:[{
                header: 'Target',
                width:100,
                sortable: true,
                dataIndex: 'target_2014',
                hideable: false,
                hidden: true,
                align: 'right'
            },{
                header: 'Realisasi',
                width:100,
                sortable: true,
                dataIndex: 'realisasi_2014',
                hideable: false,
                hidden: false,
                align: 'right'
            },{
                header: 'SPM',
                width:100,
                sortable: true,
                dataIndex: 'spm_2014',
                hideable: false,
                hidden: true,
                align: 'right'
            }]
        },{
            header: 'Keterangan',
            width:100,
            sortable: true,
            dataIndex: 'keterangan',
            hideable: false,
            hidden: false,
            align: 'left'
        },{
            header: 'SKPD',
            width:100,
            sortable: true,
            dataIndex: 'skpd',
            hideable: false,
            hidden: false,
            align: 'left'
        }];

        // this.dockedItems = [{
        //     xtype: 'toolbar',
        //     items: [{
        //         text:'Tambah',
        //         listeners:{
        //             click:'tambahMutasi'
        //         },
        //         iconCls: 'x-fa fa-plus-circle',
        //         ui: 'soft-green'
        //     },{
        //         text:'Ubah',
        //         listeners:{
        //             click:'ubahMutasi'
        //         },
        //         iconCls: 'x-fa fa-pencil-square',
        //         ui: 'soft-green'
        //     },{
        //         text:'Hapus',
        //         listeners:{
        //             click:'hapusMutasi'
        //         },
        //         iconCls: 'x-fa fa-trash-o',
        //         ui: 'soft-green'
        //     },{
        //         text:'Cetak',
        //         listeners:{
        //             click:'cetakMutasi'
        //         },
        //         iconCls: 'x-fa fa-print',
        //         ui: 'soft-green'
        //     }]
        // }];
        
        // this.bbar = Ext.create('Ext.PagingToolbar', {
        //     store: this.store,
        //     displayInfo: true,
        //     displayMsg: 'Displaying data {0} - {1} of {2}',
        //     emptyMsg: "Tidak ada data"
        // });
        
        this.callParent(arguments);
    }
});