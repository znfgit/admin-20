Ext.define('Admin.view.PesertaDidik.PdUsiaSpGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pdusiaspgrid',
    title: '',
    selType: 'rowmodel',
    listeners:{
        afterrender:'PdUsiaSpGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.PdUsiaSp');
    },
    createDockedItems: function() {
        var hash = window.location.hash.substr(1);
        var hashArr = hash.split('/');

        // console.log(getParams);

        var id_level_wilayah = hashArr[1];
        var kode_wilayah = hashArr[2];
        
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                href:'#PdUsia',
                hrefTarget: '_self'
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'PesertaDidik',
                paramMethod: 'PdUsiaSp',
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah,
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Menampilkan data {0} - {1} dari {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        Ext.apply(this, this.initialConfig);

        
        this.columns = [{
            header: 'No',
            width: 60,
            sortable: true,
            dataIndex: 'no',
            align: 'center',
            hideable: false,
            // locked: true,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1);
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Nama SP',
            width: 220,
            sortable: true,
            dataIndex: 'nama',
			hideable: false,
            // locked: true,
            renderer: function(v,p,r){
                return '<a style="color:#228ab7;text-decoration:none" href="#ProfilSatuanPendidikan/'+r.data.sekolah_id+'">'+v+'</a>';
            }
        },{
            header: 'NPSN',
            align: 'center',
            dataIndex: 'npsn',
            width: 120,
            sortable: true,
            hideable: false
        },{
            header: 'Status',
            align: 'center',
            dataIndex: 'status',
            width: 120,
            sortable: true,
            hideable: false
        },{
            header: 'Bentuk Pendidikan',
            align: 'center',
            dataIndex: 'bentuk',
            width: 170,
            sortable: true,
            hideable: false
        },{
            text: 'Kelompok Usia Jenjang SD/Sederajat',
            columns:[
                {
                    header: '< 7',
                    align:'right',
                    dataIndex: 'pd_kurang_7',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '7',
                    align:'right',
                    dataIndex: 'pd_7',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '8',
                    align:'right',
                    dataIndex: 'pd_8',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '9',
                    align:'right',
                    dataIndex: 'pd_9',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '10',
                    align:'right',
                    dataIndex: 'pd_10',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '11',
                    align:'right',
                    dataIndex: 'pd_11',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '12',
                    align:'right',
                    dataIndex: 'pd_12',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '7 - 12',
                    align:'right',
                    dataIndex: 'pd_12',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney( (r.data.pd_7 + r.data.pd_8 + r.data.pd_9 + r.data.pd_10 + r.data.pd_11 + r.data.pd_12) );
                    }
                },{
                    header: '> 12',
                    align:'right',
                    dataIndex: 'pd_lebih_12',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        },{
            text: 'Kelompok Usia Jenjang SMP/Sederajat',
            columns:[
                {
                    header: '< 13',
                    align:'right',
                    dataIndex: 'pd_kurang_13',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '13',
                    align:'right',
                    dataIndex: 'pd_13',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '14',
                    align:'right',
                    dataIndex: 'pd_14',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '15',
                    align:'right',
                    dataIndex: 'pd_15',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '13 - 15',
                    align:'right',
                    dataIndex: 'pd_13',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney( (r.data.pd_13 + r.data.pd_14 + r.data.pd_15) );
                    }
                },{
                    header: '> 15',
                    align:'right',
                    dataIndex: 'pd_lebih_15',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        },{
            text: 'Kelompok Usia Jenjang SMA/SMK/Sederajat',
            columns:[
                {
                    header: '< 16',
                    align:'right',
                    dataIndex: 'pd_kurang_16',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '16',
                    align:'right',
                    dataIndex: 'pd_16',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '17',
                    align:'right',
                    dataIndex: 'pd_17',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '18',
                    align:'right',
                    dataIndex: 'pd_18',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '16 - 18',
                    align:'right',
                    dataIndex: 'pd_13',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney( (r.data.pd_16 + r.data.pd_17 + r.data.pd_18) );
                    }
                },{
                    header: '> 18',
                    align:'right',
                    dataIndex: 'pd_lebih_18',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        }
        ];
        
        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();
        
        this.callParent(arguments);

        // this.store.load();
    }
});