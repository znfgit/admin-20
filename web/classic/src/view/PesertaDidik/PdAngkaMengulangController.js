Ext.define('Admin.view.PesertaDidik.PdAngkaMengulangController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.pdangkamengulang',
    PdAngkaMengulangGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            
            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });

        });

        grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }                

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

            var data = store.data;

            var pd_am_laki_negeri = 0;
            var pd_am_laki_swasta = 0;
            var pd_am_perempuan_negeri = 0;
            var pd_am_perempuan_swasta = 0;

            data.each(function(record){

                var dataRecord = record.data;

                pd_am_laki_negeri = pd_am_laki_negeri+dataRecord.pd_am_laki_negeri;
                pd_am_laki_swasta = pd_am_laki_swasta+dataRecord.pd_am_laki_swasta;
                pd_am_perempuan_negeri = pd_am_perempuan_negeri+dataRecord.pd_am_perempuan_negeri;
                pd_am_perempuan_swasta = pd_am_perempuan_swasta+dataRecord.pd_am_perempuan_swasta;
            });

            var recordConfig = {
                no : '',
                nama : '<b>Total<b>',
                pd_am_laki_negeri: pd_am_laki_negeri,
                pd_am_laki_swasta: pd_am_laki_swasta,
                pd_am_perempuan_negeri: pd_am_perempuan_negeri,
                pd_am_perempuan_swasta: pd_am_perempuan_swasta,
            }

            var r = new Admin.model.PdAngkaMengulang(recordConfig);
                
            grid.store.add(r);

        });

    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        combo.up('gridpanel').getStore().reload();
        combo.up('gridpanel').up('pdangkamengulang').down('pdangkamengulangbarchart').down('cartesian').getStore().reload({
            params:{
                bentuk_pendidikan_id: combo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            }
        });
    }
});
