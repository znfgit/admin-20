Ext.define('Admin.view.PesertaDidik.PdRingkasanSp', {
    extend: 'Ext.container.Container',
    xtype: 'pdringkasansp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdringkasansp',
    
    cls: 'pdringkasansp-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [
        {
            xtype: 'pdringkasanspgrid',
            title: 'Rekap Ringkasan Peserta Didik',
            // Always 100% of container
            // responsiveCls: 'big-100'
            // height:100
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});