Ext.define('Admin.view.PesertaDidik.PdAngkaKelulusanSp', {
    extend: 'Ext.container.Container',
    xtype: 'pdangkakelulusansp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdangkakelulusansp',
    
    cls: 'pdangkakelulusansp-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [
        {
            xtype: 'pdangkakelulusanspgrid',
            title: 'Rekap Angka Kelulusan Peserta Didik',
            // Always 100% of container
            // responsiveCls: 'big-100'
            // height:100
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});