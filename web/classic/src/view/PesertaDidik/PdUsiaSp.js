Ext.define('Admin.view.PesertaDidik.PdUsiaSp', {
    extend: 'Ext.container.Container',
    xtype: 'pdusiasp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdusiasp',
    
    cls: 'pdusiasp-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [
        {
            xtype: 'pdusiaspgrid',
            title: 'Rekap Usia Peserta Didik',
            // Always 100% of container
            // responsiveCls: 'big-100'
            // height:100
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});