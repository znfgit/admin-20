Ext.define('Admin.view.PesertaDidik.PdTingkatSpController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.pdtingkatsp',
    PdTingkatSpGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var panel = grid.up('pdtingkatsp');

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: panel.id_level_wilayah,
                kode_wilayah: panel.kode_wilayah,
                oke: 'sip'
            });

        });

        grid.getStore().on('load', function(store){

            var panel = grid.up('pdtingkatsp');
            var rec = store.getAt(0);

            if(rec){
                if(panel.id_level_wilayah == 1){
                    grid.setTitle('Jumlah Peserta Didik Berdasarkan Tingkat Pendidikan di ' + rec.data.propinsi);
                }else if(panel.id_level_wilayah == 2){
                    grid.setTitle('Jumlah Peserta Didik Berdasarkan Tingkat Pendidikan di ' + rec.data.kabupaten);
                }else if(panel.id_level_wilayah == 3){
                    grid.setTitle('Jumlah Peserta Didik Berdasarkan Tingkat Pendidikan di ' + rec.data.kecamatan);
                }
            }


        });

        if(session.tingkat == 'DIKMEN'){
        
            for (var i = 8; i <= 34; i++) {
                grid.columns[i].hide();
            };
        
            // for (var i = 41; i <= 43; i++) {
            //     grid.columns[i].hide();
            // };
        
        }else if(session.tingkat == 'DIKDAS'){
        
            for (var i = 35; i <= 46; i++) {
                grid.columns[i].hide();
            };
        
            // for (var i = 41; i <= 43; i++) {
            //     grid.columns[i].hide();
            // };
        
        }

        grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
        combo.up('gridpanel').getStore().reload();
    }

    // TODO - Add control logic or remove if not needed
});
