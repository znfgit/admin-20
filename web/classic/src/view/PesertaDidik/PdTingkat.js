Ext.define('Admin.view.PesertaDidik.PdTingkat', {
    extend: 'Ext.container.Container',
    xtype: 'pdtingkat',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdtingkat',
    
    cls: 'pdtingkat-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [
        {
            xtype:'tabpanel',
            region:'center',
            items:[
                {
                    xtype: 'pdtingkatgrid',
                    title: 'Rekap Peserta Didik Berdasarkan Tingkat Pendidikan',
                    iconCls: 'fa fa-child'
                    // Always 100% of container
                    // responsiveCls: 'big-100',
                    // minHeight:200
                    // region: 'center'
                    // html: 'isinya daftar pengiriman'
                },{
                    xtype: 'pdtingkatbarchart',
                    title: 'Rekap Peserta Didik Berdasarkan Tingkat Pendidikan',
                    iconCls: 'fa fa-bar-chart'
                    // Always 100% of container
                    // responsiveCls: 'big-100',
                    // minHeight:200
                }
            ]
        }
    ]
});
