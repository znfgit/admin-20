Ext.define('Admin.view.PesertaDidik.PdAgamaController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.pdagama',
    PdAgamaGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            
            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });

        });

        grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }                

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

            
            var data = store.data;

            var pd_islam = 0;
            var pd_laki_islam = 0;
            var pd_perempuan_islam = 0;
            var pd_kristen = 0;
            var pd_laki_kristen = 0;
            var pd_perempuan_kristen = 0;
            var pd_katholik = 0;
            var pd_laki_katholik = 0;
            var pd_perempuan_katholik = 0;
            var pd_hindu = 0;
            var pd_laki_hindu = 0;
            var pd_perempuan_hindu = 0;
            var pd_budha = 0;
            var pd_laki_budha = 0;
            var pd_perempuan_budha = 0;
            var pd_konghucu = 0;
            var pd_laki_konghucu = 0;
            var pd_perempuan_konghucu = 0;
            var pd_kepercayaan = 0;
            var pd_laki_kepercayaan = 0;
            var pd_perempuan_kepercayaan = 0;
            var pd_agama_tidak_diisi = 0;
            var pd_laki_agama_tidak_diisi = 0;
            var pd_perempuan_agama_tidak_diisi = 0;
            
            data.each(function(record){
                // console.log(record.data);

                var dataRecord = record.data;
                // console.log(dataRecord);

                pd_islam = pd_islam+dataRecord.pd_islam;
                pd_laki_islam = pd_laki_islam+dataRecord.pd_laki_islam;
                pd_perempuan_islam = pd_perempuan_islam+dataRecord.pd_perempuan_islam;
                pd_kristen = pd_kristen+dataRecord.pd_kristen;
                pd_laki_kristen = pd_laki_kristen+dataRecord.pd_laki_kristen;
                pd_perempuan_kristen = pd_perempuan_kristen+dataRecord.pd_perempuan_kristen;
                pd_katholik = pd_katholik+dataRecord.pd_katholik;
                pd_laki_katholik = pd_laki_katholik+dataRecord.pd_laki_katholik;
                pd_perempuan_katholik = pd_perempuan_katholik+dataRecord.pd_perempuan_katholik;
                pd_hindu = pd_hindu+dataRecord.pd_hindu;
                pd_laki_hindu = pd_laki_hindu+dataRecord.pd_laki_hindu;
                pd_perempuan_hindu = pd_perempuan_hindu+dataRecord.pd_perempuan_hindu;
                pd_budha = pd_budha+dataRecord.pd_budha;
                pd_laki_budha = pd_laki_budha+dataRecord.pd_laki_budha;
                pd_perempuan_budha = pd_perempuan_budha+dataRecord.pd_perempuan_budha;
                pd_konghucu = pd_konghucu+dataRecord.pd_konghucu;
                pd_laki_konghucu = pd_laki_konghucu+dataRecord.pd_laki_konghucu;
                pd_perempuan_konghucu = pd_perempuan_konghucu+dataRecord.pd_perempuan_konghucu;
                pd_kepercayaan = pd_kepercayaan+dataRecord.pd_kepercayaan;
                pd_laki_kepercayaan = pd_laki_kepercayaan+dataRecord.pd_laki_kepercayaan;
                pd_perempuan_kepercayaan = pd_perempuan_kepercayaan+dataRecord.pd_perempuan_kepercayaan;
                pd_agama_tidak_diisi = pd_agama_tidak_diisi+dataRecord.pd_agama_tidak_diisi;
                pd_laki_agama_tidak_diisi = pd_laki_agama_tidak_diisi+dataRecord.pd_laki_agama_tidak_diisi;
                pd_perempuan_agama_tidak_diisi = pd_perempuan_agama_tidak_diisi+dataRecord.pd_perempuan_agama_tidak_diisi;
            });

            // console.log(guru_negeri);

            var recordConfig = {
                no : '',
                nama : '<b>Total<b>',
                pd_islam: pd_islam,
                pd_laki_islam: pd_laki_islam,
                pd_perempuan_islam: pd_perempuan_islam,
                pd_kristen: pd_kristen,
                pd_laki_kristen: pd_laki_kristen,
                pd_perempuan_kristen: pd_perempuan_kristen,
                pd_katholik: pd_katholik,
                pd_laki_katholik: pd_laki_katholik,
                pd_perempuan_katholik: pd_perempuan_katholik,
                pd_hindu: pd_hindu,
                pd_laki_hindu: pd_laki_hindu,
                pd_perempuan_hindu: pd_perempuan_hindu,
                pd_budha: pd_budha,
                pd_laki_budha: pd_laki_budha,
                pd_perempuan_budha: pd_perempuan_budha,
                pd_konghucu: pd_konghucu,
                pd_laki_konghucu: pd_laki_konghucu,
                pd_perempuan_konghucu: pd_perempuan_konghucu,
                pd_kepercayaan: pd_kepercayaan,
                pd_laki_kepercayaan: pd_laki_kepercayaan,
                pd_perempuan_kepercayaan: pd_perempuan_kepercayaan,
                pd_agama_tidak_diisi: pd_agama_tidak_diisi,
                pd_laki_agama_tidak_diisi: pd_laki_agama_tidak_diisi,
                pd_perempuan_agama_tidak_diisi: pd_perempuan_agama_tidak_diisi

            };
            
            var r = new Admin.model.PdAgama(recordConfig);
            
            grid.store.add(r);

        });

    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        combo.up('gridpanel').getStore().reload();
        combo.up('gridpanel').up('pdagama').down('pdagamabarchart').down('cartesian').getStore().reload({
            params:{
                bentuk_pendidikan_id: combo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            }
        });
    }
});
