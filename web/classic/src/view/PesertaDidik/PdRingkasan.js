Ext.define('Admin.view.PesertaDidik.PdRingkasan', {
    extend: 'Ext.container.Container',
    xtype: 'pdringkasan',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdringkasan',
    
    cls: 'pdringkasan-container',

    // layout: 'responsivecolumn',
    // scrollable: 'y',
    layout: 'border',

    items: [
        {
            xtype:'tabpanel',
            region:'center',
            items:[
                {
                    xtype: 'pdringkasangrid',
                    title: 'Ringkasan Rekap Peserta Didik',
                    iconCls: 'fa fa-child'
                    // Always 100% of container
                    // responsiveCls: 'big-100',
                    // minHeight:200
                    // region: 'center'
                    // html: 'isinya daftar pengiriman'
                },{
                    xtype: 'pdringkasanbarchart',
                    title: 'Rekap Peserta Didik Berdasarkan Status Sekolah',
                    iconCls: 'fa fa-bar-chart'
                    // responsiveCls: 'big-100',
                    // minHeight:200

                }
            ]
        }
    ]
});
