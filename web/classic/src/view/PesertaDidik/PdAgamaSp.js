Ext.define('Admin.view.PesertaDidik.PdAgamaSp', {
    extend: 'Ext.container.Container',
    xtype: 'pdagamasp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdagamasp',
    
    cls: 'pdagamasp-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [
        {
            xtype: 'pdagamaspgrid',
            title: 'Rekap Agama Peserta Didik',
            // Always 100% of container
            // responsiveCls: 'big-100'
            // height:100
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});