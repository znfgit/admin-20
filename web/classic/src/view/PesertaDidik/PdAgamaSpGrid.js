Ext.define('Admin.view.PesertaDidik.PdAgamaSpGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pdagamaspgrid',
    title: '',
    selType: 'rowmodel',
    columnLines: true,
    listeners: {
        afterrender: 'PdAgamaSpGridSetup',
        itemdblclick: function (dv, record) {
            if (record.data.id_level_wilayah != 3) {
                this.store.reload({
                    params: {
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function () {
        return Ext.create('Admin.store.PdAgamaSp');
    },
    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Menampilkan data {0} - {1} dari {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    createDockedItems: function () {

        var hash = window.location.hash.substr(1);
        var hashArr = hash.split('/');

        // console.log(getParams);

        var id_level_wilayah = hashArr[1];
        var kode_wilayah = hashArr[2];

        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                href: '#PdAgama',
                hrefTarget: '_self'
            }, {
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners: {
                    change: 'bentukPendidikanComboChange'
                }
            }, {
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah,
                paramClass: 'PesertaDidik',
                paramMethod: 'PdAgamaSp',
                listeners: {
                    click: 'tombolUnduhExcel'
                }
            }, '->', {
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners: {
                    click: 'refresh'
                }
            }]
        }];
    },
    formatMoney: function (a, c, d, t) {
        var n = a,
            c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function () {
        var grid = this;

        this.store = this.createStore();

        Ext.apply(this, this.initialConfig);

        this.columns = [{
            header: 'No',
            width: 60,
            sortable: true,
            dataIndex: 'no',
            align: 'center',
            hideable: false,
            // locked: true,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1);
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Nama SP',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            // locked: true,
            renderer: function (v, p, r) {
                return '<a style="color:#228ab7;text-decoration:none" href="#ProfilSatuanPendidikan/' + r.data.sekolah_id + '">' + v + '</a>';
            }
        },{
            header: 'NPSN',
            align: 'center',
            dataIndex: 'npsn',
            width: 90,
            sortable: true,
            hideable: false
        },{
            header: 'Status',
            align: 'center',
            dataIndex: 'status',
            width: 100,
            sortable: true,
            hideable: false
        },{
            header: 'Bentuk',
            align: 'center',
            dataIndex: 'bentuk',
            width: 100,
            sortable: true,
            hideable: false
        },{
            header: 'Agama',
            columns:[{
                header: 'Islam',
                columns:[{
                    header: 'L',
                    align:'right',
                    dataIndex: 'pd_laki_islam',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'P',
                    align:'right',
                    dataIndex: 'pd_perempuan_islam',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'pd_islam',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return "<b>"+grid.formatMoney(v)+"</b>";
                    }
                }]
            },{
                header: 'Kristen',
                columns:[{
                    header: 'L',
                    align:'right',
                    dataIndex: 'pd_laki_kristen',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'P',
                    align:'right',
                    dataIndex: 'pd_perempuan_kristen',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'pd_kristen',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return "<b>"+grid.formatMoney(v)+"</b>";
                    }
                }]
            },{
                header: 'Katholik',
                columns:[{
                    header: 'L',
                    align:'right',
                    dataIndex: 'pd_laki_katholik',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'P',
                    align:'right',
                    dataIndex: 'pd_perempuan_katholik',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'pd_katholik',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return "<b>"+grid.formatMoney(v)+"</b>";
                    }
                }]
            },{
                header: 'Hindu',
                columns:[{
                    header: 'L',
                    align:'right',
                    dataIndex: 'pd_laki_hindu',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'P',
                    align:'right',
                    dataIndex: 'pd_perempuan_hindu',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'pd_hindu',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return "<b>"+grid.formatMoney(v)+"</b>";
                    }
                }]
            },{
                header: 'Budha',
                columns:[{
                    header: 'L',
                    align:'right',
                    dataIndex: 'pd_laki_budha',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'P',
                    align:'right',
                    dataIndex: 'pd_perempuan_budha',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'pd_budha',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return "<b>"+grid.formatMoney(v)+"</b>";
                    }
                }]
            },{
                header: 'Konghucu',
                columns:[{
                    header: 'L',
                    align:'right',
                    dataIndex: 'pd_laki_konghucu',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'P',
                    align:'right',
                    dataIndex: 'pd_perempuan_konghucu',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'pd_konghucu',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return "<b>"+grid.formatMoney(v)+"</b>";
                    }
                }]
            },{
                header: 'Kepercayaan',
                columns:[{
                    header: 'L',
                    align:'right',
                    dataIndex: 'pd_laki_kepercayaan',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'P',
                    align:'right',
                    dataIndex: 'pd_perempuan_kepercayaan',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'pd_kepercayaan',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return "<b>"+grid.formatMoney(v)+"</b>";
                    }
                }]
            },{
                header: 'Lainnya',
                columns:[{
                    header: 'L',
                    align:'right',
                    dataIndex: 'pd_laki_agama_tidak_diisi',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'P',
                    align:'right',
                    dataIndex: 'pd_perempuan_agama_tidak_diisi',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'pd_agama_tidak_diisi',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return "<b>"+grid.formatMoney(v)+"</b>";
                    }
                }]
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'swasta',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return "<b>"+grid.formatMoney(r.data.pd_islam + r.data.pd_kristen + r.data.pd_katholik+ r.data.pd_hindu+ r.data.pd_budha+ r.data.pd_konghucu+ r.data.pd_kepercayaan+ r.data.pd_agama_tidak_diisi)+"</b>";
                }
            }]
        },{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }];

        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();

        this.callParent(arguments);

        // this.store.load();
    }
});