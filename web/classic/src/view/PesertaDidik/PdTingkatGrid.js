Ext.define('Admin.view.PesertaDidik.PdTingkatGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pdtingkatgrid',
    title: '',
    selType: 'rowmodel',
    columnLines: true,
    listeners:{
        afterrender:'PdTingkatGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.PdTingkat');
    },
    createDockedItems: function() {

        var pilihStatusStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'nama'],
            data : [
                {"id":"1", "nama":"Negeri"},
                {"id":"2", "nama":"Swasta"},
                {"id":"999", "nama":"Semua"}
            ]
        });

        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                listeners:{
                    click: 'kembali'
                }
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype:'combobox',
                name: 'pilihstatuscombo',
                emptyText:'Pilih Status Sekolah...',
                width: 200,
                itemId: 'pilihstatuscombo',
                store: pilihStatusStore,
                queryMode: 'local',
                displayField: 'nama',
                valueField: 'id',
                listeners:{
                    change: function(combo){
                        combo.up('gridpanel').getStore().load();
                    }
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'PesertaDidik',
                paramMethod: 'PdTingkat',
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Menampilkan data {0} - {1} dari {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        Ext.apply(this, this.initialConfig);

        
        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            locked: false,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1); 
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Wilayah',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
			hideable: false,
            locked: false,
            renderer: function(v,p,r){

                if(r.data.id_level_wilayah){
                    return '<a style="color:#228ab7;text-decoration:none" href="#PdTingkatSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah+'">'+v+'</a>';
                }else{
                    return v;
                }

            }
        },{
            text: 'Kelas 1',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_1_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_1_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_1_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_1_laki+r.data.pd_kelas_1_perempuan);
                }
            }]
        },{
            text: 'Kelas 2',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_2_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_2_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_2_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_2_laki+r.data.pd_kelas_2_perempuan);
                }
            }]
        },{
            text: 'Kelas 3',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_3_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_3_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_3_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_3_laki+r.data.pd_kelas_3_perempuan);
                }
            }]
        },{
            text: 'Kelas 4',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_4_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_4_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_4_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_4_laki+r.data.pd_kelas_4_perempuan);
                }
            }]
        },{
            text: 'Kelas 5',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_5_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_5_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_5_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_5_laki+r.data.pd_kelas_5_perempuan);
                }
            }]
        },{
            text: 'Kelas 6',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_6_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_6_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_6_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_6_laki+r.data.pd_kelas_6_perempuan);
                }
            }]
        },{
            text: 'Kelas 7',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_7_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_7_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_7_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_7_laki+r.data.pd_kelas_7_perempuan);
                }
            }]
        },{
            text: 'Kelas 8',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_8_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_8_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_8_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_8_laki+r.data.pd_kelas_8_perempuan);
                }
            }]
        },{
            text: 'Kelas 9',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_9_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_9_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_9_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_9_laki+r.data.pd_kelas_9_perempuan);
                }
            }]
        },{
            text: 'Kelas 10',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_10_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_10_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_10_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_10_laki+r.data.pd_kelas_10_perempuan);
                }
            }]
        },{
            text: 'Kelas 11',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_11_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_11_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_11_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_11_laki+r.data.pd_kelas_11_perempuan);
                }
            }]
        },{
            text: 'Kelas 12',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_12_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_12_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_12_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_12_laki+r.data.pd_kelas_12_perempuan);
                }
            }]
        },{
            text: 'Kelas 13',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_kelas_13_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_kelas_13_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_kelas_13_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_kelas_13_laki+r.data.pd_kelas_13_perempuan);
                }
            }]
        },{
            text: 'Total',
            columns:[{
                header: 'L',
                align:'right',
                dataIndex: 'pd_total_laki',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_total_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_total_perempuan',
                width: 75,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(r.data.pd_total_laki+r.data.pd_total_perempuan);
                }
            }]
        }
        ];
        
        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();
        
        this.callParent(arguments);

        // this.store.load();
    }
});