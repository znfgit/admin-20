Ext.define('Admin.view.PesertaDidik.PdAngkaMengulangSp', {
    extend: 'Ext.container.Container',
    xtype: 'pdangkamengulangsp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdangkamengulangsp',
    
    cls: 'pdangkamengulangsp-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [
        {
            xtype: 'pdangkamengulangspgrid',
            title: 'Rekap Angka Mengulang Peserta Didik',
            // Always 100% of container
            // responsiveCls: 'big-100'
            // height:100
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});