Ext.define('Admin.view.PesertaDidik.PdAngkaKelulusanController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.pdangkakelulusan',
    PdAngkaKelulusanGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });

        });

        grid.getStore().on('load', function(store){

            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

            var data = store.data;

            var pd_kelulusan_laki_2014 = 0;
            var pd_kelulusan_perempuan_2014 = 0;
            var pd_kelulusan_laki_2015 = 0;
            var pd_kelulusan_perempuan_2015 = 0;
            var pd_kelulusan_laki_2016 = 0;
            var pd_kelulusan_perempuan_2016 = 0;

            data.each(function(record){

                var dataRecord = record.data;

                pd_kelulusan_laki_2014 = pd_kelulusan_laki_2014+dataRecord.pd_kelulusan_laki_2014;
                pd_kelulusan_laki_2015 = pd_kelulusan_laki_2015+dataRecord.pd_kelulusan_laki_2015;
                pd_kelulusan_laki_2016 = pd_kelulusan_laki_2016+dataRecord.pd_kelulusan_laki_2016;
                pd_kelulusan_perempuan_2014 = pd_kelulusan_perempuan_2014+dataRecord.pd_kelulusan_perempuan_2014;
                pd_kelulusan_perempuan_2015 = pd_kelulusan_perempuan_2015+dataRecord.pd_kelulusan_perempuan_2015;
                pd_kelulusan_perempuan_2016 = pd_kelulusan_perempuan_2016+dataRecord.pd_kelulusan_perempuan_2016;
            });

            var recordConfig = {
                no : '',
                nama : '<b>Total<b>',
                pd_kelulusan_laki_2014: pd_kelulusan_laki_2014,
                pd_kelulusan_laki_2015: pd_kelulusan_laki_2015,
                pd_kelulusan_laki_2016: pd_kelulusan_laki_2016,
                pd_kelulusan_perempuan_2014: pd_kelulusan_perempuan_2014,
                pd_kelulusan_perempuan_2015: pd_kelulusan_perempuan_2015,
                pd_kelulusan_perempuan_2016: pd_kelulusan_perempuan_2016,
            }

            var r = new Admin.model.PdAngkaKelulusan(recordConfig);
                
            grid.store.add(r);
        });

    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        combo.up('gridpanel').getStore().reload();
        combo.up('gridpanel').up('pdangkakelulusan').down('pdangkakelulusanbarchart').down('cartesian').getStore().reload({
            params:{
                bentuk_pendidikan_id: combo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            }
        });
    }
});
