Ext.define('Admin.view.PesertaDidik.PdUsiaBarChart', {
    extend: 'Ext.Panel',
    ui: 'yellow',
    xtype: 'pdusiabarchart', 
    // width: 650,
    initComponent: function() {
        var me = this;

        this.myDataStore = Ext.create('Admin.store.PdUsia');

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        var titleV =  [ '< 7', '7', '8', '9', '10', '11', '12', '> 12' ];
        var yFieldV = [ 'pd_kurang_7', 'pd_7', 'pd_8', 'pd_9', 'pd_10', 'pd_11', 'pd_12', 'pd_lebih_12' ];

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            items:[{
                text: 'Simpan',
                iconCls: 'fa fa-save',
                ui: 'soft-blue',
                listeners:{
                    click: function(btn){
                        Ext.MessageBox.confirm('Konfirmasi Unduh', 'Apakah Anda ingin mengunduh grafik ini sebagai gambar?', function (choice) {
                            if (choice == 'yes') {
                                var chart = btn.up('toolbar').up('panel').down('cartesian');

                                // console.log(chart);

                                if (Ext.os.is.Desktop) {
                                    chart.download({
                                        filename: 'Chart Rekap PD Berdasarkan Usia'
                                    });
                                } else {
                                    chart.preview();
                                }
                              }
                        });
                    }
                }
            }]
        }];

        me.items = [{
            xtype: 'cartesian',
            width: '100%',
            height: 480,
            legend: {
                docked: 'bottom'
            },
            interactions: ['itemhighlight'],
            store: this.myDataStore,
            insetPadding: {
                top: 40,
                left: 40,
                right: 40,
                bottom: 40
            },
            axes: [{
                type: 'numeric',
                position: 'left',
                adjustByMajorUnit: true,
                grid: true,
                fields: yFieldV,
                minimum: 0
            }, {
                type: 'category',
                position: 'bottom',
                grid: true,
                fields: ['nama'],
                label: {
                    rotate: {
                        degrees: -90
                    }
                }
            }],
            series: [{
                type: 'bar',
                axis: 'left',
                title: titleV,
                xField: 'nama',
                yField: yFieldV,
                stacked: true,
                style: {
                    opacity: 0.80
                },
                highlight: {
                    fillStyle: 'yellow'
                },
                tooltip: {
                    style: 'background: #fff',
                    renderer: function(storeItem, item) {
                        var browser = item.series.getTitle()[Ext.Array.indexOf(item.series.getYField(), item.field)];
                        this.setHtml(browser + ' di wilayah ' + storeItem.get('nama') + ': ' + storeItem.get(item.field));
                    }
                }
            }]
        }];

        this.myDataStore.load();

        this.callParent();
    }
});