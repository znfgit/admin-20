Ext.define('Admin.view.PesertaDidik.PdUsiaGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pdusiagrid',
    title: '',
    selType: 'rowmodel',
    columnLines: true,
    listeners:{
        afterrender:'PdUsiaGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.PdUsia');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                listeners:{
                    click: 'kembali'
                }
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'PesertaDidik',
                paramMethod: 'PdUsia',
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        Ext.apply(this, this.initialConfig);

        
        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            locked: false,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1); 
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Wilayah',
            width: 220,
            sortable: true,
            dataIndex: 'nama',
            locked: false,
			hideable: false,
            renderer: function(v,p,r){

                if(r.data.id_level_wilayah){
                    return '<a style="color:#228ab7;text-decoration:none" href="#PdUsiaSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah+'">'+v+'</a>';
                }else{
                    return v;
                }

            }
        }
        ,{
            text: 'Kelompok Usia Jenjang SD/Sederajat',
            columns:[
                {
                    header: '< 7',
                    align:'right',
                    dataIndex: 'pd_kurang_7',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '7',
                    align:'right',
                    dataIndex: 'pd_7',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '8',
                    align:'right',
                    dataIndex: 'pd_8',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '9',
                    align:'right',
                    dataIndex: 'pd_9',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '10',
                    align:'right',
                    dataIndex: 'pd_10',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '11',
                    align:'right',
                    dataIndex: 'pd_11',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '12',
                    align:'right',
                    dataIndex: 'pd_12',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '7 - 12',
                    align:'right',
                    dataIndex: 'pd_12',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney( (r.data.pd_7 + r.data.pd_8 + r.data.pd_9 + r.data.pd_10 + r.data.pd_11 + r.data.pd_12) );
                    }
                },{
                    header: '> 12',
                    align:'right',
                    dataIndex: 'pd_lebih_12',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        },{
            text: 'Kelompok Usia Jenjang SMP/Sederajat',
            columns:[
                {
                    header: '< 13',
                    align:'right',
                    dataIndex: 'pd_kurang_13',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '13',
                    align:'right',
                    dataIndex: 'pd_13',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '14',
                    align:'right',
                    dataIndex: 'pd_14',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '15',
                    align:'right',
                    dataIndex: 'pd_15',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '13 - 15',
                    align:'right',
                    dataIndex: 'pd_13',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney( (r.data.pd_13 + r.data.pd_14 + r.data.pd_15) );
                    }
                },{
                    header: '> 15',
                    align:'right',
                    dataIndex: 'pd_lebih_15',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        },{
            text: 'Kelompok Usia Jenjang SMA/SMK/Sederajat',
            columns:[
                {
                    header: '< 16',
                    align:'right',
                    dataIndex: 'pd_kurang_16',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '16',
                    align:'right',
                    dataIndex: 'pd_16',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '17',
                    align:'right',
                    dataIndex: 'pd_17',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '18',
                    align:'right',
                    dataIndex: 'pd_18',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: '16 - 18',
                    align:'right',
                    dataIndex: 'pd_13',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney( (r.data.pd_16 + r.data.pd_17 + r.data.pd_18) );
                    }
                },{
                    header: '> 18',
                    align:'right',
                    dataIndex: 'pd_lebih_18',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        },{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }
        ];
        
        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();
        
        this.callParent(arguments);

        // this.store.load();
    }
});