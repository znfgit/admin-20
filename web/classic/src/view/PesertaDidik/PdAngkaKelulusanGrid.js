Ext.define('Admin.view.PesertaDidik.PdAngkaKelulusanGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pdangkakelulusangrid',
    title: '',
    selType: 'rowmodel',
    columnLines: true,
    listeners:{
        afterrender:'PdAngkaKelulusanGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.PdAngkaKelulusan');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                listeners:{
                    click: 'kembali'
                }
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'PesertaDidik',
                paramMethod: 'PdAngkaKelulusan',
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a,
            c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {

        var grid = this;

        this.store = this.createStore();

        Ext.apply(this, this.initialConfig);


        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            locked: false,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1);
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Wilayah',
            width: 220,
            sortable: true,
            dataIndex: 'nama',
			hideable: false,
            locked: false,
            renderer: function(v,p,r){

                if(r.data.id_level_wilayah){
                    return '<a style="color:#228ab7;text-decoration:none" href="#PdAngkaKelulusanSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah+'">'+v+'</a>';
                }else{
                    return v;
                }

            }
        }
        ,{
            text: '2014/2015',
            columns:[
                {
                    header: 'L',
                    align:'right',
                    dataIndex: 'pd_kelulusan_laki_2014',
                    width: 80,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'P',
                    align:'right',
                    dataIndex: 'pd_kelulusan_perempuan_2014',
                    width: 80,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    width: 80,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney( (r.data.pd_kelulusan_laki_2014 + r.data.pd_kelulusan_perempuan_2014) );
                    }
                }
            ]
        }
        ,{
            text: '2015/2016',
            columns:[
                {
                    header: 'L',
                    align:'right',
                    dataIndex: 'pd_kelulusan_laki_2015',
                    width: 80,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'P',
                    align:'right',
                    dataIndex: 'pd_kelulusan_perempuan_2015',
                    width: 80,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    width: 80,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney( (r.data.pd_kelulusan_laki_2015 + r.data.pd_kelulusan_perempuan_2015) );
                    }
                }
            ]
        }
        ,{
            text: '2016/2017',
            columns:[
                {
                    header: 'L',
                    align:'right',
                    dataIndex: 'pd_kelulusan_laki_2016',
                    width: 80,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'P',
                    align:'right',
                    dataIndex: 'pd_kelulusan_perempuan_2016',
                    width: 80,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    width: 80,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney( (r.data.pd_kelulusan_laki_2016 + r.data.pd_kelulusan_perempuan_2016) );
                    }
                }
            ]
        },{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }/*,{
            text: 'Total',
            columns:[
                {
                    header: 'L',
                    align:'right',
                    dataIndex: 'pd_kelulusan_laki_total',
                    width: 80,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(
                            (r.data.pd_kelulusan_laki_negeri + r.data.pd_kelulusan_laki_swasta)
                        );
                    }
                },{
                    header: 'P',
                    align:'right',
                    dataIndex: 'pd_kelulusan_perempuan_total',
                    width: 80,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(
                            (r.data.pd_kelulusan_perempuan_negeri + r.data.pd_kelulusan_perempuan_swasta)
                        );
                    }
                },{
                    header: 'L + P',
                    align:'right',
                    dataIndex: 'pd_kelulusan_laki_total',
                    width: 80,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(
                            (r.data.pd_kelulusan_laki_negeri + r.data.pd_kelulusan_laki_swasta) +
                            (r.data.pd_kelulusan_perempuan_negeri + r.data.pd_kelulusan_perempuan_swasta)
                        );
                    }
                }
            ]
        }*/
        ];

        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();

        this.callParent(arguments);

        // this.store.load();
    }
});