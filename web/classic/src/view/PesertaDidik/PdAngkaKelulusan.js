Ext.define('Admin.view.PesertaDidik.PdAngkaKelulusan', {
    extend: 'Ext.container.Container',
    xtype: 'pdangkakelulusan',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdangkakelulusan',

    cls: 'pdangkakelulusan-container',

    // layout: 'responsivecolumn',
    // scrollable: 'y',
    layout: 'border',

    items: [
        {
            xtype:'tabpanel',
            region:'center',
            items:[{
                xtype: 'pdangkakelulusangrid',
                title: 'Rekap Angka Kelulusan Peserta Didik',
                // Always 100% of container
                // responsiveCls: 'big-100',
                // minHeight:200
                iconCls: 'fa fa-child'
                // region: 'center'
                // html: 'isinya daftar pengiriman'
            },{
                xtype: 'pdangkakelulusanbarchart',
                title: 'Rekap Angka Kelulusan Peserta Didik',
                // Always 100% of container
                // responsiveCls: 'big-100',
                // minHeight:200
                iconCls: 'fa fa-bar-chart'
                // region: 'center'
                // html: 'isinya daftar pengiriman'
            }]
        }
    ]
});
