Ext.define('Admin.view.PesertaDidik.PesertaDidikGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pesertadidikgrid',
    title: '',
    selType: 'rowmodel',
    controller: 'pesertadidikgrid',
    listeners:{
        afterrender:'PesertaDidikGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.PesertaDidik');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: []
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        Ext.apply(this, this.initialConfig);

        this.lookupStoreAgama = new Admin.store.Agama();
        this.lookupStoreJenisTinggal = new Admin.store.JenisTinggal();
        this.lookupStoreAlatTransportasi = new Admin.store.AlatTransportasi();
        // this.lookupStorePekerjaan = new Admin.store.Pekerjaan();
        this.lookupStorePenghasilanOrangtuaWali = new Admin.store.PenghasilanOrangtuaWali();
        // this.lookupStorePenghasilanOrangtuaWali = new Admin.store.PenghasilanOrangtuaWali();
        // this.lookupStorePekerjaan = new Admin.store.Pekerjaan();
        this.lookupStorePekerjaan = new Admin.store.Pekerjaan();
        // this.lookupStorePenghasilanOrangtuaWali = new Admin.store.PenghasilanOrangtuaWali();
        
        this.lookupStoreAgama.load();
        this.lookupStoreJenisTinggal.load();
        this.lookupStoreAlatTransportasi.load();
        this.lookupStorePekerjaan.load();
        this.lookupStorePenghasilanOrangtuaWali.load();
        // this.lookupStorePenghasilanOrangtuaWali.load();
        // this.lookupStorePekerjaan.load();
        // this.lookupStorePekerjaan.load();
        // this.lookupStorePenghasilanOrangtuaWali.load();

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 80,
            sortable: true,
            dataIndex: 'peserta_didik_id',
            hideable: false,
            hidden: true
        },{
            header: 'Nama',
            tooltip: 'Nama',
            width: 240,
            sortable: true,
            dataIndex: 'nama',
            hideable: false
        },{
            header: 'Jenis Kelamin',
            tooltip: 'Jenis Kelamin',
            width: 80,
            sortable: true,
            dataIndex: 'jenis_kelamin',
            hideable: false
        },{
            header: 'Nisn',
            tooltip: 'Nisn',
            width: 80,
            sortable: true,
            dataIndex: 'nisn',
            hideable: false
        },{
            header: 'Nik',
            tooltip: 'Nik',
            width: 144,
            sortable: true,
            dataIndex: 'nik',
            hideable: false
        },{
            header: 'Tempat Lahir',
            tooltip: 'Tempat Lahir',
            width: 128,
            sortable: true,
            dataIndex: 'tempat_lahir',
            hideable: false
        },{
            header: 'Tanggal Lahir',
            tooltip: 'Tanggal Lahir',
            width: 80,
            sortable: true,
            dataIndex: 'tanggal_lahir',
            hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'Agama',
            tooltip: 'Agama',
            width: 80,
            sortable: true,
            dataIndex: 'agama_id',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStoreAgama.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Alamat Jalan',
            tooltip: 'Alamat Jalan',
            width: 320,
            sortable: true,
            dataIndex: 'alamat_jalan',
            hideable: false
        },{
            header: 'RT',
            tooltip: 'RT',
            width: 50,
            sortable: true,
            dataIndex: 'rt',
            hideable: false
        },{
            header: 'RW',
            tooltip: 'RW',
            width: 50,
            sortable: true,
            dataIndex: 'rw',
            hideable: false
        },{
            header: 'Nama Dusun',
            tooltip: 'Nama Dusun',
            width: 200,
            sortable: true,
            dataIndex: 'nama_dusun',
            hideable: false
        },{
            header: 'Desa Kelurahan',
            tooltip: 'Desa Kelurahan',
            width: 200,
            sortable: true,
            dataIndex: 'desa_kelurahan',
            hideable: false
        },{
            header: 'Kode Pos',
            tooltip: 'Kode Pos',
            width: 80,
            sortable: true,
            dataIndex: 'kode_pos',
            hideable: false
        },{
            header: 'Jenis Tinggal',
            tooltip: 'Jenis Tinggal',
            width: 50,
            sortable: true,
            dataIndex: 'jenis_tinggal_id',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStoreJenisTinggal.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Alat Transportasi',
            tooltip: 'Alat Transportasi',
            width: 50,
            sortable: true,
            dataIndex: 'alat_transportasi_id',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStoreAlatTransportasi.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Nomor Telepon Rumah',
            tooltip: 'Nomor Telepon Rumah',
            width: 80,
            sortable: true,
            dataIndex: 'nomor_telepon_rumah',
            hideable: false
        },{
            header: 'Nomor Telepon Seluler',
            tooltip: 'Nomor Telepon Seluler',
            width: 80,
            sortable: true,
            dataIndex: 'nomor_telepon_seluler',
            hideable: false
        },{
            header: 'Email',
            tooltip: 'Email',
            width: 200,
            sortable: true,
            dataIndex: 'email',
            hideable: false
        },{
            header: 'Penerima Kps',
            tooltip: 'Penerima Kps',
            width: 262144,
            sortable: true,
            dataIndex: 'penerima_kps',
            hideable: false
        },{
            header: 'No KPS',
            tooltip: 'No Kps',
            width: 160,
            sortable: true,
            dataIndex: 'no_kps',
            hideable: false
        },{
            header: 'Nama Ayah',
            tooltip: 'Nama Ayah',
            width: 240,
            sortable: true,
            dataIndex: 'nama_ayah',
            hideable: false
        },{
            header: 'Tahun Lahir Ayah',
            tooltip: 'Tahun Lahir Ayah',
            width: 1048576,
            sortable: true,
            dataIndex: 'tahun_lahir_ayah',
            hideable: false
        },{
            header: 'Jenjang Pendidikan Ayah',
            tooltip: 'Jenjang Pendidikan Ayah',
            width: 50,
            sortable: true,
            dataIndex: 'jenjang_pendidikan_ayah',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.jenjang_pendidikan_id_str;
            }
        },{
            header: 'Pekerjaan Ayah',
            tooltip: 'Pekerjaan Ayah',
            width: 80,
            sortable: true,
            dataIndex: 'pekerjaan_id_ayah',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStorePekerjaan.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Penghasilan Ayah',
            tooltip: 'Penghasilan Ayah',
            width: 80,
            sortable: true,
            dataIndex: 'penghasilan_id_ayah',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStorePenghasilanOrangtuaWali.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Kebutuhan Khusus Ayah',
            tooltip: 'Kebutuhan Khusus Ayah',
            width: 80,
            sortable: true,
            dataIndex: 'kebutuhan_khusus_id_ayah',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.kebutuhan_khusus_id_str;
            }
        },{
            header: 'Nama Ibu Kandung',
            tooltip: 'Nama Ibu Kandung',
            width: 240,
            sortable: true,
            dataIndex: 'nama_ibu_kandung',
            hideable: false
        },{
            header: 'Tahun Lahir Ibu',
            tooltip: 'Tahun Lahir Ibu',
            width: 1048576,
            sortable: true,
            dataIndex: 'tahun_lahir_ibu',
            hideable: false
        },{
            header: 'Jenjang Pendidikan Ibu',
            tooltip: 'Jenjang Pendidikan Ibu',
            width: 50,
            sortable: true,
            dataIndex: 'jenjang_pendidikan_ibu',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.jenjang_pendidikan_id_str;
            }
        },{
            header: 'Penghasilan Ibu',
            tooltip: 'Penghasilan Ibu',
            width: 80,
            sortable: true,
            dataIndex: 'penghasilan_id_ibu',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStorePenghasilanOrangtuaWali.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Pekerjaan Ibu',
            tooltip: 'Pekerjaan Ibu',
            width: 80,
            sortable: true,
            dataIndex: 'pekerjaan_id_ibu',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStorePekerjaan.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Kebutuhan Khusus Ibu',
            tooltip: 'Kebutuhan Khusus Ibu',
            width: 80,
            sortable: true,
            dataIndex: 'kebutuhan_khusus_id_ibu',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.kebutuhan_khusus_id_str;
            }
        },{
            header: 'Nama Wali',
            tooltip: 'Nama Wali',
            width: 120,
            sortable: true,
            dataIndex: 'nama_wali',
            hideable: false
        },{
            header: 'Tahun Lahir Wali',
            tooltip: 'Tahun Lahir Wali',
            width: 1048576,
            sortable: true,
            dataIndex: 'tahun_lahir_wali',
            hideable: false
        },{
            header: 'Jenjang Pendidikan Wali',
            tooltip: 'Jenjang Pendidikan Wali',
            width: 50,
            sortable: true,
            dataIndex: 'jenjang_pendidikan_wali',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.jenjang_pendidikan_id_str;
            }
        },{
            header: 'Pekerjaan Wali',
            tooltip: 'Pekerjaan Wali',
            width: 80,
            sortable: true,
            dataIndex: 'pekerjaan_id_wali',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStorePekerjaan.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Penghasilan Wali',
            tooltip: 'Penghasilan Wali',
            width: 80,
            sortable: true,
            dataIndex: 'penghasilan_id_wali',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStorePenghasilanOrangtuaWali.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Kewarganegaraan',
            tooltip: 'Kewarganegaraan',
            width: 80,
            sortable: true,
            dataIndex: 'kewarganegaraan',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.negara_id_str;
            }
        }];
        
        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();
        
        this.callParent(arguments);

        // this.store.load();
    }
});