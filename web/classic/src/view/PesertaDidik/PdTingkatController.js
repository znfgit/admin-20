Ext.define('Admin.view.PesertaDidik.PdTingkatController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.pdtingkat',
    PdTingkatGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));



        if(session.tingkat == 'DIKMEN'){
        
            for (var i = 2; i <= 28; i++) {
                grid.columns[i].hide();
            };
        
            // for (var i = 41; i <= 43; i++) {
            //     grid.columns[i].hide();
            // };
        
        }else if(session.tingkat == 'DIKDAS'){
        
            for (var i = 29; i <= 40; i++) {
                grid.columns[i].hide();
            };
        
            // for (var i = 41; i <= 43; i++) {
            //     grid.columns[i].hide();
            // };
        
        }
        
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var statusCombo = grid.down('toolbar').down('combobox[itemId=pilihstatuscombo]');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah,
                status_sekolah: statusCombo.getValue()
            });


        });

        grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            // console.log(rec);
            if(rec){
                if(session.id_level_wilayah == '0'){

                    if(rec.data.id_level_wilayah == 1){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }

                }else if(session.id_level_wilayah == '1'){

                    if(rec.data.id_level_wilayah == 2){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }                

                }else if(session.id_level_wilayah == '2'){

                    if(rec.data.id_level_wilayah == 3){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }

                }
            }

            var data = store.data;

            var pd_kelas_1_laki = 0;
            var pd_kelas_2_laki = 0;
            var pd_kelas_3_laki = 0;
            var pd_kelas_4_laki = 0;
            var pd_kelas_5_laki = 0;
            var pd_kelas_6_laki = 0;
            var pd_kelas_7_laki = 0;
            var pd_kelas_8_laki = 0;
            var pd_kelas_9_laki = 0;
            var pd_kelas_10_laki = 0;
            var pd_kelas_11_laki = 0;
            var pd_kelas_12_laki = 0;
            var pd_kelas_13_laki = 0;

            var pd_kelas_1_perempuan = 0;
            var pd_kelas_2_perempuan = 0;
            var pd_kelas_3_perempuan = 0;
            var pd_kelas_4_perempuan = 0;
            var pd_kelas_5_perempuan = 0;
            var pd_kelas_6_perempuan = 0;
            var pd_kelas_7_perempuan = 0;
            var pd_kelas_8_perempuan = 0;
            var pd_kelas_9_perempuan = 0;
            var pd_kelas_10_perempuan = 0;
            var pd_kelas_11_perempuan = 0;
            var pd_kelas_12_perempuan = 0;
            var pd_kelas_13_perempuan = 0;

            var pd_kelas_1_total = 0;
            var pd_kelas_2_total = 0;
            var pd_kelas_3_total = 0;
            var pd_kelas_4_total = 0;
            var pd_kelas_5_total = 0;
            var pd_kelas_6_total = 0;
            var pd_kelas_7_total = 0;
            var pd_kelas_8_total = 0;
            var pd_kelas_9_total = 0;
            var pd_kelas_10_total = 0;
            var pd_kelas_11_total = 0;
            var pd_kelas_12_total = 0;
            var pd_kelas_13_total = 0;
            
            data.each(function(record){
                // console.log(record.data);

                var dataRecord = record.data;
                // console.log(dataRecord);

                pd_kelas_1_laki = pd_kelas_1_laki+dataRecord.pd_kelas_1_laki;
                pd_kelas_2_laki = pd_kelas_2_laki+dataRecord.pd_kelas_2_laki;                
                pd_kelas_3_laki = pd_kelas_3_laki+dataRecord.pd_kelas_3_laki;
                pd_kelas_4_laki = pd_kelas_4_laki+dataRecord.pd_kelas_4_laki;
                pd_kelas_5_laki = pd_kelas_5_laki+dataRecord.pd_kelas_5_laki;
                pd_kelas_6_laki = pd_kelas_6_laki+dataRecord.pd_kelas_6_laki;
                pd_kelas_7_laki = pd_kelas_7_laki+dataRecord.pd_kelas_7_laki;
                pd_kelas_8_laki = pd_kelas_8_laki+dataRecord.pd_kelas_8_laki;
                pd_kelas_9_laki = pd_kelas_9_laki+dataRecord.pd_kelas_9_laki;
                pd_kelas_10_laki = pd_kelas_10_laki+dataRecord.pd_kelas_10_laki;
                pd_kelas_11_laki = pd_kelas_11_laki+dataRecord.pd_kelas_11_laki;
                pd_kelas_12_laki = pd_kelas_12_laki+dataRecord.pd_kelas_12_laki;
                pd_kelas_13_laki = pd_kelas_13_laki+dataRecord.pd_kelas_13_laki;

                pd_kelas_1_perempuan = pd_kelas_1_perempuan+dataRecord.pd_kelas_1_perempuan;
                pd_kelas_2_perempuan = pd_kelas_2_perempuan+dataRecord.pd_kelas_2_perempuan;                
                pd_kelas_3_perempuan = pd_kelas_3_perempuan+dataRecord.pd_kelas_3_perempuan;
                pd_kelas_4_perempuan = pd_kelas_4_perempuan+dataRecord.pd_kelas_4_perempuan;
                pd_kelas_5_perempuan = pd_kelas_5_perempuan+dataRecord.pd_kelas_5_perempuan;
                pd_kelas_6_perempuan = pd_kelas_6_perempuan+dataRecord.pd_kelas_6_perempuan;
                pd_kelas_7_perempuan = pd_kelas_7_perempuan+dataRecord.pd_kelas_7_perempuan;
                pd_kelas_8_perempuan = pd_kelas_8_perempuan+dataRecord.pd_kelas_8_perempuan;
                pd_kelas_9_perempuan = pd_kelas_9_perempuan+dataRecord.pd_kelas_9_perempuan;
                pd_kelas_10_perempuan = pd_kelas_10_perempuan+dataRecord.pd_kelas_10_perempuan;
                pd_kelas_11_perempuan = pd_kelas_11_perempuan+dataRecord.pd_kelas_11_perempuan;
                pd_kelas_12_perempuan = pd_kelas_12_perempuan+dataRecord.pd_kelas_12_perempuan;
                pd_kelas_13_perempuan = pd_kelas_13_perempuan+dataRecord.pd_kelas_13_perempuan;

                pd_kelas_1_total = pd_kelas_1_total+dataRecord.pd_kelas_1_total;
                pd_kelas_2_total = pd_kelas_2_total+dataRecord.pd_kelas_2_total;
                pd_kelas_3_total = pd_kelas_3_total+dataRecord.pd_kelas_3_total;
                pd_kelas_4_total = pd_kelas_4_total+dataRecord.pd_kelas_4_total;
                pd_kelas_5_total = pd_kelas_5_total+dataRecord.pd_kelas_5_total;
                pd_kelas_6_total = pd_kelas_6_total+dataRecord.pd_kelas_6_total;
                pd_kelas_7_total = pd_kelas_7_total+dataRecord.pd_kelas_7_total;
                pd_kelas_8_total = pd_kelas_8_total+dataRecord.pd_kelas_8_total;
                pd_kelas_9_total = pd_kelas_9_total+dataRecord.pd_kelas_9_total;
                pd_kelas_10_total = pd_kelas_10_total+dataRecord.pd_kelas_10_total;
                pd_kelas_11_total = pd_kelas_11_total+dataRecord.pd_kelas_11_total;
                pd_kelas_12_total = pd_kelas_12_total+dataRecord.pd_kelas_12_total;
                pd_kelas_13_total = pd_kelas_13_total+dataRecord.pd_kelas_13_total;            
            });

            // console.log(guru_negeri);

            var recordConfig = {
                no : '',
                nama : '<b>Total<b>',
                pd_kelas_1_laki: pd_kelas_1_laki,
                pd_kelas_2_laki: pd_kelas_2_laki,
                pd_kelas_3_laki: pd_kelas_3_laki,
                pd_kelas_4_laki: pd_kelas_4_laki,
                pd_kelas_5_laki: pd_kelas_5_laki,
                pd_kelas_6_laki: pd_kelas_6_laki,
                pd_kelas_7_laki: pd_kelas_7_laki,
                pd_kelas_8_laki: pd_kelas_8_laki,
                pd_kelas_9_laki: pd_kelas_9_laki,
                pd_kelas_10_laki: pd_kelas_10_laki,
                pd_kelas_11_laki: pd_kelas_11_laki,
                pd_kelas_12_laki: pd_kelas_12_laki,
                pd_kelas_13_laki: pd_kelas_13_laki,
                
                pd_kelas_1_perempuan: pd_kelas_1_perempuan,
                pd_kelas_2_perempuan: pd_kelas_2_perempuan,
                pd_kelas_3_perempuan: pd_kelas_3_perempuan,
                pd_kelas_4_perempuan: pd_kelas_4_perempuan,
                pd_kelas_5_perempuan: pd_kelas_5_perempuan,
                pd_kelas_6_perempuan: pd_kelas_6_perempuan,
                pd_kelas_7_perempuan: pd_kelas_7_perempuan,
                pd_kelas_8_perempuan: pd_kelas_8_perempuan,
                pd_kelas_9_perempuan: pd_kelas_9_perempuan,
                pd_kelas_10_perempuan: pd_kelas_10_perempuan,
                pd_kelas_11_perempuan: pd_kelas_11_perempuan,
                pd_kelas_12_perempuan: pd_kelas_12_perempuan,
                pd_kelas_13_perempuan: pd_kelas_13_perempuan,

                pd_kelas_1_total: pd_kelas_1_total,
                pd_kelas_2_total: pd_kelas_2_total,
                pd_kelas_3_total: pd_kelas_3_total,
                pd_kelas_4_total: pd_kelas_4_total,
                pd_kelas_5_total: pd_kelas_5_total,
                pd_kelas_6_total: pd_kelas_6_total,
                pd_kelas_7_total: pd_kelas_7_total,
                pd_kelas_8_total: pd_kelas_8_total,
                pd_kelas_9_total: pd_kelas_9_total,
                pd_kelas_10_total: pd_kelas_10_total,
                pd_kelas_11_total: pd_kelas_11_total,
                pd_kelas_12_total: pd_kelas_12_total,
                pd_kelas_13_total: pd_kelas_13_total           
            };
            
            var r = new Admin.model.PdTingkat(recordConfig);
            
            grid.store.add(r);

        });

        grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        combo.up('gridpanel').getStore().reload();
        combo.up('gridpanel').up('pdtingkat').down('pdtingkatbarchart').down('cartesian').getStore().reload({
            params:{
                bentuk_pendidikan_id: combo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            }
        });

    },
    PdTingkatSpGridSetup: function(grid){
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var panel = grid.up('container');

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: panel.id_level_wilayah,
                kode_wilayah: panel.kode_wilayah,
                oke: 'sip'
            });


        });

        grid.getStore().on('load', function(store){

            var panel = grid.up('container');
            var rec = store.getAt(0);

            if(rec){
                if(panel.id_level_wilayah == 1){
                    grid.setTitle('Peserta Didik Berdasarkan Tingkat Kelas per Satuan Pendidikan di ' + rec.data.propinsi);
                }else if(panel.id_level_wilayah == 2){
                    grid.setTitle('Peserta Didik Berdasarkan Tingkat Kelas per Satuan Pendidikan di ' + rec.data.kabupaten);
                }else if(panel.id_level_wilayah == 3){
                    grid.setTitle('Peserta Didik Berdasarkan Tingkat Kelas per Satuan Pendidikan di ' + rec.data.kecamatan);
                }
            }


        });

        grid.getStore().load();
    }

    // TODO - Add control logic or remove if not needed
});
