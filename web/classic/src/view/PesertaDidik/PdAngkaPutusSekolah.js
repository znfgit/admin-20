Ext.define('Admin.view.PesertaDidik.PdAngkaPutusSekolah', {
    extend: 'Ext.container.Container',
    xtype: 'pdangkaputussekolah',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdangkaputussekolah',
    
    cls: 'pdangkaputussekolah-container',

    // layout: 'responsivecolumn',
    // scrollable: 'y',
    layout: 'border',

    items: [
        {
            xtype:'tabpanel',
            region:'center',
            items:[{
                xtype: 'pdangkaputussekolahgrid',
                title: 'Rekap Angka Putus Sekolah Peserta Didik',
                iconCls: 'fa fa-child'
                // Always 100% of container
                // responsiveCls: 'big-100',
                // minHeight:200
                // region: 'center'
                // html: 'isinya daftar pengiriman'
            },{
                xtype: 'pdangkaputussekolahbarchart',
                title: 'Rekap Angka Putus Sekolah Peserta Didik',
                iconCls: 'fa fa-bar-chart'
                // Always 100% of container
                // responsiveCls: 'big-100',
                // minHeight:200
                // region: 'center'
                // html: 'isinya daftar pengiriman'
            }]
        }   
    ]
});
