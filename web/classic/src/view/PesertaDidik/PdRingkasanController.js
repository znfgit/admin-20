Ext.define('Admin.view.PesertaDidik.PdRingkasanController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.pdringkasan',
    PdRingkasanGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });


        });

        if(session.tingkat == 'DIKMEN'){
            for (var i = 5; i <= 13; i++) {
                grid.columns[i].hide();
            };

            grid.columns[18].hide();
            grid.columns[19].hide();
        }else if(session.tingkat == 'DIKDAS'){
            for (var i = 14; i <= 17; i++) {
                grid.columns[i].hide();
            };

            grid.columns[18].hide();
            grid.columns[20].hide();
        }

        grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }                

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

            var data = store.data;

            var pd_negeri = 0;
            var pd_swasta = 0;
            
            var pd_kelas_1 = 0;
            var pd_kelas_2 = 0;
            var pd_kelas_3 = 0;
            var pd_kelas_4 = 0;
            var pd_kelas_5 = 0;
            var pd_kelas_6 = 0;
            var pd_kelas_7 = 0;
            var pd_kelas_8 = 0;
            var pd_kelas_9 = 0;

            var pd_kelas_10 = 0;
            var pd_kelas_11 = 0;
            var pd_kelas_12 = 0;
            var pd_kelas_13 = 0;
            
            data.each(function(record){
                // console.log(record.data);

                var dataRecord = record.data;
                // console.log(dataRecord);

                pd_kelas_1 = pd_kelas_1+dataRecord.pd_kelas_1;
                pd_kelas_2 = pd_kelas_2+dataRecord.pd_kelas_2;
                pd_kelas_3 = pd_kelas_3+dataRecord.pd_kelas_3;
                pd_kelas_4 = pd_kelas_4+dataRecord.pd_kelas_4;
                pd_kelas_5 = pd_kelas_5+dataRecord.pd_kelas_5;
                pd_kelas_6 = pd_kelas_6+dataRecord.pd_kelas_6;
                pd_kelas_7 = pd_kelas_7+dataRecord.pd_kelas_7;
                pd_kelas_8 = pd_kelas_8+dataRecord.pd_kelas_8;
                pd_kelas_9 = pd_kelas_9+dataRecord.pd_kelas_9;

                pd_negeri = pd_negeri+dataRecord.pd_negeri;
                pd_swasta = pd_swasta+dataRecord.pd_swasta;
                pd_kelas_10 = pd_kelas_10+dataRecord.pd_kelas_10;
                pd_kelas_11 = pd_kelas_11+dataRecord.pd_kelas_11;
                pd_kelas_12 = pd_kelas_12+dataRecord.pd_kelas_12;
                pd_kelas_13 = pd_kelas_13+dataRecord.pd_kelas_13;
                
            });

            // console.log(guru_negeri);

            var recordConfig = {
                no : '',
                nama : '<b>Total<b>',
                pd_negeri: pd_negeri,
                pd_swasta: pd_swasta,
                pd_kelas_10: pd_kelas_10,
                pd_kelas_11: pd_kelas_11,
                pd_kelas_12: pd_kelas_12,
                pd_kelas_13: pd_kelas_13,
                pd_kelas_1: pd_kelas_1,
                pd_kelas_2: pd_kelas_2,
                pd_kelas_3: pd_kelas_3,
                pd_kelas_4: pd_kelas_4,
                pd_kelas_5: pd_kelas_5,
                pd_kelas_6: pd_kelas_6,
                pd_kelas_7: pd_kelas_7,
                pd_kelas_8: pd_kelas_8,
                pd_kelas_9: pd_kelas_9,
                
            };
            
            var r = new Admin.model.PdRingkasan(recordConfig);
            
            grid.store.add(r);


        });

    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
    	combo.up('gridpanel').getStore().reload();
    }

    // TODO - Add control logic or remove if not needed
});
