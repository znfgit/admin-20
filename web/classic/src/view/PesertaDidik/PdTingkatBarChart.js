Ext.define('Admin.view.PesertaDidik.PdTingkatBarChart', {
    extend: 'Ext.Panel',
    ui: 'yellow',
    xtype: 'pdtingkatbarchart', 
    // width: 650,
    initComponent: function() {
        var me = this;

        this.myDataStore = Ext.create('Admin.store.PdTingkatBarChart');

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        if(session){

            if(session.tingkat ==  'DIKDAS'){

                var titleV =  [ 'Kelas 1', 'Kelas 2', 'Kelas 3', 'Kelas 4', 'Kelas 5', 'Kelas 6', 'Kelas 7', 'Kelas 8', 'Kelas 9' ];
                var yFieldV = [ 'pd_kelas_1', 'pd_kelas_2', 'pd_kelas_3', 'pd_kelas_4', 'pd_kelas_5', 'pd_kelas_6', 'pd_kelas_7', 'pd_kelas_8', 'pd_kelas_9' ];

            }else if (session.tingkat == 'DIKMEN'){

                var titleV =  [ 'Kelas 10', 'Kelas 11', 'Kelas 12', 'Kelas 13' ];
                var yFieldV = [ 'pd_kelas_10', 'pd_kelas_11', 'pd_kelas_12', 'pd_kelas_13' ];            

            }else{
                
                var titleV =  [ 'Kelas 1', 'Kelas 2', 'Kelas 3', 'Kelas 4', 'Kelas 5', 'Kelas 6', 'Kelas 7', 'Kelas 8', 'Kelas 9', 'Kelas 10', 'Kelas 11', 'Kelas 12', 'Kelas 13' ];
                var yFieldV = [ 'pd_kelas_1', 'pd_kelas_2', 'pd_kelas_3', 'pd_kelas_4', 'pd_kelas_5', 'pd_kelas_6', 'pd_kelas_7', 'pd_kelas_8', 'pd_kelas_9', 'pd_kelas_10', 'pd_kelas_11', 'pd_kelas_12', 'pd_kelas_13' ];

            }
        
        }else{
            Ext.Ajax.request({
                url: 'session?caller=barchart',
                timeout: 300000000,
                success: function(response){
                    var text = response.responseText;

                    var session = Ext.JSON.decode(response.responseText);

                    if(session.tingkat ==  'DIKDAS'){

                        var titleV =  [ 'Kelas 1', 'Kelas 2', 'Kelas 3', 'Kelas 4', 'Kelas 5', 'Kelas 6', 'Kelas 7', 'Kelas 8', 'Kelas 9' ];
                        var yFieldV = [ 'pd_kelas_1', 'pd_kelas_2', 'pd_kelas_3', 'pd_kelas_4', 'pd_kelas_5', 'pd_kelas_6', 'pd_kelas_7', 'pd_kelas_8', 'pd_kelas_9' ];

                    }else if (session.tingkat == 'DIKMEN'){

                        var titleV =  [ 'Kelas 10', 'Kelas 11', 'Kelas 12', 'Kelas 13' ];
                        var yFieldV = [ 'pd_kelas_10', 'pd_kelas_11', 'pd_kelas_12', 'pd_kelas_13' ];            

                    }else{
                        
                        var titleV =  [ 'Kelas 1', 'Kelas 2', 'Kelas 3', 'Kelas 4', 'Kelas 5', 'Kelas 6', 'Kelas 7', 'Kelas 8', 'Kelas 9', 'Kelas 10', 'Kelas 11', 'Kelas 12', 'Kelas 13' ];
                        var yFieldV = [ 'pd_kelas_1', 'pd_kelas_2', 'pd_kelas_3', 'pd_kelas_4', 'pd_kelas_5', 'pd_kelas_6', 'pd_kelas_7', 'pd_kelas_8', 'pd_kelas_9', 'pd_kelas_10', 'pd_kelas_11', 'pd_kelas_12', 'pd_kelas_13' ];

                    }
                }
            });
        }


        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            items:[{
                text: 'Simpan',
                iconCls: 'fa fa-save',
                ui: 'soft-blue',
                listeners:{
                    click: function(btn){
                        Ext.MessageBox.confirm('Konfirmasi Unduh', 'Apakah Anda ingin mengunduh grafik ini sebagai gambar?', function (choice) {
                            if (choice == 'yes') {
                                var chart = btn.up('toolbar').up('panel').down('cartesian');

                                // console.log(chart);

                                if (Ext.os.is.Desktop) {
                                    chart.download({
                                        filename: 'Chart Rekap PD Berdasarkan Tingkat'
                                    });
                                } else {
                                    chart.preview();
                                }
                              }
                        });
                    }
                }
            }]
        }];

        me.items = [{
            xtype: 'cartesian',
            width: '100%',
            height: 500,
            flipXY: true,
            legend: {
                docked: 'bottom'
            },
            interactions: ['itemhighlight'],
            store: this.myDataStore,
            insetPadding: {
                top: 40,
                left: 40,
                right: 40,
                bottom: 40
            },
            axes: [{
                type: 'numeric',
                position: 'bottom',
                adjustByMajorUnit: true,
                grid: true,
                fields: yFieldV,
                minimum: 0,
            }, {
                type: 'category',
                position: 'left',
                grid: true,
                fields: ['nama']
            }],
            series: [{
                type: 'bar',
                axis: 'bottom',
                title: titleV,
                xField: 'nama',
                yField: yFieldV,
                stacked: true,
                style: {
                    opacity: 0.80
                },
                highlight: {
                    fillStyle: 'yellow'
                },
                label:{
                    display: 'insideStart',
                    // color: '#ffffff',
                    orientation : 'horizontal',
                    font: '10px Helvetica',
                    field: yFieldV,
                    renderer: function(text, sprite, config, rendererData, index){
                        return text;
                    }
                },
                tooltip: {
                    style: 'background: #fff',
                    renderer: function(storeItem, item) {
                        var browser = item.series.getTitle()[Ext.Array.indexOf(item.series.getYField(), item.field)];
                        this.setHtml(browser + ' di wilayah ' + storeItem.get('nama') + ': ' + storeItem.get(item.field));
                    }
                }
            }]
        }];

        this.myDataStore.load();

        this.callParent();
    }
});