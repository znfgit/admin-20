Ext.define('Admin.view.PesertaDidik.PdAgama', {
    extend: 'Ext.container.Container',
    xtype: 'pdagama',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdagama',
    
    cls: 'pdagama-container',

    // layout: 'responsivecolumn',
    // scrollable: 'y',
    layout: 'border',

    items: [
        {
            xtype:'tabpanel',
            region:'center',
            items:[{
                xtype: 'pdagamagrid',
                title: 'Rekap Peserta Didik Berdasarkan Agama',
                // Always 100% of container
                // responsiveCls: 'big-100',
                iconCls: 'fa fa-child'
                // minHeight:200
                // region: 'center'
                // html: 'isinya daftar pengiriman'
            },{
                xtype: 'pdagamabarchart',
                title: 'Rekap Peserta Didik Berdasarkan Agama',
                // Always 100% of container
                // responsiveCls: 'big-100',
                iconCls: 'fa fa-bar-chart'
                // minHeight:200
            }]
        }
    ]
});
