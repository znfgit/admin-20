Ext.define('Admin.view.PesertaDidik.PdRingkasanSpController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.pdringkasansp',
    PdRingkasanSpGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var panel = grid.up('pdringkasansp');

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: panel.id_level_wilayah,
                kode_wilayah: panel.kode_wilayah,
                oke: 'sip'
            });

        });

        grid.getStore().on('load', function(store){

            var panel = grid.up('pdringkasansp');
            var rec = store.getAt(0);

            if(rec){
                if(panel.id_level_wilayah == 1){
                    grid.setTitle('Jumlah Ringkasan Peserta Didik di ' + rec.data.propinsi);
                }else if(panel.id_level_wilayah == 2){
                    grid.setTitle('Jumlah Ringkasan Peserta Didik di ' + rec.data.kabupaten);
                }else if(panel.id_level_wilayah == 3){
                    grid.setTitle('Jumlah Ringkasan Peserta Didik di ' + rec.data.kecamatan);
                }
            }


        });

        grid.getStore().load();

        if(session.tingkat == 'DIKMEN'){
        
            for (var i = 6; i <= 14; i++) {
                grid.columns[i].hide();
            };
        
            
        }else if(session.tingkat == 'DIKDAS'){
        
            for (var i = 15; i <= 18; i++) {
                grid.columns[i].hide();
            };
        
            
        }
    },
    bentukPendidikanComboChange: function(combo){
        combo.up('gridpanel').getStore().reload();
    }

    // TODO - Add control logic or remove if not needed
});
