Ext.define('Admin.view.PesertaDidik.PdUsiaController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.pdusia',
    PdUsiaGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        
        if(session.tingkat == 'DIKMEN'){

            // grid.up('tabpanel').down('pdusiabarchart').hide();
            // grid.up('tabpanel').down('pdusiabarchart2').hide();
            grid.up('tabpanel').add({
                xtype: 'pdusiabarchart3',
                title: 'Rekap Usia Tingkat SMA/SMK',
                iconCls: 'fa fa-bar-chart'
                // Always 100% of container
                // responsiveCls: 'big-100',
                // minHeight:200
                // region: 'center'
            });
            
        }else if(session.tingkat == 'DIKDAS'){
            grid.up('tabpanel').add({
                    xtype: 'pdusiabarchart',
                    title: 'Rekap Usia Tingkat SD',
                    iconCls: 'fa fa-bar-chart'
                    // Always 100% of container
                    // responsiveCls: 'big-50',
                    // minHeight:200
                    // region: 'center'
                },{
                    xtype: 'pdusiabarchart2',
                    title: 'Rekap Usia Tingkat SMP',
                    iconCls: 'fa fa-bar-chart'
                    // Always 100% of container
                    // responsiveCls: 'big-50',
                    // minHeight:200
                    // region: 'center'
                });            
            // grid.up('tabpanel').down('pdusiabarchart3').hide();

        }

        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });

        });

        grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }                

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

            var data = store.data;

            var pd_kurang_7 = 0;
            var pd_7 = 0;
            var pd_8 = 0;
            var pd_9 = 0;
            var pd_10 = 0;
            var pd_11 = 0;
            var pd_12 = 0;
            var pd_lebih_12 = 0;
            var pd_kurang_13 = 0;
            var pd_13 = 0;
            var pd_14 = 0;
            var pd_15 = 0;
            var pd_lebih_15 = 0;
            var pd_kurang_16 = 0;
            var pd_16 = 0;
            var pd_17 = 0;
            var pd_18 = 0;
            var pd_lebih_18 = 0;
            
            data.each(function(record){
                // console.log(record.data);

                var dataRecord = record.data;
                // console.log(dataRecord);

                pd_kurang_7 = pd_kurang_7+dataRecord.pd_kurang_7;
                pd_7 = pd_7+dataRecord.pd_7;
                pd_8 = pd_8+dataRecord.pd_8;
                pd_9 = pd_9+dataRecord.pd_9;
                pd_10 = pd_10+dataRecord.pd_10;
                pd_11 = pd_11+dataRecord.pd_11;
                pd_12 = pd_12+dataRecord.pd_12;
                pd_lebih_12 = pd_lebih_12+dataRecord.pd_lebih_12;
                pd_kurang_13 = pd_kurang_13+dataRecord.pd_kurang_13;
                pd_13 = pd_13+dataRecord.pd_13;
                pd_14 = pd_14+dataRecord.pd_14;
                pd_15 = pd_15+dataRecord.pd_15;
                pd_lebih_15 = pd_lebih_15+dataRecord.pd_lebih_15;
                pd_kurang_16 = pd_kurang_16+dataRecord.pd_kurang_16;
                pd_16 = pd_16+dataRecord.pd_16;
                pd_17 = pd_17+dataRecord.pd_17;
                pd_18 = pd_18+dataRecord.pd_18;
                pd_lebih_18 = pd_lebih_18+dataRecord.pd_lebih_18;
            });

            // console.log(guru_negeri);

            var recordConfig = {
                no : '',
                nama : '<b>Total<b>',
                pd_kurang_7: pd_kurang_7,
                pd_7: pd_7,
                pd_8: pd_8,
                pd_9: pd_9,
                pd_10: pd_10,
                pd_11: pd_11,
                pd_12: pd_12,
                pd_lebih_12: pd_lebih_12,
                pd_kurang_13: pd_kurang_13,
                pd_13: pd_13,
                pd_14: pd_14,
                pd_15: pd_15,
                pd_lebih_15: pd_lebih_15,
                pd_kurang_16: pd_kurang_16,
                pd_16: pd_16,
                pd_17: pd_17,
                pd_18: pd_18,
                pd_lebih_18: pd_lebih_18,

            };
            
            var r = new Admin.model.PdAgama(recordConfig);
            
            grid.store.add(r);

        });

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        if(session.tingkat == 'DIKMEN'){
            for (var i = 2; i <= 16; i++) {
                grid.columns[i].hide();
            }

            // grid.up('tabpanel').down('pdusiabarchart').destroy();
            // grid.up('tabpanel').down('pdusiabarchart2').destroy();

        }else if(session.tingkat == 'DIKDAS'){
            for (var j = 17; j <= 22; j++) {
                grid.columns[j].hide();
            }

            // grid.up('tabpanel').down('pdusiabarchart3').destroy();
        }

        grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        combo.up('gridpanel').getStore().reload();

        if(session.tingkat == 'DIKMEN'){

            combo.up('gridpanel').up('pdusia').down('pdusiabarchart3').down('cartesian').getStore().reload({
                params:{
                    bentuk_pendidikan_id: combo.getValue(),
                    id_level_wilayah: session.id_level_wilayah,
                    kode_wilayah: session.kode_wilayah
                }
            });

        }else if(session.tingkat == 'DIKDAS'){

            combo.up('gridpanel').up('pdusia').down('pdusiabarchart').down('cartesian').getStore().reload({
                params:{
                    bentuk_pendidikan_id: combo.getValue(),
                    id_level_wilayah: session.id_level_wilayah,
                    kode_wilayah: session.kode_wilayah
                }
            });

            combo.up('gridpanel').up('pdusia').down('pdusiabarchart2').down('cartesian').getStore().reload({
                params:{
                    bentuk_pendidikan_id: combo.getValue(),
                    id_level_wilayah: session.id_level_wilayah,
                    kode_wilayah: session.kode_wilayah
                }
            });

        }
    }

    // TODO - Add control logic or remove if not needed
});
