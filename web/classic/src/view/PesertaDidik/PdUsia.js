Ext.define('Admin.view.PesertaDidik.PdUsia', {
    extend: 'Ext.container.Container',
    xtype: 'pdusia',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdusia',
    
    cls: 'pdusia-container',

    // layout: 'responsivecolumn',
    // scrollable: 'y',
    layout: 'border',

    items: [
        {
            xtype:'tabpanel',
            region:'center',
            items:[
                {
                    xtype: 'pdusiagrid',
                    title: 'Rekap Peserta Didik Berdasarkan Usia',
                    iconCls: 'fa fa-child'
                    // Always 100% of container
                    // responsiveCls: 'big-100',
                    // minHeight:200
                    // html: 'isinya daftar pengiriman'
                }
            ]
        }
    ]
});
