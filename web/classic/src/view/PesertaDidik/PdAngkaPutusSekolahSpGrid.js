Ext.define('Admin.view.Ptk.PdAngkaPutusSekolahSpGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pdangkaputussekolahspgrid',
    title: '',
    selType: 'rowmodel',
    columnLines: true,
    listeners: {
        afterrender: 'PdAngkaPutusSekolahSpGridSetup',
        itemdblclick: function (dv, record) {
            if (record.data.id_level_wilayah != 3) {
                this.store.reload({
                    params: {
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function () {
        return Ext.create('Admin.store.PdAngkaPutusSekolahSp');
    },
    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Menampilkan data {0} - {1} dari {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    createDockedItems: function () {

        var hash = window.location.hash.substr(1);
        var hashArr = hash.split('/');

        // console.log(getParams);

        var id_level_wilayah = hashArr[1];
        var kode_wilayah = hashArr[2];

        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                href: '#PdAngkaPutusSekolah',
                hrefTarget: '_self'
            }, {
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners: {
                    change: 'bentukPendidikanComboChange'
                }
            }, {
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'PesertaDidik',
                paramMethod: 'PdAngkaPutusSekolahSp',
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah,
                listeners: {
                    click: 'tombolUnduhExcel'
                }
            }, '->', {
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners: {
                    click: 'refresh'
                }
            }]
        }];
    },
    formatMoney: function (a, c, d, t) {
        var n = a,
            c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function () {
        var grid = this;

        this.store = this.createStore();

        Ext.apply(this, this.initialConfig);

        this.columns = [{
            header: 'No',
            width: 60,
            sortable: true,
            dataIndex: 'no',
            align: 'center',
            hideable: false,
            // locked: true,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1);
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Nama',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            // locked: true,
            renderer: function (v, p, r) {
                return '<a style="color:#228ab7;text-decoration:none" href="#ProfilSatuanPendidikan/' + r.data.sekolah_id + '">' + v + '</a>';
            }
        },{
            header: 'NPSN',
            align: 'center',
            dataIndex: 'npsn',
            width: 90,
            sortable: true,
            hideable: false
        },{
            header: 'Status',
            align: 'center',
            dataIndex: 'status',
            width: 90,
            sortable: true,
            hideable: false
        },{
            header: 'Bentuk',
            align: 'center',
            dataIndex: 'bentuk',
            width: 90,
            sortable: true,
            hideable: false
        },{
            header: 'Angka Putus Sekolah',
            columns: [{
                header: 'L',
                align:'right',
                dataIndex: 'pd_ap_laki',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'P',
                align:'right',
                dataIndex: 'pd_ap_perempuan',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'pd_ap_laki',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney((r.data.pd_ap_laki + r.data.pd_ap_perempuan));
                }
            }]
        },{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }];

        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();

        this.callParent(arguments);

        // this.store.load();
    }
});