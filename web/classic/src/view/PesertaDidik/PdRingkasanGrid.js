Ext.define('Admin.view.PesertaDidik.PdRingkasanGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pdringkasangrid',
    title: '',
    selType: 'rowmodel',
    columnLines: true,
    listeners:{
        afterrender:'PdRingkasanGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.PdRingkasan');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                listeners:{
                    click: 'kembali'
                }
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'PesertaDidik',
                paramMethod: 'PdRingkasan',
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        Ext.apply(this, this.initialConfig);

        
        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            dataIndex: 'no',
            locked: false,
            hideable: false,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1); 
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Wilayah',
            width: 220,
            sortable: false,
            dataIndex: 'nama',
            locked: false,
			hideable: false,
            renderer: function(v,p,r){

                if(r.data.id_level_wilayah){
                    return '<a style="color:#228ab7;text-decoration:none" href="#PdRingkasanSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah+'">'+v+'</a>';
                }else{
                    return v;
                }

            }
        }
        ,{
            text: 'Status Sekolah',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'pd_negeri',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'pd_swasta',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'swasta',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(r.data.pd_negeri + r.data.pd_swasta);
                    }
                }
            ]
        }
        ,{
            text: 'Tingkat Pendidikan',
            columns:[
                {
                    header: 'Kelas 1',
                    align:'right',
                    dataIndex: 'pd_kelas_1',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Kelas 2',
                    align:'right',
                    dataIndex: 'pd_kelas_2',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Kelas 3',
                    align:'right',
                    dataIndex: 'pd_kelas_3',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Kelas 4',
                    align:'right',
                    dataIndex: 'pd_kelas_4',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Kelas 5',
                    align:'right',
                    dataIndex: 'pd_kelas_5',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Kelas 6',
                    align:'right',
                    dataIndex: 'pd_kelas_6',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Kelas 7',
                    align:'right',
                    dataIndex: 'pd_kelas_7',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Kelas 8',
                    align:'right',
                    dataIndex: 'pd_kelas_8',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Kelas 9',
                    align:'right',
                    dataIndex: 'pd_kelas_9',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Kelas 10',
                    align:'right',
                    dataIndex: 'pd_kelas_10',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Kelas 11',
                    align:'right',
                    dataIndex: 'pd_kelas_11',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Kelas 12',
                    align:'right',
                    dataIndex: 'pd_kelas_12',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Kelas 13',
                    align:'right',
                    dataIndex: 'pd_kelas_13',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total Dikdasmen',
                    align:'right',
                    dataIndex: 'dikdasmen',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(
                            r.data.pd_kelas_1 + 
                            r.data.pd_kelas_2 + 
                            r.data.pd_kelas_3 + 
                            r.data.pd_kelas_4 + 
                            r.data.pd_kelas_5 + 
                            r.data.pd_kelas_6 + 
                            r.data.pd_kelas_7 + 
                            r.data.pd_kelas_8 + 
                            r.data.pd_kelas_9 + 
                            r.data.pd_kelas_10 + 
                            r.data.pd_kelas_11 + 
                            r.data.pd_kelas_12 + 
                            r.data.pd_kelas_13);
                    }
                },{
                    header: 'Total Dikdas',
                    align:'right',
                    dataIndex: 'dikdas',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(
                            r.data.pd_kelas_1 + 
                            r.data.pd_kelas_2 + 
                            r.data.pd_kelas_3 + 
                            r.data.pd_kelas_4 + 
                            r.data.pd_kelas_5 + 
                            r.data.pd_kelas_6 + 
                            r.data.pd_kelas_7 + 
                            r.data.pd_kelas_8 + 
                            r.data.pd_kelas_9);
                    }
                },{
                    header: 'Total Dikmen',
                    align:'right',
                    dataIndex: 'dikmen',
                    width: 120,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(
                            r.data.pd_kelas_10 + 
                            r.data.pd_kelas_11 + 
                            r.data.pd_kelas_12 + 
                            r.data.pd_kelas_13);
                    }
                }
            ]
        },{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }
        ];
        
        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();
        
        this.callParent(arguments);

        // this.store.load();
    }
});