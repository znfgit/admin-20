Ext.define('Admin.view.PesertaDidik.PdUsiaSpController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.pdusiasp',
    PdUsiaSpGridSetup: function(grid){
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var panel = grid.up('pdusiasp');

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: panel.id_level_wilayah,
                kode_wilayah: panel.kode_wilayah,
                oke: 'sip'
            });

        });

        grid.getStore().on('load', function(store){

            var panel = grid.up('pdusiasp');
            var rec = store.getAt(0);

            if(rec){
                if(panel.id_level_wilayah == 1){
                    grid.setTitle('Jumlah Usia di ' + rec.data.propinsi);
                }else if(panel.id_level_wilayah == 2){
                    grid.setTitle('Jumlah Usia di ' + rec.data.kabupaten);
                }else if(panel.id_level_wilayah == 3){
                    grid.setTitle('Jumlah Usia di ' + rec.data.kecamatan);
                }
            }


        });

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        /*if(session.tingkat == 'DIKMEN'){
            for (var i = 2; i <= 16; i++) {
                grid.columns[i].hide();
            }
        }else if(session.tingkat == 'DIKDAS'){
            for (var j = 17; j <= 22; j++) {
                grid.columns[j].hide();
            }
        }*/


        grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
        combo.up('gridpanel').getStore().reload();
    }

    // TODO - Add control logic or remove if not needed
});
