Ext.define('Admin.view.PesertaDidik.PdTingkatSp', {
    extend: 'Ext.container.Container',
    xtype: 'pdtingkatsp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdtingkatsp',

    cls: 'pdtingkatsp-container',

    // layout: 'responsivecolumn',
    layout: 'border',

    // scrollable: 'y',

    items: [
        {
            xtype: 'pdtingkatspgrid',
            title: 'Rekap Tingkat Peserta Didik',
            // Always 100% of container
            // responsiveCls: 'big-100',
            // minHeight:200
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});