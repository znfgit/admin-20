Ext.define('Admin.view.PesertaDidik.PdAngkaPutusSekolahSp', {
    extend: 'Ext.container.Container',
    xtype: 'pdangkaputussekolahsp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pdangkaputussekolahsp',
    
    cls: 'pdangkaputussekolahsp-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [
        {
            xtype: 'pdangkaputussekolahspgrid',
            title: 'Rekap Angka Putus Sekolah Peserta Didik',
            // Always 100% of container
            // responsiveCls: 'big-100'
            // height:100
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});