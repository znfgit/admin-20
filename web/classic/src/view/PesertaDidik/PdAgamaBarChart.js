Ext.define('Admin.view.PesertaDidik.PdAgamaBarChart', {
    extend: 'Ext.Panel',
    ui: 'yellow',
    xtype: 'pdagamabarchart', 
    // width: 650,
    initComponent: function() {
        var me = this;

        this.myDataStore = Ext.create('Admin.store.PdAgama');

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        
        var titleV =  [ 'islam','kristen','katholik','hindu','budha','konghucu','kepercayaan','lainnya' ];
        var yFieldV = [ 'pd_islam','pd_kristen','pd_katholik','pd_hindu','pd_budha','pd_konghucu','pd_kepercayaan','pd_agama_tidak_diisi' ];

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            items:[{
                text: 'Simpan',
                iconCls: 'fa fa-save',
                ui: 'soft-blue',
                listeners:{
                    click: function(btn){
                        Ext.MessageBox.confirm('Konfirmasi Unduh', 'Apakah Anda ingin mengunduh grafik ini sebagai gambar?', function (choice) {
                            if (choice == 'yes') {
                                var chart = btn.up('toolbar').up('panel').down('cartesian');

                                // console.log(chart);

                                if (Ext.os.is.Desktop) {
                                    chart.download({
                                        filename: 'Chart Rekap PD Berdasarkan Agama'
                                    });
                                } else {
                                    chart.preview();
                                }
                              }
                        });
                    }
                }
            }]
        }];        

        me.items = [{
            xtype: 'cartesian',
            width: '100%',
            height: 480,
            flipXY: true,
            legend: {
                docked: 'bottom'
            },
            interactions: ['itemhighlight'],
            store: this.myDataStore,
            insetPadding: {
                top: 40,
                left: 40,
                right: 40,
                bottom: 40
            },
            axes: [{
                type: 'numeric',
                position: 'bottom',
                adjustByMajorUnit: true,
                grid: true,
                fields: yFieldV,
                minimum: 0
            }, {
                type: 'category',
                position: 'left',
                grid: true,
                fields: ['nama']
            }],
            series: [{
                type: 'bar',
                axis: 'bottom',
                title: titleV,
                xField: 'nama',
                yField: yFieldV,
                stacked: true,
                style: {
                    opacity: 0.80
                },
                highlight: {
                    fillStyle: 'yellow'
                },
                label:{
                    display: 'insideStart',
                    // color: '#ffffff',
                    orientation : 'horizontal',
                    font: '10px Helvetica',
                    field: yFieldV,
                    renderer: function(text, sprite, config, rendererData, index){
                        return text;
                    }
                },
                tooltip: {
                    style: 'background: #fff',
                    renderer: function(storeItem, item) {
                        var browser = item.series.getTitle()[Ext.Array.indexOf(item.series.getYField(), item.field)];
                        this.setHtml(browser + ' di wilayah ' + storeItem.get('nama') + ': ' + storeItem.get(item.field));
                    }
                }
            }]
        }];

        this.myDataStore.load();

        this.callParent();
    }
});