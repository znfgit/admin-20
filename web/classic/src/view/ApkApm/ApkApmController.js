Ext.define('Admin.view.ApkApm.ApkApmController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.apkapm',
    ApkApmGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.getStore().on('beforeload', function(store){
            Ext.apply(store.proxy.extraParams, {
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });


        });

        grid.getStore().on('load', function(store){

            var rec = store.getAt(0);

            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

        });

    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
    	combo.up('gridpanel').getStore().reload();
    },
    simpanApkApm: function(btn){
        // Ext.MessageBox.alert('tes');

        var grid = btn.up('gridpanel');

        var store = grid.getStore();
        var data = store.data;

        var json = '[';

        data.each(function(record){
            // console.log(record.data.penduduk_6_12_tahun);
            // console.log(record.data.penduduk_13_15_tahun);
            // console.log(record.data.penduduk_16_19_tahun);
            // if(record.data.penduduk_6_12_tahun || record.data.penduduk_13_15_tahun || record.data.penduduk_16_19_tahun ){
            json += '{';

            json += '"kode_wilayah" : "'+record.data.kode_wilayah+'",';

            if( record.data.penduduk_6_12_tahun ){
                json += '"penduduk_6_12_tahun" : "'+record.data.penduduk_6_12_tahun+'",';
            }else{
                json += '"penduduk_6_12_tahun" : "0",';
            }

            if( record.data.penduduk_13_15_tahun ){
                json += '"penduduk_13_15_tahun" : "'+record.data.penduduk_13_15_tahun+'",';
            }else{
                json += '"penduduk_13_15_tahun" : "0",';
            }

            if( record.data.penduduk_16_19_tahun ){
                json += '"penduduk_16_19_tahun" : "'+record.data.penduduk_16_19_tahun+'",';
            }else{
                json += '"penduduk_16_19_tahun" : "0",';
            }

            if( record.data.pd_mi ){
                json += '"pd_mi" : "'+record.data.pd_mi+'",';
            }else{
                json += '"pd_mi" : "0",';
            }

            if( record.data.pd_mi_6_12_tahun ){
                json += '"pd_mi_6_12_tahun" : "'+record.data.pd_mi_6_12_tahun+'",';
            }else{
                json += '"pd_mi_6_12_tahun" : "0",';
            }

            if( record.data.pd_mts ){
                json += '"pd_mts" : "'+record.data.pd_mts+'",';
            }else{
                json += '"pd_mts" : "0",';
            }    

            if( record.data.pd_mts_13_15_tahun ){
                json += '"pd_mts_13_15_tahun" : "'+record.data.pd_mts_13_15_tahun+'",';
            }else{
                json += '"pd_mts_13_15_tahun" : "0",';
            }   

            if( record.data.pd_ma ){
                json += '"pd_ma" : "'+record.data.pd_ma+'",';
            }else{
                json += '"pd_ma" : "0",';
            }   

            if( record.data.pd_ma_16_19_tahun ){
                json += '"pd_ma_16_19_tahun" : "'+record.data.pd_ma_16_19_tahun+'",';
            }else{
                json += '"pd_ma_16_19_tahun" : "0",';
            }  

            json = json.substring(0, (json.length - 1));

            json += '},';
            // }


        });

        json = json.substring(0, (json.length - 1));

        json += ']';

        // console.log(json);

        Ext.Ajax.request({
            url     : 'Administrasi/simpanApkApm',
            method  : 'POST',
            params  : {
                data: json
            },
            failure : function(response, options){
                Ext.MessageBox.alert('Warning','Ada Kesalahan dalam pemrosesan data. Mohon dicek lagi');
            },
            success : function(response){
                var json = Ext.JSON.decode(response.responseText);

                grid.getStore().reload();

                // console.log(json);
            }
        });


    }

    // TODO - Add control logic or remove if not needed
});
