Ext.define('Admin.view.ApkApm.ApkApmGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.apkapmgrid',
    title: '',
    columnLines: true,
    selType: 'rowmodel',
    listeners:{
        afterrender:'ApkApmGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.ApkApm');
    },
    createEditing: function() {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                listeners:{
                    click: 'kembali'
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'SatuanPendidikan',
                paramMethod: 'SatuanPendidikanRingkasan',
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a,
            c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {

        var grid = this;

        this.store = this.createStore();

        Ext.apply(this, this.initialConfig);

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function(rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });

        var editing = this.createEditing();


        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            // locked: true,
            dataIndex: 'no',
            hideable: false,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1);
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Wilayah',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            // locked: true
        },
        {
            text: 'SD/Sederajat',
            columns: [{
                header: 'Penduduk<br>6-12 Tahun',
                align:'right',
                dataIndex: 'penduduk_6_12_tahun',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                },
                field: {
                    xtype: 'numberfield'
                    ,maxLength: 500
                    ,enforceMaxLength: true
                    ,minValue: 0
                }
            },{
                header: 'PD MI',
                align:'right',
                dataIndex: 'pd_mi',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                },
                field: {
                    xtype: 'numberfield'
                    ,maxLength: 500
                    ,enforceMaxLength: true
                    ,minValue: 0
                }
            },{
                header: 'PD MI<br>6-12 Tahun',
                align:'right',
                dataIndex: 'pd_mi_6_12_tahun',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                },
                field: {
                    xtype: 'numberfield'
                    ,maxLength: 500
                    ,enforceMaxLength: true
                    ,minValue: 0
                }
            },{
                header: 'PD SD/<br>Sederajat',
                align:'right',
                dataIndex: 'pd_sd',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'PD SD/Sederajat<br>6-12 Tahun',
                align:'right',
                dataIndex: 'pd_sd_6_12_tahun',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'APK SD',
                align:'right',
                dataIndex: 'apk_sd',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    // return grid.formatMoney(v);
                    if(r.data.penduduk_6_12_tahun > 0){
                        var ret = ( (r.data.pd_sd+r.data.pd_mi) / r.data.penduduk_6_12_tahun) * 100;

                        return '<b>'+grid.formatMoney(ret)+' %</b>';
                    }else{
                        return 0;
                    }
                }
            },{
                header: 'APM SD',
                align:'right',
                dataIndex: 'apm_sd',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    // return grid.formatMoney(v);
                    if(r.data.penduduk_6_12_tahun > 0){
                        var ret = ( (r.data.pd_sd_6_12_tahun+r.data.pd_mi_6_12_tahun) / r.data.penduduk_6_12_tahun) * 100;

                        return '<b>'+grid.formatMoney(ret)+' %</b>';
                    }else{
                        return 0;
                    }
                }
            }]
        },{
            text: 'SMP/Sederajat',
            columns: [{
                header: 'Penduduk<br>13-15 Tahun',
                align:'right',
                dataIndex: 'penduduk_13_15_tahun',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                },
                field: {
                    xtype: 'numberfield'
                    ,maxLength: 500
                    ,enforceMaxLength: true
                    ,minValue: 0
                }
            },{
                header: 'PD MTs',
                align:'right',
                dataIndex: 'pd_mts',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                },
                field: {
                    xtype: 'numberfield'
                    ,maxLength: 500
                    ,enforceMaxLength: true
                    ,minValue: 0
                }
            },{
                header: 'PD MTs<br>13-15 Tahun',
                align:'right',
                dataIndex: 'pd_mts_13_15_tahun',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                },
                field: {
                    xtype: 'numberfield'
                    ,maxLength: 500
                    ,enforceMaxLength: true
                    ,minValue: 0
                }
            },{
                header: 'PD SMP/Sederajat',
                align:'right',
                dataIndex: 'pd_smp',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'PD SMP/Sederajat<br>13-15 Tahun',
                align:'right',
                dataIndex: 'pd_smp_13_15_tahun',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'APK SMP',
                align:'right',
                dataIndex: 'apk_smp',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    // return grid.formatMoney(v);
                    if(r.data.penduduk_13_15_tahun > 0){
                        var ret = ( (r.data.pd_smp+r.data.pd_mts) / r.data.penduduk_13_15_tahun) * 100;

                        return '<b>'+grid.formatMoney(ret)+' %</b>';
                    }else{
                        return 0;
                    }
                }
            },{
                header: 'APM SMP',
                align:'right',
                dataIndex: 'apm_smp',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    // return grid.formatMoney(v);
                    if(r.data.penduduk_13_15_tahun > 0){
                        var ret = ( (r.data.pd_smp_13_15_tahun+r.data.pd_mts_13_15_tahun) / r.data.penduduk_13_15_tahun) * 100;

                        return '<b>'+grid.formatMoney(ret)+' %</b>';
                    }else{
                        return 0;
                    }
                }
            }]
        },{
            text: 'SMA/Sederajat',
            columns: [{
                header: 'Penduduk<br>16-19 Tahun',
                align:'right',
                dataIndex: 'penduduk_16_19_tahun',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                },
                field: {
                    xtype: 'numberfield'
                    ,maxLength: 500
                    ,enforceMaxLength: true
                    ,minValue: 0
                }
            },{
                header: 'PD MA',
                align:'right',
                dataIndex: 'pd_ma',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                },
                field: {
                    xtype: 'numberfield'
                    ,maxLength: 500
                    ,enforceMaxLength: true
                    ,minValue: 0
                }
            },{
                header: 'PD MA<br>16-19 Tahun',
                align:'right',
                dataIndex: 'pd_ma_16_19_tahun',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                },
                field: {
                    xtype: 'numberfield'
                    ,maxLength: 500
                    ,enforceMaxLength: true
                    ,minValue: 0
                }
            },{
                header: 'PD SMA/SMK',
                align:'right',
                dataIndex: 'pd_sma',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'PD SMA/SMK<br>16-19 Tahun',
                align:'right',
                dataIndex: 'pd_sma_16_19_tahun',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'APK SMA/SMK',
                align:'right',
                dataIndex: 'apk_sma',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){

                    if(r.data.penduduk_16_19_tahun > 0){
                        var ret = ( (r.data.pd_sma+r.data.pd_ma) / r.data.penduduk_16_19_tahun) * 100;

                        return '<b>'+grid.formatMoney(ret)+' %</b>';
                    }else{
                        return 0;
                    }

                }
            },{
                header: 'APM SMA/SMK',
                align:'right',
                dataIndex: 'apm_sma',
                width: 120,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    if(r.data.penduduk_16_19_tahun > 0){
                        var ret = ( (r.data.pd_sma_16_19_tahun+r.data.pd_ma_16_19_tahun) / r.data.penduduk_16_19_tahun) * 100;

                        return '<b>'+grid.formatMoney(ret)+' %</b>';
                    }else{
                        return 0;
                    }
                }
            }]
        }
        ];

        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();

        this.callParent(arguments);

        // this.store.load();
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        if(session.peran_id == 1){
            this.rowEditing = editing;
            this.plugins = [editing];

            grid.down('toolbar').insert(2,{
                xtype: 'button',
                text: 'Simpan',
                ui: 'soft-green',
                iconCls: 'x-fa fa-check',
                listeners:{
                    click: 'simpanApkApm'
                }
            });
        }
    }
});