Ext.define('Admin.view.ApkApm.ApkApm', {
    extend: 'Ext.container.Container',
    xtype: 'apkapm',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'apkapm',
    
    cls: 'apkapm-container',

    // layout: 'responsivecolumn',
    layout: 'border',

    items: [
        {
            xtype: 'apkapmgrid',
            ui: 'yellow',
            title: 'APK dan APM Berdasarkan Wilayah',
            // Always 100% of container
            // responsiveCls: 'big-100',
            // minHeight:200
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});
