Ext.define('Admin.view.Tentang.Tentang', {
    extend: 'Ext.container.Container',
    xtype: 'tentangcontainer',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'tentang',
    
    cls: 'tentang-container',

    layout: 'responsivecolumn',
    // layout: 'border',

    items: [
        {
            xtype: 'panel',
            // title: 'Panel Pertama',
            // Always 100% of container
            responsiveCls: 'big-100 small-100',
            minHeight:300,
            listeners:{
                afterrender:'panelPertamaSetup'
            },
            scrollable: true,
            html: '<div class="col-md-12" style="margin:10px;font-size:14px;line-height:25px">'
                +'<img src="resources/images/logo_dikbud.png" width="80px"/><br><hr>'
                +'<h2><strong>Deskripsi :</strong></h2>'
                +'<p>Aplikasi Profil Pendidikan merupakan aplikasi yang menyajikan informasi pendidikan bagi pihak yang berkepentingan untuk melakukan evaluasi, monitoring dan koordinasi bagi para pemangku kepentingan. Keberadaan Sistem Informasi Manajemen Pendidikan Dasar yang berbasis Teknologi Informasi dan Komunikasi (TIK), memiliki dasar hukum dan merupakan kewajiban kabupaten/kota di Indonesia. <br><br>Manfaat yang diharapkan dengan diaplikasikannya sistem ini adalah:</p>'
                +'<ol>'
                +'<li>'
                +'Menampilkan Data Informasi Manajemen Pendidikan berbasis TIK yang dikembangkan oleh kabupaten/kota'
                +'</li>'
                +'<li>'
                +'Sumber data untuk perencanaan program pendidikan di kabupaten kota (data ter-update per semester dari sekolah)'
                +'</li>'
                +'<li>'
                +'Mengukur kinerja pelayanan pendidikan kabupaten kota terhadap indikator pendidikan dan sasaran pendidikan'
                +'</li>'
                +'<li>'
                +'Sumber data bagi Sistem Informasi Manajemen Standar Pelayanan Minimum (SPM) Pendidikan untuk mempermudah monitoring dan pelaporan profil pendidikan kepada pimpinan dengan data yang up-to-date sebagai bahan pertimbangan pengambilan keputusan'
                +'</li>'
                +'<li>'
                +'Sebagai sarana informasi mengenai lingkup kedinasan, khususnya lingkup di Dinas Pendidikan Kota dalam menunjang kinerja perkantorannya'
                +'</li>'
                +'</ol>'
                +'<hr>'
            +'</div>'
            // ,dockedItems:[{
            //     xtype: 'toolbar',
            //     dock: 'bottom',
            //     items:[{
            //         xtype: 'button',
            //         text: 'Klik dong',
            //         name: 'buttonklik',
            //         ui:'soft-red'
            //     }]
            // }]
            // region: 'center'
            // html: 'isinya daftar pengiriman',
        }
        // ,{
        //     xtype: 'panel',
        //     title: 'Panel Kedua',
        //     name:'panelkedua',
        //     // Always 100% of container
        //     responsiveCls: 'big-40 small-100',
        //     minHeight:300
        //     // region: 'center'
        //     // html: 'isinya daftar pengiriman'
        // },
        // {
        //     xtype: 'panel',
        //     title: 'Panel Ketiga',
        //     // Always 100% of container
        //     responsiveCls: 'big-50 small-100',
        //     minHeight:300
        //     // region: 'center'
        //     // html: 'isinya daftar pengiriman'
        // },
        // {
        //     xtype: 'panel',
        //     title: 'Panel Keempat',
        //     // Always 100% of container
        //     responsiveCls: 'big-50 small-100',
        //     minHeight:300
        //     // region: 'center'
        //     // html: 'isinya daftar pengiriman'
        // }
    ]
});
