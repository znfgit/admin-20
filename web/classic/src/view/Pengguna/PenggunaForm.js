Ext.define('Admin.view.Pengguna.PenggunaForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.penggunaform',
    bodyPadding: 10,
    defaults: {
        labelWidth: 140,
        anchor: '100%'
    },
    initComponent: function() {
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        this.items = [{
            xtype: 'hidden'
            ,fieldLabel: 'Id'
            ,labelAlign: 'right'
            ,allowBlank: false
            ,maxValue: 999999999
            ,minValue: 0
            ,maxLength: 11
            ,enforceMaxLength: true
            ,name: 'pengguna_id'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Wilayah'
            ,labelAlign: 'right'
            ,allowBlank: false
            ,maxValue: 999999999
            ,minValue: 0
            ,name: 'kode_wilayah_str'
            ,disabled: true
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Peran'
            ,labelAlign: 'right'
            ,allowBlank: false
            ,maxValue: 999999999
            ,minValue: 0
            ,name: 'peran_id_str'
            ,disabled: true
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Username'
            ,labelAlign: 'right'
            ,allowBlank: false
            ,maxValue: 999999999
            ,minValue: 0
            ,maxLength: 50
            ,enforceMaxLength: true
            ,name: 'username'
            ,disabled: true
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Nama'
            ,labelAlign: 'right'
            ,allowBlank: false
            ,maxValue: 999999999
            ,minValue: 0
            ,maxLength: 60
            ,enforceMaxLength: true
            ,name: 'nama'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'NIP/NIM'
            ,labelAlign: 'right'
            ,maxValue: 999999999
            ,minValue: 0
            ,maxLength: 9
            ,enforceMaxLength: true
            ,name: 'nip_nim'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Jabatan'
            ,labelAlign: 'right'
            ,maxValue: 999999999
            ,minValue: 0
            ,maxLength: 25
            ,enforceMaxLength: true
            ,name: 'jabatan_lembaga'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Yahoo Messanger'
            ,labelAlign: 'right'
            ,maxValue: 999999999
            ,minValue: 0
            ,maxLength: 20
            ,enforceMaxLength: true
            ,name: 'ym'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Skype'
            ,labelAlign: 'right'
            ,maxValue: 999999999
            ,minValue: 0
            ,maxLength: 20
            ,enforceMaxLength: true
            ,name: 'skype'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Alamat'
            ,labelAlign: 'right'
            ,maxValue: 999999999
            ,minValue: 0
            ,maxLength: 80
            ,enforceMaxLength: true
            ,name: 'alamat'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'No HP'
            ,labelAlign: 'right'
            ,maxValue: 999999999
            ,minValue: 0
            ,maxLength: 20
            ,enforceMaxLength: true
            ,name: 'no_hp'
        }];

        this.buttons = [{
            text: 'Simpan Perubahan',
            iconCls: 'fa fa-save fa-inverse fa-lg',
            handler: 'simpanForm'
            // action: 'Simpan Perubahan'
        },{
            xtype: 'button',
            text: 'Keluar',
            ui: 'soft-red',
            iconCls: 'right-icon x-fa fa-sign-out',
            handler: function(btn){
                localStorage.setItem("local.dapodik.session", null);

                window.location.replace('prosesLogout');
            }
        }];

        this.callParent(arguments);
    }
});