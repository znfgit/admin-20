Ext.define('Admin.view.Pengguna.PenggunaController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.pengguna',
    PenggunaGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        var textfield = grid.down('toolbar').down('textfield');
        var perancombo = grid.down('toolbar').down('perancombo');

        grid.getStore().on('beforeload', function(store){

            Ext.apply(store.proxy.extraParams, {
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah,
                keyword: textfield.getValue(),
                // peran_id: perancombo.getValue(),
                peran_id: 10
            });


        });

        textfield.on('specialkey', function(f, e){
            if (e.getKey() == e.ENTER) {
                // your action

                grid.getStore().load();
            }
        });

        perancombo.on('change', function(combo, options){
            grid.getStore().load();
        });

    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
    	combo.up('gridpanel').getStore().reload();
    },
    saveForm: function(btn) {

        //Ext.Msg.alert('Saving', 'Manyimpan..');

        // Dari button, 'look up'/lihat ke atas, cari container dengan xtype = 'form'
        var form = btn.up('form');
        if (form.isValid()) {
            // Ambil record dari form, form tersebut diisi dari sebuah record
            var record = form.getRecord();
            var values = form.getValues(false,false,false,true);

            Ext.Ajax.request({
                url: 'getHash?keyword='+values.password,
                success: function(response){
                    var text = response.responseText;

                    values.password = text;
                    values.aktif = 1;

                    record.set(values);

                    var store = record.store;
                    store.sync({
                        success: function(){
                            var win = form.up('window');
                            win.destroy();
                        }
                    });
                }
            });

        } else {
            Ext.MessageBox.alert('Error', 'Gagal menyimpan, mohon isi form dengan benar. <br>Field yang salah ditandai dengan kotak merah.');
        }

    },
    addRecord: function(btn) {

        //Ext.Msg.alert('Info', 'Manambah..');
        //return;

        // Dari button, 'look up'/lihat ke atas, cari container dengan xtype = 'form'
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        // Create a model instance
        var r = this.getNewRecord(grid);

        grid.store.insert(0, r);
        // grid.rowEditing.startEdit(0, 0);



    },
    // editRecord: function(btn) {
    //     // Defaults using row editor. Override this to use other method, such as form etc
    //     var grid = btn.up('gridpanel');
    //     grid.rowEditing.cancelEdit();

    //     //var selections = grid.getSelectionModel().getSelection();
    //     var selections = grid.getSelection();
    //     var r = selections[0];
    //     if (!r) {
    //         Xond.msg('Error', 'Mohon pilih salah satu baris');
    //         return;
    //     }
    //     var startEditingColumnNumber = 0;
    //     for (var i=0; i<grid.columns.length; i++) {
    //         if (columns[i].isVisible()) {
    //             var startEditingColumnNumber = i;
    //             break;
    //         }
    //     }
    //     grid.rowEditing.startEdit(r, startEditingColumnNumber);
    // },
    editRecord: function(btn) {
        // Defaults using row editor. Override this to use other method, such as form etc
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        var selections = grid.getSelectionModel().getSelection();
        var r = selections[0];
        if (!r) {
            Ext.MessageBox.alert('Error', 'Mohon pilih salah satu baris');
            return;
        }
        grid.rowEditing.startEdit(r, 0);

    },
    saveRecord: function(btn) {
        var grid = btn.up('gridpanel');
        grid.store.sync();
    },
    deleteRecord: function(btn) {
        // Defaults using row editor. Override this to use other method, such as form etc
        var grid = btn.up('gridpanel');
        grid.rowEditing.cancelEdit();

        // var selections = grid.getSelectionModel().getSelection();
        var selections = grid.getSelection();
        var r = selections[0];
        if (!r) {
            Ext.MessageBox.alert('Error', 'Mohon pilih salah satu baris');
            return;
        }

        Ext.MessageBox.show({
            title: 'Hapus Data',
            msg: 'Apakah anda yakin ingin menghapus data tsb ?',
            buttonText: {yes: "Ya", no: "Tidak"},
            fn: function(btn){
                if (btn == "yes") {

                    Ext.Ajax.request({
                        url: 'Administrasi/HapusPengguna',
                        params:{
                            id:r.data.pengguna_id
                        },
                        success: function(response){
                            var text = response.responseText;

                            var json = Ext.JSON.decode(text);

                            if(json.success){
                                Ext.MessageBox.alert('Berhasil', json.message);
                                grid.getStore().reload();
                            }else{
                                Ext.MessageBox.alert('Gagal', json.message);
                            }
                        }
                    });

                }
                // console.debug('you clicked: ',btn); //you clicked:  yes
            }
        });
    },
    getNewRecord: function(grid) {
        var recordConfig = {
            pengguna_id : '',
            sekolah_id : '',
            lembaga_id : '',
            yayasan_id : '',
            la_id : '',
            peran_id : 1,
            username : '',
            password : '',
            nama : '',
            nip_nim : '',
            jabatan_lembaga : '',
            ym : '',
            skype : '',
            alamat : '',
            kode_wilayah : '',
            no_telepon : '',
            no_hp : '',
            aktif : 1,
            last_update : '',
            soft_delete : 0,
            last_sync : '',
            updater_id : ''
        };
        Ext.apply(recordConfig, grid.newRecordCfg);
        var r = new Admin.model.Pengguna(recordConfig);
        return r;
    },
    addRecordWindow: function(btn){
        var height = Ext.Element.getViewportHeight() - 64;
        var grid = btn.up('gridpanel');
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        // var store = new Admin.store.Pengguna();

        // var r = this.getNewRecord(grid);

        // store.insert(r);

        var store = Ext.create('Admin.store.Pengguna');

        var recordConfig = {
            pengguna_id : '',
            sekolah_id : '',
            lembaga_id : '',
            yayasan_id : '',
            la_id : '',
            peran_id : 1,
            username : '',
            password : '',
            nama : '',
            nip_nim : '',
            jabatan_lembaga : '',
            ym : '',
            skype : '',
            alamat : '',
            kode_wilayah : session.kode_wilayah,
            no_telepon : '',
            no_hp : '',
            aktif : 1,
            last_update : '',
            soft_delete : 0,
            last_sync : '',
            updater_id : ''
        };
        var r = new Admin.model.Pengguna(recordConfig);
        store.insert(0, r);

        var win = Ext.create('Admin.view.Pengguna.PenggunaWindow',{
            height: height
        });

        var form = win.down('form');

        form.loadRecord(r);

        Ext.apply(Ext.form.VTypes, {
            password: function(val, field) {
                // console.log(val, field);
                if (field.initialPassField) {
                    var pwd = Ext.getCmp(field.initialPassField);
                    return (val == pwd.getValue());
                }
                return true;
            },
            passwordText: 'Password tidak sama!'
        });

        form.down('hidden[name=username]').destroy();
        form.down('hidden[name=password]').destroy();
        form.down('hidden[name=kode_wilayah]').destroy();
        form.down('hidden[name=peran_id]').destroy();

        form.add(/*{
            xtype: 'perancombo',
            allowBlank: false,
            fieldLabel: 'Peran',
            labelAlign: 'right',
            name: 'peran_id',
            listeners: {
                'change': function(field, newValue, oldValue) {
                    var val = newValue;

                    var combowilayah = this.up('form').down('mstwilayahcombo[name=kode_wilayah]');

                    if(val == 2 || val == 3 || val == 4 || val == 5){
                        combowilayah.setValue('000000 ');
                        combowilayah.disable();

                    }else if(val == 6 || val == 7){
                        combowilayah.enable();

                        combowilayah.setValue('');

                        combowilayah.getStore().on('beforeload', function(store){
                            Ext.apply(store.proxy.extraParams, {
                                id_level_wilayah: 1
                            });
                        });

                        combowilayah.getStore().reload();
                    }else if(val == 8 || val == 9){
                        combowilayah.enable();

                        combowilayah.setValue('');

                        combowilayah.getStore().on('beforeload', function(store){
                            Ext.apply(store.proxy.extraParams, {
                                id_level_wilayah: 2
                            });
                        });

                        combowilayah.getStore().reload();
                    }else{
                        combowilayah.enable();

                        combowilayah.setValue('');

                        combowilayah.getStore().reload();
                    }

                }
            }
        },*/{
            xtype: 'mstwilayahcombo',
            anchor: '100%',
            fieldLabel: 'Wilayah',
            name: 'kode_wilayah',
            // disabled: true,
            hidden: true,
            value: session.kode_wilayah,
            emptyText: 'Pilih Wilayah (Ketik untuk mencari)',
            labelAlign: 'right',
            labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
        },{
            xtype: 'textfield',
            allowBlank: false,
            fieldLabel: 'Username (Email)',
            name: 'username',
            emptyText: 'Username...',
            vtype: 'email',
            labelAlign: 'right',
            labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
        },{
            xtype: 'textfield',
            allowBlank: false,
            fieldLabel: 'Password',
            name: 'password',
            labelAlign: 'right',
            id: 'password',
            emptyText: 'Password...',
            inputType: 'password',
            labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
        },{
            xtype: 'textfield',
            allowBlank: false,
            labelAlign: 'right',
            fieldLabel: 'Ulang Password',
            name: 'password_confirm',
            emptyText: 'Konfirmasi password...',
            inputType: 'password',
            vtype: 'password',
            initialPassField: 'password',
            labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
        },{
            xtype: 'textfield',
            hidden: true,
            value: 1,
            name: 'aktif'
        });

        win.show();
    }

    // TODO - Add control logic or remove if not needed
});
