Ext.define('Admin.view.Pengguna.Pengguna', {
    extend: 'Ext.container.Container',
    xtype: 'pengguna',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'pengguna',

    cls: 'pengguna-container',

    // layout: 'responsivecolumn',
    // scrollable: 'y',
    layout: 'border',

    items: [
        {
            xtype: 'penggunagrid',
            title: 'Manajemen Pengguna',
            // Always 100% of container
            // responsiveCls: 'big-100',
            // minHeight:200,
            // height: 500
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});
