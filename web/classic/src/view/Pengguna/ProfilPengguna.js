Ext.define('Admin.view.Pengguna.ProfilPengguna', {
    extend: 'Ext.container.Container',
    xtype: 'profilpengguna',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'profilpengguna',
    
    cls: 'profilpengguna-container',

    layout: 'responsivecolumn',
    // layout: 'border',
    scrollable: 'y',

    id: 'profilPengguna',

    items: [
        {
            xtype: 'panel',
            // title: 'Profil Pengguna',
            // Always 100% of container
            responsiveCls: 'big-100',
            minHeight:200,
            layout: 'responsivecolumn',
            // dockedItems:[{
            //     xtype: 'toolbar',
            //     dock: 'top',
            //     items:['->',
            //     // {
            //     //     xtype: 'button',
            //     //     text: 'Ganti Password',
            //     //     ui: 'soft-green',
            //     //     iconCls: 'right-icon x-fa fa-key'
            //     // },
            //     ]
            // }],
            items:[{
                xtype: 'penggunaform',
                title: 'Profil Pengguna',
                responsiveCls: 'big-50 small-100',
                cls: 'shadow-panel',
                minHeight: 515,
                listeners:{
                    afterrender:'ProfilPenggunaSetup'
                }
                // border: true
                // title: 'Panel Kiri'
            },{
                xtype: 'panel',
                responsiveCls: 'big-50 small-100',
                cls: 'shadow-panel',
                minHeight: 275,
                title: 'Gambar Profil',
                ui: 'soft-green',
                itemId: 'panelGambar',
                html: '<div style="width:100%;border:1px solid #ccc;height:100%;background:black"></div>',
                header:{
                    items:[{
                        xtype: 'button',
                        iconCls: 'fa fa-pencil',
                        text: 'ubah gambar',
                        ui: 'soft-green',
                        listeners:{
                            click: 'gantiGambar'
                        }
                    }]
                }
            },{
                xtype: 'ubahpasswordform',
                title: 'Ubah Password',
                itemId: 'ubahpasswordform',
                responsiveCls: 'big-50 small-100',
                cls: 'shadow-panel',
                minHeight: 200,
                // ui: 'soft-red',
                listeners:{
                    afterrender:'UbahPasswordSetup'
                }
                // border: true
                // title: 'Panel Kanan'
            }]
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});
