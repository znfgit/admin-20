Ext.define('Admin.view.Pengguna.UbahPasswordForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.ubahpasswordform',
    bodyPadding: 10,
    defaults: {
        labelWidth: 140,
        anchor: '100%'
    },
    initComponent: function() {
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        this.items = [{
            xtype: 'hidden'
            ,fieldLabel: 'Id'
            ,labelAlign: 'right'
            ,allowBlank: false
            ,maxValue: 999999999
            ,minValue: 0
            ,maxLength: 11
            ,enforceMaxLength: true
            ,name: 'pengguna_id'
        },{
            xtype: 'textfield',
            inputType: 'password'
            ,fieldLabel: 'Password Lama'
            ,labelAlign: 'right'
            ,allowBlank: false
            ,maxValue: 999999999
            ,minValue: 0
            ,name: 'password_lama'
            ,disabled: false
        },{
            xtype: 'textfield',
            inputType: 'password'
            ,fieldLabel: 'Password Baru'
            ,labelAlign: 'right'
            ,allowBlank: false
            ,maxValue: 999999999
            ,minValue: 0
            ,name: 'password_baru'
            ,disabled: false
        },{
            xtype: 'textfield',
            inputType: 'password'
            ,fieldLabel: 'Ulangi Password Baru'
            ,labelAlign: 'right'
            ,allowBlank: false
            ,maxValue: 999999999
            ,minValue: 0
            ,name: 'password_baru_ulang'
            ,disabled: false
        }];

        this.buttons = [{
            text: 'Simpan Password',
            iconCls: 'fa fa-save fa-inverse fa-lg',
            handler: 'simpanUbahPassword'
            // action: 'Simpan Perubahan'
        }];

        this.callParent(arguments);
    }
});