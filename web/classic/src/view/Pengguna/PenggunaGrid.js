Ext.define('Admin.view.Pengguna.PenggunaGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.penggunagrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners: {
        afterrender: 'PenggunaGridSetup'
    },
    height: '600',
    createStore: function() {
        return Ext.create('Admin.store.Pengguna');
    },
    createEditing: function() {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Tambah',
                // iconCls: 'fa fa-plus fa-lg glyph-dark-green glyph-shadow',
                iconCls: 'ic-add',
                scope: this,
                action: 'add',
                ui: 'soft-green',
                iconCls: 'x-fa fa-plus-circle',
                // hidden: true,
                listeners:{
                    click:'addRecordWindow'
                }
            }, {
                xtype: 'button',
                text: 'Ubah',
                iconCls: 'fa fc-book_edit fa-lg glyph-dark-orange glyph-shadow',
                // iconCls: 'ic-pencil',
                itemId: 'edit',
                scope: this,
                action: 'edit',
                iconCls: 'x-fa fa-pencil-square',
                // hidden: true,
                ui: 'soft-green',
                listeners:{
                    click:'editRecord'
                }
            }, {
                xtype: 'button',
                text: 'Simpan',
                // iconCls: 'fa fa-check fa-lg glyph-blue glyph-shadow',
                iconCls: 'ic-disk',
                itemId: 'save',
                scope: this,
                action: 'save',
                iconCls: 'x-fa fa-save',
                // hidden: true,
                ui: 'soft-green',
                listeners:{
                    click:'saveRecord'
                }
            },{
                xtype: 'button',
                text: 'Hapus',
                // iconCls: 'fa fa-remove fa-lg glyph-red glyph-shadow',
                iconCls: 'x-fa fa-trash-o',
                // hidden: true,
                itemId: 'delete',
                scope: this,
                ui: 'soft-green',
                action: 'delete',
                listeners:{
                    click:'deleteRecord'
                }
            },{
                xtype: 'textfield',
                name: 'keyword',
                emptyText: 'Ketik Kata Kunci ...'
            },{
                xtype: 'perancombo',
                name: 'peran_id',
                width:300,
                hidden: true,
                emptyText: 'Filter berdasarkan Peran ...'
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function() {

        var grid = this;

        this.store = this.createStore();

        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }
        if (this.initialConfig.baseParams) {

            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });

        }

        // this.getSelectionModel().setSelectionMode('SINGLE');

        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function(rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });

        var editing = this.createEditing();

        this.rowEditing = editing;
        this.plugins = [editing];

        this.lookupStorePeran = new Admin.store.Peran({
            autoLoad: true
        });

        // this.lookupStorePeran.load();

        // this.lookupStorePeran = new Admin.store.Peran({
        //     autoLoad: true
        // });

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 80,
            sortable: true,
            dataIndex: 'pengguna_id',
			hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
				,maxLength: 11
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'No',
            width: 50,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1);
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Nama',
            tooltip: 'Nama',
            width: 240,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            field: {
                xtype: 'textfield'
                ,maxLength: 60
                ,enforceMaxLength: true
                ,minValue: 0
            }
        },{
            header: 'Peran',
            tooltip: 'Peran',
            width: 250,
            sortable: true,
            dataIndex: 'peran_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.peran_id_str;
            }
        },{
            header: 'Wilayah',
            tooltip: 'Wilayah',
            width: 150,
            sortable: true,
            dataIndex: 'kode_wilayah',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.kode_wilayah_str;
            }
        },{
            header: 'Username',
            tooltip: 'Username',
            width: 200,
            sortable: true,
            dataIndex: 'username',
			hideable: false
        },{
            header: 'NIP/NIM',
            tooltip: 'Nip Nim',
            width: 80,
            sortable: true,
            dataIndex: 'nip_nim',
			hideable: false,
            field: {
                xtype: 'textfield'
				,maxLength: 9
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'Jabatan',
            tooltip: 'Jabatan',
            width: 100,
            sortable: true,
            dataIndex: 'jabatan_lembaga',
			hideable: false,
            field: {
                xtype: 'textfield'
				,maxLength: 25
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'YM',
            tooltip: 'Ym',
            width: 80,
            sortable: true,
            dataIndex: 'ym',
			hideable: false,
            field: {
                xtype: 'textfield'
				,maxLength: 20
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'Skype',
            tooltip: 'Skype',
            width: 80,
            sortable: true,
            dataIndex: 'skype',
			hideable: false,
            field: {
                xtype: 'textfield'
				,maxLength: 20
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'Alamat',
            tooltip: 'Alamat',
            width: 320,
            sortable: true,
            dataIndex: 'alamat',
			hideable: false,
            field: {
                xtype: 'textfield'
				,maxLength: 80
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'No Telepon',
            tooltip: 'No Telepon',
            width: 80,
            sortable: true,
            dataIndex: 'no_telepon',
			hideable: false,
            field: {
                xtype: 'textfield'
				,maxLength: 20
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'No Hp',
            tooltip: 'No Hp',
            width: 80,
            sortable: true,
            dataIndex: 'no_hp',
			hideable: false,
            field: {
                xtype: 'textfield'
				,maxLength: 20
				,enforceMaxLength: true
				,minValue: 0
            }
        }];

        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();

        this.callParent(arguments);
    }
});