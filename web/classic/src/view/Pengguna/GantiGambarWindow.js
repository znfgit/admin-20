Ext.define('Admin.view.Pengguna.GantiGambarWindow', {
	extend: 'Ext.window.Window',
    alias: 'widget.gantigambarwindow',
    requires:[
        'Ext.layout.container.Fit',
        'Ext.layout.container.VBox',
        'Ext.TabPanel'
    ],
    layout: {
        type: 'border'
    },
    title: 'Ganti Gambar Profil',
    width: 500,
    height: 150,
    modal: true,
    initComponent: function() { 

        this.items = [{
            xtype: 'form',
            region: 'center',
            bodyPadding: 10,
            items: [{
                xtype: 'filefield',
                name: 'gambar',
                msgTarget: 'top',
                allowBlank: false,
                anchor: '100%',
                buttonText: 'Pilih Gambar...'
            }],
            buttons:[{
                text: 'Simpan',
                iconCls: 'fa fa-save',
                handler: function() {
                    var form = this.up('form').getForm();
                    var formC = this.up('form');
                    var win = this.up('form').up('window');

                    console.log(win.pengguna_id);

                    if(form.isValid()){
                        form.submit({
                            url: 'Administrasi/gantiGambar',
                            waitMsg: 'Mengunggah gambar...',
                            params:{
                                pengguna_id: win.pengguna_id
                            },
                            success: function(fp, o) {
                                Ext.Msg.alert('Success', 'Gambar berhasil diupload');
                                

                                var profilPengguna = Ext.getCmp('profilPengguna');
                                var menuUtama = Ext.getCmp('menuUtama');
                                var panelGambar = profilPengguna.down('panel').down('panel[itemId=panelGambar]'); 

                                Ext.Ajax.request({
                                    url     : 'Administrasi/infoPengguna',
                                    method  : 'GET', 
                                    params  : {
                                        pengguna_id: win.pengguna_id,
                                    },
                                    success : function(response){
                                        var json = Ext.JSON.decode(response.responseText);
                                        
                                        if(json[0]){
                                            // console.log('ada');
                                            panelGambar.update('<div style="width:100%;border:1px solid #ccc;height:100%;background:#434343 url(resources/images/pengguna/'+json[0].gambar+');background-size:contain;background-position:center;background-repeat:no-repeat"></div>');
                                            menuUtama.down('panel[region=center]').down('panel[itemId=panelInfoPengguna]').down('image').destroy();
                                            // menuUtama.down('panel[region=center]').down('panel[itemId=panelInfoPengguna]').down('image') = null;

                                            menuUtama.down('panel[region=center]').down('panel[itemId=panelInfoPengguna]').insert(0,{
                                                xtype: 'image',
                                                cls: 'userProfilePic',
                                                height: 70,
                                                width: 70,
                                                margin:'10 10 10 10',
                                                alt: 'profile-picture',
                                                src: 'resources/images/pengguna/'+json[0].gambar
                                            });
                                        }
                                    }
                                });
                                
                                win.destroy();
                                win = null;
                            }
                        });
                    }
                }
            }]
        }];

        this.callParent(arguments);
    }
});