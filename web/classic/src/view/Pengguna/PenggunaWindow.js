Ext.define('Admin.view.Pengguna.PenggunaWindow', {
	extend: 'Ext.window.Window',
    alias: 'widget.penggunawindow',
    requires:[
        'Ext.layout.container.Fit',
        'Ext.layout.container.VBox',
        'Ext.TabPanel'
    ],
    layout: {
        type: 'fit'
    },
    title: 'Tambah Pengguna',
    width: 800,
    height: 500,
    modal: true,
    initComponent: function() {
    	//var record = this.initialConfig.record;

    	this.items = [{
                    xtype: 'tambahpenggunaform',
                    region: 'center'
                }];

        this.tools = [{
            type: 'maximize',
            handler: function(event, target, owner, tool){
                win = tool.up('window');
                win.toggleMaximize();

                win.addCls('round-corners');

                var tool = win.down('tool[type=minimize]');

                tool.show();
            }
        },{
            type: 'minimize',
            hidden:true,
            handler: function(event, target, owner, tool){
                win = tool.up('window');
                win.toggleMaximize();

                win.removeCls('round-corners');

                var tool = win.down('tool[type=minimize]');

                tool.hide();
            }
        }];
        
        this.callParent(arguments);
    }
});