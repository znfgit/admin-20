Ext.define('Admin.view.Pengguna.ProfilPenggunaController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.profilpengguna',
    ProfilPenggunaSetup: function (form) {
        
        var store = new Admin.store.Pengguna();
        var storePeran = new Admin.store.Peran({
            autoload:true
        });
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        if(session.original == 2){
            form.up('container').add({
                xtype: 'panel',
                responsiveCls: 'big-100 small-100',
                cls: 'shadow-panel',
                // padding:10,
                bodyStyle: 'background:#434343;color:#ccc;font-size:18px;padding:10px',
                html: 'Pengguna yang berasal dari SSO tidak dapat mengubah profil pengguna di halaman ini.'
            });

            form.hide();
            form.up('container').down('panel[itemId=panelGambar]').hide();
            form.up('container').down('panel[itemId=ubahpasswordform]').hide();

            return true;
        }        

        store.load({
            params:{
                pengguna_id: session.pengguna_id
            }
        });

        store.on('load', function(store){
            var rec = store.getAt(0);

            if(rec){
                form.loadRecord(rec);
                
                // console.log(rec.data.pengguna_id);

                Ext.Ajax.request({
                    url     : 'Administrasi/infoPengguna',
                    method  : 'GET', 
                    params  : {
                        pengguna_id: rec.data.pengguna_id,
                    },
                    failure : function(response, options){
                        Ext.MessageBox.alert('Mohon maaf','Ada Kesalahan dalam menampilkan data');
                    },
                    success : function(response){
                        var json = Ext.JSON.decode(response.responseText);
                        
                        if(json[0]){
                            // console.log('ada');

                            form.up('panel').down('panel[itemId=panelGambar]').pengguna_id = rec.data.pengguna_id;
                            form.up('panel').down('panel[itemId=panelGambar]').update('<div style="width:100%;border:1px solid #ccc;height:100%;background:#434343 url(resources/images/pengguna/'+json[0].gambar+');background-size:contain;background-position:center;background-repeat:no-repeat"></div>');

                        }
                    }
                });

            }

            storePeran.load();

            storePeran.on('load', function(storePeran){
                var recPeran = storePeran.getById(rec.data.peran_id);
                
                if(recPeran){
                    form.down('textfield[name=peran_id_str]').setValue(recPeran.data.nama);
                }
            });



        })

        // Ext.MessageBox.alert('nendang bro');

    },
    UbahPasswordSetup: function(form){
        var store = new Admin.store.Pengguna();
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        store.load({
            params:{
                pengguna_id: session.pengguna_id
            }
        });

        store.on('load', function(store){
            var rec = store.getAt(0);

            if(rec){
                form.loadRecord(rec);
            }
        });
    },
    simpanUbahPassword: function(btn){
        var form = btn.up('form');

        if (form.isValid()) {
            // Ambil record dari form, form tersebut diisi dari sebuah record
            // var record = form.getRecord();
            var values = form.getValues(false,false,false,true);
            // record.set(values);

            // console.log(values);

            if(values.password_baru != values.password_baru_ulang){
                Ext.MessageBox.alert('Peringatan','Pengulangan Password Baru tidak sesuai! Silakan ulangi pengisian!');
            }else{

                Ext.Ajax.request({
                    url     : 'UbahPassword',
                    method  : 'POST',
                    params  : {
                        pengguna_id: values.pengguna_id,
                        password_lama: values.password_lama,
                        password_baru: values.password_baru
                    },
                    failure : function(response, options){
                        Ext.MessageBox.alert('Warning','Ada Kesalahan dalam pemrosesan data. Mohon dicek lagi');
                    },
                    success : function(response){
                        var json = Ext.JSON.decode(response.responseText);
                        
                        if(json.success){
                            
                            Ext.MessageBox.hide();
                            Ext.MessageBox.alert('Berhasil', json.message);

                        } else if(json.success == false) {
                            Ext.MessageBox.alert('Error',json.message);
                        }
                    }
                });
            }
        }
    },
    gantiGambar: function(btn){
        var panelGambar = btn.up('panel');

        console.log(panelGambar.pengguna_id);

        var win = Ext.create('Admin.view.Pengguna.GantiGambarWindow',{
            pengguna_id: panelGambar.pengguna_id
        });

        win.show();
    }
});
