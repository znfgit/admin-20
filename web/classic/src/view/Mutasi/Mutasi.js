Ext.define('Admin.view.Mutasi.Mutasi', {
    extend: 'Ext.container.Container',
    xtype: 'mutasi',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'mutasi',
    
    cls: 'mutasi-container',

    // layout: 'responsivecolumn',
    layout: 'border',

    items: [
        {
            xtype: 'mutasigrid',
            title: 'Tabel Mutasi Peserta Didik',
            // Always 100% of container
            // responsiveCls: 'big-100',
            // minHeight:500
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});
