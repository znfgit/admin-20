Ext.define('Admin.view.Mutasi.MutasiController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.mutasi',
    setupMutasiGrid: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.getStore().on('beforeload', function(store){

            if(session.peran_id == 10){

                Ext.apply(store.proxy.extraParams, {
                    sekolah_id : session.sekolah_id                
                });
                
            }else{
                grid.down('toolbar').down('button[itemId=ubah]').hide();
                grid.down('toolbar').down('button[itemId=hapus]').hide();

                grid.down('toolbar').add({
                    text: 'Ubah Status',
                    itemId: 'ubahStatus',
                    iconCls: 'x-fa fa-pencil-square',
                    ui: 'soft-blue',
                    listeners:{
                        click: function(btn){
                            // Ext.MessageBox.alert('tes', 'oke');
                            var grid = btn.up('gridpanel');
                            var selections = grid.getSelectionModel().getSelection();
                            var r = selections[0];
                            if (!r) {
                                Ext.MessageBox.alert('Error', 'Mohon pilih salah satu baris');
                                return;
                            }


                            var height = Ext.Element.getViewportHeight() - 100;
                            var width = Ext.Element.getViewportWidth() - 300;

                            var winForm = new Ext.window.Window({   
                                height: 400,
                                width: width,
                                modal:true,
                                resizable: false,
                                title: 'Ubah Status Mutasi Peserta Didik a/n ' + r.data.nama,
                                autoScroll: true,
                                resizable: true,
                                frame: false,            
                                border: true,
                                layout: 'fit',
                                closable: true,
                                items: [{
                                    xtype:'mutasipesertadidikform',
                                    // region:'center',
                                    frame: false,            
                                    border: false,  
                                    bodyPadding: '10 10 10 10',
                                    defaultType: 'textfield',
                                    record: r,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                }]
                            });

                            var form = winForm.down('mutasipesertadidikform');

                            var fs0 = form.down('fieldset[itemId=surat]');

                            var comboJenisSurat = fs0.down('combojenissurat').hide();
                            var fs1 = form.down('fieldset[itemId=pdfs]').hide();
                            var fs2 = form.down('fieldset[itemId=dataOrangTua]').hide();
                            var fs2 = form.down('fieldset[itemId=detailMutasi]').hide();

                            fs0.insert(0,{
                                xtype: 'combostatusmutasi',
                                fieldLabel: 'Status Mutasi',
                                name: 'status_mutasi_id',
                                labelAlign: 'right',
                                allowBlank: false,
                                margins: '0 0 0 0',
                                width: 200,
                                listeners: {
                                    'afterrender': function(combo) {
                                        combo.getStore().load({
                                            params:{
                                                status_mutasi_id: r.get('status_mutasi_id')
                                            }
                                        });
                                    }
                                }
                            });

                            fs0.insert(1,{
                                xtype: 'textfield',
                                fieldLabel: 'Keterangan',
                                name: 'keterangan',
                                labelAlign: 'right'
                            });

                            console.log(r.data.tanggal_surat);

                            if(r.data.tanggal_surat == ''){
                                fs0.down('datefield').setValue(new Date());
                            }


                            winForm.on('close', function(window){
                                grid.getStore().reload();
                            });

                            winForm.show();
                        }
                    }
                })
            }

        });

        grid.down('toolbar').add({
            text: 'Cetak',
            ui: 'soft-purple',
            iconCls: 'x-fa fa-print',
            itemId: 'cetak',
            disabled: true,
            listeners:{
                click: function(btn){
                    var grid = btn.up('gridpanel');
                    var selections = grid.getSelectionModel().getSelection();
                    var rec = selections[0];
                    if (!rec) {
                        Ext.MessageBox.alert('Error', 'Mohon pilih salah satu baris');
                        return;
                    }

                    var height = Ext.Element.getViewportHeight() - 100;
                    var width = Ext.Element.getViewportWidth() - 300;
                    var urlCetak = 'cetak/Mutasi/'+rec.data.mutasi_id + '/lihat';

                    var win = new Ext.window.Window({   
                        title: 'Cetak Mutasi Peserta Didik a/n ' + rec.data.nama,
                        width : 700,
                        height: height,
                        maximizable: true,
                        modal:true,
                        layout : 'fit',
                        closeAction: 'hide',
                        items : [{
                            xtype : "component",
                            autoEl : {
                                tag : "iframe",
                                src : urlCetak
                            }
                        }],
                        buttons: [{
                            text: 'Cetak PDF',
                            scope: this,
                            handler: function(btn) {
                                // window.location.replace('cetak/Mutasi/'+ rec.data.mutasi_id + '/pdf');
                                window.open('cetak/Mutasi/'+ rec.data.mutasi_id + '/pdf','_blank');
                            }
                        }]
                    });

                    win.show();
                }
            }
        });

        grid.getStore().load();

        grid.getSelectionModel().on({
            selectionchange: function(sm, selectedRows, opts) {
                var rec = grid.getSelectionModel().getSelection()[0];

                if(rec.data.status_mutasi_id != 1){
                    grid.down('toolbar').down('button[itemId=ubah]').disable();
                    grid.down('toolbar').down('button[itemId=hapus]').disable();
                    // grid.down('toolbar').down('button[itemId=cetak]').enable();
                }else{
                    grid.down('toolbar').down('button[itemId=ubah]').enable();
                    grid.down('toolbar').down('button[itemId=hapus]').enable();
                    // grid.down('toolbar').down('button[itemId=cetak]').enable();
                }

                if(rec.data.status_mutasi_id == 3){
                    grid.down('toolbar').down('button[itemId=cetak]').enable();
                }else{
                    grid.down('toolbar').down('button[itemId=cetak]').disable();
                }

            }
        });
    },
    hapusMutasi: function(btn){
        var grid = btn.up('gridpanel');
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        
        var selections = grid.getSelectionModel().getSelection();
        var r = selections[0];
        if (!r) {
            Ext.MessageBox.alert('Error', 'Mohon pilih salah satu baris');
            return;
        }

        Ext.MessageBox.show({
            title: 'Hapus Data',
            msg: 'Apakah anda yakin ingin menghapus data tsb ?',
            buttonText: {yes: "Ya", no: "Tidak"},
            fn: function(btn){
                if (btn == "yes") {
                    Ext.Ajax.request({
                        url: 'hapus/Mutasi',
                        params:{
                            id:r.data.mutasi_id
                        },
                        success: function(response){
                            var text = response.responseText;
                            
                            var json = Ext.JSON.decode(text);

                            if(json.success){
                                Ext.MessageBox.alert('Berhasil', json.message);
                                grid.getStore().reload();
                            }else{
                                Ext.MessageBox.alert('Gagal', json.message);
                            }
                        }
                    });
                }
            }
        });
    },
    ubahMutasi: function(btn){
        var grid = btn.up('gridpanel');
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        
        var selections = grid.getSelectionModel().getSelection();
        var r = selections[0];
        if (!r) {
            Ext.MessageBox.alert('Error', 'Mohon pilih salah satu baris');
            return;
        }

        console.log(r);

        var height = Ext.Element.getViewportHeight() - 100;
        var width = Ext.Element.getViewportWidth() - 300;

        var winForm = new Ext.window.Window({   
            height: height,
            width: width,
            modal:true,
            resizable: false,
            title: 'Ubah Mutasi Peserta Didik a/n ' + r.data.nama,
            autoScroll: true,
            resizable: true,
            frame: false,            
            border: true,
            layout: 'fit',
            closable: true,
            items: [{
                xtype:'mutasipesertadidikform',
                // region:'center',
                frame: false,            
                border: false,  
                bodyPadding: '10 10 10 10',
                defaultType: 'textfield',
                record: r,
                defaults: {
                    anchor: '100%'
                },
            }]
        });

        var form = winForm.down('mutasipesertadidikform');

        if(session.peran_id == 10){
            form.down('combopengguna').hide();
            form.down('textfield[name=nomor_surat]').hide();
            form.down('datefield[name=tanggal_surat]').hide();
        }

        winForm.on('close', function(window){
            grid.getStore().reload();
        });

        winForm.show();


    },
    tambahMutasi: function(btn){
        var store = Ext.create('Admin.store.PesertaDidikBasic');
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        // store.load();
        var win = new Ext.window.Window({   
            height: 200,
            width: 500,
            modal:true,
            resizable: false,
            title: 'Tambah Mutasi Peserta Didik',
            autoScroll: true,
            frame: false,            
            border: true,
            layout: 'fit',
            closable: true,
            items: [{
                xtype:'form',
                // region:'center',
                frame: false,            
                border: false,  
                bodyPadding: '10 10 10 10',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%',
                    labelAlign: 'top'
                },
                items: [
                    {
                        allowBlank: false,
                        xtype:'combobox',
                        fieldLabel: 'Ketik Kata Kunci Nama atau NISN Peserta Didik',
                        name: 'keyword',
                        store:store,
                        hideTrigger:true,
                        queryMode: 'remote',
                        emptyText: 'Kata Kunci Nama PD',
                        displayField:'nama',
                        valueField:'peserta_didik_id',
                        listConfig: {
                            loadingText: 'Mencari...',
                            emptyText: 'Data Tidak Ditemukan',
                            // Custom rendering template for each item
                            getInnerTpl: function() {
                                return '<b>{nama}</b> (<i>{nisn}</i>)';
                            }
                        },
                        pageSize: 50
                    }
                ],
                buttons:[{
                    text:'Pilih',
                    handler:function(btn){
                        var id = btn.up('form').down('combobox').getValue();
                        console.log(id);

                        store.load({
                            params:{
                                peserta_didik_id: id
                            }
                        });

                        store.on('load', function(store){
                            // console.log(store);
                            var height = Ext.Element.getViewportHeight() - 100;
                            var width = Ext.Element.getViewportWidth() - 300;

                            var rec = store.getAt(0);

                            console.log(rec);

                            win.close();

                            var winForm = new Ext.window.Window({   
                                height: height,
                                width: width,
                                modal:true,
                                resizable: false,
                                title: 'Tambah Mutasi Peserta Didik a/n ' + rec.data.nama,
                                autoScroll: true,
                                resizable: true,
                                frame: false,            
                                border: true,
                                layout: 'fit',
                                closable: true,
                                items: [{
                                    xtype:'mutasipesertadidikform',
                                    // region:'center',
                                    frame: false,            
                                    border: false,  
                                    bodyPadding: '10 10 10 10',
                                    defaultType: 'textfield',
                                    record: rec,
                                    defaults: {
                                        anchor: '100%'
                                    },
                                }]
                            });

                            var form = winForm.down('mutasipesertadidikform');

                            // form.loadRecord(rec);

                            if(session.peran_id == 10){
                                form.down('combopengguna').hide();
                                form.down('textfield[name=nomor_surat]').hide();
                                form.down('datefield[name=tanggal_surat]').hide();
                            }


                            winForm.on('close', function(window){
                                grid.getStore().reload();
                            });

                            // if(session.peran_id == 10){
                            //     winForm.down('fieldset[itemId=pdfs]').down('sekolahcombo').disable();
                            // }

                            
                            winForm.show();
                        });
                    }
                }]
            }]
        });

        win.down('form').down('combobox').getStore().on('beforeload', function(store){

            if(session.peran_id == 10){
            
                Ext.apply(store.proxy.extraParams, {
                    sekolah_id: session.sekolah_id,
                    soft_delete: 0
                });
            
            }


        });


        win.show();
    },
    simpanMutasi: function(btn){
        // Ext.MessageBox.alert('tes', 'testing');

        var form = btn.up('form');
        var window = form.up('window');
        var values = form.getValues(false,false,false,true);

        console.log(values);

        var json = Ext.JSON.encode(values);

        console.log(json);


        // if(session.peran_id == 10){
        //     form.down('fieldset[itemId=pdfs]').down('sekolahcombo').enable();
        // }

        Ext.Ajax.request({
            url     : 'Administrasi/SimpanMutasi',
            method  : 'POST',
            params  : {
                data: json
            },
            failure : function(response, options){
                Ext.MessageBox.alert('Warning','Ada Kesalahan dalam pemrosesan data. Mohon dicek lagi');
            },
            success : function(response){
                var json = Ext.JSON.decode(response.responseText);
                
                if(json.success){
                    
                    Ext.MessageBox.hide();
                    Ext.MessageBox.alert('Berhasil', json.message);
                    window.close();

                } else if(json.success == false) {
                    Ext.MessageBox.alert('Error',json.message);
                }
            }
        });

        // console.log(form.getValues());

    }
    // TODO - Add control logic or remove if not needed
});
