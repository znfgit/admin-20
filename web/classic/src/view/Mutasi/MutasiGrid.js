Ext.define('Admin.view.Mutasi.MutasiGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.mutasigrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    controller: 'mutasi',
    listeners:{
        afterrender:'setupMutasiGrid'
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('Admin.store.Mutasi');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        // this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1); 
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Status',
            width:150,
            sortable: true,
            dataIndex: 'status_mutasi_id',
            hideable: false,
            hidden: false,
            renderer: function(v,p,r){
                if(v == 1){
                    var color = 'green';
                }else if(v == 2){
                    var color = 'purple';
                }else if(v == 3){
                    var color = 'blue';
                }else{
                    var color = 'red';
                }

                return '<b style="color:'+color+'">'+r.data.status_mutasi_id_str+'</b>'
            }
        },{
            header: 'Nomor Surat',
            width:150,
            sortable: true,
            dataIndex: 'nomor_surat',
            hideable: false,
            hidden: false
        },{
            header: 'Nama',
            width:150,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            hidden: false
        },{
            header: 'Sekolah Asal',
            width:150,
            sortable: true,
            dataIndex: 'sekolah_id_str',
            hideable: false,
            hidden: false
        },{
            header: 'NISN',
            width:150,
            sortable: true,
            dataIndex: 'NISN',
            hideable: false,
            hidden: false
        },{
            header: 'NIS',
            width:150,
            sortable: true,
            dataIndex: 'NIS',
            hideable: false,
            hidden: false
        },{
            header: 'Nama Orang Tua',
            width:150,
            sortable: true,
            dataIndex: 'nama_ayah',
            hideable: false,
            hidden: false
        },{
            header: 'Berangkat Ke',
            width:150,
            sortable: true,
            dataIndex: 'kode_wilayah_berangkat',
            hideable: false,
            hidden: false,
            renderer: function(v,p,r){
                return r.data.kode_wilayah_berangkat_str;
            }
        },{
            header: 'Maksud dan Tujuan',
            width:150,
            sortable: true,
            dataIndex: 'maksud_tujuan',
            hideable: false,
            hidden: false
        },{
            header: 'Alamat',
            width:150,
            sortable: true,
            dataIndex: 'alamat_jalan',
            hideable: false,
            hidden: false
        },{
            header: 'Tanggal Berangkat',
            width:150,
            sortable: true,
            dataIndex: 'tanggal_berangkat',
			hideable: false,
            hidden: false,
            renderer: function(v,p,r){
                if(v){
                    return v.substring(0,10);
                }
            }
        }];

        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                text:'Tambah',
                listeners:{
                    click:'tambahMutasi'
                },
                iconCls: 'x-fa fa-plus-circle',
                ui: 'soft-green'
            },{
                text:'Ubah',
                itemId: 'ubah',
                listeners:{
                    click:'ubahMutasi'
                },
                iconCls: 'x-fa fa-pencil-square',
                ui: 'soft-green'
            },{
                text:'Hapus',
                itemId: 'hapus',
                listeners:{
                    click:'hapusMutasi'
                },
                iconCls: 'x-fa fa-trash-o',
                ui: 'soft-green'
            }
            // ,{
            //     text:'Cetak',
            //     itemId: 'cetak',
            //     listeners:{
            //         click:'cetakMutasi'
            //     },
            //     iconCls: 'x-fa fa-print',
            //     ui: 'soft-green'
            // }
            ]
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});