Ext.define('Admin.view.progres.KualitasDataSekolah', {
    extend: 'Ext.container.Container',
    xtype: 'KualitasDataSekolahPanel',

    controller: 'Pengiriman',
    
    cls: 'kualitas-data-sekolah-container',
    
    layout: {
        type: 'border'
    },

    items: [
        {
            xtype: 'kualitasdatasekolahgrid',
            title: 'Kualitas Data Sekolah Dapodik',
            // responsiveCls: 'big-100',
            // minHeight:200,
            iconCls: 'right-icon x-fa fa-line-chart ',
            region: 'center',
            // html: 'pengiriman'
        },{
            xtype: 'deskripsikualitas',
            region:'east',
            width: 400,
            collapsed: true,
            collapsible: true,
            border: true,
            split:true
        }
        // ,{
        //     xtype: 'panel',
        //     title: 'Progres per Satuan Pendidikan di wilayah',
        //     iconCls: 'right-icon x-fa fa-send',
        //     region: 'south',
        //     minHeight: 200,
        //     split: false,
        //     collapsible: true
        // }
    ]
});
