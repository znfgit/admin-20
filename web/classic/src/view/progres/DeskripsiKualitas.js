Ext.define('Admin.view.progres.DeskripsiKualitas', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.deskripsikualitas',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

        this.title = 'Deskripsi Kualitas Pengumpulan Data';

    	this.items = [{
            region: 'center',
            xtype: 'tabpanel',
            itemId: 'DeskripsiKualitas',
            items:[{
                title: 'Identitas Satuan Pendidikan',
                html: '<div style="padding:10px">' +
                        'Daftar komponen isian identitas sekolah yang wajib diisi sebagai syarat data berkualitas'+
                        '<ol>'+
                            '<li>'+
                                'NPSN' +
                            '</li>'+
                            '<li>'+
                                'SK Pendirian Sekolah' +
                            '</li>'+
                            '<li>'+
                                'Tanggal SK Pendirian' +
                            '</li>'+
                            '<li>'+
                                'SK Izin Operasional' +
                            '</li>'+
                            '<li>'+
                                'Tanggal SK Izin Operasional' +
                            '</li>'+
                            '<li>'+
                                'No. Rekening' +
                            '</li>'+
                            '<li>'+
                                'Nama Bank' +
                            '</li>'+
                            '<li>'+
                                'Cabang/KCP/Unit' +
                            '</li>'+ 
                            '<li>'+ 
                                'Rekening Atas Nama' +
                            '</li>'+ 
                            '<li>'+ 
                                'Nama Kepala Sekolah' +
                            '</li>'+ 
                            '<li>'+ 
                                'Nomor Telepon Kepala Sekolah' +
                            '</li>'+ 
                            '<li>'+ 
                                'Email Kepala Sekolah' +
                            '</li>'+ 
                            '<li>'+ 
                                'Isian Bersedia Menerima BOS' +
                            '</li>'+ 
                            '<li>'+ 
                                'Waktu Penyelenggaraan' +
                            '</li>'+ 
                            '<li>'+ 
                                'Sumber Listrik' +
                            '</li>'+ 
                            '<li>'+ 
                                'Daya Listrik' +
                            '</li>'+ 
                            '<li>'+ 
                                'Sertifikasi ISO' +
                            '</li>'+ 
                            '<li>'+ 
                                'Akses Internet' +
                            '</li>'+ 
                            
                        '</ol>'+
                        '</div>'
            },{
                title: 'PTK',
                html: '<div style="padding:10px">' +
                        'Daftar komponen isian PTK yang wajib diisi sebagai syarat data berkualitas'+
                        '<ol>'+
                            '<li>'+
                                'SK CPNS (bagi PNS)' +
                            '</li>'+
                            '<li>'+
                                'Tanggal CPNS (bagi PNS)' +
                            '</li>'+
                            '<li>'+
                                'TMT PNS (bagi PNS)' +
                            '</li>'+
                            '<li>'+
                                'Pangkat Golongan (bagi PNS)' +
                            '</li>'+
                            '<li>'+
                                'Nama suami / istri' +
                            '</li>'+
                            '<li>'+
                                'Pekerjaan suami / istri' +
                            '</li>'+
                            '<li>'+
                                'SK Pengangkatan' +
                            '</li>'+
                            '<li>'+
                                'TMT Pengangkatan' +
                            '</li>'+ 
                            '<li>'+ 
                                'Nama ibu kandung' +
                            '</li>'+ 
                            '<li>'+ 
                                'NIK' +
                            '</li>'+ 
                            '<li>'+ 
                                'NUPTK' +
                            '</li>'+ 
                            '<li>'+ 
                                'Desa/Kelurahan' +
                            '</li>'+ 
                            '<li>'+ 
                                'Nomor HP' +
                            '</li>'+ 
                            '<li>'+ 
                                'Email' +
                            '</li>'+ 
                            '<li>'+ 
                                'NPWP' +
                            '</li>'+ 
                            
                            
                        '</ol>'+
                        '</div>'
            },{
                title: 'PD',
                html: '<div style="padding:10px">' +
                        'Daftar komponen isian Peserta Didik yang wajib diisi sebagai syarat data berkualitas'+
                        '<ol>'+
                            '<li>'+
                                'NISN' +
                            '</li>'+
                            '<li>'+
                                'NIK' +
                            '</li>'+
                            '<li>'+
                                'Desa/Kelurahan' +
                            '</li>'+
                            '<li>'+
                                'Kode pos' +
                            '</li>'+
                            '<li>'+
                                'Tempat Tinggal' +
                            '</li>'+
                            '<li>'+
                                'Alat Transportasi' +
                            '</li>'+
                            '<li>'+
                                'Nomor HP' +
                            '</li>'+
                            '<li>'+
                                'Email' +
                            '</li>'+
                            '<li>'+
                                'No KPS (bila menerima)' +
                            '</li>'+
                            '<li>'+
                                'Nama Ayah' +
                            '</li>'+
                            '<li>'+
                                'Tahun Lahir Ayah' +
                            '</li>'+
                            '<li>'+
                                'Jenjang Pendidikan Ayah' +
                            '</li>'+
                            '<li>'+
                                'Pekerjaan Ayah' +
                            '</li>'+
                            '<li>'+
                                'Penghasilan Ayah' +
                            '</li>'+
                            '<li>'+
                                'Nama Ibu Kandung' +
                            '</li>'+
                            '<li>'+
                                'Tahun Lahir Ibu' +
                            '</li>'+
                            '<li>'+
                                'Jenjang Pendidikan Ibu' +
                            '</li>'+
                            '<li>'+
                                'Pekerjaan Ibu' +
                            '</li>'+
                            '<li>'+
                                'Penghasilan Ibu' +
                            '</li>'+
                            '<li>'+
                                'NIPD / NIS' +
                            '</li>'+
                            '<li>'+
                                'No. SKHUN' +
                            '</li>'+
                        '</ol>'+
                        '</div>'
            },{
                title: 'Prasarana',
                html: '<div style="padding:10px">' +
                        'Daftar komponen isian Prasarana yang wajib diisi sebagai syarat data berkualitas'+
                        '<ol>'+
                            '<li>'+
                                'Panjang' +
                            '</li>'+
                            '<li>'+
                                'Lebar' +
                            '</li>'+
                            '<li>'+
                                'Tingkat Kerusakan' +
                            '</li>'+
                            '<li>'+
                                'Kepemilikan Sarana' +
                            '</li>'+
                            '<li>'+
                                'Kepemilikan Buku & Alat' +
                            '</li>'+
                        '</ol>'+
                        '</div>'
            }]
        }]

    	this.callParent(arguments);
    }
});