Ext.define('Admin.view.progres.Pencapaian', {
    extend: 'Ext.container.Container',
    xtype: 'PencapaianPanel',

    controller: 'Pencapaian',
    
    cls: 'Pencapaian-container',
    
    layout: {
        type: 'border'
    },

    items: [
        {
            xtype: 'PencapaianGrid',
            title: 'Pencapaian Kualitas Data Bulanan',
            // responsiveCls: 'big-100',
            // minHeight:200,
            iconCls: 'right-icon x-fa fa-line-chart',
            region: 'center',
            // html: 'pengiriman'
        }
        // ,{
        //     xtype: 'panel',
        //     title: 'Progres per Satuan Pendidikan di wilayah',
        //     iconCls: 'right-icon x-fa fa-send',
        //     region: 'south',
        //     minHeight: 200,
        //     split: false,
        //     collapsible: true
        // }
    ]
});
