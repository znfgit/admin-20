Ext.define('Admin.view.progres.PencapaianSp', {
    extend: 'Ext.container.Container',
    xtype: 'PencapaianSpPanel',

    controller: 'Pencapaian',
    
    cls: 'PencapaianSp-container',
    
    layout: {
        type: 'border'
    },

    items: [
        {
            xtype: 'PencapaianSpGrid',
            title: 'Pencapaian Kualitas Data Bulanan Per Satuan Pendidikan',
            // responsiveCls: 'big-100',
            // minHeight:200,
            iconCls: 'right-icon x-fa fa-line-chart',
            region: 'center',
            // html: 'pengiriman'
        }
        // ,{
        //     xtype: 'panel',
        //     title: 'Progres per Satuan Pendidikan di wilayah',
        //     iconCls: 'right-icon x-fa fa-send',
        //     region: 'south',
        //     minHeight: 200,
        //     split: false,
        //     collapsible: true
        // }
    ]
});
