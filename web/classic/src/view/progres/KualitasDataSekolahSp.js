Ext.define('Admin.view.progres.KualitasDataSekolahSp', {
    extend: 'Ext.container.Container',
    xtype: 'KualitasDataSekolahSpPanel',

    controller: 'Pengiriman',
    
    cls: 'kualitas-data-sekolah-sp-container',
    
    layout: {
        type: 'border'
    },

    items: [
        {
            xtype: 'kualitasdatasekolahspgrid',
            title: 'Kualitas Data Sekolah Dapodik Per Satuan Pendidikan',
            // responsiveCls: 'big-100',
            // minHeight:200,
            iconCls: 'right-icon x-fa fa-line-chart ',
            region: 'center',
            // html: 'pengiriman'
        }
        // ,{
        //     xtype: 'panel',
        //     title: 'Progres per Satuan Pendidikan di wilayah',
        //     iconCls: 'right-icon x-fa fa-send',
        //     region: 'south',
        //     minHeight: 200,
        //     split: false,
        //     collapsible: true
        // }
    ]
});
