Ext.define('Admin.view.progres.PengirimanSpGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pengirimanspgrid',
    title: '',
    controller: 'Pengiriman',
    // scrollable: true,
    columnLines: true,
    listeners:{
        afterrender: 'setupPengirimanSpGrid'
    },
    createStore: function() {
        return Ext.create('Admin.store.PengirimanSp');
    },
    createEditing: function() {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: ['-']
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }      
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        // this.getSelectionModel().setSelectionMode('SINGLE');
        
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function(rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });
        
        var editing = this.createEditing();

        // this.rowEditing = editing;
        // this.plugins = [editing];

        
        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 144,
            sortable: true,
            dataIndex: 'sekolah_id',
            hideable: true,
            hidden: true
        },{
            header: 'Nama',
            tooltip: 'Nama',
            width: 200,
            sortable: true,
            locked: false,
            dataIndex: 'nama',
            hideable: true,
            renderer: function(v,p,r){
                return '<a style="color:#228ab7;text-decoration:none" href="#ProfilSatuanPendidikan/'+r.data.sekolah_id+'">'+v+'</a>';
            }
        }
        ,{
            header: 'NPSN',
            dataIndex: 'npsn',
            width: 90,
            align: 'right'
        }
        ,{
            header: 'Bentuk',
            dataIndex: 'bentuk',
            width: 90,
            align: 'right'
        }
        ,{
            header: 'Jumlah<br>Pengiriman',
            dataIndex: 'jumlah_kirim',
            width: 120,
            align: 'right'
        }
        ,{
            header: 'Sinkron<br>Terakhir',
            dataIndex: 'sinkron_terakhir',
            width: 150,
            align: 'right'
        }
        ,{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }];
        
        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();
        
        this.callParent(arguments);
    }
});