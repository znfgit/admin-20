Ext.define('Admin.view.progres.Pengiriman', {
    extend: 'Ext.container.Container',
    xtype: 'pengirimanPanel',

    controller: 'Pengiriman',
    
    cls: 'pengiriman-container',
    
    layout: {
        type: 'border'
    },

    items: [
        {
            xtype: 'pengirimangrid',
            title: 'Sinkronisasi Dapodik',
            // responsiveCls: 'big-100',
            // minHeight:200,
            iconCls: 'right-icon x-fa fa-send',
            region: 'center',
            // html: 'pengiriman'
        }
        // ,{
        //     xtype: 'panel',
        //     title: 'Progres per Satuan Pendidikan di wilayah',
        //     iconCls: 'right-icon x-fa fa-send',
        //     region: 'south',
        //     minHeight: 200,
        //     split: false,
        //     collapsible: true
        // }
    ]
});
