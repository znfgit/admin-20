Ext.define('Admin.view.progres.KualitasDataIdentitasSp', {
    extend: 'Ext.container.Container',
    xtype: 'KualitasDataIdentitasSpPanel',

    controller: 'Pengiriman',
    
    cls: 'kualitas-data-identitas-container',
    
    layout: {
        type: 'border'
    },

    items: [
        {
            xtype: 'kualitasdataidentitasspgrid',
            // xtype: 'panel',
            title: 'Kualitas Data Identitas Sekolah Per Satuan Pendidikan',
            // responsiveCls: 'big-100',
            // minHeight:200,
            iconCls: 'right-icon x-fa fa-line-chart ',
            region: 'center',
            // html: 'pengiriman'
        }
        // ,{
        //     xtype: 'panel',
        //     title: 'Progres per Satuan Pendidikan di wilayah',
        //     iconCls: 'right-icon x-fa fa-send',
        //     region: 'south',
        //     minHeight: 200,
        //     split: false,
        //     collapsible: true
        // }
    ]
});
