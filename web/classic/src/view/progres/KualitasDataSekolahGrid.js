Ext.define('Admin.view.progres.KualitasDataSekolahGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.kualitasdatasekolahgrid',
    title: '',
    controller: 'Pengiriman',
    // scrollable: true,
    columnLines: true,
    listeners:{
        afterrender: 'setupKualitasDataSekolahGrid',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                window.location="#KualitasData/"+record.data.id_level_wilayah+"/"+record.data.kode_wilayah;
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.KualitasDataSekolah');
    },
    createEditing: function() {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: ['-',{
                iconCls: 'x-fa fa-arrow-circle-left',
                ui: 'soft-blue',
                itemId: 'kembaliButton',
                hidden: true,
                listeners:{
                    click: 'kembali'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }      
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        // this.getSelectionModel().setSelectionMode('SINGLE');
        
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function(rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });
        
        var editing = this.createEditing();

        // this.rowEditing = editing;
        // this.plugins = [editing];

        
        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 144,
            sortable: true,
            dataIndex: 'kode_wilayah',
            hideable: true,
            hidden: true
        },{
            header: 'Wilayah',
            tooltip: 'Nama Wilayah',
            width: 200,
            sortable: true,
            locked: false,
            dataIndex: 'wilayah',
            hideable: true,
            renderer: function(v,p,r){
                return '<a style="color:#228ab7;text-decoration:none" href="#KualitasDataSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah.trim()+'">'+v+'</a>';
            }
        },{
            header: 'Rata-rata<br>Kualitas<br>Kelengkapan Data',
            width: 150,
            sortable: true,
            dataIndex: 'total_valid',
            hideable: false,
            align:'right',
            renderer:function(v,p,r){
                return v.toFixed(2);
            }
        },{
            text: 'Persentase Indikator Kualitas Kelengkapan Data',
            itemId:'kualitas_group', 
            columns: [{
                header: 'Identitas<br>Sekolah',
                width: 100,
                sortable: true,
                dataIndex: 'identitas_valid',
                hideable: false,
                align:'right',
                // renderer:function(v,p,r){
                //     return v.toFixed(2);
                // }
                renderer: function(v,p,r){
                    var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

                    if(session.session == true){

                        if(r.data.id_level_wilayah != 3){
                            return '<a style="color:#228ab7;text-decoration:none" href="#KualitasDataIdentitas/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah.trim()+'">'+v.toFixed(2)+'</a>';
                        }else{
                            return '<a style="color:#228ab7;text-decoration:none" href="#KualitasDataIdentitasSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah.trim()+'">'+v.toFixed(2)+'</a>';
                            // return v.toFixed(2);
                        }
                    }else{
                        return v.toFixed(2);
                    }


                }
                
            },{
                header: 'Guru &<br>Tendik',
                width: 100,
                sortable: true,
                dataIndex: 'ptk_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Peserta<br>Didik',
                width: 100,
                sortable: true,
                dataIndex: 'pd_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Sarpras',
                width: 100,
                sortable: true,
                dataIndex: 'prasarana_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            }]
        },{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }];
        
        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();
        
        this.callParent(arguments);
    }
});