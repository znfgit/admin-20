Ext.define('Admin.view.progres.PencapaianController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.Pencapaian',

    // TODO - Add control logic or remove if not needed
    setupPencapaianGrid: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        if(session.id_level_wilayah == 1){
            grid.setTitle('Pencapaian Kualitas Data Bulanan di '+session.propinsi);
        }else if(session.id_level_wilayah == 2){
            grid.setTitle('Pencapaian Kualitas Data Bulanan di '+session.kabupaten_kota); 
        }

        grid.getStore().on('beforeload', function(store){
            var hash = window.location.hash.substr(1);
            var hashArr = hash.split('/');

            if(typeof hashArr[1] === 'undefined' || typeof hashArr[2] === 'undefined'){
                var kode_wilayah = session.kode_wilayah;
                var id_level_wilayah = session.id_level_wilayah;
            }else{
                var kode_wilayah = hashArr[2];
                var id_level_wilayah = hashArr[1];
            }

            grid.id_level_wilayah = id_level_wilayah;
            grid.kode_wilayah = kode_wilayah;

            Ext.apply(store.proxy.extraParams, {
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah
            });
        });

        grid.down('toolbar').add({
            iconCls: 'x-fa fa-arrow-circle-left',
            name: 'kembaliButton',
            listeners:{
                click: 'kembali'
            }
        },'-','->',{
            xtype: 'button',
            text: 'Unduh Xls',
            ui: 'soft-green',
            iconCls: 'x-fa fa-file-excel-o',
            paramClass: 'Progres\\Pencapaian',
            paramMethod: 'Pencapaian',
            listeners:{
                click: 'tombolUnduhExcel'
            }
        },'-',{
            iconCls: 'fa fa-refresh',
            listeners:{
                click: 'refresh'
            }
        });

        grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            console.log(rec);

            if(rec){
                if(session.id_level_wilayah == '0'){

                    if(rec.data.id_level_wilayah == 1){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }

                }else if(session.id_level_wilayah == '1'){

                    if(rec.data.id_level_wilayah == 2){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }                

                }else if(session.id_level_wilayah == '2'){

                    if(rec.data.id_level_wilayah == 3){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }

                }
            }

            var data = store.data;

            var rata_rata_valid_bulan_1 = 0;
            var rata_rata_valid_bulan_2 = 0;
            var rata_rata_valid_bulan_3 = 0;
            var rata_rata_valid_bulan_4 = 0;
            var rata_rata_valid_bulan_5 = 0;
            var rata_rata_valid_bulan_6 = 0;
            var rata_rata_valid_bulan_7 = 0;
            var rata_rata_valid_bulan_8 = 0;
            var rata_rata_valid_bulan_9 = 0;
            var rata_rata_valid_bulan_10 = 0;
            var rata_rata_valid_bulan_11 = 0;
            var rata_rata_valid_bulan_12 = 0;

            data.each(function(record){
                var dataRecord = record.data;

                rata_rata_valid_bulan_1 = rata_rata_valid_bulan_1+dataRecord.rata_rata_valid_bulan_1;
                rata_rata_valid_bulan_2 = rata_rata_valid_bulan_2+dataRecord.rata_rata_valid_bulan_2;
                rata_rata_valid_bulan_3 = rata_rata_valid_bulan_3+dataRecord.rata_rata_valid_bulan_3;
                rata_rata_valid_bulan_4 = rata_rata_valid_bulan_4+dataRecord.rata_rata_valid_bulan_4;
                rata_rata_valid_bulan_5 = rata_rata_valid_bulan_5+dataRecord.rata_rata_valid_bulan_5;
                rata_rata_valid_bulan_6 = rata_rata_valid_bulan_6+dataRecord.rata_rata_valid_bulan_6;
                rata_rata_valid_bulan_7 = rata_rata_valid_bulan_7+dataRecord.rata_rata_valid_bulan_7;
                rata_rata_valid_bulan_8 = rata_rata_valid_bulan_8+dataRecord.rata_rata_valid_bulan_8;
                rata_rata_valid_bulan_9 = rata_rata_valid_bulan_9+dataRecord.rata_rata_valid_bulan_9;
                rata_rata_valid_bulan_10 = rata_rata_valid_bulan_10+dataRecord.rata_rata_valid_bulan_10;
                rata_rata_valid_bulan_11 = rata_rata_valid_bulan_11+dataRecord.rata_rata_valid_bulan_11;
                rata_rata_valid_bulan_12 = rata_rata_valid_bulan_12+dataRecord.rata_rata_valid_bulan_12;
            });

            var recordConfig = {
                mst_kode_wilayah: null,
                kode_wilayah: null,
                id_level_wilayah: null,
                nama : '<b>Total<b>',
                rata_rata_valid_bulan_1: (rata_rata_valid_bulan_1/data.length).toFixed(2),
                rata_rata_valid_bulan_2: (rata_rata_valid_bulan_2/data.length).toFixed(2),
                rata_rata_valid_bulan_3: (rata_rata_valid_bulan_3/data.length).toFixed(2),
                rata_rata_valid_bulan_4: (rata_rata_valid_bulan_4/data.length).toFixed(2),
                rata_rata_valid_bulan_5: (rata_rata_valid_bulan_5/data.length).toFixed(2),
                rata_rata_valid_bulan_6: (rata_rata_valid_bulan_6/data.length).toFixed(2),
                rata_rata_valid_bulan_7: (rata_rata_valid_bulan_7/data.length).toFixed(2),
                rata_rata_valid_bulan_8: (rata_rata_valid_bulan_8/data.length).toFixed(2),
                rata_rata_valid_bulan_9: (rata_rata_valid_bulan_9/data.length).toFixed(2),
                rata_rata_valid_bulan_10: (rata_rata_valid_bulan_10/data.length).toFixed(2),
                rata_rata_valid_bulan_11: (rata_rata_valid_bulan_11/data.length).toFixed(2),
                rata_rata_valid_bulan_12: (rata_rata_valid_bulan_12/data.length).toFixed(2)
            };

            var r = new Admin.model.Pencapaian(recordConfig);

            // console.log(r);

            grid.store.add(r);
        });

        grid.getStore().load();
    },
    setupPencapaianSpGrid: function(grid){

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        if(session.id_level_wilayah == 1){
            grid.setTitle('Pencapaian Kualitas Data Bulanan Per Satuan Pendidikan di '+session.propinsi);
        }else if(session.id_level_wilayah == 2){
            grid.setTitle('Pencapaian Kualitas Data Bulanan Per Satuan Pendidikan di '+session.kabupaten_kota); 
        }

        grid.down('toolbar').add({
            iconCls: 'x-fa fa-arrow-circle-left',
            name: 'kembaliButton',
            listeners:{
                click: function(btn){
                    window.location.href="#PencapaianKualitasBulanan";
                }
            }
        },{
            xtype: 'button',
            text: 'Unduh Xls',
            ui: 'soft-green',
            iconCls: 'x-fa fa-file-excel-o',
            paramClass: 'Progres\\Pencapaian',
            paramMethod: 'PencapaianSp',
            listeners:{
                click: 'tombolUnduhExcel'
            }
        });

        grid.getStore().on('beforeload', function(store){
            var hash = window.location.hash.substr(1);
            var hashArr = hash.split('/');

            if(typeof hashArr[1] === 'undefined' || typeof hashArr[2] === 'undefined'){
                var kode_wilayah = session.kode_wilayah;
                var id_level_wilayah = session.id_level_wilayah;
            }else{
                var kode_wilayah = hashArr[2];
                var id_level_wilayah = hashArr[1];
            }

            grid.id_level_wilayah = id_level_wilayah;
            grid.kode_wilayah = kode_wilayah;

            Ext.apply(store.proxy.extraParams, {
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah
            });
        });

        grid.getStore().on('load', function(store){
            var rec = store.getAt(0);

            if(rec){
                grid.setTitle('Pencapaian Kualitas Data Bulanan Per Satuan Pendidikan di '+rec.data.wilayah);
            }
        });

        grid.getStore().load();
    }
});
