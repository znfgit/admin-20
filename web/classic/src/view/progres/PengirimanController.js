Ext.define('Admin.view.progres.PengirimanController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.Pengiriman',

    // TODO - Add control logic or remove if not needed
    setupKualitasDataIdentitasSpGrid:function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.down('toolbar').add({
            xtype: 'textfield',
            emptyText: 'Cari Nama Wilayah ...',
            width: 200,
            enableKeyEvents: true,
            listeners: {
                keypress: function (field, e) {
                    if (e.getKey() == e.ENTER){

                        // console.log('tes');

                        grid.getStore().load();
                    }
                }
            }
        },'->',{
            text: 'Unduh Xlsx',
            iconCls: 'x-fa fa-download',
            listeners:{
                click: function(btn){

                    window.location.href="Excel/generate?kode_wilayah="+grid.kode_wilayah+"&id_level_wilayah="+grid.id_level_wilayah+"&class=progres\\KualitasDataSekolah&method=KualitasDataIdentitasSp&tipe=1";
                }
            }
        },'-',{
            iconCls: 'fa fa-refresh',
            listeners:{
                click: 'refresh'
            }
        });
        
        grid.getStore().on('beforeload', function(store){
            var hash = window.location.hash.substr(1);
            var hashArr = hash.split('/');

            if(typeof hashArr[1] === 'undefined' || typeof hashArr[2] === 'undefined'){
                var kode_wilayah = session.kode_wilayah;
                var id_level_wilayah = session.id_level_wilayah;
            }else{
                var kode_wilayah = hashArr[2];
                var id_level_wilayah = hashArr[1];
            }

            grid.id_level_wilayah = id_level_wilayah;
            grid.kode_wilayah = kode_wilayah;

            var textfield = grid.down('toolbar').down('textfield');

            Ext.apply(store.proxy.extraParams, {
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah,
                keyword: textfield.getValue()
            });

            var storeMst = Ext.create('Admin.store.MstWilayah');

            storeMst.load({
                params: {
                    kode_wilayah: kode_wilayah
                }
            });

            storeMst.on('load', function(storeMst){
                var rec = storeMst.getAt(0);

                if(rec){
                    grid.setTitle('Kualitas Data Identitas Per Satuan Pendidikan di '+rec.data.nama);
                }
            });
        });

        grid.getStore().load();
    },
    setupKualitasDataIdentitasGrid:function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.down('toolbar').add({
            xtype: 'textfield',
            emptyText: 'Cari Nama Wilayah ...',
            width: 200,
            enableKeyEvents: true,
            listeners: {
                keypress: function (field, e) {
                    if (e.getKey() == e.ENTER){

                        // console.log('tes');

                        grid.getStore().load();
                    }
                }
            }
        },'->',{
            text: 'Unduh Xlsx',
            iconCls: 'x-fa fa-download',
            listeners:{
                click: function(btn){

                    window.location.href="Excel/generate?kode_wilayah="+grid.kode_wilayah+"&id_level_wilayah="+grid.id_level_wilayah+"&class=progres\\KualitasDataSekolah&method=KualitasDataIdentitas&tipe=1";
                }
            }
        },'-',{
            iconCls: 'fa fa-refresh',
            listeners:{
                click: 'refresh'
            }
        });
        
        grid.getStore().on('beforeload', function(store){
            var hash = window.location.hash.substr(1);
            var hashArr = hash.split('/');

            if(typeof hashArr[1] === 'undefined' || typeof hashArr[2] === 'undefined'){
                var kode_wilayah = session.kode_wilayah;
                var id_level_wilayah = session.id_level_wilayah;
            }else{
                var kode_wilayah = hashArr[2];
                var id_level_wilayah = hashArr[1];
            }

            grid.id_level_wilayah = id_level_wilayah;
            grid.kode_wilayah = kode_wilayah;

            var textfield = grid.down('toolbar').down('textfield');

            Ext.apply(store.proxy.extraParams, {
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah,
                keyword: textfield.getValue()
            });

            var storeMst = Ext.create('Admin.store.MstWilayah');

            storeMst.load({
                params: {
                    kode_wilayah: kode_wilayah
                }
            });

            storeMst.on('load', function(storeMst){
                var rec = storeMst.getAt(0);

                if(rec){
                    grid.setTitle('Kualitas Data Identitas Sekolah di '+rec.data.nama);
                }
            });
        });

        grid.getStore().load();
    },

    setupKualitasDataSekolahSpGrid: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.down('toolbar').add({
            xtype: 'textfield',
            emptyText: 'Cari Nama Wilayah ...',
            width: 200,
            enableKeyEvents: true,
            listeners: {
                keypress: function (field, e) {
                    if (e.getKey() == e.ENTER){

                        // console.log('tes');

                        grid.getStore().load();
                    }
                }
            }
        },'->',{
            text: 'Unduh Xlsx',
            iconCls: 'x-fa fa-download',
            listeners:{
                click: function(btn){

                    window.location.href="Excel/generate?kode_wilayah="+grid.kode_wilayah+"&id_level_wilayah="+grid.id_level_wilayah+"&class=progres\\KualitasDataSekolah&method=KualitasDataSekolahSp&tipe=1";
                }
            }
        },'-',{
            iconCls: 'fa fa-refresh',
            listeners:{
                click: 'refresh'
            }
        });
        
        grid.getStore().on('beforeload', function(store){
            var hash = window.location.hash.substr(1);
            var hashArr = hash.split('/');

            if(typeof hashArr[1] === 'undefined' || typeof hashArr[2] === 'undefined'){
                var kode_wilayah = session.kode_wilayah;
                var id_level_wilayah = session.id_level_wilayah;
            }else{
                var kode_wilayah = hashArr[2];
                var id_level_wilayah = hashArr[1];
            }

            grid.id_level_wilayah = id_level_wilayah;
            grid.kode_wilayah = kode_wilayah;

            var textfield = grid.down('toolbar').down('textfield');

            Ext.apply(store.proxy.extraParams, {
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah,
                keyword: textfield.getValue()
            });

            var storeMst = Ext.create('Admin.store.MstWilayah');

            storeMst.load({
                params: {
                    kode_wilayah: kode_wilayah
                }
            });

            storeMst.on('load', function(storeMst){
                var rec = storeMst.getAt(0);

                if(rec){
                    grid.setTitle('Kualitas Data Sekolah Dapodik Per Satuan Pendidikan di '+rec.data.nama);
                }
            });
        });

        grid.getStore().load();
    },
    setupKualitasDataSekolahGrid: function(grid){
        // console.log('testing');
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.down('toolbar').add({
            xtype: 'textfield',
            emptyText: 'Cari Nama Wilayah ...',
            width: 200,
            enableKeyEvents: true,
            listeners: {
                keypress: function (field, e) {
                    if (e.getKey() == e.ENTER){

                        // console.log('tes');

                        grid.getStore().load();
                    }
                }
            }
        },'->',{
            text: 'Unduh Xlsx',
            iconCls: 'x-fa fa-download',
            listeners:{
                click: function(btn){

                    window.location.href="Excel/generate?kode_wilayah="+grid.kode_wilayah+"&id_level_wilayah="+grid.id_level_wilayah+"&class=progres\\KualitasDataSekolah&method=KualitasDataSekolah&tipe=1";
                }
            }
        },'-',{
            iconCls: 'fa fa-refresh',
            listeners:{
                click: 'refresh'
            }
        });
        
        grid.getStore().on('beforeload', function(store){
            var hash = window.location.hash.substr(1);
            var hashArr = hash.split('/');

            if(typeof hashArr[1] === 'undefined' || typeof hashArr[2] === 'undefined'){
                var kode_wilayah = session.kode_wilayah;
                var id_level_wilayah = session.id_level_wilayah;
            }else{
                var kode_wilayah = hashArr[2];
                var id_level_wilayah = hashArr[1];
            }

            grid.id_level_wilayah = id_level_wilayah;
            grid.kode_wilayah = kode_wilayah;

            var textfield = grid.down('toolbar').down('textfield');

            Ext.apply(store.proxy.extraParams, {
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah,
                keyword: textfield.getValue()
            });

            var storeMst = Ext.create('Admin.store.MstWilayah');

            storeMst.load({
                params: {
                    kode_wilayah: kode_wilayah
                }
            });

            storeMst.on('load', function(storeMst){
                var rec = storeMst.getAt(0);

                if(rec){
                    grid.setTitle('Kualitas Data Sekolah Dapodik di '+rec.data.nama);
                }
            });
        });

        grid.getStore().load();

        grid.getStore().on('load', function(store){
            var rec = store.getAt(0);

            if(rec){
                console.log(parseInt(rec.data.id_level_wilayah)-1);
                console.log(session.id_level_wilayah);

                if(rec.data.id_level_wilayah != 1 && (parseInt(rec.data.id_level_wilayah)-1) != session.id_level_wilayah){

                    grid.down('toolbar').down('button[itemId=kembaliButton]').show();
                }else{
                    grid.down('toolbar').down('button[itemId=kembaliButton]').hide();
                }
            }
        });

    },

    setupPengirimanGrid: function(grid){

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        if(session.id_level_wilayah == 1){
            grid.setTitle('Sinkronisasi Dapodik di '+session.propinsi);
        }else if(session.id_level_wilayah == 2){
            grid.setTitle('Sinkronisasi Dapodik di '+session.kabupaten_kota); 
        }

        // console.log(session.tingkat);

        if (session.tingkat == 'DIKMEN') {

            // console.log('tingkat');

            // grid.columns[1].hide();
            
            //kolom total
            grid.columns[2].hide();
            grid.columns[3].hide();

            //kolom Sd-SMP-SLB
            grid.columns[4].hide();
            grid.columns[5].hide();
            grid.columns[6].hide();
            grid.columns[7].hide();
            grid.columns[8].hide();
            grid.columns[9].hide();
            grid.columns[16].hide();
            grid.columns[17].hide();
            grid.columns[18].hide();

            if(session.bentuk_pendidikan == 'SMA'){
                
                grid.columns[13].hide();
                grid.columns[14].hide();
                grid.columns[15].hide();

                grid.headerCt.insert(7,{
                    header: '%',
                    dataIndex: 'persen_sma',
                    width: 80,
                    align: 'right',
                    renderer: function(v,p,r){
                        // return ((r.data.kirim_sma/r.data.sma*100)*1).toFixed(2);
                        return v.toFixed(2);
                    }
                });

            }
        };


        grid.getStore().on('beforeload', function(store){
            var hash = window.location.hash.substr(1);
            var hashArr = hash.split('/');

            if(typeof hashArr[1] === 'undefined' || typeof hashArr[2] === 'undefined'){
                var kode_wilayah = session.kode_wilayah;
                var id_level_wilayah = session.id_level_wilayah;
            }else{
                var kode_wilayah = hashArr[2];
                var id_level_wilayah = hashArr[1];
            }

            grid.id_level_wilayah = id_level_wilayah;
            grid.kode_wilayah = kode_wilayah;

            Ext.apply(store.proxy.extraParams, {
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah
            });
        });
         



    	grid.down('toolbar').down('button[action=add]').hide();
    	grid.down('toolbar').down('button[action=edit]').hide();
    	grid.down('toolbar').down('button[action=save]').hide();
    	grid.down('toolbar').down('button[action=delete]').hide();
    	
    	grid.down('toolbar').add({
    		iconCls: 'x-fa fa-arrow-circle-left',
    		name: 'kembaliButton',
            listeners:{
                click: 'kembali'
            }
    	},'-','->',{
    		text: 'Unduh Xlsx',
    		iconCls: 'x-fa fa-download',
            listeners:{
                click: function(btn){

                    window.location.href="Excel/generate?kode_wilayah="+grid.kode_wilayah+"&id_level_wilayah="+grid.id_level_wilayah+"&class=progres\\Pengiriman&method=Pengiriman&tipe=1";
                }
            }
    	},'-',{
    		iconCls: 'fa fa-refresh',
    		listeners:{
    			click: 'refresh'
    		}
    	});

    	grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            // console.log(rec);

            if(rec){
                if(session.id_level_wilayah == '0'){

                    if(rec.data.id_level_wilayah == 1){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }

                }else if(session.id_level_wilayah == '1'){

                    if(rec.data.id_level_wilayah == 2){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }                

                }else if(session.id_level_wilayah == '2'){

                    if(rec.data.id_level_wilayah == 3){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }

                }
            }
		});

    	grid.getStore().load();

        grid.getStore().sort('persen_sma', 'DESC');
    },
    setupPengirimanGridBefore: function(grid){

    },
    setupPengirimanSpGrid: function(grid){

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        if(session.id_level_wilayah == 1){
            grid.setTitle('Sinkronisasi Dapodik di '+session.propinsi);
        }else if(session.id_level_wilayah == 2){
            grid.setTitle('Sinkronisasi Dapodik di '+session.kabupaten_kota); 
        }

        grid.down('toolbar').add({
            iconCls: 'x-fa fa-arrow-circle-left',
            name: 'kembaliButton',
            listeners:{
                click: function(btn){
                    window.location.href="#Pengiriman";
                }
            }
        },{
            xtype: 'checkbox',
            boxLabel: 'Hanya tampilkan sekolah belum sinkron',
            name: 'sekolah-belum-sinkron',
            inputValue: '1',
            listeners:{
                change:function(checkbox,newValue,oldValue){
                    // console.log(newValue);
                    grid.getStore().load();
                }
            }
        });

        grid.getStore().on('beforeload', function(store){
            var hash = window.location.hash.substr(1);
            var hashArr = hash.split('/');

            if(typeof hashArr[1] === 'undefined' || typeof hashArr[2] === 'undefined'){
                var kode_wilayah = session.kode_wilayah;
                var id_level_wilayah = session.id_level_wilayah;
            }else{
                var kode_wilayah = hashArr[2];
                var id_level_wilayah = hashArr[1];
            }

            grid.id_level_wilayah = id_level_wilayah;
            grid.kode_wilayah = kode_wilayah;

            Ext.apply(store.proxy.extraParams, {
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah,
                belum_sinkron_saja: grid.down('toolbar').down('checkbox').getValue()
            });
        });


        grid.getStore().on('load', function(store){
            var rec = store.getAt(0);

            if(rec){
                grid.setTitle('Sinkronisasi Dapodik per Sekolah di '+rec.data.wilayah);
            }
        });

        grid.getStore().load();
    },
    setupPengirimanSpGridBefore: function(grid){

    },
});
