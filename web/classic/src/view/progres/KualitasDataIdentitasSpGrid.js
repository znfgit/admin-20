Ext.define('Admin.view.progres.KualitasDataIdentitasSpGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.kualitasdataidentitasspgrid',
    title: '',
    controller: 'Pengiriman',
    // scrollable: true,
    columnLines: true,
    listeners:{
        afterrender: 'setupKualitasDataIdentitasSpGrid'
    },
    createStore: function() {
        return Ext.create('Admin.store.KualitasDataIdentitasSp');
    },
    createEditing: function() {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: ['-',{
                iconCls: 'x-fa fa-arrow-circle-left',
                ui: 'soft-blue',
                itemId: 'kembaliButton',
                listeners:{
                    click: function(btn){
                        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
                        var hash = window.location.hash.substr(1);
                        var hashArr = hash.split('/');

                        if(typeof hashArr[1] === 'undefined' || typeof hashArr[2] === 'undefined'){
                            var kode_wilayah = session.kode_wilayah;
                            var id_level_wilayah = session.id_level_wilayah;
                        }else{
                            var kode_wilayah = hashArr[2];
                            var id_level_wilayah = hashArr[1];
                        }

                        var storeMst = Ext.create('Admin.store.MstWilayah');

                        storeMst.load({
                            params: {
                                kode_wilayah: kode_wilayah
                            }
                        });

                        storeMst.on('load', function(storeMst){
                            var rec = storeMst.getAt(0);

                            if(rec){
                                window.location.href="#KualitasDataIdentitas/"+(id_level_wilayah-1)+"/"+rec.data.mst_kode_wilayah;
                            }
                        });
                    }
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }      
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        // this.getSelectionModel().setSelectionMode('SINGLE');
        
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function(rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });
        
        var editing = this.createEditing();

        // this.rowEditing = editing;
        // this.plugins = [editing];

        
        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 144,
            sortable: true,
            dataIndex: 'sekolah_id',
            hideable: true,
            hidden: true
        },{
            header: 'Nama',
            tooltip: 'Nama Satuan Pendidikan',
            width: 200,
            sortable: true,
            locked: false,
            dataIndex: 'nama',
            hideable: true,
            renderer: function(v,p,r){
                return '<a style="color:#228ab7;text-decoration:none" href="#ProfilSatuanPendidikan/'+r.data.sekolah_id+'">'+v+'</a>';
            }
        },{
            header: 'NPSN',
            tooltip: 'npsn',
            width: 120,
            sortable: true,
            dataIndex: 'npsn',
            hideable: true,
            hidden: false
        },{
            header: 'Kabupaten',
            tooltip: 'Kabupaten',
            width: 120,
            sortable: true,
            dataIndex: 'kabupaten',
            hideable: true,
            hidden: false
        },{
            header: 'Rata-rata<br>Kualitas<br>Identitas',
            width: 150,
            sortable: true,
            dataIndex: 'total_valid',
            hideable: false,
            align:'right',
            renderer:function(v,p,r){
                return v.toFixed(2);
            }
        },{
            text: 'Persentase Indikator Kualitas Data Identitas Sekolah',
            itemId:'kualitas_group', 
            columns: [{
                header: 'Kepsek',
                width: 100,
                sortable: true,
                dataIndex: 'nama_kepsek_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'No Hp Kepsek',
                width: 100,
                sortable: true,
                dataIndex: 'email_kepsek_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'NPSN',
                width: 100,
                sortable: true,
                dataIndex: 'npsn_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'SK Pendirian',
                width: 100,
                sortable: true,
                dataIndex: 'sk_pendirian_sekolah_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'Tgl SK',
                width: 100,
                sortable: true,
                dataIndex: 'tanggal_sk_pendirian_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'SK Izin',
                width: 100,
                sortable: true,
                dataIndex: 'sk_izin_operasional_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'Tgl SK Izin',
                width: 100,
                sortable: true,
                dataIndex: 'tanggal_sk_izin_operasional_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'No Rekening',
                width: 100,
                sortable: true,
                dataIndex: 'no_rekening_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'Nama Bank',
                width: 100,
                sortable: true,
                dataIndex: 'nama_bank_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'Cabang KCP',
                width: 100,
                sortable: true,
                dataIndex: 'cabang_kcp_unit_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'Rekening Atas Nama',
                width: 100,
                sortable: true,
                dataIndex: 'rekening_atas_nama_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'Daya Listrik',
                width: 100,
                sortable: true,
                dataIndex: 'daya_listrik_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'Bersedia Menerima BOS',
                width: 100,
                sortable: true,
                dataIndex: 'partisipasi_bos_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'Waktu Penyelenggaraan',
                width: 100,
                sortable: true,
                dataIndex: 'waktu_penyelenggaraan_id_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'Sumber Listrik',
                width: 100,
                sortable: true,
                dataIndex: 'sumber_listrik_id_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'Sertifikasi ISO',
                width: 100,
                sortable: true,
                dataIndex: 'sertifikasi_iso_id_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            },{
                header: 'Akses Internet',
                width: 100,
                sortable: true,
                dataIndex: 'akses_internet_id_valid',
                hideable: false,
                align:'center',
                renderer: function(v,p,r){
                    if(v == 1){
                        return '<i class="fa fa-fw" style=font-size:20px;color:#1DA075>&#xf046</i>';
                    }else{
                        // return "tes";
                        return '<i class="fa fa-fw" style=font-size:20px;color:#cccccc>-</i>';
                    }
                }
            }]
        }];
        
        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();
        
        this.callParent(arguments);
    }
});