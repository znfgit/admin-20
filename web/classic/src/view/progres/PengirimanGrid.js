Ext.define('Admin.view.progres.PengirimanGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pengirimangrid',
    title: '',
    controller: 'Pengiriman',
    // scrollable: true,
    columnLines: true,
    listeners:{
        afterrender: 'setupPengirimanGrid',
        beforerender: 'setupPengirimanGridBefore',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                window.location="#ProgresPengiriman/"+record.data.id_level_wilayah+"/"+record.data.kode_wilayah;

                // this.store.reload({
                //     params:{
                //         kode_wilayah: record.data.kode_wilayah,
                //         id_level_wilayah: record.data.id_level_wilayah
                //     }
                // });
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.Pengiriman');
    },
    createEditing: function() {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Tambah',
                //cls: 'addbutton',
                iconCls: 'x-fa fa-plus',
                // glyph: 61525,
                ui: 'soft-blue',
                scope: this,
                action: 'add',
                listeners:{
                    click: 'addRecord'
                }
            }, {
                xtype: 'button',
                text: 'Ubah',
                iconCls: 'x-fa fa-pencil',
                // glyph: 61508,
                //cls: 'editbutton',
                itemId: 'edit',
                ui: 'soft-purple',
                scope: this,
                action: 'edit',
                listeners:{
                    click: 'editRecord'
                }
            }, {
                xtype: 'button',
                text: 'Simpan',
                iconCls: 'x-fa fa-save',
                // glyph: 61639,
                //cls: 'editbutton',
                itemId: 'save',
                ui: 'soft-green',
                scope: this,
                action: 'save',
                listeners:{
                    click: 'saveRecord'
                }
            }, {
                xtype: 'button',
                text: 'Hapus',
                //cls: 'deletebutton',
                iconCls: 'x-fa fa-trash',
                // glyph: 61526,
                itemId: 'delete',
                ui: 'soft-red',
                scope: this,
                action: 'delete',
                listeners:{
                    click: 'deleteRecord'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }      
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        // this.getSelectionModel().setSelectionMode('SINGLE');
        
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function(rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });
        
        var editing = this.createEditing();

        // this.rowEditing = editing;
        // this.plugins = [editing];

        
        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 144,
            sortable: true,
            dataIndex: 'kode_wilayah',
            hideable: true,
            hidden: true
        },{
            header: 'Wilayah',
            tooltip: 'Nama Wilayah',
            width: 200,
            sortable: true,
            locked: false,
            dataIndex: 'nama',
            hideable: true,
            renderer: function(v,p,r){
                return '<a style="color:#228ab7;text-decoration:none" href="#PengirimanSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah.trim()+'">'+v+'</a>';
            }
        },{
            header: '%',
            dataIndex: 'persen',
            width: 80,
            align: 'right',
            renderer: function(v,p,r){
                return (r.data.persen*1).toFixed(2);
            }
        },{
            header: 'Total<br>Kirim',
            dataIndex: 'sekolah_total',
            width: 100,
            align: 'right'
        },{
            header: 'SD',
            hideable: true,
            columns:[{
                header: 'Total',
                dataIndex: 'sd',
                width: 80,
                align: 'right'
            },{
                header: 'Kirim',
                dataIndex: 'kirim_sd',
                width: 90,
                align: 'right'
            },{
                header: 'Sisa',
                dataIndex: 'persen',
                width: 90,
                align: 'right',
                renderer: function(v,p,r){
                    return r.data.sd - r.data.kirim_sd;
                }
            }]
        },{
            header: 'SMP',
            hideable: true,
            columns:[{
                header: 'Total',
                dataIndex: 'smp',
                width: 80,
                align: 'right'
            },{
                header: 'Kirim',
                dataIndex: 'kirim_smp',
                width: 90,
                align: 'right'
            },{
                header: 'Sisa',
                dataIndex: 'persen',
                width: 90,
                align: 'right',
                renderer: function(v,p,r){
                    return r.data.smp - r.data.kirim_smp;
                }
            }]
        },{
            header: 'SMA',
            hideable: true,
            columns:[{
                header: 'Total',
                dataIndex: 'sma',
                width: 80,
                align: 'right'
            },{
                header: 'Kirim',
                dataIndex: 'kirim_sma',
                width: 90,
                align: 'right'
            },{
                header: 'Sisa',
                dataIndex: 'persen',
                width: 90,
                align: 'right',
                renderer: function(v,p,r){
                    return r.data.sma - r.data.kirim_sma;
                }
            }]
        },{
            header: 'SMK',
            hideable: true,
            columns:[{
                header: 'Total',
                dataIndex: 'smk',
                width: 80,
                align: 'right'
            },{
                header: 'Kirim',
                dataIndex: 'kirim_smk',
                width: 90,
                align: 'right'
            },{
                header: 'Sisa',
                dataIndex: 'persen',
                width: 90,
                align: 'right',
                renderer: function(v,p,r){
                    return r.data.sma - r.data.kirim_sma;
                }
            }]
        },{
            header: 'SLB',
            hideable: true,
            columns:[{
                header: 'Total',
                dataIndex: 'slb',
                width: 80,
                align: 'right'
            },{
                header: 'Kirim',
                dataIndex: 'kirim_slb',
                width: 90,
                align: 'right'
            },{
                header: 'Sisa',
                dataIndex: 'persen',
                width: 90,
                align: 'right',
                renderer: function(v,p,r){
                    return r.data.slb - r.data.kirim_slb;
                }
            }]
        }
        ,{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }];
        
        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();
        
        this.callParent(arguments);
    }
});