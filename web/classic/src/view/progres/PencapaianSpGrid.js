Ext.define('Admin.view.progres.PencapaianSpGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.PencapaianSpGrid',
    title: '',
    controller: 'Pencapaian',
    // scrollable: true,
    columnLines: true,
    listeners:{
        afterrender: 'setupPencapaianSpGrid'
    },
    createStore: function() {
        return Ext.create('Admin.store.PencapaianSp');
    },
    createEditing: function() {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: ['-']
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }      
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        // this.getSelectionModel().setSelectionMode('SINGLE');
        
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function(rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });
        
        var editing = this.createEditing();

        // this.rowEditing = editing;
        // this.plugins = [editing];

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        console.log(session.tahun_ajaran_id);

        if(session.tahun_ajaran_id != 2016){

            this.columns = [{
                header: 'Id',
                tooltip: 'Id',
                width: 144,
                sortable: true,
                dataIndex: 'sekolah_id',
                hideable: true,
                hidden: true
            },{
                header: 'Nama',
                tooltip: 'Nama Sekolah',
                width: 200,
                sortable: true,
                locked: false,
                dataIndex: 'nama',
                hideable: true,
                renderer: function(v,p,r){
                    return '<a style="color:#228ab7;text-decoration:none" href="#ProfilSatuanPendidikan/'+r.data.sekolah_id+'">'+v+'</a>';
                }
            },{
                header: 'NPSN',
                tooltip: 'NPSN',
                width: 90,
                sortable: true,
                locked: false,
                dataIndex: 'npsn',
                hideable: true
            },{
                header: 'Kecamatan',
                tooltip: 'Kecamatan',
                width: 120,
                sortable: true,
                locked: false,
                dataIndex: 'kecamatan',
                hideable: true
            },{
                header: session.tahun_ajaran_id,
                hideable: true,
                columns:[{
                    header: 'Jul',
                    dataIndex: 'rata_rata_valid_bulan_7',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Ags',
                    dataIndex: 'rata_rata_valid_bulan_8',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Sep',
                    dataIndex: 'rata_rata_valid_bulan_9',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Okt',
                    dataIndex: 'rata_rata_valid_bulan_10',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Nov',
                    dataIndex: 'rata_rata_valid_bulan_11',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Des',
                    dataIndex: 'rata_rata_valid_bulan_12',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                }]
            },{
                header: (parseInt(session.tahun_ajaran_id)+1),
                hideable: true,
                columns:[{
                    header: 'Jan',
                    dataIndex: 'rata_rata_valid_bulan_1',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Feb',
                    dataIndex: 'rata_rata_valid_bulan_2',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Mar',
                    dataIndex: 'rata_rata_valid_bulan_3',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Apr',
                    dataIndex: 'rata_rata_valid_bulan_4',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Mei',
                    dataIndex: 'rata_rata_valid_bulan_5',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Jun',
                    dataIndex: 'rata_rata_valid_bulan_6',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                }]
            }];
            
        }else{

            this.columns = [{
                header: 'Id',
                tooltip: 'Id',
                width: 144,
                sortable: true,
                dataIndex: 'sekolah_id',
                hideable: true,
                hidden: true
            },{
                header: 'Nama',
                tooltip: 'Nama Sekolah',
                width: 200,
                sortable: true,
                locked: false,
                dataIndex: 'nama',
                hideable: true,
                renderer: function(v,p,r){
                    return '<a style="color:#228ab7;text-decoration:none" href="#ProfilSatuanPendidikan/'+r.data.sekolah_id+'">'+v+'</a>';
                }
            },{
                header: 'NPSN',
                tooltip: 'NPSN',
                width: 90,
                sortable: true,
                locked: false,
                dataIndex: 'npsn',
                hideable: true
            },{
                header: 'Kecamatan',
                tooltip: 'Kecamatan',
                width: 120,
                sortable: true,
                locked: false,
                dataIndex: 'kecamatan',
                hideable: true
            },{
                header: session.tahun_ajaran_id,
                hideable: true,
                columns:[{
                    header: 'Ags',
                    dataIndex: 'rata_rata_valid_bulan_8',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Sep',
                    dataIndex: 'rata_rata_valid_bulan_9',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Okt',
                    dataIndex: 'rata_rata_valid_bulan_10',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Nov',
                    dataIndex: 'rata_rata_valid_bulan_11',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Des',
                    dataIndex: 'rata_rata_valid_bulan_12',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                }]
            },{
                header: (parseInt(session.tahun_ajaran_id)+1),
                hideable: true,
                columns:[{
                    header: 'Jan',
                    dataIndex: 'rata_rata_valid_bulan_1',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Feb',
                    dataIndex: 'rata_rata_valid_bulan_2',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Mar',
                    dataIndex: 'rata_rata_valid_bulan_3',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Apr',
                    dataIndex: 'rata_rata_valid_bulan_4',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Mei',
                    dataIndex: 'rata_rata_valid_bulan_5',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Jun',
                    dataIndex: 'rata_rata_valid_bulan_6',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                },{
                    header: 'Jul',
                    dataIndex: 'rata_rata_valid_bulan_7',
                    width: 75,
                    align: 'right',
                    renderer:function(v,p,r){
                        if(!v){
                            return '-';
                        }else{
                            return v;
                        }
                    }
                }]
            }];

        }

        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();
        
        this.callParent(arguments);
    }
});