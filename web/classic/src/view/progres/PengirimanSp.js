Ext.define('Admin.view.progres.PengirimanSp', {
    extend: 'Ext.container.Container',
    xtype: 'pengirimanPanel',

    controller: 'Pengiriman',
    
    cls: 'pengiriman-sp-container',
    
    layout: {
        type: 'border'
    },

    items: [
        {
            xtype: 'pengirimanspgrid',
            title: 'Sinkronisasi Dapodik per Sekolah',
            // responsiveCls: 'big-100',
            // minHeight:200,
            iconCls: 'right-icon x-fa fa-send',
            region: 'center',
            // html: 'pengiriman'
        }
        // ,{
        //     xtype: 'panel',
        //     title: 'Progres per Satuan Pendidikan di wilayah',
        //     iconCls: 'right-icon x-fa fa-send',
        //     region: 'south',
        //     minHeight: 200,
        //     split: false,
        //     collapsible: true
        // }
    ]
});
