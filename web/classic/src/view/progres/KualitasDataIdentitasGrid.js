Ext.define('Admin.view.progres.KualitasDataIdentitasGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.kualitasdataidentitasgrid',
    title: '',
    controller: 'Pengiriman',
    scrollable: true,
    columnLines: true,
    listeners:{
        afterrender: 'setupKualitasDataIdentitasGrid',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                window.location="#KualitasDataIdentitas/"+record.data.id_level_wilayah+"/"+record.data.kode_wilayah;
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.KualitasDataIdentitas');
    },
    createEditing: function() {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: ['-',{
                iconCls: 'x-fa fa-arrow-circle-left',
                ui: 'soft-blue',
                itemId: 'kembaliButton',
                listeners:{
                    click: 'kembali'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }      
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        // this.getSelectionModel().setSelectionMode('SINGLE');
        
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function(rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });
        
        var editing = this.createEditing();

        // this.rowEditing = editing;
        // this.plugins = [editing];

        
        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 144,
            sortable: true,
            dataIndex: 'kode_wilayah',
            hideable: true,
            hidden: true
        },{
            header: 'Wilayah',
            tooltip: 'Nama Wilayah',
            width: 200,
            sortable: true,
            locked: false,
            dataIndex: 'wilayah',
            hideable: true,
            renderer: function(v,p,r){
                return '<a style="color:#228ab7;text-decoration:none" href="#KualitasDataIdentitasSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah.trim()+'">'+v+'</a>';
            }
        },{
            header: 'Rata-rata<br>Kualitas<br>Identitas',
            width: 120,
            sortable: true,
            dataIndex: 'total_valid',
            hideable: false,
            align:'right',
            renderer:function(v,p,r){
                return v.toFixed(2);
            }
        },{
            text: 'Persentase Indikator Kualitas Data Identitas Sekolah',
            itemId:'kualitas_group', 
            columns: [{
                header: 'Kepsek',
                width: 100,
                sortable: true,
                dataIndex: 'nama_kepsek_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'No Hp Kepsek',
                width: 100,
                sortable: true,
                dataIndex: 'email_kepsek_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'NPSN',
                width: 100,
                sortable: true,
                dataIndex: 'npsn_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'SK Pendirian',
                width: 100,
                sortable: true,
                dataIndex: 'sk_pendirian_sekolah_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Tgl SK',
                width: 100,
                sortable: true,
                dataIndex: 'tanggal_sk_pendirian_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'SK Izin',
                width: 100,
                sortable: true,
                dataIndex: 'sk_izin_operasional_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Tgl SK Izin',
                width: 100,
                sortable: true,
                dataIndex: 'tanggal_sk_izin_operasional_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'No Rekening',
                width: 100,
                sortable: true,
                dataIndex: 'no_rekening_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Nama Bank',
                width: 100,
                sortable: true,
                dataIndex: 'nama_bank_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Cabang KCP',
                width: 100,
                sortable: true,
                dataIndex: 'cabang_kcp_unit_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Rekening Atas Nama',
                width: 100,
                sortable: true,
                dataIndex: 'rekening_atas_nama_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Daya Listrik',
                width: 100,
                sortable: true,
                dataIndex: 'daya_listrik_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Bersedia Menerima BOS',
                width: 100,
                sortable: true,
                dataIndex: 'partisipasi_bos_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Waktu Penyelenggaraan',
                width: 100,
                sortable: true,
                dataIndex: 'waktu_penyelenggaraan_id_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Sumber Listrik',
                width: 100,
                sortable: true,
                dataIndex: 'sumber_listrik_id_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Sertifikasi ISO',
                width: 100,
                sortable: true,
                dataIndex: 'sertifikasi_iso_id_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            },{
                header: 'Akses Internet',
                width: 100,
                sortable: true,
                dataIndex: 'akses_internet_id_valid',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return v.toFixed(2);
                }
            }]
        },{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }];
        
        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();
        
        this.callParent(arguments);
    }
});