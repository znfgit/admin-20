Ext.define('Admin.view.SatuanPendidikan.SpWaktuPenyelenggaraanSp', {
    extend: 'Ext.container.Container',
    xtype: 'spwaktupenyelenggaraansp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'spwaktupenyelenggaraan',
    
    cls: 'spwaktupenyelenggaraan-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',
    items: [
        {
            xtype: 'spwaktupenyelenggaraanspgrid',
            title: 'Satuan Pendidikan Berdasarkan Waktu Penyelenggaraan',
            // Always 100% of container
            // responsiveCls: 'big-100',
            // minHeight:200
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});
