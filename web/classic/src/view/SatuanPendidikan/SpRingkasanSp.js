Ext.define('Admin.view.SatuanPendidikan.SpRingkasanSp', {
    extend: 'Ext.container.Container',
    xtype: 'springkasansp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'springkasan',

    cls: 'springkasansp-container',

    layout: 'responsivecolumn',
    // layout: 'border',
    scrollable: 'y',
    items: [/*{
        xtype: 'panel',
        title: 'Rekap Ringkasan Satuan Pendidikan',
        // Always 100% of container
        responsiveCls: 'big-100',
        minHeight:200
        // region: 'center'
        // html: 'isinya daftar pengiriman'
    }*/{
        xtype: 'satuanpendidikangrid',
        name: 'tabelDataPokok',
        ui: 'yellow',
        title: 'Data Pokok Satuan Pendidikan',
        margin: '0 20 20 0',
        flex: 1,
        minHeight: 550
    }]
});
