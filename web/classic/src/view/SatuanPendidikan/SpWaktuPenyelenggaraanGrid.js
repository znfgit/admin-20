Ext.define('Admin.view.SatuanPendidikan.SpWaktuPenyelenggaraanGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.spwaktupenyelenggaraangrid',
    title: '',
    selType: 'rowmodel',
    listeners:{
        afterrender:'SpWaktuPenyelenggaraanGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.SatuanPendidikanWaktuPenyelenggaraan');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                listeners:{
                    click: 'kembali'
                }
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'SatuanPendidikan',
                paramMethod: 'SatuanPendidikanWaktuPenyelenggaraan',
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        Ext.apply(this, this.initialConfig);

        
        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            // locked: true,
            dataIndex: 'no',
            hideable: false,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1); 
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Wilayah',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
			hideable: false,
            // locked: true,
            renderer: function(v,p,r){
                if(r.data.id_level_wilayah){
                    return '<a style="color:#228ab7;text-decoration:none" href="#SpWaktuPenyelenggaraanSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah+'">'+v+'</a>';
                }else{
                    return v;
                }
            }
        }
        ,{
            text: 'pagi',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'negeri_pagi',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'swasta_pagi',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'swasta_siang',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(r.data.negeri_pagi + r.data.swasta_pagi);
                    }
                }
            ]
        },{
            text: 'Siang',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'negeri_siang',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'swasta_siang',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'swasta_siang',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(r.data.negeri_siang + r.data.swasta_siang);
                    }
                }
            ]
        },{
            text: 'Sore',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'negeri_sore',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'swasta_sore',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'swasta_siang',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(r.data.negeri_sore + r.data.swasta_sore);
                    }
                }
            ]
        },{
            text: 'Malam',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'negeri_malam',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'swasta_malam',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'swasta_siang',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(r.data.negeri_malam + r.data.swasta_malam);
                    }
                }
            ]
        },{
            text: 'Kombinasi',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'negeri_kombinasi',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'swasta_kombinasi',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'swasta_siang',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(r.data.negeri_kombinasi + r.data.swasta_kombinasi);
                    }
                }
            ]
        },{
            text: 'Sehari Penuh (5 h/m)',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'negeri_sehari_penuh_5',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'swasta_sehari_penuh_5',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'swasta_siang',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(r.data.negeri_sehari_penuh_5 + r.data.swasta_sehari_penuh_5);
                    }
                }
            ]
        },{
            text: 'Sehari Penuh (6 h/m)',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'negeri_sehari_penuh_6',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'swasta_sehari_penuh_6',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'swasta_siang',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(r.data.negeri_sehari_penuh_6 + r.data.swasta_sehari_penuh_6);
                    }
                }
            ]
        },{
            text: 'Lainnya',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'negeri_lainnya',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'swasta_lainnya',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'swasta_siang',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(r.data.negeri_lainnya + r.data.swasta_lainnya);
                    }
                }
            ]
        },{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }
        ];
        
        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();
        
        this.callParent(arguments);

        // this.store.load();
    }
});