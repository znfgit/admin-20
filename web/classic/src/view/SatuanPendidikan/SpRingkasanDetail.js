Ext.define('Admin.view.SatuanPendidikan.SpRingkasanDetail', {
    extend: 'Ext.container.Container',
    xtype: 'springkasandetail',

    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'Admin.view.SatuanPendidikan.SpRingkasanDetailGrid'
    ],

    controller: 'springkasandetail',

    cls: 'springkasandetail-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',
    items: [{
        xtype: 'springkasandetailgrid',
        title: 'Rekap Satuan Pendidikan ',
        // responsiveCls: 'big-100',
        // minHeight:'100%'
        region: 'center'
    }]
});
