Ext.define('Admin.view.SatuanPendidikan.SpKurikulum', {
    extend: 'Ext.container.Container',
    xtype: 'SpKurikulum',

    requires: [
        'Admin.view.SatuanPendidikan.SpKurikulumGrid'
    ],

    controller: 'spKurikulum',

    cls: 'spKurikulum-container',

    // layout: 'responsivecolumn',

    // scrollable: 'y',
    layout: {type:'border'},
    items: [
        {
            xtype: 'tabpanel',
            region: 'center',
            items:[{
                title: 'Rekap Kurikulum Satuan Pendidikan',
                xtype: 'spkurikulumgrid',
                iconCls: 'x-fa fa-building'
            }]
        }
    ]
});
