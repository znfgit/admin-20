Ext.define('Admin.view.SatuanPendidikan.DataRinciWindow', {
	extend: 'Ext.window.Window',
    alias: 'widget.datarinciwindow',
    requires:[
        'Ext.layout.container.Fit',
        'Ext.layout.container.VBox',
        'Ext.TabPanel'
    ],
    // layout: {
    //     type: 'border'
    // },
    layout: 'fit',
    controller: 'profilsatuanpendidikan',
    title: 'Data Rinci Sekolah',
    width: 500,
    height: 500,
    modal: true,
    scrollable: 'y',
    initComponent: function() { 

        this.items = [{
            xtype: 'tabpanel',
            // region: 'center',
            // ui: 'navigation-yellow',
            // minHeight:300,
            itemId: 'tabDataRinci',
            responsiveCls: 'big-100 small-100',
            items:[{
                xtype: 'sekolahlongitudinalform',
                title: 'Data Periodik',
                iconCls: 'fa fa-heartbeat'
            },{
                title: 'Sanitasi',
                xtype: 'sanitasiform',
                iconCls: 'fa fa-male'
            },{
                title: 'Blockgrant',
                xtype: 'blockgrantgrid',
                iconCls: 'fa fa-life-bouy',
                listeners:{
                    afterrender: 'blockgrandGridSetup'
                }
            }
            // ,{
            //     title: 'Paket Keahlian Dilayani'
            // }
            ]
        }];

        this.callParent(arguments);
    }
});