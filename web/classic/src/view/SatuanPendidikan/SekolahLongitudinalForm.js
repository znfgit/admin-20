Ext.define('Admin.view.SatuanPendidikan.SekolahLongitudinalForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.sekolahlongitudinalform',
    bodyPadding: 10,
    defaults: {
        labelWidth: 210,
        anchor: '100%'
    },
    layout: 'responsivecolumn',
    initComponent: function() {
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }
        
        this.items = [{
            xtype: 'panel',
            responsiveCls: 'big-50 small-100',
            cls: 'shadow-panel',
            defaults: {
                labelWidth: 210,
                anchor: '100%'
            },
            items:[{
                xtype: 'hidden'
                ,fieldLabel: ' '
                ,labelAlign: 'right'
                ,minValue: 0
                ,maxLength: 100
                ,enforceMaxLength: true
                ,cls: 'dsp-field'
                ,name: 'sekolah_longitudinal_id'
            },{
                xtype: 'displayfield'
                ,fieldLabel: 'Waktu penyelenggaraan'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,maxValue: 999999999
                ,minValue: 0
                ,cls: 'dsp-field'
                ,name: 'waktu_penyelenggaraan_id_str',
                renderer: function(v,p,r){
                    if(v){

                        return "<b>"+ v + "</b>";
                    }
                }
            },{
                xtype: 'displayfield'
                ,fieldLabel: 'Wilayah terpencil'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,maxValue: 999999999
                ,minValue: 0
                ,hideTrigger:true
                ,maxLength: 65536
                ,enforceMaxLength: true
                ,cls: 'dsp-field'
                ,name: 'wilayah_terpencil'
                ,renderer: function(v,p,r){
                    if(v == 1 || v == '1'){
                        return '<b>Ya</b>';
                    }else{
                        return '<b>Tidak</b>';
                    }
                }           
            },{
                xtype: 'displayfield'
                ,fieldLabel: 'Wilayah perbatasan'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,maxValue: 999999999
                ,minValue: 0
                ,hideTrigger:true
                ,maxLength: 65536
                ,enforceMaxLength: true
                ,cls: 'dsp-field'
                ,name: 'wilayah_perbatasan'
                ,renderer: function(v,p,r){
                    if(v == 1 || v == '1'){
                        return '<b>Ya</b>';
                    }else{
                        return '<b>Tidak</b>';
                    }
                }
            },{
                xtype: 'displayfield'
                ,fieldLabel: 'Wilayah transmigrasi'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,maxValue: 999999999
                ,minValue: 0
                ,hideTrigger:true
                ,maxLength: 65536
                ,enforceMaxLength: true
                ,cls: 'dsp-field'
                ,name: 'wilayah_transmigrasi'
                ,renderer: function(v,p,r){
                    if(v == 1 || v == '1'){
                        return '<b>Ya</b>';
                    }else{
                        return '<b>Tidak</b>';
                    }
                }
            },{
                xtype: 'displayfield'
                ,fieldLabel: 'Wilayah adat terpencil'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,maxValue: 999999999
                ,minValue: 0
                ,hideTrigger:true
                ,maxLength: 65536
                ,enforceMaxLength: true
                ,cls: 'dsp-field'
                ,name: 'wilayah_adat_terpencil'
                ,renderer: function(v,p,r){
                    if(v == 1 || v == '1'){
                        return '<b>Ya</b>';
                    }else{
                        return '<b>Tidak</b>';
                    }
                }
            },{
                xtype: 'displayfield'
                ,fieldLabel: 'Wilayah bencana alam'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,maxValue: 999999999
                ,minValue: 0
                ,hideTrigger:true
                ,maxLength: 65536
                ,enforceMaxLength: true
                ,cls: 'dsp-field'
                ,name: 'wilayah_bencana_alam'
                ,renderer: function(v,p,r){
                    if(v == 1 || v == '1'){
                        return '<b>Ya</b>';
                    }else{
                        return '<b>Tidak</b>';
                    }
                }
            },{
                xtype: 'displayfield'
                ,fieldLabel: 'Wilayah bencana sosial'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,maxValue: 999999999
                ,minValue: 0
                ,hideTrigger:true
                ,maxLength: 65536
                ,enforceMaxLength: true
                ,cls: 'dsp-field'
                ,name: 'wilayah_bencana_sosial'
                ,renderer: function(v,p,r){
                    if(v == 1 || v == '1'){
                        return '<b>Ya</b>';
                    }else{
                        return '<b>Tidak</b>';
                    }
                }
            }]
        },{
            xtype: 'panel',
            responsiveCls: 'big-50 small-100',
            cls: 'shadow-panel',
            defaults: {
                labelWidth: 210,
                anchor: '100%'
            },
            items:[{
                xtype: 'displayfield'
                ,fieldLabel: 'Bersedia Menerima BOS?'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,maxValue: 999999999
                ,minValue: 0
                ,hideTrigger:true
                ,maxLength: 65536
                ,enforceMaxLength: true
                ,cls: 'dsp-field'
                ,name: 'partisipasi_bos'
                ,renderer: function(v,p,r){
                    if(v == 1 || v == '1'){
                        return '<b>Ya</b>';
                    }else{
                        return '<b>Tidak</b>';
                    }
                }
            },{
                xtype: 'displayfield'
                ,fieldLabel: 'Sertifikasi ISO'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,maxValue: 999999999
                ,minValue: 0
                ,cls: 'dsp-field'
                ,name: 'sertifikasi_iso_id_str'
                ,renderer: function(v,p,r){
                    if(v){

                        return "<b>"+ v + "</b>";
                    }
                }
            },{
                xtype: 'displayfield'
                ,fieldLabel: 'Sumber listrik'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,maxValue: 999999999
                ,minValue: 0
                ,cls: 'dsp-field'
                ,name: 'sumber_listrik_id_str'
                ,renderer: function(v,p,r){
                    if(v){

                        return "<b>"+ v + "</b>";
                    }
                }
            },{
                xtype: 'displayfield'
                ,fieldLabel: 'Daya listrik'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,maxValue: 999999999
                ,minValue: 0
                ,hideTrigger:true
                ,maxLength: 393216
                ,enforceMaxLength: true
                ,cls: 'dsp-field'
                ,name: 'daya_listrik'
                ,renderer: function(v,p,r){
                    if(v){

                        return "<b>"+ v + "</b>";
                    }
                }
            },{
                xtype: 'displayfield'
                ,fieldLabel: 'Akses internet'
                ,labelAlign: 'right'
                ,maxValue: 999999999
                ,minValue: 0
                ,cls: 'dsp-field'
                ,name: 'akses_internet_id_str'
                ,renderer: function(v,p,r){
                    if(v){

                        return "<b>"+ v + "</b>";
                    }
                }
            },{
                xtype: 'displayfield'
                ,fieldLabel: 'Akses internet Alternatif'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,maxValue: 999999999
                ,minValue: 0
                ,cls: 'dsp-field'
                ,name: 'akses_internet_2_id_str'
                ,renderer: function(v,p,r){
                    if(v){

                        return "<b>"+ v + "</b>";
                    }
                }
            }]
        }];

        // this.buttons = [{
        //     text: 'Save',
        //     iconCls: 'fa fa-save fa-inverse fa-lg',
        //     action: 'save'
        // }];

        this.callParent(arguments);
    }
});