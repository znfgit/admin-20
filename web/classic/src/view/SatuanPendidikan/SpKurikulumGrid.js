Ext.define('Admin.view.SatuanPendidikan.SpKurikulumGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.spkurikulumgrid',
    title: '',
    columnLines: true,
    selType: 'rowmodel',
    listeners:{
        afterrender:'SpKurikulumGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.SatuanPendidikanKurikulum');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                listeners:{
                    click: 'kembali'
                }
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'SatuanPendidikan',
                paramMethod: 'SatuanPendidikanKurikulum',
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a,
            c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {

        var grid = this;

        this.store = this.createStore();

        Ext.apply(this, this.initialConfig);


        this.columns = [{
            header: 'No',
            width: 40,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            // locked: true,
            renderer: function(v,p,r){
                // console.log(v);
                // console.log(p.recordIndex);
                // console.log(r);
                return (p.recordIndex + 1);
            }
        },
        {
            header: 'Wilayah',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            // locked: true,
            renderer: function(v,p,r){

                if(r.data.id_level_wilayah){
                    return '<a style="color:#228ab7;text-decoration:none" href="#SpKurikulumSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah+'">'+v+'</a>';
                }else{
                    return v;
                }

            }
        }
        ,{
            text: 'Kurikulum 2013',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'k13_negeri',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'k13_swasta',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'k13_total',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        }
        ,{
            text: 'KTSP',
            columns:[
                {
                    header: 'Negeri',
                    align:'right',
                    dataIndex: 'ktsp_negeri',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Swasta',
                    align:'right',
                    dataIndex: 'ktsp_swasta',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'ktsp_total',
                    width: 90,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                }
            ]
        }
        ,{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }
        ];

        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();

        this.callParent(arguments);

        // this.store.load();
    }
});