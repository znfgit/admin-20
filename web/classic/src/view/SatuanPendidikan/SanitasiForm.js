Ext.define('Admin.view.SatuanPendidikan.SanitasiForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.sanitasiform',
    bodyPadding: 10,
    ui: 'soft-green',
    defaults: {
        labelWidth: 300,
        anchor: '100%'
    },
    initComponent: function() {
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        /*this.on('beforeadd', function(form, field){
            if (!field.allowBlank)
              field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
        });*/
        
        this.items = [{
            xtype: 'hidden'
            ,fieldLabel: ' '
            ,labelAlign: 'right'
			,minValue: 0
            ,maxLength: 100
            ,enforceMaxLength: true
            ,cls: 'dsp-field'
            ,name: 'sanitasi_id'
        },{
            xtype: 'hidden'
            ,fieldLabel: 'ID'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
			,allowBlank: false
			,maxValue: 99999999
			,minValue: 0
            ,cls: 'dsp-field'
            ,name: 'sekolah_id'
        },{
            xtype: 'displayfield'
            ,fieldLabel: 'Kecukupan air'
            ,labelAlign: 'right'
            ,cls: 'dsp-field'
            ,name: 'kecukupan_air'
            ,renderer: function(v,p,r){
                if(v == 1){
                    return '<b>Cukup</b>';
                }else if(v == 2){
                    return '<b>Kurang</b>';
                }else{
                    return '<b>Tidak Ada</b>';
                }
            }
        },{
            xtype: 'displayfield'
            ,fieldLabel: 'Sekolah memproses air sendiri'
            ,labelAlign: 'right'
            ,cls: 'dsp-field'
            ,name: 'memproses_air'
			,allowBlank: false
            ,renderer: function(v,p,r){
                if(v == 1){
                    return '<b>Ya</b>';
                }else{
                    return '<b>Tidak</b>';
                }
            }
        },{
            xtype: 'displayfield'
            ,fieldLabel: 'Air minum untuk siswa'
            ,labelAlign: 'right'
            ,cls: 'dsp-field'
            ,name: 'minum_siswa'
            ,renderer: function(v,p,r){
                if(v == 1){
                    return '<b>Disediakan Sekolah</b>';
                }else{
                    return '<b>Tidak Disediakan</b>';
                }
            }
        },{
            xtype: 'displayfield'
            ,fieldLabel: 'Mayoritas Siswa Membawa Air Minum'
            ,labelAlign: 'right'
            ,cls: 'dsp-field'
            ,name: 'siswa_bawa_air'
			,allowBlank: false
            ,renderer: function(v,p,r){
                if(v == 1){
                    return '<b>Ya</b>';
                }else{
                    return '<b>Tidak</b>';
                }
            }
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Jumlah toilet utk laki-laki'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
			,allowBlank: false
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 100
            ,enforceMaxLength: true
            ,cls: 'dsp-field'
            ,name: 'toilet_siswa_laki'
			,anchor: '25%%'
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Jumlah toilet utk perempuan'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
			,allowBlank: false
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 100
            ,enforceMaxLength: true
            ,cls: 'dsp-field'
            ,name: 'toilet_siswa_perempuan'
			,anchor: '25%%'
        },{
            xtype: 'displayfield'
            ,fieldLabel: 'Jumlah toilet berkebutuhan khusus'
            ,labelAlign: 'right'
            ,cls: 'dsp-field'
            ,name: 'toilet_siswa_kk'
			,anchor: '25%%'
            ,renderer: function(v,p,r){
                if(v){
                    return '<b>'+v+'</b>';
                }else{
                    return '<b>-</b>';
                }
            }
        },{
            xtype: 'hidden'
            ,fieldLabel: 'ID'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
			,allowBlank: false
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 5
            ,enforceMaxLength: true
            ,cls: 'dsp-field'
            ,name: 'semester_id'
        },{
            xtype: 'displayfield'
            ,fieldLabel: 'Sumber air sanitasi'
            ,labelAlign: 'right'
            ,allowBlank: false
			,maxValue: 8
			,minValue: 0
            ,cls: 'dsp-field'
            ,name: 'sumber_air_id_str'
            ,renderer: function(v,p,r){
                if(v){
                    return '<b>'+v+'</b>';
                }else{
                    return '<b>-</b>';
                }
            }
        },{
            xtype: 'displayfield'
            ,fieldLabel: 'Ketersediaan air sanitasi'
            ,labelAlign: 'right'
            ,cls: 'dsp-field'
            ,name: 'ketersediaan_air'
            ,allowBlank: false
            ,renderer: function(v,p,r){
                if(v == 1){
                    return '<b>Ada Sumber Air</b>';
                }else{
                    return '<b>Tidak Ada</b>';
                }
            }
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Jml toilet utk siswa kecil (kls 1 &amp; 2)'
            ,labelAlign: 'right'
            ,allowBlank: false
			,maxValue: 9
			,minValue: 0
            ,maxLength: 1
            ,enforceMaxLength: true
            ,cls: 'dsp-field'
            ,name: 'toilet_siswa_kecil'
			,anchor: '25%%'
            ,renderer: function(v,p,r){
                if(v){
                    return '<b>'+v+'</b>';
                }else{
                    return '<b>-</b>';
                }
            }
        },{
            xtype: 'displayfield'
            ,fieldLabel: 'Jumlah tempat cuci tangan'
            ,labelAlign: 'right'
            ,cls: 'dsp-field'
            ,name: 'tempat_cuci_tangan'
			,anchor: '25%%'
            ,renderer: function(v,p,r){
                if(v){
                    return '<b>'+v+'</b>';
                }else{
                    return '<b>-</b>';
                }
            }
        }];

        // this.buttons = [
        // {
        //    glyph: 61529,
        //    scope: this,
        //    action: 'help',
        //    maxWidth: 30,
        //    tooltip: 'Klik tombol berikut untuk mendapatkan bantuan'
        // },{
        //     text: 'Simpan',
        //     glyph: 61639,
        //     action: 'save'
        // }
        // ,{    
        //     text: '<b>Bantuan (?)</b>',
        //     itemId: 'help',
        //     width: 50,
        //     scope: this,
        //     action: 'help'
        // }
        // ];

        this.callParent(arguments);
    }
});