Ext.define('Admin.view.SatuanPendidikan.ProfilSatuanPendidikan', {
    extend: 'Ext.container.Container',
    xtype: 'profilsatuanpendidikan',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'profilsatuanpendidikan',
    viewModel: {
        type: 'dashboard'
    },
    scrollable: 'y',

    layout: 'responsivecolumn',

    items: [
        {
            xtype: 'panel',
            itemId: 'headerProfilSatuanPendidikan',
            responsiveCls: 'big-100',
            cls: 'admin-widget-small sale-panel info-card-item shadow-panel',
            layout: 'responsivecolumn',
            ui: 'yellow',
            bodyStyle: 'background:url(resources/images/bg1.png);background-size:cover;color:white',
            minHeight: 70,
            listeners: {
                afterrender: 'headerProfilSetup'
            },
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                style: 'background: #02478E',
                items: [{
                    xtype: 'button',
                    text: 'Unduh Profil',
                    ui: 'soft-green',
                    align: 'right',
                    iconCls: 'x-fa fa-file-excel-o',
                    listeners: {
                        click: 'tombolUnduhProfilSekolah'
                    }
                }]
            }]
        },
        {
            xtype: 'tabpanel',
            // ui: 'navigation-yellow',
            responsiveCls: 'big-100',
            minHeight: 300,
            items: [
                // {
                //     title: 'Ikhtisar',
                //     iconCls: 'right-icon x-fa fa-connectdevelop',
                //     layout: 'responsivecolumn',
                //     items:[{
                //         xtype: 'piesarpraskerusakansekolah',
                //         title: 'Rekap Kerusakan Ruang Kelas',
                //         responsiveCls: 'big-40 small-100',
                //         cls: 'admin-widget-small sale-panel info-card-item shadow-panel'
                //         // minHeight: 100
                //     },{
                //         xtype:'pdperjurusan',
                //         title: 'Jumlah Peserta Didik Per Kompetensi',
                //         responsiveCls: 'big-60 small-100',
                //         cls: 'dashboard-main-chart shadow-panel'
                //         // minHeight: 100
                //     }]
                // },
                {
                    title: 'Profil',
                    iconCls: 'right-icon x-fa fa-building',
                    itemId: 'profilSekolah',
                    layout: 'responsivecolumn',
                    items: [{
                        xtype: 'sekolahform',
                        minHeight: 200,
                        title: 'Identitas Satuan Pendidikan',
                        responsiveCls: 'big-100 small-100',
                        cls: 'shadow-panel',
                        tbar: ['->', {
                            xtype: 'button',
                            text: 'Data Rinci Sekolah',
                            ui: 'soft-purple',
                            iconCls: 'fa fa-list',
                            listeners: {
                                click: 'dataRinciSekolah'
                            }
                        }, {
                            xtype: 'button',
                            text: 'Unduh Xls',
                            ui: 'soft-green',
                            hidden: true,
                            iconCls: 'fa fa-file-excel-o',
                            listeners: {
                                click: function (btn) {
                                    var panel = btn.up('sekolahform').up('panel').up('tabpanel').up('profilsatuanpendidikan');
                                    // Ext.MessageBox.alert('Mohon Maaf', 'Fitur ini belum tersedia untuk saat ini');
                                }
                            }
                        }]
                    }
                        // ,{
                        //     xtype: 'tabpanel',
                        //     // title: 'Data Rinci',
                        //     minHeight:300,
                        //     itemId: 'tabDataRinci',
                        //     responsiveCls: 'big-100 small-100',
                        //     cls: 'shadow-panel',
                        //     items:[{
                        //         xtype: 'sekolahlongitudinalform',
                        //         title: 'Data Periodik'
                        //     },{
                        //         title: 'Sanitasi',
                        //         xtype: 'sanitasiform'
                        //     },{
                        //         title: 'Blockgrant',
                        //         xtype: 'blockgrantgrid'
                        //     },{
                        //         title: 'Paket Keahlian Dilayani'
                        //     }]
                        // }
                    ]
                }]
        }
    ]
});
