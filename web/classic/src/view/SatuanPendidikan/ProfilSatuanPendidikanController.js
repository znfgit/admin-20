Ext.define('Admin.view.SatuanPendidikan.ProfilSatuanPendidikanController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.profilsatuanpendidikan',
    headerProfilSetup: function(panel){
        // panel.setTitle('testing');

        var profilPanel = panel.up('profilsatuanpendidikan');
        var tabPanelTitle = profilPanel.down('panel');
        var tabPanelProfil = profilPanel.down('tabpanel');
        var breadcrumb = profilPanel.down('panel[itemId=breadcrumb]');
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        var formSekolah = profilPanel.down('tabpanel').down('panel[itemId=profilSekolah]').down('sekolahform');

        // console.log(formSekolah);

        // console.log(profilPanel.sekolah_id);

        // console.log(session);
        if(session.session){
            // apa yang mau dikerjain klo ada session nya

            if(session.peran_id != 10){
                /*tabPanelTitle.add({
                    xtype: 'button',
                    text: 'Unduh Profil',
                    ui: 'soft-green',
                    align: 'right',
                    iconCls: 'x-fa fa-file-excel-o',
                    listeners:{
                        click: 'tombolUnduhProfilSekolah'
                    }
                });*/

                tabPanelProfil.add({
                    xtype: 'prasaranagrid',
                    title: 'Sarpras',
                    iconCls: 'right-icon x-fa fa-road'
                },{
                    xtype: 'ptkgrid',
                    title: 'PTK',
                    iconCls: 'right-icon x-fa fa-graduation-cap',
                    listeners:{
                        afterrender:'PtkGridSetup'
                    }
                },{
                    xtype: 'pesertadidikgrid',
                    title: 'Peserta Didik',
                    iconCls: 'right-icon x-fa fa-child'
                },{
                    xtype: 'rombonganbelajargrid',
                    title: 'Rombel',
                    iconCls: 'right-icon x-fa fa-users'
                });
            }

        }else{
            panel.down('toolbar').hide();
            // tabPanelProfil.down('prasaranagrid').destroy();
            // tabPanelProfil.down('ptkgrid').destroy();
            // tabPanelProfil.down('pesertadidikgrid').destroy();
            // tabPanelProfil.down('rombonganbelajargrid').destroy();
        }

        var storeSekolah  = new Admin.store.SatuanPendidikan();

        storeSekolah.load({
            params:{
                sekolah_id: profilPanel.sekolah_id
            }
        });

        storeSekolah.on('load', function(store){

            var rec = store.getAt(0);

            formSekolah.loadRecord(rec);

            // console.log(rec.data.sekolah_id);

            Ext.Ajax.request({
                url: 'SatuanPendidikan/getRekapSekolah?sekolah_id='+rec.data.sekolah_id+'&semester_id='+session.semester_berjalan,
                success: function(response){
                    var text = response.responseText;
                    
                    var obj = Ext.JSON.decode(response.responseText);

                    console.log(obj[0].akreditasi_id_str);

                    formSekolah.down('displayfield[name=akreditasi_id_str]').setValue(obj[0].akreditasi_id_str);
                    formSekolah.down('displayfield[name=sk_akreditasi]').setValue(obj[0].sk_akreditasi);
                    formSekolah.down('displayfield[name=tmt_akreditasi]').setValue(obj[0].tmt_akreditasi);
                    formSekolah.down('displayfield[name=txt_akreditasi]').setValue(obj[0].txt_akreditasi);
                }
            });

            if(session.sekolah_id == rec.data.sekolah_id){
                tabPanelProfil.add({
                    xtype: 'prasaranagrid',
                    title: 'Sarpras',
                    iconCls: 'right-icon x-fa fa-road'
                },{
                    xtype: 'ptkgrid',
                    title: 'PTK',
                    iconCls: 'right-icon x-fa fa-graduation-cap'
                },{
                    xtype: 'pesertadidikgrid',
                    title: 'Peserta Didik',
                    iconCls: 'right-icon x-fa fa-child'
                },{
                    xtype: 'rombonganbelajargrid',
                    title: 'Rombel',
                    iconCls: 'right-icon x-fa fa-users'
                });
            }else{

                if(session.session){

                    if(session.peran_id == 10){
                        if(session.sekolah_id != rec.data.sekolah_id){
                            formSekolah.down('fieldset[itemId=kontakSekolah]').down('displayfield[name=nomor_telepon]').destroy();
                            formSekolah.down('fieldset[itemId=kontakSekolah]').down('displayfield[name=nomor_fax]').destroy();
                            formSekolah.down('fieldset[itemId=kontakSekolah]').down('displayfield[name=email]').destroy();
                        }
                    }

                }else{
                    formSekolah.down('fieldset[itemId=kontakSekolah]').down('displayfield[name=nomor_telepon]').destroy();
                    formSekolah.down('fieldset[itemId=kontakSekolah]').down('displayfield[name=nomor_fax]').destroy();
                    formSekolah.down('fieldset[itemId=kontakSekolah]').down('displayfield[name=email]').destroy();
                }

            }

            // console.log(rec.data.kode_wilayah.substring(0,6));

            var storeKecamatan = new Admin.store.MstWilayah();

            storeKecamatan.load({
                params:{
                    kode_wilayah: rec.data.kode_wilayah.substring(0,6)
                }
            });

            storeKecamatan.on('load', function(store){
                var rec = store.getAt(0);

                if(rec){
                    formSekolah.down('displayfield[name=kecamatan]').setValue(rec.data.nama);

                    var storeKabupaten = new Admin.store.MstWilayah();

                    storeKabupaten.load({
                        params:{
                            kode_wilayah: rec.data.mst_kode_wilayah.substring(0,6)
                        }
                    });

                    storeKabupaten.on('load', function(store){
                        var rec = store.getAt(0);

                        if(rec){
                            formSekolah.down('displayfield[name=kabupaten]').setValue(rec.data.nama);

                            var storePropinsi = new Admin.store.MstWilayah();

                            storePropinsi.load({
                                params:{
                                    kode_wilayah: rec.data.mst_kode_wilayah.substring(0,6)
                                }
                            });

                            storePropinsi.on('load', function(store){
                                var rec = store.getAt(0);

                                if(rec){
                                    formSekolah.down('displayfield[name=propinsi]').setValue(rec.data.nama);
                                }
                            });
                        }
                    });
                }
            });

            // panel.update('<b style="font-size:35px">'+rec.data.nama+'</b>'+'<div style="padding-top:7px;font-size:15px;margin-left:-10px">('+rec.data.npsn+')</div>'
                            // +'');

            panel.add({
                xtype: 'tbtext',
                text: '<b style="font-size:35px;color:white">'+rec.data.nama+'</b>'+'<span style="padding-top:7px;font-size:15px;margin-left:10px;color:white">('+rec.data.npsn+')</span>'
            });

            // panel.setTitle('<span style="font-size:12px;color:white"><a style="color:white;text-decoration:none" href="#SatuanPendidikan">Data Pokok Satuan Pendidikan</a> / ' + rec.data.nama + '</span>');

        });

        // var storeSanitasi  = new Admin.store.Sanitasi();

        // storeSanitasi.load({
        //     params:{
        //         sekolah_id: profilPanel.sekolah_id,
        //         Soft_delete:0,
        //         semester_id: session.semester_id
        //     }
        // });

        // storeSanitasi.on('load', function(store){
        //     var rec = store.getAt(0);

        //     var formSanitasi = profilPanel.down('tabpanel').down('panel[itemId=profilSekolah]').down('tabpanel[itemId=tabDataRinci]').down('sanitasiform');

        //     if(rec){

        //         formSanitasi.loadRecord(rec);
        //     }

        // });

        // var storeSekolahLong  = new Admin.store.SekolahLongitudinal();

        // storeSekolahLong.load({
        //     params:{
        //         sekolah_id: profilPanel.sekolah_id,
        //         Soft_delete:0,
        //         semester_id: session.semester_id
        //     }
        // });

        // storeSekolahLong.on('load', function(store){

        //     var rec = store.getAt(0);

        //     var formSekLong = profilPanel.down('tabpanel').down('panel[itemId=profilSekolah]').down('tabpanel[itemId=tabDataRinci]').down('sekolahlongitudinalform');

        //     // formSekLong.setTitle('tes');

        //     console.log(session.semester_id);

        //     if(rec){

        //         formSekLong.loadRecord(rec);
        //     }else{
        //         var storeSekolahLongPrev  = new Admin.store.SekolahLongitudinal();

        //         storeSekolahLongPrev.load({
        //             params:{
        //                 sekolah_id: profilPanel.sekolah_id,
        //                 Soft_delete:0,
        //                 semester_id: '20142'
        //             }
        //         });

        //         storeSekolahLongPrev.on('load', function(store){

        //             var rec = store.getAt(0);

        //             formSekLong.loadRecord(rec);
        //         });

        //     }

        // });

        // var gridBlockgrant = profilPanel.down('tabpanel').down('panel[itemId=profilSekolah]').down('tabpanel[itemId=tabDataRinci]').down('blockgrantgrid');

        // gridBlockgrant.getStore().load({
        //     params:{
        //         sekolah_id: profilPanel.sekolah_id,
        //         Soft_delete:0
        //     }
        // })
    },
    setupPdPerJurusan: function(panel){
        var store = new Admin.store.PdPerJurusan();

        var profilPanel = panel.up('profilsatuanpendidikan');

        store.on('load', function(store){

            var data = store.data;
            var total = 0;
            var i = 0;

            data.each(function(record){
                total += record.data.jumlah;
            });

            var warna = ['research',
                        'finance',
                        'marketing',
                        'hijau'];


            data.each(function(record){
                var cls_prog = 'bottom-indent service-'+warna[i];

                var persen = record.data.jumlah / total * 100;
                var values = record.data.jumlah / total;

                panel.add(
                    {
                        xtype:'component',
                        data: {
                            name: record.data.nama_jurusan_sp,
                            value: persen.toFixed(2)
                        },
                        tpl: '<div class="left-aligned-div">'+record.data.nama_jurusan_sp+'</div><div class="right-aligned-div"><b>'+record.data.jumlah+'</b> ('+persen.toFixed(2)+'%)</div>'
                    },
                    {
                        xtype: 'progressbar',
                        cls: cls_prog,
                        height: 4,
                        minHeight: 4,
                        value: values
                    }
                );

                i++;

            });

        });

        store.load({
            params:{
                sekolah_id: profilPanel.sekolah_id
            }
        });
    },
    PtkGridSetup: function(grid){

        var sekolah_id = grid.up('tabpanel').up('profilsatuanpendidikan').sekolah_id;

        // console.log(sekolah_id);

        grid.getStore().on('beforeload', function(store){
            Ext.apply(store.proxy.extraParams, {
                Soft_delete: 0,
                sekolah_id: sekolah_id
            });
        });

        grid.getStore().load();
    },
    dataRinciSekolah: function(btn){
        var panel = btn.up('sekolahform').up('panel').up('tabpanel').up('profilsatuanpendidikan');
        var height = Ext.Element.getViewportHeight() - 200;
        var width = Ext.Element.getViewportWidth() - 250;
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        // console.log(panel.sekolah_id);

        var win = Ext.create('Admin.view.SatuanPendidikan.DataRinciWindow',{
            height: height,
            width: width,
            sekolah_id: panel.sekolah_id
            // title: 'Data Rinci Sekolah ....'
        });

        Ext.MessageBox.show({
            msg: 'memuat data ...',
            progressText: 'Memuat data ...',
            width:300,
            wait:true,
            waitConfig: {interval:100},
            iconHeight: 50
        });

        var storeSanitasi  = new Admin.store.Sanitasi();

        storeSanitasi.load({
            params:{
                sekolah_id: panel.sekolah_id,
                Soft_delete:0,
                semester_id: session.semester_id
            }
        });

        storeSanitasi.on('load', function(store){
            var rec = store.getAt(0);

            var formSanitasi = win.down('tabpanel').down('sanitasiform');

            if(rec){

                formSanitasi.loadRecord(rec);
            }

        });

        var storeSekolahLong  = new Admin.store.SekolahLongitudinal();

        storeSekolahLong.load({
            params:{
                sekolah_id: panel.sekolah_id,
                Soft_delete:0,
                semester_id: session.semester_id
            }
        });

        storeSekolahLong.on('load', function(store){

            Ext.MessageBox.hide();

            var rec = store.getAt(0);

            var formSekLong = win.down('tabpanel').down('sekolahlongitudinalform');

            if(rec){

                formSekLong.loadRecord(rec);

            }else{

                var storeSekolahLongPrev  = new Admin.store.SekolahLongitudinal();

                storeSekolahLongPrev.load({
                    params:{
                        sekolah_id: panel.sekolah_id,
                        Soft_delete:0,
                        semester_id: '20142'
                    }
                });

                storeSekolahLongPrev.on('load', function(store){

                    var rec = store.getAt(0);

                    formSekLong.loadRecord(rec);
                });

            }

        });

        win.show();
    },
    blockgrandGridSetup: function(grid){
        var win = grid.up('tabpanel').up('window');

        if(win){
           grid.getStore().on('beforeload', function(store){
                Ext.apply(store.proxy.extraParams, {
                    Soft_delete: 0,
                    sekolah_id: win.sekolah_id
                });
            });
        }

        grid.getStore().load();
    },
    tombolUnduhProfilSekolah: function(btn){

        // var getParams = document.URL.split("/");

        var hash = window.location.hash.substr(1);
        var hashArr = hash.split('/');

        // console.log(getParams[4]);

        if(hashArr){
            var sekolah_id = hashArr[1];            
        }


        var url = "getProfilSekolah/" + sekolah_id;

        window.location.replace(url);

    }
});
