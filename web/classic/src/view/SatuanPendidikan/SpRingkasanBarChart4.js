Ext.define('Admin.view.SatuanPendidikan.SpRingkasanBarChart4', {
    extend: 'Ext.Panel',
    ui: 'yellow',
    xtype: 'springkasanbarchart4', 
    // width: 650,
    initComponent: function() {
        var me = this;

        this.myDataStore = Ext.create('Admin.store.SatuanPendidikanRingkasan');

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        
        var titleV =  [ 'PD Negeri','PD Swasta'];
        var yFieldV = [ 'pd_negeri','pd_swasta'];    

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            items:[{
                text: 'Simpan',
                iconCls: 'fa fa-save',
                ui: 'soft-blue',
                listeners:{
                    click: function(btn){
                        Ext.MessageBox.confirm('Konfirmasi Unduh', 'Apakah Anda ingin mengunduh grafik ini sebagai gambar?', function (choice) {
                            if (choice == 'yes') {
                                var chart = btn.up('toolbar').up('panel').down('cartesian');

                                // console.log(chart);

                                if (Ext.os.is.Desktop) {
                                    chart.download({
                                        filename: 'Chart Rekap PD Berdasarkan Status Sekolah'
                                    });
                                } else {
                                    chart.preview();
                                }
                              }
                        });
                    }
                }
            }]
        }];    

        me.items = [{
            xtype: 'cartesian',
            width: '100%',
            height: 400,
            flipXY: true,
            legend: {
                docked: 'bottom'
            },
            interactions: ['itemhighlight'],
            store: this.myDataStore,
            insetPadding: {
                top: 40,
                left: 40,
                right: 40,
                bottom: 40
            },
            axes: [{
                type: 'numeric',
                position: 'bottom',
                adjustByMajorUnit: true,
                grid: true,
                fields: yFieldV,
                minimum: 0
            }, {
                type: 'category',
                position: 'left',
                grid: true,
                fields: ['nama']
            }],
            series: [{
                type: 'bar',
                axis: 'bottom',
                title: titleV,
                xField: 'nama',
                yField: yFieldV,
                stacked: true,
                style: {
                    opacity: 0.80
                },
                highlight: {
                    fillStyle: 'yellow'
                },
                label:{
                    display: 'insideStart',
                    // color: '#ffffff',
                    orientation : 'horizontal',
                    font: '10px Helvetica',
                    field: yFieldV,
                    renderer: function(text, sprite, config, rendererData, index){
                        return text;
                    }
                },
                tooltip: {
                    style: 'background: #fff',
                    renderer: function(storeItem, item) {
                        var browser = item.series.getTitle()[Ext.Array.indexOf(item.series.getYField(), item.field)];
                        this.setHtml(browser + ' di wilayah ' + storeItem.get('nama') + ': ' + storeItem.get(item.field));
                    }
                }
            }]
        }];

        this.myDataStore.load();

        this.callParent();
    }
});