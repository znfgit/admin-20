Ext.define('Admin.view.SatuanPendidikan.BlockgrantGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.blockgrantgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    createStore: function() {
        return Ext.create('Admin.store.Blockgrant');
    },
    createEditing: function() {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Tambah',
                // iconCls: 'fa fa-plus fa-lg glyph-dark-green glyph-shadow',
                iconCls: 'ic-add',
                scope: this,
                action: 'add'
            }, {
                xtype: 'button',
                text: 'Ubah',
                iconCls: 'fa fc-book_edit fa-lg glyph-dark-orange glyph-shadow',
                // iconCls: 'ic-pencil',
                itemId: 'edit',
                scope: this,
                action: 'edit'
            }, {
                xtype: 'button',
                text: 'Simpan',
                // iconCls: 'fa fa-check fa-lg glyph-blue glyph-shadow',
                iconCls: 'ic-disk',
                itemId: 'save',
                scope: this,
                action: 'save'
            }, {
                xtype: 'button',
                text: 'Hapus',
                // iconCls: 'fa fa-remove fa-lg glyph-red glyph-shadow',
                iconCls: 'ic-cross',
                itemId: 'delete',
                scope: this,
                action: 'delete'
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }      
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        // this.getSelectionModel().setSelectionMode('SINGLE');
        
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function(rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });
        
        var editing = this.createEditing();

        // this.rowEditing = editing;
        // this.plugins = [editing];
        
        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 80,
            sortable: true,
            dataIndex: 'blockgrant_id',
			hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
				,maxLength: 11
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'Sekolah',
            tooltip: 'Sekolah',
            width: 80,
            sortable: true,
            dataIndex: 'sekolah_id',
			hideable: false,
            hidden: true,
            renderer: function(v,p,r) {
                return r.data.sekolah_id_str;
            },
            field: {
                displayField: 'nama',
                xtype: 'sekolahcombo',
                listeners: {
                    change: function(combo, newValue, oldValue, eOpts ) {
                        var grid = combo.up('gridpanel');
                        var selections = grid.getSelectionModel().getSelection();
                        var r = selections[0];
                        r.set('sekolah_id_str', combo.getRawValue());
                    }
                }
            }
        },{
            header: 'Nama',
            tooltip: 'Nama',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
			hideable: false,
            field: {
                xtype: 'textfield'
				,maxLength: 50
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'Tahun',
            tooltip: 'Tahun',
            width: 100,
            sortable: true,
            dataIndex: 'tahun',
			hideable: false,
            field: {
                xtype: 'numberfield'
				,maxLength: 262144
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'Jenis Bantuan',
            tooltip: 'Jenis Bantuan',
            width: 200,
            sortable: true,
            dataIndex: 'jenis_bantuan_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.jenis_bantuan_id_str;
            },
            field: {
                displayField: 'nama',
                xtype: 'jenisbantuancombo',
                listeners: {
                    change: function(combo, newValue, oldValue, eOpts ) {
                        var grid = combo.up('gridpanel');
                        var selections = grid.getSelectionModel().getSelection();
                        var r = selections[0];
                        r.set('jenis_bantuan_id_str', combo.getRawValue());
                    }
                }
            }
        },{
            header: 'Sumber Dana',
            tooltip: 'Sumber Dana',
            width: 200,
            sortable: true,
            dataIndex: 'sumber_dana_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.sumber_dana_id_str;
            },
            field: {
                displayField: 'nama',
                xtype: 'sumberdanacombo',
                listeners: {
                    change: function(combo, newValue, oldValue, eOpts ) {
                        var grid = combo.up('gridpanel');
                        var selections = grid.getSelectionModel().getSelection();
                        var r = selections[0];
                        r.set('sumber_dana_id_str', combo.getRawValue());
                    }
                }
            }
        },{
            header: 'Besar Bantuan',
            tooltip: 'Besar Bantuan',
            width: 200,
            sortable: true,
            dataIndex: 'besar_bantuan',
			hideable: false,
            field: {
                xtype: 'numberfield'
				,maxLength: 983040
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'Dana Pendamping',
            tooltip: 'Dana Pendamping',
            width: 200,
            sortable: true,
            dataIndex: 'dana_pendamping',
			hideable: false,
            field: {
                xtype: 'numberfield'
				,maxLength: 983040
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'Peruntukan Dana',
            tooltip: 'Peruntukan Dana',
            width: 200,
            sortable: true,
            dataIndex: 'peruntukan_dana',
			hideable: false,
            field: {
                xtype: 'textfield'
				,maxLength: 50
				,enforceMaxLength: true
				,minValue: 0
            }
        },{
            header: 'Asal Data',
            tooltip: 'Asal Data',
            width: 200,
            sortable: true,
            dataIndex: 'asal_data',
			hideable: false,
            hidden: true,
            field: {
                xtype: 'textfield'
				,maxLength: 1
				,enforceMaxLength: true
				,minValue: 0
            }
        }];
        
        // this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();
        
        this.callParent(arguments);
    }
});