Ext.define('Admin.view.SatuanPendidikan.SatuanPendidikanController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.satuanpendidikan',
    SatuanPendidikanGridSetup: function(grid){

        grid.down('toolbar').add('->',{
            text: 'Unduh Xlsx',
            iconCls: 'x-fa fa-download',
            listeners:{
                click: function(btn){

                    var form = grid.up('satuanpendidikan').down('filterform');

                    var keywordField = form.down('textfield[name=keyword]');
                    var prop = form.down('mstwilayahcombo[name=comboprop]');
                    var kab = form.down('mstwilayahcombo[name=combokab]');
                    var kec = form.down('mstwilayahcombo[name=combokec]');
                    var bp = form.down('bentukpendidikancombo');
                    var status = form.down('combobox[name=pilihstatuscombo]');

                    window.location.href="Excel/generate?class=SatuanPendidikan&method=SatuanPendidikan&tipe=1&keyword="+keywordField.getValue()+"&propinsi="+prop.getValue()+"&kabupaten="+kab.getValue()+"&kecamatan="+kec.getValue()+"&bentuk_pendidikan_id="+bp.getValue()+"&status_sekolah="+status.getValue()+"";
                }
            }
        },'-',{
            iconCls: 'fa fa-refresh',
            listeners:{
                click: 'refresh'
            }
        });

        grid.getStore().on('beforeload', function(store){

            var form = grid.up('satuanpendidikan').down('filterform');

            var keywordField = form.down('textfield[name=keyword]');
            var prop = form.down('mstwilayahcombo[name=comboprop]');
            var kab = form.down('mstwilayahcombo[name=combokab]');
            var kec = form.down('mstwilayahcombo[name=combokec]');
            var bp = form.down('bentukpendidikancombo');
            var status = form.down('combobox[name=pilihstatuscombo]');

            Ext.apply(store.proxy.extraParams, {
                keyword: keywordField.getValue(),
                propinsi: prop.getValue(),
                kabupaten: kab.getValue(),
                kecamatan: kec.getValue(),
                bentuk_pendidikan_id: bp.getValue(),
                status_sekolah: status.getValue()
            });

        });

    	// grid.getStore().on('load', function(store){

     //        var rec = store.getAt(0);

     //        // console.log(rec);

     //        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

     //        if(session.id_level_wilayah == '0'){

     //            if(rec.data.id_level_wilayah == 1){
     //                grid.down('toolbar').down('button[name=kembaliButton]').hide();
     //            }else{
     //                grid.down('toolbar').down('button[name=kembaliButton]').show();
     //            }

     //        }else if(session.id_level_wilayah == '1'){

     //            if(rec.data.id_level_wilayah == 2){
     //                grid.down('toolbar').down('button[name=kembaliButton]').hide();
     //            }else{
     //                grid.down('toolbar').down('button[name=kembaliButton]').show();
     //            }

     //        }else if(session.id_level_wilayah == '2'){

     //            if(rec.data.id_level_wilayah == 3){
     //                grid.down('toolbar').down('button[name=kembaliButton]').hide();
     //            }else{
     //                grid.down('toolbar').down('button[name=kembaliButton]').show();
     //            }

     //        }

     //    });
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        if(session.peran_id == 1 && session.kode_wilayah.trim() == '000000'){
            grid.down('toolbar').add({
                xtype: 'button',
                itemId: 'koreg',
                text: 'Kode Registrasi',
                iconCls: 'fa fa-database',
                ui: 'soft-green',
                listeners:{
                    click: 'bukaKoreg'
                }
            })
        }

    	grid.getStore().load();
    },
    bukaKoreg: function(btn){
        // Ext.MessageBox.alert('tes');
        var grid = btn.up('gridpanel');

        var selections = grid.getSelectionModel().getSelection();
        var r = selections[0];
        if (!r) {
            Ext.MessageBox.alert('Error', 'Mohon pilih salah satu baris');
            return;
        }

        if(r.data.koreggen != 0){
            var textBtn = 'Reset';
        }else{
            var textBtn = 'Generate';
        }

        var win = Ext.create('Ext.window.Window',{
            height: 200,
            width: 400,
            modal: true,
            title: 'Kode Registrasi ' + r.data.nama,
            html: '<div style="margin:30px">Nama: ' + r.data.nama + '<br>'
                    + 'NPSN: ' + r.data.npsn + '<br>'
                    + 'Kode Registrasi: <b style="font-size:20px">' + r.data.koreggen + '</b><br></div>',
            buttons:[{
                text: textBtn + ' Koreg',
                listeners:{
                    click: function(btn){

                    }
                }
            }]
        });

        win.show();

    },
    bentukPendidikanComboChange: function(combo){
    	combo.up('gridpanel').getStore().reload();
    },
    filterFormSetup: function(form){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        if(session.id_level_wilayah == 1){
            form.down('mstwilayahcombo[name=comboprop]').setValue(session.kode_wilayah);

            form.down('mstwilayahcombo[name=comboprop]').hide();
        }else if(session.id_level_wilayah == 2){
            form.down('mstwilayahcombo[name=combokab]').setValue(session.kode_wilayah);

            form.down('mstwilayahcombo[name=comboprop]').hide();
            form.down('mstwilayahcombo[name=combokab]').hide();
        }
    }

    // TODO - Add control logic or remove if not needed
});
