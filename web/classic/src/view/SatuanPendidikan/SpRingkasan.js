Ext.define('Admin.view.SatuanPendidikan.SpRingkasan', {
    extend: 'Ext.container.Container',
    xtype: 'springkasan',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'springkasan',

    cls: 'springkasan-container',

    // layout: 'responsivecolumn',

    // scrollable: 'y',
    listeners:{
        afterrender: function(panel){
            var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

            // console.log(session);

            if(session.id_level_wilayah == 2){
                panel.down('tabpanel').add({
                    xtype: 'springkasanbarchart',
                    iconCls: 'x-fa fa-bar-chart',
                    title: 'Rekap Sekolah Berdasarkan Status',
                    responsiveCls: 'big-100 small-100',
                    minHeight:200,
                },{
                    xtype: 'springkasanbarchart2',
                    ui: 'soft-blue',
                    iconCls: 'x-fa fa-bar-chart',
                    title: 'Rekap Guru Berdasarkan Status Sekolah',
                    responsiveCls: 'big-50 small-100',
                    minHeight:200,
                },{
                    xtype: 'springkasanbarchart3',
                    ui: 'soft-green',
                    iconCls: 'x-fa fa-bar-chart',
                    title: 'Rekap Pegawai Berdasarkan Status Sekolah',
                    responsiveCls: 'big-50 small-100',
                    minHeight:200,
                },{
                    xtype: 'springkasanbarchart4',
                    iconCls: 'x-fa fa-bar-chart',
                    title: 'Rekap Peserta Didik Berdasarkan Status Sekolah',
                    responsiveCls: 'big-50 small-100',
                    minHeight:200,
                },{
                    xtype: 'springkasanbarchart5',
                    iconCls: 'x-fa fa-bar-chart',
                    ui: 'soft-red',
                    title: 'Rekap Rombel Berdasarkan Status Sekolah',
                    responsiveCls: 'big-50 small-100',
                    minHeight:200,
                });
            }
        }
    },
    layout: {type:'border'},
    items: [
        {
            xtype: 'tabpanel',
            region: 'center',
            items:[{
                title: 'Ringkasan Rekap Satuan Pendidikan',
                xtype: 'springkasangrid',
                iconCls: 'x-fa fa-building'
                // region:'center',
                // scrollable: 'x',
                // Always 100% of container
                // responsiveCls: 'big-100',
                // minHeight:200,
                // html: 'isinya daftar pengiriman'
                // listeners:{
                //     click: function(grid){
                //         grid.columns[0].setWidth(200);
                //     }
                // }
            }]
        }
    ]
});
