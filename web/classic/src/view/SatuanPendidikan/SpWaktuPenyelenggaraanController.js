Ext.define('Admin.view.SatuanPendidikan.SpWaktuPenyelenggaraanController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.spwaktupenyelenggaraan',
    SpWaktuPenyelenggaraanGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });


        });

        grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            // console.log(rec);

            if(rec){
                
                if(session.id_level_wilayah == '0'){

                    if(rec.data.id_level_wilayah == 1){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }

                }else if(session.id_level_wilayah == '1'){

                    if(rec.data.id_level_wilayah == 2){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }                

                }else if(session.id_level_wilayah == '2'){

                    if(rec.data.id_level_wilayah == 3){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }

                }
            }

            var data = store.data;

            var negeri_pagi = 0;
            var swasta_pagi = 0;

            var negeri_siang = 0;
            var swasta_siang = 0;

            var negeri_sore = 0;
            var swasta_sore = 0;

            var negeri_malam = 0;
            var swasta_malam = 0;

            var negeri_kombinasi = 0;
            var swasta_kombinasi = 0;

            var negeri_sehari_penuh_5 = 0;
            var swasta_sehari_penuh_5 = 0;

            var negeri_sehari_penuh_6 = 0;
            var swasta_sehari_penuh_6 = 0;

            var negeri_lainnya = 0;
            var swasta_lainnya = 0;

            if(data){

                data.each(function(record){
                    // console.log(record.data);

                    var dataRecord = record.data;
                    // console.log(dataRecord);

                    negeri_pagi = negeri_pagi+dataRecord.negeri_pagi;
                    swasta_pagi = swasta_pagi+dataRecord.swasta_pagi;
                    
                    negeri_siang = negeri_siang+dataRecord.negeri_siang;
                    swasta_siang = swasta_siang+dataRecord.swasta_siang;
                    
                    negeri_sore = negeri_sore+dataRecord.negeri_sore;
                    swasta_sore = swasta_sore+dataRecord.swasta_sore;
                    
                    negeri_malam = negeri_malam+dataRecord.negeri_malam;
                    swasta_malam = swasta_malam+dataRecord.swasta_malam;
                    
                    negeri_kombinasi = negeri_kombinasi+dataRecord.negeri_kombinasi;
                    swasta_kombinasi = swasta_kombinasi+dataRecord.swasta_kombinasi;
                    
                    negeri_sehari_penuh_5 = negeri_sehari_penuh_5+dataRecord.negeri_sehari_penuh_5;
                    swasta_sehari_penuh_5 = swasta_sehari_penuh_5+dataRecord.swasta_sehari_penuh_5;
                    
                    negeri_sehari_penuh_6 = negeri_sehari_penuh_6+dataRecord.negeri_sehari_penuh_6;
                    swasta_sehari_penuh_6 = swasta_sehari_penuh_6+dataRecord.swasta_sehari_penuh_6;
                    
                    negeri_lainnya = negeri_lainnya+dataRecord.negeri_lainnya;
                    swasta_lainnya = swasta_lainnya+dataRecord.swasta_lainnya;
                    
                });

                // console.log(guru_negeri);

                var recordConfig = {
                    no : '',
                    nama : '<b>Total<b>',
                    negeri_pagi: negeri_pagi,
                    swasta_pagi: swasta_pagi,
                    negeri_siang: negeri_siang,
                    swasta_siang: swasta_siang,
                    negeri_sore: negeri_sore,
                    swasta_sore: swasta_sore,
                    negeri_malam: negeri_malam,
                    swasta_malam: swasta_malam,
                    negeri_kombinasi: negeri_kombinasi,
                    swasta_kombinasi: swasta_kombinasi,
                    negeri_sehari_penuh_5: negeri_sehari_penuh_5,
                    swasta_sehari_penuh_5: swasta_sehari_penuh_5,
                    negeri_sehari_penuh_6: negeri_sehari_penuh_6,
                    swasta_sehari_penuh_6: swasta_sehari_penuh_6,
                    negeri_lainnya: negeri_lainnya,
                    swasta_lainnya: swasta_lainnya,
                };
                
                var r = new Admin.model.SatuanPendidikanWaktuPenyelenggaraan(recordConfig);
                
                grid.store.add(r);

            }
        });

        grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
        combo.up('gridpanel').getStore().reload();
    },
    SpWaktuPenyelenggaraanSpGridSetup: function(grid){

        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var panel = grid.up('spwaktupenyelenggaraansp');

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: panel.id_level_wilayah,
                kode_wilayah: panel.kode_wilayah,
                oke: 'sip'
            });


        });

        grid.getStore().on('load', function(store){

            var panel = grid.up('spwaktupenyelenggaraansp');
            var rec = store.getAt(0);

            if(rec){
                if(panel.id_level_wilayah == 1){
                    grid.setTitle('Satuan Pendidikan Berdasarkan Waktu Penyelenggaraan di ' + rec.data.propinsi);
                }else if(panel.id_level_wilayah == 2){
                    grid.setTitle('Satuan Pendidikan Berdasarkan Waktu Penyelenggaraan di ' + rec.data.kabupaten);
                }else if(panel.id_level_wilayah == 3){
                    grid.setTitle('Satuan Pendidikan Berdasarkan Waktu Penyelenggaraan di ' + rec.data.kecamatan);
                }
            }


        });

        grid.getStore().load();
    }

    // TODO - Add control logic or remove if not needed
});
