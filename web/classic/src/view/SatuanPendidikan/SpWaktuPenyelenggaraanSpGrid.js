Ext.define('Admin.view.SatuanPendidikan.SpWaktuPenyelenggaraanSpGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.spwaktupenyelenggaraanspgrid',
    title: '',
    selType: 'rowmodel',
    columnLines: true,
    controller: 'spwaktupenyelenggaraan',
    listeners:{
        afterrender:'SpWaktuPenyelenggaraanSpGridSetup'
    },
    createStore: function() {
        return Ext.create('Admin.store.SatuanPendidikanWaktuPenyelenggaraanSp');
    },
    createDockedItems: function() {
        var hash = window.location.hash.substr(1);
        var hashArr = hash.split('/');

        // console.log(getParams);

        var id_level_wilayah = hashArr[1];
        var kode_wilayah = hashArr[2];


        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                href:'#SpWaktuPenyelenggaraan',
                hrefTarget: '_self'
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'SatuanPendidikan',
                paramMethod: 'SatuanPendidikanWaktuPenyelenggaraanSp',
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah,
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        Ext.apply(this, this.initialConfig);
        
        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            // locked: true,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1);
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
        /*this.columns = [
        {
            header: 'No',
            width: 60,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            // locked: true,
            renderer: function(v,p,r){
                // console.log(v);
                // console.log(p.recordIndex);
                // console.log(r.store.currentPage);
                return (((r.store.currentPage)-1)*50)+(p.recordIndex + 1);
            }
        }
        ,{*/
            header: 'Nama SP',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
			hideable: false,
            // locked: true,
            renderer: function(v,p,r){
                return '<a style="color:#228ab7;text-decoration:none" href="#ProfilSatuanPendidikan/'+r.data.sekolah_id+'">'+v+'</a>';
            }
        },{
            header: 'NPSN',
            width: 100,
            sortable: true,
            dataIndex: 'npsn',
            hideable: false,
            locked: false
        },{
            header: 'Bentuk',
            width: 100,
            sortable: true,
            dataIndex: 'bentuk_pendidikan_id',
            hideable: false,
            locked: false,
            renderer: function(v,p,r){
                return r.data.bentuk;
            }
        },{
            header: 'Status',
            width: 100,
            sortable: true,
            dataIndex: 'status_sekolah',
            hideable: false,
            locked: false,
            renderer: function(v,p,r){
                switch (v) {
                    case "1": return 'Negeri'; break;                  
                    case "2" : return 'Swasta'; break;                  
                    default : return '-'; break;
                }
            }
        },{
            header: 'Kecamatan',
            width: 100,
            sortable: true,
            dataIndex: 'kecamatan',
            hideable: false,
            locked: false
        },{
            header: 'Kabupaten',
            width: 100,
            sortable: true,
            dataIndex: 'kabupaten',
            hideable: false,
            locked: false
        },{
            header: 'Provinsi',
            width: 100,
            sortable: true,
            dataIndex: 'propinsi',
            hideable: false,
            locked: false
        },{
            header: 'Waktu Penyelenggaraan',
            width: 200,
            sortable: true,
            dataIndex: 'waktu_penyelenggaraan_id_str',
            hideable: false,
            locked: false,
            align: 'center'
        },{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }
        ];
        
        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();
        
        this.callParent(arguments);

        // this.store.load();
    }
});