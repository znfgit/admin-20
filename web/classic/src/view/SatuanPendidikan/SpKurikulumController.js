Ext.define('Admin.view.SatuanPendidikan.SpKurikulumController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.spKurikulum',
    SpKurikulumGridSetup: function(grid){

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });

        });

    	grid.getStore().on('load', function(store){

            var rec = store.getAt(0);

            // console.log(rec);

            // if(rec){
            //     if(session.id_level_wilayah == '0'){

            //         if(rec.data.id_level_wilayah == 1){
            //             grid.down('toolbar').down('button[name=kembaliButton]').hide();
            //         }else{
            //             grid.down('toolbar').down('button[name=kembaliButton]').show();
            //         }

            //     }else if(session.id_level_wilayah == '1'){

            //         if(rec.data.id_level_wilayah == 2){
            //             grid.down('toolbar').down('button[name=kembaliButton]').hide();
            //         }else{
            //             grid.down('toolbar').down('button[name=kembaliButton]').show();
            //         }

            //     }else if(session.id_level_wilayah == '2'){

            //         if(rec.data.id_level_wilayah == 3){
            //             grid.down('toolbar').down('button[name=kembaliButton]').hide();
            //         }else{
            //             grid.down('toolbar').down('button[name=kembaliButton]').show();
            //         }

            //     }
            // }

            // store.data.each(function(record){
            //     // console.log(record.data);

            //     var arr = [];

            //     for (var property in record.data) {
            //         if (record.data.hasOwnProperty(property)) {
            //             // do stuff
            //             // console.log(property + ' = ' +record.data[property]);

            //             arr[property] = arr[property] + record.data[property];
            //         }
            //     }

            //     console.log(arr);

            // });

            // var data = store.data;

            // var k13_negeri = 0;
            // var k13_swasta = 0;
            // var k13_total = 0;
            // var ktsp_negeri = 0;
            // var ktsp_swasta = 0;
            // var ktsp_total = 0;

            // data.each(function(record){
            //     // console.log(record.data);

            //     var dataRecord = record.data;
            //     // console.log(dataRecord);

            //     k13_negeri = k13_negeri+dataRecord.k13_negeri;
            //     k13_swasta = k13_swasta+dataRecord.k13_swasta;
            //     k13_total = k13_total+dataRecord.k13_total;

            //     ktsp_negeri = ktsp_negeri+dataRecord.ktsp_negeri;
            //     ktsp_swasta = ktsp_swasta+dataRecord.ktsp_swasta;
            //     ktsp_total = ktsp_total+dataRecord.ktsp_total;

            // });

            // // console.log(k13_negeri);

            // var recordConfig = {
            //     no : '',
            //     nama : '<b>Total<b>',
            //     k13_negeri : k13_negeri,
            //     k13_swasta : k13_swasta,
            //     k13_total : k13_total,
            //     ktsp_negeri: ktsp_negeri,
            //     ktsp_swasta: ktsp_swasta,
            //     ktsp_total : ktsp_total,
            // };

            // var r = new Admin.model.SatuanPendidikanKurikulum(recordConfig);

            // grid.store.add(r);

        });


        grid.getStore().load();
    }
    ,SpKurikulumSpGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var panel = grid.up('spkurikulumsp');
            // console.log(panel);
            // console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: panel.id_level_wilayah,
                kode_wilayah: panel.kode_wilayah
            });


        });

        grid.getStore().on('load', function(store){

            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

        });

        grid.getStore().load();
    }
    ,bentukPendidikanComboChange: function(combo){
        combo.up('gridpanel').getStore().reload();
    }
    // TODO - Add control logic or remove if not needed
});
