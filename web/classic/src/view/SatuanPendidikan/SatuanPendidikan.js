Ext.define('Admin.view.SatuanPendidikan.SatuanPendidikan', {
    extend: 'Ext.container.Container',
    xtype: 'satuanpendidikan',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    id: 'satuanpendidikan',

    controller: 'satuanpendidikan',
    
    viewModel: {
        type: 'dashboard'
    },

    // scrollable: 'y',

    // layout: 'responsivecolumn',

    // items: [
    //     {
    //         xtype: 'panel',
    //         title: 'Kriteria',
    //         responsiveCls: 'big-40 small-100',
    //         cls: 'dashboard-main-chart shadow-panel',
    //         minHeight:550
    //     },
    //     {
    //         xtype: 'panel',
    //         title: 'Data Pokok Satuan Pendidikan',
    //         cls: 'dashboard-main-chart shadow-panel',
    //         responsiveCls: 'big-60 small-100',
    //         minHeight: 550
    //     }
    // ]

    // layout: {
    //     type: 'hbox',
    //     align: 'stretch'
    // },

    // margin: '20 0 0 20',
    
    layout: 'border',

    items: [
        {
            xtype: 'filterform',
            title: 'Kriteria',
            // width: '35%',
            region: 'west',
            minWidth: 240,
            maxWidth: 300,
            split: true,
            collapsible: true,
            // minHeight:550,
            ui: 'soft-red',
            // margin: '0 20 20 0'
        },
        {
            xtype: 'satuanpendidikangrid',
            name: 'tabelDataPokok',
            region: 'center',
            ui: 'soft-green',
            title: 'Data Pokok Satuan Pendidikan',
            // margin: '0 20 20 0',
            flex: 1,
            // minHeight: 550
        }
    ]
});
