Ext.define('Admin.view.SatuanPendidikan.FilterForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.filterform',
    bodyPadding: 10,
    fieldDefaults: {
        anchor: '100%',
        labelWidth: 120
    },
    listeners:{
        afterrender: 'filterFormSetup'
    },
    initComponent: function() {
        
        // var record = this.initialConfig.record ? this.initialConfig.record : false;
        // if (record){
        //     this.listeners = {
        //         afterrender: function(form, options) {
        //             form.loadRecord(record);
        //         }
        //     };
        // }

        var pilihStatusStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'nama'],
            data : [
                {"id":"1", "nama":"Negeri"},
                {"id":"2", "nama":"Swasta"},
                {"id":"999", "nama":"Semua"}
            ]
        });
        
        this.items = [{
            xtype: 'textfield'
            ,fieldLabel: 'Kata Kunci'
            ,labelAlign: 'top'
            ,width: '100%'
            ,allowBlank: true
            ,maxValue: 99999999
            ,minValue: 0
            ,maxLength: 100
            ,enforceMaxLength: false
            ,name: 'keyword'
            ,emptyText: 'Ketik Kata Kunci... (Tekan Enter)'
            ,enableKeyEvents: true
            ,listeners: {
                keypress: function (field, e) {
                    if (e.getKey() == e.ENTER){
                        field.up('form').up('container').down('gridpanel').getStore().load({
                            params:{
                                page:1,
                                start:0,
                                limit:50
                            }
                        });
                    }
                }
            }
        }
        ,{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            width: '100%',
            fieldLabel: 'Provinsi',
            name: 'comboprop',
            emptyText: 'Pilih Provinsi',
            listeners:{
                afterrender: function(combo,options){

                    combo.getStore().on('beforeload', function(store){
                        Ext.apply(store.proxy.extraParams, {
                            id_level_wilayah:1
                        });       
                    });

                },
                change: function(combo, options){
                    combo.up('form').down('mstwilayahcombo[name=combokab]').setValue('');
                    combo.up('form').down('mstwilayahcombo[name=combokab]').getStore().reload();
                }
            }
        }
        ,{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            fieldLabel: 'Kabupaten/Kota',
            name: 'combokab',
            width: '100%',
            emptyText: 'Pilih Kabupaten/Kota',
            listeners:{
                afterrender: function(combo,options){

                    combo.getStore().on('beforeload', function(store){

                        var comboprop = combo.up('form').down('mstwilayahcombo[name=comboprop]');

                        if(comboprop.getValue()){
                            Ext.apply(store.proxy.extraParams, {
                                id_level_wilayah:2,
                                mst_kode_wilayah: comboprop.getValue()
                            });       
                        }else{
                            Ext.apply(store.proxy.extraParams, {
                                id_level_wilayah:99
                            });
                        }       
                    });

                },
                change: function(combo, options){
                    combo.up('form').down('mstwilayahcombo[name=combokec]').setValue('');
                    combo.up('form').down('mstwilayahcombo[name=combokec]').getStore().reload();
                }
            }
        }
        ,{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            fieldLabel: 'Kecamatan',
            name: 'combokec',
            width: '100%',
            emptyText: 'Pilih Kecamatan',
            listeners:{
                afterrender: function(combo,options){

                    combo.getStore().on('beforeload', function(store){

                        var combokab = combo.up('form').down('mstwilayahcombo[name=combokab]');

                        if(combokab.getValue()){
                            Ext.apply(store.proxy.extraParams, {
                                id_level_wilayah:3,
                                mst_kode_wilayah: combokab.getValue()
                            });       
                        }else{
                            Ext.apply(store.proxy.extraParams, {
                                id_level_wilayah:99
                            });
                        }

                    });

                }
            }
        }
        ,{
            xtype:'bentukpendidikancombo',
            name: 'pilihbentukcombo',
            emptyText:'Pilih Bentuk Pendidikan...',
            itemId: 'pilihbentukcombo',
            labelAlign: 'top',
            width: '100%',
            fieldLabel: 'Bentuk Pendidikan',
            displayField: 'nama'   
        },{
            xtype:'combobox',
            name: 'pilihstatuscombo',
            emptyText:'Pilih Status Sekolah...',
            width: '100%',
            itemId: 'pilihstatuscombo',
            store: pilihStatusStore,
            labelAlign: 'top',
            fieldLabel: 'Status Sekolah',
            queryMode: 'local',
            displayField: 'nama',
            valueField: 'id'
        },{
            xtype: 'button',
            text: 'Cari',
            width: '100%',
            scale: 'medium',
            iconCls: 'x-fa fa-search',
            margin: '10 0 0 0',
            handler: function(btn){
                // btn.up('form').up('satuanpendidikan').down('panel[name=tabelDataPokok]').setTitle('testing');
                btn.up('form').up('container').down('gridpanel').getStore().load({
                    params:{
                        page:1,
                        start:0,
                        limit:50
                    }
                });
            }
        }];

        // this.buttons = [];

        this.callParent(arguments);
    }
});