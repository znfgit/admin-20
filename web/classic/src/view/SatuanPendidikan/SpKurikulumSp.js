Ext.define('Admin.view.SatuanPendidikan.SpKurikulumSp', {
    extend: 'Ext.container.Container',
    xtype: 'spkurikulumsp',

    requires: [
        'Admin.view.SatuanPendidikan.SpKurikulumSpGrid'
    ],

    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'Admin.view.SatuanPendidikan.SpKurikulumSpGrid'
    ],

    controller: 'spKurikulum',

    cls: 'spKurikulumSp-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',
    items: [{
        xtype: 'spkurikulumspgrid',
        title: 'Rekap Kurikulum Satuan Pendidikan',
        region: 'center'
    }]
});
