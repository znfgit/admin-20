Ext.define('Admin.view.SatuanPendidikan.SpWaktuPenyelenggaraan', {
    extend: 'Ext.container.Container',
    xtype: 'spwaktupenyelenggaraan',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'spwaktupenyelenggaraan',
    
    cls: 'spwaktupenyelenggaraan-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',
    items: [{
            xtype: 'tabpanel',
            region: 'center',
            items:[{
                xtype: 'spwaktupenyelenggaraangrid',
                title: 'Satuan Pendidikan Berdasarkan Waktu Penyelenggaraan',
                // Always 100% of container
                // responsiveCls: 'big-100',
                iconCls: 'x-fa fa-building',
                minHeight:'100%'
                // region: 'center'
                // html: 'isinya daftar pengiriman'
            },
            {
                xtype: 'spwaktupenyelenggaraanbarchart',
                title: 'Rekap Waktu Penyelenggaraan (Negeri)',
                iconCls: 'x-fa fa-bar-chart',
                // Always 100% of container
                // responsiveCls: 'big-100',
                minHeight:'100%'
                // region: 'center'
                // html: 'isinya daftar pengiriman'
            },
            {
                xtype: 'spwaktupenyelenggaraanbarchart2',
                title: 'Rekap Waktu Penyelenggaraan (Swasta)',
                iconCls: 'x-fa fa-bar-chart',
                // Always 100% of container
                // responsiveCls: 'big-100',
                minHeight:'100%'
                // region: 'center'
                // html: 'isinya daftar pengiriman'
            }]
        }
        
    ]
});
