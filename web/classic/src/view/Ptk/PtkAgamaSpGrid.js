Ext.define('Admin.view.Ptk.PtkAgamaSpGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ptkagamaspgrid',
    title: '',
    selType: 'rowmodel',
    columnLines: true,
    listeners:{
        afterrender:'PtkAgamaSpGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.PtkAgamaSp');
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    createDockedItems: function() {
        var hash = window.location.hash.substr(1);
        var hashArr = hash.split('/');

        // console.log(getParams);

        var id_level_wilayah = hashArr[1];
        var kode_wilayah = hashArr[2];

        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                href:'#PtkAgama',
                hrefTarget: '_self'
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype: 'jenisptkcombo',
                emptyText: 'Pilih Jenis PTK',
                width: 200,
                listeners:{
                    change: function(combo){
                        console.log('nendang bro');

                        combo.up('gridpanel').getStore().reload();
                    }
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'Ptk',
                paramMethod: 'PtkAgamaSp',
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah,
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        var grid = this;
        
        this.store = this.createStore();
        
        Ext.apply(this, this.initialConfig);

        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            // locked: true,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1);
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Nama SP',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            // locked: true,
            renderer: function(v,p,r){
                return '<a style="color:#228ab7;text-decoration:none" href="#ProfilSatuanPendidikan/'+r.data.sekolah_id+'">'+v+'</a>';
            }
        },{
            header: 'NPSN',
            width: 100,
            sortable: true,
            dataIndex: 'npsn',
            hideable: false
        },{
            header: 'Bentuk',
            width: 100,
            sortable: true,
            dataIndex: 'bentuk_pendidikan_id',
            hideable: false,
            renderer: function(v,p,r){
                return r.data.bentuk;
            }
        },{
            header: 'Status',
            width: 100,
            sortable: true,
            dataIndex: 'status_sekolah',
            hideable: false,
            renderer: function(v,p,r){
                if(v == 1){
                    return 'Negeri';
                }else{
                    return 'Swasta';
                }
            }
        },{
            header: 'Kecamatan',
            width: 100,
            sortable: true,
            dataIndex: 'kecamatan',
            hideable: false
        }
        // ,{
        //     header: 'Kabupaten/Kota',
        //     width: 110,
        //     sortable: true,
        //     dataIndex: 'kabupaten',
        //     hideable: false
        // },{
        //     header: 'Propinsi',
        //     width: 110,
        //     sortable: true,
        //     dataIndex: 'propinsi',
        //     hideable: false
        // }
        ,{
            text: 'Agama',
            columns: [{
                header: 'Islam',
                width: 85,
                sortable: true,
                dataIndex: 'islam',
                hideable: false,
                align:'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Kristen',
                width: 85,
                sortable: true,
                dataIndex: 'kristen',
                hideable: false,
                align:'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Katholik',
                width: 85,
                sortable: true,
                dataIndex: 'katholik',
                hideable: false,
                align:'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Hindu',
                width: 85,
                sortable: true,
                dataIndex: 'hindu',
                hideable: false,
                align:'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Budha',
                width: 85,
                sortable: true,
                dataIndex: 'budha',
                hideable: false,
                align:'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Konghucu',
                width: 85,
                sortable: true,
                dataIndex: 'konghucu',
                hideable: false,
                align:'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Kepercayaan',
                width: 85,
                sortable: true,
                dataIndex: 'kepercayaan',
                hideable: false,
                align:'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Lainnya',
                width: 85,
                sortable: true,
                dataIndex: 'lainnya',
                hideable: false,
                align:'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            }
            ]
        }
        ];

        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();
        
        this.callParent(arguments);

        // this.store.load();
    }
})