Ext.define('Admin.view.Ptk.PtkStatusPegawaiSpGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ptkstatuspegawaispgrid',
    title: '',
    selType: 'rowmodel',
    columnLines: true,
    listeners: {
        afterrender: 'PtkStatusPegawaiSpGridSetup',
        itemdblclick: function (dv, record, item, index, e) {
            if (record.data.id_level_wilayah != 3) {
                this.store.reload({
                    params: {
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function () {
        return Ext.create('Admin.store.PtkStatusPegawaiSp');
    },
    createDockedItems: function () {

        var hash = window.location.hash.substr(1);
        var hashArr = hash.split('/');

        // console.log(getParams);

        var id_level_wilayah = hashArr[1];
        var kode_wilayah = hashArr[2];


        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                href: '#PtkStatusPegawai',
                hrefTarget: '_self'
            }, {
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners: {
                    change: 'bentukPendidikanComboChange'
                }
            }, {
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'Ptk',
                paramMethod: 'PtkStatusPegawaiSp',
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah,
                listeners: {
                    click: 'tombolUnduhExcel'
                }
            }, '->', {
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners: {
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function () {
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Menampilkan data {0} - {1} dari {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function (a, c, d, t) {
        var n = a,
            c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function () {
        var grid = this;

        this.store = this.createStore();

        Ext.apply(this, this.initialConfig);

        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            // locked: true,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1);
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Nama',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            // locked: true,
            renderer: function (v, p, r) {
                return '<a style="color:#228ab7;text-decoration:none" href="#ProfilSatuanPendidikan/' + r.data.sekolah_id + '">' + v + '</a>';
            }
        }, {
            header: 'NPSN',
            align: 'center',
            dataIndex: 'npsn',
            width: 90,
            sortable: true,
            hideable: false
        }, {
            header: 'Status',
            align: 'center',
            dataIndex: 'status_sekolah',
            width: 90,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return r.data.status;
            }
        }, {
            header: 'Bentuk',
            align: 'center',
            dataIndex: 'bentuk_pendidikan_id',
            width: 90,
            sortable: true,
            hideable: false,
            renderer: function(v,p,r){
                return r.data.bentuk;
            }
        },{
            header: 'Status Kepegawaian',
            columns: [{
                header: 'PNS',
                align:'right',
                dataIndex: 'pns',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'CPNS',
                align:'right',
                dataIndex: 'cpns',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'PNS<br>Diperbantukan',
                align:'right',
                dataIndex: 'pns_diperbantukan',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'PNS<br>Depag',
                align:'right',
                dataIndex: 'pns_depag',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Guru Honor<br>Sekolah',
                align:'right',
                dataIndex: 'guru_honor_sekolah',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Tenaga Honor<br>Sekolah',
                align:'right',
                dataIndex: 'tenaga_honor_sekolah',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'GTY/PTY',
                align:'right',
                dataIndex: 'gty_pty',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'GTT/PTT<br>Provinsi',
                align:'right',
                dataIndex: 'gtt_provinsi',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'GTT/PTT<br>Kabupaten/Kota',
                align:'right',
                dataIndex: 'gtt_kabkota',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Guru Bantu<br>Pusat',
                align:'right',
                dataIndex: 'guru_bantu_pusat',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Lainnya',
                align:'right',
                dataIndex: 'lainnya',
                width: 100,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            }]
        },{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }];

        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();

        this.callParent(arguments);

        // this.store.load();
    }
})