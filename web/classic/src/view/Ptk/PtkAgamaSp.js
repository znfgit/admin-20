Ext.define('Admin.view.Ptk.PtkAgamaSp', {
    extend: 'Ext.container.Container',
    xtype: 'ptkagamasp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'ptkagamasp',
    
    cls: 'ptkagamasp-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [
        {
            xtype: 'ptkagamaspgrid',
            title: 'Rekap PTK Berdasarkan Agama',
            // Always 100% of container
            // responsiveCls: 'big-100',
            // height:100
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});
