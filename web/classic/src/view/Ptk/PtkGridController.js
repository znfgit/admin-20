Ext.define('Admin.view.Ptk.PtkGridController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.ptkgrid',
    PtkGridSetup: function(grid){

        var sekolah_id = grid.up('tabpanel').up('profilsatuanpendidikan').sekolah_id;

        console.log(sekolah_id);

        grid.getStore().on('beforeload', function(store){
            Ext.apply(store.proxy.extraParams, {
                Soft_delete: 0,
                sekolah_id: sekolah_id,
                keyword: null,
                propinsi: null,
                kecamatan: null,
                kabupaten: null,
                status_sekolah: null
            });
        });
        
    	grid.getStore().load();
    }
});
