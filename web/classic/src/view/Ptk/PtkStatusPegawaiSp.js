Ext.define('Admin.view.Ptk.PtkStatusPegawaiSp', {
    extend: 'Ext.container.Container',
    xtype: 'ptkstatuspegawaisp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'ptkstatuspegawaisp',
    
    cls: 'ptkstatuspegawaisp-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [
        {
            xtype: 'ptkstatuspegawaispgrid',
            title: 'Rekap Status Kepegawaian PTK',
            // Always 100% of container
            // responsiveCls: 'big-100'
            region: 'center'
        }
    ]
});
