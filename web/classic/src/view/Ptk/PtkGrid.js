Ext.define('Admin.view.Ptk.PtkGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ptkgrid',
    title: '',
    selType: 'rowmodel',
    listeners:{
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.Ptk');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: []
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        Ext.apply(this, this.initialConfig);

        this.lookupStoreStatusKepegawaian = new Admin.store.StatusKepegawaian();
        this.lookupStoreAgama = new Admin.store.Agama();
        this.lookupStoreStatusKeaktifanPegawai = new Admin.store.StatusKeaktifanPegawai();
        this.lookupStoreLembagaPengangkat = new Admin.store.LembagaPengangkat();
        this.lookupStorePangkatGolongan = new Admin.store.PangkatGolongan();
        this.lookupStoreSumberGaji = new Admin.store.SumberGaji();
        this.lookupStorePekerjaan = new Admin.store.Pekerjaan();

        this.lookupStoreStatusKepegawaian.load();
        this.lookupStoreAgama.load();
        this.lookupStoreStatusKeaktifanPegawai.load();
        this.lookupStoreLembagaPengangkat.load();
        this.lookupStorePangkatGolongan.load();
        this.lookupStoreSumberGaji.load();
        this.lookupStorePekerjaan.load();

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 80,
            sortable: true,
            dataIndex: 'ptk_id',
            hideable: false,
            hidden: true
        },{
            header: 'Nama',
            tooltip: 'Nama',
            width: 240,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            renderer: function(v,p,r){
                return '<a style="color:#228ab7;text-decoration:none" href="#ProfilPtk/'+r.data.ptk_id+'">'+v+'</a>';
            }
        },{
            header: 'Bidang Studi',
            tooltip: 'bidang studi',
            width: 150,
            sortable: true,
            dataIndex: 'bidang_studi_id_str',
            hideable: false
        },{
            header: 'NIP',
            tooltip: 'NIP',
            width: 80,
            sortable: true,
            dataIndex: 'nip',
            hideable: false
        },{
            
            header: 'JK',
            tooltip: 'JK',
            width: 80,
            sortable: true,
            dataIndex: 'jenis_kelamin',
            hideable: false,
            renderer: function(v,p,r) {
                switch (v) {
                    case 'L' : return 'L'; break;
                    case 'P' : return 'P'; break;
                    default : return '-'; break;
                }
            }
        },{
            header: 'Tempat Lahir',
            tooltip: 'Tmp.Lahir',
            width: 128,
            sortable: true,
            dataIndex: 'tempat_lahir',
            hideable: false
        },{
            header: 'Tgl Lahir',
            tooltip: 'Tgl Lahir',
            width: 80,
            sortable: true,
            dataIndex: 'tanggal_lahir',
            hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'NIK',
            tooltip: 'NIK',
            width: 144,
            sortable: true,
            dataIndex: 'nik',
            hideable: false
        },{
            header: 'NIY NIGK',
            tooltip: 'Niy Nigk',
            width: 120,
            sortable: true,
            dataIndex: 'niy_nigk',
            hideable: false
        },{
            header: 'NUPTK',
            tooltip: 'NUPTK',
            width: 144,
            sortable: true,
            dataIndex: 'nuptk',
            hideable: false
        },{
            header: 'Status Kepegawaian',
            tooltip: 'Status Kepegawaian',
            width: 80,
            sortable: true,
            dataIndex: 'status_kepegawaian_id',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStoreStatusKepegawaian.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Jenis PTK',
            tooltip: 'Jenis PTK',
            width: 100,
            sortable: true,
            dataIndex: 'jenis_ptk_id',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.jenis_ptk_id_str;
            }
        },{
            header: 'Agama',
            tooltip: 'Agama',
            width: 80,
            sortable: true,
            dataIndex: 'agama_id',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStoreAgama.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Alamat Jalan',
            tooltip: 'Alamat Jalan',
            width: 320,
            sortable: true,
            dataIndex: 'alamat_jalan',
            hideable: false
        },{
            header: 'RT',
            tooltip: 'RT',
            width: 50,
            sortable: true,
            dataIndex: 'rt',
            hideable: false
        },{
            header: 'RW',
            tooltip: 'RW',
            width: 50,
            sortable: true,
            dataIndex: 'rw',
            hideable: false
        },{
            header: 'Nama Dusun',
            tooltip: 'Nama Dusun',
            width: 200,
            sortable: true,
            dataIndex: 'nama_dusun',
            hideable: false
        },{
            header: 'Desa Kelurahan',
            tooltip: 'Desa Kelurahan',
            width: 200,
            sortable: true,
            dataIndex: 'desa_kelurahan',
            hideable: false
        },{
            header: 'Kode Pos',
            tooltip: 'Kode Pos',
            width: 80,
            sortable: true,
            dataIndex: 'kode_pos',
            hideable: false
        },{
            header: 'No Telepon Rumah',
            tooltip: 'No Telepon Rumah',
            width: 80,
            sortable: true,
            dataIndex: 'no_telepon_rumah',
            hideable: false
        },{
            header: 'No Hp',
            tooltip: 'No Hp',
            width: 80,
            sortable: true,
            dataIndex: 'no_hp',
            hideable: false
        },{
            header: 'Email',
            tooltip: 'Email',
            width: 200,
            sortable: true,
            dataIndex: 'email',
            hideable: false
        },{
            header: 'Status Keaktifan',
            tooltip: 'Status Keaktifan',
            width: 120,
            sortable: true,
            dataIndex: 'status_keaktifan_id',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStoreStatusKeaktifanPegawai.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'SK Cpns',
            tooltip: 'SK Cpns',
            width: 160,
            sortable: true,
            dataIndex: 'sk_cpns',
            hideable: false
        },{
            header: 'Tgl Cpns',
            tooltip: 'Tgl Cpns',
            width: 80,
            sortable: true,
            dataIndex: 'tgl_cpns',
            hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'SK Pengangkatan',
            tooltip: 'SK Pengangkatan',
            width: 160,
            sortable: true,
            dataIndex: 'sk_pengangkatan',
            hideable: false
        },{
            header: 'TMT Pengangkatan',
            tooltip: 'TMT Pengangkatan',
            width: 80,
            sortable: true,
            dataIndex: 'tmt_pengangkatan',
            hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'Lembaga Pengangkat',
            tooltip: 'Lembaga Pengangkat',
            width: 120,
            sortable: true,
            dataIndex: 'lembaga_pengangkat_id',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStoreLembagaPengangkat.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Pangkat Golongan',
            tooltip: 'Pangkat Golongan',
            width: 100,
            sortable: true,
            dataIndex: 'pangkat_golongan_id',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStorePangkatGolongan.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Keahlian Laboratorium',
            tooltip: 'Keahlian Laboratorium',
            width: 80,
            sortable: true,
            dataIndex: 'keahlian_laboratorium_id',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.keahlian_laboratorium_id_str;
            }
        },{
            header: 'Sumber Gaji',
            tooltip: 'Sumber Gaji',
            width: 150,
            sortable: true,
            dataIndex: 'sumber_gaji_id',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStoreSumberGaji.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'Nama Ibu Kandung',
            tooltip: 'Nama Ibu Kandung',
            width: 240,
            sortable: true,
            dataIndex: 'nama_ibu_kandung',
            hideable: false
        },{
            
            header: 'Status Perkawinan',
            tooltip: 'Status Perkawinan',
            width: 100,
            sortable: true,
            dataIndex: 'status_perkawinan',
            hideable: false,
            renderer: function(v,p,r) {
                switch (v) {
                    case 1 : return 'Kawin'; break;                 
                    case 2 : return 'Belum Kawin'; break;                   
                    case 3 : return 'Janda/Duda'; break;                    
                    default : return '-'; break;
                }
            }
        },{
            header: 'Nama Suami Istri',
            tooltip: 'Nama Suami Istri',
            width: 240,
            sortable: true,
            dataIndex: 'nama_suami_istri',
            hideable: false
        },{
            header: 'NIP Suami Istri',
            tooltip: 'NIP Suami Istri',
            width: 80,
            sortable: true,
            dataIndex: 'nip_suami_istri',
            hideable: false
        },{
            header: 'Pekerjaan Suami Istri',
            tooltip: 'Pekerjaan Suami Istri',
            width: 80,
            sortable: true,
            dataIndex: 'pekerjaan_suami_istri',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStorePekerjaan.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }                        
            }
        },{
            header: 'TMT PNS',
            tooltip: 'Tmt Pns',
            width: 80,
            sortable: true,
            dataIndex: 'tmt_pns',
            hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'NPWP',
            tooltip: 'Npwp',
            width: 80,
            sortable: true,
            dataIndex: 'npwp',
            hideable: false
        },{
            header: 'Kewarganegaraan',
            tooltip: 'Kewarganegaraan',
            width: 80,
            sortable: true,
            dataIndex: 'kewarganegaraan',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.negara_id_str;
            }
        }];
        
        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();
        
        this.callParent(arguments);

        // this.store.load();
    }
});