Ext.define('Admin.view.Ptk.PtkRingkasan', {
    extend: 'Ext.container.Container',
    xtype: 'ptkringkasan',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'ptkringkasan',
    
    cls: 'ptkringkasan-container',

    // layout: 'responsivecolumn',
    // scrollable: 'y',
    layout: 'border',

    items: [{
        xtype:'tabpanel',
        region:'center',
        items:[{
            xtype: 'ptkringkasangrid',
            title: 'Ringkasan Rekap PTK',
            iconCls: 'x-fa fa-graduation-cap',
            // Always 100% of container
            // responsiveCls: 'big-100',
            minHeight:200
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        },{
            xtype: 'ptkringkasanbarchart',
            title: 'Guru Berdasarkan Status Sekolah',
            iconCls: 'x-fa fa-bar-chart',
            // Always 100% of container
            // responsiveCls: 'big-50 small-100',
            minHeight:200
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        },{
            xtype: 'ptkringkasanbarchart2',
            title: 'Tendik Berdasarkan Status Sekolah',
            iconCls: 'x-fa fa-bar-chart',
            // Always 100% of container
            // responsiveCls: 'big-50 small-100',
            minHeight:200
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        }]
    }]
});
