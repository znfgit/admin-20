Ext.define('Admin.view.Ptk.ProfilPtk', {
    extend: 'Ext.container.Container',
    xtype: 'profilptk',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    // id: 'profilptk',

    controller: 'ptk',
    viewModel: {
        type: 'dashboard'
    },

    layout: 'responsivecolumn',
    scrollable: 'y',
    // items: [
    //     {
    //         xtype: 'panel',
    //         title: 'Kriteria',
    //         responsiveCls: 'big-40 small-100',
    //         cls: 'dashboard-main-chart shadow-panel',
    //         minHeight:550
    //     },
    //     {
    //         xtype: 'panel',
    //         title: 'Data Pokok Satuan Pendidikan',
    //         cls: 'dashboard-main-chart shadow-panel',
    //         responsiveCls: 'big-60 small-100',
    //         minHeight: 550
    //     }
    // ]

    items: [
        {
            xtype: 'tabpanel',
            responsiveCls: 'big-100 small-100',
            itemId: 'profilPtkTabPanel',
            tabBar: {
                items: [{
                    xtype:'button',
                    text: 'Unduh Profil',
                    ui: 'soft-green',
                    hidden: false,
                    iconCls: 'fa fa-file-excel-o',
                    listeners: {
                        click: 'tombolUnduhProfilPtk'
                    }
                }]
            },
            items:[{
                xtype: 'ptkform',
                name: 'ptkform',
                title: 'Profil PTK',
                margin: '0 20 20 0',
                flex: 1,
                minHeight: 550,
                listeners:{
                    afterrender: 'ptkFormSetup'
                }
            },{
                xtype: 'ptkterdaftargrid',
                title: 'Penugasan'
            }]
        }
    ]
});
