Ext.define('Admin.view.Ptk.PtkKualifikasiController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.ptkkualifikasi',
    PtkKualifikasiGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var jptkCombo = grid.down('toolbar').down('jenisptkcombo');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                jenis_ptk_id : jptkCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });

        });

        grid.getStore().on('load', function(store){

            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

            var data = store.data;

            var d3 = 0;
            var d4 = 0;
            var s1 = 0;
            var s2 = 0;
            var s3 = 0;
            var gtk = 0;

            data.each(function(record){
                // console.log(record.data);

                var dataRecord = record.data;
                // console.log(dataRecord);

                d3 += dataRecord.d3;
                d4 += dataRecord.d4;
                s1 += dataRecord.s1;
                s2 += dataRecord.s2;
                s3 += dataRecord.s3;
                gtk += dataRecord.gtk;

            });

            // console.log(guru_negeri);

            var recordConfig = {
                no : '',
                nama : '<b>Total<b>',
                d3: d3,
                d3: d4,
                s1: s1,
                s2: s2,
                s3: s3,
                lainnya: (gtk-(d3+d4+s1+s2+s3)),
                gtk: gtk

            };

            var r = new Admin.model.PtkKualifikasi(recordConfig);

            grid.store.add(r);


        });

    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
    	var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        combo.up('gridpanel').getStore().reload();
        combo.up('gridpanel').up('ptkkualifikasi').down('ptkkualifikasibarchart').down('cartesian').getStore().reload({
            params:{
                bentuk_pendidikan_id: combo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            }
        });
    }

    // TODO - Add control logic or remove if not needed
});
