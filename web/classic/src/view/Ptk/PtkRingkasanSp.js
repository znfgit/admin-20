Ext.define('Admin.view.Ptk.PtkRingkasanSp', {
    extend: 'Ext.container.Container',
    xtype: 'ptkringkasansp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'ptkringkasansp',
    
    cls: 'ptkringkasansp-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [
        {
            xtype: 'ptkringkasanspgrid',
            title: 'Ringkasan PTK Per Satuan Pendidikan',
            // Always 100% of container
            // responsiveCls: 'big-100',
            // height:100
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});
