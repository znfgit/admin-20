Ext.define('Admin.view.Ptk.PtkKualifikasi', {
    extend: 'Ext.container.Container',
    xtype: 'ptkkualifikasi',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'ptkkualifikasi',

    cls: 'ptkagama-container',

    // layout: 'responsivecolumn',
    // scrollable: 'y',
    layout: 'border',

    items: [{
        xtype:'tabpanel',
        region:'center',
        items:[{
            xtype: 'ptkkualifikasigrid',
            title: 'Rekap PTK Berdasarkan Kualifikasi',
            // Always 100% of container
            // responsiveCls: 'big-100',
            iconCls: 'x-fa fa-building',
            minHeight:200
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        },{
            xtype: 'ptkkualifikasibarchart',
            title: 'Rekap PTK Berdasarkan Kualifikasi',
            // Always 100% of container
            // responsiveCls: 'big-100',
            iconCls: 'x-fa fa-bar-chart',
            minHeight:200
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        }]
    }]
});
