Ext.define('Admin.view.Ptk.PtkStatusPegawaiController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.ptkstatuspegawai',
    PtkStatusPegawaiGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var jptkCombo = grid.down('toolbar').down('jenisptkcombo');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                jenis_ptk_id : jptkCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });

        });

        grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }                

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

            var data = store.data;

            var pns = 0;
            var cpns = 0;
            var pns_diperbantukan = 0;
            var pns_depag = 0;
            var guru_honor_sekolah = 0;
            var tenaga_honor_sekolah = 0;
            var gty_pty = 0;
            var gtt_kabkota = 0;
            var gtt_provinsi = 0;
            var guru_bantu_pusat = 0;
            var lainnya = 0;
            
            data.each(function(record){
                // console.log(record.data);

                var dataRecord = record.data;
                // console.log(dataRecord);

                pns = pns+dataRecord.pns;
                cpns = cpns+dataRecord.cpns;
                pns_diperbantukan = pns_diperbantukan+dataRecord.pns_diperbantukan;
                pns_depag = pns_depag+dataRecord.pns_depag;
                guru_honor_sekolah = guru_honor_sekolah+dataRecord.guru_honor_sekolah;
                tenaga_honor_sekolah = tenaga_honor_sekolah+dataRecord.tenaga_honor_sekolah;
                gty_pty = gty_pty+dataRecord.gty_pty;
                gtt_kabkota = gtt_kabkota+dataRecord.gtt_kabkota;
                gtt_provinsi = gtt_provinsi+dataRecord.gtt_provinsi;
                guru_bantu_pusat = guru_bantu_pusat+dataRecord.guru_bantu_pusat;
                lainnya = lainnya+dataRecord.lainnya;
                
            });

            // console.log(guru_negeri);

            var recordConfig = {
                no : '',
                nama : '<b>Total<b>',
                pns: pns,
                cpns: cpns,
                pns_diperbantukan: pns_diperbantukan,
                pns_depag: pns_depag,
                guru_honor_sekolah: guru_honor_sekolah,
                tenaga_honor_sekolah: tenaga_honor_sekolah,
                gty_pty: gty_pty,
                gtt_kabkota: gtt_kabkota,
                gtt_provinsi: gtt_provinsi,
                guru_bantu_pusat: guru_bantu_pusat,
                lainnya: lainnya,
                
            };
            
            var r = new Admin.model.PtkStatusPegawai(recordConfig);
            
            grid.store.add(r);


        });

    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
    	var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        combo.up('gridpanel').getStore().reload();
        combo.up('gridpanel').up('ptkstatuspegawai').down('ptkstatuspegawaibarchart').down('cartesian').getStore().reload({
            params:{
                bentuk_pendidikan_id: combo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            }
        });
    }

    // TODO - Add control logic or remove if not needed
});
