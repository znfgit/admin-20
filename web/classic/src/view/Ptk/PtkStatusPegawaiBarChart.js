Ext.define('Admin.view.Ptk.PtkStatusPegawaiBarChart', {
    extend: 'Ext.Panel',
    ui: 'yellow',
    xtype: 'ptkstatuspegawaibarchart', 
    // width: 650,
    initComponent: function() {
        var me = this;

        this.myDataStore = Ext.create('Admin.store.PtkStatusPegawai');

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        
        var titleV =  [ 'PNS', 'CPNS', 'PNS Diperbantukan', 'PNS Depag', 'Guru Honor Sekolah', 'Tenaga Honor Sekolah', 'GTY/PTY', 'GTT/PTT Propinsi', 'GTT/PTT Kab/Kota', 'Guru Bantu Pusat', 'Lainnya' ];
        var yFieldV = [ 'pns', 'cpns', 'pns_diperbantukan', 'pns_depag', 'guru_honor_sekolah', 'tenaga_honor_sekolah', 'gty_pty', 'gtt_provinsi', 'gtt_kabkota', 'guru_bantu_pusat', 'lainnya' ];        

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            items:[{
                text: 'Simpan',
                iconCls: 'fa fa-save',
                ui: 'soft-blue',
                listeners:{
                    click: function(btn){
                        Ext.MessageBox.confirm('Konfirmasi Unduh', 'Apakah Anda ingin mengunduh grafik ini sebagai gambar?', function (choice) {
                            if (choice == 'yes') {
                                var chart = btn.up('toolbar').up('panel').down('cartesian');

                                // console.log(chart);

                                if (Ext.os.is.Desktop) {
                                    chart.download({
                                        filename: 'Chart Rekap PTK Berdasarkan status pegawai'
                                    });
                                } else {
                                    chart.preview();
                                }
                              }
                        });
                    }
                }
            }]
        }];

        me.items = [{
            xtype: 'cartesian',
            width: '100%',
            height: 480,
            flipXY: true,
            legend: {
                docked: 'right',
                // scrollable: false,
                // width: 220
            },
            interactions: ['itemhighlight'],
            store: this.myDataStore,
            insetPadding: {
                top: 40,
                left: 40,
                right: 40,
                bottom: 40
            },
            axes: [{
                type: 'numeric',
                position: 'bottom',
                adjustByMajorUnit: true,
                grid: true,
                fields: yFieldV,
                minimum: 0
            }, {
                type: 'category',
                position: 'left',
                grid: true,
                fields: ['nama']
            }],
            series: [{
                type: 'bar',
                axis: 'bottom',
                title: titleV,
                xField: 'nama',
                yField: yFieldV,
                stacked: true,
                style: {
                    opacity: 0.80
                },
                highlight: {
                    fillStyle: 'yellow'
                },
                label:{
                    display: 'insideStart',
                    // color: '#ffffff',
                    orientation : 'horizontal',
                    font: '10px Helvetica',
                    field: yFieldV,
                    renderer: function(text, sprite, config, rendererData, index){
                        return text;
                    }
                },
                tooltip: {
                    style: 'background: #fff',
                    renderer: function(storeItem, item) {
                        var browser = item.series.getTitle()[Ext.Array.indexOf(item.series.getYField(), item.field)];
                        this.setHtml(browser + ' di wilayah ' + storeItem.get('nama') + ': ' + storeItem.get(item.field));
                    }
                }
            }]
        }];

        this.myDataStore.load();

        this.callParent();
    }
});