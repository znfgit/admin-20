Ext.define('Admin.view.Ptk.PtkAgamaSpController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.ptkagamasp',
    PtkAgamaSpGridSetup: function(grid){
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var jenisCombo = grid.down('toolbar').down('jenisptkcombo');
            var panel = grid.up('ptkagamasp');

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                jenis_ptk_id: jenisCombo.getValue(),
                id_level_wilayah: panel.id_level_wilayah,
                kode_wilayah: panel.kode_wilayah,
                oke: 'sip'
            });


        });

        grid.getStore().on('load', function(store){

            var panel = grid.up('ptkagamasp');
            var rec = store.getAt(0);

            if(rec){
                if(panel.id_level_wilayah == 1){
                    grid.setTitle('Jumlah PTK Berdasarkan Agama di ' + rec.data.propinsi);
                }else if(panel.id_level_wilayah == 2){
                    grid.setTitle('Jumlah PTK Berdasarkan Agama di ' + rec.data.kabupaten);
                }else if(panel.id_level_wilayah == 3){
                    grid.setTitle('Jumlah PTK Berdasarkan Agama di ' + rec.data.kecamatan);
                }
            }


        });

        grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
        combo.up('gridpanel').getStore().reload();
    },
    jenisPtkComboChange: function(combo){
    	combo.up('gridpanel').getStore().reload();
    }

    // TODO - Add control logic or remove if not needed
});
