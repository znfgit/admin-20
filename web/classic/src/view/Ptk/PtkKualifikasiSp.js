Ext.define('Admin.view.Ptk.PtkKualifikasiSp', {
    extend: 'Ext.container.Container',
    xtype: 'ptkkualifikasisp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'ptkkualifikasisp',
    
    cls: 'ptkkualifikasisp-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [
        {
            xtype: 'ptkkualifikasispgrid',
            title: 'Kualifikasi Rekap PTK',
            // Always 100% of container
            // responsiveCls: 'big-100',
            // height:100
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});