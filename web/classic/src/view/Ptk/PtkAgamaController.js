Ext.define('Admin.view.Ptk.PtkAgamaController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.ptkagama',
    PtkAgamaGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var jptkCombo = grid.down('toolbar').down('jenisptkcombo');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                jenis_ptk_id : jptkCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });

        });

        grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }                

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

            var data = store.data;

            var islam = 0;
            var kristen = 0;
            var katholik = 0;
            var hindu = 0;
            var budha = 0;
            var konghucu = 0;
            var lainnya = 0;
            var total = 0;
            
            data.each(function(record){
                // console.log(record.data);

                var dataRecord = record.data;
                // console.log(dataRecord);

                islam = islam+dataRecord.islam;
                kristen = kristen+dataRecord.kristen;
                katholik = katholik+dataRecord.katholik;
                hindu = hindu+dataRecord.hindu;
                budha = budha+dataRecord.budha;
                konghucu = konghucu+dataRecord.konghucu;
                lainnya = lainnya+dataRecord.lainnya;
                total = total+dataRecord.total;
            });

            // console.log(guru_negeri);

            var recordConfig = {
                no : '',
                nama : '<b>Total<b>',
                islam: islam,
                kristen: kristen,
                katholik: katholik,
                hindu: hindu,
                budha: budha,
                konghucu: konghucu,
                lainnya: lainnya,
                total: total
            };
            
            var r = new Admin.model.PtkAgama(recordConfig);
            
            grid.store.add(r);

        });
        
    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        // var bpCombo = combo.up('gridpanel').down('toolbar').down('bentukpendidikancombo');
        var jptkCombo = combo.up('gridpanel').down('toolbar').down('jenisptkcombo');

        combo.up('gridpanel').getStore().reload();
        combo.up('gridpanel').up('ptkagama').down('ptkagamabarchart').down('cartesian').getStore().reload({
            params:{
                bentuk_pendidikan_id: combo.getValue(),
                jenis_ptk_id : jptkCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            }
        });
    },
    jenisPtkComboChange: function(combo){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        var bpCombo = combo.up('gridpanel').down('toolbar').down('bentukpendidikancombo');
        // var jptkCombo = combo.up('gridpanel').down('toolbar').down('jenisptkcombo');

        combo.up('gridpanel').getStore().reload();
        combo.up('gridpanel').up('ptkagama').down('ptkagamabarchart').down('cartesian').getStore().reload({
            params:{
                bentuk_pendidikan_id: bpCombo.getValue(),
                jenis_ptk_id : combo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            }
        });
    }

    // TODO - Add control logic or remove if not needed
});
