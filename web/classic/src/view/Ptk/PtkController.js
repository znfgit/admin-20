Ext.define('Admin.view.Ptk.PtkController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.ptk',
    filterFormSetup: function(form){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        if(session.id_level_wilayah == 1){
            form.down('mstwilayahcombo[name=comboprop]').setValue(session.kode_wilayah);

            form.down('mstwilayahcombo[name=comboprop]').hide();
        }else if(session.id_level_wilayah == 2){
            form.down('mstwilayahcombo[name=combokab]').setValue(session.kode_wilayah);

            form.down('mstwilayahcombo[name=comboprop]').hide();
            form.down('mstwilayahcombo[name=combokab]').hide();
        }
    },
    ptkSetup: function(grid){

        grid.up('container').down('filterform').insert(3,{
            xtype: 'BidangStudicombo',
            width: '100%',
            labelAlign: 'top',
            allowBlank: true,
            emptyText: 'Cari Bidang Studi ...',
            listConfig: {
                getInnerTpl: function() {
                    return '<div class="search-item">{bidang_studi} ({bidang_studi_id})</div>';
                }
            }
        });

        grid.down('toolbar').add('->',{
            text: 'Unduh Xlsx',
            iconCls: 'x-fa fa-download',
            listeners:{
                click: function(btn){

                    var form = grid.up('ptk').down('filterform');

                    var keywordField = form.down('textfield[name=keyword]');
                    var prop = form.down('mstwilayahcombo[name=comboprop]');
                    var kab = form.down('mstwilayahcombo[name=combokab]');
                    var kec = form.down('mstwilayahcombo[name=combokec]');
                    var bp = form.down('bentukpendidikancombo');
                    var status = form.down('combobox[name=pilihstatuscombo]');
                    var bidang_studi = form.down('BidangStudicombo');

                    window.location.href="Excel/generate?class=Ptk&method=Ptk&tipe=1&keyword="+keywordField.getValue()+"&propinsi="+prop.getValue()+"&kabupaten="+kab.getValue()+"&kecamatan="+kec.getValue()+"&bentuk_pendidikan_id="+bp.getValue()+"&status_sekolah="+status.getValue()+"&bidang_studi_id="+bidang_studi.getValue()+"";
                }
            }
        },'-',{
            iconCls: 'fa fa-refresh',
            listeners:{
                click: 'refresh'
            }
        });
        // Ext.MessageBox.alert('tes');

        grid.getStore().on('beforeload',function(store){

            var form = grid.up('container').down('filterform');

            var keywordField = form.down('textfield[name=keyword]');
            var prop = form.down('mstwilayahcombo[name=comboprop]');
            var kab = form.down('mstwilayahcombo[name=combokab]');
            var kec = form.down('mstwilayahcombo[name=combokec]');
            var bp = form.down('bentukpendidikancombo');
            var status = form.down('combobox[name=pilihstatuscombo]');
            var bidang_studi = form.down('BidangStudicombo');

            Ext.apply(store.proxy.extraParams, {
                keyword: keywordField.getValue(),
                propinsi: prop.getValue(),
                kabupaten: kab.getValue(),
                kecamatan: kec.getValue(),
                bentuk_pendidikan_id: bp.getValue(),
                status_sekolah: status.getValue(),
                bidang_studi_id: bidang_studi.getValue()
            });

            Ext.apply(store.proxy.extraParams, {
                Soft_delete: 0,
                sekolah_id: null
            });
        });

        grid.getStore().load();
    },
    ptkFormSetup: function(form){
        var container = form.up('tabpanel').up('container');
        var ptk_id = container.ptk_id;

        // console.log(ptk_id);

        var storePtk = new Admin.store.Ptk();

        storePtk.load({
            params:{
                ptk_id: ptk_id
            }
        });

        storePtk.on('load', function(store){
            var rec = store.getAt(0);

            if(rec){
                form.loadRecord(rec);
            }
        });
    },
    setupPtkTerdaftarGrid: function(grid){
        var container = grid.up('tabpanel').up('container');
        var ptk_id = container.ptk_id;

        grid.getStore().on('beforeload', function(store){
            Ext.apply(store.proxy.extraParams, {
                Soft_delete: 0,
                ptk_id: ptk_id
            });
        });

        grid.getStore().load();

    },
    tombolUnduhProfilPtk : function(btn){
        var hash = window.location.hash.substr(1);
        var hashArr = hash.split('/');

        // console.log(getParams[4]);

        var ptk_id = hashArr[1];

        var url = "getProfilPtk/" + ptk_id;

        window.location.replace(url);

    }

    // TODO - Add control logic or remove if not needed
});
