Ext.define('Admin.view.Ptk.PtkKualifikasiGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ptkkualifikasigrid',
    title: '',
    columnLines: true,
    selType: 'rowmodel',
    listeners:{
        afterrender:'PtkKualifikasiGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.PtkKualifikasi');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                listeners:{
                    click: 'kembali'
                }
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype: 'jenisptkcombo',
                emptyText: 'Pilih Jenis PTK',
                width: 200,
                listeners:{
                    change: function(combo){
                        console.log('nendang bro');

                        combo.up('gridpanel').getStore().reload();
                    }
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'Ptk',
                paramMethod: 'PtkKualifikasi',
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a,
            c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {

        var grid = this;

        this.store = this.createStore();

        Ext.apply(this, this.initialConfig);


        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            dataIndex: 'no',
            // locked: true,
            hideable: false,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1);
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Wilayah',
            width: 170,
            sortable: true,
            dataIndex: 'nama',
            // locked: true,
            hideable: false,
            renderer: function(v,p,r){

                if(r.data.id_level_wilayah){
                    return '<a style="color:#228ab7;text-decoration:none" href="#PtkKualifikasiSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah+'">'+v+'</a>';
                }else{
                    return v;
                }

            }
        },{
            header: 'Kualifikasi',
            columns:[{
                header: 'D3',
                align:'right',
                dataIndex: 'd3',
                width: 90,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'D4',
                align:'right',
                dataIndex: 'd4',
                width: 90,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'S1',
                align:'right',
                dataIndex: 's1',
                width: 90,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'S2',
                align:'right',
                dataIndex: 's2',
                width: 90,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'S3',
                align:'right',
                dataIndex: 's3',
                width: 90,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Lainnya',
                align:'right',
                dataIndex: 'lainnya',
                width: 90,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){

                    if(v){
                        return v;
                    }else{
                        return grid.formatMoney( r.data.gtk - (r.data.d3+r.data.d4+r.data.s1+r.data.s2+r.data.s3) );
                    }

                }
            },{
                header: 'Total',
                align:'right',
                dataIndex: 'gtk',
                width: 90,
                sortable: true,
                hideable: false,
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            }]
        },{
            header: 'Tanggal<br>Rekap<br>Terakhir',
            width: 130,
            sortable: true,
            dataIndex: 'tanggal_rekap_terakhir',
            renderer: Ext.util.Format.dateRenderer('d/m/Y')
        }
        ];

        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();

        this.callParent(arguments);

        // this.store.load();
    }
});