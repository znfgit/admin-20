Ext.define('Admin.view.Ptk.PtkRingkasanController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.ptkringkasan',
    PtkRingkasanGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });


        });

        grid.getStore().on('load', function(store){

            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

            var data = store.data;

            var guru_negeri = 0;
            var guru_swasta = 0;

            var pegawai_negeri = 0;
            var pegawai_swasta = 0;

            var ptk_negeri = 0;
            var ptk_swasta = 0;

            data.each(function(record){
                // console.log(record.data);

                var dataRecord = record.data;
                // console.log(dataRecord);

                guru_negeri = guru_negeri+dataRecord.guru_negeri;
                guru_swasta = guru_swasta+dataRecord.guru_swasta;

                pegawai_negeri = pegawai_negeri+dataRecord.pegawai_negeri;
                pegawai_swasta = pegawai_swasta+dataRecord.pegawai_swasta;

                ptk_negeri = ptk_negeri+dataRecord.ptk_negeri;
                ptk_swasta = ptk_swasta+dataRecord.ptk_swasta;

            });

            // console.log(guru_negeri);

            var recordConfig = {
                no : '',
                nama : '<b>Total<b>',
                guru_negeri: guru_negeri,
                guru_swasta: guru_swasta,
                pegawai_negeri: pegawai_negeri,
                pegawai_swasta: pegawai_swasta,
                ptk_negeri: ptk_negeri,
                ptk_swasta: ptk_swasta,
            };

            var r = new Admin.model.PtkRingkasan(recordConfig);

            grid.store.add(r);


        });

    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
    	combo.up('gridpanel').getStore().reload();
    }

    // TODO - Add control logic or remove if not needed
});
