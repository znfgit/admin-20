Ext.define('Admin.view.Ptk.PtkStatusPegawai', {
    extend: 'Ext.container.Container',
    xtype: 'ptkstatuspegawai',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'ptkstatuspegawai',
    
    cls: 'ptkagama-container',

    // layout: 'responsivecolumn',
    // scrollable: 'y',
    layout: 'border',

    items: [{
        xtype:'tabpanel',
        region:'center',
        items:[{
            xtype: 'ptkstatuspegawaigrid',
            title: 'Rekap PTK Berdasarkan Status Kepegawaian',
            // Always 100% of container
            // responsiveCls: 'big-100',
            iconCls: 'x-fa fa-building',
            minHeight:200
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        },{
            xtype: 'ptkstatuspegawaibarchart',
            title: 'Rekap PTK Berdasarkan Status Kepegawaian',
            // Always 100% of container
            // responsiveCls: 'big-100',
            iconCls: 'x-fa fa-bar-chart',
            minHeight:200
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        }]
    }]
});
