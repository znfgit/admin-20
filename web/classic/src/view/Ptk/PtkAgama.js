Ext.define('Admin.view.Ptk.PtkAgama', {
    extend: 'Ext.container.Container',
    xtype: 'ptkagama',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'ptkagama',
    
    cls: 'ptkagama-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [{
        xtype:'tabpanel',
        region:'center',
        items:[{
            xtype: 'ptkagamagrid',
            title: 'Rekap PTK Berdasarkan Agama',
            iconCls: 'x-fa fa-graduation-cap',
            // Always 100% of container
            // responsiveCls: 'big-100',
            minHeight:200
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        },
        {
            xtype: 'ptkagamabarchart',
            title: 'Rekap PTK Berdasarkan Agama',
            iconCls: 'x-fa fa-bar-chart',
            // Always 100% of container
            // responsiveCls: 'big-100',
            minHeight:200
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        }]
    }]
});
