Ext.define('Admin.view.Ptk.Ptk', {
    extend: 'Ext.container.Container',
    xtype: 'ptk',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    id: 'ptk',

    controller: 'ptk',
    viewModel: {
        type: 'dashboard'
    },

    // scrollable: 'y',

    // layout: 'responsivecolumn',

    // items: [
    //     {
    //         xtype: 'panel',
    //         title: 'Kriteria',
    //         responsiveCls: 'big-40 small-100',
    //         cls: 'dashboard-main-chart shadow-panel',
    //         minHeight:550
    //     },
    //     {
    //         xtype: 'panel',
    //         title: 'Data Pokok Satuan Pendidikan',
    //         cls: 'dashboard-main-chart shadow-panel',
    //         responsiveCls: 'big-60 small-100',
    //         minHeight: 550
    //     }
    // ]

    layout: 'border',

    // layout: {
    //     type: 'hbox',
    //     align: 'stretch'
    // },

    // margin: '20 0 0 20',

    items: [
        {
            xtype: 'filterform',
            title: 'Kriteria',
            // width: '35%',
            region: 'west',
            split: true,
            collapsible: true,
            minWidth: 240,
            maxWidth: 300,
            // minHeight:550,
            // margin: '0 20 20 0'
        },
        {
            xtype: 'ptkgrid',
            name: 'tabelDataPokokPtk',
            title: 'Data Pokok PTK',
            // margin: '0 20 20 0',
            region: 'center',
            flex: 1,
            // minHeight: 550,
            listeners:{
                afterrender: 'ptkSetup'
            }
        }
    ]
});
