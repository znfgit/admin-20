Ext.define('Admin.view.Sarpras.PrasaranaGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.prasaranagrid',
    title: '',
    selType: 'rowmodel',
    controller: 'prasaranagrid',
    listeners:{
        afterrender:'PrasaranaGridSetup',
        itemdblclick: function(dv, record, item, index, e) {

        }
    },
    createStore: function() {
        return Ext.create('Admin.store.Sarpras');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: []
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a,
            c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {

        var grid = this;

        this.store = this.createStore();

        Ext.apply(this, this.initialConfig);

        this.lookupStoreJenisPrasarana = new Admin.store.JenisPrasarana({
            autoload: true
        });

        this.lookupStoreStatusKepemilikanSarpras = new Admin.store.StatusKepemilikanSarpras({
            autoload: true
        });

        this.lookupStoreJenisPrasarana.load();
        this.lookupStoreStatusKepemilikanSarpras.load();

        this.columns = [{
            header: 'Id',
            tooltip: 'Id',
            width: 80,
            sortable: true,
            dataIndex: 'prasarana_id',
            hideable: false,
            hidden: true
        },{
            header: 'Sekolah',
            tooltip: 'Sekolah',
            width: 80,
            sortable: true,
            dataIndex: 'sekolah_id',
            hideable: false,
            hidden: true,
            renderer: function(v,p,r) {
                return r.data.sekolah_id_str;
            }
        },{
            header: 'Jenis Prasarana',
            tooltip: 'Jenis Prasarana',
            width: 200,
            sortable: true,
            dataIndex: 'jenis_prasarana_id',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStoreJenisPrasarana.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }
            }
        },{
            header: 'Kepemilikan',
            tooltip: 'Kepemilikan Sarpras',
            width: 200,
            sortable: true,
            dataIndex: 'kepemilikan_sarpras_id',
            hideable: false,
            renderer: function(v,p,r) {
                var record = this.lookupStoreStatusKepemilikanSarpras.getById(v);
                if(record) {
                    return record.get('nama');
                } else {
                    return v;
                }
            }
        },{
            header: 'Nama',
            tooltip: 'Nama',
            width: 250,
            sortable: true,
            dataIndex: 'nama',
            hideable: false
        },{
            header: 'Panjang',
            tooltip: 'Panjang',
            width: 80,
            sortable: true,
            dataIndex: 'panjang',
            hideable: false
        },{
            header: 'Lebar',
            tooltip: 'Lebar',
            width: 80,
            sortable: true,
            dataIndex: 'lebar',
            hideable: false
        },{
            text: 'Tingkat Kerusakan (%)',
            columns: [{
                header: 'Penutup<br>Atap',
                tooltip: 'Penutup Atap',
                width: 100,
                sortable: true,
                dataIndex: 'rusak_penutup_atap',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    if (v == '.00'){
                        return 0
                    }else{
                        return v;
                    }
                }
            },{
                header: 'Rangka<br>Atap',
                tooltip: 'Rangka Atap',
                width: 100,
                sortable: true,
                dataIndex: 'rusak_rangka_atap',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    if (v == '.00'){
                        return 0
                    }else{
                        return v;
                    }
                }
            },{
                header: 'Rangka<br>Plafon',
                tooltip: 'Rangka Plafon',
                width: 100,
                sortable: true,
                dataIndex: 'rusak_rangka_plafon',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    if (v == '.00'){
                        return 0
                    }else{
                        return v;
                    }
                }
            },{
                header: 'Kolom<br>Ring Balok',
                tooltip: 'Rangka kolom ring balok',
                width: 100,
                sortable: true,
                dataIndex: 'rusak_kolom_ringbalok',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    if (v == '.00'){
                        return 0
                    }else{
                        return v;
                    }
                }
            },{
                header: 'Bata/Dinding<br>Pengisi',
                tooltip: 'bata/dinding pengisi',
                width: 100,
                sortable: true,
                dataIndex: 'rusak_bata_dindingpengisi',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    if (v == '.00'){
                        return 0
                    }else{
                        return v;
                    }
                }
            },{
                header: 'Kusen',
                tooltip: 'kusen',
                width: 100,
                sortable: true,
                dataIndex: 'rusak_kusen',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    if (v == '.00'){
                        return 0
                    }else{
                        return v;
                    }
                }
            },{
                header: 'Daun<br>Pintu',
                tooltip: 'Daun Pintu',
                width: 100,
                sortable: true,
                dataIndex: 'rusak_daun_pintu',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    if (v == '.00'){
                        return 0
                    }else{
                        return v;
                    }
                }
            },{
                header: 'Daun<br>Jendela',
                tooltip: 'Daun Jendela',
                width: 100,
                sortable: true,
                dataIndex: 'rusak_daun_jendela',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    if (v == '.00'){
                        return 0
                    }else{
                        return v;
                    }
                }
            },{
                header: 'Penutup<br>Lantai',
                tooltip: 'Penutup Lantai',
                width: 100,
                sortable: true,
                dataIndex: 'rusak_penutup_lantai',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    if (v == '.00'){
                        return 0
                    }else{
                        return v;
                    }
                }
            },{
                header: 'Pondasi',
                tooltip: 'Pondasi',
                width: 100,
                sortable: true,
                dataIndex: 'rusak_pondasi',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    if (v == '.00'){
                        return 0
                    }else{
                        return v;
                    }
                }
            }]
        }];

        this.dockedItems = this.createDockedItems();

        this.bbar = this.createBbar();

        this.callParent(arguments);

        // this.store.load();
    }
});