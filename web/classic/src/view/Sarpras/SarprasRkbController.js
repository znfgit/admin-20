Ext.define('Admin.view.Sarpras.SarprasRkbController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.sarprasrkb',
    SarprasRkbGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            
            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });

        });

        grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            // console.log(rec);


            if(session.id_level_wilayah == '0'){

                if(rec.data.id_level_wilayah == 1){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }else if(session.id_level_wilayah == '1'){

                if(rec.data.id_level_wilayah == 2){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }                

            }else if(session.id_level_wilayah == '2'){

                if(rec.data.id_level_wilayah == 3){
                    grid.down('toolbar').down('button[name=kembaliButton]').hide();
                }else{
                    grid.down('toolbar').down('button[name=kembaliButton]').show();
                }

            }

            var data = store.data;

            var pd_negeri = 0;
            var pd_swasta = 0;
            var pd_total = 0;
            var jumlah_ruang_kelas_negeri = 0;
            var jumlah_ruang_kelas_swasta = 0;
            var jumlah_ruang_kelas_total = 0;
            var rkb_negeri = 0;
            var rkb_swasta = 0;
            var rkb_total = 0;

            data.each(function(record){
                // console.log(record.data);

                var dataRecord = record.data;
                // console.log(dataRecord);

                pd_negeri = pd_negeri+parseInt(dataRecord.pd_negeri);
                pd_swasta = pd_swasta+parseInt(dataRecord.pd_swasta);
                pd_total = pd_total+parseInt(dataRecord.pd_total);
                jumlah_ruang_kelas_negeri = jumlah_ruang_kelas_negeri+parseInt(dataRecord.jumlah_ruang_kelas_negeri);
                jumlah_ruang_kelas_swasta = jumlah_ruang_kelas_swasta+parseInt(dataRecord.jumlah_ruang_kelas_swasta);
                jumlah_ruang_kelas_total = jumlah_ruang_kelas_total+parseInt(dataRecord.jumlah_ruang_kelas_total);
                rkb_negeri = rkb_negeri+parseInt(dataRecord.rkb_negeri);
                rkb_swasta = rkb_swasta+parseInt(dataRecord.rkb_swasta);
                rkb_total = rkb_total+parseInt(dataRecord.rkb_total);

            });

            // console.log(guru_negeri);

            var recordConfig = {
                no : '',
                nama : '<b>Total<b>',
                pd_negeri: pd_negeri,
                pd_swasta: pd_swasta,
                pd_total: pd_total,
                jumlah_ruang_kelas_negeri: jumlah_ruang_kelas_negeri,
                jumlah_ruang_kelas_swasta: jumlah_ruang_kelas_swasta,
                jumlah_ruang_kelas_total: jumlah_ruang_kelas_total,
                rkb_negeri: rkb_negeri,
                rkb_swasta: rkb_swasta,
                rkb_total: rkb_total

            };
            
            var r = new Admin.model.SarprasRkb(recordConfig);
            
            grid.store.add(r);

        });

    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        combo.up('gridpanel').getStore().reload();
        combo.up('gridpanel').up('sarprasrkb').down('sarprasrkbbarchart').down('cartesian').getStore().reload({
            params:{
                bentuk_pendidikan_id: combo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            }
        });
    }
});
