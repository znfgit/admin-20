Ext.define('Admin.view.Sarpras.PrasaranaGridController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.prasaranagrid',
    PrasaranaGridSetup: function(grid){

        var sekolah_id = grid.up('tabpanel').up('profilsatuanpendidikan').sekolah_id;

        console.log(sekolah_id);

        grid.getStore().on('beforeload', function(store){
            Ext.apply(store.proxy.extraParams, {
                Soft_delete: 0,
                sekolah_id: sekolah_id
            });
        });
        
    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
        combo.up('gridpanel').getStore().reload();
    },
    SarprasRkbGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });

        });

        grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            // console.log(rec);

            if(rec){

                if(session.id_level_wilayah == '0'){

                    if(rec.data.id_level_wilayah == 1){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }

                }else if(session.id_level_wilayah == '1'){

                    if(rec.data.id_level_wilayah == 2){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }                

                }else if(session.id_level_wilayah == '2'){

                    if(rec.data.id_level_wilayah == 3){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }

                }
                
            }


        });

        grid.getStore().load();
    },
    SarprasRkbSpGridSetup: function(grid){
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var panel = grid.up('container');

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: panel.id_level_wilayah,
                kode_wilayah: panel.kode_wilayah,
                oke: 'sip'
            });


        });

        grid.getStore().on('load', function(store){

            var panel = grid.up('container');
            var rec = store.getAt(0);

            if(rec){
                if(panel.id_level_wilayah == 1){
                    grid.setTitle('Kebutuhan RKB per Satuan Pendidikan di ' + rec.data.propinsi);
                }else if(panel.id_level_wilayah == 2){
                    grid.setTitle('Kebutuhan RKB per Satuan Pendidikan di ' + rec.data.kabupaten);
                }else if(panel.id_level_wilayah == 3){
                    grid.setTitle('Kebutuhan RKB per Satuan Pendidikan di ' + rec.data.kecamatan);
                }
            }


        });

        grid.getStore().load();
    },
    SarprasLahanRuangGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah
            });

        });

        grid.getStore().on('load', function(store){

            var panel = grid.up('container');
            var rec = store.getAt(0);

            if(rec){
                if(panel.id_level_wilayah == 1){
                    grid.setTitle('Tabulasi Lahan dan Ruang Prasarana di ' + rec.data.propinsi);
                }else if(panel.id_level_wilayah == 2){
                    grid.setTitle('Tabulasi Lahan dan Ruang Prasarana di ' + rec.data.kabupaten);
                }else if(panel.id_level_wilayah == 3){
                    grid.setTitle('Tabulasi Lahan dan Ruang Prasarana di ' + rec.data.kecamatan);
                }
            }


        });

        grid.getStore().load();
    }
});
