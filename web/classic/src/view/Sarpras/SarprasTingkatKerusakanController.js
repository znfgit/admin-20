Ext.define('Admin.view.Sarpras.SarprasTingkatKerusakanController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.sarprastingkatkerusakan',
    SarprasTingkatKerusakanGridSetup: function(grid){
        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        grid.down('toolbar').insert(3,{
            xtype: 'textfield',
            emptyText: 'Cari Nama Wilayah ... (Tekan Enter)',
            width: 250,
            itemId: 'keyword',
            enableKeyEvents: true,
            listeners: {
                keypress: function (field, e) {
                    if (e.getKey() == e.ENTER){

                        grid.getStore().load();
                    }
                }
            }
        });
        
        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var txtKeyword = grid.down('toolbar').down('textfield[itemId=keyword]');

            console.log(bpCombo.getValue());

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: session.id_level_wilayah,
                kode_wilayah: session.kode_wilayah,
                keyword: txtKeyword.getValue()
            });

        });

        grid.getStore().on('load', function(store){
            
            var rec = store.getAt(0);

            // console.log(rec);

            if(rec){

                if(session.id_level_wilayah == '0'){

                    if(rec.data.id_level_wilayah == 1){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }

                }else if(session.id_level_wilayah == '1'){

                    if(rec.data.id_level_wilayah == 2){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }                

                }else if(session.id_level_wilayah == '2'){

                    if(rec.data.id_level_wilayah == 3){
                        grid.down('toolbar').down('button[name=kembaliButton]').hide();
                    }else{
                        grid.down('toolbar').down('button[name=kembaliButton]').show();
                    }

                }

            }

            var data = store.data;

            var r_kelas_baik = 0;
            var r_kelas_rusak_ringan = 0;
            var r_kelas_rusak_sedang = 0;
            var r_kelas_rusak_berat = 0;
            var bengkel_baik = 0;
            var bengkel_rusak_ringan = 0;
            var bengkel_rusak_sedang = 0;
            var bengkel_rusak_berat = 0;
            var perpustakaan_baik = 0;
            var perpustakaan_rusak_ringan = 0;
            var perpustakaan_rusak_sedang = 0;
            var perpustakaan_rusak_berat = 0;

            data.each(function(record){
                // console.log(record.data);

                var dataRecord = record.data;
                // console.log(dataRecord);

                r_kelas_baik = r_kelas_baik+dataRecord.r_kelas_baik;
                r_kelas_rusak_ringan = r_kelas_rusak_ringan+dataRecord.r_kelas_rusak_ringan;
                r_kelas_rusak_sedang = r_kelas_rusak_sedang+dataRecord.r_kelas_rusak_sedang;
                r_kelas_rusak_berat = r_kelas_rusak_berat+dataRecord.r_kelas_rusak_berat;
                bengkel_baik = bengkel_baik+dataRecord.bengkel_baik;
                bengkel_rusak_ringan = bengkel_rusak_ringan+dataRecord.bengkel_rusak_ringan;
                bengkel_rusak_sedang = bengkel_rusak_sedang+dataRecord.bengkel_rusak_sedang;
                bengkel_rusak_berat = bengkel_rusak_berat+dataRecord.bengkel_rusak_berat;
                perpustakaan_baik = perpustakaan_baik+dataRecord.perpustakaan_baik;
                perpustakaan_rusak_ringan = perpustakaan_rusak_ringan+dataRecord.perpustakaan_rusak_ringan;
                perpustakaan_rusak_sedang = perpustakaan_rusak_sedang+dataRecord.perpustakaan_rusak_sedang;
                perpustakaan_rusak_berat = perpustakaan_rusak_berat+dataRecord.perpustakaan_rusak_berat;

            });

            // console.log(guru_negeri);

            var recordConfig = {
                no: '',
                nama: '<b>Total<b>',
                r_kelas_baik: r_kelas_baik,
                r_kelas_rusak_ringan: r_kelas_rusak_ringan,
                r_kelas_rusak_sedang: r_kelas_rusak_sedang,
                r_kelas_rusak_berat: r_kelas_rusak_berat,
                bengkel_baik: bengkel_baik,
                bengkel_rusak_ringan: bengkel_rusak_ringan,
                bengkel_rusak_sedang: bengkel_rusak_sedang,
                bengkel_rusak_berat: bengkel_rusak_berat,
                perpustakaan_baik: perpustakaan_baik,
                perpustakaan_rusak_ringan: perpustakaan_rusak_ringan,
                perpustakaan_rusak_sedang: perpustakaan_rusak_sedang,
                perpustakaan_rusak_berat: perpustakaan_rusak_berat

            };

            var r = new Admin.model.SarprasTingkatKerusakan(recordConfig);

            grid.store.add(r);


        });

    	grid.getStore().load();
    },
    bentukPendidikanComboChange: function(combo){
    	combo.up('gridpanel').getStore().reload();
    },
    SarprasTingkatKerusakanSpGridSetup: function(grid){
        grid.down('toolbar').insert(3,{
            xtype: 'textfield',
            emptyText: 'Cari Nama/NPSN ...(Tekan Enter)',
            width: 250,
            itemId: 'keyword',
            enableKeyEvents: true,
            listeners: {
                keypress: function (field, e) {
                    if (e.getKey() == e.ENTER){

                        grid.getStore().load();
                    }
                }
            }
        });

        grid.getStore().on('beforeload', function(store){
            var bpCombo = grid.down('toolbar').down('bentukpendidikancombo');
            var txtKeyword = grid.down('toolbar').down('textfield[itemId=keyword]');
            var panel = grid.up('container');

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: bpCombo.getValue(),
                id_level_wilayah: panel.id_level_wilayah,
                kode_wilayah: panel.kode_wilayah,
                keyword: txtKeyword.getValue()
            });


        });

        grid.getStore().on('load', function(store){

            var panel = grid.up('container');
            var rec = store.getAt(0);

            if(rec){
                if(panel.id_level_wilayah == 1){
                    grid.setTitle('Rekap Tingkat Kerusakan Sarpras di ' + rec.data.propinsi);
                }else if(panel.id_level_wilayah == 2){
                    grid.setTitle('Rekap Tingkat Kerusakan Sarpras di ' + rec.data.kabupaten);
                }else if(panel.id_level_wilayah == 3){
                    grid.setTitle('Rekap Tingkat Kerusakan Sarpras di ' + rec.data.kecamatan);
                }
            }


        });

        grid.getStore().load();
    }

    // TODO - Add control logic or remove if not needed
});
