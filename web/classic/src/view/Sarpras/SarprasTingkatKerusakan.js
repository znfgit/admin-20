Ext.define('Admin.view.Sarpras.SarprasTingkatKerusakan', {
    extend: 'Ext.container.Container',
    xtype: 'sarprastingkatkerusakan',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'sarprastingkatkerusakan',
    
    cls: 'sarprastingkatkerusakan-container',

    // layout: 'responsivecolumn',
    // scrollable: 'y',
    layout: 'border',

    items: [{
        xtype:'tabpanel',
        region:'center',
        items:[
            {
                xtype: 'sarprastingkatkerusakangrid',
                title: 'Tingkat Kerusakan Prasarana',
                // Always 100% of container
                // responsiveCls: 'big-100',
                // minHeight:200
                // region: 'center'
                // html: 'isinya daftar pengiriman'
            },{
                xtype: 'sarprastingkatkerusakanbarchart',
                title: 'Chart Tingkat Kerusakan Ruang Kelas',
                scrollable: true
                // Always 100% of container
                // responsiveCls: 'big-100',
                // minHeight:200
                // region: 'center'
                // html: 'isinya daftar pengiriman'
            }
        ]
    }]
});
