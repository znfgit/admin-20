Ext.define('Admin.view.Sarpras.SarprasRkbSp', {
    extend: 'Ext.container.Container',
    xtype: 'sarprasrkbsp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'sarprasrkbsp',
    
    cls: 'sarprasrkbsp-container',

    // layout: 'responsivecolumn',
    layout: 'border',
    // scrollable: 'y',

    items: [
        {
            xtype: 'sarprasrkbspgrid',
            title: 'Rekap Sarpras Berdasarkan Kebutuhan Ruang Kelas Baru',
            // Always 100% of container
            // responsiveCls: 'big-100'
            // height:100
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});