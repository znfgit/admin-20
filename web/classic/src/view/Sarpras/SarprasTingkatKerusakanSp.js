Ext.define('Admin.view.Sarpras.SarprasTingkatKerusakanSp', {
    extend: 'Ext.container.Container',
    xtype: 'sarprastingkatkerusakansp',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'sarprastingkatkerusakan',
    
    cls: 'sarprastingkatkerusakan-container',

    // layout: 'responsivecolumn',

    // scrollable: 'y',
    layout: 'border',

    items: [
        {
            xtype: 'sarprastingkatkerusakanspgrid',
            title: 'Tingkat Kerusakan Prasarana Per Satuan Pendidikan',
            // Always 100% of container
            // responsiveCls: 'big-100',
            // minHeight:200
            region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});
