Ext.define('Admin.view.Sarpras.SarprasRkbGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.sarprasrkbgrid',
    title: '',
    selType: 'rowmodel',
    columnLines: true,
    listeners:{
        afterrender:'SarprasRkbGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.SarprasRkb');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                listeners:{
                    click: 'kembali'
                }
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'PesertaDidik',
                paramMethod: 'SarprasRkb',
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        Ext.apply(this, this.initialConfig);

        
        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            locked: true,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1); 
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Wilayah',
            width: 150,
            sortable: true,
            dataIndex: 'nama',
			hideable: false,
            locked: true,
            renderer: function(v,p,r){

                if(r.data.id_level_wilayah){
                    return '<a style="color:#228ab7;text-decoration:none" href="#SarprasRkbSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah+'">'+v+'</a>';
                }else{
                    return v;
                }

            }
        },{
            text: 'Negeri',
            columns: [{
                header: 'Jumlah<br>PD',
                width: 100,
                sortable: true,
                dataIndex: 'pd_negeri',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Jumlah<br>R.Kelas',
                width: 100,
                sortable: true,
                dataIndex: 'jumlah_ruang_kelas_negeri',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Kebutuhan<br>RKB',
                width: 120,
                sortable: true,
                dataIndex: 'rkb_negeri',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            }]
        },{
            text:'Swasta',
            columns:[{
                header: 'Jumlah<br>PD',
                width: 100,
                sortable: true,
                dataIndex: 'pd_swasta',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Jumlah<br>R.Kelas',
                width: 100,
                sortable: true,
                dataIndex: 'jumlah_ruang_kelas_swasta',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Kebutuhan<br>RKB',
                width: 120,
                sortable: true,
                dataIndex: 'rkb_swasta',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            }]
        },{
            text:'Total',
            columns:[{
                header: 'Jumlah<br>PD',
                width: 100,
                sortable: true,
                dataIndex: 'pd_total',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Jumlah<br>R.Kelas',
                width: 100,
                sortable: true,
                dataIndex: 'jumlah_ruang_kelas_total',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            },{
                header: 'Kebutuhan<br>RKB',
                width: 120,
                sortable: true,
                dataIndex: 'rkb_total',
                hideable: false,
                align: 'right',
                renderer: function(v,p,r){
                    return grid.formatMoney(v);
                }
            }]
        }
        ];
        
        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();
        
        this.callParent(arguments);

        // this.store.load();
    }
});