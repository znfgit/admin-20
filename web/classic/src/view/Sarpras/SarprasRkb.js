Ext.define('Admin.view.Sarpras.SarprasRkb', {
    extend: 'Ext.container.Container',
    xtype: 'sarprasrkb',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'sarprasrkb',
    
    cls: 'sarprasrkb-container',

    // layout: 'responsivecolumn',
    // scrollable: 'y',
    layout: 'border',

    items: [{
        xtype:'tabpanel',
        region:'center',
        items:[{
            xtype: 'sarprasrkbgrid',
            title: 'Rekap Sarpras Berdasarkan Kebutuhan Ruang Kelas Baru',
            // Always 100% of container
            // responsiveCls: 'big-100',
            // minHeight:200
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        },{
            xtype: 'sarprasrkbbarchart',
            title: 'Chart Sarpras Berdasarkan Kebutuhan Ruang Kelas Baru',
            scrollable: true
            // Always 100% of container
            // responsiveCls: 'big-100',
            // minHeight:200
        }]
    }]
        
});
