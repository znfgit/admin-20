Ext.define('Admin.view.Sarpras.SarprasTingkatKerusakanGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.sarprastingkatkerusakangrid',
    title: '',
    selType: 'rowmodel',
    listeners:{
        afterrender:'SarprasTingkatKerusakanGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.SarprasTingkatKerusakan');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                name: 'kembaliButton',
                ui: 'soft-green',
                iconCls: 'x-fa fa-arrow-circle-left',
                listeners:{
                    click: 'kembali'
                }
            },{
                xtype: 'bentukpendidikancombo',
                emptyText: 'Pilih Bentuk Pendidikan',
                width: 200,
                listeners:{
                    change: 'bentukPendidikanComboChange'
                }
            },{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'Sarpras',
                paramMethod: 'TingkatKerusakan',
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: 'refresh'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a,
            c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {

        var grid = this;

        this.store = this.createStore();

        Ext.apply(this, this.initialConfig);


        this.columns = [{
            header: 'No',
            width: 50,
            sortable: true,
            dataIndex: 'no',
            hideable: false,
            locked: true,
            renderer: function(v,p,r){
                if(r.store.currentPage == 1){
                    var indexPos = 0;
                }else{
                    var indexPos = (r.store.config.pageSize) * (r.store.currentPage - 1);
                }
                return ( indexPos + (p.recordIndex + 1) );
            }
        },{
            header: 'Wilayah',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
			hideable: false,
            locked: true,
            renderer: function(v,p,r){

                if(r.data.id_level_wilayah){
                    return '<a style="color:#228ab7;text-decoration:none" href="#SarprasTingkatKerusakanSp/'+r.data.id_level_wilayah+'/'+r.data.kode_wilayah+'">'+v+'</a>';
                }else{
                    return v;
                }
            }
        }
        ,{
            text: 'R.Kelas',
            columns:[
                {
                    header: 'Baik',
                    align:'right',
                    dataIndex: 'r_kelas_baik',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'R. Ringan',
                    align:'right',
                    dataIndex: 'r_kelas_rusak_ringan',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'R. Sedang',
                    align:'right',
                    dataIndex: 'r_kelas_rusak_sedang',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'R. Berat',
                    align:'right',
                    dataIndex: 'r_kelas_rusak_berat',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'r_kelas_rusak_berat',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(r.data.r_kelas_baik + r.data.r_kelas_rusak_sedang + r.data.r_kelas_rusak_berat + r.data.r_kelas_rusak_ringan);
                    }
                }

            ]
        },{
            text: 'Perpustakaan',
            columns:[
                {
                    header: 'Baik',
                    align:'right',
                    dataIndex: 'perpustakaan_baik',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'R. Ringan',
                    align:'right',
                    dataIndex: 'perpustakaan_rusak_ringan',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'R. Sedang',
                    align:'right',
                    dataIndex: 'perpustakaan_rusak_sedang',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'R. Berat',
                    align:'right',
                    dataIndex: 'perpustakaan_rusak_berat',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'perpustakaan_rusak_berat',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(r.data.perpustakaan_baik + r.data.perpustakaan_rusak_sedang + r.data.perpustakaan_rusak_berat + r.data.perpustakaan_rusak_ringan);
                    }
                }

            ]
        },{
            text: 'Labor',
            columns:[
                {
                    header: 'Baik',
                    align:'right',
                    dataIndex: 'bengkel_baik',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'R. Ringan',
                    align:'right',
                    dataIndex: 'bengkel_rusak_ringan',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'R. Sedang',
                    align:'right',
                    dataIndex: 'bengkel_rusak_sedang',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'R. Berat',
                    align:'right',
                    dataIndex: 'bengkel_rusak_berat',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(v);
                    }
                },{
                    header: 'Total',
                    align:'right',
                    dataIndex: 'bengkel_rusak_berat',
                    width: 100,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        return grid.formatMoney(r.data.bengkel_baik + r.data.bengkel_rusak_sedang + r.data.bengkel_rusak_berat + r.data.bengkel_rusak_ringan);
                    }
                }

            ]
        }
        ];

        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();

        this.callParent(arguments);

        // this.store.load();
    }
});