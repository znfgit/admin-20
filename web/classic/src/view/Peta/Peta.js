Ext.define('Admin.view.Peta.Peta', {
    extend: 'Ext.container.Container',

    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'Ext.chart.interactions.ItemHighlight',
    ],

    // id: 'peta',

    // controller: 'dashboard',
    viewModel: {
        type: 'dashboard'
    },

    layout: 'border',
    // layout: 'responsivecolumn',
    
    // scrollable: true,
    
    listeners: {
        hide: 'onHideView'
    },

    items: [
        {
            xtype: 'panel',
            cls: 'dashboard-main-chart shadow-panel',
            // responsiveCls: 'big-60 small-100',
            html: '<div id="map_besar" style="height:'+(Ext.Element.getViewportHeight()-50)+'px;width:100%"></div>',
            headerPosition: 'bottom',
            title: "Klik Wilayah di peta untuk menampilkan detail",
            ui: 'light',
            iconCls: 'fa fa-map-marker',
            region: 'center'
        },
        {
            xtype: 'panel',
            ui: 'soft-red',
            title: 'Pencarian',
            itemId: 'map_pencarian',
            // height:150,
            height: Ext.Element.getViewportHeight()-165,
            // responsiveCls: 'big-40 small-100',
            iconCls: 'fa fa-search',
            collapsible: true,
            region: 'east',
            width: 300,
            collapsed: true,
            layout: {
                type: 'border'
            },
            items:[{
                xtype: 'form',
                itemId: 'pencarian_form',
                region: 'north',
                height: 100,
                bodyStyle: 'background:#cccccc',
                bodyPadding: 10,
                fieldDefaults: {
                    anchor: '100%',
                    labelWidth: 120
                },
                items:[{
                    xtype: 'textfield'
                    // ,fieldLabel: 'Kata Kunci'
                    ,labelAlign: 'top'
                    ,allowBlank: true
                    ,maxValue: 99999999
                    ,minValue: 0
                    ,maxLength: 100
                    ,enforceMaxLength: false
                    ,name: 'keyword'
                    ,emptyText: 'Ketik Nama Sekolah / NPSN ...'
                }],
                dockedItems:[{
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [{
                        text: "Cari",
                        itemId: 'cariBtn',
                        ui: 'soft-blue',
                        iconCls: 'fa fa-search'
                    }]
                }],
            },{
                xtype: 'panel',
                itemId: 'panel_result',
                region: 'center',
                scrollable: 'y',
                layout: {
                    type: 'vbox',
                    pack: 'start',
                    align: 'stretch'
                },
                // bodyStyle: 'background:yellow'
            }],
            listeners:{
                collapse: function(form){
                    // // form.up('container').down('')
                    // // Ext.MessageBox.alert('collapse');
                    // // form.up('container').down('panel[itemId=map_legend]').setHeight(Ext.Element.getViewportHeight()-165);
                    // form.up('container').down('panel[itemId=map_legend]').expand();
                },
                beforeexpand: function(form){
                    // // Ext.MessageBox.alert('Expand');
                    // form.up('container').down('panel[itemId=map_legend]').collapse();
                    // // form.up('container').down('panel[itemId=map_legend]').setHeight(Ext.Element.getViewportHeight()-270);
                }
            }
        },
        // {
        //     xtype: 'form',
        //     ui: 'soft-red',
        //     title: 'Pencarian',
        //     itemId: 'pencarian_form',
        //     // height:150,
        //     height: Ext.Element.getViewportHeight()-165,
        //     responsiveCls: 'big-40 small-100',
        //     iconCls: 'fa fa-search',
        //     collapsible: true,
        //     collapsed: true,
        //     bodyPadding: 10,
        //     fieldDefaults: {
        //         anchor: '100%',
        //         labelWidth: 120
        //     },
        //     items:[{
        //         xtype: 'textfield'
        //         // ,fieldLabel: 'Kata Kunci'
        //         ,labelAlign: 'top'
        //         ,allowBlank: true
        //         ,maxValue: 99999999
        //         ,minValue: 0
        //         ,maxLength: 100
        //         ,enforceMaxLength: false
        //         ,name: 'keyword'
        //         ,emptyText: 'Ketik Nama Sekolah / NPSN ...'
        //     }],
        //     dockedItems:[{
        //         xtype: 'toolbar',
        //         dock: 'bottom',
        //         items: [{
        //             text: "Cari",
        //             itemId: 'cariBtn',
        //             ui: 'soft-blue',
        //             iconCls: 'fa fa-search'
        //         }
        //         // ,{
        //         //     text: 'Bersihkan peta',
        //         //     itemId: 'bersihkanBtn',
        //         //     ui: 'soft-red',
        //         //     iconCls: 'fa fa-refresh'
        //         // }
        //         ]
        //     }],
        //     listeners:{
        //         collapse: function(form){
        //             // form.up('container').down('')
        //             // Ext.MessageBox.alert('collapse');
        //             // form.up('container').down('panel[itemId=map_legend]').setHeight(Ext.Element.getViewportHeight()-165);
        //             form.up('container').down('panel[itemId=map_legend]').expand();
        //         },
        //         beforeexpand: function(form){
        //             // Ext.MessageBox.alert('Expand');
        //             form.up('container').down('panel[itemId=map_legend]').collapse();
        //             // form.up('container').down('panel[itemId=map_legend]').setHeight(Ext.Element.getViewportHeight()-270);
        //         }
        //     }
            
        // }
        ,{
            xtype: 'panel',
            itemId: 'map_legend',
            // bodyStyle: 'background:#F39C12',
            collapsible: true,
            bodyStyle: 'background:#757575',
            cls: 'dashboard-main-chart shadow-panel',
            // responsiveCls: 'big-40 small-100',
            title: 'Informasi',
            ui: 'soft-green',
            height: Ext.Element.getViewportHeight()-165,
            scrollable: 'y',
            region: 'west',
            width: 300,
            collapsed: true,
            listeners:{
                collapse: function(form){
                    // // form.up('container').down('')
                    // // Ext.MessageBox.alert('collapse');
                    // // form.up('container').down('panel[itemId=map_legend]').setHeight(Ext.Element.getViewportHeight()-165);
                    // form.up('container').down('panel[itemId=map_pencarian]').expand();
                },
                beforeexpand: function(form){
                    // // Ext.MessageBox.alert('Expand');
                    // form.up('container').down('panel[itemId=map_pencarian]').collapse();
                    // // form.up('container').down('panel[itemId=map_legend]').setHeight(Ext.Element.getViewportHeight()-270);
                }
            }
            // html: '<div id="map_legend" style="height:'+(Ext.Element.getViewportHeight()-165)+'px;width:100%;margin:10px;color:white">Klik wilayah untuk melihat detail</div>'

        },
    ],
    destroy: function() {
        this.callParent();

        // !!! prevents memory leak.
        this.setBasicInfoComponent(null);

        console.log('destroy work');
    },
    listeners:{
        afterrender: function(container){

            console.log('nendang');

            var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

            Ext.Ajax.request({
                // url: 'getKoodinatWilayah?kode_wilayah=000000',
                url: 'getKoodinatWilayah?kode_wilayah='+session.kode_wilayah,
                success: function(response){
                    var text = response.responseText;
                    
                    var obj = Ext.JSON.decode(response.responseText);

                    // console.log(obj[0].lintang);

                    // Ext.MessageBox.alert('tes');
                    var map_besar = L.map('map_besar').setView([obj[0].bujur, obj[0].lintang], (obj[0].zoom+1));

                    var layerGroup = L.featureGroup().addTo(map_besar);
                    // var tile = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Shaded_Relief/MapServer/tile/{z}/{y}/{x}', {
                    //             attribution: 'Tiles &copy; Esri &mdash; Source: Esri',
                    //             maxZoom: 20
                    //         }).addTo(map);

                    // var tile = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    //                 maxZoom: 19,
                    //                 attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                    //             }).addTo(map_besar);

                    // var tile = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    //                 attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    //             }).addTo(map_besar);

                    var tile =  L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                                    maxZoom: 19,
                                    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                                }).addTo(map_besar);


                    function getColor(d) {
                        return d > 200000  ? '#8f0026' :
                               d > 150000  ? '#800026' :
                               d > 100000  ? '#006CDD' :
                               d > 75000   ? '#00db36' :
                               d > 50000   ? '#FC4E2A' :
                               d > 30000   ? '#FD8D3C' :
                               d > 20000   ? '#f4ee30' :
                               d > 10000   ? '#f4c030' :
                                             '#f24242';
                    }

                    function style(feature) {
                        return {
                            weight: 2,
                            opacity: 1,
                            color: '#4f4f4f',
                            dashArray: '3',
                            fillOpacity: 0.2,
                            // fillColor: 'black'
                            fillColor: getColor(feature.properties.pd)
                        };
                    }

                    function highlightFeature(e) {
                        var layer = e.target;

                        layer.setStyle({
                            weight: 2,
                            color: '#666',
                            dashArray: '',
                            fillOpacity: 0.3
                        });

                        if (!L.Browser.ie && !L.Browser.opera) {
                            layer.bringToFront();
                        }
            //            console.dir(layer);
                        // info.update(layer.feature.properties, statesData.candidate);
                    }

                    var geojson;

                    function resetHighlight(e) {
                        geojson.resetStyle(e.target);
                        // info.update();
                    }

                    function zoomToFeature(e) {
                        map_besar.fitBounds(e.target.getBounds());

                        console.log(feature.properties.kode_wilayah);
                    }

                    function onEachFeature(feature, layer) {
            //          console.dir(layer);
                        // var popupContent = "<h3 id=popup_"+feature.properties.kode_wilayah+">"+feature.properties.name+"</h3>";
                        var domInduk = document.createElement('div');

                        var domelem = document.createElement('h3');
                        domelem.id = "popup_"+feature.properties.kode_wilayah;
                        domelem.style = "cursor:pointer";
                        domelem.innerHTML = feature.properties.name;
                        domInduk.appendChild(domelem);

                        if(feature.properties.id_level_wilayah != 1){
                            var domelemBalik = document.createElement('div');
                            domelemBalik.className = "x-btn x-btn-default-medium x-unselectable";
                            domelemBalik.style = "color:white";
                            // domelemBalik.className = "";
                            // domelemBalik.className = "";
                            domelemBalik.innerHTML = 'Kembali ke wilayah induk';
                            domInduk.appendChild(domelemBalik);
                            
                            domelemBalik.onclick = function() {

                                Ext.Ajax.request({
                                    url: 'getGeoJsonBasic?kode_wilayah='+feature.properties.mst_kode_wilayah_induk+'&id_level_wilayah='+(feature.properties.id_level_wilayah-2),
                                    timeout: 24000000,
                                    success: function(response){
                                        layerGroup.clearLayers();
                                   
                                        var text = response.responseText;
                                        
                                        var objGeo = Ext.JSON.decode(response.responseText);

                                        geojson = L.geoJson({
                                            "type":"FeatureCollection",
                                            "features": objGeo
                                        }, {
                                            style: style,
                                            onEachFeature: onEachFeature
                                        }).addTo(layerGroup);

                                        map_besar.fitBounds(layerGroup.getBounds(),{

                                        });
                                    }
                                });

                            }
                            
                        }


                        domelem.onclick = function() {
                            // alert(this.id);
                            // do whatever else you want to do - open accordion etc

                            // map_besar.eachLayer(function (layer) {
                            // });

                            if(feature.properties.id_level_wilayah != 3){

                                Ext.Ajax.request({
                                    url: 'getGeoJsonBasic?kode_wilayah='+feature.properties.kode_wilayah+'&id_level_wilayah='+feature.properties.id_level_wilayah,
                                    timeout: 24000000,
                                    success: function(response){
                                        layerGroup.clearLayers();
                                        var text = response.responseText;
                                        
                                        var objGeo = Ext.JSON.decode(response.responseText);
                                    
                                        // console.log(obj);
                                
                                        geojson = L.geoJson({
                                            "type":"FeatureCollection",
                                            "features": objGeo
                                        }, {
                                            style: style,
                                            onEachFeature: onEachFeature
                                        }).addTo(layerGroup);

                                        layerGroup.getBounds();
                                    }
                                });

                            }


                        };

                        layer.bindPopup(domInduk);

                        // Ext.get('popup_'+feature.properties.kode_wilayah).on('click', function(){
                        //     alert('oke');
                        // });
                        
                        // popupContent.on('click', function(){
                        //     alert('oke');
                        // });

                        layer.on({
                            mouseover: highlightFeature,
                            mouseout: resetHighlight
                        });

                        layer.on('click', function (e) {
                            var kode_wilayah = feature.properties.kode_wilayah;

                            //window.location.replace("laman/detailprop/" + kode_wilayah);

                            map_besar.fitBounds(e.target.getBounds());

                            

                            // $('#map_legend').html( '<h2>'
                            //                             +feature.properties.name
                            //                             +'</h2>'
                            //                             +'PD: '+feature.properties.pd+'<br>'
                            //                             +'Guru: '+feature.properties.guru+'<br>'
                            //                             +'Pegawai: '+feature.properties.pegawai+'<br>'
                            //                             );

                            // this._div.innerHTML = '<center><h4 style=color:white>' + feature.properties.name + '</h4><hr></center>';

                            container.down('panel[itemId=map_legend]').setTitle(feature.properties.name);
                            container.down('panel[itemId=map_legend]').id_level_wilayah = feature.properties.id_level_wilayah;
                            container.down('panel[itemId=map_legend]').kode_wilayah = feature.properties.kode_wilayah;
                            
                            if(container.down('panel[itemId=map_legend]').down('panel[itemId=panelSummaryPeta]')){
                                container.down('panel[itemId=map_legend]').down('panel[itemId=panelSummaryPeta]').destroy();
                                container.down('panel[itemId=map_legend]').down('button').destroy();
                            }

                            container.down('panel[itemId=map_legend]').add({
                                xtype: 'panel',
                                itemId: 'panelSummaryPeta',
                                bodyStyle: 'background:#434343',
                                html: '<div id="map_legend" style="height:80px;width:100%;margin:10px;color:white">'
                                // + '<b>'+feature.properties.name+'</b>'
                                +'<br>'
                                +'<div style=float:left;width:50%>'
                                    + '<b>Sekolah: </b>'+feature.properties.sekolah
                                    +'<br>'
                                    + '<b>Peserta Didik: </b>'+feature.properties.pd
                                    +'<br>'
                                    + '<b>Rombel: </b>'+feature.properties.rombel
                                    +'<br>'
                                +'</div>'
                                +'<div style=float:left;width:50%>'
                                    + '<b>Guru: </b>'+feature.properties.guru
                                    +'<br>'
                                    + '<b>Pegawai: </b>'+feature.properties.pegawai
                                    +'<br>'
                                +'</div>'
                                +'<div style=clear:both></div>'
                                +'</div>'
                            },{
                                xtype: 'button',
                                text: 'Tampilkan Sebaran Sekolah',
                                iconCls: 'fa fa-map-marker',
                                width: '100%',
                                handler: function(btn){
                                    var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

                                    // if(session.id_level_wilayah == 0){
                                    //     wilayah = 'propinsi';
                                    // }else if(session.id_level_wilayah == 1){
                                    //     wilayah = 'kabupaten';
                                    // }else if(session.id_level_wilayah == 2){
                                    //     wilayah = 'kecamatan';
                                    // }else{
                                    //     wilayah = 'kecamatan';
                                    // }

                                    if(container.down('panel[itemId=map_legend]').id_level_wilayah == 1){
                                        wilayah = 'propinsi';
                                    }else if(container.down('panel[itemId=map_legend]').id_level_wilayah == 2){
                                        wilayah = 'kabupaten';
                                    }else if(container.down('panel[itemId=map_legend]').id_level_wilayah == 3){
                                        wilayah = 'kecamatan';
                                    }else{
                                        wilayah = 'kecamatan';
                                    }

                                    Ext.Ajax.request({
                                        url: 'SatuanPendidikan/SatuanPendidikan?'+wilayah+'='+feature.properties.kode_wilayah+'&limit=1000000',
                                        success: function(response){
                                            var text = response.responseText;
                                            
                                            var obj = Ext.JSON.decode(response.responseText);

                                            // console.log(obj.rows);

                                            console.log(Object.keys(map_besar._layers).length);

                                            // for(key in map_besar._layers){
                                            //     if (map_besar._layers.hasOwnProperty(key)) {
                                            //         // console.log(key + " -> " + map_besar._layers[key]._icon);
                                            //         if(map_besar._layers[key]._icon){
                                            //             map_besar.removeLayer(map_besar._layers[key]);
                                            //         }
                                            //     }
                                            // }

                                            // if(markerClusters){
                                            //     //do something
                                            //     map_besar.removeLayer(markerClusters);
                                            // }
                                            
                                            var markerClusters = L.markerClusterGroup();

                                            for (var i = 0; i < obj.rows.length; i++) {
                                            
                                                if(obj.rows[i].lintang && obj.rows[i].bujur){

                                                    if(obj.rows[i].status_sekolah == 1){
                                                        var status = 'Negeri';
                                                    }else{
                                                        var status = 'Swasta';
                                                    }

                                                    var popup = '<span style=font-weight:bold;font-size:20px>'+obj.rows[i].nama+'</span>' +
                                                              '<br/><b>NPSN:</b>' + obj.rows[i].npsn +
                                                              '<br/><b>Bentuk Pendidikan:</b> ' + obj.rows[i].bentuk_pendidikan_id_str+
                                                              '<br/><b>Status:</b> ' + status +
                                                              '<br/><b>Alamat:</b> ' + obj.rows[i].alamat_jalan +
                                                              '<br/><b>Kecamatan:</b> ' + obj.rows[i].kecamatan +
                                                              '<br/><b>Kabupaten:</b> ' + obj.rows[i].kabupaten +
                                                              '<br/><br/><a style="color:white" href="#ProfilSatuanPendidikan/'+obj.rows[i].sekolah_id+'" class="x-btn x-unselectable x-btn-default-medium">Profil Sekolah</a> ';

                                                    marker = new L.Marker([obj.rows[i].lintang, obj.rows[i].bujur], {draggable:false}).bindPopup( popup );
                                                    // map_besar.addLayer(marker);
                                                    
                                                    markerClusters.addLayer( marker );
                                                    // map_besar.removeLayer(marker);
                                                }
                                            };

                                            layerGroup.addLayer(markerClusters);
                                        }
                                    });
                                    
                                }
                            });
                                

                             
                            // update('<div id="map_legend" style="height:2000px;width:100%;margin:10px;color:white">'
                            //     // + '<b>'+feature.properties.name+'</b>'
                            //     +'<br>'
                            //     +'<div style=float:left;width:50%>'
                            //         + '<b>Sekolah: </b>'+feature.properties.sekolah
                            //         +'<br>'
                            //         + '<b>Peserta Didik: </b>'+feature.properties.pd
                            //         +'<br>'
                            //         + '<b>Rombel: </b>'+feature.properties.rombel
                            //         +'<br>'
                            //     +'</div>'
                            //     +'<div style=float:left;width:50%>'
                            //         + '<b>Guru: </b>'+feature.properties.guru
                            //         +'<br>'
                            //         + '<b>Pegawai: </b>'+feature.properties.pegawai
                            //         +'<br>'
                            //     +'</div>'
                            //     +'<div style=clear:both></div>'
                            //     +'</div>');
                        });
                    }

                    var popup = L.popup();

                    function onMapClick(e) {
                        var layer = e.target;

                        // console.log(layer.properties.kode_wilayah);

                        //return "You clicked the map at propinsi ";
                        
                    }

                    map_besar.on('click', onMapClick);
                    
                    var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
                    
                    Ext.Ajax.request({
                        url: 'getGeoJsonBasic?kode_wilayah='+session.kode_wilayah+'&id_level_wilayah='+session.id_level_wilayah,
                        timeout: 24000000,
                        success: function(response){
                            var text = response.responseText;
                            
                            var objGeo = Ext.JSON.decode(response.responseText);
                        
                            // console.log(obj);
                    
                            geojson = L.geoJson({
                                "type":"FeatureCollection",
                                "features": objGeo
                            }, {
                                style: style,
                                onEachFeature: onEachFeature
                            }).addTo(layerGroup);

                            map_besar.fitBounds(layerGroup.getBounds());
                        }
                    });


                    container.down('panel[itemId=map_pencarian]').down('form[itemId=pencarian_form]').down('toolbar').down('button[itemId=cariBtn]').on('click', function(btn){

                        if(session.id_level_wilayah == 2){
                            var params = '?kabupaten='+session.kode_wilayah;
                        }else if(session.id_level_wilayah == 3){
                            var params = '?kecamatan='+session.kode_wilayah;
                        }else if(session.id_level_wilayah == 1){
                            var params = '?propinsi='+session.kode_wilayah; 
                        }else{
                            var params = '?yes=null'; 
                        }

                        var keyword = btn.up('form').down('textfield[name=keyword]').getValue();

                        if(keyword.length > 0){

                            params += '&keyword='+keyword;

                            Ext.Ajax.request({
                                url: 'SatuanPendidikan/SatuanPendidikan'+params,
                                success: function(response){
                                    var text = response.responseText;
                                    
                                    var obj = Ext.JSON.decode(response.responseText);

                                    // for(key in map_besar._layers){
                                    //     if (map_besar._layers.hasOwnProperty(key)) {
                                    //         // console.log(key + " -> " + map_besar._layers[key]._icon);
                                    //         if(map_besar._layers[key]._icon){
                                    //             map_besar.removeLayer(map_besar._layers[key]);
                                    //         }
                                    //     }
                                    // }

                                    if(obj.rows.length > 0){

                                        btn.up('form').up('panel').down('panel[itemId=panel_result]').removeAll();

                                        // console.log(btn.up('form').up('panel').down('panel[itemId=panel_result]').items.items);

                                        // var yangAdaSekarang = btn.up('form').up('panel').down('panel[itemId=panel_result]').items.items.length;

                                        // console.log(yangAdaSekarang);

                                        // if(yangAdaSekarang > 0){

                                        //     for (var i = 0; i < yangAdaSekarang; i++) {
                                        //         btn.up('form').up('panel').down('panel[itemId=panel_result]').items.items[i].destroy();
                                        //     };

                                        // }

                                        var markerClusters = L.markerClusterGroup();

                                        for (var i = 0; i < obj.rows.length; i++) {
                                            // obj.rows[i]
                                            if(obj.rows[i].lintang && obj.rows[i].bujur){

                                                if(obj.rows[i].status_sekolah == 1){
                                                    var status = 'Negeri';
                                                }else{
                                                    var status = 'Swasta';
                                                }

                                                // var marker = L.marker([obj.rows[i].lintang, obj.rows[i].bujur]).addTo(map_besar);
                                                var popup = '<span style=font-weight:bold;font-size:20px>'+obj.rows[i].nama+'</span>' +
                                                          '<br/><b>NPSN:</b>' + obj.rows[i].npsn +
                                                          '<br/><b>Bentuk Pendidikan:</b> ' + obj.rows[i].bentuk_pendidikan_id_str+
                                                          '<br/><b>Status:</b> ' + status +
                                                          '<br/><b>Alamat:</b> ' + obj.rows[i].alamat_jalan +
                                                          '<br/><b>Kecamatan:</b> ' + obj.rows[i].kecamatan +
                                                          '<br/><b>Kabupaten:</b> ' + obj.rows[i].kabupaten +
                                                          '<br/><br/><a style="color:white" href="#ProfilSatuanPendidikan/'+obj.rows[i].sekolah_id+'" class="x-btn x-unselectable x-btn-default-medium">Profil Sekolah</a> ';

                                                marker = new L.Marker([obj.rows[i].lintang, obj.rows[i].bujur], {draggable:false}).bindPopup( popup );
                                                // map_besar.addLayer(marker);
                                                
                                                markerClusters.addLayer( marker );

                                                // map_besar.removeLayer(marker);
                                                

                                                btn.up('form').up('panel').down('panel[itemId=panel_result]').add({
                                                    xtype: 'panel',
                                                    border: false,
                                                    bodyPadding: 10,
                                                    foo: 'bar',
                                                    height:80,
                                                    layout: 'responsivecolumn',
                                                    items: [{
                                                        xtype: 'panel',
                                                        responsiveCls: 'big-80 small-100',
                                                        html: '<a href="#ProfilSatuanPendidikan/'+obj.rows[i].sekolah_id+'" style="color:#35BAF6;text-decoration:none"><b>'+obj.rows[i].nama+'</b></a>'
                                                                + '<br>NPSN: '+ obj.rows[i].npsn
                                                                + '<br>Alamat: '+ obj.rows[i].alamat_jalan + ', ' + obj.rows[i].kecamatan
                                                    },{
                                                        xtype: 'panel',
                                                        responsiveCls: 'big-20 small-100',
                                                        items:[{
                                                            xtype: 'button',
                                                            // text: 'Detail',
                                                            sekolah_id: obj.rows[i].sekolah_id,
                                                            iconCls: 'fa fa-map-marker',
                                                            lintang: obj.rows[i].lintang,
                                                            bujur: obj.rows[i].bujur,
                                                            width: '100%',
                                                            handler: function(btn){
                                                                map_besar.setView([btn.lintang, btn.bujur], 17);
                                                            }
                                                        }]

                                                    }]
                                                });
                                                
                                            }

                                        };

                                        layerGroup.addLayer(markerClusters);
                                        map_besar.fitBounds(markerClusters.getBounds());

                                    }else{
                                        Ext.MessageBox.alert('Hasil Pencarian', 'Sekolah tidak ditemukan');
                                    }
                                }
                            });
                        
                        }else{
                            Ext.MessageBox.alert('Peringatan','Kata Kunci / NPSN tidak boleh kosong!');
                        }




                    });


                    // container.down('form[itemId=pencarian_form]').down('toolbar').down('button[itemId=bersihkanBtn]').on('click', function(btn){
                    //     map_besar.removeLayer(markerClusters);

                    //     // for(key in map_besar._layers){
                    //     //     if (map_besar._layers.hasOwnProperty(key)) {
                    //     //         // console.log(key + " -> " + map_besar._layers[key]._icon);
                    //     //         if(map_besar._layers[key]._icon){
                    //     //             map_besar.removeLayer(map_besar._layers[key]);
                    //     //             // console.log(map_besar._layers[key]);
                    //     //         }
                    //     //     }
                    //     // }
                    // });
                }   
            });

            
            // map.fitBounds();
        }
    }
});
