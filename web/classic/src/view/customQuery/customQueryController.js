Ext.define('Admin.view.customQuery.customQueryController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.customQuery',

    // TODO - Add control logic or remove if not needed
    prosesCustomQuery: function(btn){

        Ext.MessageBox.show({
            msg: 'Mengolah Data ...',
            progressText: 'Mengolah Data ...',
            width: 200,
            wait: true
        });

    	var panels = btn.up('toolbar').up('panel').down('panel[itemId=kriteriaQueryPanel]').items.items;
    	var hasilGrid = btn.up('toolbar').up('panel').down('hasilQueryGrid');

    	var urlQuery = '';

    	for (var i = 0; i < panels.length; i++) {
    		console.log(panels[i].itemId);
    		console.log(panels[i].down('textfield[itemId=keyword]').getValue());

    		var str = "&" + panels[i].itemId + "=" + panels[i].down('textfield[itemId=keyword]').getValue() + panels[i].down('combobox').getValue();
    	
    		urlQuery += str;
    	};

    	console.log(urlQuery);

        if(btn.aksi == 'tampil'){

        	Ext.Ajax.request({
                url     : 'CustomQuery/prosesCustomQuery?'+urlQuery,
                method  : 'GET',
                success : function(response){

                    var json = Ext.JSON.decode(response.responseText);
                    var __columns = [];

                    for (var i = 0; i <= json.kolom.length - 1; i++) {
                        // console.log(json.kolom[i]);
                        var _kol = json.kolom[i].name.split(".");

                        var kolom = Ext.create('Ext.grid.column.Column', {
                            header: json.kolom[i].text.replace("pegawai", "tendik"),
                            width: 120,
                            sortable: true,
                            locked: false,
                            align: 'left',
                            dataIndex: _kol[1],
                            hideable: false
                        });

                        __columns.push(kolom);
                    }

                    var gridStore = Ext.create('Ext.data.Store', {
                        fields: json.kolom,
                        data : json.rows
                    });

                    // console.log(json.rows.length);

                    if(btn.up('toolbar').down('tbtext')){
                        btn.up('toolbar').down('tbtext').destroy();
                    }
                    
                    btn.up('toolbar').add('->',{
                        xtype: 'tbtext',
                        text: 'Jumlah Data: '+ json.rows.length 
                    });

                    hasilGrid.reconfigure(gridStore, __columns);
                    
                    Ext.MessageBox.hide();
                },
                failure: function () {
                    Ext.MessageBox.hide();
                }
            });

        }else if(btn.aksi == 'unduh'){
            Ext.MessageBox.hide();
            window.location.replace('CustomQuery/prosesCustomQuery?'+urlQuery+"&tipe_output=excel");
        }


        // console.log(btn.up('toolbar').up('panel').down('panel[itemId=kriteriaQueryPanel]').items.items);
    }
});
