Ext.define('Admin.view.customQuery.hasilQueryGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.hasilQueryGrid',
    title: '',
    controller: 'customQuery',
    columnLines: true,
    createStore: function() {
        return Ext.create('Admin.store.customQuery');
    },
    createEditing: function() {
        return Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2,
            pluginId: 'editing'
        });
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Tambah',
                //cls: 'addbutton',
                iconCls: 'x-fa fa-plus',
                // glyph: 61525,
                ui: 'soft-blue',
                scope: this,
                action: 'add',
                listeners:{
                    click: 'addRecord'
                }
            }, {
                xtype: 'button',
                text: 'Ubah',
                iconCls: 'x-fa fa-pencil',
                // glyph: 61508,
                //cls: 'editbutton',
                itemId: 'edit',
                ui: 'soft-purple',
                scope: this,
                action: 'edit',
                listeners:{
                    click: 'editRecord'
                }
            }, {
                xtype: 'button',
                text: 'Simpan',
                iconCls: 'x-fa fa-save',
                // glyph: 61639,
                //cls: 'editbutton',
                itemId: 'save',
                ui: 'soft-green',
                scope: this,
                action: 'save',
                listeners:{
                    click: 'saveRecord'
                }
            }, {
                xtype: 'button',
                text: 'Hapus',
                //cls: 'deletebutton',
                iconCls: 'x-fa fa-trash',
                // glyph: 61526,
                itemId: 'delete',
                ui: 'soft-red',
                scope: this,
                action: 'delete',
                listeners:{
                    click: 'deleteRecord'
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        // Assume all the setting upfront in initialConfig is applied directly to the object
        Ext.apply(this, this.initialConfig);

        // You can instaniate the component with basic parameters that affects filtering of the store
        // For example:
        // {
        //     xtype: 'yourgrid',
        //     baseParams: {
        //         area_id: 120,
        //         status_id: 1
        //     }
        // }      
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        // this.getSelectionModel().setSelectionMode('SINGLE');
        
        var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            listeners: {
                cancelEdit: function(rowEditing, context) {
                    // Canceling editing of a locally added, unsaved record: remove it
                    if (context.record.phantom) {
                        grid.store.remove(context.record);
                    }
                }
            }
        });
        
        var editing = this.createEditing();

        // this.rowEditing = editing;
        // this.plugins = [editing];

        
        this.columns = [];
        
        // this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();
        
        this.callParent(arguments);
    }
});