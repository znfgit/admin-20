Ext.define('Admin.view.customQuery.customQuery', {
    extend: 'Ext.container.Container',
    xtype: 'customQueryPanel',
    requires: [
        'Ext.layout.container.Accordion'
    ],

    controller: 'customQuery',
    
    cls: 'customQuery-container',
    
    layout: {
        type: 'border'
    },

    items: [
        {
            xtype: 'panel',
            title: 'Custom Query',
            iconCls: 'right-icon x-fa fa-list',
            region: 'west',
            width: 300,
            split: true,
            collapsible: true,
            layout: 'accordion',
            items:[{
                xtype: 'kriteriaSekolahIndividu',
                title: 'Data Pokok Sekolah'
            },{
                xtype: 'kriteriaSekolahIndividuTurunan',
                title: 'Data Rekap Peserta Didik',
                // itemId: 'treePd',
                jenis: 'pd'
            },{
                xtype: 'kriteriaSekolahIndividuTurunan',
                title: 'Data Rekap Guru',
                // itemId: 'treeGuru',
                jenis: 'guru'
            },{
                xtype: 'kriteriaSekolahIndividuTurunan',
                title: 'Data Rekap Tendik',
                // itemId: 'treeTendik',
                jenis: 'pegawai'
            }]
        }
        ,{
            xtype: 'panel',
            // title: 'Custom Query',
            // iconCls: 'right-icon x-fa fa-sliders',
            region: 'center',
            layout: 'border',
            dockedItems:[{
                xtype: 'toolbar',
                dock: 'top',
                items:[{
                    text: 'Tampilkan',
                    ui: 'soft-blue',
                    iconCls: 'x-fa fa-send',
                    aksi: 'tampil',
                    listeners:{
                        click: 'prosesCustomQuery'
                    }
                },{
                    text: 'Unduh Xls',
                    ui: 'soft-green',
                    iconCls: 'x-fa fa-download',
                    aksi: 'unduh',
                    listeners:{
                        click: 'prosesCustomQuery'
                    }
                }]
            }],
            items:[{
                xtype: 'panel',
                region: 'north',
                scrollable: true,
                layout: {
                    type: 'vbox',
                    pack: 'start',
                    align: 'stretch'
                },
                itemId: 'kriteriaQueryPanel',
                flex: 1,
                split: true
            },{
                xtype: 'hasilQueryGrid',
                itemId: 'hasilQueryPanel',
                region: 'center',
                flex: 2
            }]
        }
    ]
});
