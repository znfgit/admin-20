Ext.define('Admin.view.customQuery.kriteriaSekolahIndividu', {
    extend: 'Ext.tree.Panel',
    
    requires: [
        'Ext.data.TreeStore'
    ],
    xtype: 'kriteriaSekolahIndividu',
    
    
    rootVisible: false,
    useArrows: true,
    frame: false,
    // width: 280,
    // height: 300,
    bufferedRenderer: false,
    animate: true,
    listeners: {
        checkchange: function( node, checked, eOpts ){
            
            var panel = this;
            // console.log(panel.up('panel').up('container').down('panel[region=center]').down('panel[itemId=kriteriaQueryPanel]'));
            var panelKriteria = panel.up('panel').up('container').down('panel[region=center]').down('panel[itemId=kriteriaQueryPanel]');

            // console.log(node.data);
            // console.log(checked);

            if(node.data.tipe_combo){

                console.log(node.data.itemId);

                if(node.data.itemId == 'sekolah-bentuk_pendidikan'){
                    
                    var keyword = Ext.create('Admin.view._components.combo.BentukPendidikanCombo',{
                        emptyText: 'ketik bentuk pendidikan ...',
                        itemId: 'keyword',
                        // valueField: 'nama',
                        // displayField: 'nama',
                        flex: 1
                    });

                }else if(node.data.itemId == 'sekolah-propinsi'){

                    var keyword = Ext.create('Admin.view._components.combo.MstWilayahCombo',{
                        flex: 1,
                        emptyText: 'ketik nama propinsi ...',
                        valueField: 'nama',
                        itemId: 'keyword',
                        listeners:{
                            afterrender: function(combo){
                                combo.getStore().on('beforeload', function(store){
                                    Ext.apply(store.proxy.extraParams, {
                                        id_level_wilayah: 1
                                    });
                                });
                            }
                        },
                    });

                }else if(node.data.itemId == 'sekolah-kabupaten'){

                    var keyword = Ext.create('Admin.view._components.combo.MstWilayahCombo',{
                        flex: 1,
                        emptyText: 'ketik nama kabupaten ...',
                        valueField: 'nama',
                        itemId: 'keyword',
                        listeners:{
                            afterrender: function(combo){
                                combo.getStore().on('beforeload', function(store){
                                    Ext.apply(store.proxy.extraParams, {
                                        id_level_wilayah: 2
                                    });
                                });
                            }
                        },
                    });

                }else if(node.data.itemId == 'sekolah-kecamatan'){

                    var keyword = Ext.create('Admin.view._components.combo.MstWilayahCombo',{
                        flex: 1,
                        emptyText: 'ketik nama kecamatan ...',
                        valueField: 'nama',
                        itemId: 'keyword',
                        listeners:{
                            afterrender: function(combo){
                                combo.getStore().on('beforeload', function(store){
                                    Ext.apply(store.proxy.extraParams, {
                                        id_level_wilayah: 3
                                    });
                                });
                            }
                        },
                    });

                }else if(node.data.itemId == 'sekolah-status_kepemilikan'){

                    var keyword = Ext.create('Admin.view._components.combo.StatusKepemilikanCombo',{
                        flex: 1,
                        itemId: 'keyword',
                        emptyText: 'pilih status kepemilikan...',
                        valueField: 'nama'
                    });

                }else if(node.data.itemId == 'sekolah-status_sekolah'){

                    var keyword = Ext.create('Admin.view._components.combo.StatusSekolahCombo',{
                        flex: 1,
                        itemId: 'keyword',
                        emptyText: 'pilih status sekolah...'
                    });

                }


            }else{
                var keyword = {
                        xtype: 'textfield',
                        emptyText: '?',
                        itemId: 'keyword',
                        flex: 1
                    };
            }

            var kriteriaStore = Ext.create('Ext.data.Store', {
                fields: ['id', 'nama'],
                data : [
                    {"id":"|EQUAL", "nama":"sama dengan"},
                    {"id":"|LIKE", "nama":"mengandung kata"},
                    {"id":"|LESSTHAN", "nama":"kurang dari"},
                    {"id":"|LESSTHANEQUAL", "nama":"kurang dari atau sama dengan"},
                    {"id":"|GREATERTHAN", "nama":"lebih besar dari"},
                    {"id":"|GREATERTHANEQUAL", "nama":"lebih besar dari atau sama dengan"},
                    {"id":"|ISNOTNULL", "nama":"terisi"},
                    {"id":"|ISNULL", "nama":"tidak terisi"}
                ]
            });


            if(checked){
                panelKriteria.add({
                    xtype: 'panel',
                    itemId: node.data.itemId,
                    height: 45,
                    // border: true,
                    layout: {
                        type: 'hbox',
                        pack: 'start',
                        align: 'stretch'
                    },
                    style: 'border-bottom: 1px solid #cccccc;',
                    bodyPadding: 5,
                    items:[{
                        xtype: 'panel',
                        bodyPadding: 5,
                        bodyStyle: 'font-weight:bold;font-size:15px',
                        html: node.data.text,
                        flex: 1
                    },{
                        xtype: 'combobox',
                        emptyText: 'Kriteria',
                        flex: 1,
                        valueField: 'id',
                        displayField: 'nama',
                        allowBlank: false,
                        forceSelection: true,
                        itemId: 'kriteria',
                        name: 'kriteria',
                        store: kriteriaStore,
                        listeners:{
                            afterrender: function (combo) {
                                combo.setValue('|LIKE');
                            }
                        }
                    },keyword]
                })
            }else{
                panelKriteria.down('panel[itemId='+node.data.itemId+']').destroy();
            }
        }
    },
    initComponent: function(){

        Ext.apply(this, {
            store: new Ext.data.TreeStore({
                proxy: {
                    type: 'ajax',
                    url: 'CustomQuery/kriteriaSekolahIndividu'
                },
                sorters: [{
                    property: 'urutan',
                    direction: 'ASC'
                }, {
                    property: 'text',
                    direction: 'ASC'
                }]
            })
        });
        this.callParent();
    },
    
    // onCheckedNodesClick: function(){
    //     var records = this.getView().getChecked(),
    //         names = [];
                   
    //     Ext.Array.each(records, function(rec){
    //         names.push(rec.get('text'));
    //     });
                    
    //     Ext.MessageBox.show({
    //         title: 'Selected Nodes',
    //         msg: names.join('<br />'),
    //         icon: Ext.MessageBox.INFO
    //     });
    // }
});