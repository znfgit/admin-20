Ext.define('Admin.view.dashboard.Dashboard', {
    extend: 'Ext.container.Container',

    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'admin.view.dashboard.PieSpBentukPendidikan',
        'admin.view.dashboard.PieSarprasKerusakan',
        'Ext.chart.interactions.ItemHighlight',
        // 'GeoExt.panel.Map',
        // 'GeoExt.Action'
    ],

    id: 'dashboard',

    controller: 'dashboard',
    viewModel: {
        type: 'dashboard'
    },

    layout: 'responsivecolumn',

    scrollable: true,

    listeners: {
        hide: 'onHideView'
    },

    items: [
        {
            xtype: 'panel',
            // ui: 'soft-red',
            title: 'Versi 2017 Beta'
        }
        ,{
            xtype: 'panel',
            height: 120,
            responsiveCls: 'big-25 small-100',
            bodyStyle: 'background:#E44959',
            itemId: 'panelSekolahTotal',
            layout:{
                type: 'border'
            },
            items:[{
                xtype: 'panel',
                region: 'center',
                flex: 2,
                html: 'angka',
                bodyStyle: 'color:#ffffff;background:transparent;text-align:right',
                bodyPadding: 10,
                itemId: 'paneljumlahkepsek',
                html: 'Jumlah Sekolah<div style="color:#ffffff;font-size:3vw;font-weight:bold;margin-top:10px;margin-bottom:10px">0</div>'
            },{
                xtype: 'panel',
                region: 'west',
                flex: 1,
                html: 'icon',
                bodyStyle: 'color:white;background:transparent;padding-top:50px',
                bodyPadding: 10,
                html: '<i class="fa fa-building fa-5" style="font-size:4vw;color:white" aria-hidden="true"></i>'
            }]
        },
        {
            xtype: 'panel',
            height: 120,
            responsiveCls: 'big-25 small-100',
            itemId: 'panelPesertaDidik',
            bodyStyle: 'background:#F39C12',
            // bodyStyle: 'background:#00e000',
            layout:{
                type: 'border'
            },
            items:[{
                xtype: 'panel',
                region: 'center',
                flex: 2,
                html: 'angka',
                bodyStyle: 'color:white;background:transparent;text-align:right',
                bodyPadding: 10,
                itemId: 'paneljumlahtas',
                html: 'Jumlah Peserta Didik<div style="color:#ffffff;font-size:3vw;font-weight:bold;margin-top:10px;margin-bottom:10px">0</div>'
            },{
                xtype: 'panel',
                region: 'west',
                flex: 1,
                html: 'icon',
                bodyStyle: 'color:white;background:transparent;padding-top:50px',
                bodyPadding: 10,
                html: '<i class="fa fa-child fa-5" style="font-size:4vw;color:white" aria-hidden="true"></i>'
            }]
        },
        {
            xtype: 'panel',
            height: 120,
            responsiveCls: 'big-25 small-100',
            bodyStyle: 'background:#00e000',
            itemId: 'panelGuru',
            layout:{
                type: 'border'
            },
            items:[{
                xtype: 'panel',
                region: 'center',
                flex: 2,
                html: 'angka',
                bodyStyle: 'color:white;background:transparent;text-align:right',
                bodyPadding: 10,
                itemId: 'paneljumlahlaboran',
                html: 'Jumlah Guru<div style="color:#ffffff;font-size:3vw;font-weight:bold;margin-top:10px;margin-bottom:10px">0</div>'
            },{
                xtype: 'panel',
                region: 'west',
                flex: 1,
                html: 'icon',
                bodyStyle: 'color:white;background:transparent;padding-top:50px',
                bodyPadding: 10,
                html: '<i class="fa fa-graduation-cap fa-5" style="font-size:4vw;color:white" aria-hidden="true"></i>'
            }]
        },
        {
            xtype: 'panel',
            height: 120,
            responsiveCls: 'big-25 small-100',
            bodyStyle: 'background:#d800e0',
            itemId: 'panelTendik',
            layout:{
                type: 'border'
            },
            items:[{
                xtype: 'panel',
                region: 'center',
                flex: 2,
                html: 'angka',
                bodyStyle: 'color:white;background:transparent;text-align:right',
                bodyPadding: 10,
                itemId: 'paneljumlahpustakawan',
                html: 'Jumlah Tendik<div style="color:#ffffff;font-size:3vw;font-weight:bold;margin-top:10px;margin-bottom:10px">0</div>'
            },{
                xtype: 'panel',
                region: 'west',
                flex: 1,
                html: 'icon',
                bodyStyle: 'color:white;background:transparent;padding-top:50px',
                bodyPadding: 10,
                html: '<i class="fa fa-graduation-cap fa-5" style="font-size:4vw;color:white" aria-hidden="true"></i>'
            }]
        },{
            xtype: 'dashboardnetworkpanel',
            ui: 'yellow',
            // ui: 'highlight-framed',
            // border: true,
            // 60% width when viewport is big enough,
            // 100% when viewport is small
            responsiveCls: 'big-100 small-100',
            minHeight:200,
            border: false
        }
        ,{
            xtype: 'piespbentukpendidikan',
            responsiveCls: 'big-50 small-100',
            ui: 'soft-red'
            // height:550
        }
        ,{
            xtype: 'piesarpraskerusakan',
            title: 'Panel Kerusakan Ruang Kelas',
            cls: 'dashboard-main-chart shadow-panel',
            responsiveCls: 'big-50 small-100',
            ui: 'soft-green',
            height: 642
        },

        // {
        //     xtype: 'panel',
        //     cls: 'dashboard-main-chart shadow-panel',
        //     responsiveCls: 'big-50 small-100',
        //     html: '<div id="map" style="height:345px;width:100%"></div>',
        //     headerPosition: 'bottom',
        //     title: "Peta",
        //     ui: 'light',
        //     iconCls: 'fa fa-map-marker',
        //     tools:[{
        //         xtype: 'button',
        //         text: 'Perbesar Peta',
        //         iconCls: 'fa fa-map-marker',
        //         handler: function(btn){
        //             window.location.replace('#peta');
        //         }
        //     }]
        // }
        // ,{
        //     xtype: 'panel',
        //     itemId: 'map_legend',
        //     bodyStyle: 'background:#F39C12',
        //     cls: 'dashboard-main-chart shadow-panel',
        //     responsiveCls: 'big-50 small-100',
        //     html: '<div id="map_legend" style="height:110px;width:100%;margin:10px;color:white">Klik wilayah untuk melihat detail</div>'

        // },
        // {
        //     xtype: 'dashboardhddusagepanel',
        //     responsiveCls: 'big-20 small-50'
        // },
        // {
        //     xtype: 'dashboardearningspanel',
        //     responsiveCls: 'big-20 small-50'
        // },
        // {
        //     xtype: 'dashboardsalespanel',
        //     responsiveCls: 'big-20 small-50'
        // },
        // {
        //     xtype: 'dashboardtopmoviepanel',
        //     responsiveCls: 'big-20 small-50'
        // },
        // {
        //     xtype: 'dashboardweatherpanel',
        //     responsiveCls: 'big-40 small-100'
        // },
        // {
        //     xtype: 'pdtingkatbarchart',
        //     responsiveCls: 'big-100 small-100',
        //     ui: 'soft-blue',
        //     controller: 'pdtingkat',
        //     title: 'Peserta Didik Berdasarkan Tingkat Pendidikan'
        //     // height:550
        // }
    ],
    destroy: function() {
        this.callParent();

        // !!! prevents memory leak.
        this.setBasicInfoComponent(null);

        console.log('destroy work');
    },
    listeners:{
        afterrender: function(container){

            Ext.Ajax.request({
                url: 'getKoodinatWilayah',
                success: function(response){
                    var text = response.responseText;

                    var obj = Ext.JSON.decode(response.responseText);

                    // console.log(obj[0].lintang);

                    // Ext.MessageBox.alert('tes');
                    var map = L.map('map').setView([obj[0].bujur, obj[0].lintang], obj[0].zoom);
                    // var tile = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Shaded_Relief/MapServer/tile/{z}/{y}/{x}', {
                    //             attribution: 'Tiles &copy; Esri &mdash; Source: Esri',
                    //             maxZoom: 20
                    //         }).addTo(map);

                    var tile = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                                    maxZoom: 19,
                                    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                                }).addTo(map);


                    function getColor(d) {
                        return d > 200000  ? '#8f0026' :
                               d > 150000  ? '#800026' :
                               d > 100000  ? '#006CDD' :
                               d > 75000   ? '#00db36' :
                               d > 50000   ? '#FC4E2A' :
                               d > 30000   ? '#FD8D3C' :
                               d > 20000   ? '#f4ee30' :
                               d > 10000   ? '#f4c030' :
                                             '#f24242';
                    }

                    function style(feature) {
                        return {
                            weight: 2,
                            opacity: 1,
                            color: 'white',
                            dashArray: '3',
                            fillOpacity: 0.7,
                            fillColor: getColor(feature.properties.pd)
                        };
                    }

                    function highlightFeature(e) {
                        var layer = e.target;

                        layer.setStyle({
                            weight: 2,
                            color: '#666',
                            dashArray: '',
                            fillOpacity: 0.7
                        });

                        if (!L.Browser.ie && !L.Browser.opera) {
                            layer.bringToFront();
                        }
            //            console.dir(layer);
                        // info.update(layer.feature.properties, statesData.candidate);
                    }

                    var geojson;

                    function resetHighlight(e) {
                        geojson.resetStyle(e.target);
                        // info.update();
                    }

                    function zoomToFeature(e) {
                        map.fitBounds(e.target.getBounds());

                        console.log(feature.properties.kode_wilayah);
                    }

                    function onEachFeature(feature, layer) {
            //          console.dir(layer);
                        layer.on({
                            mouseover: highlightFeature,
                            mouseout: resetHighlight
                        });

                        layer.on('click', function (e) {
                            var kode_wilayah = feature.properties.kode_wilayah;

                            //window.location.replace("laman/detailprop/" + kode_wilayah);

                            map.fitBounds(e.target.getBounds());

                            // $('#map_legend').html( '<h2>'
                            //                             +feature.properties.name
                            //                             +'</h2>'
                            //                             +'PD: '+feature.properties.pd+'<br>'
                            //                             +'Guru: '+feature.properties.guru+'<br>'
                            //                             +'Pegawai: '+feature.properties.pegawai+'<br>'
                            //                             );

                            // this._div.innerHTML = '<center><h4 style=color:white>' + feature.properties.name + '</h4><hr></center>';

                            container.down('panel[itemId=map_legend]').update('<div id="map_legend" style="height:110px;width:650px;margin:10px;color:white">'
                                + '<b>'+feature.properties.name+'</b>'
                                +'<br>'
                                +'<div style=float:left;width:50%>'
                                    + '<b>Sekolah: </b>'+feature.properties.sekolah
                                    +'<br>'
                                    + '<b>Peserta Didik: </b>'+feature.properties.pd
                                    +'<br>'
                                    + '<b>Rombel: </b>'+feature.properties.rombel
                                    +'<br>'
                                +'</div>'
                                +'<div style=float:left;width:50%>'
                                    + '<b>Guru: </b>'+feature.properties.guru
                                    +'<br>'
                                    + '<b>Pegawai: </b>'+feature.properties.pegawai
                                    +'<br>'
                                +'</div>'
                                +'<div style=clear:both></div>'
                                +'</div>');
                        });
                    }

                    var popup = L.popup();

                    function onMapClick(e) {
                        var layer = e.target;

                        console.log(layer.properties.kode_wilayah);

                        //return "You clicked the map at propinsi ";

                    }

                    map.on('click', onMapClick);

                    // geojson = L.geoJson(statesData, {
                    //     style: style,
                    //     onEachFeature: onEachFeature
                    // }).addTo(map);

                    var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
                    
                    Ext.Ajax.request({
                        url: 'getGeoJsonBasic?kode_wilayah='+session.kode_wilayah+'&id_level_wilayah='+session.id_level_wilayah,
                        timeout: 24000000,
                        success: function(response){
                            var text = response.responseText;
                            
                            var objGeo = Ext.JSON.decode(response.responseText);
                        
                            // console.log(obj);
                    
                            geojson = L.geoJson({
                                "type":"FeatureCollection",
                                "features": objGeo
                            }, {
                                style: style,
                                onEachFeature: onEachFeature
                            }).addTo(map);
                        }
                    });

                }
            });


            // map.fitBounds();
        }
    }
});
