Ext.define('Admin.view.dashboard.DashboardController', {
    extend: 'Admin.view.main.ViewportController',
    alias: 'controller.dashboard',

    requires: [
        'Ext.util.TaskRunner'
    ],
    
    onRefreshToggle: function(tool, e, owner) {
        var me = this,
            store=this.getViewModel().getStore('dashboardfulllinechartstore'),
            items=Ext.Array.from(store && store.getData().items),
            num_items=items.length;

        if (tool.toggleValue){
            me.clearChartUpdates(owner);
        } else {
            if (num_items) {
                me.chartTaskRunner  = me.chartTaskRunner || Ext.create('Ext.util.TaskRunner');
                me.chartTaskRunner.start({
                    run : function () {
                        this.last_x += this.last_x - this.second_last_x;
                        var first = this.items[0].data;
                        this.store.removeAt(0);
                        this.store.add({xvalue: first.xvalue, y1value: first.y1value, y2value: first.y2value});
                        this.count++;
                    },
                    store : store,
                    count : 0,
                    items : items,
                    last_x : items[num_items -1].data.xvalue,
                    second_last_x : items[num_items -2].data.xvalue,
                    interval : 200
                });
            }
        }

        // change the toggle value
        tool.toggleValue = !tool.toggleValue;
    },

    clearChartUpdates : function() {
        this.chartTaskRunner = Ext.destroy(this.chartTaskRunner);
    },
    
    onDestroy: function () {
        this.clearChartUpdates();
        this.callParent();
    },
    
    onHideView: function () {
        this.clearChartUpdates();
    },

    RekapTotalGridSetup: function(grid){
        
            Ext.Ajax.request({
                url: 'session',
                success: function(response){
                    var text = response.responseText;
                    
                    var session = Ext.JSON.decode(response.responseText);
    
                    grid.up('panel').setTitle('Rekap Total Pendidikan di ' + session.kode_wilayah_str);

                    grid.getStore().on('load', function(store){
                        console.log(store.getCount());

                        if(store.getCount() == 1){
                            console.log(grid.up('panel'));

                            grid.up('panel').setHeight(200);
                        }

                        if(store.getCount() == 4){
                            grid.up('panel').setHeight(330);
                        }

                        if(store.getCount() == 5){
                            grid.up('panel').setHeight(360);

                        }

                        if(store.getCount() > 5){
                            grid.up('panel').setHeight(450);

                        }

                        var data = store.data;

                        var negeri = 0;
                        var pd_negeri = 0;
                        var swasta = 0;
                        var pd_swasta = 0;
                        var guru_negeri = 0;
                        var guru_swasta = 0;
                        var pegawai_negeri = 0;
                        var pegawai_swasta = 0;

                        data.each(function(record){

                            var dataRecord = record.data;

                            if(dataRecord.nama.toLowerCase() != 'total'){
                                negeri = negeri+dataRecord.negeri;
                                pd_negeri = pd_negeri+dataRecord.pd_negeri;
                                swasta = swasta+dataRecord.swasta;
                                pd_swasta = pd_swasta+dataRecord.pd_swasta;
                                guru_negeri = guru_negeri+dataRecord.guru_negeri;
                                guru_swasta = guru_swasta+dataRecord.guru_swasta;
                                pegawai_negeri = pegawai_negeri+dataRecord.pegawai_negeri;
                                pegawai_swasta = pegawai_swasta+dataRecord.pegawai_swasta;
                            }

                        });

                        var sekolahTotal = negeri+swasta;
                        var pdTotal = pd_negeri+pd_swasta;
                        var guruTotal = guru_negeri+guru_swasta;
                        var pegawaiTotal = pegawai_negeri+pegawai_swasta;

                        grid.up('dashboardnetworkpanel').up('container').down('panel[itemId=panelSekolahTotal]').down('panel[region=center]').update('Jumlah Sekolah<br>Total<div style="color:#ffffff;font-size:3vw;font-weight:bold;margin-top:10px;margin-bottom:10px">'+grid.formatMoney(sekolahTotal)+'</div>')
                        grid.up('dashboardnetworkpanel').up('container').down('panel[itemId=panelPesertaDidik]').down('panel[region=center]').update('Jumlah Peserta Didik<br>Total<div style="color:#ffffff;font-size:2vw;font-weight:bold;margin-top:10px;margin-bottom:10px">'+grid.formatMoney(pdTotal)+'</div>')
                        grid.up('dashboardnetworkpanel').up('container').down('panel[itemId=panelGuru]').down('panel[region=center]').update('Jumlah Guru<br>Total<div style="color:#ffffff;font-size:2.5vw;font-weight:bold;margin-top:10px;margin-bottom:10px">'+grid.formatMoney(guruTotal)+'</div>')
                        grid.up('dashboardnetworkpanel').up('container').down('panel[itemId=panelTendik]').down('panel[region=center]').update('Jumlah Tendik<br>Total<div style="color:#ffffff;font-size:2.5vw;font-weight:bold;margin-top:10px;margin-bottom:10px">'+grid.formatMoney(pegawaiTotal)+'</div>')

                    });

                    grid.getStore().on('beforeload', function(store){
                        Ext.apply(store.proxy.extraParams, {
                            id_level_wilayah: session.id_level_wilayah,
                            kode_wilayah: session.kode_wilayah
                        });                
                    });

                    grid.getStore().load();

                }
            });            


    },

    refreshGrid: function(tool, e, owner) {
        var grid = tool.down('gridpanel');

        grid.getStore().reload();
    }

});
