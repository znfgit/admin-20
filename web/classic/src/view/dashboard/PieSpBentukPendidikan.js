Ext.define('admin.view.dashboard.PieSpBentukPendidikan', {
    extend: 'Ext.Panel',
    alias: 'widget.piespbentukpendidikan',
    // width: 650,
    cls: 'dashboard-main-chart shadow-panel',
    title: 'Rekap SP - Bentuk Pendidikan',
    bodyPadding: 25,
    layout: 'responsivecolumn',
    defaults: {
        height:126,
        insetPadding: '7.5 7.5 7.5 7.5',
        background: 'rgba(255, 255, 255, 1)',
        colors: [
            '#6aa5dc',
            '#fdbf00',
            '#ee929d',
            '#90B962'
        ],
        interactions: [
            {
                type: 'rotate'
            }
        ]
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },

    initComponent: function() {
        var me = this;

        me.myDataStore = Ext.create('admin.store.SpBentukPendidikan');

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'top',
            items:[{
                text: 'Simpan',
                iconCls: 'fa fa-save',
                ui: 'soft-blue',
                listeners:{
                    click: function(btn){
                        Ext.MessageBox.confirm('Konfirmasi Unduh', 'Apakah Anda ingin mengunduh grafik ini sebagai gambar?', function (choice) {
                            if (choice == 'yes') {
                                var chart = btn.up('toolbar').up('panel').down('polar');

                                // console.log(chart);

                                if (Ext.os.is.Desktop) {
                                    chart.download({
                                        filename: 'Chart Bentuk Pendidikan'
                                    });
                                } else {
                                    chart.preview();
                                }
                              }
                        });
                    }
                }
            }]
        }];

        me.items = [{
            xtype: 'polar',
            // width: '100%',
            height: 380,
            responsiveCls: 'big-100 small-100',
            store: this.myDataStore,
            insetPadding: 25,
            innerPadding: 15,
            // background: '#343434',
            // legend: {
            //     docked: 'bottom'
            // },
            interactions: ['rotate', 'itemhighlight'],
            series: [{
                type: 'pie',
                angleField: 'jumlah', 
                donut: 20,
                label: {
                    field: 'desk',
                    renderer: function(text, sprite, config, rendererData, index){
                        // console.log(rendererData.store);

                        var storeItem = rendererData.store.findRecord('desk', text);
                        return storeItem.get('desk') + ' (' + me.formatMoney(storeItem.get('jumlah')) + ')';
                    }
                },
                highlight: true,
                tooltip: {
                    trackMouse: true,
                    renderer: function(storeItem, item) {

                        // this.setHtml(storeItem.get('desk') + ': ' + storeItem.get('jumlah'));
                    }
                }
            }]
        },{
            xtype: 'container',
            flex: 1,
            name:'containerLegenda',
            responsiveCls: 'big-100 small-100',
            height:100,
            scrollable:true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            }
            // items: [
            //     {
            //         xtype:'component',
            //         data: {
            //             name: 'Finance',
            //             value: '20%'
            //         },
            //         tpl: '<div class="left-aligned-div">{name}</div><div class="right-aligned-div">{value}</div>'
            //     },
            //     {
            //         xtype: 'progressbar',
            //         cls: 'bottom-indent service-finance',
            //         height: 4,
            //         minHeight: 4,
            //         value: 0.2
            //     },
            //     {
            //         xtype:'component',
            //         data: {
            //             name: 'Research',
            //             value: '68%'
            //         },
            //         tpl: '<div class="left-aligned-div">{name}</div><div class="right-aligned-div">{value}</div>'
            //     },
            //     {
            //         xtype: 'progressbar',
            //         cls: 'bottom-indent service-research',
            //         height: 4,
            //         minHeight: 4,
            //         value: 0.68
            //     },
            //     {
            //         xtype:'component',
            //         data: {
            //             name: 'Marketing',
            //             value: '12%'
            //         },
            //         tpl: '<div class="left-aligned-div">{name}</div><div class="right-aligned-div">{value}</div>'
            //     },
            //     {
            //         xtype: 'progressbar',
            //         cls: 'bottom-indent service-marketing',
            //         height: 4,
            //         value: 0.12
            //     }
            // ]
        }];

        // var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        Ext.Ajax.request({
                url: 'session',
                success: function(response){
                    var text = response.responseText;
                    
                    var session = Ext.JSON.decode(response.responseText);
    
                    me.myDataStore.load({
                        params:{
                            id_level_wilayah:session.id_level_wilayah,
                            kode_wilayah:session.kode_wilayah
                        }
                    });

            }
        });

        me.myDataStore.on('load', function(store){

            // var warna = ['biru',
            //             'kuning',
            //             'merah',
            //             'hijau'];
            
            var warna = ['research',
                        'finance',
                        'marketing',
                        'hijau'];


            if(store.getCount() == 6){
                me.setHeight(450);
            }

            if(store.getCount() >= 8){
                me.setHeight(550);
            }



            var data = store.data;
            var total = 0;
            var i = 0;
            data.each(function(record){
                // console.log(record.data.desk);
                total += record.data.jumlah;

            });

            console.log(total);

            data.each(function(record){
                
                var cls_prog = 'bottom-indent service-'+warna[i];
                
                var persen = record.data.jumlah / total * 100;
                var values = record.data.jumlah / total;

                me.down('container[name=containerLegenda]').add(
                    {
                        xtype:'component',
                        data: {
                            name: record.data.desk,
                            value: persen.toFixed(2)
                        },
                        tpl: '<div class="left-aligned-div">'+record.data.desk+'</div><div class="right-aligned-div"><b>'+me.formatMoney(record.data.jumlah)+'</b> ('+persen.toFixed(2)+'%)</div>'
                    },
                    {
                        xtype: 'progressbar',
                        cls: cls_prog,
                        height: 4,
                        minHeight: 4,
                        value: values
                    }
                );
                
                console.log(cls_prog);

                i++;
            });

        });

        this.callParent();
    }
});