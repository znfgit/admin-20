Ext.define('Admin.view.dashboard.RekapTotalGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.rekaptotalgrid',
    title: '',
    selType: 'rowmodel',
    listeners:{
        afterrender:'RekapTotalGridSetup',
        itemdblclick: function(dv, record, item, index, e) {
            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }
        }
    },
    createStore: function() {
        return Ext.create('Admin.store.RekapTotal');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-file-excel-o',
                paramClass: 'SatuanPendidikan',
                paramMethod: 'RekapTotal',
                listeners:{
                    click: 'tombolUnduhExcel'
                }
            },'->',{
                xtype: 'button',
                // text: 'Unduh Xls',
                ui: 'soft-green',
                iconCls: 'x-fa fa-refresh',
                listeners:{
                    click: function(btn){
                        var grid = btn.up('gridpanel');

                        grid.getStore().reload();
                    }
                }
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        Ext.apply(this, this.initialConfig);

        
        this.columns = [{
            header: ' ',
            width: 90,
            // flex: 1,
            sortable: true,
            dataIndex: 'nama',
			hideable: false,
            locked: true,
            renderer: function(v,p,r){
                if(v == 'Total'){
                    return "<b>"+v+"</b>";
                }else{
                    return v;
                }
            }
        }
        ,{
            text: 'Satuan Pendidikan',
            columns:[
                {
                    header: 'N',
                    align:'right',
                    dataIndex: 'negeri',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            return "<b>"+grid.formatMoney(v)+"</b>";
                        }else{
                            return grid.formatMoney(v);
                        }

                        // return r.data.bentuk_pendidikan_id +; 
                    }
                },{
                    header: 'S',
                    align:'right',
                    dataIndex: 'swasta',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            return "<b>"+grid.formatMoney(v)+"</b>";
                        }else{
                            return grid.formatMoney(v);
                        }
                    }
                },{
                    header: 'T',
                    align:'right',
                    dataIndex: 'swasta',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            // return "<b>"+grid.formatMoney(v)+"</b>";
                            return "<b>"+grid.formatMoney(r.data.negeri + r.data.swasta)+"</b>";
                        }else{
                            return grid.formatMoney(r.data.negeri + r.data.swasta);
                        }
                    }
                }
                // ,{
                //     header: 'T',
                //     align:'right',
                //     dataIndex: 'swasta',
                //     width: 70,
                //     sortable: true,
                //     hideable: false,
                //     renderer: function(v,p,r){
                //         return grid.formatMoney(r.data.guru_negeri + r.data.guru_swasta);
                //     }
                // }
            ]
        },{
            text: 'Guru',
            columns:[
                {
                    header: 'N',
                    align:'right',
                    dataIndex: 'guru_negeri',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            return "<b>"+grid.formatMoney(v)+"</b>";
                        }else{
                            return grid.formatMoney(v);
                        }
                    }
                },{
                    header: 'S',
                    align:'right',
                    dataIndex: 'guru_swasta',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            return "<b>"+grid.formatMoney(v)+"</b>";
                        }else{
                            return grid.formatMoney(v);
                        }
                    }
                },{
                    header: 'T',
                    align:'right',
                    dataIndex: 'swasta',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            // return "<b>"+grid.formatMoney(v)+"</b>";
                            return "<b>"+grid.formatMoney(r.data.guru_negeri + r.data.guru_swasta)+"</b>";
                        }else{
                            return grid.formatMoney(r.data.guru_negeri + r.data.guru_swasta);
                        }
                        // return grid.formatMoney(r.data.guru_negeri + r.data.guru_swasta);
                    }
                }
                // ,{
                //     header: 'T',
                //     align:'right',
                //     dataIndex: 'swasta',
                //     width: 70,
                //     sortable: true,
                //     hideable: false,
                //     renderer: function(v,p,r){
                //         return grid.formatMoney(r.data.guru_negeri + r.data.guru_swasta);
                //     }
                // }
            ]
        },{
            text: 'Tendik',
            columns:[
                {
                    header: 'N',
                    align:'right',
                    dataIndex: 'pegawai_negeri',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            return "<b>"+grid.formatMoney(v)+"</b>";
                        }else{
                            return grid.formatMoney(v);
                        }
                    }
                },{
                    header: 'S',
                    align:'right',
                    dataIndex: 'pegawai_swasta',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            return "<b>"+grid.formatMoney(v)+"</b>";
                        }else{
                            return grid.formatMoney(v);
                        }
                    }
                },{
                    header: 'T',
                    align:'right',
                    dataIndex: 'swasta',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            // return "<b>"+grid.formatMoney(v)+"</b>";
                            return "<b>"+grid.formatMoney(r.data.pegawai_negeri + r.data.pegawai_swasta)+"</b>";
                        }else{
                            return grid.formatMoney(r.data.pegawai_negeri + r.data.pegawai_swasta);
                        }
                        // return grid.formatMoney(r.data.pegawai_negeri + r.data.pegawai_swasta);
                    }
                }
                // ,{
                //     header: 'T',
                //     align:'right',
                //     dataIndex: 'swasta',
                //     width: 70,
                //     sortable: true,
                //     hideable: false,
                //     renderer: function(v,p,r){
                //         return grid.formatMoney(r.data.pegawai_negeri + r.data.pegawai_swasta);
                //     }
                // }
            ]
        },{
            text: 'Peserta Didik',
            columns:[
                {
                    header: 'N',
                    align:'right',
                    dataIndex: 'pd_negeri',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            return "<b>"+grid.formatMoney(v)+"</b>";
                        }else{
                            return grid.formatMoney(v);
                        }
                    }
                },{
                    header: 'S',
                    align:'right',
                    dataIndex: 'pd_swasta',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            return "<b>"+grid.formatMoney(v)+"</b>";
                        }else{
                            return grid.formatMoney(v);
                        }
                    }
                },{
                    header: 'T',
                    align:'right',
                    dataIndex: 'swasta',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            return "<b>"+grid.formatMoney(r.data.pd_negeri + r.data.pd_swasta)+"</b>";
                        }else{
                            return grid.formatMoney(r.data.pd_negeri + r.data.pd_swasta);
                        }
                        // return grid.formatMoney(r.data.pd_negeri + r.data.pd_swasta);
                    }
                }
                // ,{
                //     header: 'T',
                //     align:'right',
                //     dataIndex: 'swasta',
                //     width: 70,
                //     sortable: true,
                //     hideable: false,
                //     renderer: function(v,p,r){
                //         return grid.formatMoney(r.data.pd_negeri + r.data.pd_swasta);
                //     }
                // }
            ]
        },{
            text: 'Rombel',
            columns:[
                {
                    header: 'N',
                    align:'right',
                    dataIndex: 'rombel_negeri',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            return "<b>"+grid.formatMoney(v)+"</b>";
                        }else{
                            return grid.formatMoney(v);
                        }
                    }
                },{
                    header: 'S',
                    align:'right',
                    dataIndex: 'rombel_swasta',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            return "<b>"+grid.formatMoney(v)+"</b>";
                        }else{
                            return grid.formatMoney(v);
                        }
                    }
                },{
                    header: 'T',
                    align:'right',
                    dataIndex: 'swasta',
                    width: 70,
                    sortable: true,
                    hideable: false,
                    renderer: function(v,p,r){
                        if(r.data.nama == 'Total'){
                            return "<b>"+grid.formatMoney(r.data.rombel_negeri + r.data.rombel_swasta)+"</b>";
                        }else{
                            return grid.formatMoney(r.data.rombel_negeri + r.data.rombel_swasta);
                        }
                        // return grid.formatMoney(r.data.rombel_negeri + r.data.rombel_swasta);
                    }
                }
                // ,{
                //     header: 'T',
                //     align:'right',
                //     dataIndex: 'swasta',
                //     width: 70,
                //     sortable: true,
                //     hideable: false,
                //     renderer: function(v,p,r){
                //         return grid.formatMoney(r.data.rombel_negeri + r.data.rombel_swasta);
                //     }
                // }
            ]
        }
        ];
        
        this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();
        
        this.callParent(arguments);

        // this.store.load();
    }
});