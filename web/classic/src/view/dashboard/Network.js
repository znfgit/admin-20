Ext.define('Admin.view.dashboard.Network', {
    extend: 'Ext.panel.Panel',
    
    requires: [
        'Ext.chart.CartesianChart',
        'Ext.chart.axis.Category',
        'Ext.chart.axis.Numeric',
        'Ext.chart.series.Line',
        'Ext.ProgressBar'
    ],

    xtype: 'dashboardnetworkpanel',
    cls: 'dashboard-main-chart shadow-panel',
    // height: 300,

    // bodyPadding: 15,

    title: 'Rekap Total Pendidikan',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    // tools: [
    //     {
    //         xtype: 'tool',
    //         toggleValue: false,
    //         cls: 'x-fa fa-refresh dashboard-tools',
    //         listeners: {
    //             click: 'onRefreshToggle'
    //         },
    //         width: 20,
    //         height: 20
    //     }
    // ],

    items: [
        {
            xtype: 'container',
            flex: 1,
            layout: 'fit',
            // html: 'testing'
            items: [{
                xtype: 'rekaptotalgrid'
            }]   
        }
    ]
});
