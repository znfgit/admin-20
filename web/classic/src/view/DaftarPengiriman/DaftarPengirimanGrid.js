Ext.define('Admin.view.DaftarPengiriman.DaftarPengirimanGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.daftarpengirimangrid',
    title: '',
    selType: 'rowmodel',
    createStore: function() {
        return Ext.create('Admin.store.DaftarPengiriman');
    },
    createDockedItems: function() {
        return [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Tambah',
                // iconCls: 'fa fa-plus fa-lg glyph-dark-green glyph-shadow',
                iconCls: 'ic-add',
                scope: this,
                action: 'add'
            }, {
                xtype: 'button',
                text: 'Ubah',
                // iconCls: 'fa fa-eraser fa-lg glyph-dark-orange glyph-shadow',
                iconCls: 'ic-pencil',
                itemId: 'edit',
                scope: this,
                action: 'edit'
            }, {
                xtype: 'button',
                text: 'Simpan',
                // iconCls: 'fa fa-check fa-lg glyph-blue glyph-shadow',
                iconCls: 'ic-disk',
                itemId: 'save',
                scope: this,
                action: 'save'
            }, {
                xtype: 'button',
                text: 'Hapus',
                // iconCls: 'fa fa-remove fa-lg glyph-red glyph-shadow',
                iconCls: 'ic-cross',
                itemId: 'delete',
                scope: this,
                action: 'delete'
            }]
        }];
    },
    createBbar: function(){
        return Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = this.createStore();
        
        Ext.apply(this, this.initialConfig);

        
        this.columns = [{
            header: 'Wilayah',
            tooltip: 'nama wilayah',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
			hideable: false
        }
        ,{
            text: 'TK',
            columns:[{
                header: 'Jumlah',
                align:'right',
                tooltip: 'Jumlah TK',
                width: 90,
                sortable: true,
                dataIndex: 'tk',
                hideable: false
            },{
                header: 'Kirim',
                align:'right',
                tooltip: 'Kirim TK',
                width: 90,
                sortable: true,
                dataIndex: 'kirim_tk',
                hideable: false
            },{
                header: 'Sisa',
                align:'right',
                tooltip: 'Sisa TK',
                width: 90,
                sortable: true,
                dataIndex: 'sisa_tk',
                hideable: false
            }]
        }
        ,{
            text: 'KB',
            columns:[{
                header: 'Jumlah',
                align:'right',
                tooltip: 'Jumlah KB',
                width: 90,
                sortable: true,
                dataIndex: 'kb',
                hideable: false
            },{
                header: 'Kirim',
                align:'right',
                tooltip: 'Kirim KB',
                width: 90,
                sortable: true,
                dataIndex: 'kirim_kb',
                hideable: false
            },{
                header: 'Sisa',
                align:'right',
                tooltip: 'Sisa KB',
                width: 90,
                sortable: true,
                dataIndex: 'sisa_kb',
                hideable: false
            }]
        }
        ,{
            text: 'TPA',
            columns:[{
                header: 'Jumlah',
                align:'right',
                tooltip: 'Jumlah TPA',
                width: 90,
                sortable: true,
                dataIndex: 'tpa',
                hideable: false
            },{
                header: 'Kirim',
                align:'right',
                tooltip: 'Kirim TPA',
                width: 90,
                sortable: true,
                dataIndex: 'kirim_tpa',
                hideable: false
            },{
                header: 'Sisa',
                align:'right',
                tooltip: 'Sisa TPA',
                width: 90,
                sortable: true,
                dataIndex: 'sisa_tpa',
                hideable: false
            }]
        }
        ,{
            text: 'SPS',
            columns:[{
                header: 'Jumlah',
                align:'right',
                tooltip: 'Jumlah SPS',
                width: 90,
                sortable: true,
                dataIndex: 'sps',
                hideable: false
            },{
                header: 'Kirim',
                align:'right',
                tooltip: 'Kirim SPS',
                width: 90,
                sortable: true,
                dataIndex: 'kirim_sps',
                hideable: false
            },{
                header: 'Sisa',
                align:'right',
                tooltip: 'Sisa SPS',
                width: 90,
                sortable: true,
                dataIndex: 'sisa_sps',
                hideable: false
            }]
        }
        // ,{
        //     text: 'SMA',
        //     columns:[{
        //         header: 'Jumlah',
        //         align:'right',
        //         tooltip: 'Jumlah SMA',
        //         width: 90,
        //         sortable: true,
        //         dataIndex: 'jumlah_sma',
        //         hideable: false
        //     },{
        //         header: 'Kirim',
        //         align:'right',
        //         tooltip: 'Kirim SMA',
        //         width: 90,
        //         sortable: true,
        //         dataIndex: 'kirim_sma',
        //         hideable: false
        //     },{
        //         header: 'Sisa',
        //         align:'right',
        //         tooltip: 'Sisa SMA',
        //         width: 90,
        //         sortable: true,
        //         dataIndex: 'sisa_sma',
        //         hideable: false
        //     }]
        // }
        // ,{
        //     text: 'SMK',
        //     columns:[{
        //         header: 'Jumlah',
        //         align:'right',
        //         tooltip: 'Jumlah SMK',
        //         width: 90,
        //         sortable: true,
        //         dataIndex: 'jumlah_smk',
        //         hideable: false
        //     },{
        //         header: 'Kirim',
        //         align:'right',
        //         tooltip: 'Kirim SMK',
        //         width: 90,
        //         sortable: true,
        //         dataIndex: 'kirim_smk',
        //         hideable: false
        //     },{
        //         header: 'Sisa',
        //         align:'right',
        //         tooltip: 'Sisa SMK',
        //         width: 90,
        //         sortable: true,
        //         dataIndex: 'sisa_smk',
        //         hideable: false
        //     }]
        // }
        ,{
            text: 'Total',
            columns:[{
                header: 'Jumlah',
                align:'right',
                tooltip: 'Jumlah Total',
                width: 90,
                sortable: true,
                dataIndex: 'jumlah_total',
                hideable: false,
                renderer: function(v,p,r){
                    return r.data.tk + r.data.kb + r.data.tpa + r.data.sps;
                }
            },{
                header: 'Kirim',
                align:'right',
                tooltip: 'Kirim total',
                width: 90,
                sortable: true,
                dataIndex: 'kirim_total',
                hideable: false,
                renderer: function(v,p,r){
                    return r.data.kirim_tk + r.data.kirim_kb + r.data.kirim_tpa + r.data.kirim_sps;
                }
            },{
                header: 'Sisa',
                align:'right',
                tooltip: 'Sisa total',
                width: 90,
                sortable: true,
                dataIndex: 'sisa_total',
                hideable: false,
                renderer: function(v,p,r){
                    return r.data.sisa_tk + r.data.sisa_kb + r.data.sisa_tpa + r.data.sisa_sps;
                }
            }]
        }
        ,{
            header: 'Persen',
            align:'right',
            tooltip: 'persentase',
            width: 100,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            renderer: function(v,p,r){
                return ((r.data.kirim_tk + r.data.kirim_kb + r.data.kirim_tpa + r.data.kirim_sps) / (r.data.tk + r.data.kb + r.data.tpa + r.data.sps) * 100).toFixed(2);
            }
        }
        ];
        
        // this.dockedItems = this.createDockedItems();

        // this.bbar = this.createBbar();
        
        this.callParent(arguments);

        this.store.load();
    }
});