Ext.define('Admin.view.DaftarPengiriman.DaftarPengiriman', {
    extend: 'Ext.container.Container',
    xtype: 'daftarpengiriman',

    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],

    controller: 'daftarpengiriman',
    viewModel: {
        type: 'DaftarPengiriman'
    },
    cls: 'daftarpengiriman-container',

    layout: 'responsivecolumn',
    // layout: 'border',

    items: [
        {
            xtype: 'daftarpengirimangrid',
            title: 'Daftar Pengiriman',
            // Always 100% of container
            responsiveCls: 'big-100',
            minHeight:200
            // region: 'center'
            // html: 'isinya daftar pengiriman'
        }
    ]
});
