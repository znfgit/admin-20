Ext.define('Admin.view.main.Viewport', {
    extend: 'Ext.container.Viewport',
    xtype: 'mainviewport',

    requires: [
        'Ext.list.Tree',
        // 'GeoExt.panel.Map',
        // 'GeoExt.Action'
    ],

    controller: 'mainviewport',
    viewModel: {
        type: 'mainviewport'
    },

    cls: 'sencha-dash-viewport',
    itemId: 'mainView',

    layout: {
        type: 'border'
    },

    listeners: {
        render: 'onMainViewRender'
    },

    id: 'viewport',

    items: [
        {
            region: 'west',
            border: false,
            id: 'menuUtama',
            layout: 'border',
            width: 270,
            layout: {
                type: 'border'
            },
            border: false,
            items: [{
                xtype: 'panel',
                region: 'north',
                id: 'headerLogo',
                height:50,
                bodyStyle: 'padding:15px;text-align:left;font-size:20px;color:white;background:#0795b5',
                html: '<b>Manajemen</b>Dapodik'
            },{
                xtype: 'panel',
                region: 'center',
                // bodyStyle: 'background:#32404E',
                bodyStyle: 'background:#37607D',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                border: false,
                scrollable: 'y',
                items: [{
                    xtype: 'treelist',
                    margin: '10 0 0 0',
                    reference: 'navigationTreeList',
                    store: 'NavigationTree',
                    expanderFirst: false,
                    expanderOnly: false,
                    ui: 'navigation',
                    reference: 'navigationTreeList',
                    itemId: 'navigationTreeList',
                    id: 'navigationTreeList',
                    listeners: {
                        selectionchange: 'onNavigationTreeSelectionChange'
                    }
                }]
            }],   
        },
        {
            region: 'center',
            border: false, 
            layout:{
                type: 'border'
            },
            items:[
                {
                    xtype: 'panel',
                    region: 'north',
                    id: 'headerPanel',
                    height: 50,
                    bodyStyle: 'background:#00C0EF',
                    layout: {
                        type: 'hbox',
                        align: 'left',
                    },
                    items:[
                        {
                            xtype: 'button',
                            iconCls : 'fa fa-bars fa-lg',
                            // text: '<<',
                            id: 'headerButton',
                            ui: 'green',
                            bodyStyle: 'background:#F39C12',
                            margin: 10,
                            handler: function(btn){
                                var panel = btn.up('panel').up('panel').up('mainviewport').down('panel[region=west]');

                                panel.toggleCollapse(true);

                                // if(panel.collapsed == false){
                                //     btn.setText('<<');
                                // }else{
                                //     btn.setText('>>');
                                // }
                            }
                        }
                    ]

                },{
                    xtype: 'container',
                    region: 'center',
                    reference: 'mainCardPanel',
                    cls: 'sencha-dash-right-main-container',
                    itemId: 'contentPanel',
                    id: 'contentPanel',
                    layout: {
                        type: 'card',
                        anchor: '100%'
                    }
                }
            ]
        },
        // {
        //     id: 'app-header',
        //     xtype: 'box',
        //     region: 'north',
        //     height: 50,
        //     html: '<div style="background: linear-gradient(to right, #666666 , #2d843b);height:100%">' 
        //             + '<div style="float:left;margin-top:7px;margin-left:10px">'
        //                 +'<img src="resources/images/logo_dikbud.png" width="35px"/>'
        //             +'</div>'
        //             +'<div style="color:white;float:left;margin:15px;font-size:20px;font-weight:none" class="ubuntu">'
        //                 // + 'Profil Dikdas Belitung'
        //                 + 'Sistem Informasi Manajemen Pendidikan'
        //             +'</div>'
        //             +'<div style="clear:both"></div>'
        //         +'</div>'
        // }
    ]

    // items: [
    //     {
    //         xtype: 'toolbar',
    //         cls: 'sencha-dash-dash-headerbar toolbar-btn-shadow',
    //         height: 64,
    //         itemId: 'headerBar',
    //         // cls: 'header-bar-green',
    //         listeners:{
    //             afterrender:'toolbarAfterRender'
    //         },
    //         items: [
    //             {
    //                 xtype: 'component',
    //                 reference: 'senchaLogo',
    //                 cls: 'sencha-logo-green',
    //                 html: '<div class="main-logo"><img src="resources/images/logo.png" width="30px">Profil Pendidikan</div>',
    //                 width: 280
    //             },
    //             {
    //                 margin: '0 0 0 8',
    //                 cls: 'delete-focus-bg',
    //                 iconCls:'x-fa fa-navicon',
    //                 id: 'main-navigation-btn',
    //                 handler: 'onToggleNavigationSize'
    //             },
    //             {
    //                 xtype: 'tbspacer',
    //                 flex: 1
    //             }
    //             // {
    //             //     xtype: 'tbtext',
    //             //     text: 'Mr. Khalid Saifuddin',
    //             //     cls: 'top-user-name'
    //             // },
    //             // {
    //             //     xtype: 'image',
    //             //     cls: 'header-right-profile-image',
    //             //     height: 35,
    //             //     width: 35,
    //             //     alt:'current user image',
    //             //     src: 'resources/images/user-profile/2.png'
    //             // },
                
    //         ]
    //     },
    //     {
    //         xtype: 'maincontainerwrap',
    //         id: 'main-view-detail-wrap',
    //         reference: 'mainContainerWrap',
    //         flex: 1,
    //         items: [
    //             {
    //                 xtype: 'treelist',
    //                 reference: 'navigationTreeList',
    //                 itemId: 'navigationTreeList',
    //                 id: 'navigationTreeList',
    //                 ui: 'navigation',
    //                 store: 'NavigationTree',
    //                 width: 280,
    //                 // height:500,
    //                 expanderFirst: false,
    //                 expanderOnly: false,
    //                 listeners: {
    //                     selectionchange: 'onNavigationTreeSelectionChange'
    //                 }
    //             },
    //             {
    //                 xtype: 'container',
    //                 flex: 1,
    //                 reference: 'mainCardPanel',
    //                 cls: 'sencha-dash-right-main-container',
    //                 itemId: 'contentPanel',
    //                 layout: {
    //                     type: 'card',
    //                     anchor: '100%'
    //                 }
    //             }
    //         ]
    //     }
    // ]
});
