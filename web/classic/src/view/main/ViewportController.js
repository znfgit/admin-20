Ext.define('Admin.view.main.ViewportController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.mainviewport',

    listen : {
        controller : {
            '#' : {
                unmatchedroute : 'onRouteChange'
            }
        }
    },

    routes: {
        ':node': 'onRouteChange',
        'ProfilSatuanPendidikan/:sekolah_id' : 'ProfilSatuanPendidikanRoute',
        'ProgresPengiriman/:id_level_wilayah/:kode_wilayah' : 'ProgresPengirimanRoute',
        'KualitasData/:id_level_wilayah/:kode_wilayah' : 'KualitasDataRoute',
        'KualitasDataSp/:id_level_wilayah/:kode_wilayah' : 'KualitasDataSpRoute',
        'KualitasDataIdentitas/:id_level_wilayah/:kode_wilayah' : 'KualitasDataIdentitasRoute',
        'KualitasDataIdentitasSp/:id_level_wilayah/:kode_wilayah' : 'KualitasDataIdentitasSpRoute',
        'ProfilPtk/:ptk_id' : 'ProfilPtkRoute',
        'SpWaktuPenyelenggaraanSp/:id_level_wilayah/:kode_wilayah' : 'SpWaktuPenyelenggaraanSpRoute',
        'SpRingkasanSp/:id_level_wilayah/:kode_wilayah' : 'SpRingkasanSpRoute',
        'SarprasTingkatKerusakanSp/:id_level_wilayah/:kode_wilayah' : 'SarprasTingkatKerusakanSpRoute',
        'SarprasRkbSp/:id_level_wilayah/:kode_wilayah' : 'SarprasRkbSpRoute',
        'PdTingkatSp/:id_level_wilayah/:kode_wilayah' : 'PdTingkatSpRoute',
        'PdAngkaKelulusanSp/:id_level_wilayah/:kode_wilayah' : 'PdAngkaKelulusanSpRoute',
        'PdAngkaPutusSekolahSp/:id_level_wilayah/:kode_wilayah' : 'PdAngkaPutusSekolahSpRoute',
        'PdAngkaMengulangSp/:id_level_wilayah/:kode_wilayah' : 'PdAngkaMengulangSpRoute',
        'PdRingkasanSp/:id_level_wilayah/:kode_wilayah' : 'PdRingkasanSpRoute',
        'PdAgamaSp/:id_level_wilayah/:kode_wilayah' : 'PdAgamaSpRoute',
        'PdUsiaSp/:id_level_wilayah/:kode_wilayah' : 'PdUsiaSpRoute',
        'PtkRingkasanSp/:id_level_wilayah/:kode_wilayah' : 'PtkRingkasanSpRoute',
        'PtkAgamaSp/:id_level_wilayah/:kode_wilayah' : 'PtkAgamaSpRoute',
        'PtkStatusPegawaiSp/:id_level_wilayah/:kode_wilayah' : 'PtkStatusPegawaiSpRoute',
        'PtkKualifikasiSp/:id_level_wilayah/:kode_wilayah' : 'PtkKualifikasiSpRoute',
        'ProfilSpRombelDetail/:rombongan_belajar_id' : 'ProfilSpRombelDetailRoute',
        'RingkasanRombonganBelajarSp/:id_level_wilayah/:kode_wilayah' : 'RingkasanRombonganBelajarSpRoute',
        'PengirimanSp/:id_level_wilayah/:kode_wilayah' : 'PengirimanSpRoute',
        'PencapaianKualitasBulanan/:id_level_wilayah/:kode_wilayah' : 'PencapaianKualitasBulananRoute',
        'PencapaianKualitasBulananSp/:id_level_wilayah/:kode_wilayah' : 'PencapaianKualitasBulananSpRoute',
        'SpKurikulumSp/:id_level_wilayah/:kode_wilayah' : 'SpKurikulumSpRoute',
        // 'peta/:id_level_wilayah/:kode_wilayah' : 'petaRoute'
    },

    kembali: function(btn){

        // Ext.MessageBox.alert('kembali');return true;

        var grid = btn.up('gridpanel');
        var store = grid.getStore();
        var rec = store.getAt(0);

        var hash = window.location.hash.substr(1);
        var hashArr = hash.split('/');

        if(typeof hashArr[1] === 'undefined' || typeof hashArr[2] === 'undefined'){

            var kode_wilayah = '000000';
            var id_level_wilayah = 0;
            
        }else{
    
            if(hashArr[1] == 2){
                var kode_wilayah = hashArr[2].substr(0,2)+"0000";
                var id_level_wilayah = hashArr[1]-1;
            }else if(hashArr[1] == 1){
                var kode_wilayah = '000000';
                var id_level_wilayah = hashArr[1]-1;
            }

    //         grid.getStore().reload({
    //             params:{
    //                 id_level_wilayah: (rec.data.id_level_wilayah - 2),
    //                 kode_wilayah : induk
    //             }
    //         })
        
        }
        
        window.location="#"+hashArr[0]+"/"+id_level_wilayah+"/"+kode_wilayah;
    },

    ProgresPengirimanRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.progres.Pengiriman', {
                    hideMode: 'offsets',
                    routeId: 'ProgresPengiriman/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });
        
        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }
        
        newView.getEl().scrollTo('Top', 0, true);
    },

    KualitasDataRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.progres.KualitasDataSekolah', {
                    hideMode: 'offsets',
                    routeId: 'KualitasData/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });
        
        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }
        
        newView.getEl().scrollTo('Top', 0, true);
    },

    KualitasDataSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.progres.KualitasDataSekolahSp', {
                    hideMode: 'offsets',
                    routeId: 'KualitasDataSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });
        
        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }
        
        newView.getEl().scrollTo('Top', 0, true);
    },
    
    KualitasDataIdentitasRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.progres.KualitasDataIdentitas', {
                    hideMode: 'offsets',
                    routeId: 'KualitasDataIdentitas/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });
        
        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }
        
        newView.getEl().scrollTo('Top', 0, true);
    },

    KualitasDataIdentitasSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.progres.KualitasDataIdentitasSp', {
                    hideMode: 'offsets',
                    routeId: 'KualitasDataIdentitasSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });
        
        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }
        
        newView.getEl().scrollTo('Top', 0, true);
    },

    init:function(){
        // Ext.MessageBox.alert('Status', 'Berhasil!');

        Ext.Ajax.request({
            url: 'session',
            timeout: 300000000,
            success: function(response){
                var text = response.responseText;

                var res = Ext.JSON.decode(response.responseText);

                if(typeof(Storage) !== "undefined") {
                    localStorage.setItem("local.dapodik.session", text);
                }else{
                    Ext.MessageBox.alert('Peringatan', 'Browser Anda sudah terlalu ketinggalan jaman! Ganti dengan browser yang leboh modern untuk membuka aplikasi ini!');
                }

                // console.log(this);
                var hideMask = function () {
                    // Ext.get('loading').remove();
                    Ext.fly('loading-mask').animate({
                        opacity:0,
                        remove:true
                    });
                };

                Ext.defer(hideMask, 100);

                var headerLogo = Ext.getCmp('headerLogo');
                var headerPanel = Ext.getCmp('headerPanel');
                var headerButton = Ext.getCmp('headerButton');
                var menuUtama = Ext.getCmp('menuUtama');

                headerLogo.setBodyStyle('padding:15px;text-align:left;font-size:20px;color:white;background:'+res.warna_header_logo);
                headerPanel.setBodyStyle('background:'+res.warna_header_toolbar);
                headerButton.setUI(res.warna_tema);
                menuUtama.setUI(res.warna_tema);

                if(res.background_header_toolbar){
                    headerPanel.setBodyStyle('background: '+res.warna_header_toolbar+' url(resources/images/'+res.background_header_toolbar+') right no-repeat;background-size:contain');
                    // headerLogo.setBodyStyle('padding:15px;text-align:left;font-size:20px;color:white;background: '+res.warna_header_logo+' url(resources/images/'+res.background_header_toolbar+') right no-repeat;');
                }

                // console.log(localStorage.getItem("local.dapodik.session"));

            }
        });
    },

    setCurrentView: function(hashTag) {
        hashTag = (hashTag || '').toLowerCase();

        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            navigationList = refs.navigationTreeList,
            viewModel = me.getViewModel(),
            vmData = viewModel.getData(),
            store = navigationList.getStore(),
            node = store.findNode('routeId', hashTag),
            view = node ? node.get('view') : null,
            lastView = vmData.currentView,
            existingItem = mainCard.child('component[routeId=' + hashTag + ']'),
            newView;

        // console.log(viewModel);
        // console.log(mainLayout);

        // Kill any previously routed window
        if (lastView && lastView.isWindow) {
            lastView.destroy();

        }

        lastView = null;

        lastView = mainLayout.getActiveItem();

        if (!existingItem) {
            newView = Ext.create('Admin.view.' + (view || 'pages.Error404Window'), {
                    hideMode: 'offsets',
                    routeId: hashTag
                });
        }

        if (!newView || !newView.isWindow) {
            // !newView means we have an existing view, but if the newView isWindow
            // we don't add it to the card layout.
            if (existingItem) {
                // We don't have a newView, so activate the existing view.
                if (existingItem !== lastView) {
                    mainLayout.setActiveItem(existingItem);
                }
                newView = existingItem;
            }
            else {
                // newView is set (did not exist already), so add it and make it the
                // activeItem.
                Ext.suspendLayouts();
                mainLayout.setActiveItem(mainCard.add(newView));
                Ext.resumeLayouts(true);
            }
        }

        navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        vmData.currentView = newView;

        newView.getEl().scrollTo('Top', 0, true);
    },

    ProfilSatuanPendidikanRoute: function(sekolah_id){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.SatuanPendidikan.ProfilSatuanPendidikan', {
                    hideMode: 'offsets',
                    routeId: 'ProfilSatuanPendidikan/'+sekolah_id,
                    sekolah_id: sekolah_id
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    ProfilPtkRoute: function(ptk_id){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.Ptk.ProfilPtk', {
                    hideMode: 'offsets',
                    routeId: 'ProfilPtk/'+ptk_id,
                    ptk_id: ptk_id
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    SpWaktuPenyelenggaraanSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        var kdwilayah = kode_wilayah.substring(0,6);

        newView = Ext.create('Admin.view.SatuanPendidikan.SpWaktuPenyelenggaraanSp', {
                    hideMode: 'offsets',
                    routeId: 'SpWaktuPenyelenggaraanSp/'+id_level_wilayah+'/'+kdwilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kdwilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    SpRingkasanSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        var kdwilayah = kode_wilayah.substring(0,6);
        // console.log(kdwilayah);

        newView = Ext.create('Admin.view.SatuanPendidikan.SpRingkasanDetail', {
            hideMode: 'offsets',
            routeId: 'SpRingkasanSp/'+id_level_wilayah+'/'+kdwilayah,
            id_level_wilayah: id_level_wilayah,
            kode_wilayah: kdwilayah
        });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    SpKurikulumSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        var kdwilayah = kode_wilayah.substring(0,6);
        // console.log(kdwilayah);

        newView = Ext.create('Admin.view.SatuanPendidikan.SpKurikulumSp', {
            hideMode: 'offsets',
            routeId: 'SpKurikulumSp/'+id_level_wilayah+'/'+kdwilayah,
            id_level_wilayah: id_level_wilayah,
            kode_wilayah: kdwilayah
        });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PdTingkatSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.PesertaDidik.PdTingkatSp', {
                    hideMode: 'offsets',
                    routeId: 'PdTingkatSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PdAngkaKelulusanSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.PesertaDidik.PdAngkaKelulusanSp', {
                    hideMode: 'offsets',
                    routeId: 'PdAngkaKelulusanSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PdAngkaPutusSekolahSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.PesertaDidik.PdAngkaPutusSekolahSp', {
                    hideMode: 'offsets',
                    routeId: 'PdAngkaPutusSekolahSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PdAngkaMengulangSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.PesertaDidik.PdAngkaMengulangSp', {
                    hideMode: 'offsets',
                    routeId: 'PdAngkaMengulangSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PdRingkasanSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.PesertaDidik.PdRingkasanSp', {
                    hideMode: 'offsets',
                    routeId: 'PdRingkasanSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PdAgamaSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.PesertaDidik.PdAgamaSp', {
                    hideMode: 'offsets',
                    routeId: 'PdAgamaSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PdUsiaSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.PesertaDidik.PdUsiaSp', {
                    hideMode: 'offsets',
                    routeId: 'PdUsiaSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    SarprasTingkatKerusakanSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.Sarpras.SarprasTingkatKerusakanSp', {
                    hideMode: 'offsets',
                    routeId: 'SarprasTingkatKerusakanSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    SarprasRkbSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.Sarpras.SarprasRkbSp', {
                    hideMode: 'offsets',
                    routeId: 'SarprasRkbSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PtkRingkasanSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.Ptk.PtkRingkasanSp', {
            hideMode: 'offsets',
            routeId: 'PtkRingkasanSp/'+id_level_wilayah+'/'+kode_wilayah,
            id_level_wilayah: id_level_wilayah,
            kode_wilayah: kode_wilayah
        });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },
    
    PengirimanSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.progres.PengirimanSp', {
            hideMode: 'offsets',
            routeId: 'PengirimanSp/'+id_level_wilayah+'/'+kode_wilayah,
            id_level_wilayah: id_level_wilayah,
            kode_wilayah: kode_wilayah
        });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PencapaianKualitasBulananRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.progres.Pencapaian', {
            hideMode: 'offsets',
            routeId: 'PencapaianKualitasBulanan/'+id_level_wilayah+'/'+kode_wilayah,
            id_level_wilayah: id_level_wilayah,
            kode_wilayah: kode_wilayah
        });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PencapaianKualitasBulananSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.progres.PencapaianSp', {
            hideMode: 'offsets',
            routeId: 'PencapaianKualitasBulananSp/'+id_level_wilayah+'/'+kode_wilayah,
            id_level_wilayah: id_level_wilayah,
            kode_wilayah: kode_wilayah
        });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PtkAgamaSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.Ptk.PtkAgamaSp', {
                    hideMode: 'offsets',
                    routeId: 'PtkAgamaSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PtkStatusPegawaiSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.Ptk.PtkStatusPegawaiSp', {
                    hideMode: 'offsets',
                    routeId: 'PtkStatusPegawaiSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    // petaRoute: function(id_level_wilayah, kode_wilayah){
    //     // console.log(sekolah_id);
    //     var me = this,
    //         refs = me.getReferences(),
    //         mainCard = refs.mainCardPanel,
    //         mainLayout = mainCard.getLayout(),
    //         newView;

    //     newView = Ext.create('Admin.view.Peta.Peta', {
    //                 hideMode: 'offsets',
    //                 routeId: 'peta/'+id_level_wilayah+'/'+kode_wilayah,
    //                 id_level_wilayah: id_level_wilayah,
    //                 kode_wilayah: kode_wilayah
    //             });

    //     Ext.suspendLayouts();
    //     mainLayout.setActiveItem(mainCard.add(newView));
    //     Ext.resumeLayouts(true);

    //     // navigationList.setSelection(node);

    //     if (newView.isFocusable(true)) {
    //         newView.focus();
    //     }

    //     newView.getEl().scrollTo('Top', 0, true);
    // },

    RingkasanRombonganBelajarSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.RombonganBelajar.RingkasanRombonganBelajarSp', {
                    hideMode: 'offsets',
                    routeId: 'RingkasanRombonganBelajarSp/'+id_level_wilayah+'/'+kode_wilayah,
                    id_level_wilayah: id_level_wilayah,
                    kode_wilayah: kode_wilayah
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    PtkKualifikasiSpRoute: function(id_level_wilayah, kode_wilayah){
        // console.log(sekolah_id);
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.Ptk.PtkKualifikasiSp', {
            hideMode: 'offsets',
            routeId: 'PtkKualifikasiSp/'+id_level_wilayah+'/'+kode_wilayah,
            id_level_wilayah: id_level_wilayah,
            kode_wilayah: kode_wilayah
        });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    ProfilSpRombelDetailRoute: function(rombongan_belajar_id) {
        var me = this,
            refs = me.getReferences(),
            mainCard = refs.mainCardPanel,
            mainLayout = mainCard.getLayout(),
            newView;

        newView = Ext.create('Admin.view.RombonganBelajar.RombonganBelajarDetail', {
                    hideMode: 'offsets',
                    routeId: 'ProfilSpRombelDetail/'+rombongan_belajar_id,
                    rombongan_belajar_id: rombongan_belajar_id
                });

        Ext.suspendLayouts();
        mainLayout.setActiveItem(mainCard.add(newView));
        Ext.resumeLayouts(true);

        // navigationList.setSelection(node);

        if (newView.isFocusable(true)) {
            newView.focus();
        }

        newView.getEl().scrollTo('Top', 0, true);
    },

    onNavigationTreeSelectionChange: function (tree, node) {
        if (node && node.get('view')) {
            this.redirectTo( node.get("routeId"));
        }
    },

    onToggleNavigationSize: function () {
        var me = this,
            refs = me.getReferences(),
            navigationList = refs.navigationTreeList,
            wrapContainer = refs.mainContainerWrap,
            collapsing = !navigationList.getMicro(),
            new_width = collapsing ? 64 : 250;

        if (Ext.isIE9m || !Ext.os.is.Desktop) {
            Ext.suspendLayouts();

            refs.senchaLogo.setWidth(new_width);

            navigationList.setWidth(new_width);
            navigationList.setMicro(collapsing);

            Ext.resumeLayouts(); // do not flush the layout here...

            // No animation for IE9 or lower...
            wrapContainer.layout.animatePolicy = wrapContainer.layout.animate = null;
            wrapContainer.updateLayout();  // ... since this will flush them
        }
        else {
            if (!collapsing) {
                // If we are leaving micro mode (expanding), we do that first so that the
                // text of the items in the navlist will be revealed by the animation.
                navigationList.setMicro(false);
            }

            // Start this layout first since it does not require a layout
            refs.senchaLogo.animate({dynamic: true, to: {width: new_width}});

            // Directly adjust the width config and then run the main wrap container layout
            // as the root layout (it and its chidren). This will cause the adjusted size to
            // be flushed to the element and animate to that new size.
            navigationList.width = new_width;
            wrapContainer.updateLayout({isRoot: true});

            // We need to switch to micro mode on the navlist *after* the animation (this
            // allows the "sweep" to leave the item text in place until it is no longer
            // visible.
            if (collapsing) {
                navigationList.on({
                    afterlayoutanimation: function () {
                        navigationList.setMicro(true);
                    },
                    single: true
                });
            }
        }
    },

    onMainViewRender:function() {
        if (!window.location.hash) {
            this.redirectTo("dashboard");
        }

        Ext.Ajax.request({
            url: 'Administrasi/logKunjungan?caller=onMainViewRender',
            timeout: 300000000,
            success: function(response){
                var text = response.responseText;

                console.log(text);
            }
        });

        var me = this;

        Ext.Ajax.request({
            url: 'session?mis=bar',
            timeout: 300000000,
            success: function(response){
                var text = response.responseText;

                var res = Ext.JSON.decode(response.responseText);

                var navTree = Ext.getCmp('navigationTreeList');

                navTree.up('panel').up('panel').down('panel[region=north]').update('<div style="margin-top:-10px;float:left;background:url(resources/images/'+res.logo_filename+') no-repeat;background-size:contain;height:40px;width:40px">&nbsp;</div><b>'+res.judul+'</b>');

                var header = Ext.getCmp('headerPanel');

                header.add({
                    xtype: 'displayfield',
                    value: '<h2 style=color:white>'+res.bosnya+'</h2>'
                });

                // if(res.peran_id == 1){
                // console.log(res.kode_wilayah);
                // treeNode.expandChildren(true);

                if(res.session){

                    // if(res.kode_wilayah.trim() == '000000'){
                        // navTree.getStore().getById('page-tentang').remove();
                    // }

                    // alert('oke');
                    if(res.peran_id == 1 && res.kode_wilayah.trim() != '000000'){

                        navTree.getStore().getRootNode().appendChild({
                            text: 'APK dan APM',
                            view: 'ApkApm.ApkApm',
                            leaf: true,
                            iconCls: 'x-fa fa-bar-chart-o',
                            routeId:'ApkApm'
                        });

                    }

                    if(res.peran_id == 10){

                        navTree.getStore().getRootNode().insertChild(1,{
                            text: 'Profil Sekolah',
                            view: 'SatuanPendidikan.ProfilSatuanPendidikan',
                            leaf: true,
                            iconCls: 'x-fa fa-building',
                            // routeId:'RekapRpmj'
                            routeId: '#ProfilSatuanPendidikan/' + res.sekolah_id
                            // href: '#ProfilSatuanPendidikan/' + res.sekolah_id,
                            // hrefTarget: '_self'
                        });

                        navTree.getStore().getRootNode().insertChild(2,{
                            text: 'Mutasi PD',
                            view: 'Mutasi.Mutasi',
                            leaf: true,
                            iconCls: 'x-fa  fa-paperclip',
                            routeId:'Mutasi'
                        });
                    }else{

                        if( res.kode_wilayah.trim() == '000000' || res.peran_id != 1){

                            navTree.getStore().getRootNode().getChildAt(3).appendChild({
                                text: 'PTK',
                                view: 'Ptk.Ptk',
                                leaf: true,
                                iconCls: 'x-fa fa-bar-chart-o',
                                routeId:'PTK',
                                id: 'pages-ptk'
                            });

                        }else{
                            navTree.getStore().getRootNode().getChildAt(3).appendChild({
                                text: 'PTK',
                                view: 'Ptk.Ptk',
                                leaf: true,
                                iconCls: 'x-fa fa-bar-chart-o',
                                routeId:'PTK',
                                id: 'pages-ptk'
                            });
                        }

                    }

                    if( res.kode_wilayah.trim() != '000000' && res.peran_id == 1 ){
                        navTree.getStore().getRootNode().insertChild(2,{
                            text: 'Daftar Pengguna',
                            view: 'Pengguna.Pengguna',
                            leaf: true,
                            iconCls: 'x-fa fa-list-ul',
                            id: 'pages-pengguna',
                            routeId:'Pengguna'
                        });
                    }

                    navTree.up('panel').insert(0,{
                        xtype: 'button',
                        text: 'Keluar',
                        iconCls : 'fa fa-power-off',
                        // scale: 'large',
                        ui:'soft-red',
                        handler: function(btn){
                            localStorage.setItem("local.dapodik.session", null);

                            window.location.replace('prosesLogout');
                        }
                    });

                    Ext.Ajax.request({
                        url     : 'Administrasi/infoPengguna',
                        method  : 'GET',
                        params  : {
                            pengguna_id: res.pengguna_id,
                        },
                        failure : function(response, options){
                            Ext.MessageBox.alert('Mohon maaf','Ada Kesalahan dalam menampilkan data');
                        },
                        success : function(response){
                            var json = Ext.JSON.decode(response.responseText);

                            if(json[0]){
                                var gambar = json[0].gambar;
                            }else{
                                var gambar = 'default.png';
                            }

                            navTree.up('panel').insert(0,[{
                                xtype: 'panel',
                                cls: 'social-panel shadow-panel',
                                itemId: 'panelInfoPengguna',
                                height:100,
                                bodyStyle: 'background:#434343',
                                layout: {
                                    type: 'hbox',
                                    align: 'left'
                                },
                                // padding: '20 0 0 20',
                                items:[{
                                    xtype: 'image',
                                    cls: 'userProfilePic',
                                    height: 70,
                                    width: 70,
                                    margin:'10 10 10 10',
                                    alt: 'profile-picture',
                                    src: 'resources/images/pengguna/'+gambar
                                },{
                                    xtype: 'panel',
                                    flex: 1,
                                    bodyStyle: 'background:#434343;color:white;padding:10px',
                                    html: '<b>'+res.nama+'</b><br><span style=color:#F39C12>'+res.peran_id_str+'</span><br><span style=font-size:10px>'+res.username+'</span>'
                                }]

                            },{
                                xtype: 'panel',
                                height: 30,
                                bodyPadding: 7,
                                bodyStyle: 'background:#808080;color:white',
                                cls: 'social-panel shadow-panel',
                                html: "Semester Aktif: <b>"+res.semester_berjalan_str+"</b>"
                            }]);
                        }
                    });



                }else{
                    // navTree.getStore().getById('pages-pengguna').remove();
                    navTree.getStore().getById('pages-profil-pengguna').remove();

                    // navTree.up('panel').insert(0,{
                    //     xtype: 'button',
                    //     text: 'Masuk',
                    //     scale: 'large',
                    //     handler: 'KlikTombolLogin'
                    // });

                    navTree.up('panel').insert(0,[{
                        xtype: 'panel',
                        cls: 'social-panel shadow-panel',
                        height:90,
                        bodyStyle: 'background:#434343',
                        layout: {
                            type: 'hbox',
                            align: 'left'
                        },
                        // padding: '20 0 0 20',
                        items:[{
                            xtype: 'image',
                            cls: 'userProfilePic',
                            height: 70,
                            width: 70,
                            margin:'10 10 10 10',
                            alt: 'profile-picture',
                            src: 'resources/images/pengguna/default.png'
                        },{
                            xtype: 'panel',
                            flex: 1,
                            bodyStyle: 'background:#434343;color:white;padding:10px',
                            html: '<b>Selamat Datang!</b><br><a href="login" style=text-decoration:none><span style=color:#F39C12>Masuk Sebagai Pengguna</span><br></a>'
                        }]

                    },{
                        xtype: 'panel',
                        height: 30,
                        bodyPadding: 7,
                        bodyStyle: 'background:#808080;color:white',
                        cls: 'social-panel shadow-panel',
                        html: "Semester Aktif: <b>"+res.semester_berjalan_str+"</b>"
                    }]);

                }

                if(res.kode_wilayah == '316000  '){

                    navTree.getStore().getRootNode().getChildAt(3).appendChild({
                        text: 'Rekap RPMJ',
                        view: 'RekapRpmj.RekapRpmj',
                        leaf: true,
                        iconCls: 'x-fa  fa-file',
                        routeId:'RekapRpmj'
                    });

                }

                if(res.modul_mutasi){

                    header.insert(2,{
                        // cls: 'delete-focus-bg',
                        // mixins: ['Admin.view.Mutasi.Badge']
                        xtype: 'button',
                        ui: 'gray',
                        itemId: 'notifikasiMutasi',
                        iconCls:'x-fa fa-bell',
                        text: '',
                        margin: 10,
                        href: '#Mutasi',
                        hrefTarget: '_self',
                        Jumlahexisting: 0,
                        listeners:{
                            click: function(btn){
                                btn.setUI('gray');
                                btn.setText('');
                                // btn.Jumlahexisting = 0;

                                console.log(btn.Jumlahexisting);
                            }

                        }
                    });

                    var runner = new Ext.util.TaskRunner(),clock, updateClock, task;

                    updateClock = function() {
                        // console.log('testing');

                        var storeMutasi = new Admin.store.Mutasi();
                        console.log(new Date() + ' mengecek mutasi baru');

                        if(res.peran_id == 10){
                            storeMutasi.load({
                                params:{
                                    sekolah_id: res.sekolah_id
                                }
                            });
                        }else{
                            storeMutasi.load();
                        }


                        storeMutasi.on('load', function(store){
                            var data = store.data;
                            var count = 0;

                            data.each(function(record){

                                if(res.peran_id != 10){
                                    if(record.data.status_mutasi_id == 1){
                                        count++;
                                    }
                                }else{
                                    if(record.data.status_mutasi_id != 1){
                                        count++;
                                    }

                                }
                            });

                            // console.log(count);

                            if(count > 0){

                                // if(toolbar.down('button[itemId=notifikasiMutasi]').Jumlahexisting > )
                                if(header.down('button[itemId=notifikasiMutasi]').Jumlahexisting < count){
                                    var selisih = (count - header.down('button[itemId=notifikasiMutasi]').Jumlahexisting)

                                    console.log('Selisih: ' + selisih);

                                    header.down('button[itemId=notifikasiMutasi]').setText(selisih + '');
                                    header.down('button[itemId=notifikasiMutasi]').setUI('soft-red');
                                    header.down('button[itemId=notifikasiMutasi]').Jumlahexisting = count;
                                }else{
                                    header.down('button[itemId=notifikasiMutasi]').Jumlahexisting = count;

                                }



                                console.log(header.down('button[itemId=notifikasiMutasi]').Jumlahexisting);
                            }
                        })
                    }

                    task = runner.start({
                        run: updateClock,
                        interval: 60000
                    });
                    
                    navTree.getStore().getRootNode().appendChild(
                        // text: 'Administrasi',
                        // expanded: false,
                        // selectable: false,
                        // iconCls: 'x-fa fa-simplybuilt',
                        // routeId : 'pages-parent-administrasi',
                        // id:       'pages-parent-administrasi',
                        // children: [
                        //     {
                        //         text: 'APK dan APM',
                        //         view: 'ApkApm.ApkApm',
                        //         leaf: true,
                        //         iconCls: 'x-fa  fa-paperclip',
                        //         routeId:'ApkApm'
                        //     }
                            {
                                text: 'Mutasi PD',
                                view: 'Mutasi.Mutasi',
                                leaf: true,
                                iconCls: 'x-fa  fa-paperclip',
                                routeId:'Mutasi'
                            }
                        // ]
                    );

                }
                // }
            }
        });

        // console.log(me);
    },

    onRouteChange:function(id){
        this.setCurrentView(id);
    },

    onSearchRouteChange: function () {
        this.setCurrentView('search');
    },

    onEmailRouteChange: function () {
        this.setCurrentView('email');
    },

    BentukPendidikanComboSetup: function(combo){
        // combo.getStore().reload();

        combo.getStore().on('beforeload', function(store){
            // console.log(store);

            var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

            // console.log(session);

            if(session.tingkat == 'DIKMEN'){
                // Ext.apply(store.proxy.extraParams, {
                //     bentuk_pendidikan_id: 13
                // });

                if(session.bentuk_pendidikan == 'SMA'){
                    store.filter(function(r,id){
                        if(r.data.bentuk_pendidikan_id == 13){
                            return true;
                        }else{
                            return false;
                        }

                    });


                    combo.setValue(13);
                    combo.disable();

                }else if(session.bentuk_pendidikan == 'SMK'){
                    store.filter(function(r,id){
                        if( r.data.bentuk_pendidikan_id == 15){
                            return true;
                        }else{
                            return false;
                        }

                    });

                    combo.setValue(15);
                    combo.disable();

                }else if(session.bentuk_pendidikan == 'SMASMK'){
                    store.filter(function(r,id){
                        if( r.data.bentuk_pendidikan_id == 13 || r.data.bentuk_pendidikan_id == 15){
                            return true;
                        }else{
                            return false;
                        }
                    });
                }

            }else if(session.tingkat == 'DIKDAS'){
                store.filter(function(r,id){
                    if( r.data.bentuk_pendidikan_id == 5 || r.data.bentuk_pendidikan_id == 6 || r.data.bentuk_pendidikan_id == 29){
                        return true;
                    }else{
                        return false;
                    }
                });
            }else if(session.tingkat == 'DIKDASMEN'){
                store.filter(function(r,id){
                    if( r.data.bentuk_pendidikan_id == 5 || r.data.bentuk_pendidikan_id == 6 || r.data.bentuk_pendidikan_id == 29 || r.data.bentuk_pendidikan_id == 13 || r.data.bentuk_pendidikan_id == 15){
                        return true;
                    }else{
                        return false;
                    }
                });
            }else if(session.tingkat == 'DIKDASMEN-PAUD'){
                store.filter(function(r,id){
                    if( r.data.bentuk_pendidikan_id == 1 || r.data.bentuk_pendidikan_id == 2 || r.data.bentuk_pendidikan_id == 3 || r.data.bentuk_pendidikan_id == 4 || r.data.bentuk_pendidikan_id == 5 || r.data.bentuk_pendidikan_id == 6 || r.data.bentuk_pendidikan_id == 29 || r.data.bentuk_pendidikan_id == 13 || r.data.bentuk_pendidikan_id == 15){
                        return true;
                    }else{
                        return false;
                    }
                });
            }
        });

        combo.getStore().load();

    },

    tombolUnduhExcel: function(btn){

        // Ext.MessageBox.alert('tes', 'okesp');

        var paramClass = btn.paramClass;
        var paramMethod = btn.paramMethod;
        var bpCombo = btn.up('toolbar').down('bentukpendidikancombo');
        var jptkCombo = btn.up('toolbar').down('jenisptkcombo');
        var bp = null;

        var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));

        var id_level_wilayah = 0;
        var kode_wilayah = '000000';

        if(btn.id_level_wilayah) {
            id_level_wilayah = btn.id_level_wilayah;
            kode_wilayah = btn.kode_wilayah;
        } else {
            id_level_wilayah = session.id_level_wilayah;
            kode_wilayah = session.kode_wilayah;
        }

        var url = "excelExt/getExcel?class="+paramClass+"&method="+paramMethod+"&id_level_wilayah="+id_level_wilayah+"&kode_wilayah="+kode_wilayah;

        if(bpCombo){

            if(bpCombo.getValue()){
                url += "&bentuk_pendidikan_id="+bpCombo.getValue();
            }

        }

        if(jptkCombo){

            if(jptkCombo.getValue()){
                url += "&jenis_ptk_id="+jptkCombo.getValue();
            }

        }

        window.location.replace(url);

        // return url;

        // Ext.MessageBox.alert('Status', url);
    },
    refresh: function(btn){
        btn.up('gridpanel').getStore().reload();
    },
    kembali: function(btn){
        var grid = btn.up('gridpanel');
        var store = grid.getStore();
        var rec = store.getAt(0);

        var hash = window.location.hash.substr(1);
        var hashArr = hash.split('/');

        if(typeof hashArr[1] === 'undefined' || typeof hashArr[2] === 'undefined'){

            var kode_wilayah = '000000';
            var id_level_wilayah = 0;
            
            grid.getStore().reload({
                params:{
                    id_level_wilayah: (rec.data.id_level_wilayah - 2),
                    kode_wilayah : rec.data.mst_kode_wilayah_induk
                }
            });
            
        }else{
    
            if(hashArr[1] == 2){
                var kode_wilayah = hashArr[2].substr(0,2)+"0000";
                var id_level_wilayah = hashArr[1]-1;
            }else if(hashArr[1] == 1){
                var kode_wilayah = '000000';
                var id_level_wilayah = hashArr[1]-1;
            }

            window.location="#"+hashArr[0]+"/"+id_level_wilayah+"/"+kode_wilayah;
        
        }
        

    },

    JenisPtkComboSetup: function(combo){
        combo.getStore().load();
    },
    toolbarAfterRender: function(toolbar){
        // Ext.MessageBox.alert('tes', 'oke');

        // console.log(tree.getStore().getById('pages-parent-data-pokok'));

        // var session = Ext.JSON.decode(localStorage.getItem("local.dapodik.session"));
        Ext.Ajax.request({
            url: 'session',
            timeout: 300000000,
            success: function(response){
                var text = response.responseText;

                var res = Ext.JSON.decode(response.responseText);
                var tree = toolbar.up('mainviewport').down('maincontainerwrap').down('treelist');

                if(res.session == false){
                    toolbar.insert(3,{
                        xtype: 'button',
                        text: 'Login Sebagai Pengguna',
                        ui: 'soft-green',
                        handler: 'KlikTombolLogin'
                    });

                    // tree.getStore().getById('pages-parent-data-pokok').remove();

                    tree.getStore().getById('pages-pengguna').remove();
                    tree.getStore().getById('pages-profil-pengguna').remove();

                }else{
                    toolbar.insert(3,{
                        xtype: 'button',
                        text: res.nama,
                        // handler: 'bukaProfilPengguna'
                        href: '#profilPengguna',
                        hrefTarget: '_self'
                    });

                    toolbar.insert(4,{
                        xtype: 'image',
                        cls: 'header-right-profile-image',
                        height: 35,
                        width: 35,
                        alt:'current user image',
                        src: 'resources/images/logo.png'
                    });

                    if(res.peran_id != 1){
                        tree.getStore().getById('pages-pengguna').remove();

                        // tree.getStore().add();
                    }

                    // if(res.modul_kualitas_data){
                    //     //do nothing for now
                    // }else{
                    //     tree.getStore().getById('pages-parent-administrasi').remove();                        
                    // }

                    if(res.modul_mutasi){
                        console.log('jalan');

                        toolbar.insert(3,{
                            // cls: 'delete-focus-bg',
                            // mixins: ['Admin.view.Mutasi.Badge']
                            xtype: 'button',
                            ui: 'gray',
                            itemId: 'notifikasiMutasi',
                            iconCls:'x-fa fa-bell',
                            text: '',
                            href: '#Mutasi',
                            hrefTarget: '_self',
                            Jumlahexisting: 0,
                            listeners:{
                                click: function(btn){
                                    btn.setUI('gray');
                                    btn.setText('');
                                    // btn.Jumlahexisting = 0;

                                    console.log(btn.Jumlahexisting);
                                }

                            }
                        });

                        var runner = new Ext.util.TaskRunner(),clock, updateClock, task;

                        updateClock = function() {
                            // console.log('testing');

                            var storeMutasi = new Admin.store.Mutasi();
                            console.log(new Date() + ' mengecek mutasi baru');

                            if(res.peran_id == 10){
                                storeMutasi.load({
                                    params:{
                                        sekolah_id: res.sekolah_id
                                    }
                                });
                            }else{
                                storeMutasi.load();
                            }


                            storeMutasi.on('load', function(store){
                                var data = store.data;
                                var count = 0;

                                data.each(function(record){

                                    if(res.peran_id != 10){
                                        if(record.data.status_mutasi_id == 1){
                                            count++;
                                        }
                                    }else{
                                        if(record.data.status_mutasi_id != 1){
                                            count++;
                                        }

                                    }
                                });

                                // console.log(count);

                                if(count > 0){

                                    // if(toolbar.down('button[itemId=notifikasiMutasi]').Jumlahexisting > )
                                    if(toolbar.down('button[itemId=notifikasiMutasi]').Jumlahexisting < count){
                                        var selisih = (count - toolbar.down('button[itemId=notifikasiMutasi]').Jumlahexisting)

                                        console.log('Selisih: ' + selisih);

                                        toolbar.down('button[itemId=notifikasiMutasi]').setText(selisih + '');
                                        toolbar.down('button[itemId=notifikasiMutasi]').setUI('soft-red');
                                        toolbar.down('button[itemId=notifikasiMutasi]').Jumlahexisting = count;
                                    }else{
                                        toolbar.down('button[itemId=notifikasiMutasi]').Jumlahexisting = count;

                                    }



                                    console.log(toolbar.down('button[itemId=notifikasiMutasi]').Jumlahexisting);
                                }
                            })
                        }

                        task = runner.start({
                            run: updateClock,
                            interval: 60000
                        });
                    }



                }

                toolbar.insert(2,{
                    xtype: 'tbtext',
                    text: '<b style="color:#cccccc;font-size:20px">'+res.bosnya+'</b>'
                });
            }
        });


    },
    KlikTombolLogin: function(btn){
        // Ext.MessageBox.alert('tes', 'klik tombol login');
        window.location.replace('login');
    },
    bukaProfilPengguna: function(btn){
        Ext.MessageBox.alert('tes', 'klik tombol profil pengguna');
    },
    simpanForm: function(btn){
        var form = btn.up('form');
        if (form.isValid()) {
            // Ambil record dari form, form tersebut diisi dari sebuah record
            var record = form.getRecord();
            var values = form.getValues(false,false,false,true);
            record.set(values);

            // Synchronize the store after editing the record
            var store = record.store;
            store.sync();
        } else {
            Ext.MessageBox.alert('Error', 'Gagal menyimpan, mohon isi form dengan benar. <br>Field yang salah ditandai dengan kotak merah.');
        }
    }
});
