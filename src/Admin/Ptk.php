<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Admin\Preface;
use Admin\Model\PtkPeer;
use Admin\Model\PtkTerdaftarPeer;
use Admin\Model\SekolahPeer;

class Ptk
{
	static function Ptk(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		$bidang_studi_id = $request->get('bidang_studi_id');

		$arrAll = array();
		$return = array();

		$count = 0;

		// $c = new \Criteria();
		// $c->addJoin(PtkPeer::PTK_ID, PtkTerdaftarPeer::PTK_ID, \Criteria::JOIN);
		// $c->addJoin(PtkTerdaftarPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, \Criteria::JOIN);
		// $c->add(PtkPeer::SOFT_DELETE, 0);
		// $c->add(PtkTerdaftarPeer::SOFT_DELETE, 0);

		// if($requesta['sekolah_id']){
		// 	$c->add(PtkPeer::ENTRY_SEKOLAH_ID, $requesta['sekolah_id']);
		// 	$c->add(PtkTerdaftarPeer::SEKOLAH_ID, $requesta['sekolah_id']);
		// }

		// $c->add(SekolahPeer::BENTUK_PENDIDIKAN_ID, array(5,6,13,15,29), \Criteria::IN);
		// $c->add(PtkTerdaftarPeer::JENIS_KELUAR_ID, null, \Criteria::ISNULL);
		// $c->add(PtkTerdaftarPeer::TAHUN_AJARAN_ID, $requesta['tahun_ajaran_id']);

		// $count = PtkPeer::doCount($c);

		// $c->setOffset($requesta['start']);
		// $c->setLimit($requesta['limit']);

		// $ptks = PtkPeer::doSelect($c);

		// foreach ($ptks as $ptk) {
		// 	$arr = array();

		// 	$arr = $ptk->toArray(\BasePeer::TYPE_FIELDNAME);

		// 	array_push($arrAll, $arr);
		// }

		$sql_hitung_awal = "select sum(1) as count";
		$sql_awal = "select
						bidang_sdm.bidang_studi_id,
						bidang_studi.bidang_studi as bidang_studi_id_str,
						ptk.*,
						sekolah.nama as nama_sekolah,
						skp.nama as status_kepegawaian_id_str,
						jenis_ptk.jenis_ptk as jenis_ptk_id_str,
						lembaga_pengangkat.nama as lembaga_pengangkat_id_str,
						sumber_gaji.nama as sumber_gaji_id_str,
						agama.nama as agama_id_str";

		$sql_tengah = " FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN sekolah ON sekolah.sekolah_id = ptkd.sekolah_id
						JOIN ref.mst_wilayah kec on kec.kode_wilayah = LEFT(sekolah.kode_wilayah,6)
						JOIN ref.mst_wilayah kab on kab.kode_wilayah = kec.mst_kode_wilayah
						JOIN ref.mst_wilayah prop on prop.kode_wilayah = kab.mst_kode_wilayah
						JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						LEFT OUTER JOIN ref.status_kepegawaian skp on skp.status_kepegawaian_id = ptk.status_kepegawaian_id
						LEFT OUTER JOIN ref.jenis_ptk jenis_ptk on jenis_ptk.jenis_ptk_id = ptk.jenis_ptk_id
						LEFT OUTER JOIN ref.lembaga_pengangkat lembaga_pengangkat on lembaga_pengangkat.lembaga_pengangkat_id = ptk.lembaga_pengangkat_id
						LEFT OUTER JOIN ref.sumber_gaji sumber_gaji on sumber_gaji.sumber_gaji_id = ptk.sumber_gaji_id
						LEFT OUTER JOIN ref.agama agama on agama.agama_id = ptk.agama_id
						LEFT OUTER JOIN (
							SELECT
								ptk_id,
								MAX (bidang_studi_id) AS bidang_studi_id
							FROM
								bidang_sdm
							WHERE
								soft_delete = 0
							GROUP BY
								ptk_id
						) bidang_sdm ON bidang_sdm.ptk_id = ptk.ptk_id
						LEFT OUTER JOIN ref.bidang_studi bidang_studi on bidang_studi.bidang_studi_id = bidang_sdm.bidang_studi_id
						WHERE
							ptk.Soft_delete = 0
						AND ptkd.Soft_delete = 0
						AND ptkd.ptk_induk = 1
						AND sekolah.Soft_delete = 0
						AND ptkd.tahun_ajaran_id = {$requesta['tahun_ajaran_id']}
						AND (
							ptkd.tgl_ptk_keluar > ta.tanggal_selesai
							OR ptkd.jenis_keluar_id IS NULL
						)
						AND sekolah.bentuk_pendidikan_id IN (5,6,13,15,29)
						AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14, 11, 99, 30, 40)";

		$sql_params = "";

		if($requesta['keyword'] && $requesta['keyword'] != 'null'){
			$sql_params .= " AND (ptk.nama like '%{$requesta['keyword']}%' OR ptk.nuptk like '%{$requesta['keyword']}%')";
		}

		if($requesta['propinsi'] && $requesta['propinsi'] != 'null'){
			$sql_params .= " AND prop.kode_wilayah = '{$requesta['propinsi']}'";
		}

		if($requesta['kabupaten'] && $requesta['kabupaten'] != 'null'){
			$sql_params .= " AND kab.kode_wilayah = '{$requesta['kabupaten']}'";
		}

		if($requesta['kecamatan'] && $requesta['kecamatan'] != 'null'){
			$sql_params .= " AND kec.kode_wilayah = '{$requesta['kecamatan']}'";
		}

		if($requesta['bentuk_pendidikan_id'] && $requesta['bentuk_pendidikan_id'] != 'null'){
			$sql_params .= " AND sekolah.bentuk_pendidikan_id = '{$requesta['bentuk_pendidikan_id']}'";
		}


		if($requesta['status_sekolah'] && $requesta['status_sekolah'] != 'null'){

			if($requesta['status_sekolah'] != '999'){
				$sql_params .= " AND sekolah.status_sekolah = '{$requesta['status_sekolah']}'";
			}

		}

		if($request->get('ptk_id') && $request->get('ptk_id') != 'null'){
			$sql_params .= " AND ptk.ptk_id = '{$request->get('ptk_id')}'";
		}

		if($requesta['sekolah_id'] && $requesta['sekolah_id'] != 'null'){
			$sql_params .= " AND sekolah.sekolah_id = '{$requesta['sekolah_id']}'";
		}

		if($bidang_studi_id && $bidang_studi_id != 'null'){
			$sql_params .= " AND bidang_sdm.bidang_studi_id = '{$bidang_studi_id}'";
		}

		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMA'){
				$sql_params .= " AND sekolah.bentuk_pendidikan_id = 13";
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$sql_params .= " AND sekolah.bentuk_pendidikan_id = 15";
			}else if(BENTUKPENDIDIKAN == 'SMASMK'){
				$sql_params .= " AND sekolah.bentuk_pendidikan_id IN (13,15)";
			}else{
				$sql_params .= " AND sekolah.bentuk_pendidikan_id IN (13,15)";
			}

		}
		else if(TINGKAT == 'DIKDAS'){
			$sql_params .= " AND sekolah.bentuk_pendidikan_id IN (5,6,29)";
		}else if(TINGKAT == 'DIKDASMEN'){
			$sql_params .= " AND sekolah.bentuk_pendidikan_id IN (5,6,29,13,15)";
		}

		$sql_count = $sql_hitung_awal.$sql_tengah.$sql_params;
		$sql_fetch = $sql_awal.$sql_tengah.$sql_params;


		// return $sql_fetch;die;
		// return $sql_awal.$sql_tengah;die;
		$fetch = getDataBySql($sql_count);

		$count = $fetch[0]['count'];

		$sql_fetch .= " ORDER BY ptk.nama OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

		$fetch = getDataBySql($sql_fetch);


		foreach ($fetch as $f) {

			foreach ($f as $fc => $v) {
				$arr[$fc] = $v;

			}

			array_push($arrAll, $arr);
		}

		$return['success'] = true;
		$return['results'] = $count;
		$return['rows'] = $arrAll;

		return json_encode($return);

	}

	static function PtkRingkasan(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_provinsi}
					{$add_kode_wilayah_induk_provinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					sum( case when status_sekolah = 1 then guru else 0 end ) as guru_negeri,
					sum( case when status_sekolah = 2 then guru else 0 end ) as guru_swasta,
					sum( case when status_sekolah = 1 then pegawai else 0 end ) as pegawai_negeri,
					sum( case when status_sekolah = 2 then pegawai else 0 end ) as pegawai_swasta,
					sum( case when status_sekolah = 1 then (guru+pegawai) else 0 end ) as ptk_negeri,
					sum( case when status_sekolah = 2 then (guru+pegawai) else 0 end ) as ptk_swasta,
					sum(1) as jumlah_sekolah,
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND Soft_delete = 0
				{$param_kode_wilayah}
				{$params_bp}
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_provinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_provinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);

	}

	static function PtkRingkasanSp(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				//nothing to do
				$wilayah = null;
				$params_wilayah = null;
				break;
			case "1":
				$wilayah = 'propinsi';
				$params_wilayah = 'kode_wilayah_propinsi';
				break;
			case "2":
				$wilayah = 'kabupaten';
				$params_wilayah = 'kode_wilayah_kabupaten';
				break;
			case "3":
				$wilayah = 'kecamatan';
				$params_wilayah = 'kode_wilayah_kecamatan';
				break;
			default:
				break;
		}

		$sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY nama) as 'no',
                    sekolah_id,
                    nama,
                    npsn,
                    status_sekolah,
                    CASE status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bentuk_pendidikan_id,
                    CASE 
                        WHEN bentuk_pendidikan_id = 1 THEN	'TK'
                        WHEN bentuk_pendidikan_id = 2 THEN	'KB'
                        WHEN bentuk_pendidikan_id = 3 THEN	'TPA'
                        WHEN bentuk_pendidikan_id = 4 THEN	'SPS'
                        WHEN bentuk_pendidikan_id = 5 THEN	'SD'
                        WHEN bentuk_pendidikan_id = 6 THEN	'SMP'
                        WHEN bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN bentuk_pendidikan_id = 13 THEN	'SMA'
                        WHEN bentuk_pendidikan_id = 15 THEN	'SMK'
                    ELSE '-' END AS bentuk,
                    kecamatan,
                    kode_wilayah_kecamatan,
                    kabupaten,
                    kode_wilayah_kabupaten,
                    propinsi,
                    kode_wilayah_propinsi,
                    SUM(guru) as guru,
                    SUM(pegawai) as pegawai,
                    SUM((guru+pegawai)) as ptk,
                    min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND {$params_wilayah} = '".$requesta['kode_wilayah']."'
				AND Soft_delete = 0
				{$params_bp}
				GROUP BY
                    sekolah_id,
                    nama,
                    npsn,
                    status_sekolah,
                    bentuk_pendidikan_id,
                   	kecamatan,
                    kode_wilayah_kecamatan,
                    kabupaten,
                    kode_wilayah_kabupaten,
                    propinsi,
                    kode_wilayah_propinsi
				ORDER BY
					nama";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		$sql .= " OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";


		$final = array();
		$final['results'] = sizeof($return);

		$return = Preface::getDataByQuery($sql,'rekap');

		$final['rows'] = $return;

		return json_encode($final);

	}

	static function PtkAgama(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case 0:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 1:
				$col_wilayah = "w2.nama";
				$col_kode = "w2.kode_wilayah";
				$col_id_level = "w2.id_level_wilayah";
				$col_mst_kode = "w2.mst_kode_wilayah";
				$col_mst_kode_induk = 'w3.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w3.mst_kode_wilayah,';
				$params_wilayah = " and w2.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 2:
				$col_wilayah = "w1.nama";
				$col_kode = "w1.kode_wilayah";
				$col_id_level = "w1.id_level_wilayah";
				$col_mst_kode = "w1.mst_kode_wilayah";
				$col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
				$params_wilayah = " and w1.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			default:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 's.');

		if($request->get('jenis_ptk_id')){
			if($request->get('jenis_ptk_id') == 1){
				$params_jptk = "AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)";
			}else if($request->get('jenis_ptk_id') == 2){
				$params_jptk = "AND ptk.jenis_ptk_id IN (11, 99, 30, 40)";
			}else{
				$params_jptk = "AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14, 11, 99, 30, 40)";
			}
		}else{
			$params_jptk = "AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14, 11, 99, 30, 40)";
		}

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$col_wilayah}) as 'no',
					{$col_wilayah} as nama,
					{$col_kode} as kode_wilayah,
					{$col_id_level} as id_level_wilayah,
					{$col_mst_kode} as mst_kode_wilayah,
					{$col_mst_kode_induk}
					-- count(1) as total,
					SUM(CASE WHEN ptk.agama_id in (1,2,3,4,5,6,99) THEN 1 ELSE 0 END) as total,
					SUM(CASE WHEN ptk.agama_id = 1 THEN 1 ELSE 0 END) as islam,
					SUM(CASE WHEN ptk.agama_id = 2 THEN 1 ELSE 0 END) as kristen,
					SUM(CASE WHEN ptk.agama_id = 3 THEN 1 ELSE 0 END) as katholik,
					SUM(CASE WHEN ptk.agama_id = 4 THEN 1 ELSE 0 END) as hindu,
					SUM(CASE WHEN ptk.agama_id = 5 THEN 1 ELSE 0 END) as budha,
					SUM(CASE WHEN ptk.agama_id = 6 THEN 1 ELSE 0 END) as konghucu,
					SUM(CASE WHEN ptk.agama_id = 7 THEN 1 ELSE 0 END) as kepercayaan,
					SUM(CASE WHEN ptk.agama_id = 99 THEN 1 ELSE 0 END) as lainnya,
					getdate() as tanggal_rekap_terakhir
				FROM
					ptk ptk with(nolock)
				JOIN ptk_terdaftar ptkd with(nolock) ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta with(nolock) ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				JOIN sekolah s with(nolock) ON ptkd.sekolah_id = s.sekolah_id
				JOIN ref.mst_wilayah w1 with(nolock) ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
				JOIN ref.mst_wilayah w2 with(nolock) ON w1.mst_kode_wilayah = w2.kode_wilayah
				JOIN ref.mst_wilayah w3 with(nolock) ON w2.mst_kode_wilayah = w3.kode_wilayah
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = ".$requesta['tahun_ajaran_id']."
				{$params_jptk}
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				{$params_wilayah}
				{$params_bp}
				GROUP BY
					{$col_wilayah},
					{$col_kode},
					{$col_id_level},
					{$col_mst_kode_induk_group}
					{$col_mst_kode}
				ORDER BY
					{$col_wilayah} ASC";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);

	}

    static function PtkAgamaSp(Request $request, Application $app){
        $requesta = Preface::Inisiasi($request);

        switch ($requesta['id_level_wilayah']) {
            case 0:
                $col_wilayah = "w3.nama";
                $col_kode = "w3.kode_wilayah";
                $col_id_level = "w3.id_level_wilayah";
                $col_mst_kode = "w3.mst_kode_wilayah";
                $col_mst_kode_induk = '';
                $col_mst_kode_induk_group = '';
                $params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
                break;
            case 1:
                $col_wilayah = "w2.nama";
                $col_kode = "w2.kode_wilayah";
                $col_id_level = "w2.id_level_wilayah";
                $col_mst_kode = "w2.mst_kode_wilayah";
                $col_mst_kode_induk = 'w3.mst_kode_wilayah as mst_kode_wilayah_induk,';
                $col_mst_kode_induk_group = 'w3.mst_kode_wilayah,';
                $params_wilayah = " and w2.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
                break;
            case 2:
                $col_wilayah = "w1.nama";
                $col_kode = "w1.kode_wilayah";
                $col_id_level = "w1.id_level_wilayah";
                $col_mst_kode = "w1.mst_kode_wilayah";
                $col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
                $col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
                $params_wilayah = " and w1.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
                break;
            case 3:
                $col_wilayah = "w1.nama";
                $col_kode = "w1.kode_wilayah";
                $col_id_level = "w1.id_level_wilayah";
                $col_mst_kode = "w1.mst_kode_wilayah";
                $col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
                $col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
                $params_wilayah = " and w1.kode_wilayah = '".$requesta['kode_wilayah']."'";
                break;
            default:
                $col_wilayah = "w3.nama";
                $col_kode = "w3.kode_wilayah";
                $col_id_level = "w3.id_level_wilayah";
                $col_mst_kode = "w3.mst_kode_wilayah";
                $col_mst_kode_induk = '';
                $col_mst_kode_induk_group = '';
                $params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
                break;
        }

        $params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 's.');

        if($request->get('jenis_ptk_id')){
            if($request->get('jenis_ptk_id') == 1){
                $params_jptk = "AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)";
            }else if($request->get('jenis_ptk_id') == 2){
                $params_jptk = "AND ptk.jenis_ptk_id IN (11, 99, 30, 40)";
            }else{
                $params_jptk = "AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14, 11, 99, 30, 40)";
            }
        }else{
            $params_jptk = "AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14, 11, 99, 30, 40)";
        }

        $sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY s.nama) as 'no',
					s.sekolah_id as sekolah_id,
					s.nama,
					s.npsn,
					s.bentuk_pendidikan_id,
					bp.nama as bentuk,
					s.status_sekolah,
					CASE s.status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
					w1.nama as kecamatan,
					w2.nama as kabupaten,
					w3.nama as propinsi,
					SUM(CASE WHEN ptk.agama_id = 1 THEN 1 ELSE 0 END) as islam,
					SUM(CASE WHEN ptk.agama_id = 2 THEN 1 ELSE 0 END) as kristen,
					SUM(CASE WHEN ptk.agama_id = 3 THEN 1 ELSE 0 END) as katholik,
					SUM(CASE WHEN ptk.agama_id = 4 THEN 1 ELSE 0 END) as hindu,
					SUM(CASE WHEN ptk.agama_id = 5 THEN 1 ELSE 0 END) as budha,
					SUM(CASE WHEN ptk.agama_id = 6 THEN 1 ELSE 0 END) as konghucu,
					SUM(CASE WHEN ptk.agama_id = 7 THEN 1 ELSE 0 END) as kepercayaan,
					SUM(CASE WHEN ptk.agama_id = 99 THEN 1 ELSE 0 END) as lainnya
				FROM
					ptk ptk
				LEFT OUTER JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				LEFT OUTER JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				LEFT OUTER JOIN sekolah s ON ptkd.sekolah_id = s.sekolah_id
				LEFT OUTER JOIN ref.mst_wilayah w1 ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
				LEFT OUTER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
				LEFT OUTER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
				LEFT OUTER JOIN ref.bentuk_pendidikan bp on bp.bentuk_pendidikan_id = s.bentuk_pendidikan_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = ".$requesta['tahun_ajaran_id']."
				{$params_jptk}
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				{$params_wilayah}
				{$params_bp}
				GROUP BY
					s.sekolah_id,
					s.nama,
					s.npsn,
					s.bentuk_pendidikan_id,
					bp.nama,
					s.status_sekolah,
					w1.nama,
					w2.nama,
					w3.nama
				ORDER BY
					s.nama ASC";

        // return $sql;die;
		$return = Preface::getDataByQuery($sql);

		$sql .= " OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";


		$final = array();
		$final['results'] = sizeof($return);

		$return = Preface::getDataByQuery($sql);

		$final['rows'] = $return;

		return json_encode($final);
    }

	static function PtkStatusPegawai(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case 0:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 1:
				$col_wilayah = "w2.nama";
				$col_kode = "w2.kode_wilayah";
				$col_id_level = "w2.id_level_wilayah";
				$col_mst_kode = "w2.mst_kode_wilayah";
				$col_mst_kode_induk = 'w3.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w3.mst_kode_wilayah,';
				$params_wilayah = " and w2.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 2:
				$col_wilayah = "w1.nama";
				$col_kode = "w1.kode_wilayah";
				$col_id_level = "w1.id_level_wilayah";
				$col_mst_kode = "w1.mst_kode_wilayah";
				$col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
				$params_wilayah = " and w1.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			default:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 's.');

		if($request->get('jenis_ptk_id')){
			if($request->get('jenis_ptk_id') == 1){
				$params_jptk = "AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)";
			}else if($request->get('jenis_ptk_id') == 2){
				$params_jptk = "AND ptk.jenis_ptk_id IN (11, 99, 30, 40)";
			}else{
				$params_jptk = "AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14, 11, 99, 30, 40)";
			}
		}else{
			$params_jptk = "AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14, 11, 99, 30, 40)";
		}

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$col_wilayah}) as 'no',
					{$col_wilayah} as nama,
					{$col_kode} as kode_wilayah,
					{$col_id_level} as id_level_wilayah,
					{$col_mst_kode} as mst_kode_wilayah,
					{$col_mst_kode_induk}
					count(1),
					SUM(CASE WHEN ptk.status_kepegawaian_id = 1 THEN 1 ELSE 0 END) as pns,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 2 THEN 1 ELSE 0 END) as pns_diperbantukan,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 3 THEN 1 ELSE 0 END) as pns_depag,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 4 THEN 1 ELSE 0 END) as gty_pty,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 5 THEN 1 ELSE 0 END) as gtt_provinsi,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 6 THEN 1 ELSE 0 END) as gtt_kabkota,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 7 THEN 1 ELSE 0 END) as guru_bantu_pusat,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 8 THEN 1 ELSE 0 END) as guru_honor_sekolah,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 9 THEN 1 ELSE 0 END) as tenaga_honor_sekolah,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 10 THEN 1 ELSE 0 END) as cpns,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 51 THEN 1 ELSE 0 END) as kontrak_kerja_wna,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 99 THEN 1 ELSE 0 END) as lainnya,
					getdate() as tanggal_rekap_terakhir
				FROM
					ptk ptk with(nolock)
				JOIN ptk_terdaftar ptkd with(nolock) ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta with(nolock) ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				JOIN sekolah s with(nolock) ON ptkd.sekolah_id = s.sekolah_id
				JOIN ref.mst_wilayah w1 with(nolock) ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
				JOIN ref.mst_wilayah w2 with(nolock) ON w1.mst_kode_wilayah = w2.kode_wilayah
				JOIN ref.mst_wilayah w3 with(nolock) ON w2.mst_kode_wilayah = w3.kode_wilayah
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = ".$requesta['tahun_ajaran_id']."
				{$params_jptk}
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				{$params_wilayah}
				{$params_bp}
				GROUP BY
					{$col_wilayah},
					{$col_kode},
					{$col_id_level},
					{$col_mst_kode_induk_group}
					{$col_mst_kode}
				ORDER BY
					{$col_wilayah} ASC";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);

	}

	static function PtkStatusPegawaiSp(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 's.');

		if($request->get('jenis_ptk_id')){
			if($request->get('jenis_ptk_id') == 1){
				$params_jptk = "AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)";
			}else if($request->get('jenis_ptk_id') == 2){
				$params_jptk = "AND ptk.jenis_ptk_id IN (11, 99, 30, 40)";
			}else{
				$params_jptk = "AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14, 11, 99, 30, 40)";
			}
		}else{
			$params_jptk = "AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14, 11, 99, 30, 40)";
		}

		switch ($requesta['id_level_wilayah']) {
			case 1:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 2:
				$col_wilayah = "w2.nama";
				$col_kode = "w2.kode_wilayah";
				$col_id_level = "w2.id_level_wilayah";
				$col_mst_kode = "w2.mst_kode_wilayah";
				$col_mst_kode_induk = 'w3.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w3.mst_kode_wilayah,';
				$params_wilayah = " and w2.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 3:
				$col_wilayah = "w1.nama";
				$col_kode = "w1.kode_wilayah";
				$col_id_level = "w1.id_level_wilayah";
				$col_mst_kode = "w1.mst_kode_wilayah";
				$col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
				$params_wilayah = " and w1.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			default:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
		}

		$sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY s.nama) as 'no',
                    s.sekolah_id,
                    s.nama,
                    s.npsn,
                    s.status_sekolah,
                    CASE s.status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    s.bentuk_pendidikan_id,
                    CASE 
                        WHEN s.bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN s.bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN s.bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN s.bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN s.bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN s.bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN s.bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN s.bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN s.bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
					{$col_wilayah} AS kecamatan,
					{$col_kode} as kode_wilayah,
					{$col_id_level} as id_level_wilayah,
					{$col_mst_kode} as mst_kode_wilayah,
					getdate() as tanggal_rekap_terakhir,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 1 THEN 1 ELSE 0 END) as pns,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 2 THEN 1 ELSE 0 END) as pns_diperbantukan,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 3 THEN 1 ELSE 0 END) as pns_depag,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 4 THEN 1 ELSE 0 END) as gty_pty,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 5 THEN 1 ELSE 0 END) as gtt_provinsi,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 6 THEN 1 ELSE 0 END) as gtt_kabkota,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 7 THEN 1 ELSE 0 END) as guru_bantu_pusat,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 8 THEN 1 ELSE 0 END) as guru_honor_sekolah,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 9 THEN 1 ELSE 0 END) as tenaga_honor_sekolah,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 10 THEN 1 ELSE 0 END) as cpns,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 51 THEN 1 ELSE 0 END) as kontrak_kerja_wna,
					SUM(CASE WHEN ptk.status_kepegawaian_id = 99 THEN 1 ELSE 0 END) as lainnya
				FROM
					ptk ptk WITH(NOLOCK)
                JOIN ptk_terdaftar ptkd WITH(NOLOCK) ON ptk.ptk_id = ptkd.ptk_id
                JOIN ref.tahun_ajaran ta WITH(NOLOCK) ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
                JOIN sekolah s WITH(NOLOCK) ON ptkd.sekolah_id = s.sekolah_id
                JOIN ref.mst_wilayah w1 WITH(NOLOCK) ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
                JOIN ref.mst_wilayah w2 WITH(NOLOCK) ON w1.mst_kode_wilayah = w2.kode_wilayah
                JOIN ref.mst_wilayah w3 WITH(NOLOCK) ON w2.mst_kode_wilayah = w3.kode_wilayah
                WHERE
                    ptk.Soft_delete = 0
                AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				{$params_jptk}
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND {$col_kode} = '".$requesta['kode_wilayah']."'
				{$params_bp}
				GROUP BY
                    {$col_wilayah},
                    {$col_kode},
                    {$col_id_level},
                    {$col_mst_kode},
                    s.nama,
                    s.sekolah_id,
                    s.npsn,
                    s.status_sekolah,
                    s.bentuk_pendidikan_id
				ORDER BY
					s.nama ASC";

		// return $sql;die;

		// $return = Preface::getDataByQuery($sql);

		// return json_encode($return);

		$return = Preface::getDataByQuery($sql);

		$sql .= " OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";


		$final = array();
		$final['results'] = sizeof($return);

		$return = Preface::getDataByQuery($sql);

		$final['rows'] = $return;

		return json_encode($final);
	}

	static function PtkKualifikasi(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		$jenis_ptk_id = $request->get('jenis_ptk_id');

		if($jenis_ptk_id == 1){
			$select__ = "SUM(ISNULL(guru_d3_laki_kependidikan, 0)
					+ISNULL(guru_d3_laki_non_kependidikan, 0)
					+ISNULL(guru_d3_perempuan_kependidikan, 0)
					+ISNULL(guru_d3_perempuan_non_kependidikan, 0)
					) as d3,
					SUM(ISNULL(guru_d4_laki_kependidikan, 0)
					+ISNULL(guru_d4_laki_non_kependidikan, 0)
					+ISNULL(guru_d4_perempuan_kependidikan, 0)
					+ISNULL(guru_d4_perempuan_non_kependidikan, 0)
					) as d4,
					SUM(ISNULL(guru_s1_laki_kependidikan, 0)
					+ISNULL(guru_s1_laki_non_kependidikan, 0)
					+ISNULL(guru_s1_perempuan_kependidikan, 0)
					+ISNULL(guru_s1_perempuan_non_kependidikan, 0)
					) as s1,
					SUM(ISNULL(guru_s2_laki_kependidikan, 0)
					+ISNULL(guru_s2_laki_non_kependidikan, 0)
					+ISNULL(guru_s2_perempuan_kependidikan, 0)
					+ISNULL(guru_s2_perempuan_non_kependidikan, 0)
					) as s2,
					SUM(ISNULL(guru_s3_laki_kependidikan, 0)
					+ISNULL(guru_s3_laki_non_kependidikan, 0)
					+ISNULL(guru_s3_perempuan_kependidikan, 0)
					+ISNULL(guru_s3_perempuan_non_kependidikan, 0)
					) as s3,
					sum(guru) as gtk,";
		}else if($jenis_ptk_id == 2){
			$select__ = "SUM(ISNULL(pegawai_d3_laki_kependidikan, 0)
					+ISNULL(pegawai_d3_laki_non_kependidikan, 0)
					+ISNULL(pegawai_d3_perempuan_kependidikan, 0)
					+ISNULL(pegawai_d3_perempuan_non_kependidikan, 0)) as d3,
					SUM(ISNULL(pegawai_d4_laki_kependidikan, 0)
					+ISNULL(pegawai_d4_laki_non_kependidikan, 0)
					+ISNULL(pegawai_d4_perempuan_kependidikan, 0)
					+ISNULL(pegawai_d4_perempuan_non_kependidikan, 0)) as d4,
					SUM(ISNULL(pegawai_s1_laki_kependidikan, 0)
					+ISNULL(pegawai_s1_laki_non_kependidikan, 0)
					+ISNULL(pegawai_s1_perempuan_kependidikan, 0)
					+ISNULL(pegawai_s1_perempuan_non_kependidikan, 0)) as s1,
					SUM(ISNULL(pegawai_s2_laki_kependidikan, 0)
					+ISNULL(pegawai_s2_laki_non_kependidikan, 0)
					+ISNULL(pegawai_s2_perempuan_kependidikan, 0)
					+ISNULL(pegawai_s2_perempuan_non_kependidikan, 0)) as s2,
					SUM(ISNULL(pegawai_s3_laki_kependidikan, 0)
					+ISNULL(pegawai_s3_laki_non_kependidikan, 0)
					+ISNULL(pegawai_s3_perempuan_kependidikan, 0)
					+ISNULL(pegawai_s3_perempuan_non_kependidikan, 0)) as s3,
					sum(pegawai) as gtk,";
		}else{
			$select__ = "SUM(ISNULL(guru_d3_laki_kependidikan, 0)
					+ISNULL(guru_d3_laki_non_kependidikan, 0)
					+ISNULL(guru_d3_perempuan_kependidikan, 0)
					+ISNULL(guru_d3_perempuan_non_kependidikan, 0)
					+ISNULL(pegawai_d3_laki_kependidikan, 0)
					+ISNULL(pegawai_d3_laki_non_kependidikan, 0)
					+ISNULL(pegawai_d3_perempuan_kependidikan, 0)
					+ISNULL(pegawai_d3_perempuan_non_kependidikan, 0)) as d3,
					SUM(ISNULL(guru_d4_laki_kependidikan, 0)
					+ISNULL(guru_d4_laki_non_kependidikan, 0)
					+ISNULL(guru_d4_perempuan_kependidikan, 0)
					+ISNULL(guru_d4_perempuan_non_kependidikan, 0)
					+ISNULL(pegawai_d4_laki_kependidikan, 0)
					+ISNULL(pegawai_d4_laki_non_kependidikan, 0)
					+ISNULL(pegawai_d4_perempuan_kependidikan, 0)
					+ISNULL(pegawai_d4_perempuan_non_kependidikan, 0)) as d4,
					SUM(ISNULL(guru_s1_laki_kependidikan, 0)
					+ISNULL(guru_s1_laki_non_kependidikan, 0)
					+ISNULL(guru_s1_perempuan_kependidikan, 0)
					+ISNULL(guru_s1_perempuan_non_kependidikan, 0)
					+ISNULL(pegawai_s1_laki_kependidikan, 0)
					+ISNULL(pegawai_s1_laki_non_kependidikan, 0)
					+ISNULL(pegawai_s1_perempuan_kependidikan, 0)
					+ISNULL(pegawai_s1_perempuan_non_kependidikan, 0)) as s1,
					SUM(ISNULL(guru_s2_laki_kependidikan, 0)
					+ISNULL(guru_s2_laki_non_kependidikan, 0)
					+ISNULL(guru_s2_perempuan_kependidikan, 0)
					+ISNULL(guru_s2_perempuan_non_kependidikan, 0)
					+ISNULL(pegawai_s2_laki_kependidikan, 0)
					+ISNULL(pegawai_s2_laki_non_kependidikan, 0)
					+ISNULL(pegawai_s2_perempuan_kependidikan, 0)
					+ISNULL(pegawai_s2_perempuan_non_kependidikan, 0)) as s2,
					SUM(ISNULL(guru_s3_laki_kependidikan, 0)
					+ISNULL(guru_s3_laki_non_kependidikan, 0)
					+ISNULL(guru_s3_perempuan_kependidikan, 0)
					+ISNULL(guru_s3_perempuan_non_kependidikan, 0)
					+ISNULL(pegawai_s3_laki_kependidikan, 0)
					+ISNULL(pegawai_s3_laki_non_kependidikan, 0)
					+ISNULL(pegawai_s3_perempuan_kependidikan, 0)
					+ISNULL(pegawai_s3_perempuan_non_kependidikan, 0)) as s3,
					sum(guru+pegawai) as gtk,";
		}

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_provinsi}
					{$add_kode_wilayah_induk_provinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					{$select__}
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND Soft_delete = 0
				{$param_kode_wilayah}
				{$params_bp}
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_provinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_provinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);

	}

    static function PtkKualifikasiSp(Request $request, Application $app){

        $requesta = Preface::Inisiasi($request);

        $params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

        switch ($requesta['id_level_wilayah']) {
			case "0":
				//nothing to do
				$wilayah = null;
				$params_wilayah = null;
				break;
			case "1":
				$wilayah = 'propinsi';
				$params_wilayah = 'kode_wilayah_propinsi';
				$params_mst_wilayah = 'mst_kode_wilayah_propinsi';
				$params_id_level = 'id_level_wilayah_propinsi';
				break;
			case "2":
				$wilayah = 'kabupaten';
				$params_wilayah = 'kode_wilayah_kabupaten';
				$params_mst_wilayah = 'mst_kode_wilayah_kabupaten';
				$params_id_level = 'id_level_wilayah_kabupaten';
				break;
			case "3":
				$wilayah = 'kecamatan';
				$params_wilayah = 'kode_wilayah_kecamatan';
				$params_mst_wilayah = 'mst_kode_wilayah_kecamatan';
				$params_id_level = 'id_level_wilayah_kecamatan';
				break;
			default:
				break;
		}

        $sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY nama) as 'no',
                    sekolah_id,
                    nama,
                    npsn,
                    status_sekolah,
                    CASE status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bentuk_pendidikan_id,
                    CASE 
                        WHEN bentuk_pendidikan_id = 1 THEN	'TK'
                        WHEN bentuk_pendidikan_id = 2 THEN	'KB'
                        WHEN bentuk_pendidikan_id = 3 THEN	'TPA'
                        WHEN bentuk_pendidikan_id = 4 THEN	'SPS'
                        WHEN bentuk_pendidikan_id = 5 THEN	'SD'
                        WHEN bentuk_pendidikan_id = 6 THEN	'SMP'
                        WHEN bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN bentuk_pendidikan_id = 13 THEN	'SMA'
                        WHEN bentuk_pendidikan_id = 15 THEN	'SMK'
                    ELSE '-' END AS bentuk,
					{$wilayah} as {$wilayah},
					{$params_wilayah} as kode_wilayah,
					{$params_mst_wilayah} as mst_kode_wilayah,
					{$params_id_level} as id_level_wilayah,
					SUM(ISNULL(guru_d3_laki_kependidikan, 0)
					+ISNULL(guru_d3_laki_non_kependidikan, 0)
					+ISNULL(guru_d3_perempuan_kependidikan, 0)
					+ISNULL(guru_d3_perempuan_non_kependidikan, 0)
					+ISNULL(pegawai_d3_laki_kependidikan, 0)
					+ISNULL(pegawai_d3_laki_non_kependidikan, 0)
					+ISNULL(pegawai_d3_perempuan_kependidikan, 0)
					+ISNULL(pegawai_d3_perempuan_non_kependidikan, 0)) as d3,
					SUM(ISNULL(guru_d4_laki_kependidikan, 0)
					+ISNULL(guru_d4_laki_non_kependidikan, 0)
					+ISNULL(guru_d4_perempuan_kependidikan, 0)
					+ISNULL(guru_d4_perempuan_non_kependidikan, 0)
					+ISNULL(pegawai_d4_laki_kependidikan, 0)
					+ISNULL(pegawai_d4_laki_non_kependidikan, 0)
					+ISNULL(pegawai_d4_perempuan_kependidikan, 0)
					+ISNULL(pegawai_d4_perempuan_non_kependidikan, 0)) as d4,
					SUM(ISNULL(guru_s1_laki_kependidikan, 0)
					+ISNULL(guru_s1_laki_non_kependidikan, 0)
					+ISNULL(guru_s1_perempuan_kependidikan, 0)
					+ISNULL(guru_s1_perempuan_non_kependidikan, 0)
					+ISNULL(pegawai_s1_laki_kependidikan, 0)
					+ISNULL(pegawai_s1_laki_non_kependidikan, 0)
					+ISNULL(pegawai_s1_perempuan_kependidikan, 0)
					+ISNULL(pegawai_s1_perempuan_non_kependidikan, 0)) as s1,
					SUM(ISNULL(guru_s2_laki_kependidikan, 0)
					+ISNULL(guru_s2_laki_non_kependidikan, 0)
					+ISNULL(guru_s2_perempuan_kependidikan, 0)
					+ISNULL(guru_s2_perempuan_non_kependidikan, 0)
					+ISNULL(pegawai_s2_laki_kependidikan, 0)
					+ISNULL(pegawai_s2_laki_non_kependidikan, 0)
					+ISNULL(pegawai_s2_perempuan_kependidikan, 0)
					+ISNULL(pegawai_s2_perempuan_non_kependidikan, 0)) as s2,
					SUM(ISNULL(guru_s3_laki_kependidikan, 0)
					+ISNULL(guru_s3_laki_non_kependidikan, 0)
					+ISNULL(guru_s3_perempuan_kependidikan, 0)
					+ISNULL(guru_s3_perempuan_non_kependidikan, 0)
					+ISNULL(pegawai_s3_laki_kependidikan, 0)
					+ISNULL(pegawai_s3_laki_non_kependidikan, 0)
					+ISNULL(pegawai_s3_perempuan_kependidikan, 0)
					+ISNULL(pegawai_s3_perempuan_non_kependidikan, 0)) as s3,
					sum(guru+pegawai) as gtk,
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND {$params_wilayah} = '".$requesta['kode_wilayah']."'
				AND Soft_delete = 0
				{$params_bp}
				GROUP BY
				    nama,
				    sekolah_id,
				    npsn,
				    status_sekolah,
				    bentuk_pendidikan_id,
					{$wilayah},
					{$params_wilayah},
					{$params_mst_wilayah},
					{$params_id_level}
				ORDER BY
					nama";

        // return $sql;die;

        // $return = Preface::getDataByQuery($sql, 'rekap');

        // return json_encode($return);

		$return = Preface::getDataByQuery($sql, 'rekap');

		$sql .= " OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";


		$final = array();
		$final['results'] = sizeof($return);

		$return = Preface::getDataByQuery($sql,'rekap');

		$final['rows'] = $return;

		return json_encode($final);					

    }
}