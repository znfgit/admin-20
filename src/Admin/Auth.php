<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Auth{

	static function Session(Request $request, Application $app){

		$ref = 'ref.';
		$array = array();

		$array['bentuk_pendidikan'] = BENTUKPENDIDIKAN;
		$array['tingkat'] = TINGKAT;
		$array['bosnya'] = BOSNYA;
		$array['judul'] = JUDUL;
		$array['modul_mutasi'] = MODUL_MUTASI;
		$array['modul_kualitas_data'] = MODUL_KUALITAS_DATA;
		$array['warna_header_logo'] = WARNA_HEADER_LOGO;
		$array['warna_header_toolbar'] = WARNA_HEADER_TOOLBAR;

		if(BACKGROUND_HEADER_TOOLBAR){
			$array['background_header_toolbar'] = BACKGROUND_HEADER_TOOLBAR;
		}else{
			$array['background_header_toolbar'] = null;
		}

		$array['warna_tema'] = WARNA_TEMA;
		$array['logo_filename'] = LOGO_FILENAME;

		if($app['session']->get('username')){

			$array['session'] = true;
			$array['username'] = $app['session']->get('username');
			$array['nama'] = $app['session']->get('nama');
			$array['peran_id'] = $app['session']->get('peran_id');
			$array['pengguna_id'] = $app['session']->get('pengguna_id');
			$array['id_level_wilayah'] = $app['session']->get('id_level_wilayah');
			$array['kode_wilayah'] = $app['session']->get('kode_wilayah');
			$array['sekolah_id'] = $app['session']->get('sekolah_id');
			$array['ta_berjalan'] = $app['session']->get('ta_berjalan');
			$array['tahun_ajaran_id'] = $app['session']->get('ta_berjalan');
			$array['semester_berjalan'] = $app['session']->get('semester_berjalan');

			$array['peran_id_str'] = $app['session']->get('peran_id_str');
			$array['kode_wilayah_str'] = $app['session']->get('kode_wilayah_str');
			$array['ta_berjalan_str'] = $app['session']->get('ta_berjalan_str');
			$array['semester_berjalan_str'] = $app['session']->get('semester_berjalan_str');
			$array['original'] = $app['session']->get('original');


		}else{

			// $json_session = file_get_contents(URL_SSO.'/session?tenant_id='.TENANT_ID);
			$json_session = file_get_contents(URL_SSO.'/session?tenant_id='.TENANT_ID.'&ip_address='.$_SERVER['REMOTE_ADDR']);

			// return URL_SSO.'/session?tenant_id='.TENANT_ID.'&ip_address='.$_SERVER['REMOTE_ADDR'];die;
			// return $json_session;die;

			$json_session_decode = json_decode($json_session);

			if($json_session_decode->{'akun_sama'} && $json_session_decode->{'status'}){
				$peran_id = (int)substr( $json_session_decode->{'rows'}[0]->{'pengguna'}->{'peran_id'} , 0, 1);

				$app['session']->set('username', $json_session_decode->{'rows'}[0]->{'pengguna'}->{'username'});
				$app['session']->set('nama', $json_session_decode->{'rows'}[0]->{'pengguna'}->{'nama'});
				$app['session']->set('peran_id', (int)substr( $json_session_decode->{'rows'}[0]->{'pengguna'}->{'peran_id'} , 0, 1) );
				$app['session']->set('pengguna_id', $json_session_decode->{'rows'}[0]->{'pengguna'}->{'pengguna_id'});
				$app['session']->set('kode_wilayah', trim($json_session_decode->{'rows'}[0]->{'pengguna'}->{'kode_wilayah'}));
				$app['session']->set('sekolah_id', null);
				$app['session']->set('ta_berjalan', TA_BERJALAN);
				$app['session']->set('semester_berjalan', SEMESTER_BERJALAN);
				$app['session']->set('peran_id_str', $json_session_decode->{'rows'}[0]->{'pengguna'}->{'peran_id_str'});
				$app['session']->set('login_id', $json_session_decode->{'rows'}[0]->{'login_id'});
				
				$sql_kode_wilayah = "select * from ref.mst_wilayah with(nolock) where kode_wilayah = '".$json_session_decode->{'rows'}[0]->{'pengguna'}->{'kode_wilayah'}."'";
				$return_kode_wilayah = Preface::getDataByQuery($sql_kode_wilayah);

				$app['session']->set('kode_wilayah_str', $return_kode_wilayah[0]['nama']);

				$sql_ta = "select * from ref.tahun_ajaran with(nolock) where tahun_ajaran_id = '".TA_BERJALAN."'";
				$return_ta = Preface::getDataByQuery($sql_ta);
				$app['session']->set('ta_berjalan_str', $return_ta[0]['nama']);

				$sql_semester = "select * from ref.semester with(nolock) where semester_id = '".SEMESTER_BERJALAN."'";
				$return_semester = Preface::getDataByQuery($sql_semester);
				$app['session']->set('semester_berjalan_str', $return_semester[0]['nama']);
				
				$app['session']->set('original', 2);

				if ($peran_id == 6) {
					$jenjang = "propinsi";
					$id_level_wilayah = 1;
				} else if ($peran_id == 8) {
					$jenjang = "kabupaten_kota";
					$id_level_wilayah = 2;
				} else if ($peran_id == 1) {
					$jenjang = "administrator";
					$id_level_wilayah = 0;
				}
				$app['session']->set('id_level_wilayah', $id_level_wilayah);
				$app['session']->set('jenjang', $jenjang);

				$array['session'] = true;
				$array['username'] = $app['session']->get('username');
				$array['nama'] = $app['session']->get('nama');
				$array['peran_id'] = $app['session']->get('peran_id');
				$array['pengguna_id'] = $app['session']->get('pengguna_id');
				$array['id_level_wilayah'] = $app['session']->get('id_level_wilayah');
				$array['kode_wilayah'] = $app['session']->get('kode_wilayah');
				$array['sekolah_id'] = $app['session']->get('sekolah_id');
				$array['ta_berjalan'] = $app['session']->get('ta_berjalan');
				$array['semester_berjalan'] = $app['session']->get('semester_berjalan');

				$array['peran_id_str'] = $app['session']->get('peran_id_str');
				$array['kode_wilayah_str'] = $app['session']->get('kode_wilayah_str');
				$array['ta_berjalan_str'] = $app['session']->get('ta_berjalan_str');
				$array['semester_berjalan_str'] = $app['session']->get('semester_berjalan_str');
				$array['original'] = $app['session']->get('original');	
				$array['jenjang'] = $jenjang;
				$array['login_id'] = $app['session']->get('login_id');
			
			}else{

				$array['session'] = false;
				$array['id_level_wilayah'] = ID_LEVEL_WILAYAH;
				$array['kode_wilayah'] = KODE_WILAYAH;
				$array['kode_wilayah_str'] = KODE_WILAYAH_STR;
				$array['semester_id'] = SEMESTER_BERJALAN;
				$array['semester_berjalan'] = SEMESTER_BERJALAN;
				$array['tahun_ajaran_id'] = TA_BERJALAN;

				$sql_semester = "select * from ref.semester where semester_id = '".SEMESTER_BERJALAN."'";
				$return_semester = Preface::getDataByQuery($sql_semester);

				$array['semester_berjalan_str'] = $return_semester[0]['nama'];
			}


		}

		return json_encode($array);

	}

	

	function login(Request $request, Application $app){
		// return "oke";die;

		$return = array();

		$username = ($request->get('username')) ? $request->get('username') : 0;
		$password = ($request->get('password')) ? $request->get('password') : 0;
		$semester_id = ($request->get('semester_id')) ? $request->get('semester_id') : 20162;
		$tahun_ajaran_id = substr($semester_id, 0,4);

		$username = illegal_char_remover($username);
		$password = illegal_char_remover($password);

		$sql = "select LOWER( CONVERT(VARCHAR(32), HashBytes('MD5', '".$password."'), 2) ) as iPassword,* from pengguna where username = '{$username}' AND Soft_delete = 0
			AND aktif = 1 and peran_id != 10 and peran_id != 53";

		$check = Preface::getDataByQuery($sql);

		// return json_encode($check);

		if(sizeof($check) > 0){
			// $return['success'] = true;

			if($check[0]['iPassword'] == $check[0]['password']){


				//setting up session

				$app['session']->set('username', $username);
				$app['session']->set('nama', $check[0]['nama']);
				$app['session']->set('peran_id', $check[0]['peran_id']);
				$app['session']->set('pengguna_id', $check[0]['pengguna_id']);
				$app['session']->set('kode_wilayah', $check[0]['kode_wilayah']);
				$app['session']->set('sekolah_id', $check[0]['sekolah_id']);
				$app['session']->set('ta_berjalan', $tahun_ajaran_id);
				$app['session']->set('semester_berjalan', $semester_id);

				if($check[0]['peran_id'] == 1){
					if(trim($check[0]['kode_wilayah']) != '000000'){
						// $id_level_wilayah = 0;

						$substr_kode_wilayah = substr(trim($check[0]['kode_wilayah']),2,4);

						if($substr_kode_wilayah == '0000'){
							$id_level_wilayah = 1;
						}else{
							$id_level_wilayah = 2;
						}

					}else{
						$id_level_wilayah = 0;
					}
				}else if ($check[0]['peran_id'] <= 5 && $check[0]['peran_id'] > 1 ) {
					$id_level_wilayah = 0;
				}else if($check[0]['peran_id'] == 6 || $check[0]['peran_id'] == 7 || $check[0]['peran_id'] == 54){
					$id_level_wilayah = 1;
				}else if($check[0]['peran_id'] == 8 || $check[0]['peran_id'] == 9){
					$id_level_wilayah = 2;
				}else{
					$id_level_wilayah = ID_LEVEL_WILAYAH;
					$app['session']->set('kode_wilayah', KODE_WILAYAH);
				}

				$app['session']->set('id_level_wilayah', $id_level_wilayah);

				$sql_peran = "select * from ref.peran where peran_id = ".$check[0]['peran_id'];
				$return_peran = Preface::getDataByQuery($sql_peran);

				$app['session']->set('peran_id_str', $return_peran[0]['nama']);

				$sql_kode_wilayah = "select * from ref.mst_wilayah where kode_wilayah = '".$app['session']->get('kode_wilayah')."'";
				$return_kode_wilayah = Preface::getDataByQuery($sql_kode_wilayah);

				$app['session']->set('kode_wilayah_str', $return_kode_wilayah[0]['nama']);

				if($check[0]['sekolah_id']){
					$sql_sekolah = "select * from sekolah where sekolah_id = '".$check[0]['sekolah_id']."'";
					$return_sekolah = Preface::getDataByQuery($sql_sekolah);

					$app['session']->set('sekolah_id_str', $return_sekolah[0]['nama']);
				}

				$sql_ta = "select * from ref.tahun_ajaran where tahun_ajaran_id = '".$tahun_ajaran_id."'";
				$return_ta = Preface::getDataByQuery($sql_ta);
				$app['session']->set('ta_berjalan_str', $return_ta[0]['nama']);

				$sql_semester = "select * from ref.semester where semester_id = '".$semester_id."'";
				$return_semester = Preface::getDataByQuery($sql_semester);
				$app['session']->set('semester_berjalan_str', $return_semester[0]['nama']);

				$app['session']->set('original', 1);

				//enf of setting up session

				$return['success'] = true;
				$return['message'] = 'Selamat Datang, '.$check[0]['nama'];

				return $app->redirect('.');

			}else{

				$return['success'] = false;
				$return['message'] = 'Password Salah. Mohon coba lagi';

				return $app->redirect('login#password_salah');
			}
		}else{
			$return['success'] = false;
			$return['message'] = 'Username '.$username.' tidak terdaftar di Dapodik. Mohon masukkan username lain atau masuk menggunakan akun SDM/Helpdesk';

			return $app->redirect('login#username_tidak_terdaftar');
		}

		// return json_encode($return);


	}

	function tesHelpdesk(){
		$rt = file_get_contents('http://118.98.233.178/getHashMatch?raw=123&hash=$2y$10$hL2QaKb1/PJodWYcg839UupZqPz1.cSOK6moJNc07HVLHFkXYE8nS');

		return $rt;
	}

	function logout(Request $request, Application $app){
		$original = $app['session']->get('original');
		$login_id = $app['session']->get('login_id');

		$app['session']->clear();

		if($original == 2){
			return $app->redirect(URL_SSO.'/prosesLogout?login_id='.$login_id);
		}else{
			return $app->redirect('.');
		}

		// return $app->redirect('.');
	}

	function getSSO(Request $request, Application $app){
		$tokenKey = $request->get('tokenKey');

		$json = file_get_contents('http://sso.data.kemdikbud.go.id/service/sso.svc/getUsername?tokenKey='.$tokenKey);

		$json_dec = json_decode($json);

		$datanya = $json_dec->{'d'};

		if($datanya->{'username'}){
			$app['session']->set('username', $datanya->{'username'});
			$app['session']->set('nama', $datanya->{'nama'});

			if($datanya->{'roleName'} == 'Pusat'){
				$peran_id = 1;
				$id_level_wilayah = 0;
			}else if($datanya->{'roleName'} == 'Provinsi' || $datanya->{'roleName'} == 'Propinsi'){
				$peran_id = 6;
				$id_level_wilayah = 1;
			}else if($datanya->{'roleName'} == 'Kabupaten'){
				$peran_id = 8;
				$id_level_wilayah = 2;
			}else{
				$peran_id = 10;
				$id_level_wilayah = null;
			}

			$app['session']->set('peran_id', $peran_id);
			$app['session']->set('pengguna_id', '');
			$app['session']->set('kode_wilayah', $datanya->{'wilayah_akses'});
			$app['session']->set('sekolah_id', $datanya->{'sekolah_id'});
			$app['session']->set('ta_berjalan', TA_BERJALAN);
			$app['session']->set('semester_berjalan', SEMESTER_BERJALAN);


			$app['session']->set('id_level_wilayah', $id_level_wilayah);

			$sql_peran = "select * from ref.peran where peran_id = ".$peran_id;
			$return_peran = Preface::getDataByQuery($sql_peran);

			$app['session']->set('peran_id_str', $return_peran[0]['nama']);

			$sql_kode_wilayah = "select * from ref.mst_wilayah where kode_wilayah = '".$datanya->{'wilayah_akses'}."'";
			$return_kode_wilayah = Preface::getDataByQuery($sql_kode_wilayah);

			$app['session']->set('kode_wilayah_str', $return_kode_wilayah[0]['nama']);

			if($datanya->{'sekolah_id'}){
				$sql_sekolah = "select * from sekolah where sekolah_id = '".$datanya->{'sekolah_id'}."'";
				$return_sekolah = Preface::getDataByQuery($sql_sekolah);

				$app['session']->set('sekolah_id_str', $return_sekolah[0]['nama']);
			}

			$sql_ta = "select * from ref.tahun_ajaran where tahun_ajaran_id = '".TA_BERJALAN."'";
			$return_ta = Preface::getDataByQuery($sql_ta);
			$app['session']->set('ta_berjalan_str', $return_ta[0]['nama']);

			$sql_semester = "select * from ref.semester where semester_id = '".SEMESTER_BERJALAN."'";
			$return_semester = Preface::getDataByQuery($sql_semester);
			$app['session']->set('semester_berjalan_str', $return_semester[0]['nama']);


			$app['session']->set('original', 0);

			return true;
		}else{
			return false;
		}

	}

	static function getHash(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		$sql = "select lOWER( CONVERT(VARCHAR(32), HashBytes('MD5', '{$requesta['keyword']}'), 2) ) as hash";

		$return = Preface::getDataByQuery($sql);

		return $return[0]['hash'];
	}

	static function UbahPassword(Request $request, Application $app){
		$pengguna_id = $request->get('pengguna_id');
		$password_lama = $request->get('password_lama');
		$password_baru = $request->get('password_baru');

		$count = 0;

		$sql = "select lOWER( CONVERT(VARCHAR(32), HashBytes('MD5', '".$password_lama."'), 2) ) as iPassword,* from pengguna where pengguna_id = '{$pengguna_id}'";

		$fetch = getDataBySql($sql);

		foreach ($fetch as $f) {
			$count++;
		}

		if($count > 0){
			if( $fetch[0]['iPassword'] ==  $fetch[0]['password'] ) {

				$sql = "update pengguna set
						password = lOWER( CONVERT(VARCHAR(32), HashBytes('MD5', '{$password_baru}'), 2) ),
						last_update = '".date('Y-m-d H:i:s')."'
					where pengguna_id = '".$fetch[0]['pengguna_id']."'";


				$con = \Propel::getConnection(DBNAME);
				$stmt = $con->prepare($sql);
				$stmt->execute();

				if($stmt){
					$return = array();
					$return['success'] = true;
					$return['message'] = 'Berhasil memperbarui password Pengguna';
				}else{
					$return = array();
					$return['success'] = false;
					$return['message'] = 'Gagal memperbarui password Pengguna';
				}

			} else{

				$return = array();
				$return['success'] = false;
				$return['message'] = 'Password Lama tidak sesuai!';
			}
		}else{

			$return = array();
			$return['success'] = false;
			$return['message'] = 'Pengguna tidak ditemukan!';
		}

		return json_encode($return);

	}

}