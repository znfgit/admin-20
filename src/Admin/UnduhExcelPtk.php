<?php
/**
 * Created by PhpStorm.
 * User: yadi
 * Date: 02/02/2017
 * Time: 11:14
 */

namespace Admin;


use Admin\Model\MstWilayahPeer;
use Admin\Model\PenggunaPeer;
use Admin\Model\PtkPeer;
use Admin\Model\PtkTerdaftarPeer;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class UnduhExcelPtk
{
    public function getProfilPtk(Request $request, Application $app){
//        $requesta = Preface::Inisiasi($request);

        $pengguna_id = $app['session']->get('pengguna_id');

        $ptk_id = $request->get('ptk_id');
//        echo $ptk_id; die;
        $tahun_id = TA_BERJALAN;
        $today = date('d-m-Y H:i:s');

        $ptk = PtkPeer::retrieveByPK($ptk_id);


        $sql = "
        SELECT
            TOP 1
            p.nama as nama,
            s.nama as nama_sekolah,
            s.nss as nss,
            s.npsn as npsn,
            s.alamat_jalan as alamat_sekolah,
            w.kode_wilayah as kode_wilayah_kecamatan,
            w.nama as kecamatan,
            a.nama as agama,
            n.nama as negara,
            j.nama as pekerjaan,
            k.nama as status_kepegawaian,
            jp.jenis_ptk as jenis_ptk,
            l.nama as lembaga_pengangkat,
            sg.nama as sumber_gaji,
            t.nomor_surat_tugas as nomor_surat_tugas,
            t.tanggal_surat_tugas as tanggal_surat_tugas,
            t.tmt_tugas as tmt_tugas,
            t.ptk_induk as sekolah_induk,
            pg.nama as pangkat_golongan,
            kl.nama as keahlian_lab
        FROM
            ptk p
        LEFT JOIN
            ptk_terdaftar t 
        ON t.ptk_id = p.ptk_id
        LEFT JOIN
            sekolah s 
        ON s.sekolah_id = t.sekolah_id
        LEFT JOIN
            ref.mst_wilayah w
        ON p.kode_wilayah = w.kode_wilayah
        LEFT JOIN
            ref.agama a
        ON p.agama_id = a.agama_id
        LEFT JOIN
            ref.negara n
        ON p.kewarganegaraan = n.negara_id
        LEFT JOIN
            ref.pekerjaan j
        ON p.pekerjaan_suami_istri = j.pekerjaan_id
        LEFT JOIN
            ref.status_kepegawaian k
        ON p.status_kepegawaian_id = k.status_kepegawaian_id
        LEFT JOIN
            ref.jenis_ptk jp
        ON p.jenis_ptk_id = jp.jenis_ptk_id
        LEFT JOIN
            ref.lembaga_pengangkat l 
        ON p.lembaga_pengangkat_id = l.lembaga_pengangkat_id
        LEFT JOIN
            ref.sumber_gaji sg
        ON p.sumber_gaji_id = sg.sumber_gaji_id
        LEFT JOIN
            ref.pangkat_golongan pg
        ON p.pangkat_golongan_id = pg.pangkat_golongan_id
        LEFT JOIN
            ref.keahlian_laboratorium kl
        ON p.keahlian_laboratorium_id = kl.keahlian_laboratorium_id
        WHERE
            p.ptk_id = '".$ptk_id."'
        ORDER BY 
            t.tanggal_surat_tugas DESC
        ";

        $profils = getDataBySql($sql);
        $profil = $profils[0];
//        print_r($profil['nama_sekolah']);die;

        $pengguna = PenggunaPeer::retrieveByPK($pengguna_id);
//        print_r($ptk); die;

        $objPHPExcel = \PHPExcel_IOFactory::load(__DIR__."/Templates/template-formulir-ptk.xlsx");

//        Set Document Properties
        $objPHPExcel->getProperties()->setCreator("Profil PTK")
                                    ->setLastModifiedBy("Profil PTK")
                                    ->setTitle("Formulir PTK")
                                    ->setSubject("Office 2007 XLSX Test Document")
                                    ->setDescription("Formulir PTK")
                                    ->setKeywords("Formulir PTK")
                                    ->setCategory("formulir");

//        Rename Worksheet
        $clearingName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\]]|[\.]{2,})", '',  $ptk->getNama());
        $clearingName = substr($clearingName, 0, 20);
//        print_r($clearingName); die;
        $objPHPExcel->setActiveSheetIndex(0)->setTitle('Profil '.$clearingName);

//        Data Utama
        $tanggal_sekarang = substr($today, 0, 2);
        $bulan_sekarang =  substr($today, 3, 2);
        $tahun_sekarang = substr($today, 6, 4);


        $sql_penugasan = "
        SELECT
            s.nama as nama_sekolah,
            t.tahun_ajaran_id as tahun_ajaran,
            t.nomor_surat_tugas as nomor_surat_tugas,
            t.tanggal_surat_tugas as tanggal_surat_tugas,
            t.tmt_tugas as tmt_tugas,
            t.ptk_induk as ptk_induk
        FROM
            ptk_terdaftar t
        JOIN
            sekolah s
        ON t.sekolah_id = s.sekolah_id
        WHERE
            ptk_id = '".$ptk_id."'
        ";
        $penugasans = getDataBySql($sql_penugasan);
        $n = 3;
        foreach ($penugasans as $penugasan){
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('A'.$n, $penugasan['nama_sekolah'] );
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('B'.$n, $penugasan['tahun_ajaran'] );
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('C'.$n, $penugasan['nomor_surat_tugas'] );
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('D'.$n, $penugasan['tanggal_surat_tugas'] );
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('E'.$n, $penugasan['tmt_tugas'] );
            if($penugasan['ptk_induk'] == '1'){
                $ptkInduk = 'Ya';
            }else{
                $ptkInduk = 'Tidak';
            }
            $objPHPExcel->setActiveSheetIndex(1)->setCellValue('F'.$n, $ptkInduk );
            $n++;
        }

        switch ($ptk->getJenisKelamin()){
            case 'L':
                $jenis_kelamin = "Laki-Laki";
                break;
            default :
                $jenis_kelamin = "Perempuan";
                break;
        }

        switch ($ptk->getStatusPerkawinan()) {
            case '1':
                $kawin = 'Kawin';
                break;
            case '2':
                $kawin = 'Belum Kawin';
                # code...
                break;
            case '3':
                $kawin = 'Janda/Duda';
                # code...
                break;
            default:
                $kawin = 'Belum Kawin';
                # code...
                break;
        }

        switch ($ptk->getPekerjaanSuamiIstri()) {
            case '5':
                $nip_suami_istri = $ptk->getNipSuamiIstri();
                break;
            default:
                $nip_suami_istri = ' ';
                break;
        }
        switch ($ptk->getStatusKepegawaianId()) {
            case '1':
                $nip = $ptk->getNip();
                break;
            default:
                $nip = ' ';
                break;
        }

        if($ptk->getNiyNigk() != NULL){
            $niynigk = $ptk->getNiyNigk();
        }else {
            $niynigk = ' ';
        }


        if($ptk->getNuptk() != NULL){
            $nuptk = $ptk->getNuptk();
        }else {
            $nuptk = ' ';
        }


        switch ($ptk->getStatusKeaktifanId()){
            case '1':
                $status_keaktifan = "Aktif";
                break;
            case '2':
                $status_keaktifan = "Tidak Aktif";
                break;
            case '99':
                $status_keaktifan = "Lainnya";
                break;
            default :
                $status_keaktifan = "  ";
                break;
        }

        switch ($profil['sekolah_induk']){
            case '1':
                $sekolah_induk = "Ya";
                break;
            default :
                $sekolah_induk = "Tidak";
        }

        switch ($ptk->getSudahLisensiKepalaSekolah()) {
            case 1:
                # code...
                $lisensikepsek = 'Ya';
                break;
            default:
                $lisensikepsek = 'Tidak';
                # code...
                break;
        }

        switch ($ptk->getPernahDiklatKepengawasan()) {
            case 1:
                # code...
                $diklat_kepengawasan = 'Ya';
                break;
            default:
                $diklat_kepengawasan = 'Tidak';
                # code...
                break;
        }

        switch ($ptk->getKeahlianBraille()) {
            case 1:
                # code...
                $braille = 'Ya';
                break;
            default:
                $braille = 'Tidak';
                # code...
                break;
        }

        switch ($ptk->getKeahlianBhsIsyarat()) {
            case 1:
                # code...
                $isyarat = 'Ya';
                break;
            default:
                $isyarat = 'Tidak';
                # code...
                break;
        }

        switch ($ptk->getKeahlianLaboratoriumId()) {
            case 99:
                $keahlian_lab = 'Tidak Ada';
                break;
            default:
                $keahlian_lab = $profil['keahlian_lab'];
        }

//        echo $keahlian_lab;die;

        $kecamatan = MstWilayahPeer::retrieveByPK($profil['kode_wilayah_kecamatan']);
        $kabupaten = MstWilayahPeer::retrieveByPK($kecamatan->getMstKodeWilayah());
        $provinsi = MstWilayahPeer::retrieveByPK($kabupaten->getMstKodeWilayah());
//        die;


//        Input Excel
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E3', $tanggal_sekarang);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H3', $bulan_sekarang);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K3', $tahun_sekarang);
//        A. Identitas Sekolah
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J7', $profil['nama_sekolah']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J9', $profil['nss']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z9', $profil['npsn']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J11', $profil['alamat_sekolah']);
//        A. Identitas Pendidik dan Tenaga Kependidikan
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J15', $profil['nama']);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J19', $ptk->getNik());
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J21', $jenis_kelamin);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J23', $ptk->getTempatLahir());
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AE23', substr($ptk->getTanggalLahir(), 8, 1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF23', substr($ptk->getTanggalLahir(), 9, 1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH23', substr($ptk->getTanggalLahir(), 5, 1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AI23', substr($ptk->getTanggalLahir(), 6, 1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AK23', substr($ptk->getTanggalLahir(), 0, 1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL23', substr($ptk->getTanggalLahir(), 1, 1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AM23', substr($ptk->getTanggalLahir(), 2, 1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AN23', substr($ptk->getTanggalLahir(), 3, 1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J25', $ptk->getNamaIbuKandung());
//        Data Pribadi
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J29', $ptk->getAlamatJalan());
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J31', $ptk->getNamaDusun());
        $rt = strlen($ptk->getRt());
        switch ($rt){
            case 1 :
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH31', substr($ptk->getRt(), 0,1));
                break;
            case 2 :
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH31', substr($ptk->getRt(), 0,1));
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AI31', substr($ptk->getRt(), 1,1));
                break;
            default:
                break;
        }
        $rw = strlen($ptk->getRw());
        switch ($rw){
            case 1 :
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL31', substr($ptk->getRw(), 0,1));
                break;
            case 2 :
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL31', substr($ptk->getRw(), 0,1));
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AM31', substr($ptk->getRw(), 1,1));
                break;
            default:
                break;
        }
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J33', $ptk->getDesaKelurahan());
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AI33', substr($ptk->getKodePos(), 0,1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AJ33', substr($ptk->getKodePos(), 1,1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AK33', substr($ptk->getKodePos(), 2,1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL33', substr($ptk->getKodePos(), 3,1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AM33', substr($ptk->getKodePos(), 4,1));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J35', $kecamatan->getNama() );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J37', $kabupaten->getNama() );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J39', $provinsi->getNama() );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J41', $profil['agama'] );
        if($ptk->getNpwp() != NULL){
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J43', substr($ptk->getNpwp(), 0,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K43', substr($ptk->getNpwp(), 1,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M43', substr($ptk->getNpwp(), 2,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N43', substr($ptk->getNpwp(), 3,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O43', substr($ptk->getNpwp(), 4,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q43', substr($ptk->getNpwp(), 5,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R43', substr($ptk->getNpwp(), 6,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S43', substr($ptk->getNpwp(), 7,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U43', substr($ptk->getNpwp(), 8,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W43', substr($ptk->getNpwp(), 9,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('X43', substr($ptk->getNpwp(), 10,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y43', substr($ptk->getNpwp(), 11,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AA43', substr($ptk->getNpwp(), 12,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB43', substr($ptk->getNpwp(), 13,1 ));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AC43', substr($ptk->getNpwp(), 14,1 ));
        }else{}
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J45', $ptk->getNmWp() );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J47', $profil['negara'] );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J49', $kawin );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J51', $ptk->getNamaSuamiIstri() );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J53', $profil['pekerjaan'] );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J55', $nip_suami_istri );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J59', $profil['status_kepegawaian'] );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J61', $nip );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J63', $niynigk );
//        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J65', nigb );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J67', $nuptk );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J69', $profil['jenis_ptk'] );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J71', $status_keaktifan );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J73', $ptk->getSkPengangkatan() );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J75', substr( $ptk->getTmtPengangkatan(), 8,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K75', substr( $ptk->getTmtPengangkatan(), 9,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M75', substr( $ptk->getTmtPengangkatan(), 5,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N75', substr( $ptk->getTmtPengangkatan(), 6,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P75', substr( $ptk->getTmtPengangkatan(), 0,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q75', substr( $ptk->getTmtPengangkatan(), 1,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R75', substr( $ptk->getTmtPengangkatan(), 2,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S75', substr( $ptk->getTmtPengangkatan(), 3,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J77', $profil['lembaga_pengangkat'] );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J79', $ptk->getSkCpns() );
//        tmt cpns Belum gan
        if( $ptk->getTmtPns() != NULL ){
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J83', substr( $ptk->getTmtPns(), 8,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K83', substr( $ptk->getTmtPns(), 9,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M83', substr( $ptk->getTmtPns(), 5,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N83', substr( $ptk->getTmtPns(), 6,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P83', substr( $ptk->getTmtPns(), 0,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q83', substr( $ptk->getTmtPns(), 1,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R83', substr( $ptk->getTmtPns(), 2,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S83', substr( $ptk->getTmtPns(), 3,1) );
        }else{}
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J85', $profil['pangkat_golongan'] );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J87', $profil['sumber_gaji'] );
//        Kartu PegawaiBelum
//        Kartu Pasangan Belum
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J96', $profil['nomor_surat_tugas'] );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J98', substr( $profil['tanggal_surat_tugas'], 8,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K98', substr( $profil['tanggal_surat_tugas'], 9,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M98', substr( $profil['tanggal_surat_tugas'], 5,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N98', substr( $profil['tanggal_surat_tugas'], 6,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P98', substr( $profil['tanggal_surat_tugas'], 0,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q98', substr( $profil['tanggal_surat_tugas'], 1,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R98', substr( $profil['tanggal_surat_tugas'], 2,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S98', substr( $profil['tanggal_surat_tugas'], 3,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J100', substr( $profil['tmt_tugas'], 8,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K100', substr( $profil['tmt_tugas'], 9,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M100', substr( $profil['tmt_tugas'], 5,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N100', substr( $profil['tmt_tugas'], 6,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P100', substr( $profil['tmt_tugas'], 0,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q100', substr( $profil['tmt_tugas'], 1,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R100', substr( $profil['tmt_tugas'], 2,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S100', substr( $profil['tmt_tugas'], 3,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J102', $sekolah_induk );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K108', $lisensikepsek );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K112', $keahlian_lab );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D123', $braille );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I123', $isyarat );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I127', substr($ptk->getNoHp(), 0,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J127', substr($ptk->getNoHp(), 1,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K127', substr($ptk->getNoHp(), 2,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L127', substr($ptk->getNoHp(), 3,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N127', substr($ptk->getNoHp(), 4,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O127', substr($ptk->getNoHp(), 5,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P127', substr($ptk->getNoHp(), 6,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q127', substr($ptk->getNoHp(), 7,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R127', substr($ptk->getNoHp(), 8,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S127', substr($ptk->getNoHp(), 9,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T127', substr($ptk->getNoHp(), 10,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U127', substr($ptk->getNoHp(), 11,1) );
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I129', $ptk->getEmail() );


        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Profil PTK '.$profil['nama'].' ('.$today.').xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

        return exit;






    }
}