<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Admin\Preface;
use Admin\Model\PesertaDidikPeer;
use Admin\Model\RegistrasiPesertaDidikPeer;
use Admin\Model\AnggotaRombelPeer;
use Admin\Model\RombonganBelajarPeer;
use Admin\Model\SekolahPeer;

class PesertaDidik
{
	static function PdPerJurusan(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		$sql = "SELECT
					jsp.nama_jurusan_sp as desk,
					SUM(1) as jumlah
				FROM
					peserta_didik pd
				JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
				JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
				JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
				JOIN ref.semester sm ON sm.semester_id = rb.semester_id
				JOIN jurusan_sp jsp on jsp.jurusan_sp_id = rb.jurusan_sp_id
				WHERE
					rpd.Soft_delete = 0
				AND pd.Soft_delete = 0
				AND (rpd.jenis_keluar_id is null
					OR rpd.tanggal_keluar > sm.tanggal_selesai
					OR rpd.jenis_keluar_id = '1'
				)
				AND sm.semester_id = '{$requesta['semester_id']}'
				AND rpd.sekolah_id = '{$requesta['sekolah_id']}'
				AND rb.soft_delete = 0
				AND ar.soft_delete = 0
				AND rb.jenis_rombel = 1
				group by jsp.nama_jurusan_sp";

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

	static function PesertaDidik(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		$arrAll = array();
		$return = array();

		$c = new \Criteria();
		$c->addJoin(PesertaDidikPeer::PESERTA_DIDIK_ID, RegistrasiPesertaDidikPeer::PESERTA_DIDIK_ID, \Criteria::JOIN);
		$c->addJoin(PesertaDidikPeer::PESERTA_DIDIK_ID, AnggotaRombelPeer::PESERTA_DIDIK_ID, \Criteria::JOIN);
		$c->addJoin(AnggotaRombelPeer::ROMBONGAN_BELAJAR_ID, RombonganBelajarPeer::ROMBONGAN_BELAJAR_ID, \Criteria::JOIN);

		if($requesta['sekolah_id']){
			// $c->add(PesertaDidikPeer::SEKOLAH_ID, $requesta['sekolah_id']);
			$c->add(RegistrasiPesertaDidikPeer::SEKOLAH_ID, $requesta['sekolah_id']);
		}

		if($requesta['query']){
			$c->add(PesertaDidikPeer::NAMA, $requesta['query'], \Criteria::LIKE);
		}

		$c->add(RegistrasiPesertaDidikPeer::JENIS_KELUAR_ID, null, \Criteria::ISNULL);
		$c->add(RombonganBelajarPeer::JENIS_ROMBEL, 1);
		$c->add(RombonganBelajarPeer::SEMESTER_ID, $requesta['semester_id']);
		$c->add(PesertaDidikPeer::SOFT_DELETE, 0);
		$c->add(RegistrasiPesertaDidikPeer::SOFT_DELETE, 0);
		$c->add(AnggotaRombelPeer::SOFT_DELETE, 0);
		$c->add(RombonganBelajarPeer::SOFT_DELETE, 0);

		$count = PesertaDidikPeer::doCount($c);

		$c->setOffset($requesta['start']);
		$c->setLimit($requesta['limit']);

		$pds = PesertaDidikPeer::doSelect($c);

		foreach ($pds as $pd) {
			$arr = array();

			$arr = $pd->toArray(\BasePeer::TYPE_FIELDNAME);

			// $arr['sekolah_id_str'] = SekolahPeer::retrieveByPk($arr['sekolah_id']);

			array_push($arrAll, $arr);
		}

		$return['success'] = true;
		$return['results'] = $count;
		$return['rows'] = $arrAll;

		return json_encode($return);

	}

	static function PdRingkasan(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_propinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_propinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_propinsi = "propinsi,";
				$add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_propinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_propinsi = "propinsi,";
				$add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_propinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_propinsi = "propinsi,";
				$add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_propinsi}
					{$add_kode_wilayah_induk_propinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					SUM( CASE WHEN status_sekolah = 1 then pd else 0 end ) as pd_negeri,
					SUM( CASE WHEN status_sekolah = 2 then pd else 0 end ) as pd_swasta,
					SUM( pd_kelas_1_laki + pd_kelas_1_perempuan ) as pd_kelas_1,
					SUM( pd_kelas_2_laki + pd_kelas_2_perempuan ) as pd_kelas_2,
					SUM( pd_kelas_3_laki + pd_kelas_3_perempuan ) as pd_kelas_3,
					SUM( pd_kelas_4_laki + pd_kelas_4_perempuan ) as pd_kelas_4,
					SUM( pd_kelas_5_laki + pd_kelas_5_perempuan ) as pd_kelas_5,
					SUM( pd_kelas_6_laki + pd_kelas_6_perempuan ) as pd_kelas_6,
					SUM( pd_kelas_7_laki + pd_kelas_7_perempuan ) as pd_kelas_7,
					SUM( pd_kelas_8_laki + pd_kelas_8_perempuan ) as pd_kelas_8,
					SUM( pd_kelas_9_laki + pd_kelas_9_perempuan ) as pd_kelas_9,
					SUM( pd_kelas_10_laki + pd_kelas_10_perempuan ) as pd_kelas_10,
					SUM( pd_kelas_11_laki + pd_kelas_11_perempuan ) as pd_kelas_11,
					SUM( pd_kelas_12_laki + pd_kelas_12_perempuan ) as pd_kelas_12,
					SUM( pd_kelas_13_laki + pd_kelas_13_perempuan ) as pd_kelas_13,
					SUM(1) as jumlah_sekolah,
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah WITH (NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				{$param_kode_wilayah}
				{$params_bp}
				AND soft_delete = 0
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_propinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_propinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);

	}

	static function PdRingkasanSp(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				//nothing to do
				$wilayah = null;
				$params_wilayah = null;
				break;
			case "1":
				$wilayah = 'propinsi';
				$params_wilayah = 'kode_wilayah_propinsi';
				$params_mst_wilayah = 'mst_kode_wilayah_propinsi';
				$params_id_level = 'id_level_wilayah_propinsi';
				break;
			case "2":
				$wilayah = 'kabupaten';
				$params_wilayah = 'kode_wilayah_kabupaten';
				$params_mst_wilayah = 'mst_kode_wilayah_kabupaten';
				$params_id_level = 'id_level_wilayah_kabupaten';
				break;
			case "3":
				$wilayah = 'kecamatan';
				$params_wilayah = 'kode_wilayah_kecamatan';
				$params_mst_wilayah = 'mst_kode_wilayah_kecamatan';
				$params_id_level = 'id_level_wilayah_kecamatan';
				break;
			default:
				// $wilayah = null;
				// $params_wilayah = null;
				break;
		}

		$sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY nama) as 'no',
					sekolah_id,
					nama,
					npsn,
                    status_sekolah,
                    CASE status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bentuk_pendidikan_id,
                    CASE 
                        WHEN bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
                    {$wilayah},
					{$params_wilayah} AS kode_wilayah,
					{$params_mst_wilayah} AS mst_kode_wilayah,
					{$params_id_level} AS id_level_wilayah,
					SUM(pd) AS pd,
					SUM( pd_kelas_1_laki + pd_kelas_1_perempuan ) as pd_kelas_1,
					SUM( pd_kelas_2_laki + pd_kelas_2_perempuan ) as pd_kelas_2,
					SUM( pd_kelas_3_laki + pd_kelas_3_perempuan ) as pd_kelas_3,
					SUM( pd_kelas_4_laki + pd_kelas_4_perempuan ) as pd_kelas_4,
					SUM( pd_kelas_5_laki + pd_kelas_5_perempuan ) as pd_kelas_5,
					SUM( pd_kelas_6_laki + pd_kelas_6_perempuan ) as pd_kelas_6,
					SUM( pd_kelas_7_laki + pd_kelas_7_perempuan ) as pd_kelas_7,
					SUM( pd_kelas_8_laki + pd_kelas_8_perempuan ) as pd_kelas_8,
					SUM( pd_kelas_9_laki + pd_kelas_9_perempuan ) as pd_kelas_9,
					SUM( pd_kelas_10_laki + pd_kelas_10_perempuan ) as pd_kelas_10,
					SUM( pd_kelas_11_laki + pd_kelas_11_perempuan ) as pd_kelas_11,
					SUM( pd_kelas_12_laki + pd_kelas_12_perempuan ) as pd_kelas_12,
					SUM( pd_kelas_13_laki + pd_kelas_13_perempuan ) as pd_kelas_13,
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah WITH(NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND {$params_wilayah} = '".$requesta['kode_wilayah']."'
				{$params_bp}
				AND soft_delete = 0
				GROUP BY
                    nama,
                    sekolah_id,
                    npsn,
                    status_sekolah,
                    bentuk_pendidikan_id,
                    {$wilayah},
                    {$params_wilayah},
                    {$params_mst_wilayah},
                    {$params_id_level}
				ORDER BY
					nama";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		$sql .= " OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";


		$final = array();
		$final['results'] = sizeof($return);

		$return = Preface::getDataByQuery($sql,'rekap');

		$final['rows'] = $return;

		return json_encode($final);
	}

    static function PdTingkat(Request $request, Application $app){
        $requesta = Preface::Inisiasi($request);

        $status_sekolah = $request->get('status_sekolah');

        switch ($requesta['id_level_wilayah']) {
            case "0":
                $params = 'propinsi';
                $mst_kode_wilayah_induk = '';
                $mst_kode_wilayah_induk_group = '';
                $param_kode_wilayah = '';

                $add_induk_propinsi = "null as induk_propinsi,";
                $add_kode_wilayah_induk_propinsi = "null as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

                $add_group_induk_propinsi = "propinsi,";
                $add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                break;
            case "1":
                $params = 'kabupaten';
                $mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
                $mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
                $param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

                $add_induk_propinsi = "propinsi as induk_propinsi,";
                $add_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

                $add_group_induk_propinsi = "propinsi,";
                $add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                break;
            case "2":
                $params = 'kecamatan';
                $mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
                $mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
                $param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

                $add_induk_propinsi = "propinsi as induk_propinsi,";
                $add_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

                $add_group_induk_propinsi = "propinsi,";
                $add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
                break;
            default:
                break;
        }

        $params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

        if($status_sekolah == 1){
        	$param_status = 'status_sekolah in(1)';
        }else if($status_sekolah == 2){
        	$param_status = 'status_sekolah in(2)';
        }else{
        	$param_status = 'status_sekolah in(1,2)';
        }


        $sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_propinsi}
					{$add_kode_wilayah_induk_propinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					SUM( CASE WHEN {$param_status} then pd_kelas_1_laki else 0 end ) as pd_kelas_1_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_1_perempuan else 0 end ) as pd_kelas_1_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_1_laki + pd_kelas_1_perempuan) else 0 end ) as pd_kelas_1_total,
					SUM( CASE WHEN {$param_status} then pd_kelas_2_laki else 0 end ) as pd_kelas_2_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_2_perempuan else 0 end ) as pd_kelas_2_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_2_laki + pd_kelas_2_perempuan) else 0 end ) as pd_kelas_2_total,
					SUM( CASE WHEN {$param_status} then pd_kelas_3_laki else 0 end ) as pd_kelas_3_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_3_perempuan else 0 end ) as pd_kelas_3_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_3_laki + pd_kelas_3_perempuan) else 0 end ) as pd_kelas_3_total,
					SUM( CASE WHEN {$param_status} then pd_kelas_4_laki else 0 end ) as pd_kelas_4_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_4_perempuan else 0 end ) as pd_kelas_4_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_4_laki + pd_kelas_4_perempuan) else 0 end ) as pd_kelas_4_total,
					SUM( CASE WHEN {$param_status} then pd_kelas_5_laki else 0 end ) as pd_kelas_5_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_5_perempuan else 0 end ) as pd_kelas_5_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_5_laki + pd_kelas_5_perempuan) else 0 end ) as pd_kelas_5_total,
					SUM( CASE WHEN {$param_status} then pd_kelas_6_laki else 0 end ) as pd_kelas_6_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_6_perempuan else 0 end ) as pd_kelas_6_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_6_laki + pd_kelas_6_perempuan) else 0 end ) as pd_kelas_6_total,
					SUM( CASE WHEN {$param_status} then pd_kelas_7_laki else 0 end ) as pd_kelas_7_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_7_perempuan else 0 end ) as pd_kelas_7_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_7_laki + pd_kelas_7_perempuan) else 0 end ) as pd_kelas_7_total,
					SUM( CASE WHEN {$param_status} then pd_kelas_8_laki else 0 end ) as pd_kelas_8_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_8_perempuan else 0 end ) as pd_kelas_8_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_8_laki + pd_kelas_8_perempuan) else 0 end ) as pd_kelas_8_total,
					SUM( CASE WHEN {$param_status} then pd_kelas_9_laki else 0 end ) as pd_kelas_9_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_9_perempuan else 0 end ) as pd_kelas_9_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_9_laki + pd_kelas_9_perempuan) else 0 end ) as pd_kelas_9_total,
					SUM( CASE WHEN {$param_status} then pd_kelas_10_laki else 0 end ) as pd_kelas_10_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_10_perempuan else 0 end ) as pd_kelas_10_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_10_laki + pd_kelas_10_perempuan) else 0 end ) as pd_kelas_10_total,
					SUM( CASE WHEN {$param_status} then pd_kelas_11_laki else 0 end ) as pd_kelas_11_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_11_perempuan else 0 end ) as pd_kelas_11_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_11_laki + pd_kelas_11_perempuan) else 0 end ) as pd_kelas_11_total,
					SUM( CASE WHEN {$param_status} then pd_kelas_12_laki else 0 end ) as pd_kelas_12_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_12_perempuan else 0 end ) as pd_kelas_12_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_12_laki + pd_kelas_12_perempuan) else 0 end ) as pd_kelas_12_total,
					SUM( CASE WHEN {$param_status} then pd_kelas_13_laki else 0 end ) as pd_kelas_13_laki,
					SUM( CASE WHEN {$param_status} then pd_kelas_13_perempuan else 0 end ) as pd_kelas_13_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_kelas_13_laki + pd_kelas_13_perempuan) else 0 end ) as pd_kelas_13_total,
					SUM( CASE WHEN {$param_status} then pd_laki else 0 end ) as pd_total_laki,
					SUM( CASE WHEN {$param_status} then pd_perempuan else 0 end ) as pd_total_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_laki + pd_perempuan) else 0 end ) as pd_total,

					SUM(1) as jumlah_sekolah
				FROM
					rekap_sekolah WITH (NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND soft_delete = 0
				{$param_kode_wilayah}
				{$params_bp}
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_propinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_propinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

        // return $sql;die;

        $return = Preface::getDataByQuery($sql, 'rekap');

        return json_encode($return);

    }

	static function PdTingkatSp(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$param_kode_wilayah = "";
				break;
			case "1":
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";
				break;
			case "2":
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";
				# code...
				break;
			case "3":
				$param_kode_wilayah = "AND kode_wilayah_kecamatan = '".$requesta['kode_wilayah']."'";
				# code...
				break;
			default:
				$param_kode_wilayah = "";
				break;
		}

        $params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "select
                    ROW_NUMBER() OVER (ORDER BY nama) as 'no',
					sekolah_id,
					nama,
					npsn,
                    status_sekolah,
                    CASE status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bentuk_pendidikan_id,
                    CASE 
                        WHEN bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
					kecamatan,
					kabupaten,
					propinsi,
					pd_kelas_1_laki,
					pd_kelas_1_perempuan,
					pd_kelas_2_laki,
					pd_kelas_2_perempuan,
					pd_kelas_3_laki,
					pd_kelas_3_perempuan,
					pd_kelas_4_laki,
					pd_kelas_4_perempuan,
					pd_kelas_5_laki,
					pd_kelas_5_perempuan,
					pd_kelas_6_laki,
					pd_kelas_6_perempuan,
					pd_kelas_7_laki,
					pd_kelas_7_perempuan,
					pd_kelas_8_laki,
					pd_kelas_8_perempuan,
					pd_kelas_9_laki,
					pd_kelas_9_perempuan,
					pd_kelas_10_laki,
					pd_kelas_10_perempuan,
					pd_kelas_11_laki,
					pd_kelas_11_perempuan,
					pd_kelas_12_laki,
					pd_kelas_12_perempuan,
					pd_kelas_13_laki,
					pd_kelas_13_perempuan,
					tanggal as tanggal_rekap_terakhir
				from
					rekap_sekolah
				where soft_delete = 0
				and semester_id = ".$requesta['semester_id']."
				{$param_kode_wilayah}
				{$params_bp}";

		$return = Preface::getDataByQuery($sql, 'rekap');

		$sql .= " ORDER BY nama OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

		// return $sql;die;

		$final = array();
		$final['results'] = sizeof($return);

		$return = Preface::getDataByQuery($sql, 'rekap');

		$final['rows'] = $return;

		return json_encode($final);
	}

	static function PdAgama(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

        $status_sekolah = $request->get('status_sekolah');

        switch ($requesta['id_level_wilayah']) {
            case "0":
                $params = 'propinsi';
                $mst_kode_wilayah_induk = '';
                $mst_kode_wilayah_induk_group = '';
                $param_kode_wilayah = '';

                $add_induk_propinsi = "null as induk_propinsi,";
                $add_kode_wilayah_induk_propinsi = "null as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

                $add_group_induk_propinsi = "propinsi,";
                $add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                break;
            case "1":
                $params = 'kabupaten';
                $mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
                $mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
                $param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

                $add_induk_propinsi = "propinsi as induk_propinsi,";
                $add_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

                $add_group_induk_propinsi = "propinsi,";
                $add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                break;
            case "2":
                $params = 'kecamatan';
                $mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
                $mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
                $param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

                $add_induk_propinsi = "propinsi as induk_propinsi,";
                $add_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

                $add_group_induk_propinsi = "propinsi,";
                $add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
                break;
            default:
                break;
        }

        $params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

        if($status_sekolah == 1){
        	$param_status = 'status_sekolah in(1)';
        }else if($status_sekolah == 2){
        	$param_status = 'status_sekolah in(2)';
        }else{
        	$param_status = 'status_sekolah in(1,2)';
        }


        $sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_propinsi}
					{$add_kode_wilayah_induk_propinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					SUM( CASE WHEN {$param_status} then pd_laki_islam else 0 end ) as pd_laki_islam,
					SUM( CASE WHEN {$param_status} then pd_perempuan_islam else 0 end ) as pd_perempuan_islam,
					SUM( CASE WHEN {$param_status} then (pd_laki_islam+pd_perempuan_islam) else 0 end ) as pd_islam,
					SUM( CASE WHEN {$param_status} then pd_laki_kristen else 0 end ) as pd_laki_kristen,
					SUM( CASE WHEN {$param_status} then pd_perempuan_kristen else 0 end ) as pd_perempuan_kristen,
					SUM( CASE WHEN {$param_status} then (pd_laki_kristen+pd_perempuan_kristen) else 0 end ) as pd_kristen,
					SUM( CASE WHEN {$param_status} then pd_laki_katholik else 0 end ) as pd_laki_katholik,
					SUM( CASE WHEN {$param_status} then pd_perempuan_katholik else 0 end ) as pd_perempuan_katholik,
					SUM( CASE WHEN {$param_status} then (pd_laki_katholik+pd_perempuan_katholik) else 0 end ) as pd_katholik,
					SUM( CASE WHEN {$param_status} then pd_laki_hindu else 0 end ) as pd_laki_hindu,
					SUM( CASE WHEN {$param_status} then pd_perempuan_hindu else 0 end ) as pd_perempuan_hindu,
					SUM( CASE WHEN {$param_status} then (pd_laki_hindu+pd_perempuan_hindu) else 0 end ) as pd_hindu,
					SUM( CASE WHEN {$param_status} then pd_laki_budha else 0 end ) as pd_laki_budha,
					SUM( CASE WHEN {$param_status} then pd_perempuan_budha else 0 end ) as pd_perempuan_budha,
					SUM( CASE WHEN {$param_status} then (pd_laki_budha+pd_perempuan_budha) else 0 end ) as pd_budha,
					SUM( CASE WHEN {$param_status} then pd_laki_konghucu else 0 end ) as pd_laki_konghucu,
					SUM( CASE WHEN {$param_status} then pd_perempuan_konghucu else 0 end ) as pd_perempuan_konghucu,
					SUM( CASE WHEN {$param_status} then (pd_laki_konghucu+pd_perempuan_konghucu) else 0 end ) as pd_konghucu,
					SUM( CASE WHEN {$param_status} then pd_laki_kepercayaan else 0 end ) as pd_laki_kepercayaan,
					SUM( CASE WHEN {$param_status} then pd_perempuan_kepercayaan else 0 end ) as pd_perempuan_kepercayaan,
					SUM( CASE WHEN {$param_status} then (pd_laki_kepercayaan+pd_perempuan_kepercayaan) else 0 end ) as pd_kepercayaan,
					SUM( CASE WHEN {$param_status} then pd_laki_agama_tidak_diisi else 0 end ) as pd_laki_agama_tidak_diisi,
					SUM( CASE WHEN {$param_status} then pd_perempuan_agama_tidak_diisi else 0 end ) as pd_perempuan_agama_tidak_diisi,
					SUM( CASE WHEN {$param_status} then (pd_laki_agama_tidak_diisi+pd_perempuan_agama_tidak_diisi) else 0 end ) as pd_agama_tidak_diisi,
					SUM( CASE WHEN {$param_status} then pd_laki else 0 end ) as pd_laki,
					SUM( CASE WHEN {$param_status} then pd_perempuan else 0 end ) as pd_perempuan,
					SUM( CASE WHEN {$param_status} then (pd_laki+pd_perempuan) else 0 end ) as pd,
					min(tanggal) as tanggal_rekap_terakhir,
					SUM(1) as jumlah_sekolah
				FROM
					rekap_sekolah WITH (NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND soft_delete = 0
				{$param_kode_wilayah}
				{$params_bp}
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_propinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_propinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

        // return $sql;die;

        $return = Preface::getDataByQuery($sql, 'rekap');

        return json_encode($return);
	}

	static function PdAgamaLangsung(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case 0:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 1:
				$col_wilayah = "w2.nama";
				$col_kode = "w2.kode_wilayah";
				$col_id_level = "w2.id_level_wilayah";
				$col_mst_kode = "w2.mst_kode_wilayah";
				$col_mst_kode_induk = 'w3.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w3.mst_kode_wilayah,';
				$params_wilayah = " and w2.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 2:
				$col_wilayah = "w1.nama";
				$col_kode = "w1.kode_wilayah";
				$col_id_level = "w1.id_level_wilayah";
				$col_mst_kode = "w1.mst_kode_wilayah";
				$col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
				$params_wilayah = " and w1.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			default:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 's.');

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$col_wilayah}) as 'no',
					{$col_wilayah} as nama,
					{$col_kode} as kode_wilayah,
					{$col_id_level} as id_level_wilayah,
					{$col_mst_kode} as mst_kode_wilayah,
					{$col_mst_kode_induk}
					COUNT(pd.peserta_didik_id) as total_pd,
					SUM(CASE WHEN pd.agama_id = 1 THEN 1 ELSE 0 END) as islam,
					SUM(CASE WHEN pd.agama_id = 2 THEN 1 ELSE 0 END) as kristen,
					SUM(CASE WHEN pd.agama_id = 3 THEN 1 ELSE 0 END) as katholik,
					SUM(CASE WHEN pd.agama_id = 4 THEN 1 ELSE 0 END) as hindu,
					SUM(CASE WHEN pd.agama_id = 5 THEN 1 ELSE 0 END) as budha,
					SUM(CASE WHEN pd.agama_id = 6 THEN 1 ELSE 0 END) as konghucu,
					SUM(CASE WHEN pd.agama_id = 99 THEN 1 ELSE 0 END) as lainnya
				FROM
					peserta_didik pd WITH (NOLOCK)
				JOIN registrasi_peserta_didik rpd WITH (NOLOCK) ON pd.peserta_didik_id = rpd.peserta_didik_id
				JOIN anggota_rombel ar WITH (NOLOCK) ON ar.peserta_didik_id = pd.peserta_didik_id
				JOIN rombongan_belajar rb WITH (NOLOCK) ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
				JOIN ref.semester sm WITH (NOLOCK) ON sm.semester_id = rb.semester_id
				JOIN sekolah s WITH (NOLOCK) ON rpd.sekolah_id = s.sekolah_id
				JOIN ref.mst_wilayah w1 WITH (NOLOCK) ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
				JOIN ref.mst_wilayah w2 WITH (NOLOCK) ON w1.mst_kode_wilayah = w2.kode_wilayah
				JOIN ref.mst_wilayah w3 WITH (NOLOCK) ON w2.mst_kode_wilayah = w3.kode_wilayah
				where
				  rpd.Soft_delete = 0
				AND pd.Soft_delete = 0
				AND (rpd.jenis_keluar_id is null
					OR rpd.tanggal_keluar > sm.tanggal_selesai
					OR rpd.jenis_keluar_id = '1')
				AND sm.semester_id = '".$requesta['semester_id']."'
				AND rb.soft_delete = 0
				AND ar.soft_delete = 0
				AND rb.jenis_rombel = 1
				{$params_wilayah}
				{$params_bp}
				GROUP BY
					{$col_wilayah},
					{$col_kode},
					{$col_id_level},
					{$col_mst_kode_induk_group}
					{$col_mst_kode}
				ORDER BY
					{$col_wilayah} ASC";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);

	}

	static function PdAgamaSp(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				//nothing to do
				$wilayah = null;
				$params_wilayah = null;
				break;
			case "1":
				$wilayah = 'propinsi';
				$params_wilayah = 'kode_wilayah_propinsi';
				$params_mst_wilayah = 'mst_kode_wilayah_propinsi';
				$params_id_level = 'id_level_wilayah_propinsi';
				break;
			case "2":
				$wilayah = 'kabupaten';
				$params_wilayah = 'kode_wilayah_kabupaten';
				$params_mst_wilayah = 'mst_kode_wilayah_kabupaten';
				$params_id_level = 'id_level_wilayah_kabupaten';
				break;
			case "3":
				$wilayah = 'kecamatan';
				$params_wilayah = 'kode_wilayah_kecamatan';
				$params_mst_wilayah = 'mst_kode_wilayah_kecamatan';
				$params_id_level = 'id_level_wilayah_kecamatan';
				break;
			default:
				break;
		}

		$sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY nama) as 'no',
					sekolah_id,
					nama,
					npsn,
                    status_sekolah,
                    CASE status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bentuk_pendidikan_id,
                    CASE 
                        WHEN bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
                    {$wilayah},
					{$params_wilayah} AS kode_wilayah,
					{$params_mst_wilayah} AS mst_kode_wilayah,
					{$params_id_level} AS id_level_wilayah,
					SUM( pd_laki_islam ) as pd_laki_islam,
					SUM( pd_perempuan_islam ) as pd_perempuan_islam,
					SUM( pd_laki_islam+pd_perempuan_islam ) as pd_islam,
					SUM( pd_laki_kristen ) as pd_laki_kristen,
					SUM( pd_perempuan_kristen ) as pd_perempuan_kristen,
					SUM( pd_laki_kristen+pd_perempuan_kristen ) as pd_kristen,
					SUM( pd_laki_katholik ) as pd_laki_katholik,
					SUM( pd_perempuan_katholik ) as pd_perempuan_katholik,
					SUM( pd_laki_katholik+pd_perempuan_katholik ) as pd_katholik,
					SUM( pd_laki_hindu ) as pd_laki_hindu,
					SUM( pd_perempuan_hindu ) as pd_perempuan_hindu,
					SUM( pd_laki_hindu+pd_perempuan_hindu ) as pd_hindu,
					SUM( pd_laki_budha ) as pd_laki_budha,
					SUM( pd_perempuan_budha ) as pd_perempuan_budha,
					SUM( pd_laki_budha+pd_perempuan_budha ) as pd_budha,
					SUM( pd_laki_konghucu ) as pd_laki_konghucu,
					SUM( pd_perempuan_konghucu ) as pd_perempuan_konghucu,
					SUM( pd_laki_konghucu+pd_perempuan_konghucu ) as pd_konghucu,
					SUM( pd_laki_kepercayaan ) as pd_laki_kepercayaan,
					SUM( pd_perempuan_kepercayaan ) as pd_perempuan_kepercayaan,
					SUM( pd_laki_kepercayaan+pd_perempuan_kepercayaan ) as pd_kepercayaan,
					SUM( pd_laki_agama_tidak_diisi ) as pd_laki_agama_tidak_diisi,
					SUM( pd_perempuan_agama_tidak_diisi ) as pd_perempuan_agama_tidak_diisi,
					SUM( pd_laki_agama_tidak_diisi+pd_perempuan_agama_tidak_diisi ) as pd_agama_tidak_diisi,
					SUM(pd) AS pd,
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah WITH(NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND {$params_wilayah} = '".$requesta['kode_wilayah']."'
				{$params_bp}
				AND soft_delete = 0
				GROUP BY
                    nama,
                    sekolah_id,
                    npsn,
                    status_sekolah,
                    bentuk_pendidikan_id,
                    {$wilayah},
                    {$params_wilayah},
                    {$params_mst_wilayah},
                    {$params_id_level}
				ORDER BY
					nama";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		$sql .= " OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";


		$final = array();
		$final['results'] = sizeof($return);

		$return = Preface::getDataByQuery($sql,'rekap');

		$final['rows'] = $return;

		return json_encode($final);
	}

	static function PdAgamaSpLangsung(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 's.');

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY s.nama) as 'no',
					s.sekolah_id,
					s.nama,
					s.npsn,
                    s.status_sekolah,
                    CASE s.status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    s.bentuk_pendidikan_id,
                    CASE 
                        WHEN s.bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN s.bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN s.bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN s.bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN s.bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN s.bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN s.bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN s.bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN s.bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
					w1.nama AS kecamatan,
					w1.kode_wilayah as kode_wilayah,
					w1.id_level_wilayah as id_level_wilayah,
					w1.mst_kode_wilayah as mst_kode_wilayah,
					COUNT(pd.peserta_didik_id) as total_pd,
					SUM(CASE WHEN pd.agama_id = 1 THEN 1 ELSE 0 END) as islam,
					SUM(CASE WHEN pd.agama_id = 2 THEN 1 ELSE 0 END) as kristen,
					SUM(CASE WHEN pd.agama_id = 3 THEN 1 ELSE 0 END) as katholik,
					SUM(CASE WHEN pd.agama_id = 4 THEN 1 ELSE 0 END) as hindu,
					SUM(CASE WHEN pd.agama_id = 5 THEN 1 ELSE 0 END) as budha,
					SUM(CASE WHEN pd.agama_id = 6 THEN 1 ELSE 0 END) as konghucu,
					SUM(CASE WHEN pd.agama_id = 99 THEN 1 ELSE 0 END) as lainnya
				FROM
					peserta_didik pd WITH (NOLOCK)
				JOIN registrasi_peserta_didik rpd WITH (NOLOCK) ON pd.peserta_didik_id = rpd.peserta_didik_id
				JOIN anggota_rombel ar WITH (NOLOCK) ON ar.peserta_didik_id = pd.peserta_didik_id
				JOIN rombongan_belajar rb WITH (NOLOCK) ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
				JOIN ref.semester sm WITH (NOLOCK) ON sm.semester_id = rb.semester_id
				JOIN sekolah s WITH (NOLOCK) ON rpd.sekolah_id = s.sekolah_id
				JOIN ref.mst_wilayah w1 WITH (NOLOCK) ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
				JOIN ref.mst_wilayah w2 WITH (NOLOCK) ON w1.mst_kode_wilayah = w2.kode_wilayah
				JOIN ref.mst_wilayah w3 WITH (NOLOCK) ON w2.mst_kode_wilayah = w3.kode_wilayah
				where
				rpd.Soft_delete = 0
				AND pd.Soft_delete = 0
				AND (rpd.jenis_keluar_id is null
					OR rpd.tanggal_keluar > sm.tanggal_selesai
					OR rpd.jenis_keluar_id = '1')
				AND sm.semester_id = '".$requesta['semester_id']."'
				AND rb.soft_delete = 0
				AND ar.soft_delete = 0
				AND rb.jenis_rombel = 1
				AND w1.kode_wilayah = '".$requesta['kode_wilayah']."'
				{$params_bp}
				GROUP BY
					w1.nama,
                    w1.kode_wilayah,
                    w1.id_level_wilayah,
                    w1.mst_kode_wilayah,
                    s.nama,
                    s.sekolah_id,
                    s.npsn,
                    s.status_sekolah,
                    s.bentuk_pendidikan_id";

        // return $sql;die;

        $return = Preface::getDataByQuery($sql);

        $sql .= " ORDER BY nama OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

        // return $sql;die;

        $final = array();
        $final['results'] = sizeof($return);

        $return = Preface::getDataByQuery($sql);

        $final['rows'] = $return;

        return json_encode($final);

	}

	static function PdUsia(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

        $status_sekolah = $request->get('status_sekolah');

        switch ($requesta['id_level_wilayah']) {
            case "0":
                $params = 'propinsi';
                $mst_kode_wilayah_induk = '';
                $mst_kode_wilayah_induk_group = '';
                $param_kode_wilayah = '';

                $add_induk_propinsi = "null as induk_propinsi,";
                $add_kode_wilayah_induk_propinsi = "null as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

                $add_group_induk_propinsi = "propinsi,";
                $add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                break;
            case "1":
                $params = 'kabupaten';
                $mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
                $mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
                $param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

                $add_induk_propinsi = "propinsi as induk_propinsi,";
                $add_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

                $add_group_induk_propinsi = "propinsi,";
                $add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                break;
            case "2":
                $params = 'kecamatan';
                $mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
                $mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
                $param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

                $add_induk_propinsi = "propinsi as induk_propinsi,";
                $add_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

                $add_group_induk_propinsi = "propinsi,";
                $add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
                break;
            default:
                break;
        }

        $params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

        if($status_sekolah == 1){
        	$param_status = 'status_sekolah in(1)';
        }else if($status_sekolah == 2){
        	$param_status = 'status_sekolah in(2)';
        }else{
        	$param_status = 'status_sekolah in(1,2)';
        }


        $sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_propinsi}
					{$add_kode_wilayah_induk_propinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}

					SUM( CASE WHEN {$param_status} then 
						(pd_laki_0_tahun+pd_perempuan_0_tahun) 
						+(pd_laki_1_tahun+pd_perempuan_1_tahun) 
						+(pd_laki_2_tahun+pd_perempuan_2_tahun) 
						+(pd_laki_3_tahun+pd_perempuan_3_tahun) 
						+(pd_laki_4_tahun+pd_perempuan_4_tahun) 
						+(pd_laki_5_tahun+pd_perempuan_5_tahun) 
						+(pd_laki_6_tahun+pd_perempuan_6_tahun) 
					else 0 end ) as pd_kurang_7,
					SUM( CASE WHEN {$param_status} then (pd_laki_7_tahun+pd_perempuan_7_tahun) else 0 end ) as pd_7,
					SUM( CASE WHEN {$param_status} then (pd_laki_8_tahun+pd_perempuan_8_tahun) else 0 end ) as pd_8,
					SUM( CASE WHEN {$param_status} then (pd_laki_9_tahun+pd_perempuan_9_tahun) else 0 end ) as pd_9,
					SUM( CASE WHEN {$param_status} then (pd_laki_10_tahun+pd_perempuan_10_tahun) else 0 end ) as pd_10,
					SUM( CASE WHEN {$param_status} then (pd_laki_11_tahun+pd_perempuan_11_tahun) else 0 end ) as pd_11,
					SUM( CASE WHEN {$param_status} then (pd_laki_12_tahun+pd_perempuan_12_tahun) else 0 end ) as pd_12,
					SUM( CASE WHEN {$param_status} then  
						(pd_laki_13_tahun+pd_perempuan_13_tahun) 
						+(pd_laki_14_tahun+pd_perempuan_14_tahun) 
						+(pd_laki_15_tahun+pd_perempuan_15_tahun) 
						+(pd_laki_16_tahun+pd_perempuan_16_tahun) 
						+(pd_laki_17_tahun+pd_perempuan_17_tahun) 
						+(pd_laki_18_tahun+pd_perempuan_18_tahun) 
						+(pd_laki_19_tahun+pd_perempuan_19_tahun) 
						+(pd_laki_20_tahun+pd_perempuan_20_tahun) 
						+(pd_laki_20_tahun_lebih+pd_perempuan_20_tahun_lebih) 
					else 0 end ) as pd_lebih_12,

					SUM( CASE WHEN {$param_status} then 
						(pd_laki_0_tahun+pd_perempuan_0_tahun) 
						+(pd_laki_1_tahun+pd_perempuan_1_tahun) 
						+(pd_laki_2_tahun+pd_perempuan_2_tahun) 
						+(pd_laki_3_tahun+pd_perempuan_3_tahun) 
						+(pd_laki_4_tahun+pd_perempuan_4_tahun) 
						+(pd_laki_5_tahun+pd_perempuan_5_tahun) 
						+(pd_laki_6_tahun+pd_perempuan_6_tahun) 
						+(pd_laki_7_tahun+pd_perempuan_7_tahun) 
						+(pd_laki_8_tahun+pd_perempuan_8_tahun) 
						+(pd_laki_9_tahun+pd_perempuan_9_tahun) 
						+(pd_laki_10_tahun+pd_perempuan_10_tahun) 
						+(pd_laki_11_tahun+pd_perempuan_11_tahun) 
						+(pd_laki_12_tahun+pd_perempuan_12_tahun)
					else 0 end ) as pd_kurang_13,
					SUM( CASE WHEN {$param_status} then (pd_laki_13_tahun+pd_perempuan_13_tahun) else 0 end ) as pd_13,
					SUM( CASE WHEN {$param_status} then (pd_laki_14_tahun+pd_perempuan_14_tahun) else 0 end ) as pd_14,
					SUM( CASE WHEN {$param_status} then (pd_laki_15_tahun+pd_perempuan_15_tahun) else 0 end ) as pd_15,
					SUM( CASE WHEN {$param_status} then  
						(pd_laki_16_tahun+pd_perempuan_16_tahun) 
						+(pd_laki_17_tahun+pd_perempuan_17_tahun) 
						+(pd_laki_18_tahun+pd_perempuan_18_tahun) 
						+(pd_laki_19_tahun+pd_perempuan_19_tahun) 
						+(pd_laki_20_tahun+pd_perempuan_20_tahun) 
						+(pd_laki_20_tahun_lebih+pd_perempuan_20_tahun_lebih) 
					else 0 end ) as pd_lebih_15,
					
					SUM( CASE WHEN {$param_status} then 
						(pd_laki_0_tahun+pd_perempuan_0_tahun) 
						+(pd_laki_1_tahun+pd_perempuan_1_tahun) 
						+(pd_laki_2_tahun+pd_perempuan_2_tahun) 
						+(pd_laki_3_tahun+pd_perempuan_3_tahun) 
						+(pd_laki_4_tahun+pd_perempuan_4_tahun) 
						+(pd_laki_5_tahun+pd_perempuan_5_tahun) 
						+(pd_laki_6_tahun+pd_perempuan_6_tahun) 
						+(pd_laki_7_tahun+pd_perempuan_7_tahun) 
						+(pd_laki_8_tahun+pd_perempuan_8_tahun) 
						+(pd_laki_9_tahun+pd_perempuan_9_tahun) 
						+(pd_laki_10_tahun+pd_perempuan_10_tahun) 
						+(pd_laki_11_tahun+pd_perempuan_11_tahun) 
						+(pd_laki_12_tahun+pd_perempuan_12_tahun) 
						+(pd_laki_13_tahun+pd_perempuan_13_tahun) 
						+(pd_laki_14_tahun+pd_perempuan_14_tahun) 
						+(pd_laki_15_tahun+pd_perempuan_15_tahun) 
					else 0 end ) as pd_kurang_16,
					SUM( CASE WHEN {$param_status} then (pd_laki_16_tahun+pd_perempuan_16_tahun) else 0 end ) as pd_16,
					SUM( CASE WHEN {$param_status} then (pd_laki_17_tahun+pd_perempuan_17_tahun) else 0 end ) as pd_17,
					SUM( CASE WHEN {$param_status} then (pd_laki_18_tahun+pd_perempuan_18_tahun) else 0 end ) as pd_18,
					SUM( CASE WHEN {$param_status} then 
						(pd_laki_19_tahun+pd_perempuan_19_tahun)
						+(pd_laki_20_tahun+pd_perempuan_20_tahun)
						+(pd_laki_20_tahun_lebih+pd_perempuan_20_tahun_lebih) 
					else 0 end ) as pd_lebih_18,

					min(tanggal) as tanggal_rekap_terakhir,
					SUM(1) as jumlah_sekolah
				FROM
					rekap_sekolah WITH (NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND soft_delete = 0
				{$param_kode_wilayah}
				{$params_bp}
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_propinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_propinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

        // return $sql;die;

        $return = Preface::getDataByQuery($sql, 'rekap');

        return json_encode($return);
	}

	static function PdUsiaLangsung(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case 0:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 1:
				$col_wilayah = "w2.nama";
				$col_kode = "w2.kode_wilayah";
				$col_id_level = "w2.id_level_wilayah";
				$col_mst_kode = "w2.mst_kode_wilayah";
				$col_mst_kode_induk = 'w3.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w3.mst_kode_wilayah,';
				$params_wilayah = " and w2.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 2:
				$col_wilayah = "w1.nama";
				$col_kode = "w1.kode_wilayah";
				$col_id_level = "w1.id_level_wilayah";
				$col_mst_kode = "w1.mst_kode_wilayah";
				$col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
				$params_wilayah = " and w1.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			default:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 's.');

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$col_wilayah}) as 'no',
					{$col_wilayah} as nama,
					{$col_kode} as kode_wilayah,
					{$col_id_level} as id_level_wilayah,
					{$col_mst_kode} as mst_kode_wilayah,
					{$col_mst_kode_induk}
					COUNT(pd.peserta_didik_id) as total_pd,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 7 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_kurang_7,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 7 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_7,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 8 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_8,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 9 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_9,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 10 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_10,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 11 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_11,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 12 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_12,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 12 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_lebih_12,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 13 AND s.bentuk_pendidikan_id in (6,8) then 1 else 0 end) as pd_kurang_13,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 13 AND s.bentuk_pendidikan_id in (6,8) then 1 else 0 end) as pd_13,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 14 AND s.bentuk_pendidikan_id in (6,8) then 1 else 0 end) as pd_14,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 15 AND s.bentuk_pendidikan_id in (6,8) then 1 else 0 end) as pd_15,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 15 AND s.bentuk_pendidikan_id in (6,8) then 1 else 0 end) as pd_lebih_15,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 16 AND s.bentuk_pendidikan_id in (13,15) then 1 else 0 end) as pd_kurang_16,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 16 AND s.bentuk_pendidikan_id in (13,15) then 1 else 0 end) as pd_16,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 17 AND s.bentuk_pendidikan_id in (13,15) then 1 else 0 end) as pd_17,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 18 AND s.bentuk_pendidikan_id in (13,15) then 1 else 0 end) as pd_18,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 18 AND s.bentuk_pendidikan_id in (13,15) then 1 else 0 end) as pd_lebih_18
				FROM
					peserta_didik pd WITH (NOLOCK)
				JOIN registrasi_peserta_didik rpd WITH (NOLOCK) ON pd.peserta_didik_id = rpd.peserta_didik_id
				JOIN anggota_rombel ar WITH (NOLOCK) ON ar.peserta_didik_id = pd.peserta_didik_id
				JOIN rombongan_belajar rb WITH (NOLOCK) ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
				JOIN ref.semester sm WITH (NOLOCK) ON sm.semester_id = rb.semester_id
				JOIN sekolah s WITH (NOLOCK) ON rpd.sekolah_id = s.sekolah_id
				JOIN ref.mst_wilayah w1 WITH (NOLOCK) ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
				JOIN ref.mst_wilayah w2 WITH (NOLOCK) ON w1.mst_kode_wilayah = w2.kode_wilayah
				JOIN ref.mst_wilayah w3 WITH (NOLOCK) ON w2.mst_kode_wilayah = w3.kode_wilayah
				WHERE
				  rpd.Soft_delete = 0
				AND pd.Soft_delete = 0
				AND (rpd.jenis_keluar_id is null
					OR rpd.tanggal_keluar > sm.tanggal_selesai
					OR rpd.jenis_keluar_id = '1')
				AND sm.semester_id = '".$requesta['semester_id']."'
				AND rb.soft_delete = 0
				AND ar.soft_delete = 0
				AND rb.jenis_rombel = 1
				{$params_wilayah}
				{$params_bp}
				GROUP BY
					{$col_wilayah},
					{$col_kode},
					{$col_id_level},
					{$col_mst_kode_induk_group}
					{$col_mst_kode}
				ORDER BY
					{$col_wilayah} ASC";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);

	}

	static function PdUsiaSp(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 's.');

		$sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY s.nama) as 'no',
					s.sekolah_id,
                    s.nama,
                    s.npsn,
                    s.status_sekolah,
                    CASE s.status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    s.bentuk_pendidikan_id,
                    CASE 
                        WHEN s.bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN s.bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN s.bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN s.bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN s.bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN s.bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN s.bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN s.bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN s.bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
					w1.nama AS kecamatan,
					w1.kode_wilayah as kode_wilayah,
					w1.id_level_wilayah as id_level_wilayah,
					w1.mst_kode_wilayah as mst_kode_wilayah,
					COUNT(pd.peserta_didik_id) as total_pd,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 7 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_kurang_7,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 7 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_7,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 8 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_8,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 9 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_9,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 10 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_10,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 11 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_11,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 12 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_12,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 12 AND s.bentuk_pendidikan_id in (5,7) then 1 else 0 end) as pd_lebih_12,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 13 AND s.bentuk_pendidikan_id in (6,8) then 1 else 0 end) as pd_kurang_13,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 13 AND s.bentuk_pendidikan_id in (6,8) then 1 else 0 end) as pd_13,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 14 AND s.bentuk_pendidikan_id in (6,8) then 1 else 0 end) as pd_14,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 15 AND s.bentuk_pendidikan_id in (6,8) then 1 else 0 end) as pd_15,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 15 AND s.bentuk_pendidikan_id in (6,8) then 1 else 0 end) as pd_lebih_15,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 16 AND s.bentuk_pendidikan_id in (13,15) then 1 else 0 end) as pd_kurang_16,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 16 AND s.bentuk_pendidikan_id in (13,15) then 1 else 0 end) as pd_16,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 17 AND s.bentuk_pendidikan_id in (13,15) then 1 else 0 end) as pd_17,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 18 AND s.bentuk_pendidikan_id in (13,15) then 1 else 0 end) as pd_18,
					SUM(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 18 AND s.bentuk_pendidikan_id in (13,15) then 1 else 0 end) as pd_lebih_18
				FROM
					peserta_didik pd WITH (NOLOCK)
				JOIN registrasi_peserta_didik rpd WITH (NOLOCK) ON pd.peserta_didik_id = rpd.peserta_didik_id
				JOIN anggota_rombel ar WITH (NOLOCK) ON ar.peserta_didik_id = pd.peserta_didik_id
				JOIN rombongan_belajar rb WITH (NOLOCK) ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
				JOIN ref.semester sm WITH (NOLOCK) ON sm.semester_id = rb.semester_id
				JOIN sekolah s WITH (NOLOCK) ON rpd.sekolah_id = s.sekolah_id
				JOIN ref.mst_wilayah w1 WITH (NOLOCK) ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
				JOIN ref.mst_wilayah w2 WITH (NOLOCK) ON w1.mst_kode_wilayah = w2.kode_wilayah
				JOIN ref.mst_wilayah w3 WITH (NOLOCK) ON w2.mst_kode_wilayah = w3.kode_wilayah
				WHERE
				  rpd.Soft_delete = 0
				AND pd.Soft_delete = 0
				AND (rpd.jenis_keluar_id is null
					OR rpd.tanggal_keluar > sm.tanggal_selesai
					OR rpd.jenis_keluar_id = '1')
				AND sm.semester_id = '".$requesta['semester_id']."'
				AND rb.soft_delete = 0
				AND ar.soft_delete = 0
				AND rb.jenis_rombel = 1
				AND w1.kode_wilayah = '".$requesta['kode_wilayah']."'
				{$params_bp}
				GROUP BY
					w1.nama,
                    w1.kode_wilayah,
                    w1.id_level_wilayah,
                    w1.mst_kode_wilayah,
                    s.nama,
                    s.sekolah_id,
                    s.npsn,
                    s.status_sekolah,
                    s.bentuk_pendidikan_id";

		// return $sql;die;

        $return = Preface::getDataByQuery($sql);

        $sql .= " ORDER BY nama OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

        // return $sql;die;

        $final = array();
        $final['results'] = sizeof($return);

        $return = Preface::getDataByQuery($sql);

        $final['rows'] = $return;

        return json_encode($final);

	}

	static function PdAngkaPutusSekolah(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_provinsi}
					{$add_kode_wilayah_induk_provinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					SUM(pd) as total_pd,
					SUM( CASE WHEN status_sekolah = 1 then pd_angka_putus_sekolah_laki else 0 end ) as pd_ap_laki_negeri,
					SUM( CASE WHEN status_sekolah = 1 then pd_angka_putus_sekolah_perempuan else 0 end ) as pd_ap_perempuan_negeri,
					SUM( CASE WHEN status_sekolah = 2 then pd_angka_putus_sekolah_laki else 0 end ) as pd_ap_laki_swasta,
					SUM( CASE WHEN status_sekolah = 2 then pd_angka_putus_sekolah_perempuan else 0 end ) as pd_ap_perempuan_swasta,
					min(tanggal) as tanggal_rekap_terakhir,
					SUM(1) as jumlah_sekolah
				FROM
					rekap_sekolah
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND soft_delete = 0
				{$param_kode_wilayah}
				{$params_bp}
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_provinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_provinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
	}

	static function PdAngkaPutusSekolahSp(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				//nothing to do
				$wilayah = null;
				$params_wilayah = null;
				break;
			case "1":
				$wilayah = 'propinsi';
				$params_wilayah = 'kode_wilayah_propinsi';
				$params_mst_wilayah = 'mst_kode_wilayah_propinsi';
				$params_id_level = 'id_level_wilayah_propinsi';
				break;
			case "2":
				$wilayah = 'kabupaten';
				$params_wilayah = 'kode_wilayah_kabupaten';
				$params_mst_wilayah = 'mst_kode_wilayah_kabupaten';
				$params_id_level = 'id_level_wilayah_kabupaten';
				break;
			case "3":
				$wilayah = 'kecamatan';
				$params_wilayah = 'kode_wilayah_kecamatan';
				$params_mst_wilayah = 'mst_kode_wilayah_kecamatan';
				$params_id_level = 'id_level_wilayah_kecamatan';
				break;
			default:
				break;
		}

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY nama) as 'no',
					sekolah_id,
                    nama,
                    npsn,
                    status_sekolah,
                    CASE status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bentuk_pendidikan_id,
                    CASE 
                        WHEN bentuk_pendidikan_id = 1 THEN	'TK'
                        WHEN bentuk_pendidikan_id = 2 THEN	'KB'
                        WHEN bentuk_pendidikan_id = 3 THEN	'TPA'
                        WHEN bentuk_pendidikan_id = 4 THEN	'SPS'
                        WHEN bentuk_pendidikan_id = 5 THEN	'SD'
                        WHEN bentuk_pendidikan_id = 6 THEN	'SMP'
                        WHEN bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN bentuk_pendidikan_id = 13 THEN	'SMA'
                        WHEN bentuk_pendidikan_id = 15 THEN	'SMK'
                    ELSE '-' END AS bentuk,
					{$wilayah} as {$wilayah},
					{$params_wilayah} as kode_wilayah,
					{$params_mst_wilayah} as mst_kode_wilayah,
					{$params_id_level} as id_level_wilayah,
					SUM(pd) as total_pd,
					SUM(pd_angka_putus_sekolah_laki) as pd_ap_laki,
					SUM(pd_angka_putus_sekolah_perempuan) as pd_ap_perempuan,
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah WITH (NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND {$params_wilayah} = '".$requesta['kode_wilayah']."'
				AND soft_delete = 0
				{$params_bp}
				GROUP BY
				    nama,
				    sekolah_id,
				    npsn,
				    status_sekolah,
				    bentuk_pendidikan_id,
					{$wilayah},
					{$params_wilayah},
					{$params_mst_wilayah},
					{$params_id_level}";

        // return $sql;die;

        $return = Preface::getDataByQuery($sql, 'rekap');

        $sql .= " ORDER BY nama OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

        // return $sql;die;

        $final = array();
        $final['results'] = sizeof($return);

        $return = Preface::getDataByQuery($sql, 'rekap');

        $final['rows'] = $return;

        return json_encode($final);

    }

	static function PdAngkaKelulusan(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_provinsi}
					{$add_kode_wilayah_induk_provinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					ISNULL(SUM(pd_lulus_laki_2015), 0) as pd_kelulusan_laki_2015,
					ISNULL(SUM(pd_lulus_perempuan_2015), 0) as pd_kelulusan_perempuan_2015,
					ISNULL(SUM(pd_lulus_laki_2014), 0) as pd_kelulusan_laki_2014,
					ISNULL(SUM(pd_lulus_perempuan_2014), 0) as pd_kelulusan_perempuan_2014,
					ISNULL(SUM(pd_lulus_laki_2016), 0) as pd_kelulusan_laki_2016,
					ISNULL(SUM(pd_lulus_perempuan_2016), 0) as pd_kelulusan_perempuan_2016,
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah WITH (NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND soft_delete = 0
				{$param_kode_wilayah}
				{$params_bp}
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_provinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_provinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
	}

    static function PdAngkaKelulusanSp(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				//nothing to do
				$wilayah = null;
				$params_wilayah = null;
				break;
			case "1":
				$wilayah = 'propinsi';
				$params_wilayah = 'kode_wilayah_propinsi';
				$params_mst_wilayah = 'mst_kode_wilayah_propinsi';
				$params_id_level = 'id_level_wilayah_propinsi';
				break;
			case "2":
				$wilayah = 'kabupaten';
				$params_wilayah = 'kode_wilayah_kabupaten';
				$params_mst_wilayah = 'mst_kode_wilayah_kabupaten';
				$params_id_level = 'id_level_wilayah_kabupaten';
				break;
			case "3":
				$wilayah = 'kecamatan';
				$params_wilayah = 'kode_wilayah_kecamatan';
				$params_mst_wilayah = 'mst_kode_wilayah_kecamatan';
				$params_id_level = 'id_level_wilayah_kecamatan';
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY nama) as 'no',
					nama,
					sekolah_id,
					npsn,
                    status_sekolah,
                    CASE status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bentuk_pendidikan_id,
                    CASE 
                        WHEN bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
					{$wilayah},
					{$params_wilayah} AS kode_wilayah,
					{$params_mst_wilayah} AS mst_kode_wilayah,
					{$params_id_level} AS id_level_wilayah,
					ISNULL(SUM(pd_lulus_laki_2014), 0) as pd_kelulusan_laki_2014,
					ISNULL(SUM(pd_lulus_perempuan_2014), 0) as pd_kelulusan_perempuan_2014,
					ISNULL(SUM(pd_lulus_laki_2015), 0) as pd_kelulusan_laki_2015,
					ISNULL(SUM(pd_lulus_perempuan_2015), 0) as pd_kelulusan_perempuan_2015,
					ISNULL(SUM(pd_lulus_laki_2016), 0) as pd_kelulusan_laki_2016,
					ISNULL(SUM(pd_lulus_perempuan_2016), 0) as pd_kelulusan_perempuan_2016,
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah WITH (NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND {$params_wilayah} = '".$requesta['kode_wilayah']."'
				AND soft_delete = 0
				{$params_bp}
				GROUP BY
                    nama,
                    sekolah_id,
                    npsn,
                    status_sekolah,
                    bentuk_pendidikan_id,
                    {$wilayah},
                    {$params_wilayah},
                    {$params_mst_wilayah},
                    {$params_id_level}";

        // return $sql;die;

        $return = Preface::getDataByQuery($sql, 'rekap');

        $sql .= " ORDER BY nama OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

        // return $sql;die;

        $final = array();
        $final['results'] = sizeof($return);

        $return = Preface::getDataByQuery($sql, 'rekap');

        $final['rows'] = $return;

        return json_encode($final);

	}

	static function PdAngkaMengulang(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_provinsi}
					{$add_kode_wilayah_induk_provinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					SUM(pd) as total_pd,
					SUM( CASE WHEN status_sekolah = 1 then pd_angka_mengulang_laki else 0 end ) as pd_am_laki_negeri,
					SUM( CASE WHEN status_sekolah = 1 then pd_angka_mengulang_perempuan else 0 end ) as pd_am_perempuan_negeri,
					SUM( CASE WHEN status_sekolah = 2 then pd_angka_mengulang_laki else 0 end ) as pd_am_laki_swasta,
					SUM( CASE WHEN status_sekolah = 2 then pd_angka_mengulang_perempuan else 0 end ) as pd_am_perempuan_swasta,
					min(tanggal) as tanggal_rekap_terakhir,
					SUM(1) as jumlah_sekolah
				FROM
					rekap_sekolah WITH (NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND soft_delete = 0
				{$param_kode_wilayah}
				{$params_bp}
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_provinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_provinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
	}

	static function PdAngkaMengulangSp(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				//nothing to do
				$wilayah = null;
				$params_wilayah = null;
				break;
			case "1":
				$wilayah = 'propinsi';
				$params_wilayah = 'kode_wilayah_propinsi';
				$params_mst_wilayah = 'mst_kode_wilayah_propinsi';
				$params_id_level = 'id_level_wilayah_propinsi';
				break;
			case "2":
				$wilayah = 'kabupaten';
				$params_wilayah = 'kode_wilayah_kabupaten';
				$params_mst_wilayah = 'mst_kode_wilayah_kabupaten';
				$params_id_level = 'id_level_wilayah_kabupaten';
				break;
			case "3":
				$wilayah = 'kecamatan';
				$params_wilayah = 'kode_wilayah_kecamatan';
				$params_mst_wilayah = 'mst_kode_wilayah_kecamatan';
				$params_id_level = 'id_level_wilayah_kecamatan';
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY nama) as 'no',
					sekolah_id,
                    nama,
                    npsn,
                    status_sekolah,
                    CASE status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bentuk_pendidikan_id,
                    CASE 
                        WHEN bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
					{$wilayah} as {$wilayah},
					{$params_wilayah} as kode_wilayah,
					{$params_mst_wilayah} as mst_kode_wilayah,
					{$params_id_level} as id_level_wilayah,
					SUM(pd) as total_pd,
					SUM(pd_angka_mengulang_laki) as pd_am_laki,
					SUM(pd_angka_mengulang_perempuan) as pd_am_perempuan,
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah WITH (NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND soft_delete = 0
				AND {$params_wilayah} = '".$requesta['kode_wilayah']."'
				{$params_bp}
				GROUP BY
				    nama,
				    sekolah_id,
				    npsn,
				    status_sekolah,
				    bentuk_pendidikan_id,
					{$wilayah},
					{$params_wilayah},
					{$params_mst_wilayah},
					{$params_id_level}";

        // return $sql;die;

        $return = Preface::getDataByQuery($sql, 'rekap');

        $sql .= " ORDER BY nama OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

        // return $sql;die;

        $final = array();
        $final['results'] = sizeof($return);

        $return = Preface::getDataByQuery($sql, 'rekap');

        $final['rows'] = $return;

        return json_encode($final);
	}

	static function PdTingkatBarChart(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = "";

				$add_induk_propinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_propinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_propinsi = "propinsi,";
				$add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_propinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_propinsi = "propinsi,";
				$add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_propinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_propinsi = "propinsi,";
				$add_group_kode_wilayah_induk_propinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_1_laki + pd_kelas_1_perempuan) else 0 end) as pd_kelas_1,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_2_laki + pd_kelas_2_perempuan) else 0 end) as pd_kelas_2,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_3_laki + pd_kelas_3_perempuan) else 0 end) as pd_kelas_3,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_4_laki + pd_kelas_4_perempuan) else 0 end) as pd_kelas_4,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_5_laki + pd_kelas_5_perempuan) else 0 end) as pd_kelas_5,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_6_laki + pd_kelas_6_perempuan) else 0 end) as pd_kelas_6,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_7_laki + pd_kelas_7_perempuan) else 0 end) as pd_kelas_7,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_8_laki + pd_kelas_8_perempuan) else 0 end) as pd_kelas_8,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_9_laki + pd_kelas_9_perempuan) else 0 end) as pd_kelas_9,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_10_laki + pd_kelas_10_perempuan) else 0 end) as pd_kelas_10,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_11_laki + pd_kelas_11_perempuan) else 0 end) as pd_kelas_11,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_12_laki + pd_kelas_12_perempuan) else 0 end) as pd_kelas_12,
					sum ( case when status_sekolah in (1,2) then (pd_kelas_13_laki + pd_kelas_13_perempuan) else 0 end) as pd_kelas_13
				FROM
					rekap_sekolah WITH (NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND soft_delete = 0
				{$param_kode_wilayah}
				{$params_bp}
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_propinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_propinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);

	}
}