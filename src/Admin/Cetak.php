<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Admin\Model\SekolahPeer;
use Admin\Model\Sekolah;
use Admin\Model\MstWilayahPeer;
use Admin\Model\MstWilayah;
use Admin\Model\Pengguna;
use Admin\Model\PenggunaPeer;
use Admin\Model\BentukPendidikanPeer;
use Admin\Model\StatusKepemilikanPeer;
use Admin\Model\PtkPeer;
use Admin\Model\PtkTerdaftarPeer;
use Admin\Model\PesertaDidikPeer;
use Admin\Model\RegistrasiPesertaDidikPeer;
use Admin\Model\AnggotaRombelPeer;
use Admin\Model\RombonganBelajarPeer;
use Admin\Model\TahunAjaranPeer;

class Cetak{

	function get(Request $request, Application $app){

		$model = $request->get('model');
		$today = date('Y-m-d H:i:s');
		$id = $request->get('id');
		$tahun_ajaran_id = $request->get('tahun_ajaran_id') ? $tahun_ajaran_id : TA_BERJALAN;
		$tipe = $request->get('tipe');
		$count = 0;

		$str = '';
		$arr = array();
		$fetchArr = array();

		switch ($model) {
			case 'Mutasi':

				$sql = "SELECT
							mutasi.*,
							jenis_surat.nama as jenis_surat_id_str
						FROM
							mutasi
						JOIN ref.jenis_surat on jenis_surat.jenis_surat_id = mutasi.jenis_surat_id
						WHERE
							mutasi_id = '".$id."'";

				$fetch = Preface::getDataByQuery($sql, 'rekap');

				foreach ($fetch as $f) {

					$arr = $f;

					$sekolah = SekolahPeer::retrieveByPk($f['sekolah_id']);

					if($sekolah){
						$arr['nama_sekolah'] = $sekolah->getNama();
					}else{
						$arr['nama_sekolah'] = '';
					}

					$kode_wilayah = MstWilayahPeer::retrieveByPk($f['kode_wilayah_berangkat']);

					if($kode_wilayah){
						$arr['kode_wilayah_berangkat_str'] = $kode_wilayah->getNama();

					}else{
						$arr['kode_wilayah_berangkat_str'] = '';

					}

					if($f['pengguna_id']){
						$pengguna = PenggunaPeer::retrieveByPk($f['pengguna_id']);

						if($pengguna){
							$arr['jabatan_lembaga'] = $pengguna->getJabatanLembaga();
							$arr['pengguna_id_str'] = $pengguna->getNama();
							$arr['nip_nim'] = $pengguna->getNipNim();
						}else{
							$arr['jabatan_lembaga'] = '';
							$arr['pengguna_id_str'] = '';
							$arr['nip_nim'] = '';
						}
					}


					$count++;

					array_push($fetchArr, $arr);
				}

				// return $sql;die;

				if($tipe == 'pdf'){
					$today = date('Y-m-d h:i:s');

					include "../src/Admin/mpdf/mpdf.php";

					$mpdf=new \mPDF('c');
					$mpdf->SetTitle('Surat Mutasi');
					$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins

					// $footer = '<table><tr>
					// 		<td align="left"><span><i>Surat Mutasi</i></span></td>
					// 		<td align="right" width="100%"></td>
					// 	</tr></table>';

					// $mpdf->SetHTMLFooter($footer);
					// $mpdf->SetHTMLFooter($footer,'E');

					ob_start();
					include "Templates/template-surat-mutasi.php";

					$content = ob_get_clean();

					// $stylesheet = file_get_contents(dirname(__FILE__).D.'Templates'.D.'style.css');
					// $mpdf->imgdisdik = file_get_contents(dirname(__FILE__).D.'Templates'.D.'disdik.png');
					// $mpdf->WriteHTML($stylesheet,1);
					$mpdf->WriteHTML($content);
					$mpdf->Output('Surat-Mutasi-'.$fetchArr[0]['nama'].'-['.$today.'].pdf');
					exit;
				}else{
					include "Templates/template-surat-mutasi.php";

				}

				break;

		}

		return false;
	}

}