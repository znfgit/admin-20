<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Admin\Preface;
use Admin\Model\RombonganBelajarPeer;
use Admin\Model\JurusanSpPeer;
use Admin\Model\PtkPeer;
use Admin\Model\PrasaranaPeer;

class RombonganBelajar
{
	function tesRombel(Request $request, Application $app){
		return "koe sarpras";
	}

	static function RombonganBelajar(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		$arrAll = array();
		$return = array();

		$c = new \Criteria();
		$c->add(RombonganBelajarPeer::SOFT_DELETE, 0);
		$c->add(RombonganBelajarPeer::SEKOLAH_ID, $requesta['sekolah_id']);
		$c->add(RombonganBelajarPeer::SEMESTER_ID, $requesta['semester_id']);
		$c->addAscendingOrderByColumn(RombonganBelajarPeer::JENIS_ROMBEL);
		$c->addAscendingOrderByColumn(RombonganBelajarPeer::TINGKAT_PENDIDIKAN_ID);
		$c->addAscendingOrderByColumn(RombonganBelajarPeer::NAMA);

		$count = RombonganBelajarPeer::doCount($c);

		$c->setOffset($requesta['start']);
		$c->setLimit($requesta['limit']);

		$rombels = RombonganBelajarPeer::doSelect($c);

		foreach ($rombels as $rombel) {
			$arr = array();

			$arr = $rombel->toArray(\BasePeer::TYPE_FIELDNAME);

			// $arr['jurusan_sp_id_str'] = 'testing';
			$jurusanSp = JurusanSpPeer::retrieveByPk($arr['jurusan_sp_id']);
			if($jurusanSp){$arr['jurusan_sp_id_str'] = $jurusanSp->getNamaJurusanSp();}

			$ptk = PtkPeer::retrieveByPk($arr['ptk_id']);
			if($ptk){$arr['ptk_id_str'] = $ptk->getNama();}


			$prasarana = PrasaranaPeer::retrieveByPk($arr['prasarana_id']);
			if($prasarana){$arr['prasarana_id_str'] = $prasarana->getNama();}

			array_push($arrAll, $arr);
		}

		$return['success'] = true;
		$return['results'] = $count;
		$return['rows'] = $arrAll;

		return json_encode($return);
	}

	static function RingkasanRombonganBelajar(Request $request, Application $app) {

		$requesta = Preface::Inisiasi($request);
		$id_level_wilayah = $requesta['id_level_wilayah'];
		$kode_wilayah = $requesta['kode_wilayah'];

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_provinsi}
					{$add_kode_wilayah_induk_provinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					SUM(pd_kelas_1) AS pd_kelas_1,
					SUM(pd_kelas_2) AS pd_kelas_2,
					SUM(pd_kelas_3) AS pd_kelas_3,
					SUM(pd_kelas_4) AS pd_kelas_4,
					SUM(pd_kelas_5) AS pd_kelas_5,
					SUM(pd_kelas_6) AS pd_kelas_6,
					SUM(pd_kelas_7) AS pd_kelas_7,
					SUM(pd_kelas_8) AS pd_kelas_8,
					SUM(pd_kelas_9) AS pd_kelas_9,
					SUM(pd_kelas_10) AS pd_kelas_10,
					SUM(pd_kelas_11) AS pd_kelas_11,
					SUM(pd_kelas_12) AS pd_kelas_12,
					SUM(pd_kelas_13) AS pd_kelas_13
				FROM
					rekap_sekolah
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				{$param_kode_wilayah}
				{$params_bp}
				AND soft_delete = 0
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_provinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_provinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
	}

	static function RingkasanRombonganBelajarSp(Request $request, Application $app) {

		$requesta = Preface::Inisiasi($request);

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY nama) as 'no',
					sekolah_id,
                    nama,
                    npsn,
                    status_sekolah,
                    CASE status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bentuk_pendidikan_id,
                    CASE
                        WHEN bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
					kecamatan,
					SUM(pd_kelas_1) AS pd_kelas_1,
					SUM(pd_kelas_2) AS pd_kelas_2,
					SUM(pd_kelas_3) AS pd_kelas_3,
					SUM(pd_kelas_4) AS pd_kelas_4,
					SUM(pd_kelas_5) AS pd_kelas_5,
					SUM(pd_kelas_6) AS pd_kelas_6,
					SUM(pd_kelas_7) AS pd_kelas_7,
					SUM(pd_kelas_8) AS pd_kelas_8,
					SUM(pd_kelas_9) AS pd_kelas_9,
					SUM(pd_kelas_10) AS pd_kelas_10,
					SUM(pd_kelas_11) AS pd_kelas_11,
					SUM(pd_kelas_12) AS pd_kelas_12,
					SUM(pd_kelas_13) AS pd_kelas_13
				FROM
					rekap_sekolah
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND kode_wilayah_kecamatan = '".$requesta['kode_wilayah']."'
				{$params_bp}
				AND soft_delete = 0
				GROUP BY
					nama,
				    sekolah_id,
				    npsn,
				    status_sekolah,
				    bentuk_pendidikan_id,
					kecamatan,
					kode_wilayah_kecamatan,
					mst_kode_wilayah_kecamatan,
					id_level_wilayah_kecamatan
				ORDER BY
					nama";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
	}

}