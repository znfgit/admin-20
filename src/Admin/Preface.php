<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Preface
{

	static function getOutput($data, $tipe = 'json', $nama_file = 'Unduh_Excel'){
		if($tipe == 'json'){
			return json_encode($data);
		}else if($tipe == 'excel'){
			return ExcelExt::getExcelOutput($data, $nama_file);
		}
	}

	function tesPanggil(Request $request, Application $app){
		return 'tes oke';
	}

	public static function initdb($tipe = 'utama'){

		if($tipe == 'utama'){
			$serverName = DATABASEHOST; //serverName\instanceName
        	$connectionInfo = array( "Database"=>DATABASENAME, "UID"=>DATABASEUSER, "PWD"=>DATABASEPASSWORD,'ReturnDatesAsStrings'=>true);
		}else{
			$serverName = DATABASEHOST_REKAP; //serverName\instanceName
        	$connectionInfo = array( "Database"=>DATABASENAME_REKAP, "UID"=>DATABASEUSER_REKAP, "PWD"=>DATABASEPASSWORD_REKAP,'ReturnDatesAsStrings'=>true);
		}

        $conn = sqlsrv_connect( $serverName, $connectionInfo);

        return $conn;
    }

    public function initdbMysqlHelpdesk(){
        // $serverName = "118.98.233.178"; //serverName\instanceName
        $servername = "118.98.233.178";
		$username = "root";
		$password = "D4p0d!k_$rv1";
		$dbname = 'crm_paudni2';

		$str = '';

		// Create connection
		$conn = new \mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		} 

		$sql = "SELECT * from users";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
		    // output data of each row
		    while($row = $result->fetch_assoc()) {
		        $str .= "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
		    }
		} else {
		    $str .= "0 results";
		}
		$conn->close();

		return $str;		
    }

	public static function Inisiasi($request){

		$return = array();
		
		$return['kode_wilayah'] 		= $request->get('kode_wilayah') ? $request->get('kode_wilayah') : KODE_WILAYAH;
		// $return['level_wilayah'] 		= $request->get('level_wilayah') ? $request->get('level_wilayah') : LEVEL_WILAYAH;
		$return['id_level_wilayah'] 	= $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : ID_LEVEL_WILAYAH;
		$return['semester_id'] 			= $request->get('semester_id') ? $request->get('semester_id') : SEMESTER_BERJALAN;
		$return['tahun_ajaran_id'] 		= $request->get('tahun_ajaran_id') ? $request->get('tahun_ajaran_id') : TA_BERJALAN;
		$return['pengguna_id'] 			= $request->get('pengguna_id');	
		$return['sekolah_id'] 			= $request->get('sekolah_id');

		$return['keyword'] 				= $request->get('keyword') ? $request->get('keyword') : null;	
		$return['propinsi'] 			= $request->get('propinsi') ? $request->get('propinsi') : null;	
		$return['kabupaten'] 			= $request->get('kabupaten') ? $request->get('kabupaten') : null;	
		$return['kecamatan'] 			= $request->get('kecamatan') ? $request->get('kecamatan') : null;	
		$return['bentuk_pendidikan_id'] = $request->get('bentuk_pendidikan_id') ? $request->get('bentuk_pendidikan_id') : null;	
		$return['status_sekolah'] 		= $request->get('status_sekolah')? $request->get('status_sekolah') : null;	
		$return['mode'] 				= $request->get('mode') ? $request->get('mode') : null;	
		$return['peran_id'] 			= $request->get('peran_id') ? $request->get('peran_id') : null;	

		$return['start'] 				= $request->get('start') ? $request->get('start') : 0;	
		$return['limit'] 				= $request->get('limit') ? $request->get('limit') : 20;	
		$return['id'] 					= $request->get('id') ? $request->get('id') : null;	
		$return['pengguna_id'] 			= $request->get('pengguna_id') ? $request->get('pengguna_id') : null;	
		$return['query']	 			= $request->get('query') ? $request->get('query') : null;	
		
		$return['tipe_output'] 			= $request->get('tipe_output') ? $request->get('tipe_output') : 'json';	

		return $return;
	}

	static function getDataByQuery($sql, $tipe = 'utama'){
		$con = Preface::initdb($tipe);
		$return = array();

		$stmt = sqlsrv_query( $con, $sql );
		while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
			array_push($return, $row);
		}

		return $return;
	}

	static function execQuery($sql, $tipe = 'utama'){
		$con = Preface::initdb($tipe);
		$return = array();

		$stmt = sqlsrv_query( $con, $sql );

		if($stmt){
			$return['success'] = true;
		}else{
			$return['success'] = false;
		}

		return $return;
	}

	static function Hapus(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);
		// $class = "\\Admin\\Model\\".$request->get('class')."Peer";
		$return = array();

		// // $pengguna = new Pengguna();
		// $pengguna = ${'class'}::retrieveByPk($requesta['id']);

		// if($pengguna){
		// 	$pengguna->setSoftDelete(1);

		// 	if($pengguna->save()){
		// 		$return['success'] = true;
		// 		$return['message'] = 'Berhasil Menghapus Data';
		// 	}else{
		// 		$return['success'] = false;
		// 		$return['message'] = 'Gagal Menghapus Data';
		// 	}
		// }else{
		// 	$return['success'] = false;
		// 	$return['message'] = 'Gagal Menghapus Data. Pengguna tidak ditemukan';
		// }

		$sql = "update {$request->get('class')} set soft_delete = 1 where {$request->get('class')}_id = '{$requesta['id']}'";

		$return = Preface::execQuery($sql, 'rekap');

		if($return){
			$return['success'] = true;
			$return['message'] = 'Berhasil Menghapus Data';
		}else{
			$return['success'] = false;
			$return['message'] = 'Gagal Menghapus Data';
		}

		return json_encode($return);
	}

	static function getGeoJsonBasic(Request $request, Application $app){

		$kode_wilayah = $request->get('kode_wilayah');
		
		$str = '[';

		//baru
		switch ($request->get('id_level_wilayah')) {
			case 0:
				$col_wilayah = 's.propinsi';
				$group_wilayah_1 = 's.kode_wilayah_propinsi';
				$group_wilayah_2 = 's.id_level_wilayah_propinsi';
				$group_wilayah_3 = 's.mst_kode_wilayah_propinsi';
				$group_wilayah_4 = '';
				$group_wilayah_4_group = '';
				$params_wilayah ='';
				break;
			case 1:
				$col_wilayah = 's.kabupaten';
				$group_wilayah_1 = 's.kode_wilayah_kabupaten';
				$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
				$group_wilayah_3 = 's.mst_kode_wilayah_kabupaten';
				$group_wilayah_4 = 's.mst_kode_wilayah_propinsi AS mst_kode_wilayah_induk,';
				$group_wilayah_4_group = 's.mst_kode_wilayah_propinsi,';
				$params_wilayah = ' AND s.kode_wilayah_propinsi = '.$kode_wilayah;
				break;
			case 2:
				$col_wilayah = 's.kecamatan';
				$group_wilayah_1 = 's.kode_wilayah_kecamatan';
				$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
				$group_wilayah_3 = 's.mst_kode_wilayah_kecamatan';
				$group_wilayah_4 = 's.mst_kode_wilayah_kabupaten AS mst_kode_wilayah_induk,';
				$group_wilayah_4_group = 's.mst_kode_wilayah_kabupaten,';
				$params_wilayah = ' AND s.kode_wilayah_kabupaten = '.$kode_wilayah;
				break;
			default:
				$col_wilayah = 's.propinsi';
				$group_wilayah_1 = 's.kode_wilayah_propinsi';
				$group_wilayah_2 = 's.id_level_wilayah_propinsi';
				$group_wilayah_3 = 's.mst_kode_wilayah_propinsi';
				$group_wilayah_4 = '';
				$group_wilayah_4_group = '';
				$params_wilayah ='';
				break;
		}

		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMASMK'){
				$params_bp = 'AND s.bentuk_pendidikan_id IN (13,15)';
			}else if(BENTUKPENDIDIKAN == 'SMA'){
				$params_bp = 'AND s.bentuk_pendidikan_id = 13';
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$params_bp = 'AND s.bentuk_pendidikan_id = 15';
			}else{
				$params_bp = 'AND s.bentuk_pendidikan_id IN (13,15)';
			}

		}else if(TINGKAT == 'DIKDAS'){

			$params_bp = 'AND s.bentuk_pendidikan_id IN (5,6,14,29)';

		}else if(TINGKAT == 'DIKDASMEN'){

			$params_bp = 'AND s.bentuk_pendidikan_id IN (5,6,14,29,13,15)';

		}else if(TINGKAT == 'DIKDASMEN-PAUD'){

			$params_bp = 'AND s.bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29,13,15)';

		}

		$sql = "SELECT
				{$col_wilayah} AS nama,
				{$group_wilayah_1} AS kode_wilayah,
				{$group_wilayah_2} AS id_level_wilayah,
				{$group_wilayah_3} AS mst_kode_wilayah,
				{$group_wilayah_4}
				sum(s.pd) as pd,
				sum(s.guru) as ptk,
				sum(s.guru + s.pegawai) as ptk_total,
				sum(s.pegawai) as pegawai,
				sum(s.rombel) as rombel,
				sum(1) as sekolah
			FROM
				rekap_sekolah s
			where 
				s.semester_id = 20162
				{$params_bp}
				{$params_wilayah}
				AND s.soft_delete = 0
			GROUP BY
				{$group_wilayah_1},
				{$group_wilayah_2},
				{$group_wilayah_3},
				{$group_wilayah_4_group}
				{$col_wilayah}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		// return json_encode($return);die;

		// $host = '223.27.152.200:640';
		$host = 'siduren.dikdasmen.kemdikbud.go.id';

		foreach ($return as $rw) {
			$geometry = @file_get_contents('http://'.$host.'/geoNew/'.substr($rw['kode_wilayah'],0,6).'.txt', true);

			if(substr($geometry, 0, 4) == '[[[['){
				$geometry = substr($geometry, 1, strlen($geometry)-2);
			}

			if(!array_key_exists('mst_kode_wilayah_induk', $rw) ){
				$induk = null;
			}else{
				$induk = $rw['mst_kode_wilayah_induk'];
			}

			$str .= '{
			    "type": "Feature",
			    "geometry": {
			        "type": "MultiPolygon",
			        "coordinates": [
			            '.$geometry.'
			        ]
			    },
			    "properties": {
			        "kode_wilayah": "'.substr($rw['kode_wilayah'],0,6).'",
			        "id_level_wilayah": "'.$rw['id_level_wilayah'].'",
			        "mst_kode_wilayah": "'.$rw['mst_kode_wilayah'].'",
			        "mst_kode_wilayah_induk": "'.$induk.'",
			        "name": "'.$rw['nama'].'",
			        "pd": '.$rw['pd'].',
			        "guru": '.$rw['ptk'].',
			        "pegawai": '.$rw['pegawai'].',
			        "rombel": '.$rw['rombel'].',
			        "sekolah": '.$rw['sekolah'].'
			    }
			},';

		}

		$str .= ']';
		
		return $str;

		//end of baru

		// $sql_wilayah = "select * from ref.mst_wilayah where mst_kode_wilayah = '".$request->get('kode_wilayah')."'";
		// // return $sql_wilayah;die;
		// $return_wilayah = Preface::getDataByQuery($sql_wilayah, 'utama');

		// // $host = 'pmp_dikdasmen';
		// $host = '223.27.152.200:640';
		// $host = 'siduren.dikdasmen.kemdikbud.go.id';

		// foreach ($return_wilayah as $rw) {

		// 	switch ($request->get('id_level_wilayah')) {
		// 		case 0:
		// 			$col_wilayah = 's.propinsi';
		// 			$group_wilayah_1 = 's.kode_wilayah_propinsi';
		// 			$group_wilayah_2 = 's.id_level_wilayah_propinsi';
		// 			$params_wilayah ='';
		// 			break;
		// 		case 1:
		// 			$col_wilayah = 's.kabupaten';
		// 			$group_wilayah_1 = 's.kode_wilayah_kabupaten';
		// 			$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
		// 			$params_wilayah = ' and s.kode_wilayah_kabupaten = '.$rw['kode_wilayah'];
		// 			break;
		// 		case 2:
		// 			$col_wilayah = 's.kecamatan';
		// 			$group_wilayah_1 = 's.kode_wilayah_kecamatan';
		// 			$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
		// 			$params_wilayah = ' and s.kode_wilayah_kecamatan = '.$rw['kode_wilayah'];
		// 			break;
		// 		default:
		// 			$col_wilayah = 's.propinsi';
		// 			$group_wilayah_1 = 's.kode_wilayah_propinsi';
		// 			$group_wilayah_2 = 's.id_level_wilayah_propinsi';
		// 			$params_wilayah ='';
		// 			break;
		// 	}

		// 	if(TINGKAT == 'DIKMEN'){

		// 		if(BENTUKPENDIDIKAN == 'SMASMK'){
		// 			$params_bp = 'AND s.bentuk_pendidikan_id IN (13,15)';
		// 		}else if(BENTUKPENDIDIKAN == 'SMA'){
		// 			$params_bp = 'AND s.bentuk_pendidikan_id = 13';
		// 		}else if(BENTUKPENDIDIKAN == 'SMK'){
		// 			$params_bp = 'AND s.bentuk_pendidikan_id = 15';
		// 		}else{
		// 			$params_bp = 'AND s.bentuk_pendidikan_id IN (13,15)';
		// 		}

		// 	}else if(TINGKAT == 'DIKDAS'){

		// 		$params_bp = 'AND s.bentuk_pendidikan_id IN (5,6,14,29)';

		// 	}else if(TINGKAT == 'DIKDASMEN'){

		// 		$params_bp = 'AND s.bentuk_pendidikan_id IN (5,6,14,29,13,15)';

		// 	}else if(TINGKAT == 'DIKDASMEN-PAUD'){

		// 		$params_bp = 'AND s.bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29,13,15)';

		// 	}

		// 	$sql = "SELECT
		// 			{$col_wilayah} AS nama,
		// 			{$group_wilayah_1} AS kode_wilayah,
		// 			{$group_wilayah_2} AS id_level_wilayah,
		// 			sum(s.pd) as pd,
		// 			sum(s.guru) as ptk,
		// 			sum(s.guru + s.pegawai) as ptk_total,
		// 			sum(s.pegawai) as pegawai,
		// 			sum(s.rombel) as rombel,
		// 			sum(1) as sekolah
		// 		FROM
		// 			rekap_sekolah s
		// 		where 
		// 			s.semester_id = 20161
		// 			{$params_wilayah}
		// 			{$params_bp}
		// 			AND s.soft_delete = 0
		// 		GROUP BY
		// 			{$group_wilayah_1},
		// 			{$group_wilayah_2},
		// 			{$col_wilayah}";		

		// 	// return $sql;die;

		// 	$return = Preface::getDataByQuery($sql, 'rekap');
			

		// 	$geometry = @file_get_contents('http://'.$host.'/geoNew/'.substr($rw['kode_wilayah'],0,6).'.txt', true);

		// 	if(substr($geometry, 0, 4) == '[[[['){
		// 		$geometry = substr($geometry, 1, strlen($geometry)-2);
		// 	}

		// 	$str .= '{
		// 	    "type": "Feature",
		// 	    "geometry": {
		// 	        "type": "MultiPolygon",
		// 	        "coordinates": [
		// 	            '.$geometry.'
		// 	        ]
		// 	    },
		// 	    "properties": {
		// 	        "kode_wilayah": "'.substr($rw['kode_wilayah'],0,6).'",
		// 	        "name": "'.$rw['nama'].'",
		// 	        "pd": '.$return[0]['pd'].',
		// 	        "guru": '.$return[0]['ptk'].',
		// 	        "pegawai": '.$return[0]['pegawai'].',
		// 	        "rombel": '.$return[0]['rombel'].',
		// 	        "sekolah": '.$return[0]['sekolah'].'
		// 	    }
		// 	},';

		// }

		// $str .= ']';
		
		// return $str;
		
	}

	static function getGeoJsonNew(Request $request, Application $app){
		
		// $sql = "select * from mst_wilayah where level_wilayah_id = 1 and expired_date is null and kode_wilayah not in (350000) ";
		switch ($request->get('id_level_wilayah')) {
			case 0:
				$col_wilayah = 's.propinsi';
				$group_wilayah_1 = 's.kode_wilayah_propinsi';
				$group_wilayah_2 = 's.id_level_wilayah_propinsi';
				$params_wilayah ='';
				break;
			case 1:
				$col_wilayah = 's.kabupaten';
				$group_wilayah_1 = 's.kode_wilayah_kabupaten';
				$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
				$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$request->get('kode_wilayah');
				break;
			case 2:
				$col_wilayah = 's.kecamatan';
				$group_wilayah_1 = 's.kode_wilayah_kecamatan';
				$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
				$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$request->get('kode_wilayah');
				break;
			default:
				$col_wilayah = 's.propinsi';
				$group_wilayah_1 = 's.kode_wilayah_propinsi';
				$group_wilayah_2 = 's.id_level_wilayah_propinsi';
				$params_wilayah ='';
				break;
		}

		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMASMK'){
				$params_bp = 'AND s.bentuk_pendidikan_id IN (13,15)';
			}else if(BENTUKPENDIDIKAN == 'SMA'){
				$params_bp = 'AND s.bentuk_pendidikan_id = 13';
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$params_bp = 'AND s.bentuk_pendidikan_id = 15';
			}else{
				$params_bp = 'AND s.bentuk_pendidikan_id IN (13,15)';
			}

		}else if(TINGKAT == 'DIKDAS'){

			$params_bp = 'AND s.bentuk_pendidikan_id IN (5,6,14,29)';

		}else if(TINGKAT == 'DIKDASMEN'){

			$params_bp = 'AND s.bentuk_pendidikan_id IN (5,6,14,29,13,15)';

		}else if(TINGKAT == 'DIKDASMEN-PAUD'){

			$params_bp = 'AND s.bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29,13,15)';

		}

		$sql = "SELECT
					{$col_wilayah} AS nama,
					{$group_wilayah_1} AS kode_wilayah,
					{$group_wilayah_2} AS id_level_wilayah,
					sum(s.pd_kelas_10) as pd_kelas_10,
					sum(s.pd_kelas_11) as pd_kelas_11,
					sum(s.pd_kelas_12) as pd_kelas_12,
					sum(s.pd_kelas_13) as pd_kelas_13,
					sum(s.pd) as pd,
					sum(s.guru) as ptk,
					sum(s.guru + s.pegawai) as ptk_total,
					sum(s.pegawai) as pegawai,
					sum(s.rombel) as rombel,
					sum(1) as sekolah
				FROM
					rekap_sekolah s
				where 
					s.semester_id = 20161
					{$params_wilayah}
					{$params_bp}
					AND s.soft_delete = 0
				GROUP BY
					{$group_wilayah_1},
					{$group_wilayah_2},
					{$col_wilayah}";		

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		// $host = 'pmp_dikdasmen';
		// $host = '223.27.152.200:640';
		$host = 'siduren.dikdasmen.kemdikbud.go.id';

		$str = 'var statesData = {"type":"FeatureCollection","features":[';

		foreach ($return as $ret) {
			
			// return 'http://'.$host.'/geoNew/'.substr($ret['kode_wilayah'],0,6).'.txt';die;
			$geometry = @file_get_contents('http://'.$host.'/geoNew/'.substr($ret['kode_wilayah'],0,6).'.txt', true);

			if(substr($geometry, 0, 4) == '[[[['){
				$geometry = substr($geometry, 1, strlen($geometry)-2);
			}

			$str .= '{
				    "type": "Feature",
				    "geometry": {
				        "type": "MultiPolygon",
				        "coordinates": [
				            '.$geometry.'
				        ]
				    },
				    "properties": {
				        "kode_wilayah": "'.substr($ret['kode_wilayah'],0,6).'",
				        "name": "'.$ret['nama'].'",
				        "pd": '.$ret['pd'].',
				        "guru": '.$ret['ptk'].',
				        "pegawai": '.$ret['pegawai'].',
				        "rombel": '.$ret['rombel'].',
				        "sekolah": '.$ret['sekolah'].'
				    }
				},';

		}

		$str .= ']}';
		
		return $str;
	}

	static function parameterBentukPendidikan($bentuk_pendidikan_id = null, $alias = null){
		if(TINGKAT == 'DIKMEN'){
			if(BENTUKPENDIDIKAN == 'SMASMK'){
				$params_bp = 'AND '.$alias.'bentuk_pendidikan_id IN (13,15)';
			}else if(BENTUKPENDIDIKAN == 'SMA'){
				$params_bp = 'AND '.$alias.'bentuk_pendidikan_id = 13';
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$params_bp = 'AND '.$alias.'bentuk_pendidikan_id = 15';
			}else{
				$params_bp = 'AND '.$alias.'bentuk_pendidikan_id IN (13,15)';
			}
		}else if(TINGKAT == 'DIKDAS'){
			$params_bp = 'AND '.$alias.'bentuk_pendidikan_id IN (5,6,14,29)';
		}else if(TINGKAT == 'DIKDASMEN'){
			$params_bp = 'AND '.$alias.'bentuk_pendidikan_id IN (5,6,14,29,13,15)';
		}else if(TINGKAT == 'DIKDASMEN-PAUD'){
			$params_bp = 'AND '.$alias.'bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29,13,15)';
		}else{
			$params_bp = '';
		}

		if($bentuk_pendidikan_id){
			$params_bp = 'AND '.$alias.'bentuk_pendidikan_id = '.$bentuk_pendidikan_id;
		}

		return $params_bp;
	}
}
?>