<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Admin\Preface;
use Admin\Sarpras;

class ExcelExt
{	

	function tesExcel(Request $request, Application $app){

		$objPHPExcel = new \PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Khalid Saifuddin")
									 ->setLastModifiedBy("Khalid Saifuddin")
									 ->setTitle("Kode Registrasi")
									 ->setSubject("Kode Registrasi")
									 ->setDescription("daftar kode registrasi per satuan pendidikan")
									 ->setKeywords("office 2007 openxml php")
									 ->setCategory("result file");

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition: attachment;filename="Daftar Kode Registrasi - '.date('Y-m-d H:i:s').'.xlsx"');
		header('Content-Disposition: attachment;filename="testing_'.date('Y-m-d H:i:s').'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');

		return exit;

	}

	function getExcel(Request $request, Application $app){

		$class = "\\Admin\\".$request->get('class');
		$method = $request->get('method');

        $request->query->set('limit', 200000000);

		// Get Data
		$return  = ${'class'}::${'method'}($request,$app);
		// $data = $request->get('id_level_wilayah') == 3 ? json_decode($return)->rows : json_decode($return);
        $data = isset(json_decode($return)->rows) ? json_decode($return)->rows : json_decode($return);

		// return $return; die;

        // Initiation
        $today = date('d-m-Y H:i:s');

        if(isset(json_decode($return)->rows)) {
            if($request->get('id_level_wilayah') == 1){

                // $kecamatan = isset($data[0]->kecamatan) ? $data[0]->kecamatan : '';
                $kecamatan = isset($data[0]->propinsi) ? $data[0]->propinsi : '';
            }else if($request->get('id_level_wilayah') == 2){
                $kecamatan = isset($data[0]->kabupaten) ? $data[0]->kabupaten : '';
            }else if($request->get('id_level_wilayah') == 3){
                $kecamatan = isset($data[0]->kecamatan) ? $data[0]->kecamatan : '';
            }else{
                $kecamatan = isset($data[0]->kecamatan) ? $data[0]->kecamatan : '';
            }
        }else{
            if($request->get('id_level_wilayah') == 1){
                $kecamatan = $data[0]->induk_propinsi;
            }else if($request->get('id_level_wilayah') == 2){
                $kecamatan = $data[0]->induk_kabupaten;
            }else if($request->get('id_level_wilayah') == 3){
                $kecamatan = $data[0]->induk_kecamatan;
            }else{
                $kecamatan = 'Indonesia';
            }
        }

        $cleanMethod = str_replace('Sp', '', str_replace(array('Pd', 'Ptk', 'SatuanPendidikan'), '', $method));
        $namaClass = str_replace('Ptk', 'GTK', preg_replace('/(?<!\ )[A-Z]/', ' $0', $request->get('class')));
        $namaMethod = preg_replace('/(?<!\ )[A-Z]/', ' $0', $cleanMethod);

        $bordered = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $borderedBold = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'font' => array(
                'bold' => true,
            )
        );
        $centerBorderedBold = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'font' => array(
                'bold' => true,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $styleHeader = array(
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb'=>'CFD8DC'),
            ),
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        // Limit data
        for($i = 0; $i < sizeof($data); $i++) {
            unset(
                $data[$i]->sekolah_id,
                $data[$i]->status_sekolah,
                $data[$i]->bentuk_pendidikan_id,
                $data[$i]->kecamatan,
                $data[$i]->kode_wilayah_kecamatan,
                $data[$i]->kode_wilayah,
                $data[$i]->mst_kode_wilayah_kecamatan,
                $data[$i]->mst_kode_wilayah,
                $data[$i]->mst_kode_wilayah_induk,
                $data[$i]->id_level_wilayah,
                $data[$i]->id_level_wilayah_kecamatan,
                $data[$i]->induk_propinsi,
                $data[$i]->kode_wilayah_induk_propinsi,
                $data[$i]->induk_kabupaten,
                $data[$i]->kode_wilayah_induk_kabupaten
            );
        }

        // Load template and set meta data
        if(KODE_WILAYAH != '000000'){
            $objPHPExcel = \PHPExcel_IOFactory::load(__DIR__."/Templates/template-rekap-" . KODE_WILAYAH .".xlsx");
        }else{
            $objPHPExcel = \PHPExcel_IOFactory::load(__DIR__."/Templates/template-rekap.xlsx");
        }

		$objPHPExcel->getProperties()
                    ->setCreator(BOSNYA)
                    ->setLastModifiedBy(BOSNYA)
                    ->setTitle($namaMethod)
                    ->setSubject($namaClass)
                    ->setDescription("Unduh Excel")
                    ->setKeywords("Dapodik, Simdik")
                    ->setCategory("Rekap");

        // Set Heading
        $nama_wilayah = $request->get('id_level_wilayah') == 3 ? BOSNYA . ', ' . $kecamatan . ' - ' : BOSNYA;
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'Rekap' . $namaMethod . $namaClass);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', $nama_wilayah);
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A4', 'Tanggal unduh: ' . $today);

        // Set auto cell width
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        // Insert Data
        $row = 8;
        $arrJudul = array();

        foreach ($data as $key => $value) {
			$col = 0;

			foreach ($value as $val => $vals) {
				if($row == 8){
					array_push($arrJudul, $val);
				}
				
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( $col, $row, $vals );
                // $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->applyFromArray($bordered);
       		 	$col++;
			}

			$row++;
		}

        // Merge Heading
        $last_col = ord('A') + (sizeof($arrJudul) - 1);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2' . ':' . chr($last_col) . '2');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3' . ':' . chr($last_col) . '3');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4' . ':' . chr($last_col) . '4');

        $column = 'A';
		for ($i=0; $i < sizeof($arrJudul); $i++) {

            // Insert Header
            $objPHPExcel->getActiveSheet()->getStyle($column . '7')->getAlignment()->setWrapText(true);
            // $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($i, 7, str_replace("_"," ",strtoupper($arrJudul[$i])));
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($i, 7)->applyFromArray($styleHeader);

            // Insert Total
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($i, $row, '=SUM(' . $column . '8:' . $column . ($row - 1) . ')');
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($i, $row)->applyFromArray($borderedBold);

            $column++;
        }

        // Insert Total Label
        $totalCell = $request->get('id_level_wilayah') == 3 ? 'E' : 'B';
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $row, 'Total');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $row . ':' . $totalCell . $row );
        $objPHPExcel->setActiveSheetIndex(0)->getStyle('A' . $row . ':' . $totalCell . $row)->applyFromArray($centerBorderedBold);

        /*
         * Printing Config
         * */

        // Set header and footer. When no different headers for odd/even are used, odd header is assumed.
        // $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&G&C&HPlease treat this document as confidential!');
        $objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . BOSNYA . '&RHalaman &P dari &N');

        // Set page orientation and size
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        /*
         * Saving
         * */

        // Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // $judul_wilayah = $request->get('id_level_wilayah') == 3 ? ' ' . $kecamatan . ' - ' : ' - ' . KODE_WILAYAH_STR;
		$judul_wilayah = ' ' . $kecamatan . ' - ';
        header('Content-Disposition: attachment;filename="Rekap' . $namaMethod. $namaClass . $judul_wilayah . ' ('.date('Y-m-d H:i:s').').xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');

		return exit;

	}

    static function getExcelOutput($data, $nama_file){
        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Khalid Saifuddin")
                                     ->setLastModifiedBy("Khalid Saifuddin")
                                     ->setTitle("unduhan")
                                     ->setSubject("unduhan")
                                     ->setDescription("unduhan")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("unduhan");

        //datanya di proses dari sini
        
        $row = 2;
        $arrJudul = array();

        foreach ($data['rows'] as $key => $value) {
            $col = 0;

            foreach ($value as $val => $vals) {
                
                if (strpos($val, 'id_') !== false || strpos($val, '_id') !== false || strpos($val, 'induk_') !== false) {
                    //skip
                }else{
                    
                    if($row == 2){
                        array_push($arrJudul, $val);
                    }

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( $col, $row, $vals );
                    $col++;
                }

            }   

            $row++;
        }

        for ($i=0; $i < sizeof($arrJudul); $i++) { 
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($i, 1, str_replace("_"," ",strtoupper($arrJudul[$i])));
        }

        //datanya selesai di proses di sini

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment;filename="Daftar Kode Registrasi - '.date('Y-m-d H:i:s').'.xlsx"');
        header('Content-Disposition: attachment;filename="'.$nama_file.'_'.date('Y-m-d H:i:s').'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

        return true;


    }

}