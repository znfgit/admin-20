<?php
foreach ($fetchArr as $f) {

?>
<style type="text/css">
.container{
	width: 600px;
	margin:auto;
	border:0px solid #ccc;
	padding:0px;
}
.header_logo{
	border:0px solid #ccc;
	width:14%;
	height:100px;
	float:left;
	text-align: center;
	/*background: url(../../../resources/img/header.jpg) no-repeat;*/
	/*background-size: covntain;
	background-position: center;*/ 
}
.header_text{
	border: 0px solid #ccc;
	width:85%;
	float:right;
	height:100px;
	text-align: center;
}
.clear{
	clear: both;
}
.header1{
	font-size: 15px;
}
.header2{
	font-size: 30px;
	font-weight: bold;
}
.header3{
	font-size: 12px;
}
.header_border{
	margin-top: 10px;
	margin-bottom: 10px;
	border:3px solid #000000;
}
.judul{
	font-weight: bold;
	text-decoration: underline;
	margin: auto;
	width: 100%;
	text-align: center;
}
.nomor{
	text-align: center;
	margin-bottom: 10px;
}

p{
	margin-bottom:15px;
}

@media screen {
	.container{
		border:0px solid #ccc;
		/*padding:35px;*/
	}
}

@media screen and (max-width: 1000px) {
	.container{
		width: 90%;
	}
}

</style>
<div class="container">
	<div class="header_logo">
		<img src="../../../resources/images/header.jpg" height="100px" />
	</div>
	<div class="header_text">
		<div class="header1">
			Pemerintah Kota Batam
		</div>
		<div class="header2">
			DINAS PENDIDIKAN
		</div>
		<div class="header3">
			Jl. Pramuka Telp. (0778) 322569 Fax. (0778) 324442 
		</div>
		<div class="header3">
			Sekupang - Batam Kode Pos 29422 
		</div>
	</div>
	<div class="clear"></div>
	<div class="header_border">
	</div>
	<div style="font-size:12px;padding-top:0px;line-height:20px">
		<div class="judul">
			SURAT <?php echo $f['jenis_surat_id_str']; ?>
		</div>
		<div class="nomor">
			Nomor: <?php echo $f['nomor_surat']; ?>
		</div>
		<div>
			<p>
				Sesuai Undang - undang Republik Indonesia Nomor : 32 Tahun 2004 tentang Pemerintah Daerah bahwa pengelolaan Pendidikan menjadi tanggung jawab Pemerintah Kabupaten / Kota, maka Kepala Dinas Pendidikan Kota Batam dengan ini menerangkan pelajar di bawah ini :
			</p>

			<div style="float:left;border:0px solid #ccc;padding-top:0px;padding-bottom:5px;width:4%">
				I
			</div>
			<div style="float:right;border:0px solid #ccc;width:95%">
				<div style="float:left;border:0px solid #ccc;width:30%">
					1. Nama
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<font style="font-weight:bold"><?php echo $f['nama']; ?></font>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					2. Tempat Tanggal Lahir
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['tempat_lahir']; ?>, <?php echo $f['tanggal_lahir']; ?>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					3. Asal Sekolah / Kelas
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['nama_sekolah']; ?> / <?php echo $f['kelas']; ?>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					4. NIS / NISN
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['NIS']; ?> / <?php echo $f['NISN']; ?>
				</div>
				<div class="clear"></div>

			</div>
			<div class="clear"></div>
			<br>
			<div style="float:left;border:0px solid #ccc;padding-top:0px;padding-bottom:5px;width:4%">
				II
			</div>
			<div style="float:right;border:0px solid #ccc;width:95%">
				<div style="float:left;border:0px solid #ccc;width:30%">
					1. Nama Orang Tua / Wali
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['nama_ayah']; ?>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					2. Pekerjaan
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['pekerjaan']; ?>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					3. Alamat
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['alamat_jalan']; ?>
				</div>
				<div class="clear"></div>

			</div>
			<div class="clear"></div>
			<br>
			<div style="float:left;border:0px solid #ccc;padding-top:0px;padding-bottom:5px;width:4%">
				III
			</div>
			<div style="float:right;border:0px solid #ccc;width:95%">
				<div style="float:left;border:0px solid #ccc;width:30%">
					1. Akan Berangkat Ke
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<font style="font-weight:bold"><?php echo $f['kode_wilayah_berangkat_str']; ?></font>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					2. Maksud dan Tujuan
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['maksud_tujuan']; ?>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					3. Nama yang Diikuti
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['nama_yang_diikuti']; ?>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					4. Hubungan Keluarga
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['hubungan_keluarga']; ?>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					5. Pekerjaan
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['pekerjaan']; ?>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					6. Alamat / Tempat Tinggal
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['alamat_yang_diikuti']; ?>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					7. Berangkat Pada Tanggal
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['tanggal_berangkat']; ?>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					8. Keterangan Kepala Sekolah
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['keterangan_kepala_sekolah']; ?>
				</div>
				<div class="clear"></div>

				<div style="float:left;border:0px solid #ccc;width:30%">
					9. Keterangan Lain-lain
				</div>
				<div style="float:left;border:0px solid #ccc;width:5%">
					:
				</div>
				<div style="float:right;border:0px solid #ccc;width:62%">
					<?php echo $f['keterangan_lain']; ?>
				</div>
				<div class="clear"></div>

				
			</div>
			<div class="clear"></div>

			<p>
				Demikianlah Surat Keterangan ini dibuat dengan sebenarnya dan diberikan kepada yang bersangkutan untuk dapat digunakan seperlunya.
			</p>

			<div style="float:right;border:0px solid #ccc;width:310px;text-align:left">
				<div style="width:46%;border:0px solid #ccc;float:left">
					DIKELUARKAN DI :
				</div>
				<div style="float:right;width:52%;border:0px solid #ccc;">
					BATAM
				</div>
				<div class="clear"></div>

				<div style="width:46%;border:0px solid #ccc;float:left">
					PADA TANGGAL :
				</div>
				<div style="float:right;width:52%;border:0px solid #ccc;">
					<?php echo DateToIndoReversed($f['tanggal_surat']); ?>
				</div>
				<div class="clear"></div>	
				<hr>
				<div style="text-align:center;margin-top:10px">
					<?php echo $f['jabatan_lembaga']; ?>
					<br>
					<br>
					<br>
					<br>
					<br>
					<font style="text-decoration:underline"><b><?php echo $f['pengguna_id_str']; ?></b></font><br>
					NIP. <?php echo $f['nip_nim']; ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		
	</div>
</div>

<?php

}



?>