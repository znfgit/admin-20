<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomQuery{

	static function kriteriaSekolahIndividu(Request $request, Application $app){

		$return = array();

		$return[0]['text'] = 'Data Pokok Sekolah';
		$return[0]['cls'] = 'folder';
		$return[0]['expanded'] = true;
		$return[0]['urutan'] = 1;
		$return[0]['children'] = array();

		$sql = "SELECT column_name
				FROM INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_NAME = N'sekolah'";

		$fetch = Preface::getDataByQuery($sql,'rekap');

		for ($i=0; $i < sizeof($fetch); $i++) { 
			$kolom = array();

			$mystring = $fetch[$i]['column_name'];
			$findme   = '_id';
			$pos = strpos($mystring, $findme);

			$kolom_skip = array("flag", "Last_update", "Soft_delete", "last_sync", "Updater_ID", "kode_registrasi", "kode_wilayah");

			// Note our use of ===.  Simply == would not work as expected
			// because the position of 'a' was the 0th (first) character.
			if ($pos === false) {

				if(in_array($fetch[$i]['column_name'], $kolom_skip)){
					//skip
				}else{

					$kolom['text'] = str_replace("_", " ", $fetch[$i]['column_name']);
					$kolom['itemId'] = 'sekolah-'.$fetch[$i]['column_name'];
					$kolom['leaf'] = true;
					$kolom['urutan'] = ($i+1);
					$kolom['checked'] = false;

					$kolom_combo = array("status_sekolah", "bentuk_pendidikan", "kecamatan", "kabupaten", "propinsi", "status_kepemilikan");

					if(in_array($fetch[$i]['column_name'], $kolom_combo)){
						$kolom['tipe_combo'] = true;
					}else{
						$kolom['tipe_combo'] = false;
					}

					array_push($return[0]['children'], $kolom);
				
				}

			}

		}

		// $return[1]['text'] = 'Data Longitudinal';
		// $return[1]['cls'] = 'folder';
		// $return[1]['expanded'] = true;
		// $return[1]['urutan'] = 2;
		// $return[1]['children'] = array();

		return json_encode($return);

	}

	static function kriteriaSekolahIndividuTurunan(Request $request, Application $app){

		$return = array();

		$jenis = $request->get('jenis');

		$return[0]['text'] = 'Data';
		$return[0]['cls'] = 'folder';
		$return[0]['expanded'] = true;
		$return[0]['urutan'] = 1;
		$return[0]['children'] = array();

		$sql = "SELECT column_name
				FROM INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_NAME = N'rekap_sekolah'";

		$fetch = Preface::getDataByQuery($sql,'rekap');

		for ($i=21; $i < sizeof($fetch); $i++) { 
			$kolom = array();

			$mystring = $fetch[$i]['column_name'];
			$findme   = '_id';
			$pos = strpos($mystring, $findme);

			$kolom_skip = array("flag", "Last_update", "Soft_delete", "last_sync", "Updater_ID", "kode_registrasi", "kode_wilayah");

			// Note our use of ===.  Simply == would not work as expected
			// because the position of 'a' was the 0th (first) character.
			if ($pos === false) {

				if(in_array($fetch[$i]['column_name'], $kolom_skip)){
					//skip
				}else{

					if($jenis){

						if($jenis == 'gtk'){
							$pos_jenis = strpos($fetch[$i]['column_name'], 'guru');
						}else{
							$pos_jenis = strpos($fetch[$i]['column_name'], $jenis);
						}


						if ($pos_jenis === false) {
							//skip
						}else{
							$kolom['text'] = str_replace("pegawai", "tendik", (str_replace("_", " ", $fetch[$i]['column_name'])));
							$kolom['itemId'] = 'rekap-'.$fetch[$i]['column_name'];
							$kolom['leaf'] = true;
							$kolom['urutan'] = ($i+1);
							$kolom['checked'] = false;

							$kolom_combo = array("status_sekolah", "bentuk_pendidikan", "kecamatan", "kabupaten", "propinsi", "status_kepemilikan");

							if(in_array($fetch[$i]['column_name'], $kolom_combo)){
								$kolom['tipe_combo'] = true;
							}else{
								$kolom['tipe_combo'] = false;
							}

							array_push($return[0]['children'], $kolom);
						
						}

					}else{

						$kolom['text'] = str_replace("_", " ", $fetch[$i]['column_name']);
						$kolom['itemId'] = 'rekap-'.$fetch[$i]['column_name'];
						$kolom['leaf'] = true;
						$kolom['urutan'] = ($i+1);
						$kolom['checked'] = false;

						$kolom_combo = array("status_sekolah", "bentuk_pendidikan", "kecamatan", "kabupaten", "propinsi", "status_kepemilikan");

						if(in_array($fetch[$i]['column_name'], $kolom_combo)){
							$kolom['tipe_combo'] = true;
						}else{
							$kolom['tipe_combo'] = false;
						}

						array_push($return[0]['children'], $kolom);
					
					}

				}

			}

		}

		return json_encode($return);

	}

	static function prosesCustomQuery(Request $request, Application $app){

		// return "oke";die;

		$tipe_output = $request->get('tipe_output') ? $request->get('tipe_output') : 'json'; 

		$kolom = '';
		$param = '';

		$kolom_list = array();

		foreach ($request->query as $key => $value) {
			
			$key = str_replace("-", ".", $key);

			if($key != '_dc' && $key != 'tipe_output'){
				$keyArr = explode(".", $key);

				$kolomArr = array();
				$kolomArr['name'] = $key;
				$kolomArr['type'] = 'string';
				$kolomArr['text'] = str_replace("_", " ", $keyArr[1] );

				array_push($kolom_list, $kolomArr);
				$kolom .= ', '.$key;
				
				$valueEx = explode('|', $value);

				if(sizeof($valueEx) > 1){
					$value = $valueEx[0];
					$value_kriteria = $valueEx[1];
				}else{
					$value = '';
					$value_kriteria = $valueEx[0];
				}


				if($value_kriteria != 'ISNULL' && $value_kriteria != 'ISNOTNULL'){


					if($value != '' && $value != 'null'){


						// $value = str_replace("Kec. ", "", $value);
						// $value = str_replace("Kab. ", "", $value);
						// $value = str_replace("Prop. ", "", $value);

						if($key == 'sekolah.bentuk_pendidikan'){
							$key = 'sekolah.bentuk_pendidikan_id';
						}

						switch ($value_kriteria) {
							case 'EQUAL':
								$param .= " AND ".$key." = '".$value."'";
								break;
							case 'LIKE':
								$param .= " AND ".$key." like '%".$value."%'";
								break;					
							case 'LESSTHAN':
								$param .= " AND ".$key." < ".$value."";
								break;
							case 'LESSTHANEQUAL':
								$param .= " AND ".$key." <= ".$value."";
								break;
							case 'GREATERTHAN':
								$param .= " AND ".$key." > ".$value."";
								break;
							case 'GREATERTHANEQUAL':
								$param .= " AND ".$key." >= ".$value."";
								break;
							case 'ISNOTNULL':
								$param .= " AND ".$key." is not null";
								break;
							case 'ISNULL':
								$param .= " AND ".$key." is null";
								break;
							default:
								$param .= " AND ".$key." like '%".$value."%'";
								break;
						}

					}

				}else{
					switch ($value_kriteria) {
						case 'ISNOTNULL':
							$param .= " AND ".$key." is not null";
							break;
						case 'ISNULL':
							$param .= " AND ".$key." is null";
							break;
						default:
							$param .= " AND ".$key." is not null";
							break;
					}
				}
				
			}


		}

		$kolom = substr($kolom, 1, strlen($kolom));

		if(strlen($kolom) > 0){
			$sql = 'select 
						'.$kolom.' 
					from 
						sekolah
					JOIN rekap_sekolah rekap on rekap.sekolah_id = sekolah.sekolah_id and rekap.semester_id = 20162 
					where 
						sekolah.Soft_delete = 0
					AND rekap.Soft_delete = 0
					'.$param; 
		}else{
			$sql = 'select 
						sekolah.nama 
					from 
						sekolah
					JOIN rekap_sekolah rekap on rekap.sekolah_id = sekolah.sekolah_id and rekap.semester_id = 20162
					where 
						sekolah.Soft_delete = 0
					AND rekap.Soft_delete = 0
					'.$param; 
		}

		// return $sql;die;


		$return = Preface::getDataByQuery($sql, 'rekap');

		$finalArr = array();
		$finalArr['kolom'] = $kolom_list;
		$finalArr['rows'] = $return;


		// if($tipe_output == 'json'){
		// 	return json_encode($finalArr);
		// }else{
		return Preface::getOutput($finalArr, $tipe_output);
		// }

		// return var_dump($request->query);
	}
}

