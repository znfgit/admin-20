<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Admin\Preface;
use Admin\Model\PenggunaPeer;
use Admin\Model\PeranPeer;
use Admin\Model\MstWilayahPeer;
use Admin\Model\Pengguna;
use Admin\Model\SekolahPeer;

class Administrasi
{
	function logKunjungan(Request $request, Application $app){
		$os = getSistemOperasi();
		$browser = getBrowser();
		$ip = $_SERVER['REMOTE_ADDR'];

		$sql = "insert into 
					log.kunjungan 
				values(
					newid(),
					getdate(),
					'{$ip}',
					'{$os}',
					'{$browser}',
					'".APLIKASI_ID."'
				)";

		$exec = Preface::execQuery($sql,'rekap');

		if($exec){
			return "log kunjungan : 1";
		}else{
			return "log kunjungan : 0";
		}

		// return $browser;
	}

	function tesAdmin(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		// $oke = $request->get('oke');
		// $bro = $request->get('bro');

		// return $oke . " " .$bro;die;

		return $requesta['id_level_wilayah'];die;

		$sql_tampil = "sum(1) as jumlah";

		$sql = "select {$sql_tampil} from rekap_sekolah";

		$count = Preface::getDataByQuery($sql, 'rekap');

		$sql_tampil = "*";

		$sql = "select {$sql_tampil} from rekap_sekolah";

		$sql .= " ORDER BY
					nama ASC OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

		// return $sql;die;


		$return = Preface::getDataByQuery($sql, 'rekap');

		$returnFinal = array();

		$returnFinal['success'] = true;
		$returnFinal['results'] = $count[0]['jumlah'];
		$returnFinal['rows'] = $return;

		return json_encode($returnFinal);

		// return $count[0]['count'];
	}

	static function Pengguna(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);
		$return = array();
		$arrAll = array();

		$c = new \Criteria();
		$c->add(PenggunaPeer::SOFT_DELETE,0);
		// $c->add(PenggunaPeer::PERAN_ID, 1, \Criteria::EQUAL);
		// $c->addAnd(PenggunaPeer::PERAN_ID,53,\Criteria::NOT_EQUAL);

		// return $app['session']->get('peran_id');die;

		if((ID_LEVEL_WILAYAH != '0' || ID_LEVEL_WILAYAH != 0) && $app['session']->get('peran_id') != '10' ){
			$c->addAnd(PenggunaPeer::KODE_WILAYAH, KODE_WILAYAH);
		}

		if($requesta['pengguna_id']){
			$c->add(PenggunaPeer::PENGGUNA_ID,$requesta['pengguna_id']);
		}

		// $c->addAnd(PenggunaPeer::KODE_WILAYAH, KODE_WILAYAH);

		if($requesta['keyword']){
			// $c->add(PenggunaPeer::USERNAME, '%'.$requesta['keyword'].'%', \Criteria::LIKE);
			$cton1 = $c->getNewCriterion(PenggunaPeer::USERNAME, '%'.$requesta['keyword'].'%', \Criteria::LIKE);
            $cton2 = $c->getNewCriterion(PenggunaPeer::NAMA, '%'.$requesta['keyword'].'%', \Criteria::LIKE);

            $cton1->addOr($cton2);

			$c->addAnd($cton1);
		}

		if($requesta['peran_id']){
			// $c->add(PenggunaPeer::PERAN_ID, $requesta['peran_id']);
		}

		// print_r($c->toString()); die;
		$count = PenggunaPeer::doCount($c);

		$c->setOffset($requesta['start']);
		$c->setLimit($requesta['limit']);

		$penggunas = PenggunaPeer::doSelect($c);

		foreach ($penggunas as $pengguna) {

			$arr = $pengguna->toArray(\BasePeer::TYPE_FIELDNAME);

			$peran = PeranPeer::retrieveByPk($arr['peran_id']);
			$arr['peran_id_str'] = $peran->getNama();

			$wilayah = MstWilayahPeer::retrieveByPk($arr['kode_wilayah']);
			$arr['kode_wilayah_str'] = $wilayah->getNama();

			array_push($arrAll, $arr);
		}

		$return['success'] = true;
		$return['results'] = $count;
		$return['rows'] = $arrAll;

		return json_encode($return);

	}

	static function HapusPengguna(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);
		$return = array();

		// $pengguna = new Pengguna();
		$pengguna = PenggunaPeer::retrieveByPk($requesta['id']);

		if($pengguna){
			$pengguna->setSoftDelete(1);

			if($pengguna->save()){
				$return['success'] = true;
				$return['message'] = 'Berhasil Menghapus Data';
			}else{
				$return['success'] = false;
				$return['message'] = 'Gagal Menghapus Data';
			}
		}else{
			$return['success'] = false;
			$return['message'] = 'Gagal Menghapus Data. Pengguna tidak ditemukan';
		}

		return json_encode($return);
	}

	static function ApkApm(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		if($requesta['bentuk_pendidikan_id']){
			$params_bp = 'AND bentuk_pendidikan_id = '.$requesta['bentuk_pendidikan_id'];
		}else{
			$params_bp = '';
		}

		$sql = "SELECT
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_provinsi}
					{$add_kode_wilayah_induk_provinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					sum(penduduk_6_12_tahun) as penduduk_6_12_tahun,
					sum(penduduk_13_15_tahun) as penduduk_13_15_tahun,
					sum(penduduk_16_19_tahun) as penduduk_16_19_tahun,
					sum(pd_sma) as pd_sma,
					sum(pd_sd) as pd_sd,
					sum(pd_smp) as pd_smp,
					sum(pd_mi) as pd_mi,
					sum(pd_mi_6_12_tahun) as pd_mi_6_12_tahun,
					sum(pd_mts) as pd_mts,
					sum(pd_mts_13_15_tahun) as pd_mts_13_15_tahun,
					sum(pd_ma) as pd_ma,
					sum(pd_ma_16_19_tahun) as pd_ma_16_19_tahun,
					sum(pd_sma_16_19_tahun) as pd_sma_16_19_tahun,
					sum(pd_sd_6_12_tahun) as pd_sd_6_12_tahun,
					sum(pd_smp_13_15_tahun) as pd_smp_13_15_tahun
				FROM
					apk_apm
				WHERE
				kode_wilayah_{$params} is not null
				{$param_kode_wilayah}
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_provinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_provinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					kode_wilayah_{$params}";

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);

	}

	static function simpanApkApm(Request $request, Application $app){


		$count = 0;
		$countGagal = 0;

		$data = $request->get('data');

		$dataArr = json_decode($data);
		// print_r($dataArr);die;

		foreach ($dataArr as $datum) {
			// echo $datum->{'kode_wilayah'};

			$sql = "update apk_apm set
						penduduk_6_12_tahun = {$datum->{'penduduk_6_12_tahun'}},
						penduduk_13_15_tahun = {$datum->{'penduduk_13_15_tahun'}},
						penduduk_16_19_tahun = {$datum->{'penduduk_16_19_tahun'}},
						pd_mi = {$datum->{'pd_mi'}},
						pd_mi_6_12_tahun = {$datum->{'pd_mi_6_12_tahun'}},
						pd_mts = {$datum->{'pd_mts'}},
						pd_mts_13_15_tahun = {$datum->{'pd_mts_13_15_tahun'}},
						pd_ma = {$datum->{'pd_ma'}},
						pd_ma_16_19_tahun = {$datum->{'pd_ma_16_19_tahun'}}
					where kode_wilayah_kecamatan = '{$datum->{'kode_wilayah'}}'";

			// print_r($sql);die;

			$exec = Preface::execQuery($sql,'rekap');

			if($exec){
				$count ++;
			}else{
				$countGagal ++;
			}

		}

		$return = array();
		$return['sukses'] = $count;
		$return['gagal'] = $countGagal;

		return json_encode($return);

		// return $data;

	}

	static function Mutasi(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		$sql_hitung = "select sum(1) as jumlah from mutasi where soft_delete = 0";
		$sql = "select * from mutasi where soft_delete = 0";

		if($requesta['sekolah_id']){
			$sql .= " AND sekolah_id = '{$requesta['sekolah_id']}'";
		}

		$sql .= " ORDER BY
					status_mutasi_id ASC OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

		$fetchHitung = Preface::getDataByQuery($sql_hitung, 'rekap');
		$fetch = Preface::getDataByQuery($sql, 'rekap');

		$rows = array();

		foreach ($fetch as $f) {
			$sekolah = SekolahPeer::retrieveByPk($f['sekolah_id']);

			if($sekolah){
				$f['sekolah_id_str'] = $sekolah->getNama();
			}

			$sql_status = "select nama from ref.status_mutasi where status_mutasi_id = ".$f['status_mutasi_id'];
			$status_mutasi = Preface::getDataByQuery($sql_status, 'rekap');

			$f['status_mutasi_id_str'] = $status_mutasi[0]['nama'];

			$berangkat = MstWilayahPeer::retrieveByPk($f['kode_wilayah_berangkat']);

			if($berangkat){
				$f['kode_wilayah_berangkat_str'] = $berangkat->getNama();
			}

			if($f['tanggal_berangkat'])
				$f['tanggal_berangkat'] = substr($f['tanggal_berangkat'], 0,10);

			if($f['tanggal_surat'])
				$f['tanggal_surat'] = substr($f['tanggal_surat'], 0,10);

			if($f['tanggal_lahir'])
				$f['tanggal_lahir'] = substr($f['tanggal_lahir'], 0,10);

			if($f['NIS'])
				$f['nis'] = $f['NIS'];

			if($f['NISN'])
				$f['nisn'] = $f['NISN'];


			array_push($rows, $f);
		}

		$return = array();
		$return['success'] = true;
		$return['results'] = $fetchHitung[0]['jumlah'];
		$return['rows'] = $rows;

		return json_encode($return);

	}

	static function JenisSurat(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		$sql = "select * from ref.jenis_surat";

		$fetch = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($fetch);

	}

	static function StatusMutasi(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		$sql = "select * from ref.status_mutasi";

		$fetch = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($fetch);

	}

	static function SimpanMutasi(Request $request, Application $app){
		$data = $request->get('data');

		$dataArr = json_decode($data);

		$return = array();

		// return $dataArr->{'peserta_didik_id'};

		// return $data;

		if(!$dataArr->{'mutasi_id'}){
			$pesan = 'menambah';

			$sql = "insert into mutasi(
					mutasi_id,
					peserta_didik_id,
					nomor_surat,
					sekolah_id,
					kode_wilayah_berangkat,
					maksud_tujuan,
					nama_yang_diikuti,
					hubungan_keluarga,
					pekerjaan,
					alamat_yang_diikuti,
					tanggal_berangkat,
					pengguna_id,
					sekolah_id_tujuan,
					nama,
					tempat_lahir,
					tanggal_lahir,
					kelas,
					nis,
					nisn,
					nama_ayah,
					pekerjaan_id_ayah_str,
					alamat_jalan,
					jenis_surat_id,
					tanggal_surat,
					keterangan_kepala_sekolah,
					keterangan_lain,
					soft_delete,
					status_mutasi_id
				) values(
					NEWID(),
					'{$dataArr->{'peserta_didik_id'}}',
					'{$dataArr->{'nomor_surat'}}',
					'{$dataArr->{'sekolah_id'}}',
					'{$dataArr->{'kode_wilayah_berangkat'}}',
					'{$dataArr->{'maksud_tujuan'}}',
					'{$dataArr->{'nama_yang_diikuti'}}',
					'{$dataArr->{'hubungan_keluarga'}}',
					'{$dataArr->{'pekerjaan'}}',
					'{$dataArr->{'alamat_yang_diikuti'}}',
					'{$dataArr->{'tanggal_berangkat'}}',
					'{$dataArr->{'pengguna_id'}}',
					NULL,
					'{$dataArr->{'nama'}}',
					'{$dataArr->{'tempat_lahir'}}',
					'{$dataArr->{'tanggal_lahir'}}',
					'{$dataArr->{'kelas'}}',
					'{$dataArr->{'nis'}}',
					'{$dataArr->{'nisn'}}',
					'{$dataArr->{'nama_ayah'}}',
					'{$dataArr->{'pekerjaan_id_ayah_str'}}',
					'{$dataArr->{'alamat_jalan'}}',
					'{$dataArr->{'jenis_surat_id'}}',
					'{$dataArr->{'tanggal_surat'}}',
					'{$dataArr->{'keterangan_kepala_sekolah'}}',
					'{$dataArr->{'keterangan_lain'}}',
					0,
					1
				)";
		}else{
			$pesan = 'memperbarui';

			$sql = "update mutasi set
						peserta_didik_id = '{$dataArr->{'peserta_didik_id'}}',
						nomor_surat = '{$dataArr->{'nomor_surat'}}',
						sekolah_id = '{$dataArr->{'sekolah_id'}}',
						kode_wilayah_berangkat = '{$dataArr->{'kode_wilayah_berangkat'}}',
						maksud_tujuan = '{$dataArr->{'maksud_tujuan'}}',
						nama_yang_diikuti = '{$dataArr->{'nama_yang_diikuti'}}',
						hubungan_keluarga = '{$dataArr->{'hubungan_keluarga'}}',
						pekerjaan = '{$dataArr->{'pekerjaan'}}',
						alamat_yang_diikuti = '{$dataArr->{'alamat_yang_diikuti'}}',
						tanggal_berangkat = '{$dataArr->{'tanggal_berangkat'}}',
						pengguna_id = '{$dataArr->{'pengguna_id'}}',
						nama = '{$dataArr->{'nama'}}',
						tempat_lahir = '{$dataArr->{'tempat_lahir'}}',
						tanggal_lahir = '{$dataArr->{'tanggal_lahir'}}',
						kelas = '{$dataArr->{'kelas'}}',
						nis = '{$dataArr->{'nis'}}',
						nisn = '{$dataArr->{'nisn'}}',
						nama_ayah = '{$dataArr->{'nama_ayah'}}',
						pekerjaan_id_ayah_str = '{$dataArr->{'pekerjaan_id_ayah_str'}}',
						alamat_jalan = '{$dataArr->{'alamat_jalan'}}',
						jenis_surat_id = '{$dataArr->{'jenis_surat_id'}}',
						tanggal_surat = '{$dataArr->{'tanggal_surat'}}',
						keterangan_kepala_sekolah = '{$dataArr->{'keterangan_kepala_sekolah'}}',
						keterangan_lain = '{$dataArr->{'keterangan_lain'}}'
					where mutasi_id = '{$dataArr->{'mutasi_id'}}'";

			if($dataArr->{'status_mutasi_id'}){
				$sql_status = "update mutasi set status_mutasi_id = {$dataArr->{'status_mutasi_id'}}, keterangan = '{$dataArr->{'keterangan'}}' where mutasi_id = '{$dataArr->{'mutasi_id'}}'";
				$execStatus = Preface::execQuery($sql_status,'rekap');
			}
		}

		$exec = Preface::execQuery($sql,'rekap');

		if($exec){
			$return['success'] = true;
			$return['message'] = 'Berhasil '.$pesan.' mutasi';
		}else{
			$return['success'] = false;
			$return['message'] = 'Gagal '.$pesan.' mutasi';
		}

		return json_encode($return);
	}

	function infoPengguna(Request $request, Application $app){
		$pengguna_id = $request->get('pengguna_id');

		$sql = "select * from info_pengguna where pengguna_id = '{$pengguna_id}'";

		$fetch = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($fetch);

	}

	function gantiGambar(Request $request, Application $app){

		$return = array();
		$files = $request->files->get('gambar');
    	$pengguna_id = $request->get('pengguna_id');

    	$path = __DIR__.'/../../web/resources/images/pengguna/';
    	$filename = single_spasi_remover($files->getClientOriginalName());

    	$files->move($path,$filename);

        // rename($filename, $path);

        if(!$pengguna_id){
        	$sql = "insert into info_pengguna(pengguna_id, gambar) values('".$app['session']->get('pengguna_id')."', '".$filename."')";
        }else{
        	$sql = "update info_pengguna set gambar = '".$filename."' where pengguna_id = '".$pengguna_id."'";
        }

        // return $sql;die;

        $exec = Preface::execQuery($sql,'rekap');

        if($exec){
			$return['success'] = true;
			$return['message'] = 'Berhasil';
		}else{
			$return['success'] = false;
			$return['message'] = 'Gagal';
		}

		return json_encode($return);
	}

}