<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Misc
{
	static function RekapRpmj(Request $request, Application $app){
		$returnAll = array();

		$return = array();
		$return['no'] = 1;
		$return['nama'] = "a. Angka Melek Huruf";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = "98,89";
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = "98.94";
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = "98.97";
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = "99.29";
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = "99.34";
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = "99.54";
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "LKPJ, satuan: %";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "b. Rata-rata lama sekolah";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = "10.73";
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = "10.77";
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = "10.78";
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = "10.84";
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = "10.9";
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = null;
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = "98,89";
		$return['spm_2015'] = null;
		$return['keterangan'] = "LKPJ, satuan: %";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "c. Angka Partisipasi Kasar";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = null;
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = null;
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = null;
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = null;
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = null;
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = null;
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "&nbsp;&nbsp;&nbsp; i. APK SD";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = "119,78";
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = "105.46";
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = "109.45";
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = "109.42";
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = "111.8";
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = "112,50";
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "Lakip, satuan: %";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "&nbsp;&nbsp;&nbsp; ii. APK SMP";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = "93,27";
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = "95.38";
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = "97.5";
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = "97.59";
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = "98.01";
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = "102,20";
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "Lakip, satuan: %";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "&nbsp;&nbsp;&nbsp; iii. APK SMA";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = "72,19";
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = "64.12";
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = "65.12";
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = "66.35";
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = "66.9";
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = "72,70";
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "d. Angka Pendidikan yang<br>Ditamatkan";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = "14,36";
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = "16,23";
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = "17,66";
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = "57,882";
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = "29.69";
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = "40,99";
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);
		
		$return = array();
		$return['no'] = null;
		$return['nama'] = "e. Angka Partisipasi Murni";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = null;
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = null;
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = null;
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = null;
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = null;
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = null;
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "&nbsp;&nbsp;&nbsp; i. APM SD";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = "102,28";
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = "97.65";
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = "98.68";
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = "98.65";
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = "97.29";
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = "101,25";
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "&nbsp;&nbsp;&nbsp; ii. APM SMP";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = "60,16";
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = null;
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = null;
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = null;
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = null;
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = null;
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "&nbsp;&nbsp;&nbsp; iii. APM SMA";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = null;
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = null;
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = null;
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = null;
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = null;
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = null;
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = 2;
		$return['nama'] = "Pendidikan";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = null;
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = null;
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = null;
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = null;
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = null;
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = null;
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "a. Pendidikan Dasar";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = null;
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = null;
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = null;
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = null;
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = null;
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = null;
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "&nbsp;&nbsp;&nbsp; i. Angka Partisipasi Sekolah";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = null;
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = null;
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = null;
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = null;
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = null;
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = null;
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "&nbsp;&nbsp;&nbsp; ii. Rasio ketersediaan sekolah terhadap penduduk usia sekolah";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = null;
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = null;
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = null;
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = null;
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = null;
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = null;
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "&nbsp;&nbsp;&nbsp; iii. Rasio guru terhadap murid";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = null;
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = null;
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = null;
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = null;
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = null;
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = null;
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		$return = array();
		$return['no'] = null;
		$return['nama'] = "&nbsp;&nbsp;&nbsp; iv. Rasio guru terhadap murid per kelas rata- rata	";
		$return['target_2009'] = null;
		$return['realisasi_2009'] = null;
		$return['spm_2009'] = null;
		$return['target_2010'] = null;
		$return['realisasi_2010'] = null;
		$return['spm_2010'] = null;
		$return['target_2011'] = null;
		$return['realisasi_2011'] = null;
		$return['spm_2011'] = null;
		$return['target_2012'] = null;
		$return['realisasi_2012'] = null;
		$return['spm_2012'] = null;
		$return['target_2013'] = null;
		$return['realisasi_2013'] = null;
		$return['spm_2013'] = null;
		$return['target_2014'] = null;
		$return['realisasi_2014'] = null;
		$return['spm_2014'] = null;
		$return['target_2015'] = null;
		$return['realisasi_2015'] = null;
		$return['spm_2015'] = null;
		$return['keterangan'] = "";
		$return['skpd'] = "disdik";
		array_push($returnAll, $return);

		return json_encode($returnAll);

		// return '[{
		// 	"no" : "1",
		// 	"nama" :  "a. Angka Melek Huruf",
		// 	"target_2009" : null,
		// 	"realisasi_2009" : "98,89"
		// 	"spm_2009" : null,
		// 	"target_2010" : null,
		// 	"realisasi_2010" : "98,89"
		// 	"spm_2010" : null,
		// 	"target_2011" : null,
		// 	"realisasi_2011" : "98,89"
		// 	"spm_2011" : null,
		// 	"target_2012" : null,
		// 	"realisasi_2012" : "98,89"
		// 	"spm_2012" : null,
		// 	"target_2013" : null,
		// 	"realisasi_2013" : "98,89"
		// 	"spm_2013" : null,
		// 	"target_2014" : null,
		// 	"realisasi_2014" : "98,89"
		// 	"spm_2014" : null,
		// 	"keterangan" : null,
		// 	"skpd":  "disdik"
		// },{
		// 	"no" : "",
		// 	"nama" :  "b. Rata-rata lama sekolah",
		// 	"target_2009" : null,
		// 	"realisasi_2009" : "98,89"
		// 	"spm_2009" : null,
		// 	"target_2010" : null,
		// 	"realisasi_2010" : "98,89"
		// 	"spm_2010" : null,
		// 	"target_2011" : null,
		// 	"realisasi_2011" : "98,89"
		// 	"spm_2011" : null,
		// 	"target_2012" : null,
		// 	"realisasi_2012" : "98,89"
		// 	"spm_2012" : null,
		// 	"target_2013" : null,
		// 	"realisasi_2013" : "98,89"
		// 	"spm_2013" : null,
		// 	"target_2014" : null,
		// 	"realisasi_2014" : "98,89"
		// 	"spm_2014" : null,
		// 	"keterangan" : null,
		// 	"skpd":  "disdik"
		// }]';
	}

	
}
?>