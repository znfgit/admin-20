<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Admin\Preface;

class Monitoring
{	
	function tesMonitoring(Request $request, Application $app){
		return "koe";
	}

	function getMonitoring(Request $request, Application $app){
		$con = Preface::initdb();
		$request = Preface::Inisiasi($request);

		// $kode_wilayah = $request->get('kode_wilayah');

		// return json_encode($request['kode_wilayah']);die;

		switch ($request['id_level_wilayah']) {
			case "0":
				$params = "w3";
				$col_induk_prop = 'w3.nama';
				$col_kode_induk_prop = 'w3.kode_wilayah';
				$col_induk_kab = 'w3.nama';
				$col_kode_induk_kab = 'w3.kode_wilayah';
				$induk = '';
				break;
			case "1":
				$params = "w2";
				$col_induk_prop = 'w3.nama';
				$col_kode_induk_prop = 'w3.kode_wilayah';
				$col_induk_kab = 'w3.nama';
				$col_kode_induk_kab = 'w3.kode_wilayah';
				$induk = "and w2.mst_kode_wilayah = '".$request['kode_wilayah']."'";
				break;
			case "2":
				$params = "w1";
				$col_induk_prop = 'w3.nama';
				$col_kode_induk_prop = 'w3.kode_wilayah';
				$col_induk_kab = 'w2.nama';
				$col_kode_induk_kab = 'w2.kode_wilayah';
				$induk = "and w1.mst_kode_wilayah = '".$request['kode_wilayah']."'";
				break;
			default:
				$params = "w3";
				$induk = '';
				break;
		}

		$sql = "SELECT 
				 	{$params}.nama,
				 	{$params}.kode_wilayah,
				 	{$params}.mst_kode_wilayah,
				 	{$params}.id_level_wilayah,
				 	{$col_induk_prop} as induk_propinsi,
				 	{$col_kode_induk_prop} as kode_wilayah_induk_propinsi,
				 	{$col_induk_kab} as induk_kabupaten,
				 	{$col_kode_induk_kab} as kode_wilayah_induk_kabupaten,
					SUM(CASE WHEN r.bentuk_pendidikan_id=1 THEN 1 ELSE 0 END) AS tk,
					SUM(CASE WHEN r.bentuk_pendidikan_id=2 THEN 1 ELSE 0 END) AS kb,
					SUM(CASE WHEN r.bentuk_pendidikan_id=3 THEN 1 ELSE 0 END) AS tpa,
					SUM(CASE WHEN r.bentuk_pendidikan_id=4 THEN 1 ELSE 0 END) AS sps,
					SUM(CASE WHEN r.bentuk_pendidikan_id=1 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) AS kirim_tk,
					SUM(CASE WHEN r.bentuk_pendidikan_id=2 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) AS kirim_kb,
					SUM(CASE WHEN r.bentuk_pendidikan_id=3 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) AS kirim_tpa,
					SUM(CASE WHEN r.bentuk_pendidikan_id=4 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) AS kirim_sps,
					( SUM(CASE WHEN r.bentuk_pendidikan_id=1 THEN 1 ELSE 0 END) - SUM(CASE WHEN r.bentuk_pendidikan_id=1 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) ) as sisa_tk,
					( SUM(CASE WHEN r.bentuk_pendidikan_id=2 THEN 1 ELSE 0 END) - SUM(CASE WHEN r.bentuk_pendidikan_id=2 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) ) as sisa_kb,
					( SUM(CASE WHEN r.bentuk_pendidikan_id=3 THEN 1 ELSE 0 END) - SUM(CASE WHEN r.bentuk_pendidikan_id=3 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) ) as sisa_tpa,
					( SUM(CASE WHEN r.bentuk_pendidikan_id=4 THEN 1 ELSE 0 END) - SUM(CASE WHEN r.bentuk_pendidikan_id=4 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) ) as sisa_sps,
					( (
						(
						SUM(CASE WHEN r.bentuk_pendidikan_id=1 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) +
						SUM(CASE WHEN r.bentuk_pendidikan_id=2 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) +
						SUM(CASE WHEN r.bentuk_pendidikan_id=3 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) +
						SUM(CASE WHEN r.bentuk_pendidikan_id=4 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END)
						) /
						cast( (
						SUM(CASE WHEN r.bentuk_pendidikan_id=1 THEN 1 ELSE 0 END) +
						SUM(CASE WHEN r.bentuk_pendidikan_id=2 THEN 1 ELSE 0 END) +
						SUM(CASE WHEN r.bentuk_pendidikan_id=3 THEN 1 ELSE 0 END) +
						SUM(CASE WHEN r.bentuk_pendidikan_id=4 THEN 1 ELSE 0 END) 
						) as numeric(9,2) )
					) * 100 ) as persen
				FROM dbo.sekolah r
					INNER JOIN ref.mst_wilayah w1 ON left(r.kode_wilayah,6)=w1.kode_wilayah
					INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
					INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
				 	LEFT OUTER JOIN (
				 	SELECT
				 		sekolah_id,
				 		COUNT (1) as jumlah
				 	FROM
				 		send.register_pengiriman
				 	WHERE tgl_kirim > (select tanggal_mulai from ref.semester where semester_id = ".$request['semester_id'].")
					AND tgl_kirim < (select tanggal_selesai from ref.semester where semester_id = ".$request['semester_id'].")
				 	GROUP BY
				 		sekolah_id
				 	) as jumlah_sinkron on jumlah_sinkron.sekolah_id = r.sekolah_id
				WHERE ({$params}.expired_date is null or {$params}.expired_date > GETDATE())
				AND r.Soft_delete = 0
				{$induk}
				GROUP BY
					{$col_induk_prop},
				 	{$col_induk_kab},
				 	{$col_kode_induk_prop},
				 	{$col_kode_induk_kab},
				 	{$params}.nama,
				 	{$params}.kode_wilayah,
				 	{$params}.mst_kode_wilayah,
				 	{$params}.id_level_wilayah
				ORDER BY persen DESC";

				// return $sql;die;

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

	function getMonitoringTotal(Request $request, Application $app){
		$con = Preface::initdb();
		$request = Preface::Inisiasi($request);

		// return json_encode($request['kode_wilayah']);die;

		switch ($request['id_level_wilayah']) {
			case "0":
				$params = "w3";
				$col_induk_prop = 'w3.nama';
				$col_kode_induk_prop = 'w3.kode_wilayah';
				$col_induk_kab = 'w3.nama';
				$col_kode_induk_kab = 'w3.kode_wilayah';
				$induk = '';
				break;
			case "1":
				$params = "w2";
				$col_induk_prop = 'w3.nama';
				$col_kode_induk_prop = 'w3.kode_wilayah';
				$col_induk_kab = 'w3.nama';
				$col_kode_induk_kab = 'w3.kode_wilayah';
				$induk = "and w2.mst_kode_wilayah = '".$request['kode_wilayah']."'";
				break;
			case "2":
				$params = "w1";
				$col_induk_prop = 'w3.nama';
				$col_kode_induk_prop = 'w3.kode_wilayah';
				$col_induk_kab = 'w2.nama';
				$col_kode_induk_kab = 'w2.kode_wilayah';
				$induk = "and w1.mst_kode_wilayah = '".$request['kode_wilayah']."'";
				break;
			default:
				$params = "w3";
				$induk = '';
				break;
		}

		$sql = "SELECT 
				 	SUM(CASE WHEN r.bentuk_pendidikan_id=1 THEN 1 ELSE 0 END) AS tk,
					SUM(CASE WHEN r.bentuk_pendidikan_id=2 THEN 1 ELSE 0 END) AS kb,
					SUM(CASE WHEN r.bentuk_pendidikan_id=3 THEN 1 ELSE 0 END) AS tpa,
					SUM(CASE WHEN r.bentuk_pendidikan_id=4 THEN 1 ELSE 0 END) AS sps,
					SUM(CASE WHEN r.bentuk_pendidikan_id=1 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) AS kirim_tk,
					SUM(CASE WHEN r.bentuk_pendidikan_id=2 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) AS kirim_kb,
					SUM(CASE WHEN r.bentuk_pendidikan_id=3 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) AS kirim_tpa,
					SUM(CASE WHEN r.bentuk_pendidikan_id=4 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) AS kirim_sps,
					( SUM(CASE WHEN r.bentuk_pendidikan_id=1 THEN 1 ELSE 0 END) - SUM(CASE WHEN r.bentuk_pendidikan_id=1 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) ) as sisa_tk,
					( SUM(CASE WHEN r.bentuk_pendidikan_id=2 THEN 1 ELSE 0 END) - SUM(CASE WHEN r.bentuk_pendidikan_id=2 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) ) as sisa_kb,
					( SUM(CASE WHEN r.bentuk_pendidikan_id=3 THEN 1 ELSE 0 END) - SUM(CASE WHEN r.bentuk_pendidikan_id=3 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) ) as sisa_tpa,
					( SUM(CASE WHEN r.bentuk_pendidikan_id=4 THEN 1 ELSE 0 END) - SUM(CASE WHEN r.bentuk_pendidikan_id=4 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) ) as sisa_sps,
					( (
						(
						SUM(CASE WHEN r.bentuk_pendidikan_id=1 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) +
						SUM(CASE WHEN r.bentuk_pendidikan_id=2 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) +
						SUM(CASE WHEN r.bentuk_pendidikan_id=3 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) +
						SUM(CASE WHEN r.bentuk_pendidikan_id=4 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END)
						) /
						cast( (
						SUM(CASE WHEN r.bentuk_pendidikan_id=1 THEN 1 ELSE 0 END) +
						SUM(CASE WHEN r.bentuk_pendidikan_id=2 THEN 1 ELSE 0 END) +
						SUM(CASE WHEN r.bentuk_pendidikan_id=3 THEN 1 ELSE 0 END) +
						SUM(CASE WHEN r.bentuk_pendidikan_id=4 THEN 1 ELSE 0 END) 
						) as numeric(9,2) )
					) * 100 ) as persen
				FROM dbo.sekolah r
					INNER JOIN ref.mst_wilayah w1 ON left(r.kode_wilayah,6)=w1.kode_wilayah
					INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
					INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
				 	LEFT OUTER JOIN (
				 	SELECT
				 		sekolah_id,
				 		COUNT (1) as jumlah
				 	FROM
				 		send.register_pengiriman
				 	WHERE tgl_kirim > (select tanggal_mulai from ref.semester where semester_id = ".$request['semester_id'].")
					AND tgl_kirim < (select tanggal_selesai from ref.semester where semester_id = ".$request['semester_id'].")
				 	GROUP BY
				 		sekolah_id
				 	) as jumlah_sinkron on jumlah_sinkron.sekolah_id = r.sekolah_id
				WHERE ({$params}.expired_date is null or {$params}.expired_date > GETDATE())
				AND r.Soft_delete = 0
				{$induk}
				ORDER BY persen DESC";

				// return $sql;die;

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

	function getSyncPerDay(Request $request, Application $app){
		$con = Preface::initdb();
		$request = Preface::Inisiasi($request);		

		$sql = "SELECT
					CONVERT(date, tgl_kirim) as tanggal,
					COUNT (1) AS jumlah
				FROM
					send.register_pengiriman
				where tgl_kirim > dateadd(day, -10, getdate())
				GROUP BY
					CONVERT(date, tgl_kirim)
				order by CONVERT(date, tgl_kirim) desc";

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

	function getRekapMonitoringTotalHarian(Request $request, Application $app){
		$con = Preface::initdb();
		$request = Preface::Inisiasi($request);		

		$sql = "select top 20 * from rekap.monitoring_total_harian order by tanggal desc";

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

	function getProgresRegistrasi(Request $request, Application $app){
		$sql = "SELECT
					sum(1) sekolah_total,
					sum(case when pg.jumlah_pengguna > 0 then 1 else 0 end) as sekolah_registrasi,
					( sum(case when pg.jumlah_pengguna > 0 then 1 else 0 end) / cast(sum(1) as numeric(9,2)) * 100 ) as persen
				FROM
					sekolah
				INNER JOIN ref.mst_wilayah w1 ON LEFT (sekolah.kode_wilayah, 6) = w1.kode_wilayah
				INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
				INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
				LEFT OUTER JOIN (
					SELECT
						sekolah_id,
						COUNT (1) as jumlah_pengguna
					FROM
						pengguna
					GROUP BY
						sekolah_id
				) pg on pg.sekolah_id = sekolah.sekolah_id
				WHERE
					sekolah.soft_delete = 0";

		$return = Preface::getDataByQuery($sql);

		// return $sql;die;
		return json_encode($return);
	}

	function getProgresSinkronisasi(){
		$sql = "SELECT
				( (
					(
					SUM(CASE WHEN r.bentuk_pendidikan_id=1 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) +
					SUM(CASE WHEN r.bentuk_pendidikan_id=2 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) +
					SUM(CASE WHEN r.bentuk_pendidikan_id=3 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) +
					SUM(CASE WHEN r.bentuk_pendidikan_id=4 and jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END)
					) /
					cast( (
					SUM(CASE WHEN r.bentuk_pendidikan_id=1 THEN 1 ELSE 0 END) +
					SUM(CASE WHEN r.bentuk_pendidikan_id=2 THEN 1 ELSE 0 END) +
					SUM(CASE WHEN r.bentuk_pendidikan_id=3 THEN 1 ELSE 0 END) +
					SUM(CASE WHEN r.bentuk_pendidikan_id=4 THEN 1 ELSE 0 END) 
					) as numeric(9,2) )
				) * 100 ) as persen,
				sum(1) as total,
				SUM (CASE WHEN jumlah_sinkron.jumlah > 0 THEN 1 ELSE 0 END) as kirim
			FROM
				dbo.sekolah r
			INNER JOIN ref.mst_wilayah w1 ON LEFT (r.kode_wilayah, 6) = w1.kode_wilayah
			INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
			INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
			LEFT OUTER JOIN (
			 	SELECT
			 		sekolah_id,
			 		COUNT (1) as jumlah
			 	FROM
			 		send.register_pengiriman
			 	WHERE tgl_kirim > (select tanggal_mulai from ref.semester where semester_id = 20151)
				AND tgl_kirim < (select tanggal_selesai from ref.semester where semester_id = 20151)
				GROUP BY
					sekolah_id
			) AS jumlah_sinkron ON jumlah_sinkron.sekolah_id = r.sekolah_id
			WHERE
				(
					w3.expired_date IS NULL
					OR w3.expired_date > GETDATE()
				)
			AND r.Soft_delete = 0";

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

	function getRekapStatusSinkronisasi(){
		$sql = "SELECT
					sum(case when status in (1) then 1 else 0 end) as diterima,
					sum(case when status in (2,3) then 1 else 0 end) as diproses,
					sum(case when status in (4) then 1 else 0 end) as selesai,
					sum(case when status in (5) then 1 else 0 end) as gagal
				FROM
					send.register_pengiriman";
					
		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}
}