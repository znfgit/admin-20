<?php

/* Author Khalid Saifuddin Imanullah */

namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Admin\Model\SekolahPeer;
use Admin\Model\JenisTinggalPeer;
use Admin\Model\AlatTransportasiPeer;
use Admin\Model\AksesInternetPeer;
use Admin\Model\JenjangPendidikanPeer;
use Admin\Model\SanitasiPeer;
use Admin\Model\PenghasilanOrangTuaWaliPeer;
use Admin\Model\AlasanLayakPipPeer;
use Admin\Model\Ptk;
use Admin\Model\PtkPeer;
use Admin\Model\RombonganBelajarPeer;
use Admin\Model\RombonganBelajar;
use Admin\Model\AnggotaRombel;
use Admin\Model\PtkTerdaftarPeer;
use Admin\Model\AnggotaRombelPeer;
use Admin\Model\RegistrasiPesertaDidikPeer;
use Admin\Model\PembelajaranPeer;
use Admin\Model\PesertaDidikPeer;
use Admin\Model\PesertaDidikQuery;
use Admin\Model\SekolahLongitudinalPeer;
use Admin\Model\MstWilayahPeer;
use Admin\Model\StatusKepegawaianPeer;
use Admin\Model\AgamaPeer;
use Admin\Model\PenggunaPeer;
use Admin\Model\TugasTambahanPeer;
use Admin\Model\JabatanTugasPtkPeer;
use Admin\Model\LembagaPengangkatPeer;
use Admin\Model\PangkatGolonganPeer;
use Admin\Model\SumberGajiPeer;
use Admin\Model\RwyKepangkatanPeer;
use Admin\Model\PekerjaanPeer;
use Admin\Model\BankPeer;
use Admin\Model\RwySertifikasiPeer;
use Admin\Model\RwyPendFormalPeer;
use Admin\Model\PrasaranaPeer;
use Admin\Model\PrasaranaLongitudinalPeer;
use Admin\Model\SaranaLongitudinalPeer;
use Admin\Model\SaranaPeer;
use Admin\Model\JenisSaranaPeer;
use Admin\Model\StatusKepemilikanSarprasPeer;

error_reporting(E_ERROR);

class UnduhExcelSekolah {

	public function getProfilSekolah(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		$pengguna_id = $app['session']->get('pengguna_id');
		// $pengguna_id = "690D5827-7D8A-490E-913D-D39E482F2D69";
		// echo $pengguna_id; die;
		$tahunId = TA_BERJALAN;
		$sekolah_id = $requesta['sekolah_id'];
		$today = date('d-m-Y H:i:s');

		$sekolah = SekolahPeer::retrieveByPk($sekolah_id);
		$pengguna = PenggunaPeer::retrieveByPk($pengguna_id);
		// print_r($pengguna); die;

		$objPHPExcel = \PHPExcel_IOFactory::load(__DIR__."/Templates/template-formulir-sekolah.xlsx");

			// Set document properties
		$objPHPExcel->getProperties()->setCreator("Profil Pendidikan")
									 ->setLastModifiedBy("Profil Pendidikan")
									 ->setTitle("Formulir Sekolah")
									 ->setSubject("Office 2007 XLSX Test Document")
									 ->setDescription("Formulir Sekolah")
									 ->setKeywords("Formulir Sekolah")
									 ->setCategory("formulir");

		// Rename worksheet
		$clearingName = preg_replace("([^\w\s\d\-_~,;:\[\]\(\]]|[\.]{2,})", '', $sekolah->getNama());
		$clearingName = substr($clearingName, 0, 20);
		$objPHPExcel->setActiveSheetIndex(0)->setTitle('Profil '.$clearingName);

		//data utama
		switch ($sekolah->getStatusSekolah()) {
			case 1:
				$status = 'Negeri';
				break;
			default:
				$status = 'Swasta';
				break;
		}

		switch ($sekolah->getMbs()) {
			case 1:
				$mbs = 'Ya';
				break;

			default:
				$mbs = 'Tidak';
				break;
		}

		if( strlen(trim($sekolah->getKodeWilayah())) == 8){
			$kelurahan = MstWilayahPeer::retrieveByPk($sekolah->getKodeWilayah());
			$kecamatan = MstWilayahPeer::retrieveByPk($kelurahan->getMstKodeWilayah());
			$kabupaten = MstWilayahPeer::retrieveByPk($kecamatan->getMstKodeWilayah());
			$provinsi = MstWilayahPeer::retrieveByPk($kabupaten->getMstKodeWilayah());
			$negara = MstWilayahPeer::retrieveByPk($provinsi->getMstKodeWilayah());

			$nama_kelurahan = $kelurahan->getNama();
		}else{
			$kecamatan = MstWilayahPeer::retrieveByPk($sekolah->getKodeWilayah());
			$kabupaten = MstWilayahPeer::retrieveByPk($kecamatan->getMstKodeWilayah());
			$provinsi = MstWilayahPeer::retrieveByPk($kabupaten->getMstKodeWilayah());
			$negara = MstWilayahPeer::retrieveByPk($provinsi->getMstKodeWilayah());

			$nama_kelurahan = $sekolah->getDesaKelurahan();
		}


		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D4', $sekolah->getNama());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D5', $sekolah->getNpsn());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D6', $sekolah->getBentukPendidikan()->getNama());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D7', $status);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D8', $sekolah->getAlamatJalan());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D9', $sekolah->getRt());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F9', $sekolah->getRw());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D10', $sekolah->getKodePos());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D11', $nama_kelurahan);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D12', $kecamatan->getNama());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D13', $kabupaten->getNama());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D14', $provinsi->getNama());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D15', $negara->getNama());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D16', $sekolah->getLintang());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D17', $sekolah->getBujur());

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D19', $sekolah->getSkPendirianSekolah());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D20', $sekolah->getTanggalSkPendirian());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D21', $sekolah->getStatusKepemilikan()->getNama());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D22', $sekolah->getSkIzinOperasional());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D23', $sekolah->getTanggalSkIzinOperasional());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D24', '');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D25', $sekolah->getNoRekening());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D26', $sekolah->getNamaBank());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D27', $sekolah->getCabangKcpUnit());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D28', $sekolah->getRekeningAtasNama());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D29', $mbs);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D30', $sekolah->getLuasTanahMilik());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D31', $sekolah->getLuasTanahBukanMilik());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D32', $sekolah->getNmWp());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D33', $sekolah->getNpwp());

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D35', $sekolah->getNomorTelepon());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D36', $sekolah->getNomorFax());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D37', $sekolah->getEmail());
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D38', $sekolah->getWebsite());

        //periodik
        $c = new \Criteria();
        $c->add(SekolahLongitudinalPeer::SEKOLAH_ID, $sekolah->getSekolahId());
        $c->add(SekolahLongitudinalPeer::SEMESTER_ID, SEMESTER_BERJALAN);
        $c->add(SekolahLongitudinalPeer::SOFT_DELETE, 0);

        $periodik = SekolahLongitudinalPeer::doSelectOne($c);

        // return print_r($periodik); die;

        if($periodik){
            switch ($periodik->getPartisipasiBos()) {
                case 1:
                    $bos = 'Ya';
                    break;
                default:
                    $bos = 'Tidak';
                    break;
            }

            $internet_1 = AksesInternetPeer::retrieveByPk($periodik->getAksesInternetId());
            $internet_2 = AksesInternetPeer::retrieveByPk($periodik->getAksesInternet2Id());

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D40', $periodik->getWaktuPenyelenggaraan()->getNama());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D41', $bos);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D42', $periodik->getSertifikasiIso()->getNama());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D43', $periodik->getSumberListrik()->getNama());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D44', $periodik->getDayaListrik());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D45', $internet_1->getNama());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D46', $internet_2->getnama());
        }

        //sanitasi
        $c = new \Criteria();
        $c->add(SanitasiPeer::SEKOLAH_ID, $sekolah->getSekolahId());
        $c->add(SanitasiPeer::SEMESTER_ID, SEMESTER_BERJALAN);
        $c->add(SanitasiPeer::SOFT_DELETE, 0);

        $sanitasi = SanitasiPeer::doSelectOne($c);

        if($sanitasi){
            switch ($sanitasi->getKecukupanAir()) {
                case 1:
                    $kecukupan = 'Cukup';
                    break;
                case 2:
                    $kecukupan = 'Kurang';
                    break;
                default:
                    $kecukupan = 'Tidak Ada';
                    break;
            }

            switch ($sanitasi->getMemprosesAir()) {
                case 1:
                    $proses_air = 'Ya';
                    break;
                default:
                    $proses_air = 'Tidak';
                    break;
            }

            switch ($sanitasi->getMinumSiswa()) {
                case 1:
                    $minum_siswa = 'Disediakan Sekolah';
                    break;
                default:
                    $minum_siswa = 'Tidak Disediakan';
                    break;
            }

            switch ($sanitasi->getSiswaBawaAir()) {
                case 1:
                    $siswa_bawa_air = 'Ya';
                    break;
                default:
                    $siswa_bawa_air = 'Tidak';
                    break;
            }

            switch ($sanitasi->getKetersediaanAir()) {
                case 1:
                    $sedia_air = 'Ada Sumber Air';
                    break;

                default:
                    $sedia_air = 'Tidak Ada';
                    break;
            }

            switch ($sanitasi->getTipeJamban()) {
                case 1:
                    $tipe_jamban = 'Leher angsa (toilet duduk/jongkok)';
                    break;
                case 2:
                    $tipe_jamban = 'Cubluk dengan tutup';
                    break;
                case 3:
                    $tipe_jamban = 'Jamban menggantung di atas sungai';
                    break;
                case 4:
                    $tipe_jamban = 'Cubluk tanpa tutup';
                    break;
                default:
                    $tipe_jamban = 'Tidak tersedia jamban';
                    break;
            }

            switch ($sanitasi->getASabunAirMengalir()) {
                case 1:
                    $sabun_air = 'Ya';
                    break;
                default:
                    $sabun_air = 'Tidak';
                    break;
            }

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D48', $kecukupan);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D49', $proses_air);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D51', $minum_siswa);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D52', $siswa_bawa_air);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D54', $sanitasi->getToiletSiswaKk());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D56', $sanitasi->getSumberAir()->getNama());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D57', $sedia_air);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D59', $tipe_jamban);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D60', $sanitasi->getTempatCuciTangan());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D62', $sabun_air);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D66', $sanitasi->getJmlJambanLG());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F66', $sanitasi->getJmlJambanPG());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H66', $sanitasi->getJmlJambanLPG());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D68', $sanitasi->getJmlJambanLTg());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F68', $sanitasi->getJmlJambanPTg());
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H68', $sanitasi->getJmlJambanLPTg());

        }

        $objPHPExcel->setActiveSheetIndex(1)->setCellValue('A2', $sekolah->getNama());
        $objPHPExcel->setActiveSheetIndex(1)->setCellValue('A3', ''.$kecamatan->getNama().', '.$kabupaten->getNama().', '.$provinsi->getNama());
        $objPHPExcel->setActiveSheetIndex(1)->setCellValue('A4', 'Tanggal Unduh: '.$today);
        $objPHPExcel->setActiveSheetIndex(1)->setCellValue('C4', 'Pengunduh: '.$pengguna->getNama().' ('.$pengguna->getUsername().')');

        $objPHPExcel->setActiveSheetIndex(2)->setCellValue('A2', $sekolah->getNama());
        $objPHPExcel->setActiveSheetIndex(2)->setCellValue('A3', ''.$kecamatan->getNama().', '.$kabupaten->getNama().', '.$provinsi->getNama());
        $objPHPExcel->setActiveSheetIndex(2)->setCellValue('A4', 'Tanggal Unduh: '.$today);
        $objPHPExcel->setActiveSheetIndex(2)->setCellValue('D4', 'Pengunduh: '.$pengguna->getNama().' ('.$pengguna->getUsername().')');

        $objPHPExcel->setActiveSheetIndex(3)->setCellValue('A2', $sekolah->getNama());
        $objPHPExcel->setActiveSheetIndex(3)->setCellValue('A3', ''.$kecamatan->getNama().', '.$kabupaten->getNama().', '.$provinsi->getNama());
        $objPHPExcel->setActiveSheetIndex(3)->setCellValue('A4', 'Tanggal Unduh: '.$today);
        $objPHPExcel->setActiveSheetIndex(3)->setCellValue('D4', 'Pengunduh: '.$pengguna->getNama().' ('.$pengguna->getUsername().')');

        $objPHPExcel->setActiveSheetIndex(4)->setCellValue('A2', $sekolah->getNama());
        $objPHPExcel->setActiveSheetIndex(4)->setCellValue('A3', ''.$kecamatan->getNama().', '.$kabupaten->getNama().', '.$provinsi->getNama());
        $objPHPExcel->setActiveSheetIndex(4)->setCellValue('A4', 'Tanggal Unduh: '.$today);
        $objPHPExcel->setActiveSheetIndex(4)->setCellValue('D4', 'Pengunduh: '.$pengguna->getNama().' ('.$pengguna->getUsername().')');

        $objPHPExcel->setActiveSheetIndex(5)->setCellValue('A2', $sekolah->getNama());
        $objPHPExcel->setActiveSheetIndex(5)->setCellValue('A3', ''.$kecamatan->getNama().', '.$kabupaten->getNama().', '.$provinsi->getNama());
        $objPHPExcel->setActiveSheetIndex(5)->setCellValue('A4', 'Tanggal Unduh: '.$today);
        $objPHPExcel->setActiveSheetIndex(5)->setCellValue('D4', 'Pengunduh: '.$pengguna->getNama().' ('.$pengguna->getUsername().')');

        //rombongan belajar
        // $request->query->set('model', 'RombonganBelajar');
        // $request->query->set('ascending', 'nama');
        //
        // $json = CustomRest::getRombonganBelajar($request, $app);
        /*$c = new \Criteria();
        $c->add(RombonganBelajarPeer::SEKOLAH_ID, $sekolah_id);
        $c->add(RombonganBelajarPeer::SOFT_DELETE, 0);

        $rombels = RombonganBelajarPeer::doSelect($c);
        $rowCount = sizeof($rombels);
        $json = tableJson(getArray($rombels, \BasePeer::TYPE_FIELDNAME), $rowCount, array('sarana_id'));

        $rombelsDec = json_decode($json);

        $n = 8;
        $i = 1;

        foreach ($rombelsDec->rows as $d) {

            if($d->jenis_rombel == 1){

                $objPHPExcel->setActiveSheetIndex(3)->setCellValue('A'.$n, $i);
                $objPHPExcel->setActiveSheetIndex(3)->setCellValue('B'.$n, $d->nama);
                $objPHPExcel->setActiveSheetIndex(3)->setCellValue('C'.$n, $d->tingkat_pendidikan_id);
                $objPHPExcel->setActiveSheetIndex(3)->setCellValue('G'.$n, $d->ptk_id_str);
                $objPHPExcel->setActiveSheetIndex(3)->setCellValue('H'.$n, $d->kurikulum_id_str);
                $objPHPExcel->setActiveSheetIndex(3)->setCellValue('I'.$n, $d->prasarana_id_str);

                $c = new \Criteria();
                $c->add(AnggotaRombelPeer::SOFT_DELETE,0);
                $c->add(AnggotaRombelPeer::ROMBONGAN_BELAJAR_ID,$d->rombongan_belajar_id);

                $anggotaRombels = AnggotaRombelPeer::doSelect($c);

                $anggota_laki = 0;
                $anggota_perempuan = 0;

                foreach ($anggotaRombels as $ar) {

                    if($ar->getPesertaDidik()->getJenisKelamin() == 'L'){
                        $anggota_laki++;
                    }else if($ar->getPesertaDidik()->getJenisKelamin() == 'P'){
                        $anggota_perempuan++;
                    }
                }

                $objPHPExcel->setActiveSheetIndex(3)->setCellValue('D'.$n, $anggota_laki);
                $objPHPExcel->setActiveSheetIndex(3)->setCellValue('E'.$n, $anggota_perempuan);
                $objPHPExcel->setActiveSheetIndex(3)->setCellValue('F'.$n, ($anggota_perempuan+$anggota_laki));

                $n++;
                $i++;

            }

        }*/

        // Sarana
        // $request->query->set('model', 'Sarana');
        // $request->query->set('ascending', 'prasarana_id');
        // $request->query->set('limit', 'unlimited');
        // $json = Rest::get($request, $app);
        $c = new \Criteria();
        $c->addJoin(SaranaPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID);
        $c->add(SaranaPeer::SOFT_DELETE, 0);
        $c->add(PrasaranaPeer::SOFT_DELETE, 0);
        $c->add(PrasaranaPeer::ID_HAPUS_BUKU, NULL, \Criteria::ISNULL);
        $c->add(SaranaPeer::ID_HAPUS_BUKU, NULL, \Criteria::ISNULL);
        $c->add(PrasaranaPeer::SEKOLAH_ID, $sekolah_id);
        $c->addAscendingOrderByColumn(SaranaPeer::PRASARANA_ID);
        $c->addAscendingOrderByColumn(SaranaPeer::JENIS_SARANA_ID);
        $saranas = SaranaPeer::doSelect($c);
        $rowCount = sizeof($saranas);
        $json = tableJson(getArray($saranas, \BasePeer::TYPE_FIELDNAME), $rowCount, array('sarana_id'));

        $dec = json_decode($json);

        $n = 7;
        $i = 1;

        foreach ($dec->rows as $d) {

            $pras = PrasaranaPeer::retrieveByPk($d->prasarana_id);
            if ($d->id_hapus_buku || $pras->getIdHapusBuku()) {
                continue;
            }

            $saranaLong = SaranaLongitudinalPeer::retrieveByPk($d->sarana_id, SEMESTER_BERJALAN);
            $jenis_sarana = JenisSaranaPeer::retrieveByPk($d->jenis_sarana_id);
            $prasarana = PrasaranaPeer::retrieveByPk($d->prasarana_id);
            $status_kepemilikan = StatusKepemilikanSarprasPeer::retrieveByPk($d->kepemilikan_sarpras_id);

            $objPHPExcel->setActiveSheetIndex(5)->setCellValue('A'.$n, $i);
            $objPHPExcel->setActiveSheetIndex(5)->setCellValue('B'.$n, is_object($jenis_sarana) ? $jenis_sarana->getNama() : "-");
            $objPHPExcel->setActiveSheetIndex(5)->setCellValue('C'.$n, is_object($prasarana) ? $prasarana->getNama() : "-");
            $objPHPExcel->setActiveSheetIndex(5)->setCellValue('D'.$n, is_object($status_kepemilikan) ? $status_kepemilikan->getNama() : "-");
            $objPHPExcel->setActiveSheetIndex(5)->setCellValue('E'.$n, $d->spesifikasi);

            if (is_object($saranaLong)) {
                $objPHPExcel->setActiveSheetIndex(5)->setCellValue('F'.$n, $saranaLong->getJumlah());
                $objPHPExcel->setActiveSheetIndex(5)->setCellValue('G'.$n, ($saranaLong->getStatusKelaikan() == 1 ? "Laik" : "Tidak Laik") );
            } else {
                $objPHPExcel->setActiveSheetIndex(5)->setCellValue('F'.$n, 0);
                $objPHPExcel->setActiveSheetIndex(5)->setCellValue('G'.$n, "-");
            }


            $n++;
            $i++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        $sekolahNama = preg_replace("([^\w\s\d\-_~,;:\[\]\(\]]|[\.]{2,})", '', $sekolah->getNama());

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Profil Pendidikan '.$sekolahNama.' ('.$today.').xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');

		return exit;

	}
}