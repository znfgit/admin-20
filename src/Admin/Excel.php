<?php

namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Admin\Auth;
use Admin\Preface;

class Excel{
	
	static function Generate(Request $request, Application $app){
		
		$class = "\\Admin\\".$request->get('class');
		$method = $request->get('method');

		$request->query->set('limit', '2000000');

		$return  = ${'class'}::${'method'}($request,$app);
		// $return  = entitas\${'class'}::GuruTendik($request,$app);
		$data = json_decode($return);

		$objPHPExcel = new \PHPExcel();
		// $objPHPExcel = \PHPExcel_IOFactory::load(__DIR__ . "/templates/sinkronisasi.xlsx");

		$objPHPExcel->getProperties()->setCreator("Khalid Saifuddin")
									 ->setLastModifiedBy("Khalid Saifuddin")
									 ->setTitle("Unduh Excel")
									 ->setSubject("Unduh Excel")
									 ->setDescription("unduh excel")
									 ->setKeywords("office 2007 openxml php")
									 ->setCategory("result file");

		$row = 2;
		$arrJudul = array();

		if($request->get('tipe') == 2){
			$oks = $data;
		}else{
			$oks = $data->{'rows'};
		}

		foreach ($oks as $key => $value) {
			$col = 0;

			foreach ($value as $val => $vals) {

				$pos_kode = strpos($val, 'kode_');
				$pos_id = strpos($val, '_id');
				$pos_id2 = strpos($val, 'id_');
				$pos_soft = strpos($val, 'soft_');
				$pos_koreg = strpos($val, 'koreg');

				if ($pos_kode === false && $pos_id === false && $pos_id2 === false && $pos_soft === false && $pos_koreg === false) {					

					if($row == 2){
						array_push($arrJudul, $val);
					}
					
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( $col, $row, $vals );
	       		 	$col++;

				}
			}	

			$row++;
		}

		for ($i=0; $i < sizeof($arrJudul); $i++) { 
			$pos_kode = strpos($arrJudul[$i], 'kode_');
			$pos_id = strpos($arrJudul[$i], '_id');
			$pos_id2 = strpos($arrJudul[$i], 'id_');
			$pos_soft = strpos($arrJudul[$i], 'soft_');
			$pos_koreg = strpos($arrJudul[$i], 'koreg');

			if ($pos_kode === false && $pos_id === false && $pos_id2 === false && $pos_soft === false && $pos_koreg === false) {
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($i, 1, str_replace("_"," ",strtoupper($arrJudul[$i])));
			}

		}

		$nama_sekolah = null;

		if($request->get('sekolah_id')){
			$returnSekolah  = entitas\SatuanPendidikan::SatuanPendidikan($request,$app);

			$dataSekolah = json_decode($returnSekolah)->{'rows'};

			foreach ($dataSekolah as $key) {
				$nama_sekolah = $key->{'sekolah'};
			}
		}

		if($request->get('kode_wilayah')){
			$arrKode = $request->get('kode_wilayah');
		}else{
			$arrKode = null;
		}

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition: attachment;filename="Daftar Kode Registrasi - '.date('Y-m-d H:i:s').'.xlsx"');
		header('Content-Disposition: attachment;filename="'.$arrKode.'-'.$nama_sekolah.'-'.$request->get('class').'_'.$method.'_'.date('Y-m-d H:i:s').'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');

		return true;

		// return $return;

	}
}