<?php
namespace Admin\progres;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Admin\Auth;
use Admin\Preface;

class Pengiriman{

	static function PengirimanBeranda(Request $request, Application $app){
		$id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah'); 
		$kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah'); 

		$url = 'http://dapo.dikdasmen.kemdikbud.go.id/rekap/progres?id_level_wilayah='.$id_level_wilayah.'&kode_wilayah='.trim($kode_wilayah).'&semester_id=20161';
		$json = file_get_contents($url);
		
		$arrJson = json_decode($json);

		$persen_total = 0;
		$persen_sma = 0;
		$i = 0;

		foreach ($arrJson as $arr) {
			$persen_total = $persen_total + $arr->{'persen'};

			if($arr->{'sma'} == 0){
				$arr->{'sma'} = 1;
			}

			$persen_sma = $persen_sma + ($arr->{'kirim_sma'}/$arr->{'sma'}*100);
			$i++;
		}

		$rata2 = $persen_total / $i;
		$rata2_sma = $persen_sma / $i;

		$return = array();
		$return['pengiriman'] = $rata2;
		$return['pengiriman_sma'] = $rata2_sma;

		return json_encode($return);

	}

	static function PengirimanSps(Request $request, Application $app){
		$id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah'); 
		$kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah');
		$belum_sinkron_saja = $request->get('belum_sinkron_saja') ? $request->get('belum_sinkron_saja') : 'false';
		$semester_id = $app['session']->get('semester_berjalan') ? $app['session']->get('semester_berjalan') : SEMESTER_BERJALAN;  

		$requesta = Preface::Inisiasi($request);

		switch ($id_level_wilayah) {
			case 0:
				$param_wilayah = 'propinsi';
				$wilayah = 'null';
				$param_kode_wilayah = '';
				break;
			case 1:
				$param_wilayah = 'kabupaten';
				$wilayah = 'propinsi';
				$param_kode_wilayah = ' AND rekap_sekolah.mst_kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			case 2:
				$param_wilayah = 'kecamatan';
				$wilayah = 'kabupaten';
				$param_kode_wilayah = ' AND rekap_sekolah.mst_kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			case 3:
				$param_wilayah = 'kecamatan';
				$wilayah = 'kecamatan';
				$param_kode_wilayah = ' AND rekap_sekolah.kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			default:
				$param_wilayah = 'propinsi';
				$wilayah = 'null';
				$param_kode_wilayah = '';
				break;
		}	
		
		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMA'){
				$param_bp = " AND rekap_sekolah.bentuk_pendidikan_id in (13,55)";
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$param_bp = " AND rekap_sekolah.bentuk_pendidikan_id = 15";
			}else if(BENTUKPENDIDIKAN == 'SMASMK'){
				$param_bp = " AND rekap_sekolah.bentuk_pendidikan_id IN (13,15,55)";
			}else{
				$param_bp = " AND rekap_sekolah.bentuk_pendidikan_id IN (13,15,55)";
			}

		}
		else if(TINGKAT == 'DIKDAS'){
			$param_bp = " AND rekap_sekolah.bentuk_pendidikan_id IN (5,6,14,29,53,54)";
		}
		else{
			$param_bp = " AND rekap_sekolah.bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29,13,15,53,54,55)";
		}

		if($belum_sinkron_saja == 'false'){
			$param_belum_sinkron = '';
		}else{
			$param_belum_sinkron = ' AND rekap_pengiriman.jumlah_kirim < 1';
		}	
			

		$sql = "SELECT
					rekap_sekolah.sekolah_id,
					rekap_sekolah.nama,
					rekap_sekolah.npsn,
					bp.nama as bentuk,
					rekap_pengiriman.jumlah_kirim,
					rekap_pengiriman.sinkron_terakhir,
					{$wilayah} as wilayah,
					rekap_sekolah.tanggal as tanggal_rekap_terakhir
				FROM
					rekap_sekolah
				JOIN rekap_pengiriman on rekap_pengiriman.sekolah_id = rekap_sekolah.sekolah_id and rekap_pengiriman.semester_id = ".$semester_id."
				JOIN ref.bentuk_pendidikan bp on bp.bentuk_pendidikan_id = rekap_sekolah.bentuk_pendidikan_id
				WHERE
					rekap_sekolah.soft_delete = 0
				AND rekap_sekolah.semester_id = ".$semester_id."
				{$param_kode_wilayah}
				{$param_belum_sinkron}
				{$param_bp}";

		$return_count = Preface::getDataByQuery($sql, 'rekap');

		$sql .= "order by rekap_pengiriman.jumlah_kirim desc OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

		$return = Preface::getDataByQuery($sql, 'rekap');

		$final = array();
		$final['results'] = sizeof($return_count);
		$final['rows'] = $return;

		return json_encode($final);
	}

	static function Pengiriman(Request $request, Application $app){
		$id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah'); 
		$kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah');
		$semester_id = $app['session']->get('semester_berjalan') ? $app['session']->get('semester_berjalan') : SEMESTER_BERJALAN;  

		switch ($id_level_wilayah) {
			case 0:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
			case 1:
				$param_wilayah = 'kabupaten';
				$param_kode_wilayah = ' AND rekap_sekolah.mst_kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			case 2:
				$param_wilayah = 'kecamatan';
				$param_kode_wilayah = ' AND rekap_sekolah.mst_kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			default:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
		}

		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMA'){
				$param_bp = " AND rekap_sekolah.bentuk_pendidikan_id in (13,55)";
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$param_bp = " AND rekap_sekolah.bentuk_pendidikan_id = 15";
			}else if(BENTUKPENDIDIKAN == 'SMASMK'){
				$param_bp = " AND rekap_sekolah.bentuk_pendidikan_id IN (13,15,55)";
			}else{
				$param_bp = " AND rekap_sekolah.bentuk_pendidikan_id IN (13,15,55)";
			}

		}
		else if(TINGKAT == 'DIKDAS'){
			$param_bp = " AND rekap_sekolah.bentuk_pendidikan_id IN (5,6,14,29,53,54)";
		}
		else{
			$param_bp = " AND rekap_sekolah.bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29,13,15,53,54,55)";
		}

		$sql = "SELECT
					rekap_sekolah.kode_wilayah_{$param_wilayah} as kode_wilayah,
					rekap_sekolah.{$param_wilayah} as nama,
					rekap_sekolah.id_level_wilayah_{$param_wilayah} as id_level_wilayah,
					rekap_sekolah.mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (5,53) then 1 else 0 end) as sd,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (6,54) then 1 else 0 end) as smp,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (13,55) then 1 else 0 end) as sma,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id = 15 then 1 else 0 end) as smk,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (7,8,14,29) then 1 else 0 end) as slb,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (5,53,6,54,13,55,15,7,8,14,29) then 1 else 0 end) as total,

					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (5,53) and rekap_pengiriman.jumlah_kirim > 0 then 1 else 0 end) as kirim_sd,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (6,54) and rekap_pengiriman.jumlah_kirim > 0 then 1 else 0 end) as kirim_smp,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (13,55) and rekap_pengiriman.jumlah_kirim > 0 then 1 else 0 end) as kirim_sma,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id = 15 and rekap_pengiriman.jumlah_kirim > 0 then 1 else 0 end) as kirim_smk,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (7,8,14,29) and rekap_pengiriman.jumlah_kirim > 0 then 1 else 0 end) as kirim_slb,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (5,53,6,54,13,55,15,7,8,14,29) and rekap_pengiriman.jumlah_kirim > 0 then 1 else 0 end) as kirim_total,

					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (5,53) and rekap_pengiriman.jumlah_kirim < 1 then 1 else 0 end) as sisa_sd,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (6,54) and rekap_pengiriman.jumlah_kirim < 1 then 1 else 0 end) as sisa_smp,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (13,55) and rekap_pengiriman.jumlah_kirim < 1 then 1 else 0 end) as sisa_sma,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id = 15 and rekap_pengiriman.jumlah_kirim < 1 then 1 else 0 end) as sisa_smk,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (7,8,14,29) and rekap_pengiriman.jumlah_kirim < 1 then 1 else 0 end) as sisa_slb,
					SUM (case when rekap_sekolah.bentuk_pendidikan_id in (5,53,6,54,13,55,15,7,8,14,29) and rekap_pengiriman.jumlah_kirim < 1 then 1 else 0 end) as sisa_total,

					min(rekap_sekolah.tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah
				JOIN rekap_pengiriman on rekap_pengiriman.sekolah_id = rekap_sekolah.sekolah_id and rekap_pengiriman.semester_id = ".$semester_id."
				WHERE
					rekap_sekolah.soft_delete = 0
				AND rekap_sekolah.semester_id = ".$semester_id."
				{$param_kode_wilayah}
				{$param_bp}
				GROUP BY
					rekap_sekolah.kode_wilayah_{$param_wilayah},
					rekap_sekolah.{$param_wilayah},
					rekap_sekolah.id_level_wilayah_{$param_wilayah},
					rekap_sekolah.mst_kode_wilayah_{$param_wilayah}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		for ($i=0; $i < sizeof($return); $i++) { 
			$persen_total = round((($return[$i]['kirim_total']/$return[$i]['total'])*100),2);

			if($return[$i]['sma'] == 0){
				$return[$i]['sma'] = 1;
			}

			$persen_sma = round((($return[$i]['kirim_sma']/$return[$i]['sma'])*100),2);

			$return[$i]['persen'] = $persen_total;
			$return[$i]['persen_sma'] = $persen_sma;
		}

		$final = array();
		$final['results'] = sizeof($return);
		$final['rows'] = $return;

		return json_encode($final);


	}
	
	static function PengirimanAPI(Request $request, Application $app){
		$id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah'); 
		$kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah'); 

		$url = 'http://dapo.dikdasmen.kemdikbud.go.id/rekap/progres?id_level_wilayah='.$id_level_wilayah.'&kode_wilayah='.trim($kode_wilayah).'&semester_id=20161';
		$json = file_get_contents($url);
		
		$arrJson = json_decode($json);

		$persen_total = 0;
		$persen_sma = 0;
		$i = 0;

		$return = array();

		foreach ($arrJson as $arr) {

			if($arr->{'sma'} != 0){
				// $arr->{'sma'} = 1;
				$persen_sma = ($arr->{'kirim_sma'}/$arr->{'sma'})*100;
			}else{
				$persen_sma = 0;
			}


			$arr->{'persen_sma'} = $persen_sma;

			array_push($return, $arr);
		}

		return json_encode($return);

		
	}

	static function PengirimanLocalQuery(Request $request, Application $app){

		$session = $app['session'];

		// return $session->get('username');

		$sql = "SELECT
					kode_wilayah_provinsi,
					provinsi,
					SUM (case when sg.bentuk_pendidikan_id in (5,6,7,8,13,14,15,29) then 1 else 0 end) AS total,
					SUM (case when jumlah_kirim > 0 and sg.bentuk_pendidikan_id in (5,6,7,8,13,14,15,29) then 1 else 0 end) as total_kirim,
					SUM (case when sg.bentuk_pendidikan_id = 5 then 1 else 0 end) AS sd_total,
					sum (case when jumlah_kirim > 0 and sg.bentuk_pendidikan_id = 5 then 1 else 0 end) as sd_kirim,
					SUM (case when sg.bentuk_pendidikan_id = 6 then 1 else 0 end) AS smp_total,
					sum (case when jumlah_kirim > 0 and sg.bentuk_pendidikan_id = 6 then 1 else 0 end) as smp_kirim,
					SUM (case when sg.bentuk_pendidikan_id = 13 then 1 else 0 end) AS sma_total,
					sum (case when jumlah_kirim > 0 and sg.bentuk_pendidikan_id = 13 then 1 else 0 end) as sma_kirim,
					SUM (case when sg.bentuk_pendidikan_id = 15 then 1 else 0 end) AS smk_total,
					sum (case when jumlah_kirim > 0 and sg.bentuk_pendidikan_id = 15 then 1 else 0 end) as smk_kirim,
					SUM (case when sg.bentuk_pendidikan_id in (7,8,14,29) then 1 else 0 end) AS slb_total,
					sum (case when jumlah_kirim > 0 and sg.bentuk_pendidikan_id in (7,8,14,29) then 1 else 0 end) as slb_kirim
				FROM
					rekap_sekolah
				JOIN sekolah_gabungan sg ON sg.sekolah_id = rekap_sekolah.sekolah_id
				WHERE
					rekap_sekolah.semester_id = 20161
				AND sg.semester_id = 20161
				AND rekap_sekolah.soft_delete = 0
				AND sg.soft_delete = 0
				GROUP BY
					kode_wilayah_provinsi,
					provinsi
				order by kode_wilayah_provinsi;";

	}

}