<?php
namespace Admin\progres;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Admin\Auth;
use Admin\Preface;

class Pencapaian{

	static function Pencapaian(Request $request, Application $app){

		$id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah'); 
		$kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah');
		$semester_id = $app['session']->get('semester_berjalan') ? $app['session']->get('semester_berjalan') : SEMESTER_BERJALAN;  

		switch ($id_level_wilayah) {
			case 0:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
			case 1:
				$param_wilayah = 'kabupaten';
				$param_kode_wilayah = ' AND rekap_kualitas_data_periodik.mst_kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			case 2:
				$param_wilayah = 'kecamatan';
				$param_kode_wilayah = ' AND rekap_kualitas_data_periodik.mst_kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			default:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
		}

		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMA'){
				$param_bp = " AND rekap_kualitas_data_periodik.bentuk_pendidikan_id in (13,55)";
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$param_bp = " AND rekap_kualitas_data_periodik.bentuk_pendidikan_id = 15";
			}else if(BENTUKPENDIDIKAN == 'SMASMK'){
				$param_bp = " AND rekap_kualitas_data_periodik.bentuk_pendidikan_id IN (13,15,55)";
			}else{
				$param_bp = " AND rekap_kualitas_data_periodik.bentuk_pendidikan_id IN (13,15,55)";
			}

		}
		else if(TINGKAT == 'DIKDAS'){
			$param_bp = " AND rekap_kualitas_data_periodik.bentuk_pendidikan_id IN (5,6,14,29,53,54)";
		}
		else{
			$param_bp = " AND rekap_kualitas_data_periodik.bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29,13,15,53,54,55)";
		}

		$sql = "SELECT
					rekap_kualitas_data_periodik.kode_wilayah_{$param_wilayah} as kode_wilayah,
					rekap_kualitas_data_periodik.{$param_wilayah} as nama,
					rekap_kualitas_data_periodik.id_level_wilayah_{$param_wilayah} as id_level_wilayah,
					rekap_kualitas_data_periodik.mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
					round((SUM(rata_rata_valid_bulan_1) / SUM (1)),2) as rata_rata_valid_bulan_1,
					round((SUM(rata_rata_valid_bulan_2) / SUM (1)),2) as rata_rata_valid_bulan_2,
					round((SUM(rata_rata_valid_bulan_3) / SUM (1)),2) as rata_rata_valid_bulan_3,
					round((SUM(rata_rata_valid_bulan_4) / SUM (1)),2) as rata_rata_valid_bulan_4,
					round((SUM(rata_rata_valid_bulan_5) / SUM (1)),2) as rata_rata_valid_bulan_5,
					round((SUM(rata_rata_valid_bulan_6) / SUM (1)),2) as rata_rata_valid_bulan_6,
					round((SUM(rata_rata_valid_bulan_7) / SUM (1)),2) as rata_rata_valid_bulan_7,
					round((SUM(rata_rata_valid_bulan_8) / SUM (1)),2) as rata_rata_valid_bulan_8,
					round((SUM(rata_rata_valid_bulan_9) / SUM (1)),2) as rata_rata_valid_bulan_9,
					round((SUM(rata_rata_valid_bulan_10) / SUM (1)),2) as rata_rata_valid_bulan_10,
					round((SUM(rata_rata_valid_bulan_11) / SUM (1)),2) as rata_rata_valid_bulan_11,
					round((SUM(rata_rata_valid_bulan_12) / SUM (1)),2) as rata_rata_valid_bulan_12,
					min(rekap_kualitas_data_periodik.tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_kualitas_data_periodik
				WHERE
					rekap_kualitas_data_periodik.soft_delete = 0
				AND rekap_kualitas_data_periodik.semester_id = ".$semester_id."
				{$param_kode_wilayah}
				{$param_bp}
				GROUP BY
					rekap_kualitas_data_periodik.kode_wilayah_{$param_wilayah},
					rekap_kualitas_data_periodik.{$param_wilayah},
					rekap_kualitas_data_periodik.id_level_wilayah_{$param_wilayah},
					rekap_kualitas_data_periodik.mst_kode_wilayah_{$param_wilayah}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		$final = array();
		$final['results'] = sizeof($return);
		$final['rows'] = $return;

		return json_encode($final);

	}

	static function PencapaianSp(Request $request, Application $app){
		$id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah'); 
		$kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah');
		$semester_id = $app['session']->get('semester_berjalan') ? $app['session']->get('semester_berjalan') : SEMESTER_BERJALAN;  

		$requesta = Preface::Inisiasi($request);

		switch ($id_level_wilayah) {
			case 0:
				$param_wilayah = 'propinsi';
				$wilayah = '\'Indonesia\'';
				$param_kode_wilayah = '';
				break;
			case 1:
				$param_wilayah = 'kabupaten';
				$wilayah = 'propinsi';
				$param_kode_wilayah = ' AND rekap_kualitas_data_periodik.mst_kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			case 2:
				$param_wilayah = 'kecamatan';
				$wilayah = 'kabupaten';
				$param_kode_wilayah = ' AND rekap_kualitas_data_periodik.mst_kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			default:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
		}	

		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMA'){
				$param_bp = " AND rekap_kualitas_data_periodik.bentuk_pendidikan_id in (13,55)";
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$param_bp = " AND rekap_kualitas_data_periodik.bentuk_pendidikan_id = 15";
			}else if(BENTUKPENDIDIKAN == 'SMASMK'){
				$param_bp = " AND rekap_kualitas_data_periodik.bentuk_pendidikan_id IN (13,15,55)";
			}else{
				$param_bp = " AND rekap_kualitas_data_periodik.bentuk_pendidikan_id IN (13,15,55)";
			}

		}
		else if(TINGKAT == 'DIKDAS'){
			$param_bp = " AND rekap_kualitas_data_periodik.bentuk_pendidikan_id IN (5,6,14,29,53,54)";
		}
		else{
			$param_bp = " AND rekap_kualitas_data_periodik.bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29,13,15,53,54,55)";
		}

		$sql = "SELECT
					rekap_kualitas_data_periodik.sekolah_id,
					rekap_kualitas_data_periodik.nama,
					rekap_kualitas_data_periodik.npsn,
					bp.nama as bentuk,
					{$wilayah} as wilayah,
					kecamatan,
					kabupaten,
					propinsi,
					rata_rata_valid_bulan_1,
					rata_rata_valid_bulan_2,
					rata_rata_valid_bulan_3,
					rata_rata_valid_bulan_4,
					rata_rata_valid_bulan_5,
					rata_rata_valid_bulan_6,
					rata_rata_valid_bulan_7,
					rata_rata_valid_bulan_8,
					rata_rata_valid_bulan_9,
					rata_rata_valid_bulan_10,
					rata_rata_valid_bulan_11,
					rata_rata_valid_bulan_12,
					rekap_kualitas_data_periodik.tanggal as tanggal_rekap_terakhir
				FROM
					rekap_kualitas_data_periodik
				JOIN ref.bentuk_pendidikan bp on bp.bentuk_pendidikan_id = rekap_kualitas_data_periodik.bentuk_pendidikan_id
				WHERE
					rekap_kualitas_data_periodik.soft_delete = 0
				AND rekap_kualitas_data_periodik.semester_id = ".$semester_id."
				{$param_kode_wilayah}
				{$param_bp}";

		$return_count = Preface::getDataByQuery($sql, 'rekap');

		$sql .= "order by rekap_kualitas_data_periodik.nama desc OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

		$return = Preface::getDataByQuery($sql, 'rekap');

		$final = array();
		$final['results'] = sizeof($return_count);
		$final['rows'] = $return;

		return json_encode($final);
	}
}