<?php
namespace Admin\progres;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Admin\Auth;
use Admin\Preface;

class KualitasDataSekolah{

	static function KualitasDataIdentitasSp(Request $request, Application $app){
		$kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah'); 
		$id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah');
		$keyword = $request->get('keyword') ? $request->get('keyword') : null;
		$semester_id = $app['session']->get('semester_berjalan') ? $app['session']->get('semester_berjalan') : SEMESTER_BERJALAN; 

		$start = $request->get('start') ? $request->get('start') : 0;  
		$limit = $request->get('limit') ? $request->get('limit') : 50;  

		switch ($id_level_wilayah) {
			case 0:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
			case 1:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = ' AND rekap.kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			case 2:
				$param_wilayah = 'kabupaten';
				$param_kode_wilayah = ' AND rekap.kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			case 3:
				$param_wilayah = 'kecamatan';
				$param_kode_wilayah = ' AND rekap.kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			default:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
		}

		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMA'){
				$param_bp = " AND rekap.bentuk_pendidikan_id = 13";
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$param_bp = " AND rekap.bentuk_pendidikan_id = 15";
			}else if(BENTUKPENDIDIKAN == 'SMASMK'){
				$param_bp = " AND rekap.bentuk_pendidikan_id IN (13,15)";
			}else{
				$param_bp = " AND rekap.bentuk_pendidikan_id IN (13,15)";
			}

		}
		else if(TINGKAT == 'DIKDAS'){
			$param_bp = " AND rekap.bentuk_pendidikan_id IN (5,6,14,29)";
		}
		else{
			$param_bp = " AND rekap.bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29,13,15)";
		}

		if($keyword){
			$param_keyword = " AND rekap.nama like '%".$keyword."%'";
		}else{
			$param_keyword = "";
		}

		$sql = "SELECT
					sum(1) as total
				FROM
					rekap_kualitas_data_sekolah rekap
				LEFT OUTER JOIN rekap_kualitas_data_sekolah_sum sum on sum.sekolah_id = rekap.sekolah_id and sum.semester_id = rekap.semester_id
				WHERE
					rekap.soft_delete = 0
					AND rekap.semester_id = ".$semester_id."
					{$param_kode_wilayah}
					{$param_bp}
					{$param_keyword}";

		$return_count = Preface::getDataByQuery($sql, 'rekap');
		
		$sql = "SELECT
					rekap.*,
					isnull(identitas_valid,0) as total_valid
				FROM
					rekap_kualitas_data_sekolah rekap
				LEFT OUTER JOIN rekap_kualitas_data_sekolah_sum sum on sum.sekolah_id = rekap.sekolah_id and sum.semester_id = rekap.semester_id
				WHERE
					rekap.soft_delete = 0
					AND rekap.semester_id = ".$semester_id."
					{$param_kode_wilayah}
					{$param_bp}
					{$param_keyword}
					";
					
		$sql .= " ORDER BY
						rekap.sekolah_id desc OFFSET {$start} ROWS FETCH NEXT {$limit} ROWS ONLY";

		$return = Preface::getDataByQuery($sql, 'rekap');

		$final = array();
		$final['results'] = $return_count[0]['total'];
		$final['rows'] = $return;

		return json_encode($final);
	}

	static function KualitasDataIdentitas(Request $request, Application $app){
		$kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah'); 
		$id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah');
		$keyword = $request->get('keyword') ? $request->get('keyword') : null;
		$semester_id = $app['session']->get('semester_berjalan') ? $app['session']->get('semester_berjalan') : SEMESTER_BERJALAN; 

		switch ($id_level_wilayah) {
			case 0:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
			case 1:
				$param_wilayah = 'kabupaten';
				$param_kode_wilayah = ' AND rekap.mst_kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			case 2:
				$param_wilayah = 'kecamatan';
				$param_kode_wilayah = ' AND rekap.mst_kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			default:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
		}

		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMA'){
				$param_bp = " AND rekap.bentuk_pendidikan_id in (13,55)";
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$param_bp = " AND rekap.bentuk_pendidikan_id = 15";
			}else if(BENTUKPENDIDIKAN == 'SMASMK'){
				$param_bp = " AND rekap.bentuk_pendidikan_id IN (13,15,55)";
			}else{
				$param_bp = " AND rekap.bentuk_pendidikan_id IN (13,15,55)";
			}

		}
		else if(TINGKAT == 'DIKDAS'){
			$param_bp = " AND rekap.bentuk_pendidikan_id IN (5,6,14,29,53,54)";
		}
		else{
			$param_bp = " AND rekap.bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29,13,15,53,54,55)";
		}

		if($keyword){
			$param_keyword = " AND rekap.".$param_wilayah." like '%".$keyword."%'";
		}else{
			$param_keyword = "";
		}

		$sql = "SELECT
					rekap.kode_wilayah_{$param_wilayah} as kode_wilayah,
					rekap.{$param_wilayah} as wilayah,
					rekap.id_level_wilayah_{$param_wilayah} as id_level_wilayah,
					rekap.mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
					round((SUM(identitas_valid) / SUM(1)), 2) as total_valid,
					round(((( cast((SUM (npsn_valid)) as float(24)) / sum(1))*100)),2) AS npsn_valid,
					round(((( cast((SUM (sk_pendirian_sekolah_valid)) as float(24)) / sum(1))*100)),2) AS sk_pendirian_sekolah_valid,
					round(((( cast((SUM (tanggal_sk_pendirian_valid)) as float(24)) / sum(1))*100)),2) AS tanggal_sk_pendirian_valid,
					round(((( cast((SUM (sk_izin_operasional_valid)) as float(24)) / sum(1))*100)),2) AS sk_izin_operasional_valid,
					round(((( cast((SUM (tanggal_sk_izin_operasional_valid)) as float(24)) / sum(1))*100)),2) AS tanggal_sk_izin_operasional_valid,
					round(((( cast((SUM (no_rekening_valid)) as float(24)) / sum(1))*100)),2) AS no_rekening_valid,
					round(((( cast((SUM (nama_bank_valid)) as float(24)) / sum(1))*100)),2) AS nama_bank_valid,
					round(((( cast((SUM (cabang_kcp_unit_valid)) as float(24)) / sum(1))*100)),2) AS cabang_kcp_unit_valid,
					round(((( cast((SUM (rekening_atas_nama_valid)) as float(24)) / sum(1))*100)),2) AS rekening_atas_nama_valid,
					round(((( cast((SUM (no_hp_kepsek_valid)) as float(24)) / sum(1))*100)),2) AS no_hp_kepsek_valid,
					round(((( cast((SUM (nama_kepsek_valid)) as float(24)) / sum(1))*100)),2) AS nama_kepsek_valid,
					round(((( cast((SUM (email_kepsek_valid)) as float(24)) / sum(1))*100)),2) AS email_kepsek_valid,
					round(((( cast((SUM (daya_listrik_valid)) as float(24)) / sum(1))*100)),2) AS daya_listrik_valid,
					round(((( cast((SUM (partisipasi_bos_valid)) as float(24)) / sum(1))*100)),2) AS partisipasi_bos_valid,
					round(((( cast((SUM (waktu_penyelenggaraan_id_valid)) as float(24)) / sum(1))*100)),2) AS waktu_penyelenggaraan_id_valid,
					round(((( cast((SUM (sumber_listrik_id_valid)) as float(24)) / sum(1))*100)),2) AS sumber_listrik_id_valid,
					round(((( cast((SUM (sertifikasi_iso_id_valid)) as float(24)) / sum(1))*100)),2) AS sertifikasi_iso_id_valid,
					round(((( cast((SUM (akses_internet_id_valid)) as float(24)) / sum(1))*100)),2) AS akses_internet_id_valid,
					min(rekap.tanggal) as tanggal_rekap_terakhir				
				FROM
					rekap_kualitas_data_sekolah rekap
				JOIN rekap_kualitas_data_sekolah_sum sum on sum.sekolah_id = rekap.sekolah_id and sum.semester_id = rekap.semester_id
				WHERE
					rekap.semester_id = ".$semester_id."
				AND rekap.soft_delete = 0
				{$param_kode_wilayah}
				{$param_bp}
				{$param_keyword}
				GROUP BY
					rekap.kode_wilayah_{$param_wilayah},
					rekap.{$param_wilayah},
					rekap.id_level_wilayah_{$param_wilayah},
					rekap.mst_kode_wilayah_{$param_wilayah}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		$final = array();
		$final['results'] = sizeof($return);
		$final['rows'] = $return;

		return json_encode($final);
	}

	static function KualitasDataSekolahSp(Request $request, Application $app){
		$kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah'); 
		$id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah');
		$keyword = $request->get('keyword') ? $request->get('keyword') : null;
		$semester_id = $app['session']->get('semester_berjalan') ? $app['session']->get('semester_berjalan') : SEMESTER_BERJALAN; 

		$start = $request->get('start') ? $request->get('start') : 0;  
		$limit = $request->get('limit') ? $request->get('limit') : 50;  

		switch ($id_level_wilayah) {
			case 0:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
			case 1:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = ' AND kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			case 2:
				$param_wilayah = 'kabupaten';
				$param_kode_wilayah = ' AND kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			case 3:
				$param_wilayah = 'kecamatan';
				$param_kode_wilayah = ' AND kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			default:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
		}

		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMA'){
				$param_bp = " AND bentuk_pendidikan_id = 13";
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$param_bp = " AND bentuk_pendidikan_id = 15";
			}else if(BENTUKPENDIDIKAN == 'SMASMK'){
				$param_bp = " AND bentuk_pendidikan_id IN (13,15)";
			}else{
				$param_bp = " AND bentuk_pendidikan_id IN (13,15)";
			}

		}
		else if(TINGKAT == 'DIKDAS'){
			$param_bp = " AND bentuk_pendidikan_id IN (5,6,14,29)";
		}
		else{
			$param_bp = " AND bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29,13,15)";
		}

		if($keyword){
			$param_keyword = " AND nama like '%".$keyword."%'";
		}else{
			$param_keyword = "";
		}

		$sql = "SELECT
					sum(1) as total
				FROM
					rekap_kualitas_data_sekolah_sum
				WHERE
					soft_delete = 0
					AND semester_id = ".$semester_id."
					{$param_kode_wilayah}
					{$param_bp}
					{$param_keyword}";

		$return_count = Preface::getDataByQuery($sql, 'rekap');
		
		$sql = "SELECT
					*,
					rata_rata_valid as total_valid,
					tanggal as tanggal_rekap_terakhir
				FROM
					rekap_kualitas_data_sekolah_sum
				WHERE
					soft_delete = 0
					AND semester_id = ".$semester_id."
					{$param_kode_wilayah}
					{$param_bp}
					{$param_keyword}";
					
		$sql .= " ORDER BY
						rata_rata_valid desc OFFSET {$start} ROWS FETCH NEXT {$limit} ROWS ONLY";

		$return = Preface::getDataByQuery($sql, 'rekap');

		$final = array();
		$final['results'] = $return_count[0]['total'];
		$final['rows'] = $return;

		return json_encode($final);
	}

	static function KualitasDataSekolah(Request $request, Application $app){

		$kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah'); 
		$id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah'); 
		$keyword = $request->get('keyword') ? $request->get('keyword') : null; 
		$semester_id = $app['session']->get('semester_berjalan') ? $app['session']->get('semester_berjalan') : SEMESTER_BERJALAN; 
		
		switch ($id_level_wilayah) {
			case 0:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
			case 1:
				$param_wilayah = 'kabupaten';
				$param_kode_wilayah = ' AND mst_kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			case 2:
				$param_wilayah = 'kecamatan';
				$param_kode_wilayah = ' AND mst_kode_wilayah_'.$param_wilayah.'='.$kode_wilayah;
				break;
			default:
				$param_wilayah = 'propinsi';
				$param_kode_wilayah = '';
				break;
		}

		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMA'){
				$param_bp = " AND bentuk_pendidikan_id = 13";
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$param_bp = " AND bentuk_pendidikan_id = 15";
			}else if(BENTUKPENDIDIKAN == 'SMASMK'){
				$param_bp = " AND bentuk_pendidikan_id IN (13,15)";
			}else{
				$param_bp = " AND bentuk_pendidikan_id IN (13,15)";
			}

		}
		else if(TINGKAT == 'DIKDAS'){
			$param_bp = " AND bentuk_pendidikan_id IN (5,6,14,29)";
		}
		else{
			$param_bp = " AND bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29,13,15)";
		}

		if($keyword){
			$param_keyword = " AND ".$param_wilayah." like '%".$keyword."%'";
		}else{
			$param_keyword = "";
		}

		$sql = "SELECT
					kode_wilayah_{$param_wilayah} as kode_wilayah,
					{$param_wilayah} as wilayah,
					id_level_wilayah_{$param_wilayah} as id_level_wilayah,
					mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
					round(
						(
							SUM (identitas_valid) / SUM (1)
						),
						2
					) AS identitas_valid,
					round((SUM(ptk_valid) / SUM(1)), 2) AS ptk_valid,
					round((SUM(pd_valid) / SUM(1)), 2) AS pd_valid,
					round(
						(
							SUM (prasarana_valid) / SUM (1)
						),
						2
					) AS prasarana_valid,
					round((SUM(rata_rata_valid) / SUM(1)), 2) AS total_valid,
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_kualitas_data_sekolah_sum
				WHERE
					semester_id = ".$semester_id."
				AND soft_delete = 0
				{$param_kode_wilayah}
				{$param_bp}
				{$param_keyword}
				GROUP BY
					kode_wilayah_{$param_wilayah},
					{$param_wilayah},
					id_level_wilayah_{$param_wilayah},
					mst_kode_wilayah_{$param_wilayah}
				order by round((SUM(rata_rata_valid) / SUM(1)), 2) desc;";

				// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		$final = array();
		$final['results'] = sizeof($return);
		$final['rows'] = $return;

		return json_encode($final);

	}
}