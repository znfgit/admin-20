<?php

namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Admin\Model\PtkTerdaftarPeer;
use Admin\Model\PtkPeer;
use Admin\Model\JenisPtkPeer;
use Admin\Model\PesertaDidikPeer;
use Admin\Model\RegistrasiPesertaDidikPeer;
use Admin\Model\RombonganBelajarPeer;
use Admin\Model\AnggotaRombelPeer;

class CustomRest {

	function tesPanggil(Request $request, Application $app){
		return 'tes oke';
	}

	static function JenisPtk(Request $request, Application $app){
		// return "oke";

		$returnFinal = array();

		$return = array();
		$return['jenis_ptk_id'] = '1';
		$return['jenis_ptk'] = 'Guru';
		array_push($returnFinal, $return);


		$return = array();
		$return['jenis_ptk_id'] = '2';
		$return['jenis_ptk'] = 'Tendik';
		array_push($returnFinal, $return);

		$return = array();
		$return['jenis_ptk_id'] = '3';
		$return['jenis_ptk'] = 'Semua';
		array_push($returnFinal, $return);

		return json_encode($returnFinal);
	}

	function getKoodinatWilayah(Request $request, Application $app){
		$kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : KODE_WILAYAH;

		$sql = "select * from koordinat_wilayah where kode_wilayah = '{$kode_wilayah}'";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

        if(sizeof($return) == 0){
            $host = 'siduren.dikdasmen.kemdikbud.go.id';
            $geometry = @file_get_contents('http://'.$host.'/geoNew/'.substr($kode_wilayah,0,6).'.txt', true);

            if(substr($geometry, 0, 4) == '[[[['){
                $geometry = substr($geometry, 1, strlen($geometry)-2);
            }
            
            $pos = strpos($geometry, ',');

            // return substr($geometry,($pos+1),7);
            return '[{"kode_wilayah":"'.$kode_wilayah.'","lintang":"'.substr($geometry,3,7).'","bujur":"'.substr($geometry,($pos+1),7).'","zoom":9}]';
        }else{
		  return json_encode($return);  
        }

	}

	static function getPtk(Request $request, Application $app) {

		$requesta = Preface::Inisiasi($request);

        $sekolah_id = $requesta['sekolah_id'];
        $nama_ptk = $request->get('nama') ? $request->get('nama') : '';
        $query = $request->get('query') ? $request->get('query') : '';
        $tahun_ajaran_id = TA_BERJALAN;
        $ptk_module = $request->get('ptk_module') ? $request->get('ptk_module') : 'ptkterdaftar';

        //buat jenis gtk
        $jenis_gtk = $request->get('jenis_gtk') ? $request->get('jenis_gtk') : 'semua';

        $start = ($request->get('start')) ? $request->get('start') : 0;

        if ($request->get('limit') == 'unlimited') {
            $limit = 100000;
        } else if (!$request->get('limit')) {
            $limit = 50;
        } else {
            $limit = $request->get('limit');
        }

        if($request->get('sort')){
            $sorts = $request->get('sort');
        }

        $c = new \Criteria();
        $c->addJoin(PtkTerdaftarPeer::PTK_ID, PtkPeer::PTK_ID, \Criteria::JOIN);
        $c->addJoin(PtkPeer::JENIS_PTK_ID, JenisPtkPeer::JENIS_PTK_ID, \Criteria::JOIN);
        $c->add(PtkTerdaftarPeer::TAHUN_AJARAN_ID, $tahun_ajaran_id);
        $c->add(PtkTerdaftarPeer::SEKOLAH_ID, $sekolah_id);
        $c->add(PtkTerdaftarPeer::SOFT_DELETE, 0);
        $c->addAscendingOrderByColumn(PtkPeer::NAMA);

        if ($ptk_module == "ptkterdaftar") {
            $c->add(PtkTerdaftarPeer::JENIS_KELUAR_ID, NULL, \Criteria::ISNULL);
        } else if ($ptk_module == "ptkkeluar") {
            $c->add(PtkTerdaftarPeer::JENIS_KELUAR_ID, NULL, \Criteria::ISNOTNULL);
        }

        $c->add(PtkPeer::SOFT_DELETE, 0);
        if ($nama_ptk) {
            $c->add(PtkPeer::NAMA, '%'.$nama_ptk.'%', \Criteria::ILIKE);
        }
        if ($query) {
            $c->add(PtkPeer::NAMA, '%'.$query.'%', \Criteria::ILIKE);
        }

        $rowCount = PtkTerdaftarPeer::doCount($c);

        $c->setOffset($start);
        if ($limit > 0) {
            $c->setLimit($limit);
        }

        $ptkTerdaftars = PtkTerdaftarPeer::doSelect($c);

        $outArr = array();

        foreach ($ptkTerdaftars as $pt) {

            $arr = $pt->toArray(\BasePeer::TYPE_FIELDNAME);
            $arr["jenis_keluar_id_str"] = $pt->getJenisKeluarId() ? $pt->getJenisKeluar()->getKetKeluar() : "";

            $arrPtk = $pt->getPtk()->toArray(\BasePeer::TYPE_FIELDNAME);
            foreach ($arrPtk as $key => $value) {
                $arr[$key] = $value;
            }
            $arr['jenis_ptk_id_str'] = $pt->getPtk()->getJenisPtk()->getJenisPtk();

            $outArr[] = $arr;
        }

        return json_encode($outArr);
        // return tableJson($outArr, $rowCount, PtkPeer::getFieldNames(\BasePeer::TYPE_FIELDNAME));
    }

    function getPesertaDidik(Request $request, Application $app) {

        $requesta = Preface::Inisiasi($request);

        $sekolah_id = $requesta['sekolah_id'];
        $query = $request->get('query');
        $nama_pd = $request->get('nama') ? $request->get('nama') : '';
        $pd_module = $request->get('pd_module');

        $start = ($request->get('start')) ? $request->get('start') : 0;

        if ($request->get('limit') == 'unlimited') {
            $limit = 0;
        } else if (!$request->get('limit')) {
            $limit = 50;
        } else {
            $limit = $request->get('limit');
        }

        $outArr = array();

        if ($pd_module == "pdterdaftar") {
            $sql = "select * from (
                        select
                            peserta_didik.*,
                            registrasi_peserta_didik.*,
                            peserta_didik.nama as nama_sorter
                        from peserta_didik
                        join registrasi_peserta_didik
                            on peserta_didik.peserta_didik_id = registrasi_peserta_didik.peserta_didik_id
                            and registrasi_peserta_didik.jenis_keluar_id is null
                            and registrasi_peserta_didik.sekolah_id = '{$sekolah_id}'
                        where
                            peserta_didik.soft_delete = 0
                        union all
                        select
                            peserta_didik.*,
                            registrasi_peserta_didik.*,
                            'AAAAAAAAAA' as nama_sorter
                        from peserta_didik
                        left join registrasi_peserta_didik
                            on peserta_didik.peserta_didik_id = registrasi_peserta_didik.peserta_didik_id
                        where
                            peserta_didik.soft_delete = 0
                            and registrasi_peserta_didik.registrasi_id is null
                    ) pd_all";
        } else {
            $sql = "select
                        peserta_didik.*,
                        registrasi_peserta_didik.*
                    from peserta_didik
                    join registrasi_peserta_didik
                        on peserta_didik.peserta_didik_id = registrasi_peserta_didik.peserta_didik_id
                        and registrasi_peserta_didik.sekolah_id = '{$sekolah_id}'
                    where
                        peserta_didik.soft_delete = 0
                        and registrasi_peserta_didik.jenis_keluar_id is not null ";

            if($request->get('tahun_kelulusan')){
                $sql .= " and (registrasi_peserta_didik.tanggal_keluar > '".$request->get('tahun_kelulusan').'-01-01'."' and registrasi_peserta_didik.tanggal_keluar < '".$request->get('tahun_kelulusan').'-12-31'."') ";
            }
        }

        $val = str_replace(" ", "%", $nama_pd);
        if ($nama_pd) {
            if ($pd_module == "pdterdaftar") {
                $sql .= " where nama ilike '%".$val."%'";
            } else {
                $sql .= " and nama ilike '%".$val."%'";
            }
        }

        if ($query) {
            if ($pd_module == "pdterdaftar") {
                $sql .= " where nama ilike '%".$query."%'";
            } else {
                $sql .= " and nama ilike '%".$query."%'";
            }
        }

        if ($pd_module == "pdterdaftar") {
            $sql .= " order by
                        nama_sorter";
        } else {
            $sql .= " order by
                        peserta_didik.nama";
        }

        $pd_all = getDataBySql($sql, FALSE);
        $rowCount = count($pd_all);

        $sql_pd = $sql.$limit_sql;
        // echo $sql_pd; die;
        $pesertaDidiks = getDataBySql($sql_pd, DBNAME);

        foreach ($pesertaDidiks as $pds) {
            // print_r($pds); die;
            $sessionSemester = $app['session']->get('semester_id');

            $pd = PesertaDidikPeer::retrieveByPK($pds[0]);

            $c = new \Criteria();
            $c->add(RegistrasiPesertaDidikPeer::SEKOLAH_ID, $sekolah_id);
            $c->add(RegistrasiPesertaDidikPeer::SOFT_DELETE, 0);

            $d = new \Criteria();
            $d->addJoin(AnggotaRombelPeer::ROMBONGAN_BELAJAR_ID, RombonganBelajarPeer::ROMBONGAN_BELAJAR_ID, \Criteria::LEFT_JOIN);
            $d->addAnd(AnggotaRombelPeer::PESERTA_DIDIK_ID, $pd->getPesertaDidikId());
            $d->addAnd(AnggotaRombelPeer::SOFT_DELETE, 0);
            $d->addAnd(RombonganBelajarPeer::SEMESTER_ID, $sessionSemester);
            $d->addAnd(RombonganBelajarPeer::JENIS_ROMBEL, 1);
            $d->addAnd(RombonganBelajarPeer::SOFT_DELETE, 0);
            $d->addDescendingOrderByColumn(RombonganBelajarPeer::TINGKAT_PENDIDIKAN_ID);

            $countar = AnggotaRombelPeer::doCount($d);

            $registrasiPds = $pd->getRegistrasiPesertaDidiks($c);
            $registrasiPdArr = (array) $registrasiPds;
            //$registrasiPdObj = $registrasiPdArr[0];

            $arr = $pd->toArray(\BasePeer::TYPE_FIELDNAME);

            if($countar < 1){
                // $arr['rombel_saat_ini'] = "(Belum terdaftar di Rombel)";
                $arr['rombel_saat_ini'] = " ";
            }else{
                $anggotaRombel = AnggotaRombelPeer::doSelectOne($d);

                $rombel_id = $anggotaRombel->getRombonganBelajarId();

                $rombels = RombonganBelajarPeer::retrieveByPK($rombel_id);

                $arr['tingkat_pendidikan_id'] = $rombels->getTingkatPendidikanId();
                $arr['rombel_saat_ini'] = "Kelas ".$rombels->getTingkatPendidikanId();
                $arr['rombel'] = $rombels->getNama();
            }

            if(!empty($registrasiPdArr[0])){
                $registrasiPdObj = $registrasiPdArr[0];

                $regArr = $registrasiPdObj->toArray(\BasePeer::TYPE_FIELDNAME);
                foreach ($regArr as $key => $value) {
                    // echo "data ". $arr[$key] ."=". $value;
                    if ($key == "no_skhun" && empty($value)) {
                        // $arr[$key] = $pd->getNoSkhun();
                    } else {
                        $arr[$key] = $value;
                    }
                    $arr["jenis_pendaftaran_id_str"] = $registrasiPdObj->getJenisPendaftaranId() ? $registrasiPdObj->getJenisPendaftaran()->getNama() : "";
                    $arr["jenis_keluar_id_str"] = $registrasiPdObj->getJenisKeluarId() ? $registrasiPdObj->getJenisKeluar()->getKetKeluar() : "";
                    $arr["nomor_induk_pd"] = $registrasiPdObj->getNipd() ? $registrasiPdObj->getNipd() : "";
                }
            }

            $arr['id_bank_str'] = $pd->getIdBank() ? $pd->getBank()->getNmBank() : "";
            $arr['kewarganegaraan_str'] = $pd->getKewarganegaraan() ? $pd->getNegara()->getNama() : "";

            array_push($outArr, $arr);
        }

        return json_encode($outArr);
        // return tableJson($outArr, $rowCount, PesertaDidikPeer::getFieldNames(\BasePeer::TYPE_FIELDNAME));
    }

    function getRombonganBelajar(Request $request, Application $app) {

        $requesta = Preface::Inisiasi($request);

        $sekolah_id = $requesta['sekolah_id'];

        $c = new \Criteria();
        $c->add(RombonganBelajarPeer::SEKOLAH_ID, $sekolah_id);
        $c->add(RombonganBelajarPeer::SOFT_DELETE, 0);

        $rombels = RombonganBelajarPeer::doSelect($c);

        return json_encode($rombels);
    }
}
?>