<?php

namespace Admin\Model;

use Admin\Model\om\BaseTingkatPrestasiQuery;


/**
 * Skeleton subclass for performing query and update operations on the 'ref.tingkat_prestasi' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.Admin.Model
 */
class TingkatPrestasiQuery extends BaseTingkatPrestasiQuery
{
}
