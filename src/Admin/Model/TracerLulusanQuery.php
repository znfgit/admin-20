<?php

namespace Admin\Model;

use Admin\Model\om\BaseTracerLulusanQuery;


/**
 * Skeleton subclass for performing query and update operations on the 'tracer_lulusan' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.Admin.Model
 */
class TracerLulusanQuery extends BaseTracerLulusanQuery
{
}
