<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'tunjangan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class TunjanganTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.TunjanganTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tunjangan');
        $this->setPhpName('Tunjangan');
        $this->setClassname('Admin\\Model\\Tunjangan');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('tunjangan_id', 'TunjanganId', 'CHAR', true, 16, null);
        $this->addForeignKey('semester_id', 'SemesterId', 'CHAR', 'ref.semester', 'semester_id', false, 5, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('jenis_tunjangan_id', 'JenisTunjanganId', 'INTEGER', 'ref.jenis_tunjangan', 'jenis_tunjangan_id', false, 4, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('instansi', 'Instansi', 'VARCHAR', false, 100, null);
        $this->addColumn('sk_tunjangan', 'SkTunjangan', 'VARCHAR', false, 80, null);
        $this->addColumn('tgl_sk_tunjangan', 'TglSkTunjangan', 'VARCHAR', false, 20, null);
        $this->addColumn('sumber_dana', 'SumberDana', 'VARCHAR', false, 30, null);
        $this->addColumn('dari_tahun', 'DariTahun', 'NUMERIC', true, 6, null);
        $this->addColumn('sampai_tahun', 'SampaiTahun', 'NUMERIC', false, 6, null);
        $this->addColumn('nominal', 'Nominal', 'NUMERIC', true, 18, null);
        $this->addColumn('status', 'Status', 'INTEGER', false, 4, null);
        $this->addColumn('asal_data', 'AsalData', 'CHAR', true, 1, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Ptk', 'Admin\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('JenisTunjangan', 'Admin\\Model\\JenisTunjangan', RelationMap::MANY_TO_ONE, array('jenis_tunjangan_id' => 'jenis_tunjangan_id', ), null, null);
        $this->addRelation('Semester', 'Admin\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('VldTunjangan', 'Admin\\Model\\VldTunjangan', RelationMap::ONE_TO_MANY, array('tunjangan_id' => 'tunjangan_id', ), null, null, 'VldTunjangans');
    } // buildRelations()

} // TunjanganTableMap
