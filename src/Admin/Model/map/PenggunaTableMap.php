<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'pengguna' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class PenggunaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.PenggunaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('pengguna');
        $this->setPhpName('Pengguna');
        $this->setClassname('Admin\\Model\\Pengguna');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('pengguna_id', 'PenggunaId', 'CHAR', true, 16, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 60, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 50, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 100, null);
        $this->addColumn('nip_nim', 'NipNim', 'VARCHAR', false, 18, null);
        $this->addColumn('jabatan_lembaga', 'JabatanLembaga', 'VARCHAR', false, 25, null);
        $this->addColumn('ym', 'Ym', 'VARCHAR', false, 20, null);
        $this->addColumn('skype', 'Skype', 'VARCHAR', false, 20, null);
        $this->addColumn('alamat', 'Alamat', 'VARCHAR', false, 80, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addColumn('no_telepon', 'NoTelepon', 'VARCHAR', false, 20, null);
        $this->addColumn('no_hp', 'NoHp', 'VARCHAR', false, 20, null);
        $this->addColumn('aktif', 'Aktif', 'NUMERIC', true, 3, null);
        $this->addColumn('ptk_id', 'PtkId', 'CHAR', false, 16, null);
        $this->addForeignKey('peran_id', 'PeranId', 'INTEGER', 'ref.peran', 'peran_id', true, 4, null);
        $this->addColumn('sekolah_id', 'SekolahId', 'CHAR', false, 16, null);
        $this->addForeignKey('lembaga_id', 'LembagaId', 'CHAR', 'lembaga_non_sekolah', 'lembaga_id', false, 16, null);
        $this->addForeignKey('yayasan_id', 'YayasanId', 'CHAR', 'yayasan', 'yayasan_id', false, 16, null);
        $this->addForeignKey('la_id', 'LaId', 'CHAR', 'ref.lembaga_akreditasi', 'la_id', false, 5, null);
        $this->addForeignKey('dudi_id', 'DudiId', 'CHAR', 'dudi', 'dudi_id', false, 16, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Dudi', 'Admin\\Model\\Dudi', RelationMap::MANY_TO_ONE, array('dudi_id' => 'dudi_id', ), null, null);
        $this->addRelation('LembagaNonSekolah', 'Admin\\Model\\LembagaNonSekolah', RelationMap::MANY_TO_ONE, array('lembaga_id' => 'lembaga_id', ), null, null);
        $this->addRelation('Yayasan', 'Admin\\Model\\Yayasan', RelationMap::MANY_TO_ONE, array('yayasan_id' => 'yayasan_id', ), null, null);
        $this->addRelation('LembagaAkreditasi', 'Admin\\Model\\LembagaAkreditasi', RelationMap::MANY_TO_ONE, array('la_id' => 'la_id', ), null, null);
        $this->addRelation('MstWilayah', 'Admin\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('Peran', 'Admin\\Model\\Peran', RelationMap::MANY_TO_ONE, array('peran_id' => 'peran_id', ), null, null);
        $this->addRelation('LogPengguna', 'Admin\\Model\\LogPengguna', RelationMap::ONE_TO_MANY, array('pengguna_id' => 'pengguna_id', ), null, null, 'LogPenggunas');
        $this->addRelation('SasaranSurvey', 'Admin\\Model\\SasaranSurvey', RelationMap::ONE_TO_MANY, array('pengguna_id' => 'pengguna_id', ), null, null, 'SasaranSurveys');
    } // buildRelations()

} // PenggunaTableMap
