<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.pekerjaan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class PekerjaanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.PekerjaanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.pekerjaan');
        $this->setPhpName('Pekerjaan');
        $this->setClassname('Admin\\Model\\Pekerjaan');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('pekerjaan_id', 'PekerjaanId', 'INTEGER', true, 4, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', false, 25, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('TracerLulusan', 'Admin\\Model\\TracerLulusan', RelationMap::ONE_TO_MANY, array('pekerjaan_id' => 'pekerjaan_id', ), null, null, 'TracerLulusans');
        $this->addRelation('PesertaDidikRelatedByPekerjaanIdAyah', 'Admin\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('pekerjaan_id' => 'pekerjaan_id_ayah', ), null, null, 'PesertaDidiksRelatedByPekerjaanIdAyah');
        $this->addRelation('PesertaDidikRelatedByPekerjaanIdIbu', 'Admin\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('pekerjaan_id' => 'pekerjaan_id_ibu', ), null, null, 'PesertaDidiksRelatedByPekerjaanIdIbu');
        $this->addRelation('PesertaDidikRelatedByPekerjaanIdWali', 'Admin\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('pekerjaan_id' => 'pekerjaan_id_wali', ), null, null, 'PesertaDidiksRelatedByPekerjaanIdWali');
        $this->addRelation('Ptk', 'Admin\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('pekerjaan_id' => 'pekerjaan_suami_istri', ), null, null, 'Ptks');
    } // buildRelations()

} // PekerjaanTableMap
