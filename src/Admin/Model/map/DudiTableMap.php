<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'dudi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class DudiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.DudiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('dudi');
        $this->setPhpName('Dudi');
        $this->setClassname('Admin\\Model\\Dudi');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('dudi_id', 'DudiId', 'CHAR', true, 16, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 100, null);
        $this->addForeignKey('bidang_usaha_id', 'BidangUsahaId', 'CHAR', 'ref.bidang_usaha', 'bidang_usaha_id', true, 10, null);
        $this->addColumn('alamat_jalan', 'AlamatJalan', 'VARCHAR', true, 80, null);
        $this->addColumn('rt', 'Rt', 'NUMERIC', false, 4, null);
        $this->addColumn('rw', 'Rw', 'NUMERIC', false, 4, null);
        $this->addColumn('nama_dusun', 'NamaDusun', 'VARCHAR', false, 60, null);
        $this->addColumn('desa_kelurahan', 'DesaKelurahan', 'VARCHAR', true, 60, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addColumn('kode_pos', 'KodePos', 'CHAR', false, 5, null);
        $this->addColumn('lintang', 'Lintang', 'NUMERIC', false, 13, null);
        $this->addColumn('bujur', 'Bujur', 'NUMERIC', false, 13, null);
        $this->addColumn('nomor_telepon', 'NomorTelepon', 'VARCHAR', false, 20, null);
        $this->addColumn('nomor_fax', 'NomorFax', 'VARCHAR', false, 20, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 60, null);
        $this->addColumn('website', 'Website', 'VARCHAR', false, 100, null);
        $this->addColumn('npwp', 'Npwp', 'CHAR', false, 15, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BidangUsaha', 'Admin\\Model\\BidangUsaha', RelationMap::MANY_TO_ONE, array('bidang_usaha_id' => 'bidang_usaha_id', ), null, null);
        $this->addRelation('MstWilayah', 'Admin\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('TracerLulusan', 'Admin\\Model\\TracerLulusan', RelationMap::ONE_TO_MANY, array('dudi_id' => 'dudi_id', ), null, null, 'TracerLulusans');
        $this->addRelation('VldDudi', 'Admin\\Model\\VldDudi', RelationMap::ONE_TO_MANY, array('dudi_id' => 'dudi_id', ), null, null, 'VldDudis');
        $this->addRelation('Mou', 'Admin\\Model\\Mou', RelationMap::ONE_TO_MANY, array('dudi_id' => 'dudi_id', ), null, null, 'Mous');
        $this->addRelation('Pengguna', 'Admin\\Model\\Pengguna', RelationMap::ONE_TO_MANY, array('dudi_id' => 'dudi_id', ), null, null, 'Penggunas');
    } // buildRelations()

} // DudiTableMap
