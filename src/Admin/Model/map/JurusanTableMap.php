<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jurusan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class JurusanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.JurusanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jurusan');
        $this->setPhpName('Jurusan');
        $this->setClassname('Admin\\Model\\Jurusan');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jurusan_id', 'JurusanId', 'VARCHAR', true, 25, null);
        $this->addColumn('nama_jurusan', 'NamaJurusan', 'VARCHAR', true, 100, null);
        $this->addColumn('untuk_sma', 'UntukSma', 'NUMERIC', true, 3, null);
        $this->addColumn('untuk_smk', 'UntukSmk', 'NUMERIC', true, 3, null);
        $this->addColumn('untuk_pt', 'UntukPt', 'NUMERIC', true, 3, null);
        $this->addColumn('untuk_slb', 'UntukSlb', 'NUMERIC', true, 3, null);
        $this->addColumn('untuk_smklb', 'UntukSmklb', 'NUMERIC', true, 3, null);
        $this->addForeignKey('jenjang_pendidikan_id', 'JenjangPendidikanId', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', false, 4, null);
        $this->addForeignKey('jurusan_induk', 'JurusanInduk', 'VARCHAR', 'ref.jurusan', 'jurusan_id', false, 25, null);
        $this->addForeignKey('level_bidang_id', 'LevelBidangId', 'VARCHAR', 'ref.kelompok_bidang', 'level_bidang_id', true, 5, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JenjangPendidikan', 'Admin\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('JurusanRelatedByJurusanInduk', 'Admin\\Model\\Jurusan', RelationMap::MANY_TO_ONE, array('jurusan_induk' => 'jurusan_id', ), null, null);
        $this->addRelation('KelompokBidang', 'Admin\\Model\\KelompokBidang', RelationMap::MANY_TO_ONE, array('level_bidang_id' => 'level_bidang_id', ), null, null);
        $this->addRelation('TemplateUn', 'Admin\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('jurusan_id' => 'jurusan_id', ), null, null, 'TemplateUns');
        $this->addRelation('JurusanSp', 'Admin\\Model\\JurusanSp', RelationMap::ONE_TO_MANY, array('jurusan_id' => 'jurusan_id', ), null, null, 'JurusanSps');
        $this->addRelation('JurusanRelatedByJurusanId', 'Admin\\Model\\Jurusan', RelationMap::ONE_TO_MANY, array('jurusan_id' => 'jurusan_induk', ), null, null, 'JurusansRelatedByJurusanId');
        $this->addRelation('Kurikulum', 'Admin\\Model\\Kurikulum', RelationMap::ONE_TO_MANY, array('jurusan_id' => 'jurusan_id', ), null, null, 'Kurikulums');
        $this->addRelation('MataPelajaran', 'Admin\\Model\\MataPelajaran', RelationMap::ONE_TO_MANY, array('jurusan_id' => 'jurusan_id', ), null, null, 'MataPelajarans');
        $this->addRelation('PemakaiPrasarana', 'Admin\\Model\\PemakaiPrasarana', RelationMap::ONE_TO_MANY, array('jurusan_id' => 'jurusan_id', ), null, null, 'PemakaiPrasaranas');
        $this->addRelation('PemakaiSarana', 'Admin\\Model\\PemakaiSarana', RelationMap::ONE_TO_MANY, array('jurusan_id' => 'jurusan_id', ), null, null, 'PemakaiSaranas');
    } // buildRelations()

} // JurusanTableMap
