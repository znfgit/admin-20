<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'sekolah' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class SekolahTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.SekolahTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sekolah');
        $this->setPhpName('Sekolah');
        $this->setClassname('Admin\\Model\\Sekolah');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('sekolah_id', 'SekolahId', 'CHAR', true, 16, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 100, null);
        $this->addColumn('nama_nomenklatur', 'NamaNomenklatur', 'VARCHAR', false, 100, null);
        $this->addColumn('nss', 'Nss', 'CHAR', false, 12, null);
        $this->addColumn('npsn', 'Npsn', 'CHAR', false, 8, null);
        $this->addForeignKey('bentuk_pendidikan_id', 'BentukPendidikanId', 'SMALLINT', 'ref.bentuk_pendidikan', 'bentuk_pendidikan_id', true, 2, null);
        $this->addColumn('alamat_jalan', 'AlamatJalan', 'VARCHAR', true, 80, null);
        $this->addColumn('rt', 'Rt', 'NUMERIC', false, 4, null);
        $this->addColumn('rw', 'Rw', 'NUMERIC', false, 4, null);
        $this->addColumn('nama_dusun', 'NamaDusun', 'VARCHAR', false, 60, null);
        $this->addColumn('desa_kelurahan', 'DesaKelurahan', 'VARCHAR', true, 60, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addColumn('kode_pos', 'KodePos', 'CHAR', false, 5, null);
        $this->addColumn('lintang', 'Lintang', 'NUMERIC', false, 13, null);
        $this->addColumn('bujur', 'Bujur', 'NUMERIC', false, 13, null);
        $this->addColumn('nomor_telepon', 'NomorTelepon', 'VARCHAR', false, 20, null);
        $this->addColumn('nomor_fax', 'NomorFax', 'VARCHAR', false, 20, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 60, null);
        $this->addColumn('website', 'Website', 'VARCHAR', false, 100, null);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addColumn('status_sekolah', 'StatusSekolah', 'NUMERIC', true, 3, null);
        $this->addColumn('sk_pendirian_sekolah', 'SkPendirianSekolah', 'VARCHAR', false, 80, null);
        $this->addColumn('tanggal_sk_pendirian', 'TanggalSkPendirian', 'VARCHAR', false, 20, null);
        $this->addForeignKey('status_kepemilikan_id', 'StatusKepemilikanId', 'NUMERIC', 'ref.status_kepemilikan', 'status_kepemilikan_id', true, 3, null);
        $this->addColumn('yayasan_id', 'YayasanId', 'CHAR', false, 16, null);
        $this->addColumn('sk_izin_operasional', 'SkIzinOperasional', 'VARCHAR', false, 80, null);
        $this->addColumn('tanggal_sk_izin_operasional', 'TanggalSkIzinOperasional', 'VARCHAR', false, 20, null);
        $this->addColumn('no_rekening', 'NoRekening', 'VARCHAR', false, 20, null);
        $this->addColumn('nama_bank', 'NamaBank', 'VARCHAR', false, 20, null);
        $this->addColumn('cabang_kcp_unit', 'CabangKcpUnit', 'VARCHAR', false, 60, null);
        $this->addColumn('rekening_atas_nama', 'RekeningAtasNama', 'VARCHAR', false, 50, null);
        $this->addColumn('mbs', 'Mbs', 'NUMERIC', true, 3, null);
        $this->addColumn('luas_tanah_milik', 'LuasTanahMilik', 'NUMERIC', true, 9, null);
        $this->addColumn('luas_tanah_bukan_milik', 'LuasTanahBukanMilik', 'NUMERIC', true, 9, null);
        $this->addColumn('kode_registrasi', 'KodeRegistrasi', 'BIGINT', false, 8, null);
        $this->addColumn('npwp', 'Npwp', 'CHAR', false, 15, null);
        $this->addColumn('nm_wp', 'NmWp', 'VARCHAR', false, 100, null);
        $this->addColumn('flag', 'Flag', 'CHAR', false, 3, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BentukPendidikan', 'Admin\\Model\\BentukPendidikan', RelationMap::MANY_TO_ONE, array('bentuk_pendidikan_id' => 'bentuk_pendidikan_id', ), null, null);
        $this->addRelation('KebutuhanKhusus', 'Admin\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('MstWilayah', 'Admin\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('StatusKepemilikan', 'Admin\\Model\\StatusKepemilikan', RelationMap::MANY_TO_ONE, array('status_kepemilikan_id' => 'status_kepemilikan_id', ), null, null);
        $this->addRelation('AkreditasiSp', 'Admin\\Model\\AkreditasiSp', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'AkreditasiSps');
        $this->addRelation('TugasTambahan', 'Admin\\Model\\TugasTambahan', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'TugasTambahans');
        $this->addRelation('UnitUsaha', 'Admin\\Model\\UnitUsaha', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'UnitUsahas');
        $this->addRelation('AnggotaGugus', 'Admin\\Model\\AnggotaGugus', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'AnggotaGuguses');
        $this->addRelation('Blockgrant', 'Admin\\Model\\Blockgrant', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'Blockgrants');
        $this->addRelation('BukuAlat', 'Admin\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'BukuAlats');
        $this->addRelation('GugusSekolah', 'Admin\\Model\\GugusSekolah', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_inti_id', ), null, null, 'GugusSekolahs');
        $this->addRelation('Instalasi', 'Admin\\Model\\Instalasi', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'Instalasis');
        $this->addRelation('JurusanSp', 'Admin\\Model\\JurusanSp', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'JurusanSps');
        $this->addRelation('Kepanitiaan', 'Admin\\Model\\Kepanitiaan', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'Kepanitiaans');
        $this->addRelation('LayananKhusus', 'Admin\\Model\\LayananKhusus', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'LayananKhususes');
        $this->addRelation('Mou', 'Admin\\Model\\Mou', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'Mous');
        $this->addRelation('VldSekolah', 'Admin\\Model\\VldSekolah', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'VldSekolahs');
        $this->addRelation('PesertaDidikBaru', 'Admin\\Model\\PesertaDidikBaru', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PesertaDidikBarus');
        $this->addRelation('Prasarana', 'Admin\\Model\\Prasarana', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'Prasaranas');
        $this->addRelation('ProgramInklusi', 'Admin\\Model\\ProgramInklusi', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'ProgramInklusis');
        $this->addRelation('PtkBaru', 'Admin\\Model\\PtkBaru', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PtkBarus');
        $this->addRelation('PtkTerdaftar', 'Admin\\Model\\PtkTerdaftar', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PtkTerdaftars');
        $this->addRelation('RegistrasiPesertaDidik', 'Admin\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'RegistrasiPesertaDidiks');
        $this->addRelation('RombonganBelajar', 'Admin\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'RombonganBelajars');
        $this->addRelation('Sanitasi', 'Admin\\Model\\Sanitasi', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'Sanitasis');
        $this->addRelation('Sarana', 'Admin\\Model\\Sarana', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'Saranas');
        $this->addRelation('SasaranPengawasan', 'Admin\\Model\\SasaranPengawasan', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SasaranPengawasans');
        $this->addRelation('SasaranSurvey', 'Admin\\Model\\SasaranSurvey', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SasaranSurveys');
        $this->addRelation('SekolahLongitudinal', 'Admin\\Model\\SekolahLongitudinal', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SekolahLongitudinals');
    } // buildRelations()

} // SekolahTableMap
