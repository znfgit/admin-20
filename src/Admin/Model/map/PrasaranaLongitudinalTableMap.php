<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'prasarana_longitudinal' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class PrasaranaLongitudinalTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.PrasaranaLongitudinalTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('prasarana_longitudinal');
        $this->setPhpName('PrasaranaLongitudinal');
        $this->setClassname('Admin\\Model\\PrasaranaLongitudinal');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('prasarana_id', 'PrasaranaId', 'CHAR' , 'prasarana', 'prasarana_id', true, 16, null);
        $this->addForeignPrimaryKey('semester_id', 'SemesterId', 'CHAR' , 'ref.semester', 'semester_id', true, 5, null);
        $this->addColumn('rusak_pondasi', 'RusakPondasi', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_pondasi', 'KetPondasi', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_sloop_kolom_balok', 'RusakSloopKolomBalok', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_sloop_kolom_balok', 'KetSloopKolomBalok', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_plester_struktur', 'RusakPlesterStruktur', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_plester_struktur', 'KetPlesterStruktur', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_kudakuda_atap', 'RusakKudakudaAtap', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_kudakuda_atap', 'KetKudakudaAtap', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_kaso_atap', 'RusakKasoAtap', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_kaso_atap', 'KetKasoAtap', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_reng_atap', 'RusakRengAtap', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_reng_atap', 'KetRengAtap', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_tutup_atap', 'RusakTutupAtap', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_tutup_atap', 'KetTutupAtap', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_lisplang_talang', 'RusakLisplangTalang', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_lisplang_talang', 'KetLisplangTalang', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_rangka_plafon', 'RusakRangkaPlafon', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_rangka_plafon', 'KetRangkaPlafon', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_tutup_plafon', 'RusakTutupPlafon', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_tutup_plafon', 'KetTutupPlafon', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_bata_dinding', 'RusakBataDinding', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_bata_dinding', 'KetBataDinding', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_plester_dinding', 'RusakPlesterDinding', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_plester_dinding', 'KetPlesterDinding', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_daun_jendela', 'RusakDaunJendela', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_daun_jendela', 'KetDaunJendela', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_daun_pintu', 'RusakDaunPintu', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_daun_pintu', 'KetDaunPintu', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_kusen', 'RusakKusen', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_kusen', 'KetKusen', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_tutup_lantai', 'RusakTutupLantai', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_penutup_lantai', 'KetPenutupLantai', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_inst_listrik', 'RusakInstListrik', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_inst_listrik', 'KetInstListrik', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_inst_air', 'RusakInstAir', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_inst_air', 'KetInstAir', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_drainase', 'RusakDrainase', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_drainase', 'KetDrainase', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_finish_struktur', 'RusakFinishStruktur', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_finish_struktur', 'KetFinishStruktur', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_finish_plafon', 'RusakFinishPlafon', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_finish_plafon', 'KetFinishPlafon', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_finish_dinding', 'RusakFinishDinding', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_finish_dinding', 'KetFinishDinding', 'VARCHAR', false, 120, null);
        $this->addColumn('rusak_finish_kpj', 'RusakFinishKpj', 'NUMERIC', true, 8, null);
        $this->addColumn('ket_finish_kpj', 'KetFinishKpj', 'VARCHAR', false, 120, null);
        $this->addColumn('berfungsi', 'Berfungsi', 'NUMERIC', true, 3, null);
        $this->addColumn('blob_id', 'BlobId', 'CHAR', false, 16, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Prasarana', 'Admin\\Model\\Prasarana', RelationMap::MANY_TO_ONE, array('prasarana_id' => 'prasarana_id', ), null, null);
        $this->addRelation('Semester', 'Admin\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
    } // buildRelations()

} // PrasaranaLongitudinalTableMap
