<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'tracer_lulusan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class TracerLulusanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.TracerLulusanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tracer_lulusan');
        $this->setPhpName('TracerLulusan');
        $this->setClassname('Admin\\Model\\TracerLulusan');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_tracer', 'IdTracer', 'CHAR', true, 16, null);
        $this->addForeignKey('registrasi_id', 'RegistrasiId', 'CHAR', 'registrasi_peserta_didik', 'registrasi_id', true, 16, null);
        $this->addColumn('tgl_entry', 'TglEntry', 'VARCHAR', true, 20, null);
        $this->addColumn('akt_setelah_lulus', 'AktSetelahLulus', 'CHAR', true, 1, null);
        $this->addColumn('nm_inst_perusahaan', 'NmInstPerusahaan', 'VARCHAR', false, 100, null);
        $this->addColumn('nm_sp', 'NmSp', 'VARCHAR', false, 100, null);
        $this->addColumn('nm_prodi', 'NmProdi', 'VARCHAR', false, 100, null);
        $this->addColumn('ket_tracer', 'KetTracer', 'VARCHAR', false, 250, null);
        $this->addForeignKey('pekerjaan_id', 'PekerjaanId', 'INTEGER', 'ref.pekerjaan', 'pekerjaan_id', false, 4, null);
        $this->addForeignKey('bidang_usaha_id', 'BidangUsahaId', 'CHAR', 'ref.bidang_usaha', 'bidang_usaha_id', false, 10, null);
        $this->addForeignKey('dudi_id', 'DudiId', 'CHAR', 'dudi', 'dudi_id', false, 16, null);
        $this->addColumn('id_prodi', 'IdProdi', 'CHAR', false, 16, null);
        $this->addColumn('stat_tracer', 'StatTracer', 'CHAR', true, 1, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Dudi', 'Admin\\Model\\Dudi', RelationMap::MANY_TO_ONE, array('dudi_id' => 'dudi_id', ), null, null);
        $this->addRelation('RegistrasiPesertaDidik', 'Admin\\Model\\RegistrasiPesertaDidik', RelationMap::MANY_TO_ONE, array('registrasi_id' => 'registrasi_id', ), null, null);
        $this->addRelation('BidangUsaha', 'Admin\\Model\\BidangUsaha', RelationMap::MANY_TO_ONE, array('bidang_usaha_id' => 'bidang_usaha_id', ), null, null);
        $this->addRelation('Pekerjaan', 'Admin\\Model\\Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_id' => 'pekerjaan_id', ), null, null);
    } // buildRelations()

} // TracerLulusanTableMap
