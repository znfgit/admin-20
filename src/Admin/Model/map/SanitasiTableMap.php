<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'sanitasi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class SanitasiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.SanitasiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sanitasi');
        $this->setPhpName('Sanitasi');
        $this->setClassname('Admin\\Model\\Sanitasi');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('sekolah_id', 'SekolahId', 'CHAR' , 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignPrimaryKey('semester_id', 'SemesterId', 'CHAR' , 'ref.semester', 'semester_id', true, 5, null);
        $this->addForeignKey('sumber_air_id', 'SumberAirId', 'NUMERIC', 'ref.sumber_air', 'sumber_air_id', true, 4, null);
        $this->addColumn('ketersediaan_air', 'KetersediaanAir', 'NUMERIC', true, 3, null);
        $this->addColumn('kecukupan_air', 'KecukupanAir', 'NUMERIC', true, 3, null);
        $this->addColumn('minum_siswa', 'MinumSiswa', 'NUMERIC', true, 3, null);
        $this->addColumn('memproses_air', 'MemprosesAir', 'NUMERIC', true, 3, null);
        $this->addColumn('siswa_bawa_air', 'SiswaBawaAir', 'NUMERIC', true, 3, null);
        $this->addColumn('toilet_siswa_laki', 'ToiletSiswaLaki', 'NUMERIC', true, 4, null);
        $this->addColumn('toilet_siswa_perempuan', 'ToiletSiswaPerempuan', 'NUMERIC', true, 4, null);
        $this->addColumn('toilet_siswa_kk', 'ToiletSiswaKk', 'NUMERIC', true, 4, null);
        $this->addColumn('toilet_siswa_kecil', 'ToiletSiswaKecil', 'NUMERIC', true, 3, null);
        $this->addColumn('tipe_jamban', 'TipeJamban', 'CHAR', true, 1, null);
        $this->addColumn('jml_jamban_l_g', 'JmlJambanLG', 'NUMERIC', true, 4, null);
        $this->addColumn('jml_jamban_l_tg', 'JmlJambanLTg', 'NUMERIC', true, 4, null);
        $this->addColumn('jml_jamban_p_g', 'JmlJambanPG', 'NUMERIC', true, 4, null);
        $this->addColumn('jml_jamban_p_tg', 'JmlJambanPTg', 'NUMERIC', true, 4, null);
        $this->addColumn('jml_jamban_lp_g', 'JmlJambanLpG', 'NUMERIC', true, 4, null);
        $this->addColumn('jml_jamban_lp_tg', 'JmlJambanLpTg', 'NUMERIC', true, 4, null);
        $this->addColumn('tempat_cuci_tangan', 'TempatCuciTangan', 'NUMERIC', true, 4, null);
        $this->addColumn('a_sabun_air_mengalir', 'ASabunAirMengalir', 'NUMERIC', true, 3, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Sekolah', 'Admin\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('Semester', 'Admin\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('SumberAir', 'Admin\\Model\\SumberAir', RelationMap::MANY_TO_ONE, array('sumber_air_id' => 'sumber_air_id', ), null, null);
    } // buildRelations()

} // SanitasiTableMap
