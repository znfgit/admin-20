<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'instalasi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class InstalasiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.InstalasiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('instalasi');
        $this->setPhpName('Instalasi');
        $this->setClassname('Admin\\Model\\Instalasi');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_instalasi', 'IdInstalasi', 'CHAR', true, 16, null);
        $this->addColumn('jns_instalasi', 'JnsInstalasi', 'CHAR', true, 1, null);
        $this->addColumn('a_link_aktif', 'ALinkAktif', 'NUMERIC', true, 3, null);
        $this->addColumn('ket_link', 'KetLink', 'VARCHAR', false, 100, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', false, 16, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', false, 8, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Sekolah', 'Admin\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('MstWilayah', 'Admin\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('SyncLog', 'Admin\\Model\\SyncLog', RelationMap::ONE_TO_MANY, array('id_instalasi' => 'id_instalasi', ), null, null, 'SyncLogs');
        $this->addRelation('SyncSession', 'Admin\\Model\\SyncSession', RelationMap::ONE_TO_MANY, array('id_instalasi' => 'id_instalasi', ), null, null, 'SyncSessions');
    } // buildRelations()

} // InstalasiTableMap
