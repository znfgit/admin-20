<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'anggota_panitia' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class AnggotaPanitiaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.AnggotaPanitiaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('anggota_panitia');
        $this->setPhpName('AnggotaPanitia');
        $this->setClassname('Admin\\Model\\AnggotaPanitia');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_ang_panitia', 'IdAngPanitia', 'CHAR', true, 16, null);
        $this->addForeignKey('id_panitia', 'IdPanitia', 'CHAR', 'kepanitiaan', 'id_panitia', true, 16, null);
        $this->addColumn('nm_ang', 'NmAng', 'VARCHAR', false, 100, null);
        $this->addColumn('no_kontak', 'NoKontak', 'VARCHAR', false, 20, null);
        $this->addColumn('unsur_ang', 'UnsurAng', 'CHAR', true, 1, null);
        $this->addColumn('peran_ang', 'PeranAng', 'VARCHAR', true, 30, null);
        $this->addForeignKey('peserta_didik_id', 'PesertaDidikId', 'CHAR', 'peserta_didik', 'peserta_didik_id', false, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', false, 16, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Kepanitiaan', 'Admin\\Model\\Kepanitiaan', RelationMap::MANY_TO_ONE, array('id_panitia' => 'id_panitia', ), null, null);
        $this->addRelation('PesertaDidik', 'Admin\\Model\\PesertaDidik', RelationMap::MANY_TO_ONE, array('peserta_didik_id' => 'peserta_didik_id', ), null, null);
        $this->addRelation('Ptk', 'Admin\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
    } // buildRelations()

} // AnggotaPanitiaTableMap
