<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.mata_pelajaran' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class MataPelajaranTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.MataPelajaranTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.mata_pelajaran');
        $this->setPhpName('MataPelajaran');
        $this->setClassname('Admin\\Model\\MataPelajaran');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('mata_pelajaran_id', 'MataPelajaranId', 'INTEGER', true, 4, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 80, null);
        $this->addColumn('pilihan_sekolah', 'PilihanSekolah', 'NUMERIC', true, 3, null);
        $this->addColumn('pilihan_buku', 'PilihanBuku', 'NUMERIC', true, 3, null);
        $this->addColumn('pilihan_kepengawasan', 'PilihanKepengawasan', 'NUMERIC', true, 3, null);
        $this->addColumn('pilihan_evaluasi', 'PilihanEvaluasi', 'NUMERIC', true, 3, null);
        $this->addForeignKey('jurusan_id', 'JurusanId', 'VARCHAR', 'ref.jurusan', 'jurusan_id', false, 25, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Jurusan', 'Admin\\Model\\Jurusan', RelationMap::MANY_TO_ONE, array('jurusan_id' => 'jurusan_id', ), null, null);
        $this->addRelation('TemplateRapor', 'Admin\\Model\\TemplateRapor', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'TemplateRapors');
        $this->addRelation('TemplateUnRelatedByMp1Id', 'Admin\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp1_id', ), null, null, 'TemplateUnsRelatedByMp1Id');
        $this->addRelation('TemplateUnRelatedByMp2Id', 'Admin\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp2_id', ), null, null, 'TemplateUnsRelatedByMp2Id');
        $this->addRelation('TemplateUnRelatedByMp3Id', 'Admin\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp3_id', ), null, null, 'TemplateUnsRelatedByMp3Id');
        $this->addRelation('TemplateUnRelatedByMp4Id', 'Admin\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp4_id', ), null, null, 'TemplateUnsRelatedByMp4Id');
        $this->addRelation('TemplateUnRelatedByMp5Id', 'Admin\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp5_id', ), null, null, 'TemplateUnsRelatedByMp5Id');
        $this->addRelation('TemplateUnRelatedByMp6Id', 'Admin\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp6_id', ), null, null, 'TemplateUnsRelatedByMp6Id');
        $this->addRelation('TemplateUnRelatedByMp7Id', 'Admin\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mp7_id', ), null, null, 'TemplateUnsRelatedByMp7Id');
        $this->addRelation('BukuAlat', 'Admin\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'BukuAlats');
        $this->addRelation('Pembelajaran', 'Admin\\Model\\Pembelajaran', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'Pembelajarans');
        $this->addRelation('PengawasTerdaftar', 'Admin\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'PengawasTerdaftars');
        $this->addRelation('Kompetensi', 'Admin\\Model\\Kompetensi', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'Kompetensis');
        $this->addRelation('MapBidangMataPelajaran', 'Admin\\Model\\MapBidangMataPelajaran', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'MapBidangMataPelajarans');
        $this->addRelation('MataPelajaranKurikulum', 'Admin\\Model\\MataPelajaranKurikulum', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'MataPelajaranKurikulums');
        $this->addRelation('Mulok', 'Admin\\Model\\Mulok', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null, 'Muloks');
    } // buildRelations()

} // MataPelajaranTableMap
