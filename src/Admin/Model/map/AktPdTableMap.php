<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'akt_pd' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class AktPdTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.AktPdTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('akt_pd');
        $this->setPhpName('AktPd');
        $this->setClassname('Admin\\Model\\AktPd');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_akt_pd', 'IdAktPd', 'CHAR', true, 16, null);
        $this->addForeignKey('mou_id', 'MouId', 'CHAR', 'mou', 'mou_id', true, 16, null);
        $this->addForeignKey('id_jns_akt_pd', 'IdJnsAktPd', 'NUMERIC', 'ref.jenis_akt_pd', 'id_jns_akt_pd', true, 4, null);
        $this->addColumn('judul_akt_pd', 'JudulAktPd', 'VARCHAR', true, 500, null);
        $this->addColumn('sk_tugas', 'SkTugas', 'VARCHAR', false, 80, null);
        $this->addColumn('tgl_sk_tugas', 'TglSkTugas', 'VARCHAR', false, 20, null);
        $this->addColumn('ket_akt', 'KetAkt', 'LONGVARCHAR', false, 2147483647, null);
        $this->addColumn('a_komunal', 'AKomunal', 'NUMERIC', true, 3, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Mou', 'Admin\\Model\\Mou', RelationMap::MANY_TO_ONE, array('mou_id' => 'mou_id', ), null, null);
        $this->addRelation('JenisAktPd', 'Admin\\Model\\JenisAktPd', RelationMap::MANY_TO_ONE, array('id_jns_akt_pd' => 'id_jns_akt_pd', ), null, null);
        $this->addRelation('AnggotaAktPd', 'Admin\\Model\\AnggotaAktPd', RelationMap::ONE_TO_MANY, array('id_akt_pd' => 'id_akt_pd', ), null, null, 'AnggotaAktPds');
        $this->addRelation('VldAktPd', 'Admin\\Model\\VldAktPd', RelationMap::ONE_TO_MANY, array('id_akt_pd' => 'id_akt_pd', ), null, null, 'VldAktPds');
        $this->addRelation('BimbingPd', 'Admin\\Model\\BimbingPd', RelationMap::ONE_TO_MANY, array('id_akt_pd' => 'id_akt_pd', ), null, null, 'BimbingPds');
    } // buildRelations()

} // AktPdTableMap
