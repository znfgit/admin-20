<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'layanan_khusus' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class LayananKhususTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.LayananKhususTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('layanan_khusus');
        $this->setPhpName('LayananKhusus');
        $this->setClassname('Admin\\Model\\LayananKhusus');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_lk', 'IdLk', 'CHAR', true, 16, null);
        $this->addForeignKey('id_jenis_lk', 'IdJenisLk', 'CHAR', 'ref.jenis_lk', 'id_jenis_lk', true, 6, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addColumn('sk_lk', 'SkLk', 'VARCHAR', true, 80, null);
        $this->addColumn('tmt_lk', 'TmtLk', 'VARCHAR', true, 20, null);
        $this->addColumn('tst_lk', 'TstLk', 'VARCHAR', false, 20, null);
        $this->addColumn('ket_lk', 'KetLk', 'VARCHAR', false, 200, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Sekolah', 'Admin\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JenisLk', 'Admin\\Model\\JenisLk', RelationMap::MANY_TO_ONE, array('id_jenis_lk' => 'id_jenis_lk', ), null, null);
    } // buildRelations()

} // LayananKhususTableMap
