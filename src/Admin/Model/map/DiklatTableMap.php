<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'diklat' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class DiklatTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.DiklatTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('diklat');
        $this->setPhpName('Diklat');
        $this->setClassname('Admin\\Model\\Diklat');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('diklat_id', 'DiklatId', 'CHAR', true, 16, null);
        $this->addForeignKey('jenis_diklat_id', 'JenisDiklatId', 'INTEGER', 'ref.jenis_diklat', 'jenis_diklat_id', true, 4, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('penyelenggara', 'Penyelenggara', 'VARCHAR', false, 100, null);
        $this->addColumn('tahun', 'Tahun', 'NUMERIC', true, 6, null);
        $this->addColumn('peran', 'Peran', 'VARCHAR', false, 30, null);
        $this->addColumn('tingkat', 'Tingkat', 'NUMERIC', false, 4, null);
        $this->addColumn('berapa_jam', 'BerapaJam', 'NUMERIC', true, 6, null);
        $this->addColumn('sertifikat_diklat', 'SertifikatDiklat', 'VARCHAR', false, 80, null);
        $this->addColumn('asal_data', 'AsalData', 'CHAR', true, 1, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Ptk', 'Admin\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('JenisDiklat', 'Admin\\Model\\JenisDiklat', RelationMap::MANY_TO_ONE, array('jenis_diklat_id' => 'jenis_diklat_id', ), null, null);
    } // buildRelations()

} // DiklatTableMap
