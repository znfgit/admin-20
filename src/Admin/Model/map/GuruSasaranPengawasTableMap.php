<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'guru_sasaran_pengawas' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class GuruSasaranPengawasTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.GuruSasaranPengawasTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('guru_sasaran_pengawas');
        $this->setPhpName('GuruSasaranPengawas');
        $this->setClassname('Admin\\Model\\GuruSasaranPengawas');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('pengawas_terdaftar_id', 'PengawasTerdaftarId', 'CHAR' , 'pengawas_terdaftar', 'pengawas_terdaftar_id', true, 16, null);
        $this->addForeignPrimaryKey('ptk_terdaftar_id', 'PtkTerdaftarId', 'CHAR' , 'ptk_terdaftar', 'ptk_terdaftar_id', true, 16, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PengawasTerdaftar', 'Admin\\Model\\PengawasTerdaftar', RelationMap::MANY_TO_ONE, array('pengawas_terdaftar_id' => 'pengawas_terdaftar_id', ), null, null);
        $this->addRelation('PtkTerdaftar', 'Admin\\Model\\PtkTerdaftar', RelationMap::MANY_TO_ONE, array('ptk_terdaftar_id' => 'ptk_terdaftar_id', ), null, null);
    } // buildRelations()

} // GuruSasaranPengawasTableMap
