<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.kompetensi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class KompetensiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.KompetensiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.kompetensi');
        $this->setPhpName('Kompetensi');
        $this->setClassname('Admin\\Model\\Kompetensi');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_komp', 'IdKomp', 'CHAR', true, 16, null);
        $this->addColumn('desk', 'Desk', 'LONGVARCHAR', true, 2147483647, null);
        $this->addColumn('nmr', 'Nmr', 'VARCHAR', true, 5, null);
        $this->addColumn('kelompok', 'Kelompok', 'CHAR', true, 1, null);
        $this->addColumn('versi', 'Versi', 'INTEGER', true, 4, null);
        $this->addForeignKey('id_inti_dasar', 'IdIntiDasar', 'CHAR', 'ref.kompetensi', 'id_komp', false, 16, null);
        $this->addColumn('level_komp', 'LevelKomp', 'NUMERIC', false, 5, null);
        $this->addForeignKey('tingkat_pendidikan_id', 'TingkatPendidikanId', 'NUMERIC', 'ref.tingkat_pendidikan', 'tingkat_pendidikan_id', true, 4, null);
        $this->addForeignKey('kurikulum_id', 'KurikulumId', 'SMALLINT', 'ref.kurikulum', 'kurikulum_id', true, 2, null);
        $this->addForeignKey('mata_pelajaran_id', 'MataPelajaranId', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', true, 4, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('KompetensiRelatedByIdIntiDasar', 'Admin\\Model\\Kompetensi', RelationMap::MANY_TO_ONE, array('id_inti_dasar' => 'id_komp', ), null, null);
        $this->addRelation('Kurikulum', 'Admin\\Model\\Kurikulum', RelationMap::MANY_TO_ONE, array('kurikulum_id' => 'kurikulum_id', ), null, null);
        $this->addRelation('MataPelajaran', 'Admin\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('TingkatPendidikan', 'Admin\\Model\\TingkatPendidikan', RelationMap::MANY_TO_ONE, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null);
        $this->addRelation('KompetensiRelatedByIdKomp', 'Admin\\Model\\Kompetensi', RelationMap::ONE_TO_MANY, array('id_komp' => 'id_inti_dasar', ), null, null, 'KompetensisRelatedByIdKomp');
    } // buildRelations()

} // KompetensiTableMap
