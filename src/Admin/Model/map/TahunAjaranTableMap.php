<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.tahun_ajaran' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class TahunAjaranTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.TahunAjaranTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.tahun_ajaran');
        $this->setPhpName('TahunAjaran');
        $this->setClassname('Admin\\Model\\TahunAjaran');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('tahun_ajaran_id', 'TahunAjaranId', 'NUMERIC', true, 6, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 10, null);
        $this->addColumn('periode_aktif', 'PeriodeAktif', 'NUMERIC', true, 3, null);
        $this->addColumn('tanggal_mulai', 'TanggalMulai', 'VARCHAR', true, 20, null);
        $this->addColumn('tanggal_selesai', 'TanggalSelesai', 'VARCHAR', true, 20, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('TemplateUn', 'Admin\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null, 'TemplateUns');
        $this->addRelation('BeasiswaPesertaDidikRelatedByTahunSelesai', 'Admin\\Model\\BeasiswaPesertaDidik', RelationMap::ONE_TO_MANY, array('tahun_ajaran_id' => 'tahun_selesai', ), null, null, 'BeasiswaPesertaDidiksRelatedByTahunSelesai');
        $this->addRelation('BeasiswaPesertaDidikRelatedByTahunMulai', 'Admin\\Model\\BeasiswaPesertaDidik', RelationMap::ONE_TO_MANY, array('tahun_ajaran_id' => 'tahun_mulai', ), null, null, 'BeasiswaPesertaDidiksRelatedByTahunMulai');
        $this->addRelation('Demografi', 'Admin\\Model\\Demografi', RelationMap::ONE_TO_MANY, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null, 'Demografis');
        $this->addRelation('PengawasTerdaftar', 'Admin\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null, 'PengawasTerdaftars');
        $this->addRelation('PesertaDidikBaru', 'Admin\\Model\\PesertaDidikBaru', RelationMap::ONE_TO_MANY, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null, 'PesertaDidikBarus');
        $this->addRelation('PtkBaru', 'Admin\\Model\\PtkBaru', RelationMap::ONE_TO_MANY, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null, 'PtkBarus');
        $this->addRelation('PtkTerdaftar', 'Admin\\Model\\PtkTerdaftar', RelationMap::ONE_TO_MANY, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null, 'PtkTerdaftars');
        $this->addRelation('Semester', 'Admin\\Model\\Semester', RelationMap::ONE_TO_MANY, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null, 'Semesters');
    } // buildRelations()

} // TahunAjaranTableMap
