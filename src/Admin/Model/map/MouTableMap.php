<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'mou' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class MouTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.MouTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('mou');
        $this->setPhpName('Mou');
        $this->setClassname('Admin\\Model\\Mou');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('mou_id', 'MouId', 'CHAR', true, 16, null);
        $this->addForeignKey('id_jns_ks', 'IdJnsKs', 'NUMERIC', 'ref.jenis_ks', 'id_jns_ks', true, 8, null);
        $this->addForeignKey('dudi_id', 'DudiId', 'CHAR', 'dudi', 'dudi_id', false, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addColumn('nomor_mou', 'NomorMou', 'VARCHAR', true, 80, null);
        $this->addColumn('judul_mou', 'JudulMou', 'VARCHAR', true, 80, null);
        $this->addColumn('tanggal_mulai', 'TanggalMulai', 'VARCHAR', true, 20, null);
        $this->addColumn('tanggal_selesai', 'TanggalSelesai', 'VARCHAR', true, 20, null);
        $this->addColumn('nama_dudi', 'NamaDudi', 'VARCHAR', true, 100, null);
        $this->addColumn('npwp_dudi', 'NpwpDudi', 'CHAR', false, 15, null);
        $this->addColumn('nama_bidang_usaha', 'NamaBidangUsaha', 'VARCHAR', true, 40, null);
        $this->addColumn('telp_kantor', 'TelpKantor', 'VARCHAR', false, 20, null);
        $this->addColumn('fax', 'Fax', 'VARCHAR', false, 20, null);
        $this->addColumn('contact_person', 'ContactPerson', 'VARCHAR', false, 100, null);
        $this->addColumn('telp_cp', 'TelpCp', 'VARCHAR', false, 20, null);
        $this->addColumn('jabatan_cp', 'JabatanCp', 'VARCHAR', false, 40, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Dudi', 'Admin\\Model\\Dudi', RelationMap::MANY_TO_ONE, array('dudi_id' => 'dudi_id', ), null, null);
        $this->addRelation('Sekolah', 'Admin\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JenisKs', 'Admin\\Model\\JenisKs', RelationMap::MANY_TO_ONE, array('id_jns_ks' => 'id_jns_ks', ), null, null);
        $this->addRelation('AktPd', 'Admin\\Model\\AktPd', RelationMap::ONE_TO_MANY, array('mou_id' => 'mou_id', ), null, null, 'AktPds');
        $this->addRelation('UnitUsahaKerjasama', 'Admin\\Model\\UnitUsahaKerjasama', RelationMap::ONE_TO_MANY, array('mou_id' => 'mou_id', ), null, null, 'UnitUsahaKerjasamas');
        $this->addRelation('VldMou', 'Admin\\Model\\VldMou', RelationMap::ONE_TO_MANY, array('mou_id' => 'mou_id', ), null, null, 'VldMous');
        $this->addRelation('JurusanKerjasama', 'Admin\\Model\\JurusanKerjasama', RelationMap::ONE_TO_MANY, array('mou_id' => 'mou_id', ), null, null, 'JurusanKerjasamas');
    } // buildRelations()

} // MouTableMap
