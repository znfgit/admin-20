<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'prasarana' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class PrasaranaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.PrasaranaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('prasarana');
        $this->setPhpName('Prasarana');
        $this->setClassname('Admin\\Model\\Prasarana');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('prasarana_id', 'PrasaranaId', 'CHAR', true, 16, null);
        $this->addForeignKey('jenis_prasarana_id', 'JenisPrasaranaId', 'INTEGER', 'ref.jenis_prasarana', 'jenis_prasarana_id', true, 4, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('id_induk_prasarana', 'IdIndukPrasarana', 'CHAR', 'prasarana', 'prasarana_id', false, 16, null);
        $this->addForeignKey('kepemilikan_sarpras_id', 'KepemilikanSarprasId', 'NUMERIC', 'ref.status_kepemilikan_sarpras', 'kepemilikan_sarpras_id', true, 3, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 100, null);
        $this->addColumn('panjang', 'Panjang', 'FLOAT', false, 8, null);
        $this->addColumn('lebar', 'Lebar', 'FLOAT', false, 8, null);
        $this->addColumn('reg_pras', 'RegPras', 'VARCHAR', false, 16, null);
        $this->addColumn('ket_prasarana', 'KetPrasarana', 'VARCHAR', false, 240, null);
        $this->addColumn('vol_pondasi_m3', 'VolPondasiM3', 'NUMERIC', false, 9, null);
        $this->addColumn('vol_sloop_kolom_balok_m3', 'VolSloopKolomBalokM3', 'NUMERIC', false, 9, null);
        $this->addColumn('luas_plester_m2', 'LuasPlesterM2', 'NUMERIC', false, 9, null);
        $this->addColumn('panj_kudakuda_m', 'PanjKudakudaM', 'NUMERIC', false, 9, null);
        $this->addColumn('vol_kudakuda_m3', 'VolKudakudaM3', 'NUMERIC', false, 9, null);
        $this->addColumn('panj_kaso_m', 'PanjKasoM', 'NUMERIC', false, 9, null);
        $this->addColumn('panj_reng_m', 'PanjRengM', 'NUMERIC', false, 9, null);
        $this->addColumn('luas_tutup_atap_m2', 'LuasTutupAtapM2', 'NUMERIC', false, 9, null);
        $this->addColumn('luas_plafon_m2', 'LuasPlafonM2', 'NUMERIC', false, 9, null);
        $this->addColumn('luas_dinding_m2', 'LuasDindingM2', 'NUMERIC', false, 9, null);
        $this->addColumn('luas_daun_jendela_m2', 'LuasDaunJendelaM2', 'NUMERIC', false, 9, null);
        $this->addColumn('luas_daun_pintu_m2', 'LuasDaunPintuM2', 'NUMERIC', false, 9, null);
        $this->addColumn('panj_kusen_m', 'PanjKusenM', 'NUMERIC', false, 9, null);
        $this->addColumn('luas_tutup_lantai_m2', 'LuasTutupLantaiM2', 'NUMERIC', false, 9, null);
        $this->addColumn('panj_inst_listrik_m', 'PanjInstListrikM', 'NUMERIC', false, 9, null);
        $this->addColumn('jml_inst_listrik', 'JmlInstListrik', 'NUMERIC', false, 6, null);
        $this->addColumn('panj_inst_air_m', 'PanjInstAirM', 'NUMERIC', false, 9, null);
        $this->addColumn('jml_inst_air', 'JmlInstAir', 'NUMERIC', false, 6, null);
        $this->addColumn('panj_drainase_m', 'PanjDrainaseM', 'NUMERIC', false, 9, null);
        $this->addColumn('luas_finish_struktur_m2', 'LuasFinishStrukturM2', 'NUMERIC', false, 9, null);
        $this->addColumn('luas_finish_plafon_m2', 'LuasFinishPlafonM2', 'NUMERIC', false, 9, null);
        $this->addColumn('luas_finish_dinding_m2', 'LuasFinishDindingM2', 'NUMERIC', false, 9, null);
        $this->addColumn('luas_finish_kpj_m2', 'LuasFinishKpjM2', 'NUMERIC', false, 9, null);
        $this->addForeignKey('id_hapus_buku', 'IdHapusBuku', 'CHAR', 'ref.jenis_hapus_buku', 'id_hapus_buku', false, 1, null);
        $this->addColumn('tgl_hapus_buku', 'TglHapusBuku', 'VARCHAR', false, 20, null);
        $this->addColumn('asal_data', 'AsalData', 'CHAR', true, 1, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PrasaranaRelatedByIdIndukPrasarana', 'Admin\\Model\\Prasarana', RelationMap::MANY_TO_ONE, array('id_induk_prasarana' => 'prasarana_id', ), null, null);
        $this->addRelation('Sekolah', 'Admin\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JenisHapusBuku', 'Admin\\Model\\JenisHapusBuku', RelationMap::MANY_TO_ONE, array('id_hapus_buku' => 'id_hapus_buku', ), null, null);
        $this->addRelation('JenisPrasarana', 'Admin\\Model\\JenisPrasarana', RelationMap::MANY_TO_ONE, array('jenis_prasarana_id' => 'jenis_prasarana_id', ), null, null);
        $this->addRelation('StatusKepemilikanSarpras', 'Admin\\Model\\StatusKepemilikanSarpras', RelationMap::MANY_TO_ONE, array('kepemilikan_sarpras_id' => 'kepemilikan_sarpras_id', ), null, null);
        $this->addRelation('BukuAlat', 'Admin\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'BukuAlats');
        $this->addRelation('Jadwal', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'Jadwals');
        $this->addRelation('VldPrasarana', 'Admin\\Model\\VldPrasarana', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'VldPrasaranas');
        $this->addRelation('PrasaranaRelatedByPrasaranaId', 'Admin\\Model\\Prasarana', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'id_induk_prasarana', ), null, null, 'PrasaranasRelatedByPrasaranaId');
        $this->addRelation('PrasaranaLongitudinal', 'Admin\\Model\\PrasaranaLongitudinal', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'PrasaranaLongitudinals');
        $this->addRelation('RombonganBelajar', 'Admin\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'RombonganBelajars');
        $this->addRelation('Sarana', 'Admin\\Model\\Sarana', RelationMap::ONE_TO_MANY, array('prasarana_id' => 'prasarana_id', ), null, null, 'Saranas');
    } // buildRelations()

} // PrasaranaTableMap
