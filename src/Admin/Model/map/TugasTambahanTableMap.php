<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'tugas_tambahan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class TugasTambahanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.TugasTambahanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tugas_tambahan');
        $this->setPhpName('TugasTambahan');
        $this->setClassname('Admin\\Model\\TugasTambahan');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('ptk_tugas_tambahan_id', 'PtkTugasTambahanId', 'CHAR', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('jabatan_ptk_id', 'JabatanPtkId', 'NUMERIC', 'ref.jabatan_tugas_ptk', 'jabatan_ptk_id', true, 5, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addColumn('jumlah_jam', 'JumlahJam', 'NUMERIC', true, 4, null);
        $this->addColumn('nomor_sk', 'NomorSk', 'VARCHAR', true, 80, null);
        $this->addColumn('tmt_tambahan', 'TmtTambahan', 'VARCHAR', true, 20, null);
        $this->addColumn('tst_tambahan', 'TstTambahan', 'VARCHAR', false, 20, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Ptk', 'Admin\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('Sekolah', 'Admin\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JabatanTugasPtk', 'Admin\\Model\\JabatanTugasPtk', RelationMap::MANY_TO_ONE, array('jabatan_ptk_id' => 'jabatan_ptk_id', ), null, null);
        $this->addRelation('VldTugasTambahan', 'Admin\\Model\\VldTugasTambahan', RelationMap::ONE_TO_MANY, array('ptk_tugas_tambahan_id' => 'ptk_tugas_tambahan_id', ), null, null, 'VldTugasTambahans');
    } // buildRelations()

} // TugasTambahanTableMap
