<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.errortype' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class ErrortypeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.ErrortypeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.errortype');
        $this->setPhpName('Errortype');
        $this->setClassname('Admin\\Model\\Errortype');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('idtype', 'Idtype', 'INTEGER', true, 4, null);
        $this->addColumn('kategori_error', 'KategoriError', 'INTEGER', false, 4, null);
        $this->addColumn('keterangan', 'Keterangan', 'VARCHAR', false, 255, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('VldAktPd', 'Admin\\Model\\VldAktPd', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldAktPds');
        $this->addRelation('VldAnak', 'Admin\\Model\\VldAnak', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldAnaks');
        $this->addRelation('VldBeaPd', 'Admin\\Model\\VldBeaPd', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBeaPds');
        $this->addRelation('VldBeaPtk', 'Admin\\Model\\VldBeaPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBeaPtks');
        $this->addRelation('VldBukuPtk', 'Admin\\Model\\VldBukuPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBukuPtks');
        $this->addRelation('VldDemografi', 'Admin\\Model\\VldDemografi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldDemografis');
        $this->addRelation('VldDudi', 'Admin\\Model\\VldDudi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldDudis');
        $this->addRelation('VldInpassing', 'Admin\\Model\\VldInpassing', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldInpassings');
        $this->addRelation('VldJurusanSp', 'Admin\\Model\\VldJurusanSp', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldJurusanSps');
        $this->addRelation('VldKaryaTulis', 'Admin\\Model\\VldKaryaTulis', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldKaryaTuliss');
        $this->addRelation('VldKesejahteraan', 'Admin\\Model\\VldKesejahteraan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldKesejahteraans');
        $this->addRelation('VldMou', 'Admin\\Model\\VldMou', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldMous');
        $this->addRelation('VldNilaiRapor', 'Admin\\Model\\VldNilaiRapor', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNilaiRapors');
        $this->addRelation('VldNilaiTest', 'Admin\\Model\\VldNilaiTest', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNilaiTests');
        $this->addRelation('VldNonsekolah', 'Admin\\Model\\VldNonsekolah', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNonsekolahs');
        $this->addRelation('VldPdLong', 'Admin\\Model\\VldPdLong', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPdLongs');
        $this->addRelation('VldPembelajaran', 'Admin\\Model\\VldPembelajaran', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPembelajarans');
        $this->addRelation('VldPenghargaan', 'Admin\\Model\\VldPenghargaan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPenghargaans');
        $this->addRelation('VldPesertaDidik', 'Admin\\Model\\VldPesertaDidik', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPesertaDidiks');
        $this->addRelation('VldPrasarana', 'Admin\\Model\\VldPrasarana', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPrasaranas');
        $this->addRelation('VldPrestasi', 'Admin\\Model\\VldPrestasi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPrestasis');
        $this->addRelation('VldPtk', 'Admin\\Model\\VldPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPtks');
        $this->addRelation('VldPtkTerdaftar', 'Admin\\Model\\VldPtkTerdaftar', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPtkTerdaftars');
        $this->addRelation('VldRegPd', 'Admin\\Model\\VldRegPd', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRegPds');
        $this->addRelation('VldRombel', 'Admin\\Model\\VldRombel', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRombels');
        $this->addRelation('VldRwyFungsional', 'Admin\\Model\\VldRwyFungsional', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyFungsionals');
        $this->addRelation('VldRwyKepangkatan', 'Admin\\Model\\VldRwyKepangkatan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyKepangkatans');
        $this->addRelation('VldRwyKerja', 'Admin\\Model\\VldRwyKerja', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyKerjas');
        $this->addRelation('VldRwyPendFormal', 'Admin\\Model\\VldRwyPendFormal', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyPendFormals');
        $this->addRelation('VldRwySertifikasi', 'Admin\\Model\\VldRwySertifikasi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwySertifikasis');
        $this->addRelation('VldRwyStruktural', 'Admin\\Model\\VldRwyStruktural', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyStrukturals');
        $this->addRelation('VldSarana', 'Admin\\Model\\VldSarana', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldSaranas');
        $this->addRelation('VldSekolah', 'Admin\\Model\\VldSekolah', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldSekolahs');
        $this->addRelation('VldTugasTambahan', 'Admin\\Model\\VldTugasTambahan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldTugasTambahans');
        $this->addRelation('VldTunjangan', 'Admin\\Model\\VldTunjangan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldTunjangans');
        $this->addRelation('VldUn', 'Admin\\Model\\VldUn', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldUns');
        $this->addRelation('VldYayasan', 'Admin\\Model\\VldYayasan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldYayasans');
    } // buildRelations()

} // ErrortypeTableMap
