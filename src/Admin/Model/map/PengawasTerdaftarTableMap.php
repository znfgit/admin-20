<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'pengawas_terdaftar' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class PengawasTerdaftarTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.PengawasTerdaftarTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('pengawas_terdaftar');
        $this->setPhpName('PengawasTerdaftar');
        $this->setClassname('Admin\\Model\\PengawasTerdaftar');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('pengawas_terdaftar_id', 'PengawasTerdaftarId', 'CHAR', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('lembaga_id', 'LembagaId', 'CHAR', 'lembaga_non_sekolah', 'lembaga_id', true, 16, null);
        $this->addForeignKey('tahun_ajaran_id', 'TahunAjaranId', 'NUMERIC', 'ref.tahun_ajaran', 'tahun_ajaran_id', true, 6, null);
        $this->addColumn('nomor_surat_tugas', 'NomorSuratTugas', 'VARCHAR', true, 80, null);
        $this->addColumn('tanggal_surat_tugas', 'TanggalSuratTugas', 'VARCHAR', true, 20, null);
        $this->addColumn('tmt_tugas', 'TmtTugas', 'VARCHAR', true, 20, null);
        $this->addForeignKey('mata_pelajaran_id', 'MataPelajaranId', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', false, 4, null);
        $this->addForeignKey('bidang_studi_id', 'BidangStudiId', 'INTEGER', 'ref.bidang_studi', 'bidang_studi_id', false, 4, null);
        $this->addForeignKey('jenjang_kepengawasan_id', 'JenjangKepengawasanId', 'NUMERIC', 'ref.jenjang_kepengawasan', 'jenjang_kepengawasan_id', true, 4, null);
        $this->addColumn('aktif_bulan_01', 'AktifBulan01', 'NUMERIC', true, 3, null);
        $this->addColumn('aktif_bulan_02', 'AktifBulan02', 'NUMERIC', true, 3, null);
        $this->addColumn('aktif_bulan_03', 'AktifBulan03', 'NUMERIC', true, 3, null);
        $this->addColumn('aktif_bulan_04', 'AktifBulan04', 'NUMERIC', true, 3, null);
        $this->addColumn('aktif_bulan_05', 'AktifBulan05', 'NUMERIC', true, 3, null);
        $this->addColumn('aktif_bulan_06', 'AktifBulan06', 'NUMERIC', true, 3, null);
        $this->addColumn('aktif_bulan_07', 'AktifBulan07', 'NUMERIC', true, 3, null);
        $this->addColumn('aktif_bulan_08', 'AktifBulan08', 'NUMERIC', true, 3, null);
        $this->addColumn('aktif_bulan_09', 'AktifBulan09', 'NUMERIC', true, 3, null);
        $this->addColumn('aktif_bulan_10', 'AktifBulan10', 'NUMERIC', true, 3, null);
        $this->addColumn('aktif_bulan_11', 'AktifBulan11', 'NUMERIC', true, 3, null);
        $this->addColumn('aktif_bulan_12', 'AktifBulan12', 'NUMERIC', true, 3, null);
        $this->addForeignKey('jenis_keluar_id', 'JenisKeluarId', 'CHAR', 'ref.jenis_keluar', 'jenis_keluar_id', false, 1, null);
        $this->addColumn('tgl_pengawas_keluar', 'TglPengawasKeluar', 'VARCHAR', false, 20, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('LembagaNonSekolah', 'Admin\\Model\\LembagaNonSekolah', RelationMap::MANY_TO_ONE, array('lembaga_id' => 'lembaga_id', ), null, null);
        $this->addRelation('Ptk', 'Admin\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('BidangStudi', 'Admin\\Model\\BidangStudi', RelationMap::MANY_TO_ONE, array('bidang_studi_id' => 'bidang_studi_id', ), null, null);
        $this->addRelation('JenisKeluar', 'Admin\\Model\\JenisKeluar', RelationMap::MANY_TO_ONE, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null);
        $this->addRelation('JenjangKepengawasan', 'Admin\\Model\\JenjangKepengawasan', RelationMap::MANY_TO_ONE, array('jenjang_kepengawasan_id' => 'jenjang_kepengawasan_id', ), null, null);
        $this->addRelation('MataPelajaran', 'Admin\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('TahunAjaran', 'Admin\\Model\\TahunAjaran', RelationMap::MANY_TO_ONE, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null);
        $this->addRelation('GuruSasaranPengawas', 'Admin\\Model\\GuruSasaranPengawas', RelationMap::ONE_TO_MANY, array('pengawas_terdaftar_id' => 'pengawas_terdaftar_id', ), null, null, 'GuruSasaranPengawass');
        $this->addRelation('SasaranPengawasan', 'Admin\\Model\\SasaranPengawasan', RelationMap::ONE_TO_MANY, array('pengawas_terdaftar_id' => 'pengawas_terdaftar_id', ), null, null, 'SasaranPengawasans');
    } // buildRelations()

} // PengawasTerdaftarTableMap
