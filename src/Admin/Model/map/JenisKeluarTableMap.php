<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenis_keluar' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class JenisKeluarTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.JenisKeluarTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenis_keluar');
        $this->setPhpName('JenisKeluar');
        $this->setClassname('Admin\\Model\\JenisKeluar');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jenis_keluar_id', 'JenisKeluarId', 'CHAR', true, 1, null);
        $this->addColumn('ket_keluar', 'KetKeluar', 'VARCHAR', true, 40, null);
        $this->addColumn('keluar_pd', 'KeluarPd', 'NUMERIC', true, 3, null);
        $this->addColumn('keluar_ptk', 'KeluarPtk', 'NUMERIC', true, 3, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PengawasTerdaftar', 'Admin\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null, 'PengawasTerdaftars');
        $this->addRelation('PtkTerdaftar', 'Admin\\Model\\PtkTerdaftar', RelationMap::ONE_TO_MANY, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null, 'PtkTerdaftars');
        $this->addRelation('RegistrasiPesertaDidik', 'Admin\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null, 'RegistrasiPesertaDidiks');
    } // buildRelations()

} // JenisKeluarTableMap
