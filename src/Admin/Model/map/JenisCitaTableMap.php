<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenis_cita' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class JenisCitaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.JenisCitaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenis_cita');
        $this->setPhpName('JenisCita');
        $this->setClassname('Admin\\Model\\JenisCita');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_cita', 'IdCita', 'NUMERIC', true, 7, null);
        $this->addColumn('nm_cita', 'NmCita', 'VARCHAR', true, 150, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RegistrasiPesertaDidik', 'Admin\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('id_cita' => 'id_cita', ), null, null, 'RegistrasiPesertaDidiks');
    } // buildRelations()

} // JenisCitaTableMap
