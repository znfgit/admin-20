<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'jadwal' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class JadwalTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.JadwalTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jadwal');
        $this->setPhpName('Jadwal');
        $this->setClassname('Admin\\Model\\Jadwal');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('sekolah_id', 'SekolahId', 'CHAR' , 'sekolah_longitudinal', 'semester_id', true, 16, null);
        $this->addForeignPrimaryKey('semester_id', 'SemesterId', 'CHAR' , 'sekolah_longitudinal', 'semester_id', true, 5, null);
        $this->addForeignPrimaryKey('prasarana_id', 'PrasaranaId', 'CHAR' , 'prasarana', 'prasarana_id', true, 16, null);
        $this->addPrimaryKey('Hari', 'Hari', 'NUMERIC', true, 3, null);
        $this->addForeignKey('bel_ke_01', 'BelKe01', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_02', 'BelKe02', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_03', 'BelKe03', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_04', 'BelKe04', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_05', 'BelKe05', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_06', 'BelKe06', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_07', 'BelKe07', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_08', 'BelKe08', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_09', 'BelKe09', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_10', 'BelKe10', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_11', 'BelKe11', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_12', 'BelKe12', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_13', 'BelKe13', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_14', 'BelKe14', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_15', 'BelKe15', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_16', 'BelKe16', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_17', 'BelKe17', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_18', 'BelKe18', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_19', 'BelKe19', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addForeignKey('bel_ke_20', 'BelKe20', 'CHAR', 'pembelajaran', 'pembelajaran_id', false, 16, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PembelajaranRelatedByBelKe01', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_01' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe02', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_02' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe03', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_03' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe04', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_04' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe05', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_05' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe06', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_06' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe19', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_19' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe20', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_20' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe13', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_13' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe14', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_14' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe15', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_15' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe16', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_16' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe17', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_17' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe18', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_18' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe07', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_07' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe08', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_08' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe09', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_09' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe10', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_10' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe11', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_11' => 'pembelajaran_id', ), null, null);
        $this->addRelation('PembelajaranRelatedByBelKe12', 'Admin\\Model\\Pembelajaran', RelationMap::MANY_TO_ONE, array('bel_ke_12' => 'pembelajaran_id', ), null, null);
        $this->addRelation('Prasarana', 'Admin\\Model\\Prasarana', RelationMap::MANY_TO_ONE, array('prasarana_id' => 'prasarana_id', ), null, null);
        $this->addRelation('SekolahLongitudinal', 'Admin\\Model\\SekolahLongitudinal', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'semester_id', 'semester_id' => 'semester_id', ), null, null);
    } // buildRelations()

} // JadwalTableMap
