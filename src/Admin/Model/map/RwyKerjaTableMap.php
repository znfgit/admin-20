<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'rwy_kerja' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class RwyKerjaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.RwyKerjaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rwy_kerja');
        $this->setPhpName('RwyKerja');
        $this->setClassname('Admin\\Model\\RwyKerja');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('rwy_kerja_id', 'RwyKerjaId', 'CHAR', true, 16, null);
        $this->addForeignKey('jenjang_pendidikan_id', 'JenjangPendidikanId', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', false, 4, null);
        $this->addForeignKey('jenis_lembaga_id', 'JenisLembagaId', 'NUMERIC', 'ref.jenis_lembaga', 'jenis_lembaga_id', true, 7, null);
        $this->addForeignKey('status_kepegawaian_id', 'StatusKepegawaianId', 'SMALLINT', 'ref.status_kepegawaian', 'status_kepegawaian_id', true, 2, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('jenis_ptk_id', 'JenisPtkId', 'NUMERIC', 'ref.jenis_ptk', 'jenis_ptk_id', true, 4, null);
        $this->addColumn('lembaga_pengangkat', 'LembagaPengangkat', 'VARCHAR', true, 100, null);
        $this->addColumn('no_sk_kerja', 'NoSkKerja', 'VARCHAR', true, 80, null);
        $this->addColumn('tgl_sk_kerja', 'TglSkKerja', 'VARCHAR', true, 20, null);
        $this->addColumn('tmt_kerja', 'TmtKerja', 'VARCHAR', true, 20, null);
        $this->addColumn('tst_kerja', 'TstKerja', 'VARCHAR', false, 20, null);
        $this->addColumn('tempat_kerja', 'TempatKerja', 'VARCHAR', true, 100, null);
        $this->addColumn('ttd_sk_kerja', 'TtdSkKerja', 'VARCHAR', false, 80, null);
        $this->addColumn('mapel_diajarkan', 'MapelDiajarkan', 'VARCHAR', false, 80, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Ptk', 'Admin\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('JenisLembaga', 'Admin\\Model\\JenisLembaga', RelationMap::MANY_TO_ONE, array('jenis_lembaga_id' => 'jenis_lembaga_id', ), null, null);
        $this->addRelation('JenisPtk', 'Admin\\Model\\JenisPtk', RelationMap::MANY_TO_ONE, array('jenis_ptk_id' => 'jenis_ptk_id', ), null, null);
        $this->addRelation('JenjangPendidikan', 'Admin\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('StatusKepegawaian', 'Admin\\Model\\StatusKepegawaian', RelationMap::MANY_TO_ONE, array('status_kepegawaian_id' => 'status_kepegawaian_id', ), null, null);
        $this->addRelation('VldRwyKerja', 'Admin\\Model\\VldRwyKerja', RelationMap::ONE_TO_MANY, array('rwy_kerja_id' => 'rwy_kerja_id', ), null, null, 'VldRwyKerjas');
    } // buildRelations()

} // RwyKerjaTableMap
