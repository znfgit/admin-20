<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.semester' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class SemesterTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.SemesterTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.semester');
        $this->setPhpName('Semester');
        $this->setClassname('Admin\\Model\\Semester');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('semester_id', 'SemesterId', 'CHAR', true, 5, null);
        $this->addForeignKey('tahun_ajaran_id', 'TahunAjaranId', 'NUMERIC', 'ref.tahun_ajaran', 'tahun_ajaran_id', true, 6, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 20, null);
        $this->addColumn('semester', 'Semester', 'NUMERIC', true, 3, null);
        $this->addColumn('periode_aktif', 'PeriodeAktif', 'NUMERIC', true, 3, null);
        $this->addColumn('tanggal_mulai', 'TanggalMulai', 'VARCHAR', true, 20, null);
        $this->addColumn('tanggal_selesai', 'TanggalSelesai', 'VARCHAR', true, 20, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('TahunAjaran', 'Admin\\Model\\TahunAjaran', RelationMap::MANY_TO_ONE, array('tahun_ajaran_id' => 'tahun_ajaran_id', ), null, null);
        $this->addRelation('BatasWaktuRapor', 'Admin\\Model\\BatasWaktuRapor', RelationMap::ONE_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('Tunjangan', 'Admin\\Model\\Tunjangan', RelationMap::ONE_TO_MANY, array('semester_id' => 'semester_id', ), null, null, 'Tunjangans');
        $this->addRelation('BukuAlatLongitudinal', 'Admin\\Model\\BukuAlatLongitudinal', RelationMap::ONE_TO_MANY, array('semester_id' => 'semester_id', ), null, null, 'BukuAlatLongitudinals');
        $this->addRelation('JurSpLong', 'Admin\\Model\\JurSpLong', RelationMap::ONE_TO_MANY, array('semester_id' => 'semester_id', ), null, null, 'JurSpLongs');
        $this->addRelation('Pembelajaran', 'Admin\\Model\\Pembelajaran', RelationMap::ONE_TO_MANY, array('semester_id' => 'semester_id', ), null, null, 'Pembelajarans');
        $this->addRelation('PesertaDidikLongitudinal', 'Admin\\Model\\PesertaDidikLongitudinal', RelationMap::ONE_TO_MANY, array('semester_id' => 'semester_id', ), null, null, 'PesertaDidikLongitudinals');
        $this->addRelation('PrasaranaLongitudinal', 'Admin\\Model\\PrasaranaLongitudinal', RelationMap::ONE_TO_MANY, array('semester_id' => 'semester_id', ), null, null, 'PrasaranaLongitudinals');
        $this->addRelation('RombonganBelajar', 'Admin\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('semester_id' => 'semester_id', ), null, null, 'RombonganBelajars');
        $this->addRelation('Sanitasi', 'Admin\\Model\\Sanitasi', RelationMap::ONE_TO_MANY, array('semester_id' => 'semester_id', ), null, null, 'Sanitasis');
        $this->addRelation('SaranaLongitudinal', 'Admin\\Model\\SaranaLongitudinal', RelationMap::ONE_TO_MANY, array('semester_id' => 'semester_id', ), null, null, 'SaranaLongitudinals');
        $this->addRelation('SekolahLongitudinal', 'Admin\\Model\\SekolahLongitudinal', RelationMap::ONE_TO_MANY, array('semester_id' => 'semester_id', ), null, null, 'SekolahLongitudinals');
    } // buildRelations()

} // SemesterTableMap
