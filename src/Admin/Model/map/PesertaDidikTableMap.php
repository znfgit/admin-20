<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'peserta_didik' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class PesertaDidikTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.PesertaDidikTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('peserta_didik');
        $this->setPhpName('PesertaDidik');
        $this->setClassname('Admin\\Model\\PesertaDidik');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('peserta_didik_id', 'PesertaDidikId', 'CHAR', true, 16, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 100, null);
        $this->addColumn('jenis_kelamin', 'JenisKelamin', 'CHAR', true, 1, null);
        $this->addColumn('nisn', 'Nisn', 'CHAR', false, 10, null);
        $this->addColumn('nik', 'Nik', 'CHAR', false, 16, null);
        $this->addColumn('tempat_lahir', 'TempatLahir', 'VARCHAR', false, 32, null);
        $this->addColumn('tanggal_lahir', 'TanggalLahir', 'VARCHAR', true, 20, null);
        $this->addForeignKey('agama_id', 'AgamaId', 'SMALLINT', 'ref.agama', 'agama_id', true, 2, null);
        $this->addForeignKey('kewarganegaraan', 'Kewarganegaraan', 'CHAR', 'ref.negara', 'negara_id', true, 2, null);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addColumn('alamat_jalan', 'AlamatJalan', 'VARCHAR', true, 80, null);
        $this->addColumn('rt', 'Rt', 'NUMERIC', false, 4, null);
        $this->addColumn('rw', 'Rw', 'NUMERIC', false, 4, null);
        $this->addColumn('nama_dusun', 'NamaDusun', 'VARCHAR', false, 60, null);
        $this->addColumn('desa_kelurahan', 'DesaKelurahan', 'VARCHAR', true, 60, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addColumn('kode_pos', 'KodePos', 'CHAR', false, 5, null);
        $this->addForeignKey('jenis_tinggal_id', 'JenisTinggalId', 'NUMERIC', 'ref.jenis_tinggal', 'jenis_tinggal_id', false, 4, null);
        $this->addForeignKey('alat_transportasi_id', 'AlatTransportasiId', 'NUMERIC', 'ref.alat_transportasi', 'alat_transportasi_id', false, 4, null);
        $this->addColumn('nik_ayah', 'NikAyah', 'CHAR', false, 16, null);
        $this->addColumn('nik_ibu', 'NikIbu', 'CHAR', false, 16, null);
        $this->addColumn('nik_wali', 'NikWali', 'CHAR', false, 16, null);
        $this->addColumn('nomor_telepon_rumah', 'NomorTeleponRumah', 'VARCHAR', false, 20, null);
        $this->addColumn('nomor_telepon_seluler', 'NomorTeleponSeluler', 'VARCHAR', false, 20, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 60, null);
        $this->addColumn('penerima_KPS', 'PenerimaKps', 'NUMERIC', true, 3, null);
        $this->addColumn('no_KPS', 'NoKps', 'VARCHAR', false, 80, null);
        $this->addColumn('layak_PIP', 'LayakPip', 'NUMERIC', true, 3, null);
        $this->addColumn('penerima_KIP', 'PenerimaKip', 'NUMERIC', true, 3, null);
        $this->addColumn('no_KIP', 'NoKip', 'VARCHAR', false, 6, null);
        $this->addColumn('nm_KIP', 'NmKip', 'VARCHAR', false, 100, null);
        $this->addColumn('no_KKS', 'NoKks', 'VARCHAR', false, 6, null);
        $this->addColumn('reg_akta_lahir', 'RegAktaLahir', 'VARCHAR', false, 80, null);
        $this->addForeignKey('id_layak_pip', 'IdLayakPip', 'NUMERIC', 'ref.alasan_layak_pip', 'id_layak_pip', false, 4, null);
        $this->addColumn('status_data', 'StatusData', 'INTEGER', false, 4, null);
        $this->addColumn('nama_ayah', 'NamaAyah', 'VARCHAR', false, 100, null);
        $this->addColumn('tahun_lahir_ayah', 'TahunLahirAyah', 'NUMERIC', false, 6, null);
        $this->addForeignKey('jenjang_pendidikan_ayah', 'JenjangPendidikanAyah', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', false, 4, null);
        $this->addForeignKey('pekerjaan_id_ayah', 'PekerjaanIdAyah', 'INTEGER', 'ref.pekerjaan', 'pekerjaan_id', false, 4, null);
        $this->addForeignKey('penghasilan_id_ayah', 'PenghasilanIdAyah', 'INTEGER', 'ref.penghasilan_orangtua_wali', 'penghasilan_orangtua_wali_id', false, 4, null);
        $this->addForeignKey('kebutuhan_khusus_id_ayah', 'KebutuhanKhususIdAyah', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addColumn('nama_ibu_kandung', 'NamaIbuKandung', 'VARCHAR', true, 100, null);
        $this->addColumn('tahun_lahir_ibu', 'TahunLahirIbu', 'NUMERIC', false, 6, null);
        $this->addForeignKey('jenjang_pendidikan_ibu', 'JenjangPendidikanIbu', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', false, 4, null);
        $this->addForeignKey('penghasilan_id_ibu', 'PenghasilanIdIbu', 'INTEGER', 'ref.penghasilan_orangtua_wali', 'penghasilan_orangtua_wali_id', false, 4, null);
        $this->addForeignKey('pekerjaan_id_ibu', 'PekerjaanIdIbu', 'INTEGER', 'ref.pekerjaan', 'pekerjaan_id', false, 4, null);
        $this->addForeignKey('kebutuhan_khusus_id_ibu', 'KebutuhanKhususIdIbu', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addColumn('nama_wali', 'NamaWali', 'VARCHAR', false, 30, null);
        $this->addColumn('tahun_lahir_wali', 'TahunLahirWali', 'NUMERIC', false, 6, null);
        $this->addForeignKey('jenjang_pendidikan_wali', 'JenjangPendidikanWali', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', false, 4, null);
        $this->addForeignKey('pekerjaan_id_wali', 'PekerjaanIdWali', 'INTEGER', 'ref.pekerjaan', 'pekerjaan_id', false, 4, null);
        $this->addForeignKey('penghasilan_id_wali', 'PenghasilanIdWali', 'INTEGER', 'ref.penghasilan_orangtua_wali', 'penghasilan_orangtua_wali_id', false, 4, null);
        $this->addForeignKey('id_bank', 'IdBank', 'CHAR', 'ref.bank', 'id_bank', false, 3, null);
        $this->addColumn('rekening_bank', 'RekeningBank', 'VARCHAR', false, 20, null);
        $this->addColumn('rekening_atas_nama', 'RekeningAtasNama', 'VARCHAR', false, 50, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Agama', 'Admin\\Model\\Agama', RelationMap::MANY_TO_ONE, array('agama_id' => 'agama_id', ), null, null);
        $this->addRelation('AlasanLayakPip', 'Admin\\Model\\AlasanLayakPip', RelationMap::MANY_TO_ONE, array('id_layak_pip' => 'id_layak_pip', ), null, null);
        $this->addRelation('AlatTransportasi', 'Admin\\Model\\AlatTransportasi', RelationMap::MANY_TO_ONE, array('alat_transportasi_id' => 'alat_transportasi_id', ), null, null);
        $this->addRelation('Bank', 'Admin\\Model\\Bank', RelationMap::MANY_TO_ONE, array('id_bank' => 'id_bank', ), null, null);
        $this->addRelation('JenisTinggal', 'Admin\\Model\\JenisTinggal', RelationMap::MANY_TO_ONE, array('jenis_tinggal_id' => 'jenis_tinggal_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanIbu', 'Admin\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_ibu' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanAyah', 'Admin\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_ayah' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanWali', 'Admin\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_wali' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususIdAyah', 'Admin\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id_ayah' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususIdIbu', 'Admin\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id_ibu' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususId', 'Admin\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('MstWilayah', 'Admin\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('Negara', 'Admin\\Model\\Negara', RelationMap::MANY_TO_ONE, array('kewarganegaraan' => 'negara_id', ), null, null);
        $this->addRelation('PekerjaanRelatedByPekerjaanIdAyah', 'Admin\\Model\\Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_id_ayah' => 'pekerjaan_id', ), null, null);
        $this->addRelation('PekerjaanRelatedByPekerjaanIdIbu', 'Admin\\Model\\Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_id_ibu' => 'pekerjaan_id', ), null, null);
        $this->addRelation('PekerjaanRelatedByPekerjaanIdWali', 'Admin\\Model\\Pekerjaan', RelationMap::MANY_TO_ONE, array('pekerjaan_id_wali' => 'pekerjaan_id', ), null, null);
        $this->addRelation('PenghasilanOrangtuaWaliRelatedByPenghasilanIdAyah', 'Admin\\Model\\PenghasilanOrangtuaWali', RelationMap::MANY_TO_ONE, array('penghasilan_id_ayah' => 'penghasilan_orangtua_wali_id', ), null, null);
        $this->addRelation('PenghasilanOrangtuaWaliRelatedByPenghasilanIdWali', 'Admin\\Model\\PenghasilanOrangtuaWali', RelationMap::MANY_TO_ONE, array('penghasilan_id_wali' => 'penghasilan_orangtua_wali_id', ), null, null);
        $this->addRelation('PenghasilanOrangtuaWaliRelatedByPenghasilanIdIbu', 'Admin\\Model\\PenghasilanOrangtuaWali', RelationMap::MANY_TO_ONE, array('penghasilan_id_ibu' => 'penghasilan_orangtua_wali_id', ), null, null);
        $this->addRelation('AnggotaPanitia', 'Admin\\Model\\AnggotaPanitia', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'AnggotaPanitias');
        $this->addRelation('AnggotaRombel', 'Admin\\Model\\AnggotaRombel', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'AnggotaRombels');
        $this->addRelation('BeasiswaPesertaDidik', 'Admin\\Model\\BeasiswaPesertaDidik', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'BeasiswaPesertaDidiks');
        $this->addRelation('VldPesertaDidik', 'Admin\\Model\\VldPesertaDidik', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'VldPesertaDidiks');
        $this->addRelation('KesejahteraanPd', 'Admin\\Model\\KesejahteraanPd', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'KesejahteraanPds');
        $this->addRelation('PesertaDidikBaru', 'Admin\\Model\\PesertaDidikBaru', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'PesertaDidikBarus');
        $this->addRelation('PesertaDidikLongitudinal', 'Admin\\Model\\PesertaDidikLongitudinal', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'PesertaDidikLongitudinals');
        $this->addRelation('Prestasi', 'Admin\\Model\\Prestasi', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'Prestasis');
        $this->addRelation('RegistrasiPesertaDidik', 'Admin\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('peserta_didik_id' => 'peserta_didik_id', ), null, null, 'RegistrasiPesertaDidiks');
    } // buildRelations()

} // PesertaDidikTableMap
