<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenis_lk' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class JenisLkTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.JenisLkTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenis_lk');
        $this->setPhpName('JenisLk');
        $this->setClassname('Admin\\Model\\JenisLk');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_jenis_lk', 'IdJenisLk', 'CHAR', true, 6, null);
        $this->addColumn('nm_jenis_lk', 'NmJenisLk', 'VARCHAR', true, 160, null);
        $this->addColumn('ket_jenis_lk', 'KetJenisLk', 'VARCHAR', false, 200, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('LayananKhusus', 'Admin\\Model\\LayananKhusus', RelationMap::ONE_TO_MANY, array('id_jenis_lk' => 'id_jenis_lk', ), null, null, 'LayananKhususes');
    } // buildRelations()

} // JenisLkTableMap
