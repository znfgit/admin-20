<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jabatan_tugas_ptk' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class JabatanTugasPtkTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.JabatanTugasPtkTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jabatan_tugas_ptk');
        $this->setPhpName('JabatanTugasPtk');
        $this->setClassname('Admin\\Model\\JabatanTugasPtk');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jabatan_ptk_id', 'JabatanPtkId', 'NUMERIC', true, 5, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 40, null);
        $this->addColumn('jabatan_utama', 'JabatanUtama', 'NUMERIC', true, 3, null);
        $this->addColumn('tugas_tambahan_guru', 'TugasTambahanGuru', 'NUMERIC', true, 3, null);
        $this->addColumn('jumlah_jam_diakui', 'JumlahJamDiakui', 'NUMERIC', false, 4, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('TugasTambahan', 'Admin\\Model\\TugasTambahan', RelationMap::ONE_TO_MANY, array('jabatan_ptk_id' => 'jabatan_ptk_id', ), null, null, 'TugasTambahans');
        $this->addRelation('RwyStruktural', 'Admin\\Model\\RwyStruktural', RelationMap::ONE_TO_MANY, array('jabatan_ptk_id' => 'jabatan_ptk_id', ), null, null, 'RwyStrukturals');
    } // buildRelations()

} // JabatanTugasPtkTableMap
