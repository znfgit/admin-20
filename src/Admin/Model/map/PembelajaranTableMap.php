<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'pembelajaran' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class PembelajaranTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.PembelajaranTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('pembelajaran');
        $this->setPhpName('Pembelajaran');
        $this->setClassname('Admin\\Model\\Pembelajaran');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('pembelajaran_id', 'PembelajaranId', 'CHAR', true, 16, null);
        $this->addForeignKey('rombongan_belajar_id', 'RombonganBelajarId', 'CHAR', 'rombongan_belajar', 'rombongan_belajar_id', true, 16, null);
        $this->addForeignKey('semester_id', 'SemesterId', 'CHAR', 'ref.semester', 'semester_id', true, 5, null);
        $this->addForeignKey('mata_pelajaran_id', 'MataPelajaranId', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', true, 4, null);
        $this->addForeignKey('ptk_terdaftar_id', 'PtkTerdaftarId', 'CHAR', 'ptk_terdaftar', 'ptk_terdaftar_id', true, 16, null);
        $this->addColumn('sk_mengajar', 'SkMengajar', 'VARCHAR', true, 80, null);
        $this->addColumn('tanggal_sk_mengajar', 'TanggalSkMengajar', 'VARCHAR', true, 20, null);
        $this->addColumn('jam_mengajar_per_minggu', 'JamMengajarPerMinggu', 'NUMERIC', true, 4, null);
        $this->addColumn('status_di_kurikulum', 'StatusDiKurikulum', 'NUMERIC', true, 3, null);
        $this->addColumn('nama_mata_pelajaran', 'NamaMataPelajaran', 'VARCHAR', true, 50, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkTerdaftar', 'Admin\\Model\\PtkTerdaftar', RelationMap::MANY_TO_ONE, array('ptk_terdaftar_id' => 'ptk_terdaftar_id', ), null, null);
        $this->addRelation('RombonganBelajar', 'Admin\\Model\\RombonganBelajar', RelationMap::MANY_TO_ONE, array('rombongan_belajar_id' => 'rombongan_belajar_id', ), null, null);
        $this->addRelation('MataPelajaran', 'Admin\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('Semester', 'Admin\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('JadwalRelatedByBelKe01', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_01', ), null, null, 'JadwalsRelatedByBelKe01');
        $this->addRelation('JadwalRelatedByBelKe02', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_02', ), null, null, 'JadwalsRelatedByBelKe02');
        $this->addRelation('JadwalRelatedByBelKe03', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_03', ), null, null, 'JadwalsRelatedByBelKe03');
        $this->addRelation('JadwalRelatedByBelKe04', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_04', ), null, null, 'JadwalsRelatedByBelKe04');
        $this->addRelation('JadwalRelatedByBelKe05', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_05', ), null, null, 'JadwalsRelatedByBelKe05');
        $this->addRelation('JadwalRelatedByBelKe06', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_06', ), null, null, 'JadwalsRelatedByBelKe06');
        $this->addRelation('JadwalRelatedByBelKe19', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_19', ), null, null, 'JadwalsRelatedByBelKe19');
        $this->addRelation('JadwalRelatedByBelKe20', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_20', ), null, null, 'JadwalsRelatedByBelKe20');
        $this->addRelation('JadwalRelatedByBelKe13', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_13', ), null, null, 'JadwalsRelatedByBelKe13');
        $this->addRelation('JadwalRelatedByBelKe14', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_14', ), null, null, 'JadwalsRelatedByBelKe14');
        $this->addRelation('JadwalRelatedByBelKe15', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_15', ), null, null, 'JadwalsRelatedByBelKe15');
        $this->addRelation('JadwalRelatedByBelKe16', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_16', ), null, null, 'JadwalsRelatedByBelKe16');
        $this->addRelation('JadwalRelatedByBelKe17', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_17', ), null, null, 'JadwalsRelatedByBelKe17');
        $this->addRelation('JadwalRelatedByBelKe18', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_18', ), null, null, 'JadwalsRelatedByBelKe18');
        $this->addRelation('JadwalRelatedByBelKe07', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_07', ), null, null, 'JadwalsRelatedByBelKe07');
        $this->addRelation('JadwalRelatedByBelKe08', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_08', ), null, null, 'JadwalsRelatedByBelKe08');
        $this->addRelation('JadwalRelatedByBelKe09', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_09', ), null, null, 'JadwalsRelatedByBelKe09');
        $this->addRelation('JadwalRelatedByBelKe10', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_10', ), null, null, 'JadwalsRelatedByBelKe10');
        $this->addRelation('JadwalRelatedByBelKe11', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_11', ), null, null, 'JadwalsRelatedByBelKe11');
        $this->addRelation('JadwalRelatedByBelKe12', 'Admin\\Model\\Jadwal', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'bel_ke_12', ), null, null, 'JadwalsRelatedByBelKe12');
        $this->addRelation('VldPembelajaran', 'Admin\\Model\\VldPembelajaran', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'pembelajaran_id', ), null, null, 'VldPembelajarans');
    } // buildRelations()

} // PembelajaranTableMap
