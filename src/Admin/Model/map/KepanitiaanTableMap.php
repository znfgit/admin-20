<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'kepanitiaan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class KepanitiaanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.KepanitiaanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('kepanitiaan');
        $this->setPhpName('Kepanitiaan');
        $this->setClassname('Admin\\Model\\Kepanitiaan');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_panitia', 'IdPanitia', 'CHAR', true, 16, null);
        $this->addForeignKey('id_jns_panitia', 'IdJnsPanitia', 'INTEGER', 'ref.jenis_kepanitiaan', 'id_jns_panitia', true, 4, null);
        $this->addColumn('nm_panitia', 'NmPanitia', 'VARCHAR', true, 80, null);
        $this->addColumn('instansi', 'Instansi', 'VARCHAR', true, 100, null);
        $this->addColumn('tkt_panitia', 'TktPanitia', 'CHAR', true, 1, null);
        $this->addColumn('sk_tugas', 'SkTugas', 'VARCHAR', true, 80, null);
        $this->addColumn('tmt_sk_tugas', 'TmtSkTugas', 'VARCHAR', true, 20, null);
        $this->addColumn('tst_sk_tugas', 'TstSkTugas', 'VARCHAR', false, 20, null);
        $this->addColumn('a_pasang_papan', 'APasangPapan', 'NUMERIC', true, 3, null);
        $this->addColumn('a_formulir', 'AFormulir', 'NUMERIC', true, 3, null);
        $this->addColumn('a_silabus', 'ASilabus', 'NUMERIC', true, 3, null);
        $this->addColumn('a_berlaku_pos', 'ABerlakuPos', 'NUMERIC', true, 3, null);
        $this->addColumn('a_sosialisasi_pos', 'ASosialisasiPos', 'NUMERIC', true, 3, null);
        $this->addColumn('a_ks_edukatif', 'AKsEdukatif', 'NUMERIC', true, 3, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', false, 16, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Sekolah', 'Admin\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JenisKepanitiaan', 'Admin\\Model\\JenisKepanitiaan', RelationMap::MANY_TO_ONE, array('id_jns_panitia' => 'id_jns_panitia', ), null, null);
        $this->addRelation('AnggotaPanitia', 'Admin\\Model\\AnggotaPanitia', RelationMap::ONE_TO_MANY, array('id_panitia' => 'id_panitia', ), null, null, 'AnggotaPanitias');
    } // buildRelations()

} // KepanitiaanTableMap
