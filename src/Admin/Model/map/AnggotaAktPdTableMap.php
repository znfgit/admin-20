<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'anggota_akt_pd' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class AnggotaAktPdTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.AnggotaAktPdTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('anggota_akt_pd');
        $this->setPhpName('AnggotaAktPd');
        $this->setClassname('Admin\\Model\\AnggotaAktPd');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_ang_akt_pd', 'IdAngAktPd', 'CHAR', true, 16, null);
        $this->addForeignKey('id_akt_pd', 'IdAktPd', 'CHAR', 'akt_pd', 'id_akt_pd', true, 16, null);
        $this->addForeignKey('registrasi_id', 'RegistrasiId', 'CHAR', 'registrasi_peserta_didik', 'registrasi_id', true, 16, null);
        $this->addColumn('nm_pd', 'NmPd', 'VARCHAR', true, 100, null);
        $this->addColumn('nipd', 'Nipd', 'VARCHAR', true, 24, null);
        $this->addColumn('jns_peran_pd', 'JnsPeranPd', 'CHAR', true, 1, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('AktPd', 'Admin\\Model\\AktPd', RelationMap::MANY_TO_ONE, array('id_akt_pd' => 'id_akt_pd', ), null, null);
        $this->addRelation('RegistrasiPesertaDidik', 'Admin\\Model\\RegistrasiPesertaDidik', RelationMap::MANY_TO_ONE, array('registrasi_id' => 'registrasi_id', ), null, null);
    } // buildRelations()

} // AnggotaAktPdTableMap
