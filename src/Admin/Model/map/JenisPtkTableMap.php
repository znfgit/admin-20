<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenis_ptk' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class JenisPtkTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.JenisPtkTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenis_ptk');
        $this->setPhpName('JenisPtk');
        $this->setClassname('Admin\\Model\\JenisPtk');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jenis_ptk_id', 'JenisPtkId', 'NUMERIC', true, 4, null);
        $this->addColumn('jenis_ptk', 'JenisPtk', 'VARCHAR', true, 30, null);
        $this->addColumn('guru_kelas', 'GuruKelas', 'NUMERIC', true, 3, null);
        $this->addColumn('guru_matpel', 'GuruMatpel', 'NUMERIC', true, 3, null);
        $this->addColumn('guru_bk', 'GuruBk', 'NUMERIC', true, 3, null);
        $this->addColumn('guru_inklusi', 'GuruInklusi', 'NUMERIC', true, 3, null);
        $this->addColumn('pengawas_satdik', 'PengawasSatdik', 'NUMERIC', true, 3, null);
        $this->addColumn('pengawas_plb', 'PengawasPlb', 'NUMERIC', true, 3, null);
        $this->addColumn('pengawas_matpel', 'PengawasMatpel', 'NUMERIC', true, 3, null);
        $this->addColumn('pengawas_bidang', 'PengawasBidang', 'NUMERIC', true, 3, null);
        $this->addColumn('tas', 'Tas', 'NUMERIC', true, 3, null);
        $this->addColumn('formal', 'Formal', 'NUMERIC', true, 3, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Ptk', 'Admin\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('jenis_ptk_id' => 'jenis_ptk_id', ), null, null, 'Ptks');
        $this->addRelation('RwyKerja', 'Admin\\Model\\RwyKerja', RelationMap::ONE_TO_MANY, array('jenis_ptk_id' => 'jenis_ptk_id', ), null, null, 'RwyKerjas');
    } // buildRelations()

} // JenisPtkTableMap
