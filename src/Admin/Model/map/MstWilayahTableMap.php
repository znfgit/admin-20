<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.mst_wilayah' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class MstWilayahTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.MstWilayahTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.mst_wilayah');
        $this->setPhpName('MstWilayah');
        $this->setClassname('Admin\\Model\\MstWilayah');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('kode_wilayah', 'KodeWilayah', 'CHAR', true, 8, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 60, null);
        $this->addForeignKey('id_level_wilayah', 'IdLevelWilayah', 'SMALLINT', 'ref.level_wilayah', 'id_level_wilayah', true, 2, null);
        $this->addForeignKey('mst_kode_wilayah', 'MstKodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', false, 8, null);
        $this->addForeignKey('negara_id', 'NegaraId', 'CHAR', 'ref.negara', 'negara_id', true, 2, null);
        $this->addColumn('asal_wilayah', 'AsalWilayah', 'CHAR', false, 8, null);
        $this->addColumn('kode_bps', 'KodeBps', 'CHAR', false, 7, null);
        $this->addColumn('kode_dagri', 'KodeDagri', 'CHAR', false, 7, null);
        $this->addColumn('kode_keu', 'KodeKeu', 'VARCHAR', false, 10, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('LevelWilayah', 'Admin\\Model\\LevelWilayah', RelationMap::MANY_TO_ONE, array('id_level_wilayah' => 'id_level_wilayah', ), null, null);
        $this->addRelation('MstWilayahRelatedByMstKodeWilayah', 'Admin\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('mst_kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('Negara', 'Admin\\Model\\Negara', RelationMap::MANY_TO_ONE, array('negara_id' => 'negara_id', ), null, null);
        $this->addRelation('TetanggaKabkotaRelatedByKodeWilayah1', 'Admin\\Model\\TetanggaKabkota', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah1', ), null, null, 'TetanggaKabkotasRelatedByKodeWilayah1');
        $this->addRelation('TetanggaKabkotaRelatedByKodeWilayah2', 'Admin\\Model\\TetanggaKabkota', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah2', ), null, null, 'TetanggaKabkotasRelatedByKodeWilayah2');
        $this->addRelation('Demografi', 'Admin\\Model\\Demografi', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'Demografis');
        $this->addRelation('Dudi', 'Admin\\Model\\Dudi', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'Dudis');
        $this->addRelation('Instalasi', 'Admin\\Model\\Instalasi', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'Instalasis');
        $this->addRelation('LembagaNonSekolah', 'Admin\\Model\\LembagaNonSekolah', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'LembagaNonSekolahs');
        $this->addRelation('Pengguna', 'Admin\\Model\\Pengguna', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'Penggunas');
        $this->addRelation('Yayasan', 'Admin\\Model\\Yayasan', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'Yayasans');
        $this->addRelation('PesertaDidik', 'Admin\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'PesertaDidiks');
        $this->addRelation('Ptk', 'Admin\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'Ptks');
        $this->addRelation('LembagaAkreditasi', 'Admin\\Model\\LembagaAkreditasi', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'LembagaAkreditasis');
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'Admin\\Model\\MstWilayah', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'mst_kode_wilayah', ), null, null, 'MstWilayahsRelatedByKodeWilayah');
        $this->addRelation('Mulok', 'Admin\\Model\\Mulok', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'Muloks');
        $this->addRelation('Sekolah', 'Admin\\Model\\Sekolah', RelationMap::ONE_TO_MANY, array('kode_wilayah' => 'kode_wilayah', ), null, null, 'Sekolahs');
    } // buildRelations()

} // MstWilayahTableMap
