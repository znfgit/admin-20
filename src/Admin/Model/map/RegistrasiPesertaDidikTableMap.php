<?php

namespace Admin\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'registrasi_peserta_didik' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Admin.Model.map
 */
class RegistrasiPesertaDidikTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Admin.Model.map.RegistrasiPesertaDidikTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('registrasi_peserta_didik');
        $this->setPhpName('RegistrasiPesertaDidik');
        $this->setClassname('Admin\\Model\\RegistrasiPesertaDidik');
        $this->setPackage('Admin.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('registrasi_id', 'RegistrasiId', 'CHAR', true, 16, null);
        $this->addForeignKey('jurusan_sp_id', 'JurusanSpId', 'CHAR', 'jurusan_sp', 'jurusan_sp_id', false, 16, null);
        $this->addForeignKey('peserta_didik_id', 'PesertaDidikId', 'CHAR', 'peserta_didik', 'peserta_didik_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('jenis_pendaftaran_id', 'JenisPendaftaranId', 'NUMERIC', 'ref.jenis_pendaftaran', 'jenis_pendaftaran_id', true, 3, null);
        $this->addColumn('nipd', 'Nipd', 'VARCHAR', false, 18, null);
        $this->addColumn('tanggal_masuk_sekolah', 'TanggalMasukSekolah', 'VARCHAR', true, 20, null);
        $this->addForeignKey('jenis_keluar_id', 'JenisKeluarId', 'CHAR', 'ref.jenis_keluar', 'jenis_keluar_id', false, 1, null);
        $this->addColumn('tanggal_keluar', 'TanggalKeluar', 'VARCHAR', false, 20, null);
        $this->addColumn('keterangan', 'Keterangan', 'VARCHAR', false, 128, null);
        $this->addColumn('no_SKHUN', 'NoSkhun', 'CHAR', false, 20, null);
        $this->addColumn('no_peserta_ujian', 'NoPesertaUjian', 'CHAR', false, 20, null);
        $this->addColumn('no_seri_ijazah', 'NoSeriIjazah', 'VARCHAR', false, 80, null);
        $this->addColumn('a_pernah_paud', 'APernahPaud', 'NUMERIC', true, 3, null);
        $this->addColumn('a_pernah_tk', 'APernahTk', 'NUMERIC', true, 3, null);
        $this->addColumn('sekolah_asal', 'SekolahAsal', 'VARCHAR', false, 100, null);
        $this->addForeignKey('id_hobby', 'IdHobby', 'NUMERIC', 'ref.jenis_hobby', 'id_hobby', true, 7, null);
        $this->addForeignKey('id_cita', 'IdCita', 'NUMERIC', 'ref.jenis_cita', 'id_cita', true, 7, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JurusanSp', 'Admin\\Model\\JurusanSp', RelationMap::MANY_TO_ONE, array('jurusan_sp_id' => 'jurusan_sp_id', ), null, null);
        $this->addRelation('PesertaDidik', 'Admin\\Model\\PesertaDidik', RelationMap::MANY_TO_ONE, array('peserta_didik_id' => 'peserta_didik_id', ), null, null);
        $this->addRelation('Sekolah', 'Admin\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JenisCita', 'Admin\\Model\\JenisCita', RelationMap::MANY_TO_ONE, array('id_cita' => 'id_cita', ), null, null);
        $this->addRelation('JenisHobby', 'Admin\\Model\\JenisHobby', RelationMap::MANY_TO_ONE, array('id_hobby' => 'id_hobby', ), null, null);
        $this->addRelation('JenisKeluar', 'Admin\\Model\\JenisKeluar', RelationMap::MANY_TO_ONE, array('jenis_keluar_id' => 'jenis_keluar_id', ), null, null);
        $this->addRelation('JenisPendaftaran', 'Admin\\Model\\JenisPendaftaran', RelationMap::MANY_TO_ONE, array('jenis_pendaftaran_id' => 'jenis_pendaftaran_id', ), null, null);
        $this->addRelation('TracerLulusan', 'Admin\\Model\\TracerLulusan', RelationMap::ONE_TO_MANY, array('registrasi_id' => 'registrasi_id', ), null, null, 'TracerLulusans');
        $this->addRelation('AnggotaAktPd', 'Admin\\Model\\AnggotaAktPd', RelationMap::ONE_TO_MANY, array('registrasi_id' => 'registrasi_id', ), null, null, 'AnggotaAktPds');
        $this->addRelation('VldRegPd', 'Admin\\Model\\VldRegPd', RelationMap::ONE_TO_MANY, array('registrasi_id' => 'registrasi_id', ), null, null, 'VldRegPds');
    } // buildRelations()

} // RegistrasiPesertaDidikTableMap
