<?php

namespace Admin\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Admin\Model\Prasarana;
use Admin\Model\PrasaranaLongitudinal;
use Admin\Model\PrasaranaLongitudinalPeer;
use Admin\Model\PrasaranaLongitudinalQuery;
use Admin\Model\Semester;

/**
 * Base class that represents a query for the 'prasarana_longitudinal' table.
 *
 *
 *
 * @method PrasaranaLongitudinalQuery orderByPrasaranaId($order = Criteria::ASC) Order by the prasarana_id column
 * @method PrasaranaLongitudinalQuery orderBySemesterId($order = Criteria::ASC) Order by the semester_id column
 * @method PrasaranaLongitudinalQuery orderByRusakPondasi($order = Criteria::ASC) Order by the rusak_pondasi column
 * @method PrasaranaLongitudinalQuery orderByKetPondasi($order = Criteria::ASC) Order by the ket_pondasi column
 * @method PrasaranaLongitudinalQuery orderByRusakSloopKolomBalok($order = Criteria::ASC) Order by the rusak_sloop_kolom_balok column
 * @method PrasaranaLongitudinalQuery orderByKetSloopKolomBalok($order = Criteria::ASC) Order by the ket_sloop_kolom_balok column
 * @method PrasaranaLongitudinalQuery orderByRusakPlesterStruktur($order = Criteria::ASC) Order by the rusak_plester_struktur column
 * @method PrasaranaLongitudinalQuery orderByKetPlesterStruktur($order = Criteria::ASC) Order by the ket_plester_struktur column
 * @method PrasaranaLongitudinalQuery orderByRusakKudakudaAtap($order = Criteria::ASC) Order by the rusak_kudakuda_atap column
 * @method PrasaranaLongitudinalQuery orderByKetKudakudaAtap($order = Criteria::ASC) Order by the ket_kudakuda_atap column
 * @method PrasaranaLongitudinalQuery orderByRusakKasoAtap($order = Criteria::ASC) Order by the rusak_kaso_atap column
 * @method PrasaranaLongitudinalQuery orderByKetKasoAtap($order = Criteria::ASC) Order by the ket_kaso_atap column
 * @method PrasaranaLongitudinalQuery orderByRusakRengAtap($order = Criteria::ASC) Order by the rusak_reng_atap column
 * @method PrasaranaLongitudinalQuery orderByKetRengAtap($order = Criteria::ASC) Order by the ket_reng_atap column
 * @method PrasaranaLongitudinalQuery orderByRusakTutupAtap($order = Criteria::ASC) Order by the rusak_tutup_atap column
 * @method PrasaranaLongitudinalQuery orderByKetTutupAtap($order = Criteria::ASC) Order by the ket_tutup_atap column
 * @method PrasaranaLongitudinalQuery orderByRusakLisplangTalang($order = Criteria::ASC) Order by the rusak_lisplang_talang column
 * @method PrasaranaLongitudinalQuery orderByKetLisplangTalang($order = Criteria::ASC) Order by the ket_lisplang_talang column
 * @method PrasaranaLongitudinalQuery orderByRusakRangkaPlafon($order = Criteria::ASC) Order by the rusak_rangka_plafon column
 * @method PrasaranaLongitudinalQuery orderByKetRangkaPlafon($order = Criteria::ASC) Order by the ket_rangka_plafon column
 * @method PrasaranaLongitudinalQuery orderByRusakTutupPlafon($order = Criteria::ASC) Order by the rusak_tutup_plafon column
 * @method PrasaranaLongitudinalQuery orderByKetTutupPlafon($order = Criteria::ASC) Order by the ket_tutup_plafon column
 * @method PrasaranaLongitudinalQuery orderByRusakBataDinding($order = Criteria::ASC) Order by the rusak_bata_dinding column
 * @method PrasaranaLongitudinalQuery orderByKetBataDinding($order = Criteria::ASC) Order by the ket_bata_dinding column
 * @method PrasaranaLongitudinalQuery orderByRusakPlesterDinding($order = Criteria::ASC) Order by the rusak_plester_dinding column
 * @method PrasaranaLongitudinalQuery orderByKetPlesterDinding($order = Criteria::ASC) Order by the ket_plester_dinding column
 * @method PrasaranaLongitudinalQuery orderByRusakDaunJendela($order = Criteria::ASC) Order by the rusak_daun_jendela column
 * @method PrasaranaLongitudinalQuery orderByKetDaunJendela($order = Criteria::ASC) Order by the ket_daun_jendela column
 * @method PrasaranaLongitudinalQuery orderByRusakDaunPintu($order = Criteria::ASC) Order by the rusak_daun_pintu column
 * @method PrasaranaLongitudinalQuery orderByKetDaunPintu($order = Criteria::ASC) Order by the ket_daun_pintu column
 * @method PrasaranaLongitudinalQuery orderByRusakKusen($order = Criteria::ASC) Order by the rusak_kusen column
 * @method PrasaranaLongitudinalQuery orderByKetKusen($order = Criteria::ASC) Order by the ket_kusen column
 * @method PrasaranaLongitudinalQuery orderByRusakTutupLantai($order = Criteria::ASC) Order by the rusak_tutup_lantai column
 * @method PrasaranaLongitudinalQuery orderByKetPenutupLantai($order = Criteria::ASC) Order by the ket_penutup_lantai column
 * @method PrasaranaLongitudinalQuery orderByRusakInstListrik($order = Criteria::ASC) Order by the rusak_inst_listrik column
 * @method PrasaranaLongitudinalQuery orderByKetInstListrik($order = Criteria::ASC) Order by the ket_inst_listrik column
 * @method PrasaranaLongitudinalQuery orderByRusakInstAir($order = Criteria::ASC) Order by the rusak_inst_air column
 * @method PrasaranaLongitudinalQuery orderByKetInstAir($order = Criteria::ASC) Order by the ket_inst_air column
 * @method PrasaranaLongitudinalQuery orderByRusakDrainase($order = Criteria::ASC) Order by the rusak_drainase column
 * @method PrasaranaLongitudinalQuery orderByKetDrainase($order = Criteria::ASC) Order by the ket_drainase column
 * @method PrasaranaLongitudinalQuery orderByRusakFinishStruktur($order = Criteria::ASC) Order by the rusak_finish_struktur column
 * @method PrasaranaLongitudinalQuery orderByKetFinishStruktur($order = Criteria::ASC) Order by the ket_finish_struktur column
 * @method PrasaranaLongitudinalQuery orderByRusakFinishPlafon($order = Criteria::ASC) Order by the rusak_finish_plafon column
 * @method PrasaranaLongitudinalQuery orderByKetFinishPlafon($order = Criteria::ASC) Order by the ket_finish_plafon column
 * @method PrasaranaLongitudinalQuery orderByRusakFinishDinding($order = Criteria::ASC) Order by the rusak_finish_dinding column
 * @method PrasaranaLongitudinalQuery orderByKetFinishDinding($order = Criteria::ASC) Order by the ket_finish_dinding column
 * @method PrasaranaLongitudinalQuery orderByRusakFinishKpj($order = Criteria::ASC) Order by the rusak_finish_kpj column
 * @method PrasaranaLongitudinalQuery orderByKetFinishKpj($order = Criteria::ASC) Order by the ket_finish_kpj column
 * @method PrasaranaLongitudinalQuery orderByBerfungsi($order = Criteria::ASC) Order by the berfungsi column
 * @method PrasaranaLongitudinalQuery orderByBlobId($order = Criteria::ASC) Order by the blob_id column
 * @method PrasaranaLongitudinalQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method PrasaranaLongitudinalQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method PrasaranaLongitudinalQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method PrasaranaLongitudinalQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method PrasaranaLongitudinalQuery groupByPrasaranaId() Group by the prasarana_id column
 * @method PrasaranaLongitudinalQuery groupBySemesterId() Group by the semester_id column
 * @method PrasaranaLongitudinalQuery groupByRusakPondasi() Group by the rusak_pondasi column
 * @method PrasaranaLongitudinalQuery groupByKetPondasi() Group by the ket_pondasi column
 * @method PrasaranaLongitudinalQuery groupByRusakSloopKolomBalok() Group by the rusak_sloop_kolom_balok column
 * @method PrasaranaLongitudinalQuery groupByKetSloopKolomBalok() Group by the ket_sloop_kolom_balok column
 * @method PrasaranaLongitudinalQuery groupByRusakPlesterStruktur() Group by the rusak_plester_struktur column
 * @method PrasaranaLongitudinalQuery groupByKetPlesterStruktur() Group by the ket_plester_struktur column
 * @method PrasaranaLongitudinalQuery groupByRusakKudakudaAtap() Group by the rusak_kudakuda_atap column
 * @method PrasaranaLongitudinalQuery groupByKetKudakudaAtap() Group by the ket_kudakuda_atap column
 * @method PrasaranaLongitudinalQuery groupByRusakKasoAtap() Group by the rusak_kaso_atap column
 * @method PrasaranaLongitudinalQuery groupByKetKasoAtap() Group by the ket_kaso_atap column
 * @method PrasaranaLongitudinalQuery groupByRusakRengAtap() Group by the rusak_reng_atap column
 * @method PrasaranaLongitudinalQuery groupByKetRengAtap() Group by the ket_reng_atap column
 * @method PrasaranaLongitudinalQuery groupByRusakTutupAtap() Group by the rusak_tutup_atap column
 * @method PrasaranaLongitudinalQuery groupByKetTutupAtap() Group by the ket_tutup_atap column
 * @method PrasaranaLongitudinalQuery groupByRusakLisplangTalang() Group by the rusak_lisplang_talang column
 * @method PrasaranaLongitudinalQuery groupByKetLisplangTalang() Group by the ket_lisplang_talang column
 * @method PrasaranaLongitudinalQuery groupByRusakRangkaPlafon() Group by the rusak_rangka_plafon column
 * @method PrasaranaLongitudinalQuery groupByKetRangkaPlafon() Group by the ket_rangka_plafon column
 * @method PrasaranaLongitudinalQuery groupByRusakTutupPlafon() Group by the rusak_tutup_plafon column
 * @method PrasaranaLongitudinalQuery groupByKetTutupPlafon() Group by the ket_tutup_plafon column
 * @method PrasaranaLongitudinalQuery groupByRusakBataDinding() Group by the rusak_bata_dinding column
 * @method PrasaranaLongitudinalQuery groupByKetBataDinding() Group by the ket_bata_dinding column
 * @method PrasaranaLongitudinalQuery groupByRusakPlesterDinding() Group by the rusak_plester_dinding column
 * @method PrasaranaLongitudinalQuery groupByKetPlesterDinding() Group by the ket_plester_dinding column
 * @method PrasaranaLongitudinalQuery groupByRusakDaunJendela() Group by the rusak_daun_jendela column
 * @method PrasaranaLongitudinalQuery groupByKetDaunJendela() Group by the ket_daun_jendela column
 * @method PrasaranaLongitudinalQuery groupByRusakDaunPintu() Group by the rusak_daun_pintu column
 * @method PrasaranaLongitudinalQuery groupByKetDaunPintu() Group by the ket_daun_pintu column
 * @method PrasaranaLongitudinalQuery groupByRusakKusen() Group by the rusak_kusen column
 * @method PrasaranaLongitudinalQuery groupByKetKusen() Group by the ket_kusen column
 * @method PrasaranaLongitudinalQuery groupByRusakTutupLantai() Group by the rusak_tutup_lantai column
 * @method PrasaranaLongitudinalQuery groupByKetPenutupLantai() Group by the ket_penutup_lantai column
 * @method PrasaranaLongitudinalQuery groupByRusakInstListrik() Group by the rusak_inst_listrik column
 * @method PrasaranaLongitudinalQuery groupByKetInstListrik() Group by the ket_inst_listrik column
 * @method PrasaranaLongitudinalQuery groupByRusakInstAir() Group by the rusak_inst_air column
 * @method PrasaranaLongitudinalQuery groupByKetInstAir() Group by the ket_inst_air column
 * @method PrasaranaLongitudinalQuery groupByRusakDrainase() Group by the rusak_drainase column
 * @method PrasaranaLongitudinalQuery groupByKetDrainase() Group by the ket_drainase column
 * @method PrasaranaLongitudinalQuery groupByRusakFinishStruktur() Group by the rusak_finish_struktur column
 * @method PrasaranaLongitudinalQuery groupByKetFinishStruktur() Group by the ket_finish_struktur column
 * @method PrasaranaLongitudinalQuery groupByRusakFinishPlafon() Group by the rusak_finish_plafon column
 * @method PrasaranaLongitudinalQuery groupByKetFinishPlafon() Group by the ket_finish_plafon column
 * @method PrasaranaLongitudinalQuery groupByRusakFinishDinding() Group by the rusak_finish_dinding column
 * @method PrasaranaLongitudinalQuery groupByKetFinishDinding() Group by the ket_finish_dinding column
 * @method PrasaranaLongitudinalQuery groupByRusakFinishKpj() Group by the rusak_finish_kpj column
 * @method PrasaranaLongitudinalQuery groupByKetFinishKpj() Group by the ket_finish_kpj column
 * @method PrasaranaLongitudinalQuery groupByBerfungsi() Group by the berfungsi column
 * @method PrasaranaLongitudinalQuery groupByBlobId() Group by the blob_id column
 * @method PrasaranaLongitudinalQuery groupByLastUpdate() Group by the Last_update column
 * @method PrasaranaLongitudinalQuery groupBySoftDelete() Group by the Soft_delete column
 * @method PrasaranaLongitudinalQuery groupByLastSync() Group by the last_sync column
 * @method PrasaranaLongitudinalQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method PrasaranaLongitudinalQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method PrasaranaLongitudinalQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method PrasaranaLongitudinalQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method PrasaranaLongitudinalQuery leftJoinPrasarana($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prasarana relation
 * @method PrasaranaLongitudinalQuery rightJoinPrasarana($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prasarana relation
 * @method PrasaranaLongitudinalQuery innerJoinPrasarana($relationAlias = null) Adds a INNER JOIN clause to the query using the Prasarana relation
 *
 * @method PrasaranaLongitudinalQuery leftJoinSemester($relationAlias = null) Adds a LEFT JOIN clause to the query using the Semester relation
 * @method PrasaranaLongitudinalQuery rightJoinSemester($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Semester relation
 * @method PrasaranaLongitudinalQuery innerJoinSemester($relationAlias = null) Adds a INNER JOIN clause to the query using the Semester relation
 *
 * @method PrasaranaLongitudinal findOne(PropelPDO $con = null) Return the first PrasaranaLongitudinal matching the query
 * @method PrasaranaLongitudinal findOneOrCreate(PropelPDO $con = null) Return the first PrasaranaLongitudinal matching the query, or a new PrasaranaLongitudinal object populated from the query conditions when no match is found
 *
 * @method PrasaranaLongitudinal findOneByPrasaranaId(string $prasarana_id) Return the first PrasaranaLongitudinal filtered by the prasarana_id column
 * @method PrasaranaLongitudinal findOneBySemesterId(string $semester_id) Return the first PrasaranaLongitudinal filtered by the semester_id column
 * @method PrasaranaLongitudinal findOneByRusakPondasi(string $rusak_pondasi) Return the first PrasaranaLongitudinal filtered by the rusak_pondasi column
 * @method PrasaranaLongitudinal findOneByKetPondasi(string $ket_pondasi) Return the first PrasaranaLongitudinal filtered by the ket_pondasi column
 * @method PrasaranaLongitudinal findOneByRusakSloopKolomBalok(string $rusak_sloop_kolom_balok) Return the first PrasaranaLongitudinal filtered by the rusak_sloop_kolom_balok column
 * @method PrasaranaLongitudinal findOneByKetSloopKolomBalok(string $ket_sloop_kolom_balok) Return the first PrasaranaLongitudinal filtered by the ket_sloop_kolom_balok column
 * @method PrasaranaLongitudinal findOneByRusakPlesterStruktur(string $rusak_plester_struktur) Return the first PrasaranaLongitudinal filtered by the rusak_plester_struktur column
 * @method PrasaranaLongitudinal findOneByKetPlesterStruktur(string $ket_plester_struktur) Return the first PrasaranaLongitudinal filtered by the ket_plester_struktur column
 * @method PrasaranaLongitudinal findOneByRusakKudakudaAtap(string $rusak_kudakuda_atap) Return the first PrasaranaLongitudinal filtered by the rusak_kudakuda_atap column
 * @method PrasaranaLongitudinal findOneByKetKudakudaAtap(string $ket_kudakuda_atap) Return the first PrasaranaLongitudinal filtered by the ket_kudakuda_atap column
 * @method PrasaranaLongitudinal findOneByRusakKasoAtap(string $rusak_kaso_atap) Return the first PrasaranaLongitudinal filtered by the rusak_kaso_atap column
 * @method PrasaranaLongitudinal findOneByKetKasoAtap(string $ket_kaso_atap) Return the first PrasaranaLongitudinal filtered by the ket_kaso_atap column
 * @method PrasaranaLongitudinal findOneByRusakRengAtap(string $rusak_reng_atap) Return the first PrasaranaLongitudinal filtered by the rusak_reng_atap column
 * @method PrasaranaLongitudinal findOneByKetRengAtap(string $ket_reng_atap) Return the first PrasaranaLongitudinal filtered by the ket_reng_atap column
 * @method PrasaranaLongitudinal findOneByRusakTutupAtap(string $rusak_tutup_atap) Return the first PrasaranaLongitudinal filtered by the rusak_tutup_atap column
 * @method PrasaranaLongitudinal findOneByKetTutupAtap(string $ket_tutup_atap) Return the first PrasaranaLongitudinal filtered by the ket_tutup_atap column
 * @method PrasaranaLongitudinal findOneByRusakLisplangTalang(string $rusak_lisplang_talang) Return the first PrasaranaLongitudinal filtered by the rusak_lisplang_talang column
 * @method PrasaranaLongitudinal findOneByKetLisplangTalang(string $ket_lisplang_talang) Return the first PrasaranaLongitudinal filtered by the ket_lisplang_talang column
 * @method PrasaranaLongitudinal findOneByRusakRangkaPlafon(string $rusak_rangka_plafon) Return the first PrasaranaLongitudinal filtered by the rusak_rangka_plafon column
 * @method PrasaranaLongitudinal findOneByKetRangkaPlafon(string $ket_rangka_plafon) Return the first PrasaranaLongitudinal filtered by the ket_rangka_plafon column
 * @method PrasaranaLongitudinal findOneByRusakTutupPlafon(string $rusak_tutup_plafon) Return the first PrasaranaLongitudinal filtered by the rusak_tutup_plafon column
 * @method PrasaranaLongitudinal findOneByKetTutupPlafon(string $ket_tutup_plafon) Return the first PrasaranaLongitudinal filtered by the ket_tutup_plafon column
 * @method PrasaranaLongitudinal findOneByRusakBataDinding(string $rusak_bata_dinding) Return the first PrasaranaLongitudinal filtered by the rusak_bata_dinding column
 * @method PrasaranaLongitudinal findOneByKetBataDinding(string $ket_bata_dinding) Return the first PrasaranaLongitudinal filtered by the ket_bata_dinding column
 * @method PrasaranaLongitudinal findOneByRusakPlesterDinding(string $rusak_plester_dinding) Return the first PrasaranaLongitudinal filtered by the rusak_plester_dinding column
 * @method PrasaranaLongitudinal findOneByKetPlesterDinding(string $ket_plester_dinding) Return the first PrasaranaLongitudinal filtered by the ket_plester_dinding column
 * @method PrasaranaLongitudinal findOneByRusakDaunJendela(string $rusak_daun_jendela) Return the first PrasaranaLongitudinal filtered by the rusak_daun_jendela column
 * @method PrasaranaLongitudinal findOneByKetDaunJendela(string $ket_daun_jendela) Return the first PrasaranaLongitudinal filtered by the ket_daun_jendela column
 * @method PrasaranaLongitudinal findOneByRusakDaunPintu(string $rusak_daun_pintu) Return the first PrasaranaLongitudinal filtered by the rusak_daun_pintu column
 * @method PrasaranaLongitudinal findOneByKetDaunPintu(string $ket_daun_pintu) Return the first PrasaranaLongitudinal filtered by the ket_daun_pintu column
 * @method PrasaranaLongitudinal findOneByRusakKusen(string $rusak_kusen) Return the first PrasaranaLongitudinal filtered by the rusak_kusen column
 * @method PrasaranaLongitudinal findOneByKetKusen(string $ket_kusen) Return the first PrasaranaLongitudinal filtered by the ket_kusen column
 * @method PrasaranaLongitudinal findOneByRusakTutupLantai(string $rusak_tutup_lantai) Return the first PrasaranaLongitudinal filtered by the rusak_tutup_lantai column
 * @method PrasaranaLongitudinal findOneByKetPenutupLantai(string $ket_penutup_lantai) Return the first PrasaranaLongitudinal filtered by the ket_penutup_lantai column
 * @method PrasaranaLongitudinal findOneByRusakInstListrik(string $rusak_inst_listrik) Return the first PrasaranaLongitudinal filtered by the rusak_inst_listrik column
 * @method PrasaranaLongitudinal findOneByKetInstListrik(string $ket_inst_listrik) Return the first PrasaranaLongitudinal filtered by the ket_inst_listrik column
 * @method PrasaranaLongitudinal findOneByRusakInstAir(string $rusak_inst_air) Return the first PrasaranaLongitudinal filtered by the rusak_inst_air column
 * @method PrasaranaLongitudinal findOneByKetInstAir(string $ket_inst_air) Return the first PrasaranaLongitudinal filtered by the ket_inst_air column
 * @method PrasaranaLongitudinal findOneByRusakDrainase(string $rusak_drainase) Return the first PrasaranaLongitudinal filtered by the rusak_drainase column
 * @method PrasaranaLongitudinal findOneByKetDrainase(string $ket_drainase) Return the first PrasaranaLongitudinal filtered by the ket_drainase column
 * @method PrasaranaLongitudinal findOneByRusakFinishStruktur(string $rusak_finish_struktur) Return the first PrasaranaLongitudinal filtered by the rusak_finish_struktur column
 * @method PrasaranaLongitudinal findOneByKetFinishStruktur(string $ket_finish_struktur) Return the first PrasaranaLongitudinal filtered by the ket_finish_struktur column
 * @method PrasaranaLongitudinal findOneByRusakFinishPlafon(string $rusak_finish_plafon) Return the first PrasaranaLongitudinal filtered by the rusak_finish_plafon column
 * @method PrasaranaLongitudinal findOneByKetFinishPlafon(string $ket_finish_plafon) Return the first PrasaranaLongitudinal filtered by the ket_finish_plafon column
 * @method PrasaranaLongitudinal findOneByRusakFinishDinding(string $rusak_finish_dinding) Return the first PrasaranaLongitudinal filtered by the rusak_finish_dinding column
 * @method PrasaranaLongitudinal findOneByKetFinishDinding(string $ket_finish_dinding) Return the first PrasaranaLongitudinal filtered by the ket_finish_dinding column
 * @method PrasaranaLongitudinal findOneByRusakFinishKpj(string $rusak_finish_kpj) Return the first PrasaranaLongitudinal filtered by the rusak_finish_kpj column
 * @method PrasaranaLongitudinal findOneByKetFinishKpj(string $ket_finish_kpj) Return the first PrasaranaLongitudinal filtered by the ket_finish_kpj column
 * @method PrasaranaLongitudinal findOneByBerfungsi(string $berfungsi) Return the first PrasaranaLongitudinal filtered by the berfungsi column
 * @method PrasaranaLongitudinal findOneByBlobId(string $blob_id) Return the first PrasaranaLongitudinal filtered by the blob_id column
 * @method PrasaranaLongitudinal findOneByLastUpdate(string $Last_update) Return the first PrasaranaLongitudinal filtered by the Last_update column
 * @method PrasaranaLongitudinal findOneBySoftDelete(string $Soft_delete) Return the first PrasaranaLongitudinal filtered by the Soft_delete column
 * @method PrasaranaLongitudinal findOneByLastSync(string $last_sync) Return the first PrasaranaLongitudinal filtered by the last_sync column
 * @method PrasaranaLongitudinal findOneByUpdaterId(string $Updater_ID) Return the first PrasaranaLongitudinal filtered by the Updater_ID column
 *
 * @method array findByPrasaranaId(string $prasarana_id) Return PrasaranaLongitudinal objects filtered by the prasarana_id column
 * @method array findBySemesterId(string $semester_id) Return PrasaranaLongitudinal objects filtered by the semester_id column
 * @method array findByRusakPondasi(string $rusak_pondasi) Return PrasaranaLongitudinal objects filtered by the rusak_pondasi column
 * @method array findByKetPondasi(string $ket_pondasi) Return PrasaranaLongitudinal objects filtered by the ket_pondasi column
 * @method array findByRusakSloopKolomBalok(string $rusak_sloop_kolom_balok) Return PrasaranaLongitudinal objects filtered by the rusak_sloop_kolom_balok column
 * @method array findByKetSloopKolomBalok(string $ket_sloop_kolom_balok) Return PrasaranaLongitudinal objects filtered by the ket_sloop_kolom_balok column
 * @method array findByRusakPlesterStruktur(string $rusak_plester_struktur) Return PrasaranaLongitudinal objects filtered by the rusak_plester_struktur column
 * @method array findByKetPlesterStruktur(string $ket_plester_struktur) Return PrasaranaLongitudinal objects filtered by the ket_plester_struktur column
 * @method array findByRusakKudakudaAtap(string $rusak_kudakuda_atap) Return PrasaranaLongitudinal objects filtered by the rusak_kudakuda_atap column
 * @method array findByKetKudakudaAtap(string $ket_kudakuda_atap) Return PrasaranaLongitudinal objects filtered by the ket_kudakuda_atap column
 * @method array findByRusakKasoAtap(string $rusak_kaso_atap) Return PrasaranaLongitudinal objects filtered by the rusak_kaso_atap column
 * @method array findByKetKasoAtap(string $ket_kaso_atap) Return PrasaranaLongitudinal objects filtered by the ket_kaso_atap column
 * @method array findByRusakRengAtap(string $rusak_reng_atap) Return PrasaranaLongitudinal objects filtered by the rusak_reng_atap column
 * @method array findByKetRengAtap(string $ket_reng_atap) Return PrasaranaLongitudinal objects filtered by the ket_reng_atap column
 * @method array findByRusakTutupAtap(string $rusak_tutup_atap) Return PrasaranaLongitudinal objects filtered by the rusak_tutup_atap column
 * @method array findByKetTutupAtap(string $ket_tutup_atap) Return PrasaranaLongitudinal objects filtered by the ket_tutup_atap column
 * @method array findByRusakLisplangTalang(string $rusak_lisplang_talang) Return PrasaranaLongitudinal objects filtered by the rusak_lisplang_talang column
 * @method array findByKetLisplangTalang(string $ket_lisplang_talang) Return PrasaranaLongitudinal objects filtered by the ket_lisplang_talang column
 * @method array findByRusakRangkaPlafon(string $rusak_rangka_plafon) Return PrasaranaLongitudinal objects filtered by the rusak_rangka_plafon column
 * @method array findByKetRangkaPlafon(string $ket_rangka_plafon) Return PrasaranaLongitudinal objects filtered by the ket_rangka_plafon column
 * @method array findByRusakTutupPlafon(string $rusak_tutup_plafon) Return PrasaranaLongitudinal objects filtered by the rusak_tutup_plafon column
 * @method array findByKetTutupPlafon(string $ket_tutup_plafon) Return PrasaranaLongitudinal objects filtered by the ket_tutup_plafon column
 * @method array findByRusakBataDinding(string $rusak_bata_dinding) Return PrasaranaLongitudinal objects filtered by the rusak_bata_dinding column
 * @method array findByKetBataDinding(string $ket_bata_dinding) Return PrasaranaLongitudinal objects filtered by the ket_bata_dinding column
 * @method array findByRusakPlesterDinding(string $rusak_plester_dinding) Return PrasaranaLongitudinal objects filtered by the rusak_plester_dinding column
 * @method array findByKetPlesterDinding(string $ket_plester_dinding) Return PrasaranaLongitudinal objects filtered by the ket_plester_dinding column
 * @method array findByRusakDaunJendela(string $rusak_daun_jendela) Return PrasaranaLongitudinal objects filtered by the rusak_daun_jendela column
 * @method array findByKetDaunJendela(string $ket_daun_jendela) Return PrasaranaLongitudinal objects filtered by the ket_daun_jendela column
 * @method array findByRusakDaunPintu(string $rusak_daun_pintu) Return PrasaranaLongitudinal objects filtered by the rusak_daun_pintu column
 * @method array findByKetDaunPintu(string $ket_daun_pintu) Return PrasaranaLongitudinal objects filtered by the ket_daun_pintu column
 * @method array findByRusakKusen(string $rusak_kusen) Return PrasaranaLongitudinal objects filtered by the rusak_kusen column
 * @method array findByKetKusen(string $ket_kusen) Return PrasaranaLongitudinal objects filtered by the ket_kusen column
 * @method array findByRusakTutupLantai(string $rusak_tutup_lantai) Return PrasaranaLongitudinal objects filtered by the rusak_tutup_lantai column
 * @method array findByKetPenutupLantai(string $ket_penutup_lantai) Return PrasaranaLongitudinal objects filtered by the ket_penutup_lantai column
 * @method array findByRusakInstListrik(string $rusak_inst_listrik) Return PrasaranaLongitudinal objects filtered by the rusak_inst_listrik column
 * @method array findByKetInstListrik(string $ket_inst_listrik) Return PrasaranaLongitudinal objects filtered by the ket_inst_listrik column
 * @method array findByRusakInstAir(string $rusak_inst_air) Return PrasaranaLongitudinal objects filtered by the rusak_inst_air column
 * @method array findByKetInstAir(string $ket_inst_air) Return PrasaranaLongitudinal objects filtered by the ket_inst_air column
 * @method array findByRusakDrainase(string $rusak_drainase) Return PrasaranaLongitudinal objects filtered by the rusak_drainase column
 * @method array findByKetDrainase(string $ket_drainase) Return PrasaranaLongitudinal objects filtered by the ket_drainase column
 * @method array findByRusakFinishStruktur(string $rusak_finish_struktur) Return PrasaranaLongitudinal objects filtered by the rusak_finish_struktur column
 * @method array findByKetFinishStruktur(string $ket_finish_struktur) Return PrasaranaLongitudinal objects filtered by the ket_finish_struktur column
 * @method array findByRusakFinishPlafon(string $rusak_finish_plafon) Return PrasaranaLongitudinal objects filtered by the rusak_finish_plafon column
 * @method array findByKetFinishPlafon(string $ket_finish_plafon) Return PrasaranaLongitudinal objects filtered by the ket_finish_plafon column
 * @method array findByRusakFinishDinding(string $rusak_finish_dinding) Return PrasaranaLongitudinal objects filtered by the rusak_finish_dinding column
 * @method array findByKetFinishDinding(string $ket_finish_dinding) Return PrasaranaLongitudinal objects filtered by the ket_finish_dinding column
 * @method array findByRusakFinishKpj(string $rusak_finish_kpj) Return PrasaranaLongitudinal objects filtered by the rusak_finish_kpj column
 * @method array findByKetFinishKpj(string $ket_finish_kpj) Return PrasaranaLongitudinal objects filtered by the ket_finish_kpj column
 * @method array findByBerfungsi(string $berfungsi) Return PrasaranaLongitudinal objects filtered by the berfungsi column
 * @method array findByBlobId(string $blob_id) Return PrasaranaLongitudinal objects filtered by the blob_id column
 * @method array findByLastUpdate(string $Last_update) Return PrasaranaLongitudinal objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return PrasaranaLongitudinal objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return PrasaranaLongitudinal objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return PrasaranaLongitudinal objects filtered by the Updater_ID column
 *
 * @package    propel.generator.Admin.Model.om
 */
abstract class BasePrasaranaLongitudinalQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasePrasaranaLongitudinalQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodik_Paudni', $modelName = 'Admin\\Model\\PrasaranaLongitudinal', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new PrasaranaLongitudinalQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   PrasaranaLongitudinalQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return PrasaranaLongitudinalQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof PrasaranaLongitudinalQuery) {
            return $criteria;
        }
        $query = new PrasaranaLongitudinalQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$prasarana_id, $semester_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   PrasaranaLongitudinal|PrasaranaLongitudinal[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PrasaranaLongitudinalPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 PrasaranaLongitudinal A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [prasarana_id], [semester_id], [rusak_pondasi], [ket_pondasi], [rusak_sloop_kolom_balok], [ket_sloop_kolom_balok], [rusak_plester_struktur], [ket_plester_struktur], [rusak_kudakuda_atap], [ket_kudakuda_atap], [rusak_kaso_atap], [ket_kaso_atap], [rusak_reng_atap], [ket_reng_atap], [rusak_tutup_atap], [ket_tutup_atap], [rusak_lisplang_talang], [ket_lisplang_talang], [rusak_rangka_plafon], [ket_rangka_plafon], [rusak_tutup_plafon], [ket_tutup_plafon], [rusak_bata_dinding], [ket_bata_dinding], [rusak_plester_dinding], [ket_plester_dinding], [rusak_daun_jendela], [ket_daun_jendela], [rusak_daun_pintu], [ket_daun_pintu], [rusak_kusen], [ket_kusen], [rusak_tutup_lantai], [ket_penutup_lantai], [rusak_inst_listrik], [ket_inst_listrik], [rusak_inst_air], [ket_inst_air], [rusak_drainase], [ket_drainase], [rusak_finish_struktur], [ket_finish_struktur], [rusak_finish_plafon], [ket_finish_plafon], [rusak_finish_dinding], [ket_finish_dinding], [rusak_finish_kpj], [ket_finish_kpj], [berfungsi], [blob_id], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [prasarana_longitudinal] WHERE [prasarana_id] = :p0 AND [semester_id] = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new PrasaranaLongitudinal();
            $obj->hydrate($row);
            PrasaranaLongitudinalPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return PrasaranaLongitudinal|PrasaranaLongitudinal[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|PrasaranaLongitudinal[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(PrasaranaLongitudinalPeer::PRASARANA_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(PrasaranaLongitudinalPeer::SEMESTER_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(PrasaranaLongitudinalPeer::PRASARANA_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(PrasaranaLongitudinalPeer::SEMESTER_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the prasarana_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPrasaranaId('fooValue');   // WHERE prasarana_id = 'fooValue'
     * $query->filterByPrasaranaId('%fooValue%'); // WHERE prasarana_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prasaranaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByPrasaranaId($prasaranaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prasaranaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prasaranaId)) {
                $prasaranaId = str_replace('*', '%', $prasaranaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::PRASARANA_ID, $prasaranaId, $comparison);
    }

    /**
     * Filter the query on the semester_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySemesterId('fooValue');   // WHERE semester_id = 'fooValue'
     * $query->filterBySemesterId('%fooValue%'); // WHERE semester_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $semesterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySemesterId($semesterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($semesterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $semesterId)) {
                $semesterId = str_replace('*', '%', $semesterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::SEMESTER_ID, $semesterId, $comparison);
    }

    /**
     * Filter the query on the rusak_pondasi column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakPondasi(1234); // WHERE rusak_pondasi = 1234
     * $query->filterByRusakPondasi(array(12, 34)); // WHERE rusak_pondasi IN (12, 34)
     * $query->filterByRusakPondasi(array('min' => 12)); // WHERE rusak_pondasi >= 12
     * $query->filterByRusakPondasi(array('max' => 12)); // WHERE rusak_pondasi <= 12
     * </code>
     *
     * @param     mixed $rusakPondasi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakPondasi($rusakPondasi = null, $comparison = null)
    {
        if (is_array($rusakPondasi)) {
            $useMinMax = false;
            if (isset($rusakPondasi['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PONDASI, $rusakPondasi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakPondasi['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PONDASI, $rusakPondasi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PONDASI, $rusakPondasi, $comparison);
    }

    /**
     * Filter the query on the ket_pondasi column
     *
     * Example usage:
     * <code>
     * $query->filterByKetPondasi('fooValue');   // WHERE ket_pondasi = 'fooValue'
     * $query->filterByKetPondasi('%fooValue%'); // WHERE ket_pondasi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketPondasi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetPondasi($ketPondasi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketPondasi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketPondasi)) {
                $ketPondasi = str_replace('*', '%', $ketPondasi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_PONDASI, $ketPondasi, $comparison);
    }

    /**
     * Filter the query on the rusak_sloop_kolom_balok column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakSloopKolomBalok(1234); // WHERE rusak_sloop_kolom_balok = 1234
     * $query->filterByRusakSloopKolomBalok(array(12, 34)); // WHERE rusak_sloop_kolom_balok IN (12, 34)
     * $query->filterByRusakSloopKolomBalok(array('min' => 12)); // WHERE rusak_sloop_kolom_balok >= 12
     * $query->filterByRusakSloopKolomBalok(array('max' => 12)); // WHERE rusak_sloop_kolom_balok <= 12
     * </code>
     *
     * @param     mixed $rusakSloopKolomBalok The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakSloopKolomBalok($rusakSloopKolomBalok = null, $comparison = null)
    {
        if (is_array($rusakSloopKolomBalok)) {
            $useMinMax = false;
            if (isset($rusakSloopKolomBalok['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_SLOOP_KOLOM_BALOK, $rusakSloopKolomBalok['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakSloopKolomBalok['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_SLOOP_KOLOM_BALOK, $rusakSloopKolomBalok['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_SLOOP_KOLOM_BALOK, $rusakSloopKolomBalok, $comparison);
    }

    /**
     * Filter the query on the ket_sloop_kolom_balok column
     *
     * Example usage:
     * <code>
     * $query->filterByKetSloopKolomBalok('fooValue');   // WHERE ket_sloop_kolom_balok = 'fooValue'
     * $query->filterByKetSloopKolomBalok('%fooValue%'); // WHERE ket_sloop_kolom_balok LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketSloopKolomBalok The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetSloopKolomBalok($ketSloopKolomBalok = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketSloopKolomBalok)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketSloopKolomBalok)) {
                $ketSloopKolomBalok = str_replace('*', '%', $ketSloopKolomBalok);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_SLOOP_KOLOM_BALOK, $ketSloopKolomBalok, $comparison);
    }

    /**
     * Filter the query on the rusak_plester_struktur column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakPlesterStruktur(1234); // WHERE rusak_plester_struktur = 1234
     * $query->filterByRusakPlesterStruktur(array(12, 34)); // WHERE rusak_plester_struktur IN (12, 34)
     * $query->filterByRusakPlesterStruktur(array('min' => 12)); // WHERE rusak_plester_struktur >= 12
     * $query->filterByRusakPlesterStruktur(array('max' => 12)); // WHERE rusak_plester_struktur <= 12
     * </code>
     *
     * @param     mixed $rusakPlesterStruktur The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakPlesterStruktur($rusakPlesterStruktur = null, $comparison = null)
    {
        if (is_array($rusakPlesterStruktur)) {
            $useMinMax = false;
            if (isset($rusakPlesterStruktur['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PLESTER_STRUKTUR, $rusakPlesterStruktur['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakPlesterStruktur['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PLESTER_STRUKTUR, $rusakPlesterStruktur['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PLESTER_STRUKTUR, $rusakPlesterStruktur, $comparison);
    }

    /**
     * Filter the query on the ket_plester_struktur column
     *
     * Example usage:
     * <code>
     * $query->filterByKetPlesterStruktur('fooValue');   // WHERE ket_plester_struktur = 'fooValue'
     * $query->filterByKetPlesterStruktur('%fooValue%'); // WHERE ket_plester_struktur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketPlesterStruktur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetPlesterStruktur($ketPlesterStruktur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketPlesterStruktur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketPlesterStruktur)) {
                $ketPlesterStruktur = str_replace('*', '%', $ketPlesterStruktur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_PLESTER_STRUKTUR, $ketPlesterStruktur, $comparison);
    }

    /**
     * Filter the query on the rusak_kudakuda_atap column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakKudakudaAtap(1234); // WHERE rusak_kudakuda_atap = 1234
     * $query->filterByRusakKudakudaAtap(array(12, 34)); // WHERE rusak_kudakuda_atap IN (12, 34)
     * $query->filterByRusakKudakudaAtap(array('min' => 12)); // WHERE rusak_kudakuda_atap >= 12
     * $query->filterByRusakKudakudaAtap(array('max' => 12)); // WHERE rusak_kudakuda_atap <= 12
     * </code>
     *
     * @param     mixed $rusakKudakudaAtap The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakKudakudaAtap($rusakKudakudaAtap = null, $comparison = null)
    {
        if (is_array($rusakKudakudaAtap)) {
            $useMinMax = false;
            if (isset($rusakKudakudaAtap['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KUDAKUDA_ATAP, $rusakKudakudaAtap['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakKudakudaAtap['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KUDAKUDA_ATAP, $rusakKudakudaAtap['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KUDAKUDA_ATAP, $rusakKudakudaAtap, $comparison);
    }

    /**
     * Filter the query on the ket_kudakuda_atap column
     *
     * Example usage:
     * <code>
     * $query->filterByKetKudakudaAtap('fooValue');   // WHERE ket_kudakuda_atap = 'fooValue'
     * $query->filterByKetKudakudaAtap('%fooValue%'); // WHERE ket_kudakuda_atap LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketKudakudaAtap The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetKudakudaAtap($ketKudakudaAtap = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketKudakudaAtap)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketKudakudaAtap)) {
                $ketKudakudaAtap = str_replace('*', '%', $ketKudakudaAtap);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_KUDAKUDA_ATAP, $ketKudakudaAtap, $comparison);
    }

    /**
     * Filter the query on the rusak_kaso_atap column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakKasoAtap(1234); // WHERE rusak_kaso_atap = 1234
     * $query->filterByRusakKasoAtap(array(12, 34)); // WHERE rusak_kaso_atap IN (12, 34)
     * $query->filterByRusakKasoAtap(array('min' => 12)); // WHERE rusak_kaso_atap >= 12
     * $query->filterByRusakKasoAtap(array('max' => 12)); // WHERE rusak_kaso_atap <= 12
     * </code>
     *
     * @param     mixed $rusakKasoAtap The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakKasoAtap($rusakKasoAtap = null, $comparison = null)
    {
        if (is_array($rusakKasoAtap)) {
            $useMinMax = false;
            if (isset($rusakKasoAtap['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KASO_ATAP, $rusakKasoAtap['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakKasoAtap['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KASO_ATAP, $rusakKasoAtap['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KASO_ATAP, $rusakKasoAtap, $comparison);
    }

    /**
     * Filter the query on the ket_kaso_atap column
     *
     * Example usage:
     * <code>
     * $query->filterByKetKasoAtap('fooValue');   // WHERE ket_kaso_atap = 'fooValue'
     * $query->filterByKetKasoAtap('%fooValue%'); // WHERE ket_kaso_atap LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketKasoAtap The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetKasoAtap($ketKasoAtap = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketKasoAtap)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketKasoAtap)) {
                $ketKasoAtap = str_replace('*', '%', $ketKasoAtap);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_KASO_ATAP, $ketKasoAtap, $comparison);
    }

    /**
     * Filter the query on the rusak_reng_atap column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakRengAtap(1234); // WHERE rusak_reng_atap = 1234
     * $query->filterByRusakRengAtap(array(12, 34)); // WHERE rusak_reng_atap IN (12, 34)
     * $query->filterByRusakRengAtap(array('min' => 12)); // WHERE rusak_reng_atap >= 12
     * $query->filterByRusakRengAtap(array('max' => 12)); // WHERE rusak_reng_atap <= 12
     * </code>
     *
     * @param     mixed $rusakRengAtap The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakRengAtap($rusakRengAtap = null, $comparison = null)
    {
        if (is_array($rusakRengAtap)) {
            $useMinMax = false;
            if (isset($rusakRengAtap['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_RENG_ATAP, $rusakRengAtap['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakRengAtap['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_RENG_ATAP, $rusakRengAtap['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_RENG_ATAP, $rusakRengAtap, $comparison);
    }

    /**
     * Filter the query on the ket_reng_atap column
     *
     * Example usage:
     * <code>
     * $query->filterByKetRengAtap('fooValue');   // WHERE ket_reng_atap = 'fooValue'
     * $query->filterByKetRengAtap('%fooValue%'); // WHERE ket_reng_atap LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketRengAtap The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetRengAtap($ketRengAtap = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketRengAtap)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketRengAtap)) {
                $ketRengAtap = str_replace('*', '%', $ketRengAtap);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_RENG_ATAP, $ketRengAtap, $comparison);
    }

    /**
     * Filter the query on the rusak_tutup_atap column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakTutupAtap(1234); // WHERE rusak_tutup_atap = 1234
     * $query->filterByRusakTutupAtap(array(12, 34)); // WHERE rusak_tutup_atap IN (12, 34)
     * $query->filterByRusakTutupAtap(array('min' => 12)); // WHERE rusak_tutup_atap >= 12
     * $query->filterByRusakTutupAtap(array('max' => 12)); // WHERE rusak_tutup_atap <= 12
     * </code>
     *
     * @param     mixed $rusakTutupAtap The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakTutupAtap($rusakTutupAtap = null, $comparison = null)
    {
        if (is_array($rusakTutupAtap)) {
            $useMinMax = false;
            if (isset($rusakTutupAtap['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_TUTUP_ATAP, $rusakTutupAtap['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakTutupAtap['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_TUTUP_ATAP, $rusakTutupAtap['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_TUTUP_ATAP, $rusakTutupAtap, $comparison);
    }

    /**
     * Filter the query on the ket_tutup_atap column
     *
     * Example usage:
     * <code>
     * $query->filterByKetTutupAtap('fooValue');   // WHERE ket_tutup_atap = 'fooValue'
     * $query->filterByKetTutupAtap('%fooValue%'); // WHERE ket_tutup_atap LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketTutupAtap The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetTutupAtap($ketTutupAtap = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketTutupAtap)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketTutupAtap)) {
                $ketTutupAtap = str_replace('*', '%', $ketTutupAtap);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_TUTUP_ATAP, $ketTutupAtap, $comparison);
    }

    /**
     * Filter the query on the rusak_lisplang_talang column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakLisplangTalang(1234); // WHERE rusak_lisplang_talang = 1234
     * $query->filterByRusakLisplangTalang(array(12, 34)); // WHERE rusak_lisplang_talang IN (12, 34)
     * $query->filterByRusakLisplangTalang(array('min' => 12)); // WHERE rusak_lisplang_talang >= 12
     * $query->filterByRusakLisplangTalang(array('max' => 12)); // WHERE rusak_lisplang_talang <= 12
     * </code>
     *
     * @param     mixed $rusakLisplangTalang The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakLisplangTalang($rusakLisplangTalang = null, $comparison = null)
    {
        if (is_array($rusakLisplangTalang)) {
            $useMinMax = false;
            if (isset($rusakLisplangTalang['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG, $rusakLisplangTalang['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakLisplangTalang['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG, $rusakLisplangTalang['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG, $rusakLisplangTalang, $comparison);
    }

    /**
     * Filter the query on the ket_lisplang_talang column
     *
     * Example usage:
     * <code>
     * $query->filterByKetLisplangTalang('fooValue');   // WHERE ket_lisplang_talang = 'fooValue'
     * $query->filterByKetLisplangTalang('%fooValue%'); // WHERE ket_lisplang_talang LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketLisplangTalang The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetLisplangTalang($ketLisplangTalang = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketLisplangTalang)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketLisplangTalang)) {
                $ketLisplangTalang = str_replace('*', '%', $ketLisplangTalang);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_LISPLANG_TALANG, $ketLisplangTalang, $comparison);
    }

    /**
     * Filter the query on the rusak_rangka_plafon column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakRangkaPlafon(1234); // WHERE rusak_rangka_plafon = 1234
     * $query->filterByRusakRangkaPlafon(array(12, 34)); // WHERE rusak_rangka_plafon IN (12, 34)
     * $query->filterByRusakRangkaPlafon(array('min' => 12)); // WHERE rusak_rangka_plafon >= 12
     * $query->filterByRusakRangkaPlafon(array('max' => 12)); // WHERE rusak_rangka_plafon <= 12
     * </code>
     *
     * @param     mixed $rusakRangkaPlafon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakRangkaPlafon($rusakRangkaPlafon = null, $comparison = null)
    {
        if (is_array($rusakRangkaPlafon)) {
            $useMinMax = false;
            if (isset($rusakRangkaPlafon['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON, $rusakRangkaPlafon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakRangkaPlafon['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON, $rusakRangkaPlafon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON, $rusakRangkaPlafon, $comparison);
    }

    /**
     * Filter the query on the ket_rangka_plafon column
     *
     * Example usage:
     * <code>
     * $query->filterByKetRangkaPlafon('fooValue');   // WHERE ket_rangka_plafon = 'fooValue'
     * $query->filterByKetRangkaPlafon('%fooValue%'); // WHERE ket_rangka_plafon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketRangkaPlafon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetRangkaPlafon($ketRangkaPlafon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketRangkaPlafon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketRangkaPlafon)) {
                $ketRangkaPlafon = str_replace('*', '%', $ketRangkaPlafon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_RANGKA_PLAFON, $ketRangkaPlafon, $comparison);
    }

    /**
     * Filter the query on the rusak_tutup_plafon column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakTutupPlafon(1234); // WHERE rusak_tutup_plafon = 1234
     * $query->filterByRusakTutupPlafon(array(12, 34)); // WHERE rusak_tutup_plafon IN (12, 34)
     * $query->filterByRusakTutupPlafon(array('min' => 12)); // WHERE rusak_tutup_plafon >= 12
     * $query->filterByRusakTutupPlafon(array('max' => 12)); // WHERE rusak_tutup_plafon <= 12
     * </code>
     *
     * @param     mixed $rusakTutupPlafon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakTutupPlafon($rusakTutupPlafon = null, $comparison = null)
    {
        if (is_array($rusakTutupPlafon)) {
            $useMinMax = false;
            if (isset($rusakTutupPlafon['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_TUTUP_PLAFON, $rusakTutupPlafon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakTutupPlafon['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_TUTUP_PLAFON, $rusakTutupPlafon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_TUTUP_PLAFON, $rusakTutupPlafon, $comparison);
    }

    /**
     * Filter the query on the ket_tutup_plafon column
     *
     * Example usage:
     * <code>
     * $query->filterByKetTutupPlafon('fooValue');   // WHERE ket_tutup_plafon = 'fooValue'
     * $query->filterByKetTutupPlafon('%fooValue%'); // WHERE ket_tutup_plafon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketTutupPlafon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetTutupPlafon($ketTutupPlafon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketTutupPlafon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketTutupPlafon)) {
                $ketTutupPlafon = str_replace('*', '%', $ketTutupPlafon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_TUTUP_PLAFON, $ketTutupPlafon, $comparison);
    }

    /**
     * Filter the query on the rusak_bata_dinding column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakBataDinding(1234); // WHERE rusak_bata_dinding = 1234
     * $query->filterByRusakBataDinding(array(12, 34)); // WHERE rusak_bata_dinding IN (12, 34)
     * $query->filterByRusakBataDinding(array('min' => 12)); // WHERE rusak_bata_dinding >= 12
     * $query->filterByRusakBataDinding(array('max' => 12)); // WHERE rusak_bata_dinding <= 12
     * </code>
     *
     * @param     mixed $rusakBataDinding The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakBataDinding($rusakBataDinding = null, $comparison = null)
    {
        if (is_array($rusakBataDinding)) {
            $useMinMax = false;
            if (isset($rusakBataDinding['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_BATA_DINDING, $rusakBataDinding['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakBataDinding['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_BATA_DINDING, $rusakBataDinding['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_BATA_DINDING, $rusakBataDinding, $comparison);
    }

    /**
     * Filter the query on the ket_bata_dinding column
     *
     * Example usage:
     * <code>
     * $query->filterByKetBataDinding('fooValue');   // WHERE ket_bata_dinding = 'fooValue'
     * $query->filterByKetBataDinding('%fooValue%'); // WHERE ket_bata_dinding LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketBataDinding The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetBataDinding($ketBataDinding = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketBataDinding)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketBataDinding)) {
                $ketBataDinding = str_replace('*', '%', $ketBataDinding);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_BATA_DINDING, $ketBataDinding, $comparison);
    }

    /**
     * Filter the query on the rusak_plester_dinding column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakPlesterDinding(1234); // WHERE rusak_plester_dinding = 1234
     * $query->filterByRusakPlesterDinding(array(12, 34)); // WHERE rusak_plester_dinding IN (12, 34)
     * $query->filterByRusakPlesterDinding(array('min' => 12)); // WHERE rusak_plester_dinding >= 12
     * $query->filterByRusakPlesterDinding(array('max' => 12)); // WHERE rusak_plester_dinding <= 12
     * </code>
     *
     * @param     mixed $rusakPlesterDinding The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakPlesterDinding($rusakPlesterDinding = null, $comparison = null)
    {
        if (is_array($rusakPlesterDinding)) {
            $useMinMax = false;
            if (isset($rusakPlesterDinding['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PLESTER_DINDING, $rusakPlesterDinding['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakPlesterDinding['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PLESTER_DINDING, $rusakPlesterDinding['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_PLESTER_DINDING, $rusakPlesterDinding, $comparison);
    }

    /**
     * Filter the query on the ket_plester_dinding column
     *
     * Example usage:
     * <code>
     * $query->filterByKetPlesterDinding('fooValue');   // WHERE ket_plester_dinding = 'fooValue'
     * $query->filterByKetPlesterDinding('%fooValue%'); // WHERE ket_plester_dinding LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketPlesterDinding The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetPlesterDinding($ketPlesterDinding = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketPlesterDinding)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketPlesterDinding)) {
                $ketPlesterDinding = str_replace('*', '%', $ketPlesterDinding);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_PLESTER_DINDING, $ketPlesterDinding, $comparison);
    }

    /**
     * Filter the query on the rusak_daun_jendela column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakDaunJendela(1234); // WHERE rusak_daun_jendela = 1234
     * $query->filterByRusakDaunJendela(array(12, 34)); // WHERE rusak_daun_jendela IN (12, 34)
     * $query->filterByRusakDaunJendela(array('min' => 12)); // WHERE rusak_daun_jendela >= 12
     * $query->filterByRusakDaunJendela(array('max' => 12)); // WHERE rusak_daun_jendela <= 12
     * </code>
     *
     * @param     mixed $rusakDaunJendela The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakDaunJendela($rusakDaunJendela = null, $comparison = null)
    {
        if (is_array($rusakDaunJendela)) {
            $useMinMax = false;
            if (isset($rusakDaunJendela['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA, $rusakDaunJendela['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakDaunJendela['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA, $rusakDaunJendela['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA, $rusakDaunJendela, $comparison);
    }

    /**
     * Filter the query on the ket_daun_jendela column
     *
     * Example usage:
     * <code>
     * $query->filterByKetDaunJendela('fooValue');   // WHERE ket_daun_jendela = 'fooValue'
     * $query->filterByKetDaunJendela('%fooValue%'); // WHERE ket_daun_jendela LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketDaunJendela The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetDaunJendela($ketDaunJendela = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketDaunJendela)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketDaunJendela)) {
                $ketDaunJendela = str_replace('*', '%', $ketDaunJendela);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_DAUN_JENDELA, $ketDaunJendela, $comparison);
    }

    /**
     * Filter the query on the rusak_daun_pintu column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakDaunPintu(1234); // WHERE rusak_daun_pintu = 1234
     * $query->filterByRusakDaunPintu(array(12, 34)); // WHERE rusak_daun_pintu IN (12, 34)
     * $query->filterByRusakDaunPintu(array('min' => 12)); // WHERE rusak_daun_pintu >= 12
     * $query->filterByRusakDaunPintu(array('max' => 12)); // WHERE rusak_daun_pintu <= 12
     * </code>
     *
     * @param     mixed $rusakDaunPintu The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakDaunPintu($rusakDaunPintu = null, $comparison = null)
    {
        if (is_array($rusakDaunPintu)) {
            $useMinMax = false;
            if (isset($rusakDaunPintu['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU, $rusakDaunPintu['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakDaunPintu['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU, $rusakDaunPintu['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU, $rusakDaunPintu, $comparison);
    }

    /**
     * Filter the query on the ket_daun_pintu column
     *
     * Example usage:
     * <code>
     * $query->filterByKetDaunPintu('fooValue');   // WHERE ket_daun_pintu = 'fooValue'
     * $query->filterByKetDaunPintu('%fooValue%'); // WHERE ket_daun_pintu LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketDaunPintu The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetDaunPintu($ketDaunPintu = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketDaunPintu)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketDaunPintu)) {
                $ketDaunPintu = str_replace('*', '%', $ketDaunPintu);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_DAUN_PINTU, $ketDaunPintu, $comparison);
    }

    /**
     * Filter the query on the rusak_kusen column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakKusen(1234); // WHERE rusak_kusen = 1234
     * $query->filterByRusakKusen(array(12, 34)); // WHERE rusak_kusen IN (12, 34)
     * $query->filterByRusakKusen(array('min' => 12)); // WHERE rusak_kusen >= 12
     * $query->filterByRusakKusen(array('max' => 12)); // WHERE rusak_kusen <= 12
     * </code>
     *
     * @param     mixed $rusakKusen The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakKusen($rusakKusen = null, $comparison = null)
    {
        if (is_array($rusakKusen)) {
            $useMinMax = false;
            if (isset($rusakKusen['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KUSEN, $rusakKusen['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakKusen['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KUSEN, $rusakKusen['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_KUSEN, $rusakKusen, $comparison);
    }

    /**
     * Filter the query on the ket_kusen column
     *
     * Example usage:
     * <code>
     * $query->filterByKetKusen('fooValue');   // WHERE ket_kusen = 'fooValue'
     * $query->filterByKetKusen('%fooValue%'); // WHERE ket_kusen LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketKusen The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetKusen($ketKusen = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketKusen)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketKusen)) {
                $ketKusen = str_replace('*', '%', $ketKusen);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_KUSEN, $ketKusen, $comparison);
    }

    /**
     * Filter the query on the rusak_tutup_lantai column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakTutupLantai(1234); // WHERE rusak_tutup_lantai = 1234
     * $query->filterByRusakTutupLantai(array(12, 34)); // WHERE rusak_tutup_lantai IN (12, 34)
     * $query->filterByRusakTutupLantai(array('min' => 12)); // WHERE rusak_tutup_lantai >= 12
     * $query->filterByRusakTutupLantai(array('max' => 12)); // WHERE rusak_tutup_lantai <= 12
     * </code>
     *
     * @param     mixed $rusakTutupLantai The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakTutupLantai($rusakTutupLantai = null, $comparison = null)
    {
        if (is_array($rusakTutupLantai)) {
            $useMinMax = false;
            if (isset($rusakTutupLantai['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_TUTUP_LANTAI, $rusakTutupLantai['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakTutupLantai['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_TUTUP_LANTAI, $rusakTutupLantai['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_TUTUP_LANTAI, $rusakTutupLantai, $comparison);
    }

    /**
     * Filter the query on the ket_penutup_lantai column
     *
     * Example usage:
     * <code>
     * $query->filterByKetPenutupLantai('fooValue');   // WHERE ket_penutup_lantai = 'fooValue'
     * $query->filterByKetPenutupLantai('%fooValue%'); // WHERE ket_penutup_lantai LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketPenutupLantai The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetPenutupLantai($ketPenutupLantai = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketPenutupLantai)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketPenutupLantai)) {
                $ketPenutupLantai = str_replace('*', '%', $ketPenutupLantai);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_PENUTUP_LANTAI, $ketPenutupLantai, $comparison);
    }

    /**
     * Filter the query on the rusak_inst_listrik column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakInstListrik(1234); // WHERE rusak_inst_listrik = 1234
     * $query->filterByRusakInstListrik(array(12, 34)); // WHERE rusak_inst_listrik IN (12, 34)
     * $query->filterByRusakInstListrik(array('min' => 12)); // WHERE rusak_inst_listrik >= 12
     * $query->filterByRusakInstListrik(array('max' => 12)); // WHERE rusak_inst_listrik <= 12
     * </code>
     *
     * @param     mixed $rusakInstListrik The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakInstListrik($rusakInstListrik = null, $comparison = null)
    {
        if (is_array($rusakInstListrik)) {
            $useMinMax = false;
            if (isset($rusakInstListrik['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_INST_LISTRIK, $rusakInstListrik['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakInstListrik['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_INST_LISTRIK, $rusakInstListrik['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_INST_LISTRIK, $rusakInstListrik, $comparison);
    }

    /**
     * Filter the query on the ket_inst_listrik column
     *
     * Example usage:
     * <code>
     * $query->filterByKetInstListrik('fooValue');   // WHERE ket_inst_listrik = 'fooValue'
     * $query->filterByKetInstListrik('%fooValue%'); // WHERE ket_inst_listrik LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketInstListrik The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetInstListrik($ketInstListrik = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketInstListrik)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketInstListrik)) {
                $ketInstListrik = str_replace('*', '%', $ketInstListrik);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_INST_LISTRIK, $ketInstListrik, $comparison);
    }

    /**
     * Filter the query on the rusak_inst_air column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakInstAir(1234); // WHERE rusak_inst_air = 1234
     * $query->filterByRusakInstAir(array(12, 34)); // WHERE rusak_inst_air IN (12, 34)
     * $query->filterByRusakInstAir(array('min' => 12)); // WHERE rusak_inst_air >= 12
     * $query->filterByRusakInstAir(array('max' => 12)); // WHERE rusak_inst_air <= 12
     * </code>
     *
     * @param     mixed $rusakInstAir The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakInstAir($rusakInstAir = null, $comparison = null)
    {
        if (is_array($rusakInstAir)) {
            $useMinMax = false;
            if (isset($rusakInstAir['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_INST_AIR, $rusakInstAir['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakInstAir['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_INST_AIR, $rusakInstAir['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_INST_AIR, $rusakInstAir, $comparison);
    }

    /**
     * Filter the query on the ket_inst_air column
     *
     * Example usage:
     * <code>
     * $query->filterByKetInstAir('fooValue');   // WHERE ket_inst_air = 'fooValue'
     * $query->filterByKetInstAir('%fooValue%'); // WHERE ket_inst_air LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketInstAir The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetInstAir($ketInstAir = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketInstAir)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketInstAir)) {
                $ketInstAir = str_replace('*', '%', $ketInstAir);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_INST_AIR, $ketInstAir, $comparison);
    }

    /**
     * Filter the query on the rusak_drainase column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakDrainase(1234); // WHERE rusak_drainase = 1234
     * $query->filterByRusakDrainase(array(12, 34)); // WHERE rusak_drainase IN (12, 34)
     * $query->filterByRusakDrainase(array('min' => 12)); // WHERE rusak_drainase >= 12
     * $query->filterByRusakDrainase(array('max' => 12)); // WHERE rusak_drainase <= 12
     * </code>
     *
     * @param     mixed $rusakDrainase The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakDrainase($rusakDrainase = null, $comparison = null)
    {
        if (is_array($rusakDrainase)) {
            $useMinMax = false;
            if (isset($rusakDrainase['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DRAINASE, $rusakDrainase['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakDrainase['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DRAINASE, $rusakDrainase['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_DRAINASE, $rusakDrainase, $comparison);
    }

    /**
     * Filter the query on the ket_drainase column
     *
     * Example usage:
     * <code>
     * $query->filterByKetDrainase('fooValue');   // WHERE ket_drainase = 'fooValue'
     * $query->filterByKetDrainase('%fooValue%'); // WHERE ket_drainase LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketDrainase The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetDrainase($ketDrainase = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketDrainase)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketDrainase)) {
                $ketDrainase = str_replace('*', '%', $ketDrainase);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_DRAINASE, $ketDrainase, $comparison);
    }

    /**
     * Filter the query on the rusak_finish_struktur column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakFinishStruktur(1234); // WHERE rusak_finish_struktur = 1234
     * $query->filterByRusakFinishStruktur(array(12, 34)); // WHERE rusak_finish_struktur IN (12, 34)
     * $query->filterByRusakFinishStruktur(array('min' => 12)); // WHERE rusak_finish_struktur >= 12
     * $query->filterByRusakFinishStruktur(array('max' => 12)); // WHERE rusak_finish_struktur <= 12
     * </code>
     *
     * @param     mixed $rusakFinishStruktur The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakFinishStruktur($rusakFinishStruktur = null, $comparison = null)
    {
        if (is_array($rusakFinishStruktur)) {
            $useMinMax = false;
            if (isset($rusakFinishStruktur['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_FINISH_STRUKTUR, $rusakFinishStruktur['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakFinishStruktur['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_FINISH_STRUKTUR, $rusakFinishStruktur['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_FINISH_STRUKTUR, $rusakFinishStruktur, $comparison);
    }

    /**
     * Filter the query on the ket_finish_struktur column
     *
     * Example usage:
     * <code>
     * $query->filterByKetFinishStruktur('fooValue');   // WHERE ket_finish_struktur = 'fooValue'
     * $query->filterByKetFinishStruktur('%fooValue%'); // WHERE ket_finish_struktur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketFinishStruktur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetFinishStruktur($ketFinishStruktur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketFinishStruktur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketFinishStruktur)) {
                $ketFinishStruktur = str_replace('*', '%', $ketFinishStruktur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_FINISH_STRUKTUR, $ketFinishStruktur, $comparison);
    }

    /**
     * Filter the query on the rusak_finish_plafon column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakFinishPlafon(1234); // WHERE rusak_finish_plafon = 1234
     * $query->filterByRusakFinishPlafon(array(12, 34)); // WHERE rusak_finish_plafon IN (12, 34)
     * $query->filterByRusakFinishPlafon(array('min' => 12)); // WHERE rusak_finish_plafon >= 12
     * $query->filterByRusakFinishPlafon(array('max' => 12)); // WHERE rusak_finish_plafon <= 12
     * </code>
     *
     * @param     mixed $rusakFinishPlafon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakFinishPlafon($rusakFinishPlafon = null, $comparison = null)
    {
        if (is_array($rusakFinishPlafon)) {
            $useMinMax = false;
            if (isset($rusakFinishPlafon['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_FINISH_PLAFON, $rusakFinishPlafon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakFinishPlafon['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_FINISH_PLAFON, $rusakFinishPlafon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_FINISH_PLAFON, $rusakFinishPlafon, $comparison);
    }

    /**
     * Filter the query on the ket_finish_plafon column
     *
     * Example usage:
     * <code>
     * $query->filterByKetFinishPlafon('fooValue');   // WHERE ket_finish_plafon = 'fooValue'
     * $query->filterByKetFinishPlafon('%fooValue%'); // WHERE ket_finish_plafon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketFinishPlafon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetFinishPlafon($ketFinishPlafon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketFinishPlafon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketFinishPlafon)) {
                $ketFinishPlafon = str_replace('*', '%', $ketFinishPlafon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_FINISH_PLAFON, $ketFinishPlafon, $comparison);
    }

    /**
     * Filter the query on the rusak_finish_dinding column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakFinishDinding(1234); // WHERE rusak_finish_dinding = 1234
     * $query->filterByRusakFinishDinding(array(12, 34)); // WHERE rusak_finish_dinding IN (12, 34)
     * $query->filterByRusakFinishDinding(array('min' => 12)); // WHERE rusak_finish_dinding >= 12
     * $query->filterByRusakFinishDinding(array('max' => 12)); // WHERE rusak_finish_dinding <= 12
     * </code>
     *
     * @param     mixed $rusakFinishDinding The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakFinishDinding($rusakFinishDinding = null, $comparison = null)
    {
        if (is_array($rusakFinishDinding)) {
            $useMinMax = false;
            if (isset($rusakFinishDinding['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_FINISH_DINDING, $rusakFinishDinding['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakFinishDinding['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_FINISH_DINDING, $rusakFinishDinding['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_FINISH_DINDING, $rusakFinishDinding, $comparison);
    }

    /**
     * Filter the query on the ket_finish_dinding column
     *
     * Example usage:
     * <code>
     * $query->filterByKetFinishDinding('fooValue');   // WHERE ket_finish_dinding = 'fooValue'
     * $query->filterByKetFinishDinding('%fooValue%'); // WHERE ket_finish_dinding LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketFinishDinding The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetFinishDinding($ketFinishDinding = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketFinishDinding)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketFinishDinding)) {
                $ketFinishDinding = str_replace('*', '%', $ketFinishDinding);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_FINISH_DINDING, $ketFinishDinding, $comparison);
    }

    /**
     * Filter the query on the rusak_finish_kpj column
     *
     * Example usage:
     * <code>
     * $query->filterByRusakFinishKpj(1234); // WHERE rusak_finish_kpj = 1234
     * $query->filterByRusakFinishKpj(array(12, 34)); // WHERE rusak_finish_kpj IN (12, 34)
     * $query->filterByRusakFinishKpj(array('min' => 12)); // WHERE rusak_finish_kpj >= 12
     * $query->filterByRusakFinishKpj(array('max' => 12)); // WHERE rusak_finish_kpj <= 12
     * </code>
     *
     * @param     mixed $rusakFinishKpj The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByRusakFinishKpj($rusakFinishKpj = null, $comparison = null)
    {
        if (is_array($rusakFinishKpj)) {
            $useMinMax = false;
            if (isset($rusakFinishKpj['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_FINISH_KPJ, $rusakFinishKpj['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rusakFinishKpj['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_FINISH_KPJ, $rusakFinishKpj['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::RUSAK_FINISH_KPJ, $rusakFinishKpj, $comparison);
    }

    /**
     * Filter the query on the ket_finish_kpj column
     *
     * Example usage:
     * <code>
     * $query->filterByKetFinishKpj('fooValue');   // WHERE ket_finish_kpj = 'fooValue'
     * $query->filterByKetFinishKpj('%fooValue%'); // WHERE ket_finish_kpj LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketFinishKpj The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByKetFinishKpj($ketFinishKpj = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketFinishKpj)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketFinishKpj)) {
                $ketFinishKpj = str_replace('*', '%', $ketFinishKpj);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::KET_FINISH_KPJ, $ketFinishKpj, $comparison);
    }

    /**
     * Filter the query on the berfungsi column
     *
     * Example usage:
     * <code>
     * $query->filterByBerfungsi(1234); // WHERE berfungsi = 1234
     * $query->filterByBerfungsi(array(12, 34)); // WHERE berfungsi IN (12, 34)
     * $query->filterByBerfungsi(array('min' => 12)); // WHERE berfungsi >= 12
     * $query->filterByBerfungsi(array('max' => 12)); // WHERE berfungsi <= 12
     * </code>
     *
     * @param     mixed $berfungsi The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByBerfungsi($berfungsi = null, $comparison = null)
    {
        if (is_array($berfungsi)) {
            $useMinMax = false;
            if (isset($berfungsi['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::BERFUNGSI, $berfungsi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($berfungsi['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::BERFUNGSI, $berfungsi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::BERFUNGSI, $berfungsi, $comparison);
    }

    /**
     * Filter the query on the blob_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBlobId('fooValue');   // WHERE blob_id = 'fooValue'
     * $query->filterByBlobId('%fooValue%'); // WHERE blob_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $blobId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByBlobId($blobId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($blobId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $blobId)) {
                $blobId = str_replace('*', '%', $blobId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::BLOB_ID, $blobId, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(PrasaranaLongitudinalPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaLongitudinalPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasarana($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(PrasaranaLongitudinalPeer::PRASARANA_ID, $prasarana->getPrasaranaId(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaLongitudinalPeer::PRASARANA_ID, $prasarana->toKeyValue('PrimaryKey', 'PrasaranaId'), $comparison);
        } else {
            throw new PropelException('filterByPrasarana() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prasarana relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function joinPrasarana($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prasarana');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prasarana');
        }

        return $this;
    }

    /**
     * Use the Prasarana relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrasarana($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prasarana', '\Admin\Model\PrasaranaQuery');
    }

    /**
     * Filter the query by a related Semester object
     *
     * @param   Semester|PropelObjectCollection $semester The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaLongitudinalQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySemester($semester, $comparison = null)
    {
        if ($semester instanceof Semester) {
            return $this
                ->addUsingAlias(PrasaranaLongitudinalPeer::SEMESTER_ID, $semester->getSemesterId(), $comparison);
        } elseif ($semester instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaLongitudinalPeer::SEMESTER_ID, $semester->toKeyValue('PrimaryKey', 'SemesterId'), $comparison);
        } else {
            throw new PropelException('filterBySemester() only accepts arguments of type Semester or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Semester relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function joinSemester($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Semester');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Semester');
        }

        return $this;
    }

    /**
     * Use the Semester relation Semester object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\SemesterQuery A secondary query class using the current class as primary query
     */
    public function useSemesterQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSemester($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Semester', '\Admin\Model\SemesterQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   PrasaranaLongitudinal $prasaranaLongitudinal Object to remove from the list of results
     *
     * @return PrasaranaLongitudinalQuery The current query, for fluid interface
     */
    public function prune($prasaranaLongitudinal = null)
    {
        if ($prasaranaLongitudinal) {
            $this->addCond('pruneCond0', $this->getAliasedColName(PrasaranaLongitudinalPeer::PRASARANA_ID), $prasaranaLongitudinal->getPrasaranaId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(PrasaranaLongitudinalPeer::SEMESTER_ID), $prasaranaLongitudinal->getSemesterId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
