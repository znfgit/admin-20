<?php

namespace Admin\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Admin\Model\BukuAlat;
use Admin\Model\Jadwal;
use Admin\Model\JenisHapusBuku;
use Admin\Model\JenisPrasarana;
use Admin\Model\Prasarana;
use Admin\Model\PrasaranaLongitudinal;
use Admin\Model\PrasaranaPeer;
use Admin\Model\PrasaranaQuery;
use Admin\Model\RombonganBelajar;
use Admin\Model\Sarana;
use Admin\Model\Sekolah;
use Admin\Model\StatusKepemilikanSarpras;
use Admin\Model\VldPrasarana;

/**
 * Base class that represents a query for the 'prasarana' table.
 *
 *
 *
 * @method PrasaranaQuery orderByPrasaranaId($order = Criteria::ASC) Order by the prasarana_id column
 * @method PrasaranaQuery orderByJenisPrasaranaId($order = Criteria::ASC) Order by the jenis_prasarana_id column
 * @method PrasaranaQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method PrasaranaQuery orderByIdIndukPrasarana($order = Criteria::ASC) Order by the id_induk_prasarana column
 * @method PrasaranaQuery orderByKepemilikanSarprasId($order = Criteria::ASC) Order by the kepemilikan_sarpras_id column
 * @method PrasaranaQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method PrasaranaQuery orderByPanjang($order = Criteria::ASC) Order by the panjang column
 * @method PrasaranaQuery orderByLebar($order = Criteria::ASC) Order by the lebar column
 * @method PrasaranaQuery orderByRegPras($order = Criteria::ASC) Order by the reg_pras column
 * @method PrasaranaQuery orderByKetPrasarana($order = Criteria::ASC) Order by the ket_prasarana column
 * @method PrasaranaQuery orderByVolPondasiM3($order = Criteria::ASC) Order by the vol_pondasi_m3 column
 * @method PrasaranaQuery orderByVolSloopKolomBalokM3($order = Criteria::ASC) Order by the vol_sloop_kolom_balok_m3 column
 * @method PrasaranaQuery orderByLuasPlesterM2($order = Criteria::ASC) Order by the luas_plester_m2 column
 * @method PrasaranaQuery orderByPanjKudakudaM($order = Criteria::ASC) Order by the panj_kudakuda_m column
 * @method PrasaranaQuery orderByVolKudakudaM3($order = Criteria::ASC) Order by the vol_kudakuda_m3 column
 * @method PrasaranaQuery orderByPanjKasoM($order = Criteria::ASC) Order by the panj_kaso_m column
 * @method PrasaranaQuery orderByPanjRengM($order = Criteria::ASC) Order by the panj_reng_m column
 * @method PrasaranaQuery orderByLuasTutupAtapM2($order = Criteria::ASC) Order by the luas_tutup_atap_m2 column
 * @method PrasaranaQuery orderByLuasPlafonM2($order = Criteria::ASC) Order by the luas_plafon_m2 column
 * @method PrasaranaQuery orderByLuasDindingM2($order = Criteria::ASC) Order by the luas_dinding_m2 column
 * @method PrasaranaQuery orderByLuasDaunJendelaM2($order = Criteria::ASC) Order by the luas_daun_jendela_m2 column
 * @method PrasaranaQuery orderByLuasDaunPintuM2($order = Criteria::ASC) Order by the luas_daun_pintu_m2 column
 * @method PrasaranaQuery orderByPanjKusenM($order = Criteria::ASC) Order by the panj_kusen_m column
 * @method PrasaranaQuery orderByLuasTutupLantaiM2($order = Criteria::ASC) Order by the luas_tutup_lantai_m2 column
 * @method PrasaranaQuery orderByPanjInstListrikM($order = Criteria::ASC) Order by the panj_inst_listrik_m column
 * @method PrasaranaQuery orderByJmlInstListrik($order = Criteria::ASC) Order by the jml_inst_listrik column
 * @method PrasaranaQuery orderByPanjInstAirM($order = Criteria::ASC) Order by the panj_inst_air_m column
 * @method PrasaranaQuery orderByJmlInstAir($order = Criteria::ASC) Order by the jml_inst_air column
 * @method PrasaranaQuery orderByPanjDrainaseM($order = Criteria::ASC) Order by the panj_drainase_m column
 * @method PrasaranaQuery orderByLuasFinishStrukturM2($order = Criteria::ASC) Order by the luas_finish_struktur_m2 column
 * @method PrasaranaQuery orderByLuasFinishPlafonM2($order = Criteria::ASC) Order by the luas_finish_plafon_m2 column
 * @method PrasaranaQuery orderByLuasFinishDindingM2($order = Criteria::ASC) Order by the luas_finish_dinding_m2 column
 * @method PrasaranaQuery orderByLuasFinishKpjM2($order = Criteria::ASC) Order by the luas_finish_kpj_m2 column
 * @method PrasaranaQuery orderByIdHapusBuku($order = Criteria::ASC) Order by the id_hapus_buku column
 * @method PrasaranaQuery orderByTglHapusBuku($order = Criteria::ASC) Order by the tgl_hapus_buku column
 * @method PrasaranaQuery orderByAsalData($order = Criteria::ASC) Order by the asal_data column
 * @method PrasaranaQuery orderByLastUpdate($order = Criteria::ASC) Order by the Last_update column
 * @method PrasaranaQuery orderBySoftDelete($order = Criteria::ASC) Order by the Soft_delete column
 * @method PrasaranaQuery orderByLastSync($order = Criteria::ASC) Order by the last_sync column
 * @method PrasaranaQuery orderByUpdaterId($order = Criteria::ASC) Order by the Updater_ID column
 *
 * @method PrasaranaQuery groupByPrasaranaId() Group by the prasarana_id column
 * @method PrasaranaQuery groupByJenisPrasaranaId() Group by the jenis_prasarana_id column
 * @method PrasaranaQuery groupBySekolahId() Group by the sekolah_id column
 * @method PrasaranaQuery groupByIdIndukPrasarana() Group by the id_induk_prasarana column
 * @method PrasaranaQuery groupByKepemilikanSarprasId() Group by the kepemilikan_sarpras_id column
 * @method PrasaranaQuery groupByNama() Group by the nama column
 * @method PrasaranaQuery groupByPanjang() Group by the panjang column
 * @method PrasaranaQuery groupByLebar() Group by the lebar column
 * @method PrasaranaQuery groupByRegPras() Group by the reg_pras column
 * @method PrasaranaQuery groupByKetPrasarana() Group by the ket_prasarana column
 * @method PrasaranaQuery groupByVolPondasiM3() Group by the vol_pondasi_m3 column
 * @method PrasaranaQuery groupByVolSloopKolomBalokM3() Group by the vol_sloop_kolom_balok_m3 column
 * @method PrasaranaQuery groupByLuasPlesterM2() Group by the luas_plester_m2 column
 * @method PrasaranaQuery groupByPanjKudakudaM() Group by the panj_kudakuda_m column
 * @method PrasaranaQuery groupByVolKudakudaM3() Group by the vol_kudakuda_m3 column
 * @method PrasaranaQuery groupByPanjKasoM() Group by the panj_kaso_m column
 * @method PrasaranaQuery groupByPanjRengM() Group by the panj_reng_m column
 * @method PrasaranaQuery groupByLuasTutupAtapM2() Group by the luas_tutup_atap_m2 column
 * @method PrasaranaQuery groupByLuasPlafonM2() Group by the luas_plafon_m2 column
 * @method PrasaranaQuery groupByLuasDindingM2() Group by the luas_dinding_m2 column
 * @method PrasaranaQuery groupByLuasDaunJendelaM2() Group by the luas_daun_jendela_m2 column
 * @method PrasaranaQuery groupByLuasDaunPintuM2() Group by the luas_daun_pintu_m2 column
 * @method PrasaranaQuery groupByPanjKusenM() Group by the panj_kusen_m column
 * @method PrasaranaQuery groupByLuasTutupLantaiM2() Group by the luas_tutup_lantai_m2 column
 * @method PrasaranaQuery groupByPanjInstListrikM() Group by the panj_inst_listrik_m column
 * @method PrasaranaQuery groupByJmlInstListrik() Group by the jml_inst_listrik column
 * @method PrasaranaQuery groupByPanjInstAirM() Group by the panj_inst_air_m column
 * @method PrasaranaQuery groupByJmlInstAir() Group by the jml_inst_air column
 * @method PrasaranaQuery groupByPanjDrainaseM() Group by the panj_drainase_m column
 * @method PrasaranaQuery groupByLuasFinishStrukturM2() Group by the luas_finish_struktur_m2 column
 * @method PrasaranaQuery groupByLuasFinishPlafonM2() Group by the luas_finish_plafon_m2 column
 * @method PrasaranaQuery groupByLuasFinishDindingM2() Group by the luas_finish_dinding_m2 column
 * @method PrasaranaQuery groupByLuasFinishKpjM2() Group by the luas_finish_kpj_m2 column
 * @method PrasaranaQuery groupByIdHapusBuku() Group by the id_hapus_buku column
 * @method PrasaranaQuery groupByTglHapusBuku() Group by the tgl_hapus_buku column
 * @method PrasaranaQuery groupByAsalData() Group by the asal_data column
 * @method PrasaranaQuery groupByLastUpdate() Group by the Last_update column
 * @method PrasaranaQuery groupBySoftDelete() Group by the Soft_delete column
 * @method PrasaranaQuery groupByLastSync() Group by the last_sync column
 * @method PrasaranaQuery groupByUpdaterId() Group by the Updater_ID column
 *
 * @method PrasaranaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method PrasaranaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method PrasaranaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method PrasaranaQuery leftJoinPrasaranaRelatedByIdIndukPrasarana($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaRelatedByIdIndukPrasarana relation
 * @method PrasaranaQuery rightJoinPrasaranaRelatedByIdIndukPrasarana($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaRelatedByIdIndukPrasarana relation
 * @method PrasaranaQuery innerJoinPrasaranaRelatedByIdIndukPrasarana($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaRelatedByIdIndukPrasarana relation
 *
 * @method PrasaranaQuery leftJoinSekolah($relationAlias = null) Adds a LEFT JOIN clause to the query using the Sekolah relation
 * @method PrasaranaQuery rightJoinSekolah($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Sekolah relation
 * @method PrasaranaQuery innerJoinSekolah($relationAlias = null) Adds a INNER JOIN clause to the query using the Sekolah relation
 *
 * @method PrasaranaQuery leftJoinJenisHapusBuku($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisHapusBuku relation
 * @method PrasaranaQuery rightJoinJenisHapusBuku($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisHapusBuku relation
 * @method PrasaranaQuery innerJoinJenisHapusBuku($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisHapusBuku relation
 *
 * @method PrasaranaQuery leftJoinJenisPrasarana($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisPrasarana relation
 * @method PrasaranaQuery rightJoinJenisPrasarana($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisPrasarana relation
 * @method PrasaranaQuery innerJoinJenisPrasarana($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisPrasarana relation
 *
 * @method PrasaranaQuery leftJoinStatusKepemilikanSarpras($relationAlias = null) Adds a LEFT JOIN clause to the query using the StatusKepemilikanSarpras relation
 * @method PrasaranaQuery rightJoinStatusKepemilikanSarpras($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StatusKepemilikanSarpras relation
 * @method PrasaranaQuery innerJoinStatusKepemilikanSarpras($relationAlias = null) Adds a INNER JOIN clause to the query using the StatusKepemilikanSarpras relation
 *
 * @method PrasaranaQuery leftJoinBukuAlat($relationAlias = null) Adds a LEFT JOIN clause to the query using the BukuAlat relation
 * @method PrasaranaQuery rightJoinBukuAlat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BukuAlat relation
 * @method PrasaranaQuery innerJoinBukuAlat($relationAlias = null) Adds a INNER JOIN clause to the query using the BukuAlat relation
 *
 * @method PrasaranaQuery leftJoinJadwal($relationAlias = null) Adds a LEFT JOIN clause to the query using the Jadwal relation
 * @method PrasaranaQuery rightJoinJadwal($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Jadwal relation
 * @method PrasaranaQuery innerJoinJadwal($relationAlias = null) Adds a INNER JOIN clause to the query using the Jadwal relation
 *
 * @method PrasaranaQuery leftJoinVldPrasarana($relationAlias = null) Adds a LEFT JOIN clause to the query using the VldPrasarana relation
 * @method PrasaranaQuery rightJoinVldPrasarana($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VldPrasarana relation
 * @method PrasaranaQuery innerJoinVldPrasarana($relationAlias = null) Adds a INNER JOIN clause to the query using the VldPrasarana relation
 *
 * @method PrasaranaQuery leftJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method PrasaranaQuery rightJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 * @method PrasaranaQuery innerJoinPrasaranaRelatedByPrasaranaId($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
 *
 * @method PrasaranaQuery leftJoinPrasaranaLongitudinal($relationAlias = null) Adds a LEFT JOIN clause to the query using the PrasaranaLongitudinal relation
 * @method PrasaranaQuery rightJoinPrasaranaLongitudinal($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PrasaranaLongitudinal relation
 * @method PrasaranaQuery innerJoinPrasaranaLongitudinal($relationAlias = null) Adds a INNER JOIN clause to the query using the PrasaranaLongitudinal relation
 *
 * @method PrasaranaQuery leftJoinRombonganBelajar($relationAlias = null) Adds a LEFT JOIN clause to the query using the RombonganBelajar relation
 * @method PrasaranaQuery rightJoinRombonganBelajar($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RombonganBelajar relation
 * @method PrasaranaQuery innerJoinRombonganBelajar($relationAlias = null) Adds a INNER JOIN clause to the query using the RombonganBelajar relation
 *
 * @method PrasaranaQuery leftJoinSarana($relationAlias = null) Adds a LEFT JOIN clause to the query using the Sarana relation
 * @method PrasaranaQuery rightJoinSarana($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Sarana relation
 * @method PrasaranaQuery innerJoinSarana($relationAlias = null) Adds a INNER JOIN clause to the query using the Sarana relation
 *
 * @method Prasarana findOne(PropelPDO $con = null) Return the first Prasarana matching the query
 * @method Prasarana findOneOrCreate(PropelPDO $con = null) Return the first Prasarana matching the query, or a new Prasarana object populated from the query conditions when no match is found
 *
 * @method Prasarana findOneByJenisPrasaranaId(int $jenis_prasarana_id) Return the first Prasarana filtered by the jenis_prasarana_id column
 * @method Prasarana findOneBySekolahId(string $sekolah_id) Return the first Prasarana filtered by the sekolah_id column
 * @method Prasarana findOneByIdIndukPrasarana(string $id_induk_prasarana) Return the first Prasarana filtered by the id_induk_prasarana column
 * @method Prasarana findOneByKepemilikanSarprasId(string $kepemilikan_sarpras_id) Return the first Prasarana filtered by the kepemilikan_sarpras_id column
 * @method Prasarana findOneByNama(string $nama) Return the first Prasarana filtered by the nama column
 * @method Prasarana findOneByPanjang(double $panjang) Return the first Prasarana filtered by the panjang column
 * @method Prasarana findOneByLebar(double $lebar) Return the first Prasarana filtered by the lebar column
 * @method Prasarana findOneByRegPras(string $reg_pras) Return the first Prasarana filtered by the reg_pras column
 * @method Prasarana findOneByKetPrasarana(string $ket_prasarana) Return the first Prasarana filtered by the ket_prasarana column
 * @method Prasarana findOneByVolPondasiM3(string $vol_pondasi_m3) Return the first Prasarana filtered by the vol_pondasi_m3 column
 * @method Prasarana findOneByVolSloopKolomBalokM3(string $vol_sloop_kolom_balok_m3) Return the first Prasarana filtered by the vol_sloop_kolom_balok_m3 column
 * @method Prasarana findOneByLuasPlesterM2(string $luas_plester_m2) Return the first Prasarana filtered by the luas_plester_m2 column
 * @method Prasarana findOneByPanjKudakudaM(string $panj_kudakuda_m) Return the first Prasarana filtered by the panj_kudakuda_m column
 * @method Prasarana findOneByVolKudakudaM3(string $vol_kudakuda_m3) Return the first Prasarana filtered by the vol_kudakuda_m3 column
 * @method Prasarana findOneByPanjKasoM(string $panj_kaso_m) Return the first Prasarana filtered by the panj_kaso_m column
 * @method Prasarana findOneByPanjRengM(string $panj_reng_m) Return the first Prasarana filtered by the panj_reng_m column
 * @method Prasarana findOneByLuasTutupAtapM2(string $luas_tutup_atap_m2) Return the first Prasarana filtered by the luas_tutup_atap_m2 column
 * @method Prasarana findOneByLuasPlafonM2(string $luas_plafon_m2) Return the first Prasarana filtered by the luas_plafon_m2 column
 * @method Prasarana findOneByLuasDindingM2(string $luas_dinding_m2) Return the first Prasarana filtered by the luas_dinding_m2 column
 * @method Prasarana findOneByLuasDaunJendelaM2(string $luas_daun_jendela_m2) Return the first Prasarana filtered by the luas_daun_jendela_m2 column
 * @method Prasarana findOneByLuasDaunPintuM2(string $luas_daun_pintu_m2) Return the first Prasarana filtered by the luas_daun_pintu_m2 column
 * @method Prasarana findOneByPanjKusenM(string $panj_kusen_m) Return the first Prasarana filtered by the panj_kusen_m column
 * @method Prasarana findOneByLuasTutupLantaiM2(string $luas_tutup_lantai_m2) Return the first Prasarana filtered by the luas_tutup_lantai_m2 column
 * @method Prasarana findOneByPanjInstListrikM(string $panj_inst_listrik_m) Return the first Prasarana filtered by the panj_inst_listrik_m column
 * @method Prasarana findOneByJmlInstListrik(string $jml_inst_listrik) Return the first Prasarana filtered by the jml_inst_listrik column
 * @method Prasarana findOneByPanjInstAirM(string $panj_inst_air_m) Return the first Prasarana filtered by the panj_inst_air_m column
 * @method Prasarana findOneByJmlInstAir(string $jml_inst_air) Return the first Prasarana filtered by the jml_inst_air column
 * @method Prasarana findOneByPanjDrainaseM(string $panj_drainase_m) Return the first Prasarana filtered by the panj_drainase_m column
 * @method Prasarana findOneByLuasFinishStrukturM2(string $luas_finish_struktur_m2) Return the first Prasarana filtered by the luas_finish_struktur_m2 column
 * @method Prasarana findOneByLuasFinishPlafonM2(string $luas_finish_plafon_m2) Return the first Prasarana filtered by the luas_finish_plafon_m2 column
 * @method Prasarana findOneByLuasFinishDindingM2(string $luas_finish_dinding_m2) Return the first Prasarana filtered by the luas_finish_dinding_m2 column
 * @method Prasarana findOneByLuasFinishKpjM2(string $luas_finish_kpj_m2) Return the first Prasarana filtered by the luas_finish_kpj_m2 column
 * @method Prasarana findOneByIdHapusBuku(string $id_hapus_buku) Return the first Prasarana filtered by the id_hapus_buku column
 * @method Prasarana findOneByTglHapusBuku(string $tgl_hapus_buku) Return the first Prasarana filtered by the tgl_hapus_buku column
 * @method Prasarana findOneByAsalData(string $asal_data) Return the first Prasarana filtered by the asal_data column
 * @method Prasarana findOneByLastUpdate(string $Last_update) Return the first Prasarana filtered by the Last_update column
 * @method Prasarana findOneBySoftDelete(string $Soft_delete) Return the first Prasarana filtered by the Soft_delete column
 * @method Prasarana findOneByLastSync(string $last_sync) Return the first Prasarana filtered by the last_sync column
 * @method Prasarana findOneByUpdaterId(string $Updater_ID) Return the first Prasarana filtered by the Updater_ID column
 *
 * @method array findByPrasaranaId(string $prasarana_id) Return Prasarana objects filtered by the prasarana_id column
 * @method array findByJenisPrasaranaId(int $jenis_prasarana_id) Return Prasarana objects filtered by the jenis_prasarana_id column
 * @method array findBySekolahId(string $sekolah_id) Return Prasarana objects filtered by the sekolah_id column
 * @method array findByIdIndukPrasarana(string $id_induk_prasarana) Return Prasarana objects filtered by the id_induk_prasarana column
 * @method array findByKepemilikanSarprasId(string $kepemilikan_sarpras_id) Return Prasarana objects filtered by the kepemilikan_sarpras_id column
 * @method array findByNama(string $nama) Return Prasarana objects filtered by the nama column
 * @method array findByPanjang(double $panjang) Return Prasarana objects filtered by the panjang column
 * @method array findByLebar(double $lebar) Return Prasarana objects filtered by the lebar column
 * @method array findByRegPras(string $reg_pras) Return Prasarana objects filtered by the reg_pras column
 * @method array findByKetPrasarana(string $ket_prasarana) Return Prasarana objects filtered by the ket_prasarana column
 * @method array findByVolPondasiM3(string $vol_pondasi_m3) Return Prasarana objects filtered by the vol_pondasi_m3 column
 * @method array findByVolSloopKolomBalokM3(string $vol_sloop_kolom_balok_m3) Return Prasarana objects filtered by the vol_sloop_kolom_balok_m3 column
 * @method array findByLuasPlesterM2(string $luas_plester_m2) Return Prasarana objects filtered by the luas_plester_m2 column
 * @method array findByPanjKudakudaM(string $panj_kudakuda_m) Return Prasarana objects filtered by the panj_kudakuda_m column
 * @method array findByVolKudakudaM3(string $vol_kudakuda_m3) Return Prasarana objects filtered by the vol_kudakuda_m3 column
 * @method array findByPanjKasoM(string $panj_kaso_m) Return Prasarana objects filtered by the panj_kaso_m column
 * @method array findByPanjRengM(string $panj_reng_m) Return Prasarana objects filtered by the panj_reng_m column
 * @method array findByLuasTutupAtapM2(string $luas_tutup_atap_m2) Return Prasarana objects filtered by the luas_tutup_atap_m2 column
 * @method array findByLuasPlafonM2(string $luas_plafon_m2) Return Prasarana objects filtered by the luas_plafon_m2 column
 * @method array findByLuasDindingM2(string $luas_dinding_m2) Return Prasarana objects filtered by the luas_dinding_m2 column
 * @method array findByLuasDaunJendelaM2(string $luas_daun_jendela_m2) Return Prasarana objects filtered by the luas_daun_jendela_m2 column
 * @method array findByLuasDaunPintuM2(string $luas_daun_pintu_m2) Return Prasarana objects filtered by the luas_daun_pintu_m2 column
 * @method array findByPanjKusenM(string $panj_kusen_m) Return Prasarana objects filtered by the panj_kusen_m column
 * @method array findByLuasTutupLantaiM2(string $luas_tutup_lantai_m2) Return Prasarana objects filtered by the luas_tutup_lantai_m2 column
 * @method array findByPanjInstListrikM(string $panj_inst_listrik_m) Return Prasarana objects filtered by the panj_inst_listrik_m column
 * @method array findByJmlInstListrik(string $jml_inst_listrik) Return Prasarana objects filtered by the jml_inst_listrik column
 * @method array findByPanjInstAirM(string $panj_inst_air_m) Return Prasarana objects filtered by the panj_inst_air_m column
 * @method array findByJmlInstAir(string $jml_inst_air) Return Prasarana objects filtered by the jml_inst_air column
 * @method array findByPanjDrainaseM(string $panj_drainase_m) Return Prasarana objects filtered by the panj_drainase_m column
 * @method array findByLuasFinishStrukturM2(string $luas_finish_struktur_m2) Return Prasarana objects filtered by the luas_finish_struktur_m2 column
 * @method array findByLuasFinishPlafonM2(string $luas_finish_plafon_m2) Return Prasarana objects filtered by the luas_finish_plafon_m2 column
 * @method array findByLuasFinishDindingM2(string $luas_finish_dinding_m2) Return Prasarana objects filtered by the luas_finish_dinding_m2 column
 * @method array findByLuasFinishKpjM2(string $luas_finish_kpj_m2) Return Prasarana objects filtered by the luas_finish_kpj_m2 column
 * @method array findByIdHapusBuku(string $id_hapus_buku) Return Prasarana objects filtered by the id_hapus_buku column
 * @method array findByTglHapusBuku(string $tgl_hapus_buku) Return Prasarana objects filtered by the tgl_hapus_buku column
 * @method array findByAsalData(string $asal_data) Return Prasarana objects filtered by the asal_data column
 * @method array findByLastUpdate(string $Last_update) Return Prasarana objects filtered by the Last_update column
 * @method array findBySoftDelete(string $Soft_delete) Return Prasarana objects filtered by the Soft_delete column
 * @method array findByLastSync(string $last_sync) Return Prasarana objects filtered by the last_sync column
 * @method array findByUpdaterId(string $Updater_ID) Return Prasarana objects filtered by the Updater_ID column
 *
 * @package    propel.generator.Admin.Model.om
 */
abstract class BasePrasaranaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasePrasaranaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Dapodik_Paudni', $modelName = 'Admin\\Model\\Prasarana', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new PrasaranaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   PrasaranaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return PrasaranaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof PrasaranaQuery) {
            return $criteria;
        }
        $query = new PrasaranaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Prasarana|Prasarana[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PrasaranaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Prasarana A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByPrasaranaId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Prasarana A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [prasarana_id], [jenis_prasarana_id], [sekolah_id], [id_induk_prasarana], [kepemilikan_sarpras_id], [nama], [panjang], [lebar], [reg_pras], [ket_prasarana], [vol_pondasi_m3], [vol_sloop_kolom_balok_m3], [luas_plester_m2], [panj_kudakuda_m], [vol_kudakuda_m3], [panj_kaso_m], [panj_reng_m], [luas_tutup_atap_m2], [luas_plafon_m2], [luas_dinding_m2], [luas_daun_jendela_m2], [luas_daun_pintu_m2], [panj_kusen_m], [luas_tutup_lantai_m2], [panj_inst_listrik_m], [jml_inst_listrik], [panj_inst_air_m], [jml_inst_air], [panj_drainase_m], [luas_finish_struktur_m2], [luas_finish_plafon_m2], [luas_finish_dinding_m2], [luas_finish_kpj_m2], [id_hapus_buku], [tgl_hapus_buku], [asal_data], [Last_update], [Soft_delete], [last_sync], [Updater_ID] FROM [prasarana] WHERE [prasarana_id] = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Prasarana();
            $obj->hydrate($row);
            PrasaranaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Prasarana|Prasarana[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Prasarana[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the prasarana_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPrasaranaId('fooValue');   // WHERE prasarana_id = 'fooValue'
     * $query->filterByPrasaranaId('%fooValue%'); // WHERE prasarana_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prasaranaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPrasaranaId($prasaranaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prasaranaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prasaranaId)) {
                $prasaranaId = str_replace('*', '%', $prasaranaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $prasaranaId, $comparison);
    }

    /**
     * Filter the query on the jenis_prasarana_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisPrasaranaId(1234); // WHERE jenis_prasarana_id = 1234
     * $query->filterByJenisPrasaranaId(array(12, 34)); // WHERE jenis_prasarana_id IN (12, 34)
     * $query->filterByJenisPrasaranaId(array('min' => 12)); // WHERE jenis_prasarana_id >= 12
     * $query->filterByJenisPrasaranaId(array('max' => 12)); // WHERE jenis_prasarana_id <= 12
     * </code>
     *
     * @see       filterByJenisPrasarana()
     *
     * @param     mixed $jenisPrasaranaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByJenisPrasaranaId($jenisPrasaranaId = null, $comparison = null)
    {
        if (is_array($jenisPrasaranaId)) {
            $useMinMax = false;
            if (isset($jenisPrasaranaId['min'])) {
                $this->addUsingAlias(PrasaranaPeer::JENIS_PRASARANA_ID, $jenisPrasaranaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisPrasaranaId['max'])) {
                $this->addUsingAlias(PrasaranaPeer::JENIS_PRASARANA_ID, $jenisPrasaranaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::JENIS_PRASARANA_ID, $jenisPrasaranaId, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the id_induk_prasarana column
     *
     * Example usage:
     * <code>
     * $query->filterByIdIndukPrasarana('fooValue');   // WHERE id_induk_prasarana = 'fooValue'
     * $query->filterByIdIndukPrasarana('%fooValue%'); // WHERE id_induk_prasarana LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idIndukPrasarana The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByIdIndukPrasarana($idIndukPrasarana = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idIndukPrasarana)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idIndukPrasarana)) {
                $idIndukPrasarana = str_replace('*', '%', $idIndukPrasarana);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::ID_INDUK_PRASARANA, $idIndukPrasarana, $comparison);
    }

    /**
     * Filter the query on the kepemilikan_sarpras_id column
     *
     * Example usage:
     * <code>
     * $query->filterByKepemilikanSarprasId(1234); // WHERE kepemilikan_sarpras_id = 1234
     * $query->filterByKepemilikanSarprasId(array(12, 34)); // WHERE kepemilikan_sarpras_id IN (12, 34)
     * $query->filterByKepemilikanSarprasId(array('min' => 12)); // WHERE kepemilikan_sarpras_id >= 12
     * $query->filterByKepemilikanSarprasId(array('max' => 12)); // WHERE kepemilikan_sarpras_id <= 12
     * </code>
     *
     * @see       filterByStatusKepemilikanSarpras()
     *
     * @param     mixed $kepemilikanSarprasId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByKepemilikanSarprasId($kepemilikanSarprasId = null, $comparison = null)
    {
        if (is_array($kepemilikanSarprasId)) {
            $useMinMax = false;
            if (isset($kepemilikanSarprasId['min'])) {
                $this->addUsingAlias(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $kepemilikanSarprasId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kepemilikanSarprasId['max'])) {
                $this->addUsingAlias(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $kepemilikanSarprasId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $kepemilikanSarprasId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the panjang column
     *
     * Example usage:
     * <code>
     * $query->filterByPanjang(1234); // WHERE panjang = 1234
     * $query->filterByPanjang(array(12, 34)); // WHERE panjang IN (12, 34)
     * $query->filterByPanjang(array('min' => 12)); // WHERE panjang >= 12
     * $query->filterByPanjang(array('max' => 12)); // WHERE panjang <= 12
     * </code>
     *
     * @param     mixed $panjang The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPanjang($panjang = null, $comparison = null)
    {
        if (is_array($panjang)) {
            $useMinMax = false;
            if (isset($panjang['min'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJANG, $panjang['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($panjang['max'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJANG, $panjang['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::PANJANG, $panjang, $comparison);
    }

    /**
     * Filter the query on the lebar column
     *
     * Example usage:
     * <code>
     * $query->filterByLebar(1234); // WHERE lebar = 1234
     * $query->filterByLebar(array(12, 34)); // WHERE lebar IN (12, 34)
     * $query->filterByLebar(array('min' => 12)); // WHERE lebar >= 12
     * $query->filterByLebar(array('max' => 12)); // WHERE lebar <= 12
     * </code>
     *
     * @param     mixed $lebar The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLebar($lebar = null, $comparison = null)
    {
        if (is_array($lebar)) {
            $useMinMax = false;
            if (isset($lebar['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LEBAR, $lebar['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lebar['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LEBAR, $lebar['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LEBAR, $lebar, $comparison);
    }

    /**
     * Filter the query on the reg_pras column
     *
     * Example usage:
     * <code>
     * $query->filterByRegPras('fooValue');   // WHERE reg_pras = 'fooValue'
     * $query->filterByRegPras('%fooValue%'); // WHERE reg_pras LIKE '%fooValue%'
     * </code>
     *
     * @param     string $regPras The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByRegPras($regPras = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($regPras)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $regPras)) {
                $regPras = str_replace('*', '%', $regPras);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::REG_PRAS, $regPras, $comparison);
    }

    /**
     * Filter the query on the ket_prasarana column
     *
     * Example usage:
     * <code>
     * $query->filterByKetPrasarana('fooValue');   // WHERE ket_prasarana = 'fooValue'
     * $query->filterByKetPrasarana('%fooValue%'); // WHERE ket_prasarana LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ketPrasarana The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByKetPrasarana($ketPrasarana = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ketPrasarana)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ketPrasarana)) {
                $ketPrasarana = str_replace('*', '%', $ketPrasarana);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::KET_PRASARANA, $ketPrasarana, $comparison);
    }

    /**
     * Filter the query on the vol_pondasi_m3 column
     *
     * Example usage:
     * <code>
     * $query->filterByVolPondasiM3(1234); // WHERE vol_pondasi_m3 = 1234
     * $query->filterByVolPondasiM3(array(12, 34)); // WHERE vol_pondasi_m3 IN (12, 34)
     * $query->filterByVolPondasiM3(array('min' => 12)); // WHERE vol_pondasi_m3 >= 12
     * $query->filterByVolPondasiM3(array('max' => 12)); // WHERE vol_pondasi_m3 <= 12
     * </code>
     *
     * @param     mixed $volPondasiM3 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByVolPondasiM3($volPondasiM3 = null, $comparison = null)
    {
        if (is_array($volPondasiM3)) {
            $useMinMax = false;
            if (isset($volPondasiM3['min'])) {
                $this->addUsingAlias(PrasaranaPeer::VOL_PONDASI_M3, $volPondasiM3['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($volPondasiM3['max'])) {
                $this->addUsingAlias(PrasaranaPeer::VOL_PONDASI_M3, $volPondasiM3['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::VOL_PONDASI_M3, $volPondasiM3, $comparison);
    }

    /**
     * Filter the query on the vol_sloop_kolom_balok_m3 column
     *
     * Example usage:
     * <code>
     * $query->filterByVolSloopKolomBalokM3(1234); // WHERE vol_sloop_kolom_balok_m3 = 1234
     * $query->filterByVolSloopKolomBalokM3(array(12, 34)); // WHERE vol_sloop_kolom_balok_m3 IN (12, 34)
     * $query->filterByVolSloopKolomBalokM3(array('min' => 12)); // WHERE vol_sloop_kolom_balok_m3 >= 12
     * $query->filterByVolSloopKolomBalokM3(array('max' => 12)); // WHERE vol_sloop_kolom_balok_m3 <= 12
     * </code>
     *
     * @param     mixed $volSloopKolomBalokM3 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByVolSloopKolomBalokM3($volSloopKolomBalokM3 = null, $comparison = null)
    {
        if (is_array($volSloopKolomBalokM3)) {
            $useMinMax = false;
            if (isset($volSloopKolomBalokM3['min'])) {
                $this->addUsingAlias(PrasaranaPeer::VOL_SLOOP_KOLOM_BALOK_M3, $volSloopKolomBalokM3['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($volSloopKolomBalokM3['max'])) {
                $this->addUsingAlias(PrasaranaPeer::VOL_SLOOP_KOLOM_BALOK_M3, $volSloopKolomBalokM3['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::VOL_SLOOP_KOLOM_BALOK_M3, $volSloopKolomBalokM3, $comparison);
    }

    /**
     * Filter the query on the luas_plester_m2 column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasPlesterM2(1234); // WHERE luas_plester_m2 = 1234
     * $query->filterByLuasPlesterM2(array(12, 34)); // WHERE luas_plester_m2 IN (12, 34)
     * $query->filterByLuasPlesterM2(array('min' => 12)); // WHERE luas_plester_m2 >= 12
     * $query->filterByLuasPlesterM2(array('max' => 12)); // WHERE luas_plester_m2 <= 12
     * </code>
     *
     * @param     mixed $luasPlesterM2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLuasPlesterM2($luasPlesterM2 = null, $comparison = null)
    {
        if (is_array($luasPlesterM2)) {
            $useMinMax = false;
            if (isset($luasPlesterM2['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_PLESTER_M2, $luasPlesterM2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasPlesterM2['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_PLESTER_M2, $luasPlesterM2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LUAS_PLESTER_M2, $luasPlesterM2, $comparison);
    }

    /**
     * Filter the query on the panj_kudakuda_m column
     *
     * Example usage:
     * <code>
     * $query->filterByPanjKudakudaM(1234); // WHERE panj_kudakuda_m = 1234
     * $query->filterByPanjKudakudaM(array(12, 34)); // WHERE panj_kudakuda_m IN (12, 34)
     * $query->filterByPanjKudakudaM(array('min' => 12)); // WHERE panj_kudakuda_m >= 12
     * $query->filterByPanjKudakudaM(array('max' => 12)); // WHERE panj_kudakuda_m <= 12
     * </code>
     *
     * @param     mixed $panjKudakudaM The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPanjKudakudaM($panjKudakudaM = null, $comparison = null)
    {
        if (is_array($panjKudakudaM)) {
            $useMinMax = false;
            if (isset($panjKudakudaM['min'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_KUDAKUDA_M, $panjKudakudaM['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($panjKudakudaM['max'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_KUDAKUDA_M, $panjKudakudaM['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::PANJ_KUDAKUDA_M, $panjKudakudaM, $comparison);
    }

    /**
     * Filter the query on the vol_kudakuda_m3 column
     *
     * Example usage:
     * <code>
     * $query->filterByVolKudakudaM3(1234); // WHERE vol_kudakuda_m3 = 1234
     * $query->filterByVolKudakudaM3(array(12, 34)); // WHERE vol_kudakuda_m3 IN (12, 34)
     * $query->filterByVolKudakudaM3(array('min' => 12)); // WHERE vol_kudakuda_m3 >= 12
     * $query->filterByVolKudakudaM3(array('max' => 12)); // WHERE vol_kudakuda_m3 <= 12
     * </code>
     *
     * @param     mixed $volKudakudaM3 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByVolKudakudaM3($volKudakudaM3 = null, $comparison = null)
    {
        if (is_array($volKudakudaM3)) {
            $useMinMax = false;
            if (isset($volKudakudaM3['min'])) {
                $this->addUsingAlias(PrasaranaPeer::VOL_KUDAKUDA_M3, $volKudakudaM3['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($volKudakudaM3['max'])) {
                $this->addUsingAlias(PrasaranaPeer::VOL_KUDAKUDA_M3, $volKudakudaM3['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::VOL_KUDAKUDA_M3, $volKudakudaM3, $comparison);
    }

    /**
     * Filter the query on the panj_kaso_m column
     *
     * Example usage:
     * <code>
     * $query->filterByPanjKasoM(1234); // WHERE panj_kaso_m = 1234
     * $query->filterByPanjKasoM(array(12, 34)); // WHERE panj_kaso_m IN (12, 34)
     * $query->filterByPanjKasoM(array('min' => 12)); // WHERE panj_kaso_m >= 12
     * $query->filterByPanjKasoM(array('max' => 12)); // WHERE panj_kaso_m <= 12
     * </code>
     *
     * @param     mixed $panjKasoM The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPanjKasoM($panjKasoM = null, $comparison = null)
    {
        if (is_array($panjKasoM)) {
            $useMinMax = false;
            if (isset($panjKasoM['min'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_KASO_M, $panjKasoM['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($panjKasoM['max'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_KASO_M, $panjKasoM['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::PANJ_KASO_M, $panjKasoM, $comparison);
    }

    /**
     * Filter the query on the panj_reng_m column
     *
     * Example usage:
     * <code>
     * $query->filterByPanjRengM(1234); // WHERE panj_reng_m = 1234
     * $query->filterByPanjRengM(array(12, 34)); // WHERE panj_reng_m IN (12, 34)
     * $query->filterByPanjRengM(array('min' => 12)); // WHERE panj_reng_m >= 12
     * $query->filterByPanjRengM(array('max' => 12)); // WHERE panj_reng_m <= 12
     * </code>
     *
     * @param     mixed $panjRengM The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPanjRengM($panjRengM = null, $comparison = null)
    {
        if (is_array($panjRengM)) {
            $useMinMax = false;
            if (isset($panjRengM['min'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_RENG_M, $panjRengM['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($panjRengM['max'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_RENG_M, $panjRengM['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::PANJ_RENG_M, $panjRengM, $comparison);
    }

    /**
     * Filter the query on the luas_tutup_atap_m2 column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasTutupAtapM2(1234); // WHERE luas_tutup_atap_m2 = 1234
     * $query->filterByLuasTutupAtapM2(array(12, 34)); // WHERE luas_tutup_atap_m2 IN (12, 34)
     * $query->filterByLuasTutupAtapM2(array('min' => 12)); // WHERE luas_tutup_atap_m2 >= 12
     * $query->filterByLuasTutupAtapM2(array('max' => 12)); // WHERE luas_tutup_atap_m2 <= 12
     * </code>
     *
     * @param     mixed $luasTutupAtapM2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLuasTutupAtapM2($luasTutupAtapM2 = null, $comparison = null)
    {
        if (is_array($luasTutupAtapM2)) {
            $useMinMax = false;
            if (isset($luasTutupAtapM2['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_TUTUP_ATAP_M2, $luasTutupAtapM2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasTutupAtapM2['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_TUTUP_ATAP_M2, $luasTutupAtapM2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LUAS_TUTUP_ATAP_M2, $luasTutupAtapM2, $comparison);
    }

    /**
     * Filter the query on the luas_plafon_m2 column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasPlafonM2(1234); // WHERE luas_plafon_m2 = 1234
     * $query->filterByLuasPlafonM2(array(12, 34)); // WHERE luas_plafon_m2 IN (12, 34)
     * $query->filterByLuasPlafonM2(array('min' => 12)); // WHERE luas_plafon_m2 >= 12
     * $query->filterByLuasPlafonM2(array('max' => 12)); // WHERE luas_plafon_m2 <= 12
     * </code>
     *
     * @param     mixed $luasPlafonM2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLuasPlafonM2($luasPlafonM2 = null, $comparison = null)
    {
        if (is_array($luasPlafonM2)) {
            $useMinMax = false;
            if (isset($luasPlafonM2['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_PLAFON_M2, $luasPlafonM2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasPlafonM2['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_PLAFON_M2, $luasPlafonM2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LUAS_PLAFON_M2, $luasPlafonM2, $comparison);
    }

    /**
     * Filter the query on the luas_dinding_m2 column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasDindingM2(1234); // WHERE luas_dinding_m2 = 1234
     * $query->filterByLuasDindingM2(array(12, 34)); // WHERE luas_dinding_m2 IN (12, 34)
     * $query->filterByLuasDindingM2(array('min' => 12)); // WHERE luas_dinding_m2 >= 12
     * $query->filterByLuasDindingM2(array('max' => 12)); // WHERE luas_dinding_m2 <= 12
     * </code>
     *
     * @param     mixed $luasDindingM2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLuasDindingM2($luasDindingM2 = null, $comparison = null)
    {
        if (is_array($luasDindingM2)) {
            $useMinMax = false;
            if (isset($luasDindingM2['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_DINDING_M2, $luasDindingM2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasDindingM2['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_DINDING_M2, $luasDindingM2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LUAS_DINDING_M2, $luasDindingM2, $comparison);
    }

    /**
     * Filter the query on the luas_daun_jendela_m2 column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasDaunJendelaM2(1234); // WHERE luas_daun_jendela_m2 = 1234
     * $query->filterByLuasDaunJendelaM2(array(12, 34)); // WHERE luas_daun_jendela_m2 IN (12, 34)
     * $query->filterByLuasDaunJendelaM2(array('min' => 12)); // WHERE luas_daun_jendela_m2 >= 12
     * $query->filterByLuasDaunJendelaM2(array('max' => 12)); // WHERE luas_daun_jendela_m2 <= 12
     * </code>
     *
     * @param     mixed $luasDaunJendelaM2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLuasDaunJendelaM2($luasDaunJendelaM2 = null, $comparison = null)
    {
        if (is_array($luasDaunJendelaM2)) {
            $useMinMax = false;
            if (isset($luasDaunJendelaM2['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_DAUN_JENDELA_M2, $luasDaunJendelaM2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasDaunJendelaM2['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_DAUN_JENDELA_M2, $luasDaunJendelaM2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LUAS_DAUN_JENDELA_M2, $luasDaunJendelaM2, $comparison);
    }

    /**
     * Filter the query on the luas_daun_pintu_m2 column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasDaunPintuM2(1234); // WHERE luas_daun_pintu_m2 = 1234
     * $query->filterByLuasDaunPintuM2(array(12, 34)); // WHERE luas_daun_pintu_m2 IN (12, 34)
     * $query->filterByLuasDaunPintuM2(array('min' => 12)); // WHERE luas_daun_pintu_m2 >= 12
     * $query->filterByLuasDaunPintuM2(array('max' => 12)); // WHERE luas_daun_pintu_m2 <= 12
     * </code>
     *
     * @param     mixed $luasDaunPintuM2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLuasDaunPintuM2($luasDaunPintuM2 = null, $comparison = null)
    {
        if (is_array($luasDaunPintuM2)) {
            $useMinMax = false;
            if (isset($luasDaunPintuM2['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_DAUN_PINTU_M2, $luasDaunPintuM2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasDaunPintuM2['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_DAUN_PINTU_M2, $luasDaunPintuM2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LUAS_DAUN_PINTU_M2, $luasDaunPintuM2, $comparison);
    }

    /**
     * Filter the query on the panj_kusen_m column
     *
     * Example usage:
     * <code>
     * $query->filterByPanjKusenM(1234); // WHERE panj_kusen_m = 1234
     * $query->filterByPanjKusenM(array(12, 34)); // WHERE panj_kusen_m IN (12, 34)
     * $query->filterByPanjKusenM(array('min' => 12)); // WHERE panj_kusen_m >= 12
     * $query->filterByPanjKusenM(array('max' => 12)); // WHERE panj_kusen_m <= 12
     * </code>
     *
     * @param     mixed $panjKusenM The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPanjKusenM($panjKusenM = null, $comparison = null)
    {
        if (is_array($panjKusenM)) {
            $useMinMax = false;
            if (isset($panjKusenM['min'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_KUSEN_M, $panjKusenM['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($panjKusenM['max'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_KUSEN_M, $panjKusenM['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::PANJ_KUSEN_M, $panjKusenM, $comparison);
    }

    /**
     * Filter the query on the luas_tutup_lantai_m2 column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasTutupLantaiM2(1234); // WHERE luas_tutup_lantai_m2 = 1234
     * $query->filterByLuasTutupLantaiM2(array(12, 34)); // WHERE luas_tutup_lantai_m2 IN (12, 34)
     * $query->filterByLuasTutupLantaiM2(array('min' => 12)); // WHERE luas_tutup_lantai_m2 >= 12
     * $query->filterByLuasTutupLantaiM2(array('max' => 12)); // WHERE luas_tutup_lantai_m2 <= 12
     * </code>
     *
     * @param     mixed $luasTutupLantaiM2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLuasTutupLantaiM2($luasTutupLantaiM2 = null, $comparison = null)
    {
        if (is_array($luasTutupLantaiM2)) {
            $useMinMax = false;
            if (isset($luasTutupLantaiM2['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_TUTUP_LANTAI_M2, $luasTutupLantaiM2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasTutupLantaiM2['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_TUTUP_LANTAI_M2, $luasTutupLantaiM2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LUAS_TUTUP_LANTAI_M2, $luasTutupLantaiM2, $comparison);
    }

    /**
     * Filter the query on the panj_inst_listrik_m column
     *
     * Example usage:
     * <code>
     * $query->filterByPanjInstListrikM(1234); // WHERE panj_inst_listrik_m = 1234
     * $query->filterByPanjInstListrikM(array(12, 34)); // WHERE panj_inst_listrik_m IN (12, 34)
     * $query->filterByPanjInstListrikM(array('min' => 12)); // WHERE panj_inst_listrik_m >= 12
     * $query->filterByPanjInstListrikM(array('max' => 12)); // WHERE panj_inst_listrik_m <= 12
     * </code>
     *
     * @param     mixed $panjInstListrikM The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPanjInstListrikM($panjInstListrikM = null, $comparison = null)
    {
        if (is_array($panjInstListrikM)) {
            $useMinMax = false;
            if (isset($panjInstListrikM['min'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_INST_LISTRIK_M, $panjInstListrikM['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($panjInstListrikM['max'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_INST_LISTRIK_M, $panjInstListrikM['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::PANJ_INST_LISTRIK_M, $panjInstListrikM, $comparison);
    }

    /**
     * Filter the query on the jml_inst_listrik column
     *
     * Example usage:
     * <code>
     * $query->filterByJmlInstListrik(1234); // WHERE jml_inst_listrik = 1234
     * $query->filterByJmlInstListrik(array(12, 34)); // WHERE jml_inst_listrik IN (12, 34)
     * $query->filterByJmlInstListrik(array('min' => 12)); // WHERE jml_inst_listrik >= 12
     * $query->filterByJmlInstListrik(array('max' => 12)); // WHERE jml_inst_listrik <= 12
     * </code>
     *
     * @param     mixed $jmlInstListrik The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByJmlInstListrik($jmlInstListrik = null, $comparison = null)
    {
        if (is_array($jmlInstListrik)) {
            $useMinMax = false;
            if (isset($jmlInstListrik['min'])) {
                $this->addUsingAlias(PrasaranaPeer::JML_INST_LISTRIK, $jmlInstListrik['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jmlInstListrik['max'])) {
                $this->addUsingAlias(PrasaranaPeer::JML_INST_LISTRIK, $jmlInstListrik['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::JML_INST_LISTRIK, $jmlInstListrik, $comparison);
    }

    /**
     * Filter the query on the panj_inst_air_m column
     *
     * Example usage:
     * <code>
     * $query->filterByPanjInstAirM(1234); // WHERE panj_inst_air_m = 1234
     * $query->filterByPanjInstAirM(array(12, 34)); // WHERE panj_inst_air_m IN (12, 34)
     * $query->filterByPanjInstAirM(array('min' => 12)); // WHERE panj_inst_air_m >= 12
     * $query->filterByPanjInstAirM(array('max' => 12)); // WHERE panj_inst_air_m <= 12
     * </code>
     *
     * @param     mixed $panjInstAirM The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPanjInstAirM($panjInstAirM = null, $comparison = null)
    {
        if (is_array($panjInstAirM)) {
            $useMinMax = false;
            if (isset($panjInstAirM['min'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_INST_AIR_M, $panjInstAirM['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($panjInstAirM['max'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_INST_AIR_M, $panjInstAirM['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::PANJ_INST_AIR_M, $panjInstAirM, $comparison);
    }

    /**
     * Filter the query on the jml_inst_air column
     *
     * Example usage:
     * <code>
     * $query->filterByJmlInstAir(1234); // WHERE jml_inst_air = 1234
     * $query->filterByJmlInstAir(array(12, 34)); // WHERE jml_inst_air IN (12, 34)
     * $query->filterByJmlInstAir(array('min' => 12)); // WHERE jml_inst_air >= 12
     * $query->filterByJmlInstAir(array('max' => 12)); // WHERE jml_inst_air <= 12
     * </code>
     *
     * @param     mixed $jmlInstAir The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByJmlInstAir($jmlInstAir = null, $comparison = null)
    {
        if (is_array($jmlInstAir)) {
            $useMinMax = false;
            if (isset($jmlInstAir['min'])) {
                $this->addUsingAlias(PrasaranaPeer::JML_INST_AIR, $jmlInstAir['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jmlInstAir['max'])) {
                $this->addUsingAlias(PrasaranaPeer::JML_INST_AIR, $jmlInstAir['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::JML_INST_AIR, $jmlInstAir, $comparison);
    }

    /**
     * Filter the query on the panj_drainase_m column
     *
     * Example usage:
     * <code>
     * $query->filterByPanjDrainaseM(1234); // WHERE panj_drainase_m = 1234
     * $query->filterByPanjDrainaseM(array(12, 34)); // WHERE panj_drainase_m IN (12, 34)
     * $query->filterByPanjDrainaseM(array('min' => 12)); // WHERE panj_drainase_m >= 12
     * $query->filterByPanjDrainaseM(array('max' => 12)); // WHERE panj_drainase_m <= 12
     * </code>
     *
     * @param     mixed $panjDrainaseM The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByPanjDrainaseM($panjDrainaseM = null, $comparison = null)
    {
        if (is_array($panjDrainaseM)) {
            $useMinMax = false;
            if (isset($panjDrainaseM['min'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_DRAINASE_M, $panjDrainaseM['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($panjDrainaseM['max'])) {
                $this->addUsingAlias(PrasaranaPeer::PANJ_DRAINASE_M, $panjDrainaseM['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::PANJ_DRAINASE_M, $panjDrainaseM, $comparison);
    }

    /**
     * Filter the query on the luas_finish_struktur_m2 column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasFinishStrukturM2(1234); // WHERE luas_finish_struktur_m2 = 1234
     * $query->filterByLuasFinishStrukturM2(array(12, 34)); // WHERE luas_finish_struktur_m2 IN (12, 34)
     * $query->filterByLuasFinishStrukturM2(array('min' => 12)); // WHERE luas_finish_struktur_m2 >= 12
     * $query->filterByLuasFinishStrukturM2(array('max' => 12)); // WHERE luas_finish_struktur_m2 <= 12
     * </code>
     *
     * @param     mixed $luasFinishStrukturM2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLuasFinishStrukturM2($luasFinishStrukturM2 = null, $comparison = null)
    {
        if (is_array($luasFinishStrukturM2)) {
            $useMinMax = false;
            if (isset($luasFinishStrukturM2['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_FINISH_STRUKTUR_M2, $luasFinishStrukturM2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasFinishStrukturM2['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_FINISH_STRUKTUR_M2, $luasFinishStrukturM2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LUAS_FINISH_STRUKTUR_M2, $luasFinishStrukturM2, $comparison);
    }

    /**
     * Filter the query on the luas_finish_plafon_m2 column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasFinishPlafonM2(1234); // WHERE luas_finish_plafon_m2 = 1234
     * $query->filterByLuasFinishPlafonM2(array(12, 34)); // WHERE luas_finish_plafon_m2 IN (12, 34)
     * $query->filterByLuasFinishPlafonM2(array('min' => 12)); // WHERE luas_finish_plafon_m2 >= 12
     * $query->filterByLuasFinishPlafonM2(array('max' => 12)); // WHERE luas_finish_plafon_m2 <= 12
     * </code>
     *
     * @param     mixed $luasFinishPlafonM2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLuasFinishPlafonM2($luasFinishPlafonM2 = null, $comparison = null)
    {
        if (is_array($luasFinishPlafonM2)) {
            $useMinMax = false;
            if (isset($luasFinishPlafonM2['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_FINISH_PLAFON_M2, $luasFinishPlafonM2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasFinishPlafonM2['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_FINISH_PLAFON_M2, $luasFinishPlafonM2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LUAS_FINISH_PLAFON_M2, $luasFinishPlafonM2, $comparison);
    }

    /**
     * Filter the query on the luas_finish_dinding_m2 column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasFinishDindingM2(1234); // WHERE luas_finish_dinding_m2 = 1234
     * $query->filterByLuasFinishDindingM2(array(12, 34)); // WHERE luas_finish_dinding_m2 IN (12, 34)
     * $query->filterByLuasFinishDindingM2(array('min' => 12)); // WHERE luas_finish_dinding_m2 >= 12
     * $query->filterByLuasFinishDindingM2(array('max' => 12)); // WHERE luas_finish_dinding_m2 <= 12
     * </code>
     *
     * @param     mixed $luasFinishDindingM2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLuasFinishDindingM2($luasFinishDindingM2 = null, $comparison = null)
    {
        if (is_array($luasFinishDindingM2)) {
            $useMinMax = false;
            if (isset($luasFinishDindingM2['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_FINISH_DINDING_M2, $luasFinishDindingM2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasFinishDindingM2['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_FINISH_DINDING_M2, $luasFinishDindingM2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LUAS_FINISH_DINDING_M2, $luasFinishDindingM2, $comparison);
    }

    /**
     * Filter the query on the luas_finish_kpj_m2 column
     *
     * Example usage:
     * <code>
     * $query->filterByLuasFinishKpjM2(1234); // WHERE luas_finish_kpj_m2 = 1234
     * $query->filterByLuasFinishKpjM2(array(12, 34)); // WHERE luas_finish_kpj_m2 IN (12, 34)
     * $query->filterByLuasFinishKpjM2(array('min' => 12)); // WHERE luas_finish_kpj_m2 >= 12
     * $query->filterByLuasFinishKpjM2(array('max' => 12)); // WHERE luas_finish_kpj_m2 <= 12
     * </code>
     *
     * @param     mixed $luasFinishKpjM2 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLuasFinishKpjM2($luasFinishKpjM2 = null, $comparison = null)
    {
        if (is_array($luasFinishKpjM2)) {
            $useMinMax = false;
            if (isset($luasFinishKpjM2['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_FINISH_KPJ_M2, $luasFinishKpjM2['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($luasFinishKpjM2['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LUAS_FINISH_KPJ_M2, $luasFinishKpjM2['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LUAS_FINISH_KPJ_M2, $luasFinishKpjM2, $comparison);
    }

    /**
     * Filter the query on the id_hapus_buku column
     *
     * Example usage:
     * <code>
     * $query->filterByIdHapusBuku('fooValue');   // WHERE id_hapus_buku = 'fooValue'
     * $query->filterByIdHapusBuku('%fooValue%'); // WHERE id_hapus_buku LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idHapusBuku The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByIdHapusBuku($idHapusBuku = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idHapusBuku)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idHapusBuku)) {
                $idHapusBuku = str_replace('*', '%', $idHapusBuku);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::ID_HAPUS_BUKU, $idHapusBuku, $comparison);
    }

    /**
     * Filter the query on the tgl_hapus_buku column
     *
     * Example usage:
     * <code>
     * $query->filterByTglHapusBuku('fooValue');   // WHERE tgl_hapus_buku = 'fooValue'
     * $query->filterByTglHapusBuku('%fooValue%'); // WHERE tgl_hapus_buku LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tglHapusBuku The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByTglHapusBuku($tglHapusBuku = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tglHapusBuku)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tglHapusBuku)) {
                $tglHapusBuku = str_replace('*', '%', $tglHapusBuku);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::TGL_HAPUS_BUKU, $tglHapusBuku, $comparison);
    }

    /**
     * Filter the query on the asal_data column
     *
     * Example usage:
     * <code>
     * $query->filterByAsalData('fooValue');   // WHERE asal_data = 'fooValue'
     * $query->filterByAsalData('%fooValue%'); // WHERE asal_data LIKE '%fooValue%'
     * </code>
     *
     * @param     string $asalData The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByAsalData($asalData = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($asalData)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $asalData)) {
                $asalData = str_replace('*', '%', $asalData);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::ASAL_DATA, $asalData, $comparison);
    }

    /**
     * Filter the query on the Last_update column
     *
     * Example usage:
     * <code>
     * $query->filterByLastUpdate('2011-03-14'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate('now'); // WHERE Last_update = '2011-03-14'
     * $query->filterByLastUpdate(array('max' => 'yesterday')); // WHERE Last_update > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastUpdate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLastUpdate($lastUpdate = null, $comparison = null)
    {
        if (is_array($lastUpdate)) {
            $useMinMax = false;
            if (isset($lastUpdate['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LAST_UPDATE, $lastUpdate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastUpdate['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LAST_UPDATE, $lastUpdate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LAST_UPDATE, $lastUpdate, $comparison);
    }

    /**
     * Filter the query on the Soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE Soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE Soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE Soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE Soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(PrasaranaPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(PrasaranaPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the last_sync column
     *
     * Example usage:
     * <code>
     * $query->filterByLastSync('2011-03-14'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync('now'); // WHERE last_sync = '2011-03-14'
     * $query->filterByLastSync(array('max' => 'yesterday')); // WHERE last_sync > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastSync The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByLastSync($lastSync = null, $comparison = null)
    {
        if (is_array($lastSync)) {
            $useMinMax = false;
            if (isset($lastSync['min'])) {
                $this->addUsingAlias(PrasaranaPeer::LAST_SYNC, $lastSync['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastSync['max'])) {
                $this->addUsingAlias(PrasaranaPeer::LAST_SYNC, $lastSync['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::LAST_SYNC, $lastSync, $comparison);
    }

    /**
     * Filter the query on the Updater_ID column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdaterId('fooValue');   // WHERE Updater_ID = 'fooValue'
     * $query->filterByUpdaterId('%fooValue%'); // WHERE Updater_ID LIKE '%fooValue%'
     * </code>
     *
     * @param     string $updaterId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function filterByUpdaterId($updaterId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($updaterId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $updaterId)) {
                $updaterId = str_replace('*', '%', $updaterId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(PrasaranaPeer::UPDATER_ID, $updaterId, $comparison);
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaRelatedByIdIndukPrasarana($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(PrasaranaPeer::ID_INDUK_PRASARANA, $prasarana->getPrasaranaId(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaPeer::ID_INDUK_PRASARANA, $prasarana->toKeyValue('PrimaryKey', 'PrasaranaId'), $comparison);
        } else {
            throw new PropelException('filterByPrasaranaRelatedByIdIndukPrasarana() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaRelatedByIdIndukPrasarana relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinPrasaranaRelatedByIdIndukPrasarana($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaRelatedByIdIndukPrasarana');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaRelatedByIdIndukPrasarana');
        }

        return $this;
    }

    /**
     * Use the PrasaranaRelatedByIdIndukPrasarana relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaRelatedByIdIndukPrasaranaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPrasaranaRelatedByIdIndukPrasarana($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaRelatedByIdIndukPrasarana', '\Admin\Model\PrasaranaQuery');
    }

    /**
     * Filter the query by a related Sekolah object
     *
     * @param   Sekolah|PropelObjectCollection $sekolah The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySekolah($sekolah, $comparison = null)
    {
        if ($sekolah instanceof Sekolah) {
            return $this
                ->addUsingAlias(PrasaranaPeer::SEKOLAH_ID, $sekolah->getSekolahId(), $comparison);
        } elseif ($sekolah instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaPeer::SEKOLAH_ID, $sekolah->toKeyValue('PrimaryKey', 'SekolahId'), $comparison);
        } else {
            throw new PropelException('filterBySekolah() only accepts arguments of type Sekolah or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Sekolah relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinSekolah($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Sekolah');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Sekolah');
        }

        return $this;
    }

    /**
     * Use the Sekolah relation Sekolah object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\SekolahQuery A secondary query class using the current class as primary query
     */
    public function useSekolahQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSekolah($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Sekolah', '\Admin\Model\SekolahQuery');
    }

    /**
     * Filter the query by a related JenisHapusBuku object
     *
     * @param   JenisHapusBuku|PropelObjectCollection $jenisHapusBuku The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisHapusBuku($jenisHapusBuku, $comparison = null)
    {
        if ($jenisHapusBuku instanceof JenisHapusBuku) {
            return $this
                ->addUsingAlias(PrasaranaPeer::ID_HAPUS_BUKU, $jenisHapusBuku->getIdHapusBuku(), $comparison);
        } elseif ($jenisHapusBuku instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaPeer::ID_HAPUS_BUKU, $jenisHapusBuku->toKeyValue('PrimaryKey', 'IdHapusBuku'), $comparison);
        } else {
            throw new PropelException('filterByJenisHapusBuku() only accepts arguments of type JenisHapusBuku or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisHapusBuku relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinJenisHapusBuku($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisHapusBuku');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisHapusBuku');
        }

        return $this;
    }

    /**
     * Use the JenisHapusBuku relation JenisHapusBuku object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\JenisHapusBukuQuery A secondary query class using the current class as primary query
     */
    public function useJenisHapusBukuQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinJenisHapusBuku($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisHapusBuku', '\Admin\Model\JenisHapusBukuQuery');
    }

    /**
     * Filter the query by a related JenisPrasarana object
     *
     * @param   JenisPrasarana|PropelObjectCollection $jenisPrasarana The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJenisPrasarana($jenisPrasarana, $comparison = null)
    {
        if ($jenisPrasarana instanceof JenisPrasarana) {
            return $this
                ->addUsingAlias(PrasaranaPeer::JENIS_PRASARANA_ID, $jenisPrasarana->getJenisPrasaranaId(), $comparison);
        } elseif ($jenisPrasarana instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaPeer::JENIS_PRASARANA_ID, $jenisPrasarana->toKeyValue('PrimaryKey', 'JenisPrasaranaId'), $comparison);
        } else {
            throw new PropelException('filterByJenisPrasarana() only accepts arguments of type JenisPrasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the JenisPrasarana relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinJenisPrasarana($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('JenisPrasarana');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'JenisPrasarana');
        }

        return $this;
    }

    /**
     * Use the JenisPrasarana relation JenisPrasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\JenisPrasaranaQuery A secondary query class using the current class as primary query
     */
    public function useJenisPrasaranaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJenisPrasarana($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'JenisPrasarana', '\Admin\Model\JenisPrasaranaQuery');
    }

    /**
     * Filter the query by a related StatusKepemilikanSarpras object
     *
     * @param   StatusKepemilikanSarpras|PropelObjectCollection $statusKepemilikanSarpras The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByStatusKepemilikanSarpras($statusKepemilikanSarpras, $comparison = null)
    {
        if ($statusKepemilikanSarpras instanceof StatusKepemilikanSarpras) {
            return $this
                ->addUsingAlias(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $statusKepemilikanSarpras->getKepemilikanSarprasId(), $comparison);
        } elseif ($statusKepemilikanSarpras instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $statusKepemilikanSarpras->toKeyValue('PrimaryKey', 'KepemilikanSarprasId'), $comparison);
        } else {
            throw new PropelException('filterByStatusKepemilikanSarpras() only accepts arguments of type StatusKepemilikanSarpras or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StatusKepemilikanSarpras relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinStatusKepemilikanSarpras($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StatusKepemilikanSarpras');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StatusKepemilikanSarpras');
        }

        return $this;
    }

    /**
     * Use the StatusKepemilikanSarpras relation StatusKepemilikanSarpras object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\StatusKepemilikanSarprasQuery A secondary query class using the current class as primary query
     */
    public function useStatusKepemilikanSarprasQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStatusKepemilikanSarpras($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StatusKepemilikanSarpras', '\Admin\Model\StatusKepemilikanSarprasQuery');
    }

    /**
     * Filter the query by a related BukuAlat object
     *
     * @param   BukuAlat|PropelObjectCollection $bukuAlat  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByBukuAlat($bukuAlat, $comparison = null)
    {
        if ($bukuAlat instanceof BukuAlat) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $bukuAlat->getPrasaranaId(), $comparison);
        } elseif ($bukuAlat instanceof PropelObjectCollection) {
            return $this
                ->useBukuAlatQuery()
                ->filterByPrimaryKeys($bukuAlat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBukuAlat() only accepts arguments of type BukuAlat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BukuAlat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinBukuAlat($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BukuAlat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BukuAlat');
        }

        return $this;
    }

    /**
     * Use the BukuAlat relation BukuAlat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\BukuAlatQuery A secondary query class using the current class as primary query
     */
    public function useBukuAlatQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBukuAlat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BukuAlat', '\Admin\Model\BukuAlatQuery');
    }

    /**
     * Filter the query by a related Jadwal object
     *
     * @param   Jadwal|PropelObjectCollection $jadwal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByJadwal($jadwal, $comparison = null)
    {
        if ($jadwal instanceof Jadwal) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $jadwal->getPrasaranaId(), $comparison);
        } elseif ($jadwal instanceof PropelObjectCollection) {
            return $this
                ->useJadwalQuery()
                ->filterByPrimaryKeys($jadwal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByJadwal() only accepts arguments of type Jadwal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Jadwal relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinJadwal($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Jadwal');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Jadwal');
        }

        return $this;
    }

    /**
     * Use the Jadwal relation Jadwal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\JadwalQuery A secondary query class using the current class as primary query
     */
    public function useJadwalQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinJadwal($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Jadwal', '\Admin\Model\JadwalQuery');
    }

    /**
     * Filter the query by a related VldPrasarana object
     *
     * @param   VldPrasarana|PropelObjectCollection $vldPrasarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByVldPrasarana($vldPrasarana, $comparison = null)
    {
        if ($vldPrasarana instanceof VldPrasarana) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $vldPrasarana->getPrasaranaId(), $comparison);
        } elseif ($vldPrasarana instanceof PropelObjectCollection) {
            return $this
                ->useVldPrasaranaQuery()
                ->filterByPrimaryKeys($vldPrasarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVldPrasarana() only accepts arguments of type VldPrasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VldPrasarana relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinVldPrasarana($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VldPrasarana');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VldPrasarana');
        }

        return $this;
    }

    /**
     * Use the VldPrasarana relation VldPrasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\VldPrasaranaQuery A secondary query class using the current class as primary query
     */
    public function useVldPrasaranaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVldPrasarana($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VldPrasarana', '\Admin\Model\VldPrasaranaQuery');
    }

    /**
     * Filter the query by a related Prasarana object
     *
     * @param   Prasarana|PropelObjectCollection $prasarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaRelatedByPrasaranaId($prasarana, $comparison = null)
    {
        if ($prasarana instanceof Prasarana) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $prasarana->getIdIndukPrasarana(), $comparison);
        } elseif ($prasarana instanceof PropelObjectCollection) {
            return $this
                ->usePrasaranaRelatedByPrasaranaIdQuery()
                ->filterByPrimaryKeys($prasarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrasaranaRelatedByPrasaranaId() only accepts arguments of type Prasarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaRelatedByPrasaranaId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinPrasaranaRelatedByPrasaranaId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaRelatedByPrasaranaId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaRelatedByPrasaranaId');
        }

        return $this;
    }

    /**
     * Use the PrasaranaRelatedByPrasaranaId relation Prasarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\PrasaranaQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaRelatedByPrasaranaIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPrasaranaRelatedByPrasaranaId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaRelatedByPrasaranaId', '\Admin\Model\PrasaranaQuery');
    }

    /**
     * Filter the query by a related PrasaranaLongitudinal object
     *
     * @param   PrasaranaLongitudinal|PropelObjectCollection $prasaranaLongitudinal  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrasaranaLongitudinal($prasaranaLongitudinal, $comparison = null)
    {
        if ($prasaranaLongitudinal instanceof PrasaranaLongitudinal) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $prasaranaLongitudinal->getPrasaranaId(), $comparison);
        } elseif ($prasaranaLongitudinal instanceof PropelObjectCollection) {
            return $this
                ->usePrasaranaLongitudinalQuery()
                ->filterByPrimaryKeys($prasaranaLongitudinal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrasaranaLongitudinal() only accepts arguments of type PrasaranaLongitudinal or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PrasaranaLongitudinal relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinPrasaranaLongitudinal($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PrasaranaLongitudinal');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PrasaranaLongitudinal');
        }

        return $this;
    }

    /**
     * Use the PrasaranaLongitudinal relation PrasaranaLongitudinal object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\PrasaranaLongitudinalQuery A secondary query class using the current class as primary query
     */
    public function usePrasaranaLongitudinalQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrasaranaLongitudinal($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PrasaranaLongitudinal', '\Admin\Model\PrasaranaLongitudinalQuery');
    }

    /**
     * Filter the query by a related RombonganBelajar object
     *
     * @param   RombonganBelajar|PropelObjectCollection $rombonganBelajar  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRombonganBelajar($rombonganBelajar, $comparison = null)
    {
        if ($rombonganBelajar instanceof RombonganBelajar) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $rombonganBelajar->getPrasaranaId(), $comparison);
        } elseif ($rombonganBelajar instanceof PropelObjectCollection) {
            return $this
                ->useRombonganBelajarQuery()
                ->filterByPrimaryKeys($rombonganBelajar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRombonganBelajar() only accepts arguments of type RombonganBelajar or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RombonganBelajar relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinRombonganBelajar($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RombonganBelajar');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RombonganBelajar');
        }

        return $this;
    }

    /**
     * Use the RombonganBelajar relation RombonganBelajar object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\RombonganBelajarQuery A secondary query class using the current class as primary query
     */
    public function useRombonganBelajarQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRombonganBelajar($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RombonganBelajar', '\Admin\Model\RombonganBelajarQuery');
    }

    /**
     * Filter the query by a related Sarana object
     *
     * @param   Sarana|PropelObjectCollection $sarana  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrasaranaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterBySarana($sarana, $comparison = null)
    {
        if ($sarana instanceof Sarana) {
            return $this
                ->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $sarana->getPrasaranaId(), $comparison);
        } elseif ($sarana instanceof PropelObjectCollection) {
            return $this
                ->useSaranaQuery()
                ->filterByPrimaryKeys($sarana->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySarana() only accepts arguments of type Sarana or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Sarana relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function joinSarana($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Sarana');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Sarana');
        }

        return $this;
    }

    /**
     * Use the Sarana relation Sarana object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Admin\Model\SaranaQuery A secondary query class using the current class as primary query
     */
    public function useSaranaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSarana($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Sarana', '\Admin\Model\SaranaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Prasarana $prasarana Object to remove from the list of results
     *
     * @return PrasaranaQuery The current query, for fluid interface
     */
    public function prune($prasarana = null)
    {
        if ($prasarana) {
            $this->addUsingAlias(PrasaranaPeer::PRASARANA_ID, $prasarana->getPrasaranaId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
