<?php

namespace Admin\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use Admin\Model\DudiPeer;
use Admin\Model\LembagaAkreditasiPeer;
use Admin\Model\LembagaNonSekolahPeer;
use Admin\Model\MstWilayahPeer;
use Admin\Model\Pengguna;
use Admin\Model\PenggunaPeer;
use Admin\Model\PeranPeer;
use Admin\Model\YayasanPeer;
use Admin\Model\map\PenggunaTableMap;

/**
 * Base static class for performing query and update operations on the 'pengguna' table.
 *
 *
 *
 * @package propel.generator.Admin.Model.om
 */
abstract class BasePenggunaPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'Dapodik_Paudni';

    /** the table name for this class */
    const TABLE_NAME = 'pengguna';

    /** the related Propel class for this table */
    const OM_CLASS = 'Admin\\Model\\Pengguna';

    /** the related TableMap class for this table */
    const TM_CLASS = 'PenggunaTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 24;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 24;

    /** the column name for the pengguna_id field */
    const PENGGUNA_ID = 'pengguna.pengguna_id';

    /** the column name for the username field */
    const USERNAME = 'pengguna.username';

    /** the column name for the password field */
    const PASSWORD = 'pengguna.password';

    /** the column name for the nama field */
    const NAMA = 'pengguna.nama';

    /** the column name for the nip_nim field */
    const NIP_NIM = 'pengguna.nip_nim';

    /** the column name for the jabatan_lembaga field */
    const JABATAN_LEMBAGA = 'pengguna.jabatan_lembaga';

    /** the column name for the ym field */
    const YM = 'pengguna.ym';

    /** the column name for the skype field */
    const SKYPE = 'pengguna.skype';

    /** the column name for the alamat field */
    const ALAMAT = 'pengguna.alamat';

    /** the column name for the kode_wilayah field */
    const KODE_WILAYAH = 'pengguna.kode_wilayah';

    /** the column name for the no_telepon field */
    const NO_TELEPON = 'pengguna.no_telepon';

    /** the column name for the no_hp field */
    const NO_HP = 'pengguna.no_hp';

    /** the column name for the aktif field */
    const AKTIF = 'pengguna.aktif';

    /** the column name for the ptk_id field */
    const PTK_ID = 'pengguna.ptk_id';

    /** the column name for the peran_id field */
    const PERAN_ID = 'pengguna.peran_id';

    /** the column name for the sekolah_id field */
    const SEKOLAH_ID = 'pengguna.sekolah_id';

    /** the column name for the lembaga_id field */
    const LEMBAGA_ID = 'pengguna.lembaga_id';

    /** the column name for the yayasan_id field */
    const YAYASAN_ID = 'pengguna.yayasan_id';

    /** the column name for the la_id field */
    const LA_ID = 'pengguna.la_id';

    /** the column name for the dudi_id field */
    const DUDI_ID = 'pengguna.dudi_id';

    /** the column name for the Last_update field */
    const LAST_UPDATE = 'pengguna.Last_update';

    /** the column name for the Soft_delete field */
    const SOFT_DELETE = 'pengguna.Soft_delete';

    /** the column name for the last_sync field */
    const LAST_SYNC = 'pengguna.last_sync';

    /** the column name for the Updater_ID field */
    const UPDATER_ID = 'pengguna.Updater_ID';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of Pengguna objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Pengguna[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. PenggunaPeer::$fieldNames[PenggunaPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('PenggunaId', 'Username', 'Password', 'Nama', 'NipNim', 'JabatanLembaga', 'Ym', 'Skype', 'Alamat', 'KodeWilayah', 'NoTelepon', 'NoHp', 'Aktif', 'PtkId', 'PeranId', 'SekolahId', 'LembagaId', 'YayasanId', 'LaId', 'DudiId', 'LastUpdate', 'SoftDelete', 'LastSync', 'UpdaterId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('penggunaId', 'username', 'password', 'nama', 'nipNim', 'jabatanLembaga', 'ym', 'skype', 'alamat', 'kodeWilayah', 'noTelepon', 'noHp', 'aktif', 'ptkId', 'peranId', 'sekolahId', 'lembagaId', 'yayasanId', 'laId', 'dudiId', 'lastUpdate', 'softDelete', 'lastSync', 'updaterId', ),
        BasePeer::TYPE_COLNAME => array (PenggunaPeer::PENGGUNA_ID, PenggunaPeer::USERNAME, PenggunaPeer::PASSWORD, PenggunaPeer::NAMA, PenggunaPeer::NIP_NIM, PenggunaPeer::JABATAN_LEMBAGA, PenggunaPeer::YM, PenggunaPeer::SKYPE, PenggunaPeer::ALAMAT, PenggunaPeer::KODE_WILAYAH, PenggunaPeer::NO_TELEPON, PenggunaPeer::NO_HP, PenggunaPeer::AKTIF, PenggunaPeer::PTK_ID, PenggunaPeer::PERAN_ID, PenggunaPeer::SEKOLAH_ID, PenggunaPeer::LEMBAGA_ID, PenggunaPeer::YAYASAN_ID, PenggunaPeer::LA_ID, PenggunaPeer::DUDI_ID, PenggunaPeer::LAST_UPDATE, PenggunaPeer::SOFT_DELETE, PenggunaPeer::LAST_SYNC, PenggunaPeer::UPDATER_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('PENGGUNA_ID', 'USERNAME', 'PASSWORD', 'NAMA', 'NIP_NIM', 'JABATAN_LEMBAGA', 'YM', 'SKYPE', 'ALAMAT', 'KODE_WILAYAH', 'NO_TELEPON', 'NO_HP', 'AKTIF', 'PTK_ID', 'PERAN_ID', 'SEKOLAH_ID', 'LEMBAGA_ID', 'YAYASAN_ID', 'LA_ID', 'DUDI_ID', 'LAST_UPDATE', 'SOFT_DELETE', 'LAST_SYNC', 'UPDATER_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('pengguna_id', 'username', 'password', 'nama', 'nip_nim', 'jabatan_lembaga', 'ym', 'skype', 'alamat', 'kode_wilayah', 'no_telepon', 'no_hp', 'aktif', 'ptk_id', 'peran_id', 'sekolah_id', 'lembaga_id', 'yayasan_id', 'la_id', 'dudi_id', 'Last_update', 'Soft_delete', 'last_sync', 'Updater_ID', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. PenggunaPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('PenggunaId' => 0, 'Username' => 1, 'Password' => 2, 'Nama' => 3, 'NipNim' => 4, 'JabatanLembaga' => 5, 'Ym' => 6, 'Skype' => 7, 'Alamat' => 8, 'KodeWilayah' => 9, 'NoTelepon' => 10, 'NoHp' => 11, 'Aktif' => 12, 'PtkId' => 13, 'PeranId' => 14, 'SekolahId' => 15, 'LembagaId' => 16, 'YayasanId' => 17, 'LaId' => 18, 'DudiId' => 19, 'LastUpdate' => 20, 'SoftDelete' => 21, 'LastSync' => 22, 'UpdaterId' => 23, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('penggunaId' => 0, 'username' => 1, 'password' => 2, 'nama' => 3, 'nipNim' => 4, 'jabatanLembaga' => 5, 'ym' => 6, 'skype' => 7, 'alamat' => 8, 'kodeWilayah' => 9, 'noTelepon' => 10, 'noHp' => 11, 'aktif' => 12, 'ptkId' => 13, 'peranId' => 14, 'sekolahId' => 15, 'lembagaId' => 16, 'yayasanId' => 17, 'laId' => 18, 'dudiId' => 19, 'lastUpdate' => 20, 'softDelete' => 21, 'lastSync' => 22, 'updaterId' => 23, ),
        BasePeer::TYPE_COLNAME => array (PenggunaPeer::PENGGUNA_ID => 0, PenggunaPeer::USERNAME => 1, PenggunaPeer::PASSWORD => 2, PenggunaPeer::NAMA => 3, PenggunaPeer::NIP_NIM => 4, PenggunaPeer::JABATAN_LEMBAGA => 5, PenggunaPeer::YM => 6, PenggunaPeer::SKYPE => 7, PenggunaPeer::ALAMAT => 8, PenggunaPeer::KODE_WILAYAH => 9, PenggunaPeer::NO_TELEPON => 10, PenggunaPeer::NO_HP => 11, PenggunaPeer::AKTIF => 12, PenggunaPeer::PTK_ID => 13, PenggunaPeer::PERAN_ID => 14, PenggunaPeer::SEKOLAH_ID => 15, PenggunaPeer::LEMBAGA_ID => 16, PenggunaPeer::YAYASAN_ID => 17, PenggunaPeer::LA_ID => 18, PenggunaPeer::DUDI_ID => 19, PenggunaPeer::LAST_UPDATE => 20, PenggunaPeer::SOFT_DELETE => 21, PenggunaPeer::LAST_SYNC => 22, PenggunaPeer::UPDATER_ID => 23, ),
        BasePeer::TYPE_RAW_COLNAME => array ('PENGGUNA_ID' => 0, 'USERNAME' => 1, 'PASSWORD' => 2, 'NAMA' => 3, 'NIP_NIM' => 4, 'JABATAN_LEMBAGA' => 5, 'YM' => 6, 'SKYPE' => 7, 'ALAMAT' => 8, 'KODE_WILAYAH' => 9, 'NO_TELEPON' => 10, 'NO_HP' => 11, 'AKTIF' => 12, 'PTK_ID' => 13, 'PERAN_ID' => 14, 'SEKOLAH_ID' => 15, 'LEMBAGA_ID' => 16, 'YAYASAN_ID' => 17, 'LA_ID' => 18, 'DUDI_ID' => 19, 'LAST_UPDATE' => 20, 'SOFT_DELETE' => 21, 'LAST_SYNC' => 22, 'UPDATER_ID' => 23, ),
        BasePeer::TYPE_FIELDNAME => array ('pengguna_id' => 0, 'username' => 1, 'password' => 2, 'nama' => 3, 'nip_nim' => 4, 'jabatan_lembaga' => 5, 'ym' => 6, 'skype' => 7, 'alamat' => 8, 'kode_wilayah' => 9, 'no_telepon' => 10, 'no_hp' => 11, 'aktif' => 12, 'ptk_id' => 13, 'peran_id' => 14, 'sekolah_id' => 15, 'lembaga_id' => 16, 'yayasan_id' => 17, 'la_id' => 18, 'dudi_id' => 19, 'Last_update' => 20, 'Soft_delete' => 21, 'last_sync' => 22, 'Updater_ID' => 23, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = PenggunaPeer::getFieldNames($toType);
        $key = isset(PenggunaPeer::$fieldKeys[$fromType][$name]) ? PenggunaPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(PenggunaPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, PenggunaPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return PenggunaPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. PenggunaPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(PenggunaPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PenggunaPeer::PENGGUNA_ID);
            $criteria->addSelectColumn(PenggunaPeer::USERNAME);
            $criteria->addSelectColumn(PenggunaPeer::PASSWORD);
            $criteria->addSelectColumn(PenggunaPeer::NAMA);
            $criteria->addSelectColumn(PenggunaPeer::NIP_NIM);
            $criteria->addSelectColumn(PenggunaPeer::JABATAN_LEMBAGA);
            $criteria->addSelectColumn(PenggunaPeer::YM);
            $criteria->addSelectColumn(PenggunaPeer::SKYPE);
            $criteria->addSelectColumn(PenggunaPeer::ALAMAT);
            $criteria->addSelectColumn(PenggunaPeer::KODE_WILAYAH);
            $criteria->addSelectColumn(PenggunaPeer::NO_TELEPON);
            $criteria->addSelectColumn(PenggunaPeer::NO_HP);
            $criteria->addSelectColumn(PenggunaPeer::AKTIF);
            $criteria->addSelectColumn(PenggunaPeer::PTK_ID);
            $criteria->addSelectColumn(PenggunaPeer::PERAN_ID);
            $criteria->addSelectColumn(PenggunaPeer::SEKOLAH_ID);
            $criteria->addSelectColumn(PenggunaPeer::LEMBAGA_ID);
            $criteria->addSelectColumn(PenggunaPeer::YAYASAN_ID);
            $criteria->addSelectColumn(PenggunaPeer::LA_ID);
            $criteria->addSelectColumn(PenggunaPeer::DUDI_ID);
            $criteria->addSelectColumn(PenggunaPeer::LAST_UPDATE);
            $criteria->addSelectColumn(PenggunaPeer::SOFT_DELETE);
            $criteria->addSelectColumn(PenggunaPeer::LAST_SYNC);
            $criteria->addSelectColumn(PenggunaPeer::UPDATER_ID);
        } else {
            $criteria->addSelectColumn($alias . '.pengguna_id');
            $criteria->addSelectColumn($alias . '.username');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.nama');
            $criteria->addSelectColumn($alias . '.nip_nim');
            $criteria->addSelectColumn($alias . '.jabatan_lembaga');
            $criteria->addSelectColumn($alias . '.ym');
            $criteria->addSelectColumn($alias . '.skype');
            $criteria->addSelectColumn($alias . '.alamat');
            $criteria->addSelectColumn($alias . '.kode_wilayah');
            $criteria->addSelectColumn($alias . '.no_telepon');
            $criteria->addSelectColumn($alias . '.no_hp');
            $criteria->addSelectColumn($alias . '.aktif');
            $criteria->addSelectColumn($alias . '.ptk_id');
            $criteria->addSelectColumn($alias . '.peran_id');
            $criteria->addSelectColumn($alias . '.sekolah_id');
            $criteria->addSelectColumn($alias . '.lembaga_id');
            $criteria->addSelectColumn($alias . '.yayasan_id');
            $criteria->addSelectColumn($alias . '.la_id');
            $criteria->addSelectColumn($alias . '.dudi_id');
            $criteria->addSelectColumn($alias . '.Last_update');
            $criteria->addSelectColumn($alias . '.Soft_delete');
            $criteria->addSelectColumn($alias . '.last_sync');
            $criteria->addSelectColumn($alias . '.Updater_ID');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 Pengguna
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = PenggunaPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return PenggunaPeer::populateObjects(PenggunaPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            PenggunaPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      Pengguna $obj A Pengguna object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getPenggunaId();
            } // if key === null
            PenggunaPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Pengguna object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Pengguna) {
                $key = (string) $value->getPenggunaId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Pengguna object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(PenggunaPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   Pengguna Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(PenggunaPeer::$instances[$key])) {
                return PenggunaPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (PenggunaPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        PenggunaPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to pengguna
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (string) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = PenggunaPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = PenggunaPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PenggunaPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Pengguna object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = PenggunaPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + PenggunaPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PenggunaPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            PenggunaPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related Dudi table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinDudi(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related LembagaNonSekolah table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinLembagaNonSekolah(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Yayasan table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinYayasan(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related LembagaAkreditasi table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinLembagaAkreditasi(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related MstWilayah table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinMstWilayah(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Peran table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinPeran(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of Pengguna objects pre-filled with their Dudi objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinDudi(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol = PenggunaPeer::NUM_HYDRATE_COLUMNS;
        DudiPeer::addSelectColumns($criteria);

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = DudiPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = DudiPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = DudiPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    DudiPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Pengguna) to $obj2 (Dudi)
                $obj2->addPengguna($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Pengguna objects pre-filled with their LembagaNonSekolah objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinLembagaNonSekolah(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol = PenggunaPeer::NUM_HYDRATE_COLUMNS;
        LembagaNonSekolahPeer::addSelectColumns($criteria);

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = LembagaNonSekolahPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = LembagaNonSekolahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = LembagaNonSekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    LembagaNonSekolahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Pengguna) to $obj2 (LembagaNonSekolah)
                $obj2->addPengguna($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Pengguna objects pre-filled with their Yayasan objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinYayasan(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol = PenggunaPeer::NUM_HYDRATE_COLUMNS;
        YayasanPeer::addSelectColumns($criteria);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = YayasanPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = YayasanPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = YayasanPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    YayasanPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Pengguna) to $obj2 (Yayasan)
                $obj2->addPengguna($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Pengguna objects pre-filled with their LembagaAkreditasi objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinLembagaAkreditasi(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol = PenggunaPeer::NUM_HYDRATE_COLUMNS;
        LembagaAkreditasiPeer::addSelectColumns($criteria);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = LembagaAkreditasiPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = LembagaAkreditasiPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = LembagaAkreditasiPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    LembagaAkreditasiPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Pengguna) to $obj2 (LembagaAkreditasi)
                $obj2->addPengguna($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Pengguna objects pre-filled with their MstWilayah objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinMstWilayah(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol = PenggunaPeer::NUM_HYDRATE_COLUMNS;
        MstWilayahPeer::addSelectColumns($criteria);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = MstWilayahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = MstWilayahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    MstWilayahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Pengguna) to $obj2 (MstWilayah)
                $obj2->addPengguna($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Pengguna objects pre-filled with their Peran objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinPeran(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol = PenggunaPeer::NUM_HYDRATE_COLUMNS;
        PeranPeer::addSelectColumns($criteria);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = PeranPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = PeranPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PeranPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    PeranPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Pengguna) to $obj2 (Peran)
                $obj2->addPengguna($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of Pengguna objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol2 = PenggunaPeer::NUM_HYDRATE_COLUMNS;

        DudiPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DudiPeer::NUM_HYDRATE_COLUMNS;

        LembagaNonSekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + LembagaNonSekolahPeer::NUM_HYDRATE_COLUMNS;

        YayasanPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + YayasanPeer::NUM_HYDRATE_COLUMNS;

        LembagaAkreditasiPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + LembagaAkreditasiPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        PeranPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + PeranPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Dudi rows

            $key2 = DudiPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = DudiPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = DudiPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DudiPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (Pengguna) to the collection in $obj2 (Dudi)
                $obj2->addPengguna($obj1);
            } // if joined row not null

            // Add objects for joined LembagaNonSekolah rows

            $key3 = LembagaNonSekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = LembagaNonSekolahPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = LembagaNonSekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    LembagaNonSekolahPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (Pengguna) to the collection in $obj3 (LembagaNonSekolah)
                $obj3->addPengguna($obj1);
            } // if joined row not null

            // Add objects for joined Yayasan rows

            $key4 = YayasanPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = YayasanPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = YayasanPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    YayasanPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (Pengguna) to the collection in $obj4 (Yayasan)
                $obj4->addPengguna($obj1);
            } // if joined row not null

            // Add objects for joined LembagaAkreditasi rows

            $key5 = LembagaAkreditasiPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = LembagaAkreditasiPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = LembagaAkreditasiPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    LembagaAkreditasiPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (Pengguna) to the collection in $obj5 (LembagaAkreditasi)
                $obj5->addPengguna($obj1);
            } // if joined row not null

            // Add objects for joined MstWilayah rows

            $key6 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol6);
            if ($key6 !== null) {
                $obj6 = MstWilayahPeer::getInstanceFromPool($key6);
                if (!$obj6) {

                    $cls = MstWilayahPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    MstWilayahPeer::addInstanceToPool($obj6, $key6);
                } // if obj6 loaded

                // Add the $obj1 (Pengguna) to the collection in $obj6 (MstWilayah)
                $obj6->addPengguna($obj1);
            } // if joined row not null

            // Add objects for joined Peran rows

            $key7 = PeranPeer::getPrimaryKeyHashFromRow($row, $startcol7);
            if ($key7 !== null) {
                $obj7 = PeranPeer::getInstanceFromPool($key7);
                if (!$obj7) {

                    $cls = PeranPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    PeranPeer::addInstanceToPool($obj7, $key7);
                } // if obj7 loaded

                // Add the $obj1 (Pengguna) to the collection in $obj7 (Peran)
                $obj7->addPengguna($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Dudi table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptDudi(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related LembagaNonSekolah table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptLembagaNonSekolah(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Yayasan table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptYayasan(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related LembagaAkreditasi table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptLembagaAkreditasi(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related MstWilayah table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptMstWilayah(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Peran table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptPeran(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PenggunaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of Pengguna objects pre-filled with all related objects except Dudi.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptDudi(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol2 = PenggunaPeer::NUM_HYDRATE_COLUMNS;

        LembagaNonSekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + LembagaNonSekolahPeer::NUM_HYDRATE_COLUMNS;

        YayasanPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + YayasanPeer::NUM_HYDRATE_COLUMNS;

        LembagaAkreditasiPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + LembagaAkreditasiPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        PeranPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + PeranPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined LembagaNonSekolah rows

                $key2 = LembagaNonSekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = LembagaNonSekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = LembagaNonSekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    LembagaNonSekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj2 (LembagaNonSekolah)
                $obj2->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined Yayasan rows

                $key3 = YayasanPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = YayasanPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = YayasanPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    YayasanPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj3 (Yayasan)
                $obj3->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined LembagaAkreditasi rows

                $key4 = LembagaAkreditasiPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = LembagaAkreditasiPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = LembagaAkreditasiPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    LembagaAkreditasiPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj4 (LembagaAkreditasi)
                $obj4->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined MstWilayah rows

                $key5 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = MstWilayahPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = MstWilayahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    MstWilayahPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj5 (MstWilayah)
                $obj5->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined Peran rows

                $key6 = PeranPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = PeranPeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = PeranPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    PeranPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj6 (Peran)
                $obj6->addPengguna($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Pengguna objects pre-filled with all related objects except LembagaNonSekolah.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptLembagaNonSekolah(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol2 = PenggunaPeer::NUM_HYDRATE_COLUMNS;

        DudiPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DudiPeer::NUM_HYDRATE_COLUMNS;

        YayasanPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + YayasanPeer::NUM_HYDRATE_COLUMNS;

        LembagaAkreditasiPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + LembagaAkreditasiPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        PeranPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + PeranPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Dudi rows

                $key2 = DudiPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = DudiPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = DudiPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DudiPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj2 (Dudi)
                $obj2->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined Yayasan rows

                $key3 = YayasanPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = YayasanPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = YayasanPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    YayasanPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj3 (Yayasan)
                $obj3->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined LembagaAkreditasi rows

                $key4 = LembagaAkreditasiPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = LembagaAkreditasiPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = LembagaAkreditasiPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    LembagaAkreditasiPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj4 (LembagaAkreditasi)
                $obj4->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined MstWilayah rows

                $key5 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = MstWilayahPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = MstWilayahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    MstWilayahPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj5 (MstWilayah)
                $obj5->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined Peran rows

                $key6 = PeranPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = PeranPeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = PeranPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    PeranPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj6 (Peran)
                $obj6->addPengguna($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Pengguna objects pre-filled with all related objects except Yayasan.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptYayasan(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol2 = PenggunaPeer::NUM_HYDRATE_COLUMNS;

        DudiPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DudiPeer::NUM_HYDRATE_COLUMNS;

        LembagaNonSekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + LembagaNonSekolahPeer::NUM_HYDRATE_COLUMNS;

        LembagaAkreditasiPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + LembagaAkreditasiPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        PeranPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + PeranPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Dudi rows

                $key2 = DudiPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = DudiPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = DudiPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DudiPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj2 (Dudi)
                $obj2->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined LembagaNonSekolah rows

                $key3 = LembagaNonSekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = LembagaNonSekolahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = LembagaNonSekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    LembagaNonSekolahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj3 (LembagaNonSekolah)
                $obj3->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined LembagaAkreditasi rows

                $key4 = LembagaAkreditasiPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = LembagaAkreditasiPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = LembagaAkreditasiPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    LembagaAkreditasiPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj4 (LembagaAkreditasi)
                $obj4->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined MstWilayah rows

                $key5 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = MstWilayahPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = MstWilayahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    MstWilayahPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj5 (MstWilayah)
                $obj5->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined Peran rows

                $key6 = PeranPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = PeranPeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = PeranPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    PeranPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj6 (Peran)
                $obj6->addPengguna($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Pengguna objects pre-filled with all related objects except LembagaAkreditasi.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptLembagaAkreditasi(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol2 = PenggunaPeer::NUM_HYDRATE_COLUMNS;

        DudiPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DudiPeer::NUM_HYDRATE_COLUMNS;

        LembagaNonSekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + LembagaNonSekolahPeer::NUM_HYDRATE_COLUMNS;

        YayasanPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + YayasanPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        PeranPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + PeranPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Dudi rows

                $key2 = DudiPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = DudiPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = DudiPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DudiPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj2 (Dudi)
                $obj2->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined LembagaNonSekolah rows

                $key3 = LembagaNonSekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = LembagaNonSekolahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = LembagaNonSekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    LembagaNonSekolahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj3 (LembagaNonSekolah)
                $obj3->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined Yayasan rows

                $key4 = YayasanPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = YayasanPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = YayasanPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    YayasanPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj4 (Yayasan)
                $obj4->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined MstWilayah rows

                $key5 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = MstWilayahPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = MstWilayahPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    MstWilayahPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj5 (MstWilayah)
                $obj5->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined Peran rows

                $key6 = PeranPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = PeranPeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = PeranPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    PeranPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj6 (Peran)
                $obj6->addPengguna($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Pengguna objects pre-filled with all related objects except MstWilayah.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptMstWilayah(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol2 = PenggunaPeer::NUM_HYDRATE_COLUMNS;

        DudiPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DudiPeer::NUM_HYDRATE_COLUMNS;

        LembagaNonSekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + LembagaNonSekolahPeer::NUM_HYDRATE_COLUMNS;

        YayasanPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + YayasanPeer::NUM_HYDRATE_COLUMNS;

        LembagaAkreditasiPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + LembagaAkreditasiPeer::NUM_HYDRATE_COLUMNS;

        PeranPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + PeranPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::PERAN_ID, PeranPeer::PERAN_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Dudi rows

                $key2 = DudiPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = DudiPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = DudiPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DudiPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj2 (Dudi)
                $obj2->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined LembagaNonSekolah rows

                $key3 = LembagaNonSekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = LembagaNonSekolahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = LembagaNonSekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    LembagaNonSekolahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj3 (LembagaNonSekolah)
                $obj3->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined Yayasan rows

                $key4 = YayasanPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = YayasanPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = YayasanPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    YayasanPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj4 (Yayasan)
                $obj4->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined LembagaAkreditasi rows

                $key5 = LembagaAkreditasiPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = LembagaAkreditasiPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = LembagaAkreditasiPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    LembagaAkreditasiPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj5 (LembagaAkreditasi)
                $obj5->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined Peran rows

                $key6 = PeranPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = PeranPeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = PeranPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    PeranPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj6 (Peran)
                $obj6->addPengguna($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Pengguna objects pre-filled with all related objects except Peran.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Pengguna objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptPeran(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PenggunaPeer::DATABASE_NAME);
        }

        PenggunaPeer::addSelectColumns($criteria);
        $startcol2 = PenggunaPeer::NUM_HYDRATE_COLUMNS;

        DudiPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + DudiPeer::NUM_HYDRATE_COLUMNS;

        LembagaNonSekolahPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + LembagaNonSekolahPeer::NUM_HYDRATE_COLUMNS;

        YayasanPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + YayasanPeer::NUM_HYDRATE_COLUMNS;

        LembagaAkreditasiPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + LembagaAkreditasiPeer::NUM_HYDRATE_COLUMNS;

        MstWilayahPeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + MstWilayahPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PenggunaPeer::DUDI_ID, DudiPeer::DUDI_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LEMBAGA_ID, LembagaNonSekolahPeer::LEMBAGA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::YAYASAN_ID, YayasanPeer::YAYASAN_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::LA_ID, LembagaAkreditasiPeer::LA_ID, $join_behavior);

        $criteria->addJoin(PenggunaPeer::KODE_WILAYAH, MstWilayahPeer::KODE_WILAYAH, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PenggunaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PenggunaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PenggunaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PenggunaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Dudi rows

                $key2 = DudiPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = DudiPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = DudiPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    DudiPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj2 (Dudi)
                $obj2->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined LembagaNonSekolah rows

                $key3 = LembagaNonSekolahPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = LembagaNonSekolahPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = LembagaNonSekolahPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    LembagaNonSekolahPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj3 (LembagaNonSekolah)
                $obj3->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined Yayasan rows

                $key4 = YayasanPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = YayasanPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = YayasanPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    YayasanPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj4 (Yayasan)
                $obj4->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined LembagaAkreditasi rows

                $key5 = LembagaAkreditasiPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = LembagaAkreditasiPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = LembagaAkreditasiPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    LembagaAkreditasiPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj5 (LembagaAkreditasi)
                $obj5->addPengguna($obj1);

            } // if joined row is not null

                // Add objects for joined MstWilayah rows

                $key6 = MstWilayahPeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = MstWilayahPeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = MstWilayahPeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    MstWilayahPeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (Pengguna) to the collection in $obj6 (MstWilayah)
                $obj6->addPengguna($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(PenggunaPeer::DATABASE_NAME)->getTable(PenggunaPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BasePenggunaPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BasePenggunaPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new PenggunaTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return PenggunaPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Pengguna or Criteria object.
     *
     * @param      mixed $values Criteria or Pengguna object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Pengguna object
        }


        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Pengguna or Criteria object.
     *
     * @param      mixed $values Criteria or Pengguna object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(PenggunaPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(PenggunaPeer::PENGGUNA_ID);
            $value = $criteria->remove(PenggunaPeer::PENGGUNA_ID);
            if ($value) {
                $selectCriteria->add(PenggunaPeer::PENGGUNA_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(PenggunaPeer::TABLE_NAME);
            }

        } else { // $values is Pengguna object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the pengguna table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(PenggunaPeer::TABLE_NAME, $con, PenggunaPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PenggunaPeer::clearInstancePool();
            PenggunaPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Pengguna or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Pengguna object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            PenggunaPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Pengguna) { // it's a model object
            // invalidate the cache for this single object
            PenggunaPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PenggunaPeer::DATABASE_NAME);
            $criteria->add(PenggunaPeer::PENGGUNA_ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                PenggunaPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(PenggunaPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            PenggunaPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Pengguna object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      Pengguna $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(PenggunaPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(PenggunaPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(PenggunaPeer::DATABASE_NAME, PenggunaPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      string $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Pengguna
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = PenggunaPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(PenggunaPeer::DATABASE_NAME);
        $criteria->add(PenggunaPeer::PENGGUNA_ID, $pk);

        $v = PenggunaPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Pengguna[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(PenggunaPeer::DATABASE_NAME);
            $criteria->add(PenggunaPeer::PENGGUNA_ID, $pks, Criteria::IN);
            $objs = PenggunaPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BasePenggunaPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BasePenggunaPeer::buildTableMap();

