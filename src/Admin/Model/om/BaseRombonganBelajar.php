<?php

namespace Admin\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Admin\Model\JurusanSp;
use Admin\Model\JurusanSpQuery;
use Admin\Model\KebutuhanKhusus;
use Admin\Model\KebutuhanKhususQuery;
use Admin\Model\Kurikulum;
use Admin\Model\KurikulumQuery;
use Admin\Model\Pembelajaran;
use Admin\Model\PembelajaranQuery;
use Admin\Model\Prasarana;
use Admin\Model\PrasaranaQuery;
use Admin\Model\Ptk;
use Admin\Model\PtkQuery;
use Admin\Model\RombonganBelajar;
use Admin\Model\RombonganBelajarPeer;
use Admin\Model\RombonganBelajarQuery;
use Admin\Model\Sekolah;
use Admin\Model\SekolahQuery;
use Admin\Model\Semester;
use Admin\Model\SemesterQuery;
use Admin\Model\TingkatPendidikan;
use Admin\Model\TingkatPendidikanQuery;
use Admin\Model\VldRombel;
use Admin\Model\VldRombelQuery;

/**
 * Base class that represents a row from the 'rombongan_belajar' table.
 *
 *
 *
 * @package    propel.generator.Admin.Model.om
 */
abstract class BaseRombonganBelajar extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Admin\\Model\\RombonganBelajarPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        RombonganBelajarPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the rombongan_belajar_id field.
     * @var        string
     */
    protected $rombongan_belajar_id;

    /**
     * The value for the semester_id field.
     * @var        string
     */
    protected $semester_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the tingkat_pendidikan_id field.
     * @var        string
     */
    protected $tingkat_pendidikan_id;

    /**
     * The value for the jurusan_sp_id field.
     * @var        string
     */
    protected $jurusan_sp_id;

    /**
     * The value for the kurikulum_id field.
     * @var        int
     */
    protected $kurikulum_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the ptk_id field.
     * @var        string
     */
    protected $ptk_id;

    /**
     * The value for the prasarana_id field.
     * @var        string
     */
    protected $prasarana_id;

    /**
     * The value for the moving_class field.
     * @var        string
     */
    protected $moving_class;

    /**
     * The value for the jenis_rombel field.
     * @var        string
     */
    protected $jenis_rombel;

    /**
     * The value for the sks field.
     * @var        string
     */
    protected $sks;

    /**
     * The value for the kebutuhan_khusus_id field.
     * @var        int
     */
    protected $kebutuhan_khusus_id;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        JurusanSp
     */
    protected $aJurusanSp;

    /**
     * @var        Prasarana
     */
    protected $aPrasarana;

    /**
     * @var        Ptk
     */
    protected $aPtk;

    /**
     * @var        Sekolah
     */
    protected $aSekolah;

    /**
     * @var        KebutuhanKhusus
     */
    protected $aKebutuhanKhusus;

    /**
     * @var        Kurikulum
     */
    protected $aKurikulum;

    /**
     * @var        Semester
     */
    protected $aSemester;

    /**
     * @var        TingkatPendidikan
     */
    protected $aTingkatPendidikan;

    /**
     * @var        PropelObjectCollection|VldRombel[] Collection to store aggregation of VldRombel objects.
     */
    protected $collVldRombels;
    protected $collVldRombelsPartial;

    /**
     * @var        PropelObjectCollection|Pembelajaran[] Collection to store aggregation of Pembelajaran objects.
     */
    protected $collPembelajarans;
    protected $collPembelajaransPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldRombelsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $pembelajaransScheduledForDeletion = null;

    /**
     * Get the [rombongan_belajar_id] column value.
     *
     * @return string
     */
    public function getRombonganBelajarId()
    {
        return $this->rombongan_belajar_id;
    }

    /**
     * Get the [semester_id] column value.
     *
     * @return string
     */
    public function getSemesterId()
    {
        return $this->semester_id;
    }

    /**
     * Get the [sekolah_id] column value.
     *
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [tingkat_pendidikan_id] column value.
     *
     * @return string
     */
    public function getTingkatPendidikanId()
    {
        return $this->tingkat_pendidikan_id;
    }

    /**
     * Get the [jurusan_sp_id] column value.
     *
     * @return string
     */
    public function getJurusanSpId()
    {
        return $this->jurusan_sp_id;
    }

    /**
     * Get the [kurikulum_id] column value.
     *
     * @return int
     */
    public function getKurikulumId()
    {
        return $this->kurikulum_id;
    }

    /**
     * Get the [nama] column value.
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [ptk_id] column value.
     *
     * @return string
     */
    public function getPtkId()
    {
        return $this->ptk_id;
    }

    /**
     * Get the [prasarana_id] column value.
     *
     * @return string
     */
    public function getPrasaranaId()
    {
        return $this->prasarana_id;
    }

    /**
     * Get the [moving_class] column value.
     *
     * @return string
     */
    public function getMovingClass()
    {
        return $this->moving_class;
    }

    /**
     * Get the [jenis_rombel] column value.
     *
     * @return string
     */
    public function getJenisRombel()
    {
        return $this->jenis_rombel;
    }

    /**
     * Get the [sks] column value.
     *
     * @return string
     */
    public function getSks()
    {
        return $this->sks;
    }

    /**
     * Get the [kebutuhan_khusus_id] column value.
     *
     * @return int
     */
    public function getKebutuhanKhususId()
    {
        return $this->kebutuhan_khusus_id;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [soft_delete] column value.
     *
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [updater_id] column value.
     *
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [rombongan_belajar_id] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setRombonganBelajarId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->rombongan_belajar_id !== $v) {
            $this->rombongan_belajar_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::ROMBONGAN_BELAJAR_ID;
        }


        return $this;
    } // setRombonganBelajarId()

    /**
     * Set the value of [semester_id] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setSemesterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->semester_id !== $v) {
            $this->semester_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::SEMESTER_ID;
        }

        if ($this->aSemester !== null && $this->aSemester->getSemesterId() !== $v) {
            $this->aSemester = null;
        }


        return $this;
    } // setSemesterId()

    /**
     * Set the value of [sekolah_id] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::SEKOLAH_ID;
        }

        if ($this->aSekolah !== null && $this->aSekolah->getSekolahId() !== $v) {
            $this->aSekolah = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [tingkat_pendidikan_id] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setTingkatPendidikanId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tingkat_pendidikan_id !== $v) {
            $this->tingkat_pendidikan_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::TINGKAT_PENDIDIKAN_ID;
        }

        if ($this->aTingkatPendidikan !== null && $this->aTingkatPendidikan->getTingkatPendidikanId() !== $v) {
            $this->aTingkatPendidikan = null;
        }


        return $this;
    } // setTingkatPendidikanId()

    /**
     * Set the value of [jurusan_sp_id] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setJurusanSpId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jurusan_sp_id !== $v) {
            $this->jurusan_sp_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::JURUSAN_SP_ID;
        }

        if ($this->aJurusanSp !== null && $this->aJurusanSp->getJurusanSpId() !== $v) {
            $this->aJurusanSp = null;
        }


        return $this;
    } // setJurusanSpId()

    /**
     * Set the value of [kurikulum_id] column.
     *
     * @param int $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setKurikulumId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->kurikulum_id !== $v) {
            $this->kurikulum_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::KURIKULUM_ID;
        }

        if ($this->aKurikulum !== null && $this->aKurikulum->getKurikulumId() !== $v) {
            $this->aKurikulum = null;
        }


        return $this;
    } // setKurikulumId()

    /**
     * Set the value of [nama] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [ptk_id] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setPtkId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ptk_id !== $v) {
            $this->ptk_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::PTK_ID;
        }

        if ($this->aPtk !== null && $this->aPtk->getPtkId() !== $v) {
            $this->aPtk = null;
        }


        return $this;
    } // setPtkId()

    /**
     * Set the value of [prasarana_id] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setPrasaranaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prasarana_id !== $v) {
            $this->prasarana_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::PRASARANA_ID;
        }

        if ($this->aPrasarana !== null && $this->aPrasarana->getPrasaranaId() !== $v) {
            $this->aPrasarana = null;
        }


        return $this;
    } // setPrasaranaId()

    /**
     * Set the value of [moving_class] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setMovingClass($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->moving_class !== $v) {
            $this->moving_class = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::MOVING_CLASS;
        }


        return $this;
    } // setMovingClass()

    /**
     * Set the value of [jenis_rombel] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setJenisRombel($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jenis_rombel !== $v) {
            $this->jenis_rombel = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::JENIS_ROMBEL;
        }


        return $this;
    } // setJenisRombel()

    /**
     * Set the value of [sks] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setSks($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sks !== $v) {
            $this->sks = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::SKS;
        }


        return $this;
    } // setSks()

    /**
     * Set the value of [kebutuhan_khusus_id] column.
     *
     * @param int $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setKebutuhanKhususId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->kebutuhan_khusus_id !== $v) {
            $this->kebutuhan_khusus_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::KEBUTUHAN_KHUSUS_ID;
        }

        if ($this->aKebutuhanKhusus !== null && $this->aKebutuhanKhusus->getKebutuhanKhususId() !== $v) {
            $this->aKebutuhanKhusus = null;
        }


        return $this;
    } // setKebutuhanKhususId()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = RombonganBelajarPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = RombonganBelajarPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     *
     * @param string $v new value
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = RombonganBelajarPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->rombongan_belajar_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->semester_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->sekolah_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->tingkat_pendidikan_id = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->jurusan_sp_id = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->kurikulum_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->nama = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->ptk_id = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->prasarana_id = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->moving_class = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->jenis_rombel = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->sks = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->kebutuhan_khusus_id = ($row[$startcol + 12] !== null) ? (int) $row[$startcol + 12] : null;
            $this->last_update = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->soft_delete = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->last_sync = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->updater_id = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 17; // 17 = RombonganBelajarPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating RombonganBelajar object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aSemester !== null && $this->semester_id !== $this->aSemester->getSemesterId()) {
            $this->aSemester = null;
        }
        if ($this->aSekolah !== null && $this->sekolah_id !== $this->aSekolah->getSekolahId()) {
            $this->aSekolah = null;
        }
        if ($this->aTingkatPendidikan !== null && $this->tingkat_pendidikan_id !== $this->aTingkatPendidikan->getTingkatPendidikanId()) {
            $this->aTingkatPendidikan = null;
        }
        if ($this->aJurusanSp !== null && $this->jurusan_sp_id !== $this->aJurusanSp->getJurusanSpId()) {
            $this->aJurusanSp = null;
        }
        if ($this->aKurikulum !== null && $this->kurikulum_id !== $this->aKurikulum->getKurikulumId()) {
            $this->aKurikulum = null;
        }
        if ($this->aPtk !== null && $this->ptk_id !== $this->aPtk->getPtkId()) {
            $this->aPtk = null;
        }
        if ($this->aPrasarana !== null && $this->prasarana_id !== $this->aPrasarana->getPrasaranaId()) {
            $this->aPrasarana = null;
        }
        if ($this->aKebutuhanKhusus !== null && $this->kebutuhan_khusus_id !== $this->aKebutuhanKhusus->getKebutuhanKhususId()) {
            $this->aKebutuhanKhusus = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RombonganBelajarPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = RombonganBelajarPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aJurusanSp = null;
            $this->aPrasarana = null;
            $this->aPtk = null;
            $this->aSekolah = null;
            $this->aKebutuhanKhusus = null;
            $this->aKurikulum = null;
            $this->aSemester = null;
            $this->aTingkatPendidikan = null;
            $this->collVldRombels = null;

            $this->collPembelajarans = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RombonganBelajarPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = RombonganBelajarQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RombonganBelajarPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                RombonganBelajarPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aJurusanSp !== null) {
                if ($this->aJurusanSp->isModified() || $this->aJurusanSp->isNew()) {
                    $affectedRows += $this->aJurusanSp->save($con);
                }
                $this->setJurusanSp($this->aJurusanSp);
            }

            if ($this->aPrasarana !== null) {
                if ($this->aPrasarana->isModified() || $this->aPrasarana->isNew()) {
                    $affectedRows += $this->aPrasarana->save($con);
                }
                $this->setPrasarana($this->aPrasarana);
            }

            if ($this->aPtk !== null) {
                if ($this->aPtk->isModified() || $this->aPtk->isNew()) {
                    $affectedRows += $this->aPtk->save($con);
                }
                $this->setPtk($this->aPtk);
            }

            if ($this->aSekolah !== null) {
                if ($this->aSekolah->isModified() || $this->aSekolah->isNew()) {
                    $affectedRows += $this->aSekolah->save($con);
                }
                $this->setSekolah($this->aSekolah);
            }

            if ($this->aKebutuhanKhusus !== null) {
                if ($this->aKebutuhanKhusus->isModified() || $this->aKebutuhanKhusus->isNew()) {
                    $affectedRows += $this->aKebutuhanKhusus->save($con);
                }
                $this->setKebutuhanKhusus($this->aKebutuhanKhusus);
            }

            if ($this->aKurikulum !== null) {
                if ($this->aKurikulum->isModified() || $this->aKurikulum->isNew()) {
                    $affectedRows += $this->aKurikulum->save($con);
                }
                $this->setKurikulum($this->aKurikulum);
            }

            if ($this->aSemester !== null) {
                if ($this->aSemester->isModified() || $this->aSemester->isNew()) {
                    $affectedRows += $this->aSemester->save($con);
                }
                $this->setSemester($this->aSemester);
            }

            if ($this->aTingkatPendidikan !== null) {
                if ($this->aTingkatPendidikan->isModified() || $this->aTingkatPendidikan->isNew()) {
                    $affectedRows += $this->aTingkatPendidikan->save($con);
                }
                $this->setTingkatPendidikan($this->aTingkatPendidikan);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->vldRombelsScheduledForDeletion !== null) {
                if (!$this->vldRombelsScheduledForDeletion->isEmpty()) {
                    VldRombelQuery::create()
                        ->filterByPrimaryKeys($this->vldRombelsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldRombelsScheduledForDeletion = null;
                }
            }

            if ($this->collVldRombels !== null) {
                foreach ($this->collVldRombels as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pembelajaransScheduledForDeletion !== null) {
                if (!$this->pembelajaransScheduledForDeletion->isEmpty()) {
                    PembelajaranQuery::create()
                        ->filterByPrimaryKeys($this->pembelajaransScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pembelajaransScheduledForDeletion = null;
                }
            }

            if ($this->collPembelajarans !== null) {
                foreach ($this->collPembelajarans as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aJurusanSp !== null) {
                if (!$this->aJurusanSp->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJurusanSp->getValidationFailures());
                }
            }

            if ($this->aPrasarana !== null) {
                if (!$this->aPrasarana->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPrasarana->getValidationFailures());
                }
            }

            if ($this->aPtk !== null) {
                if (!$this->aPtk->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPtk->getValidationFailures());
                }
            }

            if ($this->aSekolah !== null) {
                if (!$this->aSekolah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolah->getValidationFailures());
                }
            }

            if ($this->aKebutuhanKhusus !== null) {
                if (!$this->aKebutuhanKhusus->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKebutuhanKhusus->getValidationFailures());
                }
            }

            if ($this->aKurikulum !== null) {
                if (!$this->aKurikulum->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aKurikulum->getValidationFailures());
                }
            }

            if ($this->aSemester !== null) {
                if (!$this->aSemester->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSemester->getValidationFailures());
                }
            }

            if ($this->aTingkatPendidikan !== null) {
                if (!$this->aTingkatPendidikan->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aTingkatPendidikan->getValidationFailures());
                }
            }


            if (($retval = RombonganBelajarPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collVldRombels !== null) {
                    foreach ($this->collVldRombels as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPembelajarans !== null) {
                    foreach ($this->collPembelajarans as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_FIELDNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_FIELDNAME)
    {
        $pos = RombonganBelajarPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getRombonganBelajarId();
                break;
            case 1:
                return $this->getSemesterId();
                break;
            case 2:
                return $this->getSekolahId();
                break;
            case 3:
                return $this->getTingkatPendidikanId();
                break;
            case 4:
                return $this->getJurusanSpId();
                break;
            case 5:
                return $this->getKurikulumId();
                break;
            case 6:
                return $this->getNama();
                break;
            case 7:
                return $this->getPtkId();
                break;
            case 8:
                return $this->getPrasaranaId();
                break;
            case 9:
                return $this->getMovingClass();
                break;
            case 10:
                return $this->getJenisRombel();
                break;
            case 11:
                return $this->getSks();
                break;
            case 12:
                return $this->getKebutuhanKhususId();
                break;
            case 13:
                return $this->getLastUpdate();
                break;
            case 14:
                return $this->getSoftDelete();
                break;
            case 15:
                return $this->getLastSync();
                break;
            case 16:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['RombonganBelajar'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['RombonganBelajar'][$this->getPrimaryKey()] = true;
        $keys = RombonganBelajarPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getRombonganBelajarId(),
            $keys[1] => $this->getSemesterId(),
            $keys[2] => $this->getSekolahId(),
            $keys[3] => $this->getTingkatPendidikanId(),
            $keys[4] => $this->getJurusanSpId(),
            $keys[5] => $this->getKurikulumId(),
            $keys[6] => $this->getNama(),
            $keys[7] => $this->getPtkId(),
            $keys[8] => $this->getPrasaranaId(),
            $keys[9] => $this->getMovingClass(),
            $keys[10] => $this->getJenisRombel(),
            $keys[11] => $this->getSks(),
            $keys[12] => $this->getKebutuhanKhususId(),
            $keys[13] => $this->getLastUpdate(),
            $keys[14] => $this->getSoftDelete(),
            $keys[15] => $this->getLastSync(),
            $keys[16] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aJurusanSp) {
                $result['JurusanSp'] = $this->aJurusanSp->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPrasarana) {
                $result['Prasarana'] = $this->aPrasarana->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPtk) {
                $result['Ptk'] = $this->aPtk->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolah) {
                $result['Sekolah'] = $this->aSekolah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKebutuhanKhusus) {
                $result['KebutuhanKhusus'] = $this->aKebutuhanKhusus->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aKurikulum) {
                $result['Kurikulum'] = $this->aKurikulum->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSemester) {
                $result['Semester'] = $this->aSemester->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTingkatPendidikan) {
                $result['TingkatPendidikan'] = $this->aTingkatPendidikan->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collVldRombels) {
                $result['VldRombels'] = $this->collVldRombels->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPembelajarans) {
                $result['Pembelajarans'] = $this->collPembelajarans->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_FIELDNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_FIELDNAME)
    {
        $pos = RombonganBelajarPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setRombonganBelajarId($value);
                break;
            case 1:
                $this->setSemesterId($value);
                break;
            case 2:
                $this->setSekolahId($value);
                break;
            case 3:
                $this->setTingkatPendidikanId($value);
                break;
            case 4:
                $this->setJurusanSpId($value);
                break;
            case 5:
                $this->setKurikulumId($value);
                break;
            case 6:
                $this->setNama($value);
                break;
            case 7:
                $this->setPtkId($value);
                break;
            case 8:
                $this->setPrasaranaId($value);
                break;
            case 9:
                $this->setMovingClass($value);
                break;
            case 10:
                $this->setJenisRombel($value);
                break;
            case 11:
                $this->setSks($value);
                break;
            case 12:
                $this->setKebutuhanKhususId($value);
                break;
            case 13:
                $this->setLastUpdate($value);
                break;
            case 14:
                $this->setSoftDelete($value);
                break;
            case 15:
                $this->setLastSync($value);
                break;
            case 16:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_FIELDNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_FIELDNAME)
    {
        $keys = RombonganBelajarPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setRombonganBelajarId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setSemesterId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSekolahId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTingkatPendidikanId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setJurusanSpId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setKurikulumId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setNama($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setPtkId($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setPrasaranaId($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setMovingClass($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setJenisRombel($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setSks($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setKebutuhanKhususId($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setLastUpdate($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setSoftDelete($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setLastSync($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setUpdaterId($arr[$keys[16]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(RombonganBelajarPeer::DATABASE_NAME);

        if ($this->isColumnModified(RombonganBelajarPeer::ROMBONGAN_BELAJAR_ID)) $criteria->add(RombonganBelajarPeer::ROMBONGAN_BELAJAR_ID, $this->rombongan_belajar_id);
        if ($this->isColumnModified(RombonganBelajarPeer::SEMESTER_ID)) $criteria->add(RombonganBelajarPeer::SEMESTER_ID, $this->semester_id);
        if ($this->isColumnModified(RombonganBelajarPeer::SEKOLAH_ID)) $criteria->add(RombonganBelajarPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(RombonganBelajarPeer::TINGKAT_PENDIDIKAN_ID)) $criteria->add(RombonganBelajarPeer::TINGKAT_PENDIDIKAN_ID, $this->tingkat_pendidikan_id);
        if ($this->isColumnModified(RombonganBelajarPeer::JURUSAN_SP_ID)) $criteria->add(RombonganBelajarPeer::JURUSAN_SP_ID, $this->jurusan_sp_id);
        if ($this->isColumnModified(RombonganBelajarPeer::KURIKULUM_ID)) $criteria->add(RombonganBelajarPeer::KURIKULUM_ID, $this->kurikulum_id);
        if ($this->isColumnModified(RombonganBelajarPeer::NAMA)) $criteria->add(RombonganBelajarPeer::NAMA, $this->nama);
        if ($this->isColumnModified(RombonganBelajarPeer::PTK_ID)) $criteria->add(RombonganBelajarPeer::PTK_ID, $this->ptk_id);
        if ($this->isColumnModified(RombonganBelajarPeer::PRASARANA_ID)) $criteria->add(RombonganBelajarPeer::PRASARANA_ID, $this->prasarana_id);
        if ($this->isColumnModified(RombonganBelajarPeer::MOVING_CLASS)) $criteria->add(RombonganBelajarPeer::MOVING_CLASS, $this->moving_class);
        if ($this->isColumnModified(RombonganBelajarPeer::JENIS_ROMBEL)) $criteria->add(RombonganBelajarPeer::JENIS_ROMBEL, $this->jenis_rombel);
        if ($this->isColumnModified(RombonganBelajarPeer::SKS)) $criteria->add(RombonganBelajarPeer::SKS, $this->sks);
        if ($this->isColumnModified(RombonganBelajarPeer::KEBUTUHAN_KHUSUS_ID)) $criteria->add(RombonganBelajarPeer::KEBUTUHAN_KHUSUS_ID, $this->kebutuhan_khusus_id);
        if ($this->isColumnModified(RombonganBelajarPeer::LAST_UPDATE)) $criteria->add(RombonganBelajarPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(RombonganBelajarPeer::SOFT_DELETE)) $criteria->add(RombonganBelajarPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(RombonganBelajarPeer::LAST_SYNC)) $criteria->add(RombonganBelajarPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(RombonganBelajarPeer::UPDATER_ID)) $criteria->add(RombonganBelajarPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(RombonganBelajarPeer::DATABASE_NAME);
        $criteria->add(RombonganBelajarPeer::ROMBONGAN_BELAJAR_ID, $this->rombongan_belajar_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getRombonganBelajarId();
    }

    /**
     * Generic method to set the primary key (rombongan_belajar_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setRombonganBelajarId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getRombonganBelajarId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of RombonganBelajar (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSemesterId($this->getSemesterId());
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setTingkatPendidikanId($this->getTingkatPendidikanId());
        $copyObj->setJurusanSpId($this->getJurusanSpId());
        $copyObj->setKurikulumId($this->getKurikulumId());
        $copyObj->setNama($this->getNama());
        $copyObj->setPtkId($this->getPtkId());
        $copyObj->setPrasaranaId($this->getPrasaranaId());
        $copyObj->setMovingClass($this->getMovingClass());
        $copyObj->setJenisRombel($this->getJenisRombel());
        $copyObj->setSks($this->getSks());
        $copyObj->setKebutuhanKhususId($this->getKebutuhanKhususId());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getVldRombels() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldRombel($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPembelajarans() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPembelajaran($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setRombonganBelajarId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return RombonganBelajar Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return RombonganBelajarPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new RombonganBelajarPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a JurusanSp object.
     *
     * @param             JurusanSp $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJurusanSp(JurusanSp $v = null)
    {
        if ($v === null) {
            $this->setJurusanSpId(NULL);
        } else {
            $this->setJurusanSpId($v->getJurusanSpId());
        }

        $this->aJurusanSp = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JurusanSp object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajar($this);
        }


        return $this;
    }


    /**
     * Get the associated JurusanSp object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JurusanSp The associated JurusanSp object.
     * @throws PropelException
     */
    public function getJurusanSp(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJurusanSp === null && (($this->jurusan_sp_id !== "" && $this->jurusan_sp_id !== null)) && $doQuery) {
            $this->aJurusanSp = JurusanSpQuery::create()->findPk($this->jurusan_sp_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJurusanSp->addRombonganBelajars($this);
             */
        }

        return $this->aJurusanSp;
    }

    /**
     * Declares an association between this object and a Prasarana object.
     *
     * @param             Prasarana $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPrasarana(Prasarana $v = null)
    {
        if ($v === null) {
            $this->setPrasaranaId(NULL);
        } else {
            $this->setPrasaranaId($v->getPrasaranaId());
        }

        $this->aPrasarana = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Prasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajar($this);
        }


        return $this;
    }


    /**
     * Get the associated Prasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Prasarana The associated Prasarana object.
     * @throws PropelException
     */
    public function getPrasarana(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPrasarana === null && (($this->prasarana_id !== "" && $this->prasarana_id !== null)) && $doQuery) {
            $this->aPrasarana = PrasaranaQuery::create()->findPk($this->prasarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPrasarana->addRombonganBelajars($this);
             */
        }

        return $this->aPrasarana;
    }

    /**
     * Declares an association between this object and a Ptk object.
     *
     * @param             Ptk $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPtk(Ptk $v = null)
    {
        if ($v === null) {
            $this->setPtkId(NULL);
        } else {
            $this->setPtkId($v->getPtkId());
        }

        $this->aPtk = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Ptk object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajar($this);
        }


        return $this;
    }


    /**
     * Get the associated Ptk object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Ptk The associated Ptk object.
     * @throws PropelException
     */
    public function getPtk(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPtk === null && (($this->ptk_id !== "" && $this->ptk_id !== null)) && $doQuery) {
            $this->aPtk = PtkQuery::create()->findPk($this->ptk_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPtk->addRombonganBelajars($this);
             */
        }

        return $this->aPtk;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolah(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajar($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolah === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolah = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolah->addRombonganBelajars($this);
             */
        }

        return $this->aSekolah;
    }

    /**
     * Declares an association between this object and a KebutuhanKhusus object.
     *
     * @param             KebutuhanKhusus $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKebutuhanKhusus(KebutuhanKhusus $v = null)
    {
        if ($v === null) {
            $this->setKebutuhanKhususId(NULL);
        } else {
            $this->setKebutuhanKhususId($v->getKebutuhanKhususId());
        }

        $this->aKebutuhanKhusus = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the KebutuhanKhusus object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajar($this);
        }


        return $this;
    }


    /**
     * Get the associated KebutuhanKhusus object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return KebutuhanKhusus The associated KebutuhanKhusus object.
     * @throws PropelException
     */
    public function getKebutuhanKhusus(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKebutuhanKhusus === null && ($this->kebutuhan_khusus_id !== null) && $doQuery) {
            $this->aKebutuhanKhusus = KebutuhanKhususQuery::create()->findPk($this->kebutuhan_khusus_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKebutuhanKhusus->addRombonganBelajars($this);
             */
        }

        return $this->aKebutuhanKhusus;
    }

    /**
     * Declares an association between this object and a Kurikulum object.
     *
     * @param             Kurikulum $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setKurikulum(Kurikulum $v = null)
    {
        if ($v === null) {
            $this->setKurikulumId(NULL);
        } else {
            $this->setKurikulumId($v->getKurikulumId());
        }

        $this->aKurikulum = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Kurikulum object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajar($this);
        }


        return $this;
    }


    /**
     * Get the associated Kurikulum object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Kurikulum The associated Kurikulum object.
     * @throws PropelException
     */
    public function getKurikulum(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aKurikulum === null && ($this->kurikulum_id !== null) && $doQuery) {
            $this->aKurikulum = KurikulumQuery::create()->findPk($this->kurikulum_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aKurikulum->addRombonganBelajars($this);
             */
        }

        return $this->aKurikulum;
    }

    /**
     * Declares an association between this object and a Semester object.
     *
     * @param             Semester $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSemester(Semester $v = null)
    {
        if ($v === null) {
            $this->setSemesterId(NULL);
        } else {
            $this->setSemesterId($v->getSemesterId());
        }

        $this->aSemester = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Semester object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajar($this);
        }


        return $this;
    }


    /**
     * Get the associated Semester object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Semester The associated Semester object.
     * @throws PropelException
     */
    public function getSemester(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSemester === null && (($this->semester_id !== "" && $this->semester_id !== null)) && $doQuery) {
            $this->aSemester = SemesterQuery::create()->findPk($this->semester_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSemester->addRombonganBelajars($this);
             */
        }

        return $this->aSemester;
    }

    /**
     * Declares an association between this object and a TingkatPendidikan object.
     *
     * @param             TingkatPendidikan $v
     * @return RombonganBelajar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTingkatPendidikan(TingkatPendidikan $v = null)
    {
        if ($v === null) {
            $this->setTingkatPendidikanId(NULL);
        } else {
            $this->setTingkatPendidikanId($v->getTingkatPendidikanId());
        }

        $this->aTingkatPendidikan = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the TingkatPendidikan object, it will not be re-added.
        if ($v !== null) {
            $v->addRombonganBelajar($this);
        }


        return $this;
    }


    /**
     * Get the associated TingkatPendidikan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return TingkatPendidikan The associated TingkatPendidikan object.
     * @throws PropelException
     */
    public function getTingkatPendidikan(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTingkatPendidikan === null && (($this->tingkat_pendidikan_id !== "" && $this->tingkat_pendidikan_id !== null)) && $doQuery) {
            $this->aTingkatPendidikan = TingkatPendidikanQuery::create()->findPk($this->tingkat_pendidikan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTingkatPendidikan->addRombonganBelajars($this);
             */
        }

        return $this->aTingkatPendidikan;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('VldRombel' == $relationName) {
            $this->initVldRombels();
        }
        if ('Pembelajaran' == $relationName) {
            $this->initPembelajarans();
        }
    }

    /**
     * Clears out the collVldRombels collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return RombonganBelajar The current object (for fluent API support)
     * @see        addVldRombels()
     */
    public function clearVldRombels()
    {
        $this->collVldRombels = null; // important to set this to null since that means it is uninitialized
        $this->collVldRombelsPartial = null;

        return $this;
    }

    /**
     * reset is the collVldRombels collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldRombels($v = true)
    {
        $this->collVldRombelsPartial = $v;
    }

    /**
     * Initializes the collVldRombels collection.
     *
     * By default this just sets the collVldRombels collection to an empty array (like clearcollVldRombels());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldRombels($overrideExisting = true)
    {
        if (null !== $this->collVldRombels && !$overrideExisting) {
            return;
        }
        $this->collVldRombels = new PropelObjectCollection();
        $this->collVldRombels->setModel('VldRombel');
    }

    /**
     * Gets an array of VldRombel objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this RombonganBelajar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     * @throws PropelException
     */
    public function getVldRombels($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldRombelsPartial && !$this->isNew();
        if (null === $this->collVldRombels || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldRombels) {
                // return empty collection
                $this->initVldRombels();
            } else {
                $collVldRombels = VldRombelQuery::create(null, $criteria)
                    ->filterByRombonganBelajar($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldRombelsPartial && count($collVldRombels)) {
                      $this->initVldRombels(false);

                      foreach($collVldRombels as $obj) {
                        if (false == $this->collVldRombels->contains($obj)) {
                          $this->collVldRombels->append($obj);
                        }
                      }

                      $this->collVldRombelsPartial = true;
                    }

                    $collVldRombels->getInternalIterator()->rewind();
                    return $collVldRombels;
                }

                if($partial && $this->collVldRombels) {
                    foreach($this->collVldRombels as $obj) {
                        if($obj->isNew()) {
                            $collVldRombels[] = $obj;
                        }
                    }
                }

                $this->collVldRombels = $collVldRombels;
                $this->collVldRombelsPartial = false;
            }
        }

        return $this->collVldRombels;
    }

    /**
     * Sets a collection of VldRombel objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldRombels A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setVldRombels(PropelCollection $vldRombels, PropelPDO $con = null)
    {
        $vldRombelsToDelete = $this->getVldRombels(new Criteria(), $con)->diff($vldRombels);

        $this->vldRombelsScheduledForDeletion = unserialize(serialize($vldRombelsToDelete));

        foreach ($vldRombelsToDelete as $vldRombelRemoved) {
            $vldRombelRemoved->setRombonganBelajar(null);
        }

        $this->collVldRombels = null;
        foreach ($vldRombels as $vldRombel) {
            $this->addVldRombel($vldRombel);
        }

        $this->collVldRombels = $vldRombels;
        $this->collVldRombelsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldRombel objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldRombel objects.
     * @throws PropelException
     */
    public function countVldRombels(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldRombelsPartial && !$this->isNew();
        if (null === $this->collVldRombels || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldRombels) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldRombels());
            }
            $query = VldRombelQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRombonganBelajar($this)
                ->count($con);
        }

        return count($this->collVldRombels);
    }

    /**
     * Method called to associate a VldRombel object to this object
     * through the VldRombel foreign key attribute.
     *
     * @param    VldRombel $l VldRombel
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function addVldRombel(VldRombel $l)
    {
        if ($this->collVldRombels === null) {
            $this->initVldRombels();
            $this->collVldRombelsPartial = true;
        }
        if (!in_array($l, $this->collVldRombels->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldRombel($l);
        }

        return $this;
    }

    /**
     * @param	VldRombel $vldRombel The vldRombel object to add.
     */
    protected function doAddVldRombel($vldRombel)
    {
        $this->collVldRombels[]= $vldRombel;
        $vldRombel->setRombonganBelajar($this);
    }

    /**
     * @param	VldRombel $vldRombel The vldRombel object to remove.
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function removeVldRombel($vldRombel)
    {
        if ($this->getVldRombels()->contains($vldRombel)) {
            $this->collVldRombels->remove($this->collVldRombels->search($vldRombel));
            if (null === $this->vldRombelsScheduledForDeletion) {
                $this->vldRombelsScheduledForDeletion = clone $this->collVldRombels;
                $this->vldRombelsScheduledForDeletion->clear();
            }
            $this->vldRombelsScheduledForDeletion[]= clone $vldRombel;
            $vldRombel->setRombonganBelajar(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related VldRombels from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldRombel[] List of VldRombel objects
     */
    public function getVldRombelsJoinErrortype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldRombelQuery::create(null, $criteria);
        $query->joinWith('Errortype', $join_behavior);

        return $this->getVldRombels($query, $con);
    }

    /**
     * Clears out the collPembelajarans collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return RombonganBelajar The current object (for fluent API support)
     * @see        addPembelajarans()
     */
    public function clearPembelajarans()
    {
        $this->collPembelajarans = null; // important to set this to null since that means it is uninitialized
        $this->collPembelajaransPartial = null;

        return $this;
    }

    /**
     * reset is the collPembelajarans collection loaded partially
     *
     * @return void
     */
    public function resetPartialPembelajarans($v = true)
    {
        $this->collPembelajaransPartial = $v;
    }

    /**
     * Initializes the collPembelajarans collection.
     *
     * By default this just sets the collPembelajarans collection to an empty array (like clearcollPembelajarans());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPembelajarans($overrideExisting = true)
    {
        if (null !== $this->collPembelajarans && !$overrideExisting) {
            return;
        }
        $this->collPembelajarans = new PropelObjectCollection();
        $this->collPembelajarans->setModel('Pembelajaran');
    }

    /**
     * Gets an array of Pembelajaran objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this RombonganBelajar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     * @throws PropelException
     */
    public function getPembelajarans($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransPartial && !$this->isNew();
        if (null === $this->collPembelajarans || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPembelajarans) {
                // return empty collection
                $this->initPembelajarans();
            } else {
                $collPembelajarans = PembelajaranQuery::create(null, $criteria)
                    ->filterByRombonganBelajar($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPembelajaransPartial && count($collPembelajarans)) {
                      $this->initPembelajarans(false);

                      foreach($collPembelajarans as $obj) {
                        if (false == $this->collPembelajarans->contains($obj)) {
                          $this->collPembelajarans->append($obj);
                        }
                      }

                      $this->collPembelajaransPartial = true;
                    }

                    $collPembelajarans->getInternalIterator()->rewind();
                    return $collPembelajarans;
                }

                if($partial && $this->collPembelajarans) {
                    foreach($this->collPembelajarans as $obj) {
                        if($obj->isNew()) {
                            $collPembelajarans[] = $obj;
                        }
                    }
                }

                $this->collPembelajarans = $collPembelajarans;
                $this->collPembelajaransPartial = false;
            }
        }

        return $this->collPembelajarans;
    }

    /**
     * Sets a collection of Pembelajaran objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $pembelajarans A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function setPembelajarans(PropelCollection $pembelajarans, PropelPDO $con = null)
    {
        $pembelajaransToDelete = $this->getPembelajarans(new Criteria(), $con)->diff($pembelajarans);

        $this->pembelajaransScheduledForDeletion = unserialize(serialize($pembelajaransToDelete));

        foreach ($pembelajaransToDelete as $pembelajaranRemoved) {
            $pembelajaranRemoved->setRombonganBelajar(null);
        }

        $this->collPembelajarans = null;
        foreach ($pembelajarans as $pembelajaran) {
            $this->addPembelajaran($pembelajaran);
        }

        $this->collPembelajarans = $pembelajarans;
        $this->collPembelajaransPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Pembelajaran objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Pembelajaran objects.
     * @throws PropelException
     */
    public function countPembelajarans(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPembelajaransPartial && !$this->isNew();
        if (null === $this->collPembelajarans || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPembelajarans) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPembelajarans());
            }
            $query = PembelajaranQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRombonganBelajar($this)
                ->count($con);
        }

        return count($this->collPembelajarans);
    }

    /**
     * Method called to associate a Pembelajaran object to this object
     * through the Pembelajaran foreign key attribute.
     *
     * @param    Pembelajaran $l Pembelajaran
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function addPembelajaran(Pembelajaran $l)
    {
        if ($this->collPembelajarans === null) {
            $this->initPembelajarans();
            $this->collPembelajaransPartial = true;
        }
        if (!in_array($l, $this->collPembelajarans->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPembelajaran($l);
        }

        return $this;
    }

    /**
     * @param	Pembelajaran $pembelajaran The pembelajaran object to add.
     */
    protected function doAddPembelajaran($pembelajaran)
    {
        $this->collPembelajarans[]= $pembelajaran;
        $pembelajaran->setRombonganBelajar($this);
    }

    /**
     * @param	Pembelajaran $pembelajaran The pembelajaran object to remove.
     * @return RombonganBelajar The current object (for fluent API support)
     */
    public function removePembelajaran($pembelajaran)
    {
        if ($this->getPembelajarans()->contains($pembelajaran)) {
            $this->collPembelajarans->remove($this->collPembelajarans->search($pembelajaran));
            if (null === $this->pembelajaransScheduledForDeletion) {
                $this->pembelajaransScheduledForDeletion = clone $this->collPembelajarans;
                $this->pembelajaransScheduledForDeletion->clear();
            }
            $this->pembelajaransScheduledForDeletion[]= clone $pembelajaran;
            $pembelajaran->setRombonganBelajar(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related Pembelajarans from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransJoinPtkTerdaftar($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('PtkTerdaftar', $join_behavior);

        return $this->getPembelajarans($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related Pembelajarans from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransJoinMataPelajaran($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('MataPelajaran', $join_behavior);

        return $this->getPembelajarans($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RombonganBelajar is new, it will return
     * an empty collection; or if this RombonganBelajar has previously
     * been saved, it will retrieve related Pembelajarans from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RombonganBelajar.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Pembelajaran[] List of Pembelajaran objects
     */
    public function getPembelajaransJoinSemester($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PembelajaranQuery::create(null, $criteria);
        $query->joinWith('Semester', $join_behavior);

        return $this->getPembelajarans($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->rombongan_belajar_id = null;
        $this->semester_id = null;
        $this->sekolah_id = null;
        $this->tingkat_pendidikan_id = null;
        $this->jurusan_sp_id = null;
        $this->kurikulum_id = null;
        $this->nama = null;
        $this->ptk_id = null;
        $this->prasarana_id = null;
        $this->moving_class = null;
        $this->jenis_rombel = null;
        $this->sks = null;
        $this->kebutuhan_khusus_id = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collVldRombels) {
                foreach ($this->collVldRombels as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPembelajarans) {
                foreach ($this->collPembelajarans as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aJurusanSp instanceof Persistent) {
              $this->aJurusanSp->clearAllReferences($deep);
            }
            if ($this->aPrasarana instanceof Persistent) {
              $this->aPrasarana->clearAllReferences($deep);
            }
            if ($this->aPtk instanceof Persistent) {
              $this->aPtk->clearAllReferences($deep);
            }
            if ($this->aSekolah instanceof Persistent) {
              $this->aSekolah->clearAllReferences($deep);
            }
            if ($this->aKebutuhanKhusus instanceof Persistent) {
              $this->aKebutuhanKhusus->clearAllReferences($deep);
            }
            if ($this->aKurikulum instanceof Persistent) {
              $this->aKurikulum->clearAllReferences($deep);
            }
            if ($this->aSemester instanceof Persistent) {
              $this->aSemester->clearAllReferences($deep);
            }
            if ($this->aTingkatPendidikan instanceof Persistent) {
              $this->aTingkatPendidikan->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collVldRombels instanceof PropelCollection) {
            $this->collVldRombels->clearIterator();
        }
        $this->collVldRombels = null;
        if ($this->collPembelajarans instanceof PropelCollection) {
            $this->collPembelajarans->clearIterator();
        }
        $this->collPembelajarans = null;
        $this->aJurusanSp = null;
        $this->aPrasarana = null;
        $this->aPtk = null;
        $this->aSekolah = null;
        $this->aKebutuhanKhusus = null;
        $this->aKurikulum = null;
        $this->aSemester = null;
        $this->aTingkatPendidikan = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(RombonganBelajarPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
