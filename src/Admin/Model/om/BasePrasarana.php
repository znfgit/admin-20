<?php

namespace Admin\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Admin\Model\BukuAlat;
use Admin\Model\BukuAlatQuery;
use Admin\Model\Jadwal;
use Admin\Model\JadwalQuery;
use Admin\Model\JenisHapusBuku;
use Admin\Model\JenisHapusBukuQuery;
use Admin\Model\JenisPrasarana;
use Admin\Model\JenisPrasaranaQuery;
use Admin\Model\Prasarana;
use Admin\Model\PrasaranaLongitudinal;
use Admin\Model\PrasaranaLongitudinalQuery;
use Admin\Model\PrasaranaPeer;
use Admin\Model\PrasaranaQuery;
use Admin\Model\RombonganBelajar;
use Admin\Model\RombonganBelajarQuery;
use Admin\Model\Sarana;
use Admin\Model\SaranaQuery;
use Admin\Model\Sekolah;
use Admin\Model\SekolahQuery;
use Admin\Model\StatusKepemilikanSarpras;
use Admin\Model\StatusKepemilikanSarprasQuery;
use Admin\Model\VldPrasarana;
use Admin\Model\VldPrasaranaQuery;

/**
 * Base class that represents a row from the 'prasarana' table.
 *
 *
 *
 * @package    propel.generator.Admin.Model.om
 */
abstract class BasePrasarana extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Admin\\Model\\PrasaranaPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PrasaranaPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the prasarana_id field.
     * @var        string
     */
    protected $prasarana_id;

    /**
     * The value for the jenis_prasarana_id field.
     * @var        int
     */
    protected $jenis_prasarana_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the id_induk_prasarana field.
     * @var        string
     */
    protected $id_induk_prasarana;

    /**
     * The value for the kepemilikan_sarpras_id field.
     * @var        string
     */
    protected $kepemilikan_sarpras_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the panjang field.
     * @var        double
     */
    protected $panjang;

    /**
     * The value for the lebar field.
     * @var        double
     */
    protected $lebar;

    /**
     * The value for the reg_pras field.
     * @var        string
     */
    protected $reg_pras;

    /**
     * The value for the ket_prasarana field.
     * @var        string
     */
    protected $ket_prasarana;

    /**
     * The value for the vol_pondasi_m3 field.
     * @var        string
     */
    protected $vol_pondasi_m3;

    /**
     * The value for the vol_sloop_kolom_balok_m3 field.
     * @var        string
     */
    protected $vol_sloop_kolom_balok_m3;

    /**
     * The value for the luas_plester_m2 field.
     * @var        string
     */
    protected $luas_plester_m2;

    /**
     * The value for the panj_kudakuda_m field.
     * @var        string
     */
    protected $panj_kudakuda_m;

    /**
     * The value for the vol_kudakuda_m3 field.
     * @var        string
     */
    protected $vol_kudakuda_m3;

    /**
     * The value for the panj_kaso_m field.
     * @var        string
     */
    protected $panj_kaso_m;

    /**
     * The value for the panj_reng_m field.
     * @var        string
     */
    protected $panj_reng_m;

    /**
     * The value for the luas_tutup_atap_m2 field.
     * @var        string
     */
    protected $luas_tutup_atap_m2;

    /**
     * The value for the luas_plafon_m2 field.
     * @var        string
     */
    protected $luas_plafon_m2;

    /**
     * The value for the luas_dinding_m2 field.
     * @var        string
     */
    protected $luas_dinding_m2;

    /**
     * The value for the luas_daun_jendela_m2 field.
     * @var        string
     */
    protected $luas_daun_jendela_m2;

    /**
     * The value for the luas_daun_pintu_m2 field.
     * @var        string
     */
    protected $luas_daun_pintu_m2;

    /**
     * The value for the panj_kusen_m field.
     * @var        string
     */
    protected $panj_kusen_m;

    /**
     * The value for the luas_tutup_lantai_m2 field.
     * @var        string
     */
    protected $luas_tutup_lantai_m2;

    /**
     * The value for the panj_inst_listrik_m field.
     * @var        string
     */
    protected $panj_inst_listrik_m;

    /**
     * The value for the jml_inst_listrik field.
     * @var        string
     */
    protected $jml_inst_listrik;

    /**
     * The value for the panj_inst_air_m field.
     * @var        string
     */
    protected $panj_inst_air_m;

    /**
     * The value for the jml_inst_air field.
     * @var        string
     */
    protected $jml_inst_air;

    /**
     * The value for the panj_drainase_m field.
     * @var        string
     */
    protected $panj_drainase_m;

    /**
     * The value for the luas_finish_struktur_m2 field.
     * @var        string
     */
    protected $luas_finish_struktur_m2;

    /**
     * The value for the luas_finish_plafon_m2 field.
     * @var        string
     */
    protected $luas_finish_plafon_m2;

    /**
     * The value for the luas_finish_dinding_m2 field.
     * @var        string
     */
    protected $luas_finish_dinding_m2;

    /**
     * The value for the luas_finish_kpj_m2 field.
     * @var        string
     */
    protected $luas_finish_kpj_m2;

    /**
     * The value for the id_hapus_buku field.
     * @var        string
     */
    protected $id_hapus_buku;

    /**
     * The value for the tgl_hapus_buku field.
     * @var        string
     */
    protected $tgl_hapus_buku;

    /**
     * The value for the asal_data field.
     * @var        string
     */
    protected $asal_data;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Prasarana
     */
    protected $aPrasaranaRelatedByIdIndukPrasarana;

    /**
     * @var        Sekolah
     */
    protected $aSekolah;

    /**
     * @var        JenisHapusBuku
     */
    protected $aJenisHapusBuku;

    /**
     * @var        JenisPrasarana
     */
    protected $aJenisPrasarana;

    /**
     * @var        StatusKepemilikanSarpras
     */
    protected $aStatusKepemilikanSarpras;

    /**
     * @var        PropelObjectCollection|BukuAlat[] Collection to store aggregation of BukuAlat objects.
     */
    protected $collBukuAlats;
    protected $collBukuAlatsPartial;

    /**
     * @var        PropelObjectCollection|Jadwal[] Collection to store aggregation of Jadwal objects.
     */
    protected $collJadwals;
    protected $collJadwalsPartial;

    /**
     * @var        PropelObjectCollection|VldPrasarana[] Collection to store aggregation of VldPrasarana objects.
     */
    protected $collVldPrasaranas;
    protected $collVldPrasaranasPartial;

    /**
     * @var        PropelObjectCollection|Prasarana[] Collection to store aggregation of Prasarana objects.
     */
    protected $collPrasaranasRelatedByPrasaranaId;
    protected $collPrasaranasRelatedByPrasaranaIdPartial;

    /**
     * @var        PropelObjectCollection|PrasaranaLongitudinal[] Collection to store aggregation of PrasaranaLongitudinal objects.
     */
    protected $collPrasaranaLongitudinals;
    protected $collPrasaranaLongitudinalsPartial;

    /**
     * @var        PropelObjectCollection|RombonganBelajar[] Collection to store aggregation of RombonganBelajar objects.
     */
    protected $collRombonganBelajars;
    protected $collRombonganBelajarsPartial;

    /**
     * @var        PropelObjectCollection|Sarana[] Collection to store aggregation of Sarana objects.
     */
    protected $collSaranas;
    protected $collSaranasPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $bukuAlatsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $jadwalsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $vldPrasaranasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $prasaranasRelatedByPrasaranaIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $prasaranaLongitudinalsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rombonganBelajarsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $saranasScheduledForDeletion = null;

    /**
     * Get the [prasarana_id] column value.
     *
     * @return string
     */
    public function getPrasaranaId()
    {
        return $this->prasarana_id;
    }

    /**
     * Get the [jenis_prasarana_id] column value.
     *
     * @return int
     */
    public function getJenisPrasaranaId()
    {
        return $this->jenis_prasarana_id;
    }

    /**
     * Get the [sekolah_id] column value.
     *
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [id_induk_prasarana] column value.
     *
     * @return string
     */
    public function getIdIndukPrasarana()
    {
        return $this->id_induk_prasarana;
    }

    /**
     * Get the [kepemilikan_sarpras_id] column value.
     *
     * @return string
     */
    public function getKepemilikanSarprasId()
    {
        return $this->kepemilikan_sarpras_id;
    }

    /**
     * Get the [nama] column value.
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [panjang] column value.
     *
     * @return double
     */
    public function getPanjang()
    {
        return $this->panjang;
    }

    /**
     * Get the [lebar] column value.
     *
     * @return double
     */
    public function getLebar()
    {
        return $this->lebar;
    }

    /**
     * Get the [reg_pras] column value.
     *
     * @return string
     */
    public function getRegPras()
    {
        return $this->reg_pras;
    }

    /**
     * Get the [ket_prasarana] column value.
     *
     * @return string
     */
    public function getKetPrasarana()
    {
        return $this->ket_prasarana;
    }

    /**
     * Get the [vol_pondasi_m3] column value.
     *
     * @return string
     */
    public function getVolPondasiM3()
    {
        return $this->vol_pondasi_m3;
    }

    /**
     * Get the [vol_sloop_kolom_balok_m3] column value.
     *
     * @return string
     */
    public function getVolSloopKolomBalokM3()
    {
        return $this->vol_sloop_kolom_balok_m3;
    }

    /**
     * Get the [luas_plester_m2] column value.
     *
     * @return string
     */
    public function getLuasPlesterM2()
    {
        return $this->luas_plester_m2;
    }

    /**
     * Get the [panj_kudakuda_m] column value.
     *
     * @return string
     */
    public function getPanjKudakudaM()
    {
        return $this->panj_kudakuda_m;
    }

    /**
     * Get the [vol_kudakuda_m3] column value.
     *
     * @return string
     */
    public function getVolKudakudaM3()
    {
        return $this->vol_kudakuda_m3;
    }

    /**
     * Get the [panj_kaso_m] column value.
     *
     * @return string
     */
    public function getPanjKasoM()
    {
        return $this->panj_kaso_m;
    }

    /**
     * Get the [panj_reng_m] column value.
     *
     * @return string
     */
    public function getPanjRengM()
    {
        return $this->panj_reng_m;
    }

    /**
     * Get the [luas_tutup_atap_m2] column value.
     *
     * @return string
     */
    public function getLuasTutupAtapM2()
    {
        return $this->luas_tutup_atap_m2;
    }

    /**
     * Get the [luas_plafon_m2] column value.
     *
     * @return string
     */
    public function getLuasPlafonM2()
    {
        return $this->luas_plafon_m2;
    }

    /**
     * Get the [luas_dinding_m2] column value.
     *
     * @return string
     */
    public function getLuasDindingM2()
    {
        return $this->luas_dinding_m2;
    }

    /**
     * Get the [luas_daun_jendela_m2] column value.
     *
     * @return string
     */
    public function getLuasDaunJendelaM2()
    {
        return $this->luas_daun_jendela_m2;
    }

    /**
     * Get the [luas_daun_pintu_m2] column value.
     *
     * @return string
     */
    public function getLuasDaunPintuM2()
    {
        return $this->luas_daun_pintu_m2;
    }

    /**
     * Get the [panj_kusen_m] column value.
     *
     * @return string
     */
    public function getPanjKusenM()
    {
        return $this->panj_kusen_m;
    }

    /**
     * Get the [luas_tutup_lantai_m2] column value.
     *
     * @return string
     */
    public function getLuasTutupLantaiM2()
    {
        return $this->luas_tutup_lantai_m2;
    }

    /**
     * Get the [panj_inst_listrik_m] column value.
     *
     * @return string
     */
    public function getPanjInstListrikM()
    {
        return $this->panj_inst_listrik_m;
    }

    /**
     * Get the [jml_inst_listrik] column value.
     *
     * @return string
     */
    public function getJmlInstListrik()
    {
        return $this->jml_inst_listrik;
    }

    /**
     * Get the [panj_inst_air_m] column value.
     *
     * @return string
     */
    public function getPanjInstAirM()
    {
        return $this->panj_inst_air_m;
    }

    /**
     * Get the [jml_inst_air] column value.
     *
     * @return string
     */
    public function getJmlInstAir()
    {
        return $this->jml_inst_air;
    }

    /**
     * Get the [panj_drainase_m] column value.
     *
     * @return string
     */
    public function getPanjDrainaseM()
    {
        return $this->panj_drainase_m;
    }

    /**
     * Get the [luas_finish_struktur_m2] column value.
     *
     * @return string
     */
    public function getLuasFinishStrukturM2()
    {
        return $this->luas_finish_struktur_m2;
    }

    /**
     * Get the [luas_finish_plafon_m2] column value.
     *
     * @return string
     */
    public function getLuasFinishPlafonM2()
    {
        return $this->luas_finish_plafon_m2;
    }

    /**
     * Get the [luas_finish_dinding_m2] column value.
     *
     * @return string
     */
    public function getLuasFinishDindingM2()
    {
        return $this->luas_finish_dinding_m2;
    }

    /**
     * Get the [luas_finish_kpj_m2] column value.
     *
     * @return string
     */
    public function getLuasFinishKpjM2()
    {
        return $this->luas_finish_kpj_m2;
    }

    /**
     * Get the [id_hapus_buku] column value.
     *
     * @return string
     */
    public function getIdHapusBuku()
    {
        return $this->id_hapus_buku;
    }

    /**
     * Get the [tgl_hapus_buku] column value.
     *
     * @return string
     */
    public function getTglHapusBuku()
    {
        return $this->tgl_hapus_buku;
    }

    /**
     * Get the [asal_data] column value.
     *
     * @return string
     */
    public function getAsalData()
    {
        return $this->asal_data;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [soft_delete] column value.
     *
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [updater_id] column value.
     *
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [prasarana_id] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPrasaranaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prasarana_id !== $v) {
            $this->prasarana_id = $v;
            $this->modifiedColumns[] = PrasaranaPeer::PRASARANA_ID;
        }


        return $this;
    } // setPrasaranaId()

    /**
     * Set the value of [jenis_prasarana_id] column.
     *
     * @param int $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setJenisPrasaranaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->jenis_prasarana_id !== $v) {
            $this->jenis_prasarana_id = $v;
            $this->modifiedColumns[] = PrasaranaPeer::JENIS_PRASARANA_ID;
        }

        if ($this->aJenisPrasarana !== null && $this->aJenisPrasarana->getJenisPrasaranaId() !== $v) {
            $this->aJenisPrasarana = null;
        }


        return $this;
    } // setJenisPrasaranaId()

    /**
     * Set the value of [sekolah_id] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = PrasaranaPeer::SEKOLAH_ID;
        }

        if ($this->aSekolah !== null && $this->aSekolah->getSekolahId() !== $v) {
            $this->aSekolah = null;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [id_induk_prasarana] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setIdIndukPrasarana($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_induk_prasarana !== $v) {
            $this->id_induk_prasarana = $v;
            $this->modifiedColumns[] = PrasaranaPeer::ID_INDUK_PRASARANA;
        }

        if ($this->aPrasaranaRelatedByIdIndukPrasarana !== null && $this->aPrasaranaRelatedByIdIndukPrasarana->getPrasaranaId() !== $v) {
            $this->aPrasaranaRelatedByIdIndukPrasarana = null;
        }


        return $this;
    } // setIdIndukPrasarana()

    /**
     * Set the value of [kepemilikan_sarpras_id] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setKepemilikanSarprasId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kepemilikan_sarpras_id !== $v) {
            $this->kepemilikan_sarpras_id = $v;
            $this->modifiedColumns[] = PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID;
        }

        if ($this->aStatusKepemilikanSarpras !== null && $this->aStatusKepemilikanSarpras->getKepemilikanSarprasId() !== $v) {
            $this->aStatusKepemilikanSarpras = null;
        }


        return $this;
    } // setKepemilikanSarprasId()

    /**
     * Set the value of [nama] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = PrasaranaPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [panjang] column.
     *
     * @param double $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPanjang($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->panjang !== $v) {
            $this->panjang = $v;
            $this->modifiedColumns[] = PrasaranaPeer::PANJANG;
        }


        return $this;
    } // setPanjang()

    /**
     * Set the value of [lebar] column.
     *
     * @param double $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLebar($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->lebar !== $v) {
            $this->lebar = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LEBAR;
        }


        return $this;
    } // setLebar()

    /**
     * Set the value of [reg_pras] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setRegPras($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->reg_pras !== $v) {
            $this->reg_pras = $v;
            $this->modifiedColumns[] = PrasaranaPeer::REG_PRAS;
        }


        return $this;
    } // setRegPras()

    /**
     * Set the value of [ket_prasarana] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setKetPrasarana($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ket_prasarana !== $v) {
            $this->ket_prasarana = $v;
            $this->modifiedColumns[] = PrasaranaPeer::KET_PRASARANA;
        }


        return $this;
    } // setKetPrasarana()

    /**
     * Set the value of [vol_pondasi_m3] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setVolPondasiM3($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->vol_pondasi_m3 !== $v) {
            $this->vol_pondasi_m3 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::VOL_PONDASI_M3;
        }


        return $this;
    } // setVolPondasiM3()

    /**
     * Set the value of [vol_sloop_kolom_balok_m3] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setVolSloopKolomBalokM3($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->vol_sloop_kolom_balok_m3 !== $v) {
            $this->vol_sloop_kolom_balok_m3 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::VOL_SLOOP_KOLOM_BALOK_M3;
        }


        return $this;
    } // setVolSloopKolomBalokM3()

    /**
     * Set the value of [luas_plester_m2] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLuasPlesterM2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->luas_plester_m2 !== $v) {
            $this->luas_plester_m2 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LUAS_PLESTER_M2;
        }


        return $this;
    } // setLuasPlesterM2()

    /**
     * Set the value of [panj_kudakuda_m] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPanjKudakudaM($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->panj_kudakuda_m !== $v) {
            $this->panj_kudakuda_m = $v;
            $this->modifiedColumns[] = PrasaranaPeer::PANJ_KUDAKUDA_M;
        }


        return $this;
    } // setPanjKudakudaM()

    /**
     * Set the value of [vol_kudakuda_m3] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setVolKudakudaM3($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->vol_kudakuda_m3 !== $v) {
            $this->vol_kudakuda_m3 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::VOL_KUDAKUDA_M3;
        }


        return $this;
    } // setVolKudakudaM3()

    /**
     * Set the value of [panj_kaso_m] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPanjKasoM($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->panj_kaso_m !== $v) {
            $this->panj_kaso_m = $v;
            $this->modifiedColumns[] = PrasaranaPeer::PANJ_KASO_M;
        }


        return $this;
    } // setPanjKasoM()

    /**
     * Set the value of [panj_reng_m] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPanjRengM($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->panj_reng_m !== $v) {
            $this->panj_reng_m = $v;
            $this->modifiedColumns[] = PrasaranaPeer::PANJ_RENG_M;
        }


        return $this;
    } // setPanjRengM()

    /**
     * Set the value of [luas_tutup_atap_m2] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLuasTutupAtapM2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->luas_tutup_atap_m2 !== $v) {
            $this->luas_tutup_atap_m2 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LUAS_TUTUP_ATAP_M2;
        }


        return $this;
    } // setLuasTutupAtapM2()

    /**
     * Set the value of [luas_plafon_m2] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLuasPlafonM2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->luas_plafon_m2 !== $v) {
            $this->luas_plafon_m2 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LUAS_PLAFON_M2;
        }


        return $this;
    } // setLuasPlafonM2()

    /**
     * Set the value of [luas_dinding_m2] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLuasDindingM2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->luas_dinding_m2 !== $v) {
            $this->luas_dinding_m2 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LUAS_DINDING_M2;
        }


        return $this;
    } // setLuasDindingM2()

    /**
     * Set the value of [luas_daun_jendela_m2] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLuasDaunJendelaM2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->luas_daun_jendela_m2 !== $v) {
            $this->luas_daun_jendela_m2 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LUAS_DAUN_JENDELA_M2;
        }


        return $this;
    } // setLuasDaunJendelaM2()

    /**
     * Set the value of [luas_daun_pintu_m2] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLuasDaunPintuM2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->luas_daun_pintu_m2 !== $v) {
            $this->luas_daun_pintu_m2 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LUAS_DAUN_PINTU_M2;
        }


        return $this;
    } // setLuasDaunPintuM2()

    /**
     * Set the value of [panj_kusen_m] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPanjKusenM($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->panj_kusen_m !== $v) {
            $this->panj_kusen_m = $v;
            $this->modifiedColumns[] = PrasaranaPeer::PANJ_KUSEN_M;
        }


        return $this;
    } // setPanjKusenM()

    /**
     * Set the value of [luas_tutup_lantai_m2] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLuasTutupLantaiM2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->luas_tutup_lantai_m2 !== $v) {
            $this->luas_tutup_lantai_m2 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LUAS_TUTUP_LANTAI_M2;
        }


        return $this;
    } // setLuasTutupLantaiM2()

    /**
     * Set the value of [panj_inst_listrik_m] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPanjInstListrikM($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->panj_inst_listrik_m !== $v) {
            $this->panj_inst_listrik_m = $v;
            $this->modifiedColumns[] = PrasaranaPeer::PANJ_INST_LISTRIK_M;
        }


        return $this;
    } // setPanjInstListrikM()

    /**
     * Set the value of [jml_inst_listrik] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setJmlInstListrik($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jml_inst_listrik !== $v) {
            $this->jml_inst_listrik = $v;
            $this->modifiedColumns[] = PrasaranaPeer::JML_INST_LISTRIK;
        }


        return $this;
    } // setJmlInstListrik()

    /**
     * Set the value of [panj_inst_air_m] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPanjInstAirM($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->panj_inst_air_m !== $v) {
            $this->panj_inst_air_m = $v;
            $this->modifiedColumns[] = PrasaranaPeer::PANJ_INST_AIR_M;
        }


        return $this;
    } // setPanjInstAirM()

    /**
     * Set the value of [jml_inst_air] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setJmlInstAir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jml_inst_air !== $v) {
            $this->jml_inst_air = $v;
            $this->modifiedColumns[] = PrasaranaPeer::JML_INST_AIR;
        }


        return $this;
    } // setJmlInstAir()

    /**
     * Set the value of [panj_drainase_m] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPanjDrainaseM($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->panj_drainase_m !== $v) {
            $this->panj_drainase_m = $v;
            $this->modifiedColumns[] = PrasaranaPeer::PANJ_DRAINASE_M;
        }


        return $this;
    } // setPanjDrainaseM()

    /**
     * Set the value of [luas_finish_struktur_m2] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLuasFinishStrukturM2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->luas_finish_struktur_m2 !== $v) {
            $this->luas_finish_struktur_m2 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LUAS_FINISH_STRUKTUR_M2;
        }


        return $this;
    } // setLuasFinishStrukturM2()

    /**
     * Set the value of [luas_finish_plafon_m2] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLuasFinishPlafonM2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->luas_finish_plafon_m2 !== $v) {
            $this->luas_finish_plafon_m2 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LUAS_FINISH_PLAFON_M2;
        }


        return $this;
    } // setLuasFinishPlafonM2()

    /**
     * Set the value of [luas_finish_dinding_m2] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLuasFinishDindingM2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->luas_finish_dinding_m2 !== $v) {
            $this->luas_finish_dinding_m2 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LUAS_FINISH_DINDING_M2;
        }


        return $this;
    } // setLuasFinishDindingM2()

    /**
     * Set the value of [luas_finish_kpj_m2] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLuasFinishKpjM2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->luas_finish_kpj_m2 !== $v) {
            $this->luas_finish_kpj_m2 = $v;
            $this->modifiedColumns[] = PrasaranaPeer::LUAS_FINISH_KPJ_M2;
        }


        return $this;
    } // setLuasFinishKpjM2()

    /**
     * Set the value of [id_hapus_buku] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setIdHapusBuku($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_hapus_buku !== $v) {
            $this->id_hapus_buku = $v;
            $this->modifiedColumns[] = PrasaranaPeer::ID_HAPUS_BUKU;
        }

        if ($this->aJenisHapusBuku !== null && $this->aJenisHapusBuku->getIdHapusBuku() !== $v) {
            $this->aJenisHapusBuku = null;
        }


        return $this;
    } // setIdHapusBuku()

    /**
     * Set the value of [tgl_hapus_buku] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setTglHapusBuku($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tgl_hapus_buku !== $v) {
            $this->tgl_hapus_buku = $v;
            $this->modifiedColumns[] = PrasaranaPeer::TGL_HAPUS_BUKU;
        }


        return $this;
    } // setTglHapusBuku()

    /**
     * Set the value of [asal_data] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setAsalData($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->asal_data !== $v) {
            $this->asal_data = $v;
            $this->modifiedColumns[] = PrasaranaPeer::ASAL_DATA;
        }


        return $this;
    } // setAsalData()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = PrasaranaPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = PrasaranaPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Prasarana The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = PrasaranaPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     *
     * @param string $v new value
     * @return Prasarana The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = PrasaranaPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->prasarana_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->jenis_prasarana_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->sekolah_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->id_induk_prasarana = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->kepemilikan_sarpras_id = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->nama = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->panjang = ($row[$startcol + 6] !== null) ? (double) $row[$startcol + 6] : null;
            $this->lebar = ($row[$startcol + 7] !== null) ? (double) $row[$startcol + 7] : null;
            $this->reg_pras = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->ket_prasarana = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->vol_pondasi_m3 = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->vol_sloop_kolom_balok_m3 = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->luas_plester_m2 = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->panj_kudakuda_m = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->vol_kudakuda_m3 = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->panj_kaso_m = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->panj_reng_m = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->luas_tutup_atap_m2 = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->luas_plafon_m2 = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->luas_dinding_m2 = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->luas_daun_jendela_m2 = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->luas_daun_pintu_m2 = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->panj_kusen_m = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->luas_tutup_lantai_m2 = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->panj_inst_listrik_m = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->jml_inst_listrik = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->panj_inst_air_m = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
            $this->jml_inst_air = ($row[$startcol + 27] !== null) ? (string) $row[$startcol + 27] : null;
            $this->panj_drainase_m = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->luas_finish_struktur_m2 = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->luas_finish_plafon_m2 = ($row[$startcol + 30] !== null) ? (string) $row[$startcol + 30] : null;
            $this->luas_finish_dinding_m2 = ($row[$startcol + 31] !== null) ? (string) $row[$startcol + 31] : null;
            $this->luas_finish_kpj_m2 = ($row[$startcol + 32] !== null) ? (string) $row[$startcol + 32] : null;
            $this->id_hapus_buku = ($row[$startcol + 33] !== null) ? (string) $row[$startcol + 33] : null;
            $this->tgl_hapus_buku = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
            $this->asal_data = ($row[$startcol + 35] !== null) ? (string) $row[$startcol + 35] : null;
            $this->last_update = ($row[$startcol + 36] !== null) ? (string) $row[$startcol + 36] : null;
            $this->soft_delete = ($row[$startcol + 37] !== null) ? (string) $row[$startcol + 37] : null;
            $this->last_sync = ($row[$startcol + 38] !== null) ? (string) $row[$startcol + 38] : null;
            $this->updater_id = ($row[$startcol + 39] !== null) ? (string) $row[$startcol + 39] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 40; // 40 = PrasaranaPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Prasarana object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aJenisPrasarana !== null && $this->jenis_prasarana_id !== $this->aJenisPrasarana->getJenisPrasaranaId()) {
            $this->aJenisPrasarana = null;
        }
        if ($this->aSekolah !== null && $this->sekolah_id !== $this->aSekolah->getSekolahId()) {
            $this->aSekolah = null;
        }
        if ($this->aPrasaranaRelatedByIdIndukPrasarana !== null && $this->id_induk_prasarana !== $this->aPrasaranaRelatedByIdIndukPrasarana->getPrasaranaId()) {
            $this->aPrasaranaRelatedByIdIndukPrasarana = null;
        }
        if ($this->aStatusKepemilikanSarpras !== null && $this->kepemilikan_sarpras_id !== $this->aStatusKepemilikanSarpras->getKepemilikanSarprasId()) {
            $this->aStatusKepemilikanSarpras = null;
        }
        if ($this->aJenisHapusBuku !== null && $this->id_hapus_buku !== $this->aJenisHapusBuku->getIdHapusBuku()) {
            $this->aJenisHapusBuku = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PrasaranaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPrasaranaRelatedByIdIndukPrasarana = null;
            $this->aSekolah = null;
            $this->aJenisHapusBuku = null;
            $this->aJenisPrasarana = null;
            $this->aStatusKepemilikanSarpras = null;
            $this->collBukuAlats = null;

            $this->collJadwals = null;

            $this->collVldPrasaranas = null;

            $this->collPrasaranasRelatedByPrasaranaId = null;

            $this->collPrasaranaLongitudinals = null;

            $this->collRombonganBelajars = null;

            $this->collSaranas = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PrasaranaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PrasaranaPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPrasaranaRelatedByIdIndukPrasarana !== null) {
                if ($this->aPrasaranaRelatedByIdIndukPrasarana->isModified() || $this->aPrasaranaRelatedByIdIndukPrasarana->isNew()) {
                    $affectedRows += $this->aPrasaranaRelatedByIdIndukPrasarana->save($con);
                }
                $this->setPrasaranaRelatedByIdIndukPrasarana($this->aPrasaranaRelatedByIdIndukPrasarana);
            }

            if ($this->aSekolah !== null) {
                if ($this->aSekolah->isModified() || $this->aSekolah->isNew()) {
                    $affectedRows += $this->aSekolah->save($con);
                }
                $this->setSekolah($this->aSekolah);
            }

            if ($this->aJenisHapusBuku !== null) {
                if ($this->aJenisHapusBuku->isModified() || $this->aJenisHapusBuku->isNew()) {
                    $affectedRows += $this->aJenisHapusBuku->save($con);
                }
                $this->setJenisHapusBuku($this->aJenisHapusBuku);
            }

            if ($this->aJenisPrasarana !== null) {
                if ($this->aJenisPrasarana->isModified() || $this->aJenisPrasarana->isNew()) {
                    $affectedRows += $this->aJenisPrasarana->save($con);
                }
                $this->setJenisPrasarana($this->aJenisPrasarana);
            }

            if ($this->aStatusKepemilikanSarpras !== null) {
                if ($this->aStatusKepemilikanSarpras->isModified() || $this->aStatusKepemilikanSarpras->isNew()) {
                    $affectedRows += $this->aStatusKepemilikanSarpras->save($con);
                }
                $this->setStatusKepemilikanSarpras($this->aStatusKepemilikanSarpras);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->bukuAlatsScheduledForDeletion !== null) {
                if (!$this->bukuAlatsScheduledForDeletion->isEmpty()) {
                    foreach ($this->bukuAlatsScheduledForDeletion as $bukuAlat) {
                        // need to save related object because we set the relation to null
                        $bukuAlat->save($con);
                    }
                    $this->bukuAlatsScheduledForDeletion = null;
                }
            }

            if ($this->collBukuAlats !== null) {
                foreach ($this->collBukuAlats as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->jadwalsScheduledForDeletion !== null) {
                if (!$this->jadwalsScheduledForDeletion->isEmpty()) {
                    JadwalQuery::create()
                        ->filterByPrimaryKeys($this->jadwalsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->jadwalsScheduledForDeletion = null;
                }
            }

            if ($this->collJadwals !== null) {
                foreach ($this->collJadwals as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vldPrasaranasScheduledForDeletion !== null) {
                if (!$this->vldPrasaranasScheduledForDeletion->isEmpty()) {
                    VldPrasaranaQuery::create()
                        ->filterByPrimaryKeys($this->vldPrasaranasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vldPrasaranasScheduledForDeletion = null;
                }
            }

            if ($this->collVldPrasaranas !== null) {
                foreach ($this->collVldPrasaranas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prasaranasRelatedByPrasaranaIdScheduledForDeletion !== null) {
                if (!$this->prasaranasRelatedByPrasaranaIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->prasaranasRelatedByPrasaranaIdScheduledForDeletion as $prasaranaRelatedByPrasaranaId) {
                        // need to save related object because we set the relation to null
                        $prasaranaRelatedByPrasaranaId->save($con);
                    }
                    $this->prasaranasRelatedByPrasaranaIdScheduledForDeletion = null;
                }
            }

            if ($this->collPrasaranasRelatedByPrasaranaId !== null) {
                foreach ($this->collPrasaranasRelatedByPrasaranaId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prasaranaLongitudinalsScheduledForDeletion !== null) {
                if (!$this->prasaranaLongitudinalsScheduledForDeletion->isEmpty()) {
                    PrasaranaLongitudinalQuery::create()
                        ->filterByPrimaryKeys($this->prasaranaLongitudinalsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prasaranaLongitudinalsScheduledForDeletion = null;
                }
            }

            if ($this->collPrasaranaLongitudinals !== null) {
                foreach ($this->collPrasaranaLongitudinals as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rombonganBelajarsScheduledForDeletion !== null) {
                if (!$this->rombonganBelajarsScheduledForDeletion->isEmpty()) {
                    RombonganBelajarQuery::create()
                        ->filterByPrimaryKeys($this->rombonganBelajarsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rombonganBelajarsScheduledForDeletion = null;
                }
            }

            if ($this->collRombonganBelajars !== null) {
                foreach ($this->collRombonganBelajars as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saranasScheduledForDeletion !== null) {
                if (!$this->saranasScheduledForDeletion->isEmpty()) {
                    foreach ($this->saranasScheduledForDeletion as $sarana) {
                        // need to save related object because we set the relation to null
                        $sarana->save($con);
                    }
                    $this->saranasScheduledForDeletion = null;
                }
            }

            if ($this->collSaranas !== null) {
                foreach ($this->collSaranas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPrasaranaRelatedByIdIndukPrasarana !== null) {
                if (!$this->aPrasaranaRelatedByIdIndukPrasarana->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPrasaranaRelatedByIdIndukPrasarana->getValidationFailures());
                }
            }

            if ($this->aSekolah !== null) {
                if (!$this->aSekolah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aSekolah->getValidationFailures());
                }
            }

            if ($this->aJenisHapusBuku !== null) {
                if (!$this->aJenisHapusBuku->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisHapusBuku->getValidationFailures());
                }
            }

            if ($this->aJenisPrasarana !== null) {
                if (!$this->aJenisPrasarana->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aJenisPrasarana->getValidationFailures());
                }
            }

            if ($this->aStatusKepemilikanSarpras !== null) {
                if (!$this->aStatusKepemilikanSarpras->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStatusKepemilikanSarpras->getValidationFailures());
                }
            }


            if (($retval = PrasaranaPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collBukuAlats !== null) {
                    foreach ($this->collBukuAlats as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collJadwals !== null) {
                    foreach ($this->collJadwals as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collVldPrasaranas !== null) {
                    foreach ($this->collVldPrasaranas as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPrasaranasRelatedByPrasaranaId !== null) {
                    foreach ($this->collPrasaranasRelatedByPrasaranaId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collPrasaranaLongitudinals !== null) {
                    foreach ($this->collPrasaranaLongitudinals as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRombonganBelajars !== null) {
                    foreach ($this->collRombonganBelajars as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSaranas !== null) {
                    foreach ($this->collSaranas as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_FIELDNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_FIELDNAME)
    {
        $pos = PrasaranaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPrasaranaId();
                break;
            case 1:
                return $this->getJenisPrasaranaId();
                break;
            case 2:
                return $this->getSekolahId();
                break;
            case 3:
                return $this->getIdIndukPrasarana();
                break;
            case 4:
                return $this->getKepemilikanSarprasId();
                break;
            case 5:
                return $this->getNama();
                break;
            case 6:
                return $this->getPanjang();
                break;
            case 7:
                return $this->getLebar();
                break;
            case 8:
                return $this->getRegPras();
                break;
            case 9:
                return $this->getKetPrasarana();
                break;
            case 10:
                return $this->getVolPondasiM3();
                break;
            case 11:
                return $this->getVolSloopKolomBalokM3();
                break;
            case 12:
                return $this->getLuasPlesterM2();
                break;
            case 13:
                return $this->getPanjKudakudaM();
                break;
            case 14:
                return $this->getVolKudakudaM3();
                break;
            case 15:
                return $this->getPanjKasoM();
                break;
            case 16:
                return $this->getPanjRengM();
                break;
            case 17:
                return $this->getLuasTutupAtapM2();
                break;
            case 18:
                return $this->getLuasPlafonM2();
                break;
            case 19:
                return $this->getLuasDindingM2();
                break;
            case 20:
                return $this->getLuasDaunJendelaM2();
                break;
            case 21:
                return $this->getLuasDaunPintuM2();
                break;
            case 22:
                return $this->getPanjKusenM();
                break;
            case 23:
                return $this->getLuasTutupLantaiM2();
                break;
            case 24:
                return $this->getPanjInstListrikM();
                break;
            case 25:
                return $this->getJmlInstListrik();
                break;
            case 26:
                return $this->getPanjInstAirM();
                break;
            case 27:
                return $this->getJmlInstAir();
                break;
            case 28:
                return $this->getPanjDrainaseM();
                break;
            case 29:
                return $this->getLuasFinishStrukturM2();
                break;
            case 30:
                return $this->getLuasFinishPlafonM2();
                break;
            case 31:
                return $this->getLuasFinishDindingM2();
                break;
            case 32:
                return $this->getLuasFinishKpjM2();
                break;
            case 33:
                return $this->getIdHapusBuku();
                break;
            case 34:
                return $this->getTglHapusBuku();
                break;
            case 35:
                return $this->getAsalData();
                break;
            case 36:
                return $this->getLastUpdate();
                break;
            case 37:
                return $this->getSoftDelete();
                break;
            case 38:
                return $this->getLastSync();
                break;
            case 39:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Prasarana'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Prasarana'][$this->getPrimaryKey()] = true;
        $keys = PrasaranaPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPrasaranaId(),
            $keys[1] => $this->getJenisPrasaranaId(),
            $keys[2] => $this->getSekolahId(),
            $keys[3] => $this->getIdIndukPrasarana(),
            $keys[4] => $this->getKepemilikanSarprasId(),
            $keys[5] => $this->getNama(),
            $keys[6] => $this->getPanjang(),
            $keys[7] => $this->getLebar(),
            $keys[8] => $this->getRegPras(),
            $keys[9] => $this->getKetPrasarana(),
            $keys[10] => $this->getVolPondasiM3(),
            $keys[11] => $this->getVolSloopKolomBalokM3(),
            $keys[12] => $this->getLuasPlesterM2(),
            $keys[13] => $this->getPanjKudakudaM(),
            $keys[14] => $this->getVolKudakudaM3(),
            $keys[15] => $this->getPanjKasoM(),
            $keys[16] => $this->getPanjRengM(),
            $keys[17] => $this->getLuasTutupAtapM2(),
            $keys[18] => $this->getLuasPlafonM2(),
            $keys[19] => $this->getLuasDindingM2(),
            $keys[20] => $this->getLuasDaunJendelaM2(),
            $keys[21] => $this->getLuasDaunPintuM2(),
            $keys[22] => $this->getPanjKusenM(),
            $keys[23] => $this->getLuasTutupLantaiM2(),
            $keys[24] => $this->getPanjInstListrikM(),
            $keys[25] => $this->getJmlInstListrik(),
            $keys[26] => $this->getPanjInstAirM(),
            $keys[27] => $this->getJmlInstAir(),
            $keys[28] => $this->getPanjDrainaseM(),
            $keys[29] => $this->getLuasFinishStrukturM2(),
            $keys[30] => $this->getLuasFinishPlafonM2(),
            $keys[31] => $this->getLuasFinishDindingM2(),
            $keys[32] => $this->getLuasFinishKpjM2(),
            $keys[33] => $this->getIdHapusBuku(),
            $keys[34] => $this->getTglHapusBuku(),
            $keys[35] => $this->getAsalData(),
            $keys[36] => $this->getLastUpdate(),
            $keys[37] => $this->getSoftDelete(),
            $keys[38] => $this->getLastSync(),
            $keys[39] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aPrasaranaRelatedByIdIndukPrasarana) {
                $result['PrasaranaRelatedByIdIndukPrasarana'] = $this->aPrasaranaRelatedByIdIndukPrasarana->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSekolah) {
                $result['Sekolah'] = $this->aSekolah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisHapusBuku) {
                $result['JenisHapusBuku'] = $this->aJenisHapusBuku->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aJenisPrasarana) {
                $result['JenisPrasarana'] = $this->aJenisPrasarana->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStatusKepemilikanSarpras) {
                $result['StatusKepemilikanSarpras'] = $this->aStatusKepemilikanSarpras->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBukuAlats) {
                $result['BukuAlats'] = $this->collBukuAlats->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collJadwals) {
                $result['Jadwals'] = $this->collJadwals->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVldPrasaranas) {
                $result['VldPrasaranas'] = $this->collVldPrasaranas->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrasaranasRelatedByPrasaranaId) {
                $result['PrasaranasRelatedByPrasaranaId'] = $this->collPrasaranasRelatedByPrasaranaId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrasaranaLongitudinals) {
                $result['PrasaranaLongitudinals'] = $this->collPrasaranaLongitudinals->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRombonganBelajars) {
                $result['RombonganBelajars'] = $this->collRombonganBelajars->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaranas) {
                $result['Saranas'] = $this->collSaranas->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_FIELDNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_FIELDNAME)
    {
        $pos = PrasaranaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPrasaranaId($value);
                break;
            case 1:
                $this->setJenisPrasaranaId($value);
                break;
            case 2:
                $this->setSekolahId($value);
                break;
            case 3:
                $this->setIdIndukPrasarana($value);
                break;
            case 4:
                $this->setKepemilikanSarprasId($value);
                break;
            case 5:
                $this->setNama($value);
                break;
            case 6:
                $this->setPanjang($value);
                break;
            case 7:
                $this->setLebar($value);
                break;
            case 8:
                $this->setRegPras($value);
                break;
            case 9:
                $this->setKetPrasarana($value);
                break;
            case 10:
                $this->setVolPondasiM3($value);
                break;
            case 11:
                $this->setVolSloopKolomBalokM3($value);
                break;
            case 12:
                $this->setLuasPlesterM2($value);
                break;
            case 13:
                $this->setPanjKudakudaM($value);
                break;
            case 14:
                $this->setVolKudakudaM3($value);
                break;
            case 15:
                $this->setPanjKasoM($value);
                break;
            case 16:
                $this->setPanjRengM($value);
                break;
            case 17:
                $this->setLuasTutupAtapM2($value);
                break;
            case 18:
                $this->setLuasPlafonM2($value);
                break;
            case 19:
                $this->setLuasDindingM2($value);
                break;
            case 20:
                $this->setLuasDaunJendelaM2($value);
                break;
            case 21:
                $this->setLuasDaunPintuM2($value);
                break;
            case 22:
                $this->setPanjKusenM($value);
                break;
            case 23:
                $this->setLuasTutupLantaiM2($value);
                break;
            case 24:
                $this->setPanjInstListrikM($value);
                break;
            case 25:
                $this->setJmlInstListrik($value);
                break;
            case 26:
                $this->setPanjInstAirM($value);
                break;
            case 27:
                $this->setJmlInstAir($value);
                break;
            case 28:
                $this->setPanjDrainaseM($value);
                break;
            case 29:
                $this->setLuasFinishStrukturM2($value);
                break;
            case 30:
                $this->setLuasFinishPlafonM2($value);
                break;
            case 31:
                $this->setLuasFinishDindingM2($value);
                break;
            case 32:
                $this->setLuasFinishKpjM2($value);
                break;
            case 33:
                $this->setIdHapusBuku($value);
                break;
            case 34:
                $this->setTglHapusBuku($value);
                break;
            case 35:
                $this->setAsalData($value);
                break;
            case 36:
                $this->setLastUpdate($value);
                break;
            case 37:
                $this->setSoftDelete($value);
                break;
            case 38:
                $this->setLastSync($value);
                break;
            case 39:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_FIELDNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_FIELDNAME)
    {
        $keys = PrasaranaPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPrasaranaId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setJenisPrasaranaId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSekolahId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setIdIndukPrasarana($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setKepemilikanSarprasId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setNama($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setPanjang($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setLebar($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setRegPras($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setKetPrasarana($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setVolPondasiM3($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setVolSloopKolomBalokM3($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setLuasPlesterM2($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setPanjKudakudaM($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setVolKudakudaM3($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setPanjKasoM($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setPanjRengM($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setLuasTutupAtapM2($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setLuasPlafonM2($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setLuasDindingM2($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setLuasDaunJendelaM2($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setLuasDaunPintuM2($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setPanjKusenM($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setLuasTutupLantaiM2($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setPanjInstListrikM($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setJmlInstListrik($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setPanjInstAirM($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setJmlInstAir($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setPanjDrainaseM($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setLuasFinishStrukturM2($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setLuasFinishPlafonM2($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setLuasFinishDindingM2($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setLuasFinishKpjM2($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setIdHapusBuku($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setTglHapusBuku($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setAsalData($arr[$keys[35]]);
        if (array_key_exists($keys[36], $arr)) $this->setLastUpdate($arr[$keys[36]]);
        if (array_key_exists($keys[37], $arr)) $this->setSoftDelete($arr[$keys[37]]);
        if (array_key_exists($keys[38], $arr)) $this->setLastSync($arr[$keys[38]]);
        if (array_key_exists($keys[39], $arr)) $this->setUpdaterId($arr[$keys[39]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PrasaranaPeer::DATABASE_NAME);

        if ($this->isColumnModified(PrasaranaPeer::PRASARANA_ID)) $criteria->add(PrasaranaPeer::PRASARANA_ID, $this->prasarana_id);
        if ($this->isColumnModified(PrasaranaPeer::JENIS_PRASARANA_ID)) $criteria->add(PrasaranaPeer::JENIS_PRASARANA_ID, $this->jenis_prasarana_id);
        if ($this->isColumnModified(PrasaranaPeer::SEKOLAH_ID)) $criteria->add(PrasaranaPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(PrasaranaPeer::ID_INDUK_PRASARANA)) $criteria->add(PrasaranaPeer::ID_INDUK_PRASARANA, $this->id_induk_prasarana);
        if ($this->isColumnModified(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID)) $criteria->add(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, $this->kepemilikan_sarpras_id);
        if ($this->isColumnModified(PrasaranaPeer::NAMA)) $criteria->add(PrasaranaPeer::NAMA, $this->nama);
        if ($this->isColumnModified(PrasaranaPeer::PANJANG)) $criteria->add(PrasaranaPeer::PANJANG, $this->panjang);
        if ($this->isColumnModified(PrasaranaPeer::LEBAR)) $criteria->add(PrasaranaPeer::LEBAR, $this->lebar);
        if ($this->isColumnModified(PrasaranaPeer::REG_PRAS)) $criteria->add(PrasaranaPeer::REG_PRAS, $this->reg_pras);
        if ($this->isColumnModified(PrasaranaPeer::KET_PRASARANA)) $criteria->add(PrasaranaPeer::KET_PRASARANA, $this->ket_prasarana);
        if ($this->isColumnModified(PrasaranaPeer::VOL_PONDASI_M3)) $criteria->add(PrasaranaPeer::VOL_PONDASI_M3, $this->vol_pondasi_m3);
        if ($this->isColumnModified(PrasaranaPeer::VOL_SLOOP_KOLOM_BALOK_M3)) $criteria->add(PrasaranaPeer::VOL_SLOOP_KOLOM_BALOK_M3, $this->vol_sloop_kolom_balok_m3);
        if ($this->isColumnModified(PrasaranaPeer::LUAS_PLESTER_M2)) $criteria->add(PrasaranaPeer::LUAS_PLESTER_M2, $this->luas_plester_m2);
        if ($this->isColumnModified(PrasaranaPeer::PANJ_KUDAKUDA_M)) $criteria->add(PrasaranaPeer::PANJ_KUDAKUDA_M, $this->panj_kudakuda_m);
        if ($this->isColumnModified(PrasaranaPeer::VOL_KUDAKUDA_M3)) $criteria->add(PrasaranaPeer::VOL_KUDAKUDA_M3, $this->vol_kudakuda_m3);
        if ($this->isColumnModified(PrasaranaPeer::PANJ_KASO_M)) $criteria->add(PrasaranaPeer::PANJ_KASO_M, $this->panj_kaso_m);
        if ($this->isColumnModified(PrasaranaPeer::PANJ_RENG_M)) $criteria->add(PrasaranaPeer::PANJ_RENG_M, $this->panj_reng_m);
        if ($this->isColumnModified(PrasaranaPeer::LUAS_TUTUP_ATAP_M2)) $criteria->add(PrasaranaPeer::LUAS_TUTUP_ATAP_M2, $this->luas_tutup_atap_m2);
        if ($this->isColumnModified(PrasaranaPeer::LUAS_PLAFON_M2)) $criteria->add(PrasaranaPeer::LUAS_PLAFON_M2, $this->luas_plafon_m2);
        if ($this->isColumnModified(PrasaranaPeer::LUAS_DINDING_M2)) $criteria->add(PrasaranaPeer::LUAS_DINDING_M2, $this->luas_dinding_m2);
        if ($this->isColumnModified(PrasaranaPeer::LUAS_DAUN_JENDELA_M2)) $criteria->add(PrasaranaPeer::LUAS_DAUN_JENDELA_M2, $this->luas_daun_jendela_m2);
        if ($this->isColumnModified(PrasaranaPeer::LUAS_DAUN_PINTU_M2)) $criteria->add(PrasaranaPeer::LUAS_DAUN_PINTU_M2, $this->luas_daun_pintu_m2);
        if ($this->isColumnModified(PrasaranaPeer::PANJ_KUSEN_M)) $criteria->add(PrasaranaPeer::PANJ_KUSEN_M, $this->panj_kusen_m);
        if ($this->isColumnModified(PrasaranaPeer::LUAS_TUTUP_LANTAI_M2)) $criteria->add(PrasaranaPeer::LUAS_TUTUP_LANTAI_M2, $this->luas_tutup_lantai_m2);
        if ($this->isColumnModified(PrasaranaPeer::PANJ_INST_LISTRIK_M)) $criteria->add(PrasaranaPeer::PANJ_INST_LISTRIK_M, $this->panj_inst_listrik_m);
        if ($this->isColumnModified(PrasaranaPeer::JML_INST_LISTRIK)) $criteria->add(PrasaranaPeer::JML_INST_LISTRIK, $this->jml_inst_listrik);
        if ($this->isColumnModified(PrasaranaPeer::PANJ_INST_AIR_M)) $criteria->add(PrasaranaPeer::PANJ_INST_AIR_M, $this->panj_inst_air_m);
        if ($this->isColumnModified(PrasaranaPeer::JML_INST_AIR)) $criteria->add(PrasaranaPeer::JML_INST_AIR, $this->jml_inst_air);
        if ($this->isColumnModified(PrasaranaPeer::PANJ_DRAINASE_M)) $criteria->add(PrasaranaPeer::PANJ_DRAINASE_M, $this->panj_drainase_m);
        if ($this->isColumnModified(PrasaranaPeer::LUAS_FINISH_STRUKTUR_M2)) $criteria->add(PrasaranaPeer::LUAS_FINISH_STRUKTUR_M2, $this->luas_finish_struktur_m2);
        if ($this->isColumnModified(PrasaranaPeer::LUAS_FINISH_PLAFON_M2)) $criteria->add(PrasaranaPeer::LUAS_FINISH_PLAFON_M2, $this->luas_finish_plafon_m2);
        if ($this->isColumnModified(PrasaranaPeer::LUAS_FINISH_DINDING_M2)) $criteria->add(PrasaranaPeer::LUAS_FINISH_DINDING_M2, $this->luas_finish_dinding_m2);
        if ($this->isColumnModified(PrasaranaPeer::LUAS_FINISH_KPJ_M2)) $criteria->add(PrasaranaPeer::LUAS_FINISH_KPJ_M2, $this->luas_finish_kpj_m2);
        if ($this->isColumnModified(PrasaranaPeer::ID_HAPUS_BUKU)) $criteria->add(PrasaranaPeer::ID_HAPUS_BUKU, $this->id_hapus_buku);
        if ($this->isColumnModified(PrasaranaPeer::TGL_HAPUS_BUKU)) $criteria->add(PrasaranaPeer::TGL_HAPUS_BUKU, $this->tgl_hapus_buku);
        if ($this->isColumnModified(PrasaranaPeer::ASAL_DATA)) $criteria->add(PrasaranaPeer::ASAL_DATA, $this->asal_data);
        if ($this->isColumnModified(PrasaranaPeer::LAST_UPDATE)) $criteria->add(PrasaranaPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(PrasaranaPeer::SOFT_DELETE)) $criteria->add(PrasaranaPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(PrasaranaPeer::LAST_SYNC)) $criteria->add(PrasaranaPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(PrasaranaPeer::UPDATER_ID)) $criteria->add(PrasaranaPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PrasaranaPeer::DATABASE_NAME);
        $criteria->add(PrasaranaPeer::PRASARANA_ID, $this->prasarana_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getPrasaranaId();
    }

    /**
     * Generic method to set the primary key (prasarana_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setPrasaranaId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getPrasaranaId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Prasarana (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setJenisPrasaranaId($this->getJenisPrasaranaId());
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setIdIndukPrasarana($this->getIdIndukPrasarana());
        $copyObj->setKepemilikanSarprasId($this->getKepemilikanSarprasId());
        $copyObj->setNama($this->getNama());
        $copyObj->setPanjang($this->getPanjang());
        $copyObj->setLebar($this->getLebar());
        $copyObj->setRegPras($this->getRegPras());
        $copyObj->setKetPrasarana($this->getKetPrasarana());
        $copyObj->setVolPondasiM3($this->getVolPondasiM3());
        $copyObj->setVolSloopKolomBalokM3($this->getVolSloopKolomBalokM3());
        $copyObj->setLuasPlesterM2($this->getLuasPlesterM2());
        $copyObj->setPanjKudakudaM($this->getPanjKudakudaM());
        $copyObj->setVolKudakudaM3($this->getVolKudakudaM3());
        $copyObj->setPanjKasoM($this->getPanjKasoM());
        $copyObj->setPanjRengM($this->getPanjRengM());
        $copyObj->setLuasTutupAtapM2($this->getLuasTutupAtapM2());
        $copyObj->setLuasPlafonM2($this->getLuasPlafonM2());
        $copyObj->setLuasDindingM2($this->getLuasDindingM2());
        $copyObj->setLuasDaunJendelaM2($this->getLuasDaunJendelaM2());
        $copyObj->setLuasDaunPintuM2($this->getLuasDaunPintuM2());
        $copyObj->setPanjKusenM($this->getPanjKusenM());
        $copyObj->setLuasTutupLantaiM2($this->getLuasTutupLantaiM2());
        $copyObj->setPanjInstListrikM($this->getPanjInstListrikM());
        $copyObj->setJmlInstListrik($this->getJmlInstListrik());
        $copyObj->setPanjInstAirM($this->getPanjInstAirM());
        $copyObj->setJmlInstAir($this->getJmlInstAir());
        $copyObj->setPanjDrainaseM($this->getPanjDrainaseM());
        $copyObj->setLuasFinishStrukturM2($this->getLuasFinishStrukturM2());
        $copyObj->setLuasFinishPlafonM2($this->getLuasFinishPlafonM2());
        $copyObj->setLuasFinishDindingM2($this->getLuasFinishDindingM2());
        $copyObj->setLuasFinishKpjM2($this->getLuasFinishKpjM2());
        $copyObj->setIdHapusBuku($this->getIdHapusBuku());
        $copyObj->setTglHapusBuku($this->getTglHapusBuku());
        $copyObj->setAsalData($this->getAsalData());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getBukuAlats() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBukuAlat($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getJadwals() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addJadwal($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVldPrasaranas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVldPrasarana($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrasaranasRelatedByPrasaranaId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrasaranaRelatedByPrasaranaId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrasaranaLongitudinals() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrasaranaLongitudinal($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRombonganBelajars() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRombonganBelajar($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaranas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSarana($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setPrasaranaId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Prasarana Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PrasaranaPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PrasaranaPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Prasarana object.
     *
     * @param             Prasarana $v
     * @return Prasarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPrasaranaRelatedByIdIndukPrasarana(Prasarana $v = null)
    {
        if ($v === null) {
            $this->setIdIndukPrasarana(NULL);
        } else {
            $this->setIdIndukPrasarana($v->getPrasaranaId());
        }

        $this->aPrasaranaRelatedByIdIndukPrasarana = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Prasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasaranaRelatedByPrasaranaId($this);
        }


        return $this;
    }


    /**
     * Get the associated Prasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Prasarana The associated Prasarana object.
     * @throws PropelException
     */
    public function getPrasaranaRelatedByIdIndukPrasarana(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPrasaranaRelatedByIdIndukPrasarana === null && (($this->id_induk_prasarana !== "" && $this->id_induk_prasarana !== null)) && $doQuery) {
            $this->aPrasaranaRelatedByIdIndukPrasarana = PrasaranaQuery::create()->findPk($this->id_induk_prasarana, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPrasaranaRelatedByIdIndukPrasarana->addPrasaranasRelatedByPrasaranaId($this);
             */
        }

        return $this->aPrasaranaRelatedByIdIndukPrasarana;
    }

    /**
     * Declares an association between this object and a Sekolah object.
     *
     * @param             Sekolah $v
     * @return Prasarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSekolah(Sekolah $v = null)
    {
        if ($v === null) {
            $this->setSekolahId(NULL);
        } else {
            $this->setSekolahId($v->getSekolahId());
        }

        $this->aSekolah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Sekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasarana($this);
        }


        return $this;
    }


    /**
     * Get the associated Sekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Sekolah The associated Sekolah object.
     * @throws PropelException
     */
    public function getSekolah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aSekolah === null && (($this->sekolah_id !== "" && $this->sekolah_id !== null)) && $doQuery) {
            $this->aSekolah = SekolahQuery::create()->findPk($this->sekolah_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSekolah->addPrasaranas($this);
             */
        }

        return $this->aSekolah;
    }

    /**
     * Declares an association between this object and a JenisHapusBuku object.
     *
     * @param             JenisHapusBuku $v
     * @return Prasarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisHapusBuku(JenisHapusBuku $v = null)
    {
        if ($v === null) {
            $this->setIdHapusBuku(NULL);
        } else {
            $this->setIdHapusBuku($v->getIdHapusBuku());
        }

        $this->aJenisHapusBuku = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisHapusBuku object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasarana($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisHapusBuku object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisHapusBuku The associated JenisHapusBuku object.
     * @throws PropelException
     */
    public function getJenisHapusBuku(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisHapusBuku === null && (($this->id_hapus_buku !== "" && $this->id_hapus_buku !== null)) && $doQuery) {
            $this->aJenisHapusBuku = JenisHapusBukuQuery::create()->findPk($this->id_hapus_buku, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisHapusBuku->addPrasaranas($this);
             */
        }

        return $this->aJenisHapusBuku;
    }

    /**
     * Declares an association between this object and a JenisPrasarana object.
     *
     * @param             JenisPrasarana $v
     * @return Prasarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setJenisPrasarana(JenisPrasarana $v = null)
    {
        if ($v === null) {
            $this->setJenisPrasaranaId(NULL);
        } else {
            $this->setJenisPrasaranaId($v->getJenisPrasaranaId());
        }

        $this->aJenisPrasarana = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the JenisPrasarana object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasarana($this);
        }


        return $this;
    }


    /**
     * Get the associated JenisPrasarana object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return JenisPrasarana The associated JenisPrasarana object.
     * @throws PropelException
     */
    public function getJenisPrasarana(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aJenisPrasarana === null && ($this->jenis_prasarana_id !== null) && $doQuery) {
            $this->aJenisPrasarana = JenisPrasaranaQuery::create()->findPk($this->jenis_prasarana_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aJenisPrasarana->addPrasaranas($this);
             */
        }

        return $this->aJenisPrasarana;
    }

    /**
     * Declares an association between this object and a StatusKepemilikanSarpras object.
     *
     * @param             StatusKepemilikanSarpras $v
     * @return Prasarana The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStatusKepemilikanSarpras(StatusKepemilikanSarpras $v = null)
    {
        if ($v === null) {
            $this->setKepemilikanSarprasId(NULL);
        } else {
            $this->setKepemilikanSarprasId($v->getKepemilikanSarprasId());
        }

        $this->aStatusKepemilikanSarpras = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the StatusKepemilikanSarpras object, it will not be re-added.
        if ($v !== null) {
            $v->addPrasarana($this);
        }


        return $this;
    }


    /**
     * Get the associated StatusKepemilikanSarpras object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return StatusKepemilikanSarpras The associated StatusKepemilikanSarpras object.
     * @throws PropelException
     */
    public function getStatusKepemilikanSarpras(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStatusKepemilikanSarpras === null && (($this->kepemilikan_sarpras_id !== "" && $this->kepemilikan_sarpras_id !== null)) && $doQuery) {
            $this->aStatusKepemilikanSarpras = StatusKepemilikanSarprasQuery::create()->findPk($this->kepemilikan_sarpras_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStatusKepemilikanSarpras->addPrasaranas($this);
             */
        }

        return $this->aStatusKepemilikanSarpras;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('BukuAlat' == $relationName) {
            $this->initBukuAlats();
        }
        if ('Jadwal' == $relationName) {
            $this->initJadwals();
        }
        if ('VldPrasarana' == $relationName) {
            $this->initVldPrasaranas();
        }
        if ('PrasaranaRelatedByPrasaranaId' == $relationName) {
            $this->initPrasaranasRelatedByPrasaranaId();
        }
        if ('PrasaranaLongitudinal' == $relationName) {
            $this->initPrasaranaLongitudinals();
        }
        if ('RombonganBelajar' == $relationName) {
            $this->initRombonganBelajars();
        }
        if ('Sarana' == $relationName) {
            $this->initSaranas();
        }
    }

    /**
     * Clears out the collBukuAlats collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addBukuAlats()
     */
    public function clearBukuAlats()
    {
        $this->collBukuAlats = null; // important to set this to null since that means it is uninitialized
        $this->collBukuAlatsPartial = null;

        return $this;
    }

    /**
     * reset is the collBukuAlats collection loaded partially
     *
     * @return void
     */
    public function resetPartialBukuAlats($v = true)
    {
        $this->collBukuAlatsPartial = $v;
    }

    /**
     * Initializes the collBukuAlats collection.
     *
     * By default this just sets the collBukuAlats collection to an empty array (like clearcollBukuAlats());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBukuAlats($overrideExisting = true)
    {
        if (null !== $this->collBukuAlats && !$overrideExisting) {
            return;
        }
        $this->collBukuAlats = new PropelObjectCollection();
        $this->collBukuAlats->setModel('BukuAlat');
    }

    /**
     * Gets an array of BukuAlat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     * @throws PropelException
     */
    public function getBukuAlats($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatsPartial && !$this->isNew();
        if (null === $this->collBukuAlats || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBukuAlats) {
                // return empty collection
                $this->initBukuAlats();
            } else {
                $collBukuAlats = BukuAlatQuery::create(null, $criteria)
                    ->filterByPrasarana($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBukuAlatsPartial && count($collBukuAlats)) {
                      $this->initBukuAlats(false);

                      foreach($collBukuAlats as $obj) {
                        if (false == $this->collBukuAlats->contains($obj)) {
                          $this->collBukuAlats->append($obj);
                        }
                      }

                      $this->collBukuAlatsPartial = true;
                    }

                    $collBukuAlats->getInternalIterator()->rewind();
                    return $collBukuAlats;
                }

                if($partial && $this->collBukuAlats) {
                    foreach($this->collBukuAlats as $obj) {
                        if($obj->isNew()) {
                            $collBukuAlats[] = $obj;
                        }
                    }
                }

                $this->collBukuAlats = $collBukuAlats;
                $this->collBukuAlatsPartial = false;
            }
        }

        return $this->collBukuAlats;
    }

    /**
     * Sets a collection of BukuAlat objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $bukuAlats A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setBukuAlats(PropelCollection $bukuAlats, PropelPDO $con = null)
    {
        $bukuAlatsToDelete = $this->getBukuAlats(new Criteria(), $con)->diff($bukuAlats);

        $this->bukuAlatsScheduledForDeletion = unserialize(serialize($bukuAlatsToDelete));

        foreach ($bukuAlatsToDelete as $bukuAlatRemoved) {
            $bukuAlatRemoved->setPrasarana(null);
        }

        $this->collBukuAlats = null;
        foreach ($bukuAlats as $bukuAlat) {
            $this->addBukuAlat($bukuAlat);
        }

        $this->collBukuAlats = $bukuAlats;
        $this->collBukuAlatsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BukuAlat objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related BukuAlat objects.
     * @throws PropelException
     */
    public function countBukuAlats(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBukuAlatsPartial && !$this->isNew();
        if (null === $this->collBukuAlats || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBukuAlats) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getBukuAlats());
            }
            $query = BukuAlatQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasarana($this)
                ->count($con);
        }

        return count($this->collBukuAlats);
    }

    /**
     * Method called to associate a BukuAlat object to this object
     * through the BukuAlat foreign key attribute.
     *
     * @param    BukuAlat $l BukuAlat
     * @return Prasarana The current object (for fluent API support)
     */
    public function addBukuAlat(BukuAlat $l)
    {
        if ($this->collBukuAlats === null) {
            $this->initBukuAlats();
            $this->collBukuAlatsPartial = true;
        }
        if (!in_array($l, $this->collBukuAlats->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBukuAlat($l);
        }

        return $this;
    }

    /**
     * @param	BukuAlat $bukuAlat The bukuAlat object to add.
     */
    protected function doAddBukuAlat($bukuAlat)
    {
        $this->collBukuAlats[]= $bukuAlat;
        $bukuAlat->setPrasarana($this);
    }

    /**
     * @param	BukuAlat $bukuAlat The bukuAlat object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeBukuAlat($bukuAlat)
    {
        if ($this->getBukuAlats()->contains($bukuAlat)) {
            $this->collBukuAlats->remove($this->collBukuAlats->search($bukuAlat));
            if (null === $this->bukuAlatsScheduledForDeletion) {
                $this->bukuAlatsScheduledForDeletion = clone $this->collBukuAlats;
                $this->bukuAlatsScheduledForDeletion->clear();
            }
            $this->bukuAlatsScheduledForDeletion[]= $bukuAlat;
            $bukuAlat->setPrasarana(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsJoinSekolah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('Sekolah', $join_behavior);

        return $this->getBukuAlats($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsJoinJenisBukuAlat($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('JenisBukuAlat', $join_behavior);

        return $this->getBukuAlats($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsJoinJenisHapusBuku($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('JenisHapusBuku', $join_behavior);

        return $this->getBukuAlats($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsJoinMataPelajaran($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('MataPelajaran', $join_behavior);

        return $this->getBukuAlats($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related BukuAlats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|BukuAlat[] List of BukuAlat objects
     */
    public function getBukuAlatsJoinTingkatPendidikan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = BukuAlatQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikan', $join_behavior);

        return $this->getBukuAlats($query, $con);
    }

    /**
     * Clears out the collJadwals collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addJadwals()
     */
    public function clearJadwals()
    {
        $this->collJadwals = null; // important to set this to null since that means it is uninitialized
        $this->collJadwalsPartial = null;

        return $this;
    }

    /**
     * reset is the collJadwals collection loaded partially
     *
     * @return void
     */
    public function resetPartialJadwals($v = true)
    {
        $this->collJadwalsPartial = $v;
    }

    /**
     * Initializes the collJadwals collection.
     *
     * By default this just sets the collJadwals collection to an empty array (like clearcollJadwals());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initJadwals($overrideExisting = true)
    {
        if (null !== $this->collJadwals && !$overrideExisting) {
            return;
        }
        $this->collJadwals = new PropelObjectCollection();
        $this->collJadwals->setModel('Jadwal');
    }

    /**
     * Gets an array of Jadwal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     * @throws PropelException
     */
    public function getJadwals($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collJadwalsPartial && !$this->isNew();
        if (null === $this->collJadwals || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collJadwals) {
                // return empty collection
                $this->initJadwals();
            } else {
                $collJadwals = JadwalQuery::create(null, $criteria)
                    ->filterByPrasarana($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collJadwalsPartial && count($collJadwals)) {
                      $this->initJadwals(false);

                      foreach($collJadwals as $obj) {
                        if (false == $this->collJadwals->contains($obj)) {
                          $this->collJadwals->append($obj);
                        }
                      }

                      $this->collJadwalsPartial = true;
                    }

                    $collJadwals->getInternalIterator()->rewind();
                    return $collJadwals;
                }

                if($partial && $this->collJadwals) {
                    foreach($this->collJadwals as $obj) {
                        if($obj->isNew()) {
                            $collJadwals[] = $obj;
                        }
                    }
                }

                $this->collJadwals = $collJadwals;
                $this->collJadwalsPartial = false;
            }
        }

        return $this->collJadwals;
    }

    /**
     * Sets a collection of Jadwal objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $jadwals A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setJadwals(PropelCollection $jadwals, PropelPDO $con = null)
    {
        $jadwalsToDelete = $this->getJadwals(new Criteria(), $con)->diff($jadwals);

        $this->jadwalsScheduledForDeletion = unserialize(serialize($jadwalsToDelete));

        foreach ($jadwalsToDelete as $jadwalRemoved) {
            $jadwalRemoved->setPrasarana(null);
        }

        $this->collJadwals = null;
        foreach ($jadwals as $jadwal) {
            $this->addJadwal($jadwal);
        }

        $this->collJadwals = $jadwals;
        $this->collJadwalsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Jadwal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Jadwal objects.
     * @throws PropelException
     */
    public function countJadwals(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collJadwalsPartial && !$this->isNew();
        if (null === $this->collJadwals || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collJadwals) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getJadwals());
            }
            $query = JadwalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasarana($this)
                ->count($con);
        }

        return count($this->collJadwals);
    }

    /**
     * Method called to associate a Jadwal object to this object
     * through the Jadwal foreign key attribute.
     *
     * @param    Jadwal $l Jadwal
     * @return Prasarana The current object (for fluent API support)
     */
    public function addJadwal(Jadwal $l)
    {
        if ($this->collJadwals === null) {
            $this->initJadwals();
            $this->collJadwalsPartial = true;
        }
        if (!in_array($l, $this->collJadwals->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddJadwal($l);
        }

        return $this;
    }

    /**
     * @param	Jadwal $jadwal The jadwal object to add.
     */
    protected function doAddJadwal($jadwal)
    {
        $this->collJadwals[]= $jadwal;
        $jadwal->setPrasarana($this);
    }

    /**
     * @param	Jadwal $jadwal The jadwal object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeJadwal($jadwal)
    {
        if ($this->getJadwals()->contains($jadwal)) {
            $this->collJadwals->remove($this->collJadwals->search($jadwal));
            if (null === $this->jadwalsScheduledForDeletion) {
                $this->jadwalsScheduledForDeletion = clone $this->collJadwals;
                $this->jadwalsScheduledForDeletion->clear();
            }
            $this->jadwalsScheduledForDeletion[]= clone $jadwal;
            $jadwal->setPrasarana(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe01($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe01', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe02($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe02', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe03($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe03', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe04($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe04', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe05($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe05', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe06($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe06', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe19($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe19', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe20($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe20', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe13($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe13', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe14($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe14', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe15($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe15', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe16($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe16', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe17($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe17', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe18($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe18', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe07($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe07', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe08($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe08', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe09($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe09', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe10($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe10', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe11($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe11', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinPembelajaranRelatedByBelKe12($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('PembelajaranRelatedByBelKe12', $join_behavior);

        return $this->getJadwals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Jadwals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Jadwal[] List of Jadwal objects
     */
    public function getJadwalsJoinSekolahLongitudinal($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = JadwalQuery::create(null, $criteria);
        $query->joinWith('SekolahLongitudinal', $join_behavior);

        return $this->getJadwals($query, $con);
    }

    /**
     * Clears out the collVldPrasaranas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addVldPrasaranas()
     */
    public function clearVldPrasaranas()
    {
        $this->collVldPrasaranas = null; // important to set this to null since that means it is uninitialized
        $this->collVldPrasaranasPartial = null;

        return $this;
    }

    /**
     * reset is the collVldPrasaranas collection loaded partially
     *
     * @return void
     */
    public function resetPartialVldPrasaranas($v = true)
    {
        $this->collVldPrasaranasPartial = $v;
    }

    /**
     * Initializes the collVldPrasaranas collection.
     *
     * By default this just sets the collVldPrasaranas collection to an empty array (like clearcollVldPrasaranas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVldPrasaranas($overrideExisting = true)
    {
        if (null !== $this->collVldPrasaranas && !$overrideExisting) {
            return;
        }
        $this->collVldPrasaranas = new PropelObjectCollection();
        $this->collVldPrasaranas->setModel('VldPrasarana');
    }

    /**
     * Gets an array of VldPrasarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     * @throws PropelException
     */
    public function getVldPrasaranas($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collVldPrasaranasPartial && !$this->isNew();
        if (null === $this->collVldPrasaranas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVldPrasaranas) {
                // return empty collection
                $this->initVldPrasaranas();
            } else {
                $collVldPrasaranas = VldPrasaranaQuery::create(null, $criteria)
                    ->filterByPrasarana($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collVldPrasaranasPartial && count($collVldPrasaranas)) {
                      $this->initVldPrasaranas(false);

                      foreach($collVldPrasaranas as $obj) {
                        if (false == $this->collVldPrasaranas->contains($obj)) {
                          $this->collVldPrasaranas->append($obj);
                        }
                      }

                      $this->collVldPrasaranasPartial = true;
                    }

                    $collVldPrasaranas->getInternalIterator()->rewind();
                    return $collVldPrasaranas;
                }

                if($partial && $this->collVldPrasaranas) {
                    foreach($this->collVldPrasaranas as $obj) {
                        if($obj->isNew()) {
                            $collVldPrasaranas[] = $obj;
                        }
                    }
                }

                $this->collVldPrasaranas = $collVldPrasaranas;
                $this->collVldPrasaranasPartial = false;
            }
        }

        return $this->collVldPrasaranas;
    }

    /**
     * Sets a collection of VldPrasarana objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $vldPrasaranas A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setVldPrasaranas(PropelCollection $vldPrasaranas, PropelPDO $con = null)
    {
        $vldPrasaranasToDelete = $this->getVldPrasaranas(new Criteria(), $con)->diff($vldPrasaranas);

        $this->vldPrasaranasScheduledForDeletion = unserialize(serialize($vldPrasaranasToDelete));

        foreach ($vldPrasaranasToDelete as $vldPrasaranaRemoved) {
            $vldPrasaranaRemoved->setPrasarana(null);
        }

        $this->collVldPrasaranas = null;
        foreach ($vldPrasaranas as $vldPrasarana) {
            $this->addVldPrasarana($vldPrasarana);
        }

        $this->collVldPrasaranas = $vldPrasaranas;
        $this->collVldPrasaranasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VldPrasarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related VldPrasarana objects.
     * @throws PropelException
     */
    public function countVldPrasaranas(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collVldPrasaranasPartial && !$this->isNew();
        if (null === $this->collVldPrasaranas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVldPrasaranas) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getVldPrasaranas());
            }
            $query = VldPrasaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasarana($this)
                ->count($con);
        }

        return count($this->collVldPrasaranas);
    }

    /**
     * Method called to associate a VldPrasarana object to this object
     * through the VldPrasarana foreign key attribute.
     *
     * @param    VldPrasarana $l VldPrasarana
     * @return Prasarana The current object (for fluent API support)
     */
    public function addVldPrasarana(VldPrasarana $l)
    {
        if ($this->collVldPrasaranas === null) {
            $this->initVldPrasaranas();
            $this->collVldPrasaranasPartial = true;
        }
        if (!in_array($l, $this->collVldPrasaranas->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddVldPrasarana($l);
        }

        return $this;
    }

    /**
     * @param	VldPrasarana $vldPrasarana The vldPrasarana object to add.
     */
    protected function doAddVldPrasarana($vldPrasarana)
    {
        $this->collVldPrasaranas[]= $vldPrasarana;
        $vldPrasarana->setPrasarana($this);
    }

    /**
     * @param	VldPrasarana $vldPrasarana The vldPrasarana object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeVldPrasarana($vldPrasarana)
    {
        if ($this->getVldPrasaranas()->contains($vldPrasarana)) {
            $this->collVldPrasaranas->remove($this->collVldPrasaranas->search($vldPrasarana));
            if (null === $this->vldPrasaranasScheduledForDeletion) {
                $this->vldPrasaranasScheduledForDeletion = clone $this->collVldPrasaranas;
                $this->vldPrasaranasScheduledForDeletion->clear();
            }
            $this->vldPrasaranasScheduledForDeletion[]= clone $vldPrasarana;
            $vldPrasarana->setPrasarana(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related VldPrasaranas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|VldPrasarana[] List of VldPrasarana objects
     */
    public function getVldPrasaranasJoinErrortype($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = VldPrasaranaQuery::create(null, $criteria);
        $query->joinWith('Errortype', $join_behavior);

        return $this->getVldPrasaranas($query, $con);
    }

    /**
     * Clears out the collPrasaranasRelatedByPrasaranaId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addPrasaranasRelatedByPrasaranaId()
     */
    public function clearPrasaranasRelatedByPrasaranaId()
    {
        $this->collPrasaranasRelatedByPrasaranaId = null; // important to set this to null since that means it is uninitialized
        $this->collPrasaranasRelatedByPrasaranaIdPartial = null;

        return $this;
    }

    /**
     * reset is the collPrasaranasRelatedByPrasaranaId collection loaded partially
     *
     * @return void
     */
    public function resetPartialPrasaranasRelatedByPrasaranaId($v = true)
    {
        $this->collPrasaranasRelatedByPrasaranaIdPartial = $v;
    }

    /**
     * Initializes the collPrasaranasRelatedByPrasaranaId collection.
     *
     * By default this just sets the collPrasaranasRelatedByPrasaranaId collection to an empty array (like clearcollPrasaranasRelatedByPrasaranaId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrasaranasRelatedByPrasaranaId($overrideExisting = true)
    {
        if (null !== $this->collPrasaranasRelatedByPrasaranaId && !$overrideExisting) {
            return;
        }
        $this->collPrasaranasRelatedByPrasaranaId = new PropelObjectCollection();
        $this->collPrasaranasRelatedByPrasaranaId->setModel('Prasarana');
    }

    /**
     * Gets an array of Prasarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     * @throws PropelException
     */
    public function getPrasaranasRelatedByPrasaranaId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranasRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collPrasaranasRelatedByPrasaranaId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrasaranasRelatedByPrasaranaId) {
                // return empty collection
                $this->initPrasaranasRelatedByPrasaranaId();
            } else {
                $collPrasaranasRelatedByPrasaranaId = PrasaranaQuery::create(null, $criteria)
                    ->filterByPrasaranaRelatedByIdIndukPrasarana($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPrasaranasRelatedByPrasaranaIdPartial && count($collPrasaranasRelatedByPrasaranaId)) {
                      $this->initPrasaranasRelatedByPrasaranaId(false);

                      foreach($collPrasaranasRelatedByPrasaranaId as $obj) {
                        if (false == $this->collPrasaranasRelatedByPrasaranaId->contains($obj)) {
                          $this->collPrasaranasRelatedByPrasaranaId->append($obj);
                        }
                      }

                      $this->collPrasaranasRelatedByPrasaranaIdPartial = true;
                    }

                    $collPrasaranasRelatedByPrasaranaId->getInternalIterator()->rewind();
                    return $collPrasaranasRelatedByPrasaranaId;
                }

                if($partial && $this->collPrasaranasRelatedByPrasaranaId) {
                    foreach($this->collPrasaranasRelatedByPrasaranaId as $obj) {
                        if($obj->isNew()) {
                            $collPrasaranasRelatedByPrasaranaId[] = $obj;
                        }
                    }
                }

                $this->collPrasaranasRelatedByPrasaranaId = $collPrasaranasRelatedByPrasaranaId;
                $this->collPrasaranasRelatedByPrasaranaIdPartial = false;
            }
        }

        return $this->collPrasaranasRelatedByPrasaranaId;
    }

    /**
     * Sets a collection of PrasaranaRelatedByPrasaranaId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $prasaranasRelatedByPrasaranaId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPrasaranasRelatedByPrasaranaId(PropelCollection $prasaranasRelatedByPrasaranaId, PropelPDO $con = null)
    {
        $prasaranasRelatedByPrasaranaIdToDelete = $this->getPrasaranasRelatedByPrasaranaId(new Criteria(), $con)->diff($prasaranasRelatedByPrasaranaId);

        $this->prasaranasRelatedByPrasaranaIdScheduledForDeletion = unserialize(serialize($prasaranasRelatedByPrasaranaIdToDelete));

        foreach ($prasaranasRelatedByPrasaranaIdToDelete as $prasaranaRelatedByPrasaranaIdRemoved) {
            $prasaranaRelatedByPrasaranaIdRemoved->setPrasaranaRelatedByIdIndukPrasarana(null);
        }

        $this->collPrasaranasRelatedByPrasaranaId = null;
        foreach ($prasaranasRelatedByPrasaranaId as $prasaranaRelatedByPrasaranaId) {
            $this->addPrasaranaRelatedByPrasaranaId($prasaranaRelatedByPrasaranaId);
        }

        $this->collPrasaranasRelatedByPrasaranaId = $prasaranasRelatedByPrasaranaId;
        $this->collPrasaranasRelatedByPrasaranaIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prasarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Prasarana objects.
     * @throws PropelException
     */
    public function countPrasaranasRelatedByPrasaranaId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranasRelatedByPrasaranaIdPartial && !$this->isNew();
        if (null === $this->collPrasaranasRelatedByPrasaranaId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrasaranasRelatedByPrasaranaId) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPrasaranasRelatedByPrasaranaId());
            }
            $query = PrasaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasaranaRelatedByIdIndukPrasarana($this)
                ->count($con);
        }

        return count($this->collPrasaranasRelatedByPrasaranaId);
    }

    /**
     * Method called to associate a Prasarana object to this object
     * through the Prasarana foreign key attribute.
     *
     * @param    Prasarana $l Prasarana
     * @return Prasarana The current object (for fluent API support)
     */
    public function addPrasaranaRelatedByPrasaranaId(Prasarana $l)
    {
        if ($this->collPrasaranasRelatedByPrasaranaId === null) {
            $this->initPrasaranasRelatedByPrasaranaId();
            $this->collPrasaranasRelatedByPrasaranaIdPartial = true;
        }
        if (!in_array($l, $this->collPrasaranasRelatedByPrasaranaId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPrasaranaRelatedByPrasaranaId($l);
        }

        return $this;
    }

    /**
     * @param	PrasaranaRelatedByPrasaranaId $prasaranaRelatedByPrasaranaId The prasaranaRelatedByPrasaranaId object to add.
     */
    protected function doAddPrasaranaRelatedByPrasaranaId($prasaranaRelatedByPrasaranaId)
    {
        $this->collPrasaranasRelatedByPrasaranaId[]= $prasaranaRelatedByPrasaranaId;
        $prasaranaRelatedByPrasaranaId->setPrasaranaRelatedByIdIndukPrasarana($this);
    }

    /**
     * @param	PrasaranaRelatedByPrasaranaId $prasaranaRelatedByPrasaranaId The prasaranaRelatedByPrasaranaId object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removePrasaranaRelatedByPrasaranaId($prasaranaRelatedByPrasaranaId)
    {
        if ($this->getPrasaranasRelatedByPrasaranaId()->contains($prasaranaRelatedByPrasaranaId)) {
            $this->collPrasaranasRelatedByPrasaranaId->remove($this->collPrasaranasRelatedByPrasaranaId->search($prasaranaRelatedByPrasaranaId));
            if (null === $this->prasaranasRelatedByPrasaranaIdScheduledForDeletion) {
                $this->prasaranasRelatedByPrasaranaIdScheduledForDeletion = clone $this->collPrasaranasRelatedByPrasaranaId;
                $this->prasaranasRelatedByPrasaranaIdScheduledForDeletion->clear();
            }
            $this->prasaranasRelatedByPrasaranaIdScheduledForDeletion[]= $prasaranaRelatedByPrasaranaId;
            $prasaranaRelatedByPrasaranaId->setPrasaranaRelatedByIdIndukPrasarana(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related PrasaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     */
    public function getPrasaranasRelatedByPrasaranaIdJoinSekolah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaQuery::create(null, $criteria);
        $query->joinWith('Sekolah', $join_behavior);

        return $this->getPrasaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related PrasaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     */
    public function getPrasaranasRelatedByPrasaranaIdJoinJenisHapusBuku($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaQuery::create(null, $criteria);
        $query->joinWith('JenisHapusBuku', $join_behavior);

        return $this->getPrasaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related PrasaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     */
    public function getPrasaranasRelatedByPrasaranaIdJoinJenisPrasarana($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaQuery::create(null, $criteria);
        $query->joinWith('JenisPrasarana', $join_behavior);

        return $this->getPrasaranasRelatedByPrasaranaId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related PrasaranasRelatedByPrasaranaId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Prasarana[] List of Prasarana objects
     */
    public function getPrasaranasRelatedByPrasaranaIdJoinStatusKepemilikanSarpras($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaQuery::create(null, $criteria);
        $query->joinWith('StatusKepemilikanSarpras', $join_behavior);

        return $this->getPrasaranasRelatedByPrasaranaId($query, $con);
    }

    /**
     * Clears out the collPrasaranaLongitudinals collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addPrasaranaLongitudinals()
     */
    public function clearPrasaranaLongitudinals()
    {
        $this->collPrasaranaLongitudinals = null; // important to set this to null since that means it is uninitialized
        $this->collPrasaranaLongitudinalsPartial = null;

        return $this;
    }

    /**
     * reset is the collPrasaranaLongitudinals collection loaded partially
     *
     * @return void
     */
    public function resetPartialPrasaranaLongitudinals($v = true)
    {
        $this->collPrasaranaLongitudinalsPartial = $v;
    }

    /**
     * Initializes the collPrasaranaLongitudinals collection.
     *
     * By default this just sets the collPrasaranaLongitudinals collection to an empty array (like clearcollPrasaranaLongitudinals());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrasaranaLongitudinals($overrideExisting = true)
    {
        if (null !== $this->collPrasaranaLongitudinals && !$overrideExisting) {
            return;
        }
        $this->collPrasaranaLongitudinals = new PropelObjectCollection();
        $this->collPrasaranaLongitudinals->setModel('PrasaranaLongitudinal');
    }

    /**
     * Gets an array of PrasaranaLongitudinal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     * @throws PropelException
     */
    public function getPrasaranaLongitudinals($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranaLongitudinalsPartial && !$this->isNew();
        if (null === $this->collPrasaranaLongitudinals || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrasaranaLongitudinals) {
                // return empty collection
                $this->initPrasaranaLongitudinals();
            } else {
                $collPrasaranaLongitudinals = PrasaranaLongitudinalQuery::create(null, $criteria)
                    ->filterByPrasarana($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collPrasaranaLongitudinalsPartial && count($collPrasaranaLongitudinals)) {
                      $this->initPrasaranaLongitudinals(false);

                      foreach($collPrasaranaLongitudinals as $obj) {
                        if (false == $this->collPrasaranaLongitudinals->contains($obj)) {
                          $this->collPrasaranaLongitudinals->append($obj);
                        }
                      }

                      $this->collPrasaranaLongitudinalsPartial = true;
                    }

                    $collPrasaranaLongitudinals->getInternalIterator()->rewind();
                    return $collPrasaranaLongitudinals;
                }

                if($partial && $this->collPrasaranaLongitudinals) {
                    foreach($this->collPrasaranaLongitudinals as $obj) {
                        if($obj->isNew()) {
                            $collPrasaranaLongitudinals[] = $obj;
                        }
                    }
                }

                $this->collPrasaranaLongitudinals = $collPrasaranaLongitudinals;
                $this->collPrasaranaLongitudinalsPartial = false;
            }
        }

        return $this->collPrasaranaLongitudinals;
    }

    /**
     * Sets a collection of PrasaranaLongitudinal objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $prasaranaLongitudinals A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setPrasaranaLongitudinals(PropelCollection $prasaranaLongitudinals, PropelPDO $con = null)
    {
        $prasaranaLongitudinalsToDelete = $this->getPrasaranaLongitudinals(new Criteria(), $con)->diff($prasaranaLongitudinals);

        $this->prasaranaLongitudinalsScheduledForDeletion = unserialize(serialize($prasaranaLongitudinalsToDelete));

        foreach ($prasaranaLongitudinalsToDelete as $prasaranaLongitudinalRemoved) {
            $prasaranaLongitudinalRemoved->setPrasarana(null);
        }

        $this->collPrasaranaLongitudinals = null;
        foreach ($prasaranaLongitudinals as $prasaranaLongitudinal) {
            $this->addPrasaranaLongitudinal($prasaranaLongitudinal);
        }

        $this->collPrasaranaLongitudinals = $prasaranaLongitudinals;
        $this->collPrasaranaLongitudinalsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PrasaranaLongitudinal objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related PrasaranaLongitudinal objects.
     * @throws PropelException
     */
    public function countPrasaranaLongitudinals(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collPrasaranaLongitudinalsPartial && !$this->isNew();
        if (null === $this->collPrasaranaLongitudinals || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrasaranaLongitudinals) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getPrasaranaLongitudinals());
            }
            $query = PrasaranaLongitudinalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasarana($this)
                ->count($con);
        }

        return count($this->collPrasaranaLongitudinals);
    }

    /**
     * Method called to associate a PrasaranaLongitudinal object to this object
     * through the PrasaranaLongitudinal foreign key attribute.
     *
     * @param    PrasaranaLongitudinal $l PrasaranaLongitudinal
     * @return Prasarana The current object (for fluent API support)
     */
    public function addPrasaranaLongitudinal(PrasaranaLongitudinal $l)
    {
        if ($this->collPrasaranaLongitudinals === null) {
            $this->initPrasaranaLongitudinals();
            $this->collPrasaranaLongitudinalsPartial = true;
        }
        if (!in_array($l, $this->collPrasaranaLongitudinals->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPrasaranaLongitudinal($l);
        }

        return $this;
    }

    /**
     * @param	PrasaranaLongitudinal $prasaranaLongitudinal The prasaranaLongitudinal object to add.
     */
    protected function doAddPrasaranaLongitudinal($prasaranaLongitudinal)
    {
        $this->collPrasaranaLongitudinals[]= $prasaranaLongitudinal;
        $prasaranaLongitudinal->setPrasarana($this);
    }

    /**
     * @param	PrasaranaLongitudinal $prasaranaLongitudinal The prasaranaLongitudinal object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removePrasaranaLongitudinal($prasaranaLongitudinal)
    {
        if ($this->getPrasaranaLongitudinals()->contains($prasaranaLongitudinal)) {
            $this->collPrasaranaLongitudinals->remove($this->collPrasaranaLongitudinals->search($prasaranaLongitudinal));
            if (null === $this->prasaranaLongitudinalsScheduledForDeletion) {
                $this->prasaranaLongitudinalsScheduledForDeletion = clone $this->collPrasaranaLongitudinals;
                $this->prasaranaLongitudinalsScheduledForDeletion->clear();
            }
            $this->prasaranaLongitudinalsScheduledForDeletion[]= clone $prasaranaLongitudinal;
            $prasaranaLongitudinal->setPrasarana(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related PrasaranaLongitudinals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|PrasaranaLongitudinal[] List of PrasaranaLongitudinal objects
     */
    public function getPrasaranaLongitudinalsJoinSemester($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = PrasaranaLongitudinalQuery::create(null, $criteria);
        $query->joinWith('Semester', $join_behavior);

        return $this->getPrasaranaLongitudinals($query, $con);
    }

    /**
     * Clears out the collRombonganBelajars collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addRombonganBelajars()
     */
    public function clearRombonganBelajars()
    {
        $this->collRombonganBelajars = null; // important to set this to null since that means it is uninitialized
        $this->collRombonganBelajarsPartial = null;

        return $this;
    }

    /**
     * reset is the collRombonganBelajars collection loaded partially
     *
     * @return void
     */
    public function resetPartialRombonganBelajars($v = true)
    {
        $this->collRombonganBelajarsPartial = $v;
    }

    /**
     * Initializes the collRombonganBelajars collection.
     *
     * By default this just sets the collRombonganBelajars collection to an empty array (like clearcollRombonganBelajars());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRombonganBelajars($overrideExisting = true)
    {
        if (null !== $this->collRombonganBelajars && !$overrideExisting) {
            return;
        }
        $this->collRombonganBelajars = new PropelObjectCollection();
        $this->collRombonganBelajars->setModel('RombonganBelajar');
    }

    /**
     * Gets an array of RombonganBelajar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     * @throws PropelException
     */
    public function getRombonganBelajars($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsPartial && !$this->isNew();
        if (null === $this->collRombonganBelajars || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajars) {
                // return empty collection
                $this->initRombonganBelajars();
            } else {
                $collRombonganBelajars = RombonganBelajarQuery::create(null, $criteria)
                    ->filterByPrasarana($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRombonganBelajarsPartial && count($collRombonganBelajars)) {
                      $this->initRombonganBelajars(false);

                      foreach($collRombonganBelajars as $obj) {
                        if (false == $this->collRombonganBelajars->contains($obj)) {
                          $this->collRombonganBelajars->append($obj);
                        }
                      }

                      $this->collRombonganBelajarsPartial = true;
                    }

                    $collRombonganBelajars->getInternalIterator()->rewind();
                    return $collRombonganBelajars;
                }

                if($partial && $this->collRombonganBelajars) {
                    foreach($this->collRombonganBelajars as $obj) {
                        if($obj->isNew()) {
                            $collRombonganBelajars[] = $obj;
                        }
                    }
                }

                $this->collRombonganBelajars = $collRombonganBelajars;
                $this->collRombonganBelajarsPartial = false;
            }
        }

        return $this->collRombonganBelajars;
    }

    /**
     * Sets a collection of RombonganBelajar objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rombonganBelajars A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setRombonganBelajars(PropelCollection $rombonganBelajars, PropelPDO $con = null)
    {
        $rombonganBelajarsToDelete = $this->getRombonganBelajars(new Criteria(), $con)->diff($rombonganBelajars);

        $this->rombonganBelajarsScheduledForDeletion = unserialize(serialize($rombonganBelajarsToDelete));

        foreach ($rombonganBelajarsToDelete as $rombonganBelajarRemoved) {
            $rombonganBelajarRemoved->setPrasarana(null);
        }

        $this->collRombonganBelajars = null;
        foreach ($rombonganBelajars as $rombonganBelajar) {
            $this->addRombonganBelajar($rombonganBelajar);
        }

        $this->collRombonganBelajars = $rombonganBelajars;
        $this->collRombonganBelajarsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RombonganBelajar objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RombonganBelajar objects.
     * @throws PropelException
     */
    public function countRombonganBelajars(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRombonganBelajarsPartial && !$this->isNew();
        if (null === $this->collRombonganBelajars || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRombonganBelajars) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getRombonganBelajars());
            }
            $query = RombonganBelajarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasarana($this)
                ->count($con);
        }

        return count($this->collRombonganBelajars);
    }

    /**
     * Method called to associate a RombonganBelajar object to this object
     * through the RombonganBelajar foreign key attribute.
     *
     * @param    RombonganBelajar $l RombonganBelajar
     * @return Prasarana The current object (for fluent API support)
     */
    public function addRombonganBelajar(RombonganBelajar $l)
    {
        if ($this->collRombonganBelajars === null) {
            $this->initRombonganBelajars();
            $this->collRombonganBelajarsPartial = true;
        }
        if (!in_array($l, $this->collRombonganBelajars->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRombonganBelajar($l);
        }

        return $this;
    }

    /**
     * @param	RombonganBelajar $rombonganBelajar The rombonganBelajar object to add.
     */
    protected function doAddRombonganBelajar($rombonganBelajar)
    {
        $this->collRombonganBelajars[]= $rombonganBelajar;
        $rombonganBelajar->setPrasarana($this);
    }

    /**
     * @param	RombonganBelajar $rombonganBelajar The rombonganBelajar object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeRombonganBelajar($rombonganBelajar)
    {
        if ($this->getRombonganBelajars()->contains($rombonganBelajar)) {
            $this->collRombonganBelajars->remove($this->collRombonganBelajars->search($rombonganBelajar));
            if (null === $this->rombonganBelajarsScheduledForDeletion) {
                $this->rombonganBelajarsScheduledForDeletion = clone $this->collRombonganBelajars;
                $this->rombonganBelajarsScheduledForDeletion->clear();
            }
            $this->rombonganBelajarsScheduledForDeletion[]= clone $rombonganBelajar;
            $rombonganBelajar->setPrasarana(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajars from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsJoinJurusanSp($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('JurusanSp', $join_behavior);

        return $this->getRombonganBelajars($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajars from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsJoinPtk($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('Ptk', $join_behavior);

        return $this->getRombonganBelajars($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajars from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsJoinSekolah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('Sekolah', $join_behavior);

        return $this->getRombonganBelajars($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajars from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsJoinKebutuhanKhusus($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('KebutuhanKhusus', $join_behavior);

        return $this->getRombonganBelajars($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajars from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsJoinKurikulum($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('Kurikulum', $join_behavior);

        return $this->getRombonganBelajars($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajars from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsJoinSemester($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('Semester', $join_behavior);

        return $this->getRombonganBelajars($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related RombonganBelajars from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RombonganBelajar[] List of RombonganBelajar objects
     */
    public function getRombonganBelajarsJoinTingkatPendidikan($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RombonganBelajarQuery::create(null, $criteria);
        $query->joinWith('TingkatPendidikan', $join_behavior);

        return $this->getRombonganBelajars($query, $con);
    }

    /**
     * Clears out the collSaranas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Prasarana The current object (for fluent API support)
     * @see        addSaranas()
     */
    public function clearSaranas()
    {
        $this->collSaranas = null; // important to set this to null since that means it is uninitialized
        $this->collSaranasPartial = null;

        return $this;
    }

    /**
     * reset is the collSaranas collection loaded partially
     *
     * @return void
     */
    public function resetPartialSaranas($v = true)
    {
        $this->collSaranasPartial = $v;
    }

    /**
     * Initializes the collSaranas collection.
     *
     * By default this just sets the collSaranas collection to an empty array (like clearcollSaranas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaranas($overrideExisting = true)
    {
        if (null !== $this->collSaranas && !$overrideExisting) {
            return;
        }
        $this->collSaranas = new PropelObjectCollection();
        $this->collSaranas->setModel('Sarana');
    }

    /**
     * Gets an array of Sarana objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Prasarana is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     * @throws PropelException
     */
    public function getSaranas($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSaranasPartial && !$this->isNew();
        if (null === $this->collSaranas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSaranas) {
                // return empty collection
                $this->initSaranas();
            } else {
                $collSaranas = SaranaQuery::create(null, $criteria)
                    ->filterByPrasarana($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSaranasPartial && count($collSaranas)) {
                      $this->initSaranas(false);

                      foreach($collSaranas as $obj) {
                        if (false == $this->collSaranas->contains($obj)) {
                          $this->collSaranas->append($obj);
                        }
                      }

                      $this->collSaranasPartial = true;
                    }

                    $collSaranas->getInternalIterator()->rewind();
                    return $collSaranas;
                }

                if($partial && $this->collSaranas) {
                    foreach($this->collSaranas as $obj) {
                        if($obj->isNew()) {
                            $collSaranas[] = $obj;
                        }
                    }
                }

                $this->collSaranas = $collSaranas;
                $this->collSaranasPartial = false;
            }
        }

        return $this->collSaranas;
    }

    /**
     * Sets a collection of Sarana objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $saranas A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Prasarana The current object (for fluent API support)
     */
    public function setSaranas(PropelCollection $saranas, PropelPDO $con = null)
    {
        $saranasToDelete = $this->getSaranas(new Criteria(), $con)->diff($saranas);

        $this->saranasScheduledForDeletion = unserialize(serialize($saranasToDelete));

        foreach ($saranasToDelete as $saranaRemoved) {
            $saranaRemoved->setPrasarana(null);
        }

        $this->collSaranas = null;
        foreach ($saranas as $sarana) {
            $this->addSarana($sarana);
        }

        $this->collSaranas = $saranas;
        $this->collSaranasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Sarana objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Sarana objects.
     * @throws PropelException
     */
    public function countSaranas(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSaranasPartial && !$this->isNew();
        if (null === $this->collSaranas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaranas) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSaranas());
            }
            $query = SaranaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrasarana($this)
                ->count($con);
        }

        return count($this->collSaranas);
    }

    /**
     * Method called to associate a Sarana object to this object
     * through the Sarana foreign key attribute.
     *
     * @param    Sarana $l Sarana
     * @return Prasarana The current object (for fluent API support)
     */
    public function addSarana(Sarana $l)
    {
        if ($this->collSaranas === null) {
            $this->initSaranas();
            $this->collSaranasPartial = true;
        }
        if (!in_array($l, $this->collSaranas->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSarana($l);
        }

        return $this;
    }

    /**
     * @param	Sarana $sarana The sarana object to add.
     */
    protected function doAddSarana($sarana)
    {
        $this->collSaranas[]= $sarana;
        $sarana->setPrasarana($this);
    }

    /**
     * @param	Sarana $sarana The sarana object to remove.
     * @return Prasarana The current object (for fluent API support)
     */
    public function removeSarana($sarana)
    {
        if ($this->getSaranas()->contains($sarana)) {
            $this->collSaranas->remove($this->collSaranas->search($sarana));
            if (null === $this->saranasScheduledForDeletion) {
                $this->saranasScheduledForDeletion = clone $this->collSaranas;
                $this->saranasScheduledForDeletion->clear();
            }
            $this->saranasScheduledForDeletion[]= $sarana;
            $sarana->setPrasarana(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Saranas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasJoinSekolah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('Sekolah', $join_behavior);

        return $this->getSaranas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Saranas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasJoinJenisHapusBuku($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('JenisHapusBuku', $join_behavior);

        return $this->getSaranas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Saranas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasJoinJenisSarana($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('JenisSarana', $join_behavior);

        return $this->getSaranas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Prasarana is new, it will return
     * an empty collection; or if this Prasarana has previously
     * been saved, it will retrieve related Saranas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Prasarana.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Sarana[] List of Sarana objects
     */
    public function getSaranasJoinStatusKepemilikanSarpras($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SaranaQuery::create(null, $criteria);
        $query->joinWith('StatusKepemilikanSarpras', $join_behavior);

        return $this->getSaranas($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->prasarana_id = null;
        $this->jenis_prasarana_id = null;
        $this->sekolah_id = null;
        $this->id_induk_prasarana = null;
        $this->kepemilikan_sarpras_id = null;
        $this->nama = null;
        $this->panjang = null;
        $this->lebar = null;
        $this->reg_pras = null;
        $this->ket_prasarana = null;
        $this->vol_pondasi_m3 = null;
        $this->vol_sloop_kolom_balok_m3 = null;
        $this->luas_plester_m2 = null;
        $this->panj_kudakuda_m = null;
        $this->vol_kudakuda_m3 = null;
        $this->panj_kaso_m = null;
        $this->panj_reng_m = null;
        $this->luas_tutup_atap_m2 = null;
        $this->luas_plafon_m2 = null;
        $this->luas_dinding_m2 = null;
        $this->luas_daun_jendela_m2 = null;
        $this->luas_daun_pintu_m2 = null;
        $this->panj_kusen_m = null;
        $this->luas_tutup_lantai_m2 = null;
        $this->panj_inst_listrik_m = null;
        $this->jml_inst_listrik = null;
        $this->panj_inst_air_m = null;
        $this->jml_inst_air = null;
        $this->panj_drainase_m = null;
        $this->luas_finish_struktur_m2 = null;
        $this->luas_finish_plafon_m2 = null;
        $this->luas_finish_dinding_m2 = null;
        $this->luas_finish_kpj_m2 = null;
        $this->id_hapus_buku = null;
        $this->tgl_hapus_buku = null;
        $this->asal_data = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collBukuAlats) {
                foreach ($this->collBukuAlats as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collJadwals) {
                foreach ($this->collJadwals as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVldPrasaranas) {
                foreach ($this->collVldPrasaranas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrasaranasRelatedByPrasaranaId) {
                foreach ($this->collPrasaranasRelatedByPrasaranaId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrasaranaLongitudinals) {
                foreach ($this->collPrasaranaLongitudinals as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRombonganBelajars) {
                foreach ($this->collRombonganBelajars as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaranas) {
                foreach ($this->collSaranas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aPrasaranaRelatedByIdIndukPrasarana instanceof Persistent) {
              $this->aPrasaranaRelatedByIdIndukPrasarana->clearAllReferences($deep);
            }
            if ($this->aSekolah instanceof Persistent) {
              $this->aSekolah->clearAllReferences($deep);
            }
            if ($this->aJenisHapusBuku instanceof Persistent) {
              $this->aJenisHapusBuku->clearAllReferences($deep);
            }
            if ($this->aJenisPrasarana instanceof Persistent) {
              $this->aJenisPrasarana->clearAllReferences($deep);
            }
            if ($this->aStatusKepemilikanSarpras instanceof Persistent) {
              $this->aStatusKepemilikanSarpras->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collBukuAlats instanceof PropelCollection) {
            $this->collBukuAlats->clearIterator();
        }
        $this->collBukuAlats = null;
        if ($this->collJadwals instanceof PropelCollection) {
            $this->collJadwals->clearIterator();
        }
        $this->collJadwals = null;
        if ($this->collVldPrasaranas instanceof PropelCollection) {
            $this->collVldPrasaranas->clearIterator();
        }
        $this->collVldPrasaranas = null;
        if ($this->collPrasaranasRelatedByPrasaranaId instanceof PropelCollection) {
            $this->collPrasaranasRelatedByPrasaranaId->clearIterator();
        }
        $this->collPrasaranasRelatedByPrasaranaId = null;
        if ($this->collPrasaranaLongitudinals instanceof PropelCollection) {
            $this->collPrasaranaLongitudinals->clearIterator();
        }
        $this->collPrasaranaLongitudinals = null;
        if ($this->collRombonganBelajars instanceof PropelCollection) {
            $this->collRombonganBelajars->clearIterator();
        }
        $this->collRombonganBelajars = null;
        if ($this->collSaranas instanceof PropelCollection) {
            $this->collSaranas->clearIterator();
        }
        $this->collSaranas = null;
        $this->aPrasaranaRelatedByIdIndukPrasarana = null;
        $this->aSekolah = null;
        $this->aJenisHapusBuku = null;
        $this->aJenisPrasarana = null;
        $this->aStatusKepemilikanSarpras = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PrasaranaPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
