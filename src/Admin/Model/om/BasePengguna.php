<?php

namespace Admin\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use Admin\Model\Dudi;
use Admin\Model\DudiQuery;
use Admin\Model\LembagaAkreditasi;
use Admin\Model\LembagaAkreditasiQuery;
use Admin\Model\LembagaNonSekolah;
use Admin\Model\LembagaNonSekolahQuery;
use Admin\Model\LogPengguna;
use Admin\Model\LogPenggunaQuery;
use Admin\Model\MstWilayah;
use Admin\Model\MstWilayahQuery;
use Admin\Model\Pengguna;
use Admin\Model\PenggunaPeer;
use Admin\Model\PenggunaQuery;
use Admin\Model\Peran;
use Admin\Model\PeranQuery;
use Admin\Model\SasaranSurvey;
use Admin\Model\SasaranSurveyQuery;
use Admin\Model\Yayasan;
use Admin\Model\YayasanQuery;

/**
 * Base class that represents a row from the 'pengguna' table.
 *
 *
 *
 * @package    propel.generator.Admin.Model.om
 */
abstract class BasePengguna extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Admin\\Model\\PenggunaPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PenggunaPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the pengguna_id field.
     * @var        string
     */
    protected $pengguna_id;

    /**
     * The value for the username field.
     * @var        string
     */
    protected $username;

    /**
     * The value for the password field.
     * @var        string
     */
    protected $password;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the nip_nim field.
     * @var        string
     */
    protected $nip_nim;

    /**
     * The value for the jabatan_lembaga field.
     * @var        string
     */
    protected $jabatan_lembaga;

    /**
     * The value for the ym field.
     * @var        string
     */
    protected $ym;

    /**
     * The value for the skype field.
     * @var        string
     */
    protected $skype;

    /**
     * The value for the alamat field.
     * @var        string
     */
    protected $alamat;

    /**
     * The value for the kode_wilayah field.
     * @var        string
     */
    protected $kode_wilayah;

    /**
     * The value for the no_telepon field.
     * @var        string
     */
    protected $no_telepon;

    /**
     * The value for the no_hp field.
     * @var        string
     */
    protected $no_hp;

    /**
     * The value for the aktif field.
     * @var        string
     */
    protected $aktif;

    /**
     * The value for the ptk_id field.
     * @var        string
     */
    protected $ptk_id;

    /**
     * The value for the peran_id field.
     * @var        int
     */
    protected $peran_id;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the lembaga_id field.
     * @var        string
     */
    protected $lembaga_id;

    /**
     * The value for the yayasan_id field.
     * @var        string
     */
    protected $yayasan_id;

    /**
     * The value for the la_id field.
     * @var        string
     */
    protected $la_id;

    /**
     * The value for the dudi_id field.
     * @var        string
     */
    protected $dudi_id;

    /**
     * The value for the last_update field.
     * @var        string
     */
    protected $last_update;

    /**
     * The value for the soft_delete field.
     * @var        string
     */
    protected $soft_delete;

    /**
     * The value for the last_sync field.
     * @var        string
     */
    protected $last_sync;

    /**
     * The value for the updater_id field.
     * @var        string
     */
    protected $updater_id;

    /**
     * @var        Dudi
     */
    protected $aDudi;

    /**
     * @var        LembagaNonSekolah
     */
    protected $aLembagaNonSekolah;

    /**
     * @var        Yayasan
     */
    protected $aYayasan;

    /**
     * @var        LembagaAkreditasi
     */
    protected $aLembagaAkreditasi;

    /**
     * @var        MstWilayah
     */
    protected $aMstWilayah;

    /**
     * @var        Peran
     */
    protected $aPeran;

    /**
     * @var        PropelObjectCollection|LogPengguna[] Collection to store aggregation of LogPengguna objects.
     */
    protected $collLogPenggunas;
    protected $collLogPenggunasPartial;

    /**
     * @var        PropelObjectCollection|SasaranSurvey[] Collection to store aggregation of SasaranSurvey objects.
     */
    protected $collSasaranSurveys;
    protected $collSasaranSurveysPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $logPenggunasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $sasaranSurveysScheduledForDeletion = null;

    /**
     * Get the [pengguna_id] column value.
     *
     * @return string
     */
    public function getPenggunaId()
    {
        return $this->pengguna_id;
    }

    /**
     * Get the [username] column value.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [nama] column value.
     *
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [nip_nim] column value.
     *
     * @return string
     */
    public function getNipNim()
    {
        return $this->nip_nim;
    }

    /**
     * Get the [jabatan_lembaga] column value.
     *
     * @return string
     */
    public function getJabatanLembaga()
    {
        return $this->jabatan_lembaga;
    }

    /**
     * Get the [ym] column value.
     *
     * @return string
     */
    public function getYm()
    {
        return $this->ym;
    }

    /**
     * Get the [skype] column value.
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Get the [alamat] column value.
     *
     * @return string
     */
    public function getAlamat()
    {
        return $this->alamat;
    }

    /**
     * Get the [kode_wilayah] column value.
     *
     * @return string
     */
    public function getKodeWilayah()
    {
        return $this->kode_wilayah;
    }

    /**
     * Get the [no_telepon] column value.
     *
     * @return string
     */
    public function getNoTelepon()
    {
        return $this->no_telepon;
    }

    /**
     * Get the [no_hp] column value.
     *
     * @return string
     */
    public function getNoHp()
    {
        return $this->no_hp;
    }

    /**
     * Get the [aktif] column value.
     *
     * @return string
     */
    public function getAktif()
    {
        return $this->aktif;
    }

    /**
     * Get the [ptk_id] column value.
     *
     * @return string
     */
    public function getPtkId()
    {
        return $this->ptk_id;
    }

    /**
     * Get the [peran_id] column value.
     *
     * @return int
     */
    public function getPeranId()
    {
        return $this->peran_id;
    }

    /**
     * Get the [sekolah_id] column value.
     *
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [lembaga_id] column value.
     *
     * @return string
     */
    public function getLembagaId()
    {
        return $this->lembaga_id;
    }

    /**
     * Get the [yayasan_id] column value.
     *
     * @return string
     */
    public function getYayasanId()
    {
        return $this->yayasan_id;
    }

    /**
     * Get the [la_id] column value.
     *
     * @return string
     */
    public function getLaId()
    {
        return $this->la_id;
    }

    /**
     * Get the [dudi_id] column value.
     *
     * @return string
     */
    public function getDudiId()
    {
        return $this->dudi_id;
    }

    /**
     * Get the [optionally formatted] temporal [last_update] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastUpdate($format = 'Y-m-d H:i:s')
    {
        if ($this->last_update === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_update);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_update, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [soft_delete] column value.
     *
     * @return string
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [optionally formatted] temporal [last_sync] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastSync($format = 'Y-m-d H:i:s')
    {
        if ($this->last_sync === null) {
            return null;
        }


        try {
            $dt = new DateTime($this->last_sync);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->last_sync, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [updater_id] column value.
     *
     * @return string
     */
    public function getUpdaterId()
    {
        return $this->updater_id;
    }

    /**
     * Set the value of [pengguna_id] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setPenggunaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pengguna_id !== $v) {
            $this->pengguna_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::PENGGUNA_ID;
        }


        return $this;
    } // setPenggunaId()

    /**
     * Set the value of [username] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[] = PenggunaPeer::USERNAME;
        }


        return $this;
    } // setUsername()

    /**
     * Set the value of [password] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[] = PenggunaPeer::PASSWORD;
        }


        return $this;
    } // setPassword()

    /**
     * Set the value of [nama] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = PenggunaPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [nip_nim] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setNipNim($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nip_nim !== $v) {
            $this->nip_nim = $v;
            $this->modifiedColumns[] = PenggunaPeer::NIP_NIM;
        }


        return $this;
    } // setNipNim()

    /**
     * Set the value of [jabatan_lembaga] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setJabatanLembaga($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->jabatan_lembaga !== $v) {
            $this->jabatan_lembaga = $v;
            $this->modifiedColumns[] = PenggunaPeer::JABATAN_LEMBAGA;
        }


        return $this;
    } // setJabatanLembaga()

    /**
     * Set the value of [ym] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setYm($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ym !== $v) {
            $this->ym = $v;
            $this->modifiedColumns[] = PenggunaPeer::YM;
        }


        return $this;
    } // setYm()

    /**
     * Set the value of [skype] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setSkype($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->skype !== $v) {
            $this->skype = $v;
            $this->modifiedColumns[] = PenggunaPeer::SKYPE;
        }


        return $this;
    } // setSkype()

    /**
     * Set the value of [alamat] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setAlamat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alamat !== $v) {
            $this->alamat = $v;
            $this->modifiedColumns[] = PenggunaPeer::ALAMAT;
        }


        return $this;
    } // setAlamat()

    /**
     * Set the value of [kode_wilayah] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setKodeWilayah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah !== $v) {
            $this->kode_wilayah = $v;
            $this->modifiedColumns[] = PenggunaPeer::KODE_WILAYAH;
        }

        if ($this->aMstWilayah !== null && $this->aMstWilayah->getKodeWilayah() !== $v) {
            $this->aMstWilayah = null;
        }


        return $this;
    } // setKodeWilayah()

    /**
     * Set the value of [no_telepon] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setNoTelepon($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->no_telepon !== $v) {
            $this->no_telepon = $v;
            $this->modifiedColumns[] = PenggunaPeer::NO_TELEPON;
        }


        return $this;
    } // setNoTelepon()

    /**
     * Set the value of [no_hp] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setNoHp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->no_hp !== $v) {
            $this->no_hp = $v;
            $this->modifiedColumns[] = PenggunaPeer::NO_HP;
        }


        return $this;
    } // setNoHp()

    /**
     * Set the value of [aktif] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setAktif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->aktif !== $v) {
            $this->aktif = $v;
            $this->modifiedColumns[] = PenggunaPeer::AKTIF;
        }


        return $this;
    } // setAktif()

    /**
     * Set the value of [ptk_id] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setPtkId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ptk_id !== $v) {
            $this->ptk_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::PTK_ID;
        }


        return $this;
    } // setPtkId()

    /**
     * Set the value of [peran_id] column.
     *
     * @param int $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setPeranId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->peran_id !== $v) {
            $this->peran_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::PERAN_ID;
        }

        if ($this->aPeran !== null && $this->aPeran->getPeranId() !== $v) {
            $this->aPeran = null;
        }


        return $this;
    } // setPeranId()

    /**
     * Set the value of [sekolah_id] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::SEKOLAH_ID;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [lembaga_id] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLembagaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->lembaga_id !== $v) {
            $this->lembaga_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::LEMBAGA_ID;
        }

        if ($this->aLembagaNonSekolah !== null && $this->aLembagaNonSekolah->getLembagaId() !== $v) {
            $this->aLembagaNonSekolah = null;
        }


        return $this;
    } // setLembagaId()

    /**
     * Set the value of [yayasan_id] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setYayasanId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->yayasan_id !== $v) {
            $this->yayasan_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::YAYASAN_ID;
        }

        if ($this->aYayasan !== null && $this->aYayasan->getYayasanId() !== $v) {
            $this->aYayasan = null;
        }


        return $this;
    } // setYayasanId()

    /**
     * Set the value of [la_id] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->la_id !== $v) {
            $this->la_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::LA_ID;
        }

        if ($this->aLembagaAkreditasi !== null && $this->aLembagaAkreditasi->getLaId() !== $v) {
            $this->aLembagaAkreditasi = null;
        }


        return $this;
    } // setLaId()

    /**
     * Set the value of [dudi_id] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setDudiId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->dudi_id !== $v) {
            $this->dudi_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::DUDI_ID;
        }

        if ($this->aDudi !== null && $this->aDudi->getDudiId() !== $v) {
            $this->aDudi = null;
        }


        return $this;
    } // setDudiId()

    /**
     * Sets the value of [last_update] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLastUpdate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_update !== null || $dt !== null) {
            $currentDateAsString = ($this->last_update !== null && $tmpDt = new DateTime($this->last_update)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_update = $newDateAsString;
                $this->modifiedColumns[] = PenggunaPeer::LAST_UPDATE;
            }
        } // if either are not null


        return $this;
    } // setLastUpdate()

    /**
     * Set the value of [soft_delete] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = PenggunaPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Sets the value of [last_sync] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLastSync($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_sync !== null || $dt !== null) {
            $currentDateAsString = ($this->last_sync !== null && $tmpDt = new DateTime($this->last_sync)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->last_sync = $newDateAsString;
                $this->modifiedColumns[] = PenggunaPeer::LAST_SYNC;
            }
        } // if either are not null


        return $this;
    } // setLastSync()

    /**
     * Set the value of [updater_id] column.
     *
     * @param string $v new value
     * @return Pengguna The current object (for fluent API support)
     */
    public function setUpdaterId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->updater_id !== $v) {
            $this->updater_id = $v;
            $this->modifiedColumns[] = PenggunaPeer::UPDATER_ID;
        }


        return $this;
    } // setUpdaterId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->pengguna_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->username = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->password = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->nama = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->nip_nim = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->jabatan_lembaga = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->ym = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->skype = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->alamat = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->kode_wilayah = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->no_telepon = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->no_hp = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->aktif = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->ptk_id = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->peran_id = ($row[$startcol + 14] !== null) ? (int) $row[$startcol + 14] : null;
            $this->sekolah_id = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->lembaga_id = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->yayasan_id = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->la_id = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->dudi_id = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->last_update = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->soft_delete = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->last_sync = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->updater_id = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 24; // 24 = PenggunaPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Pengguna object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aMstWilayah !== null && $this->kode_wilayah !== $this->aMstWilayah->getKodeWilayah()) {
            $this->aMstWilayah = null;
        }
        if ($this->aPeran !== null && $this->peran_id !== $this->aPeran->getPeranId()) {
            $this->aPeran = null;
        }
        if ($this->aLembagaNonSekolah !== null && $this->lembaga_id !== $this->aLembagaNonSekolah->getLembagaId()) {
            $this->aLembagaNonSekolah = null;
        }
        if ($this->aYayasan !== null && $this->yayasan_id !== $this->aYayasan->getYayasanId()) {
            $this->aYayasan = null;
        }
        if ($this->aLembagaAkreditasi !== null && $this->la_id !== $this->aLembagaAkreditasi->getLaId()) {
            $this->aLembagaAkreditasi = null;
        }
        if ($this->aDudi !== null && $this->dudi_id !== $this->aDudi->getDudiId()) {
            $this->aDudi = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PenggunaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aDudi = null;
            $this->aLembagaNonSekolah = null;
            $this->aYayasan = null;
            $this->aLembagaAkreditasi = null;
            $this->aMstWilayah = null;
            $this->aPeran = null;
            $this->collLogPenggunas = null;

            $this->collSasaranSurveys = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PenggunaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PenggunaPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aDudi !== null) {
                if ($this->aDudi->isModified() || $this->aDudi->isNew()) {
                    $affectedRows += $this->aDudi->save($con);
                }
                $this->setDudi($this->aDudi);
            }

            if ($this->aLembagaNonSekolah !== null) {
                if ($this->aLembagaNonSekolah->isModified() || $this->aLembagaNonSekolah->isNew()) {
                    $affectedRows += $this->aLembagaNonSekolah->save($con);
                }
                $this->setLembagaNonSekolah($this->aLembagaNonSekolah);
            }

            if ($this->aYayasan !== null) {
                if ($this->aYayasan->isModified() || $this->aYayasan->isNew()) {
                    $affectedRows += $this->aYayasan->save($con);
                }
                $this->setYayasan($this->aYayasan);
            }

            if ($this->aLembagaAkreditasi !== null) {
                if ($this->aLembagaAkreditasi->isModified() || $this->aLembagaAkreditasi->isNew()) {
                    $affectedRows += $this->aLembagaAkreditasi->save($con);
                }
                $this->setLembagaAkreditasi($this->aLembagaAkreditasi);
            }

            if ($this->aMstWilayah !== null) {
                if ($this->aMstWilayah->isModified() || $this->aMstWilayah->isNew()) {
                    $affectedRows += $this->aMstWilayah->save($con);
                }
                $this->setMstWilayah($this->aMstWilayah);
            }

            if ($this->aPeran !== null) {
                if ($this->aPeran->isModified() || $this->aPeran->isNew()) {
                    $affectedRows += $this->aPeran->save($con);
                }
                $this->setPeran($this->aPeran);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->logPenggunasScheduledForDeletion !== null) {
                if (!$this->logPenggunasScheduledForDeletion->isEmpty()) {
                    LogPenggunaQuery::create()
                        ->filterByPrimaryKeys($this->logPenggunasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->logPenggunasScheduledForDeletion = null;
                }
            }

            if ($this->collLogPenggunas !== null) {
                foreach ($this->collLogPenggunas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sasaranSurveysScheduledForDeletion !== null) {
                if (!$this->sasaranSurveysScheduledForDeletion->isEmpty()) {
                    SasaranSurveyQuery::create()
                        ->filterByPrimaryKeys($this->sasaranSurveysScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sasaranSurveysScheduledForDeletion = null;
                }
            }

            if ($this->collSasaranSurveys !== null) {
                foreach ($this->collSasaranSurveys as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their coresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aDudi !== null) {
                if (!$this->aDudi->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aDudi->getValidationFailures());
                }
            }

            if ($this->aLembagaNonSekolah !== null) {
                if (!$this->aLembagaNonSekolah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aLembagaNonSekolah->getValidationFailures());
                }
            }

            if ($this->aYayasan !== null) {
                if (!$this->aYayasan->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aYayasan->getValidationFailures());
                }
            }

            if ($this->aLembagaAkreditasi !== null) {
                if (!$this->aLembagaAkreditasi->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aLembagaAkreditasi->getValidationFailures());
                }
            }

            if ($this->aMstWilayah !== null) {
                if (!$this->aMstWilayah->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aMstWilayah->getValidationFailures());
                }
            }

            if ($this->aPeran !== null) {
                if (!$this->aPeran->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPeran->getValidationFailures());
                }
            }


            if (($retval = PenggunaPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collLogPenggunas !== null) {
                    foreach ($this->collLogPenggunas as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collSasaranSurveys !== null) {
                    foreach ($this->collSasaranSurveys as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_FIELDNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_FIELDNAME)
    {
        $pos = PenggunaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getPenggunaId();
                break;
            case 1:
                return $this->getUsername();
                break;
            case 2:
                return $this->getPassword();
                break;
            case 3:
                return $this->getNama();
                break;
            case 4:
                return $this->getNipNim();
                break;
            case 5:
                return $this->getJabatanLembaga();
                break;
            case 6:
                return $this->getYm();
                break;
            case 7:
                return $this->getSkype();
                break;
            case 8:
                return $this->getAlamat();
                break;
            case 9:
                return $this->getKodeWilayah();
                break;
            case 10:
                return $this->getNoTelepon();
                break;
            case 11:
                return $this->getNoHp();
                break;
            case 12:
                return $this->getAktif();
                break;
            case 13:
                return $this->getPtkId();
                break;
            case 14:
                return $this->getPeranId();
                break;
            case 15:
                return $this->getSekolahId();
                break;
            case 16:
                return $this->getLembagaId();
                break;
            case 17:
                return $this->getYayasanId();
                break;
            case 18:
                return $this->getLaId();
                break;
            case 19:
                return $this->getDudiId();
                break;
            case 20:
                return $this->getLastUpdate();
                break;
            case 21:
                return $this->getSoftDelete();
                break;
            case 22:
                return $this->getLastSync();
                break;
            case 23:
                return $this->getUpdaterId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_FIELDNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_FIELDNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Pengguna'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Pengguna'][$this->getPrimaryKey()] = true;
        $keys = PenggunaPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getPenggunaId(),
            $keys[1] => $this->getUsername(),
            $keys[2] => $this->getPassword(),
            $keys[3] => $this->getNama(),
            $keys[4] => $this->getNipNim(),
            $keys[5] => $this->getJabatanLembaga(),
            $keys[6] => $this->getYm(),
            $keys[7] => $this->getSkype(),
            $keys[8] => $this->getAlamat(),
            $keys[9] => $this->getKodeWilayah(),
            $keys[10] => $this->getNoTelepon(),
            $keys[11] => $this->getNoHp(),
            $keys[12] => $this->getAktif(),
            $keys[13] => $this->getPtkId(),
            $keys[14] => $this->getPeranId(),
            $keys[15] => $this->getSekolahId(),
            $keys[16] => $this->getLembagaId(),
            $keys[17] => $this->getYayasanId(),
            $keys[18] => $this->getLaId(),
            $keys[19] => $this->getDudiId(),
            $keys[20] => $this->getLastUpdate(),
            $keys[21] => $this->getSoftDelete(),
            $keys[22] => $this->getLastSync(),
            $keys[23] => $this->getUpdaterId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aDudi) {
                $result['Dudi'] = $this->aDudi->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aLembagaNonSekolah) {
                $result['LembagaNonSekolah'] = $this->aLembagaNonSekolah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aYayasan) {
                $result['Yayasan'] = $this->aYayasan->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aLembagaAkreditasi) {
                $result['LembagaAkreditasi'] = $this->aLembagaAkreditasi->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMstWilayah) {
                $result['MstWilayah'] = $this->aMstWilayah->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPeran) {
                $result['Peran'] = $this->aPeran->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collLogPenggunas) {
                $result['LogPenggunas'] = $this->collLogPenggunas->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSasaranSurveys) {
                $result['SasaranSurveys'] = $this->collSasaranSurveys->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_FIELDNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_FIELDNAME)
    {
        $pos = PenggunaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setPenggunaId($value);
                break;
            case 1:
                $this->setUsername($value);
                break;
            case 2:
                $this->setPassword($value);
                break;
            case 3:
                $this->setNama($value);
                break;
            case 4:
                $this->setNipNim($value);
                break;
            case 5:
                $this->setJabatanLembaga($value);
                break;
            case 6:
                $this->setYm($value);
                break;
            case 7:
                $this->setSkype($value);
                break;
            case 8:
                $this->setAlamat($value);
                break;
            case 9:
                $this->setKodeWilayah($value);
                break;
            case 10:
                $this->setNoTelepon($value);
                break;
            case 11:
                $this->setNoHp($value);
                break;
            case 12:
                $this->setAktif($value);
                break;
            case 13:
                $this->setPtkId($value);
                break;
            case 14:
                $this->setPeranId($value);
                break;
            case 15:
                $this->setSekolahId($value);
                break;
            case 16:
                $this->setLembagaId($value);
                break;
            case 17:
                $this->setYayasanId($value);
                break;
            case 18:
                $this->setLaId($value);
                break;
            case 19:
                $this->setDudiId($value);
                break;
            case 20:
                $this->setLastUpdate($value);
                break;
            case 21:
                $this->setSoftDelete($value);
                break;
            case 22:
                $this->setLastSync($value);
                break;
            case 23:
                $this->setUpdaterId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_FIELDNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_FIELDNAME)
    {
        $keys = PenggunaPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setPenggunaId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setUsername($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setPassword($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNama($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNipNim($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setJabatanLembaga($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setYm($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setSkype($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setAlamat($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setKodeWilayah($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setNoTelepon($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setNoHp($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setAktif($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setPtkId($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setPeranId($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setSekolahId($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setLembagaId($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setYayasanId($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setLaId($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setDudiId($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setLastUpdate($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setSoftDelete($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setLastSync($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setUpdaterId($arr[$keys[23]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PenggunaPeer::DATABASE_NAME);

        if ($this->isColumnModified(PenggunaPeer::PENGGUNA_ID)) $criteria->add(PenggunaPeer::PENGGUNA_ID, $this->pengguna_id);
        if ($this->isColumnModified(PenggunaPeer::USERNAME)) $criteria->add(PenggunaPeer::USERNAME, $this->username);
        if ($this->isColumnModified(PenggunaPeer::PASSWORD)) $criteria->add(PenggunaPeer::PASSWORD, $this->password);
        if ($this->isColumnModified(PenggunaPeer::NAMA)) $criteria->add(PenggunaPeer::NAMA, $this->nama);
        if ($this->isColumnModified(PenggunaPeer::NIP_NIM)) $criteria->add(PenggunaPeer::NIP_NIM, $this->nip_nim);
        if ($this->isColumnModified(PenggunaPeer::JABATAN_LEMBAGA)) $criteria->add(PenggunaPeer::JABATAN_LEMBAGA, $this->jabatan_lembaga);
        if ($this->isColumnModified(PenggunaPeer::YM)) $criteria->add(PenggunaPeer::YM, $this->ym);
        if ($this->isColumnModified(PenggunaPeer::SKYPE)) $criteria->add(PenggunaPeer::SKYPE, $this->skype);
        if ($this->isColumnModified(PenggunaPeer::ALAMAT)) $criteria->add(PenggunaPeer::ALAMAT, $this->alamat);
        if ($this->isColumnModified(PenggunaPeer::KODE_WILAYAH)) $criteria->add(PenggunaPeer::KODE_WILAYAH, $this->kode_wilayah);
        if ($this->isColumnModified(PenggunaPeer::NO_TELEPON)) $criteria->add(PenggunaPeer::NO_TELEPON, $this->no_telepon);
        if ($this->isColumnModified(PenggunaPeer::NO_HP)) $criteria->add(PenggunaPeer::NO_HP, $this->no_hp);
        if ($this->isColumnModified(PenggunaPeer::AKTIF)) $criteria->add(PenggunaPeer::AKTIF, $this->aktif);
        if ($this->isColumnModified(PenggunaPeer::PTK_ID)) $criteria->add(PenggunaPeer::PTK_ID, $this->ptk_id);
        if ($this->isColumnModified(PenggunaPeer::PERAN_ID)) $criteria->add(PenggunaPeer::PERAN_ID, $this->peran_id);
        if ($this->isColumnModified(PenggunaPeer::SEKOLAH_ID)) $criteria->add(PenggunaPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(PenggunaPeer::LEMBAGA_ID)) $criteria->add(PenggunaPeer::LEMBAGA_ID, $this->lembaga_id);
        if ($this->isColumnModified(PenggunaPeer::YAYASAN_ID)) $criteria->add(PenggunaPeer::YAYASAN_ID, $this->yayasan_id);
        if ($this->isColumnModified(PenggunaPeer::LA_ID)) $criteria->add(PenggunaPeer::LA_ID, $this->la_id);
        if ($this->isColumnModified(PenggunaPeer::DUDI_ID)) $criteria->add(PenggunaPeer::DUDI_ID, $this->dudi_id);
        if ($this->isColumnModified(PenggunaPeer::LAST_UPDATE)) $criteria->add(PenggunaPeer::LAST_UPDATE, $this->last_update);
        if ($this->isColumnModified(PenggunaPeer::SOFT_DELETE)) $criteria->add(PenggunaPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(PenggunaPeer::LAST_SYNC)) $criteria->add(PenggunaPeer::LAST_SYNC, $this->last_sync);
        if ($this->isColumnModified(PenggunaPeer::UPDATER_ID)) $criteria->add(PenggunaPeer::UPDATER_ID, $this->updater_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PenggunaPeer::DATABASE_NAME);
        $criteria->add(PenggunaPeer::PENGGUNA_ID, $this->pengguna_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getPenggunaId();
    }

    /**
     * Generic method to set the primary key (pengguna_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setPenggunaId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getPenggunaId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Pengguna (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUsername($this->getUsername());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setNama($this->getNama());
        $copyObj->setNipNim($this->getNipNim());
        $copyObj->setJabatanLembaga($this->getJabatanLembaga());
        $copyObj->setYm($this->getYm());
        $copyObj->setSkype($this->getSkype());
        $copyObj->setAlamat($this->getAlamat());
        $copyObj->setKodeWilayah($this->getKodeWilayah());
        $copyObj->setNoTelepon($this->getNoTelepon());
        $copyObj->setNoHp($this->getNoHp());
        $copyObj->setAktif($this->getAktif());
        $copyObj->setPtkId($this->getPtkId());
        $copyObj->setPeranId($this->getPeranId());
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setLembagaId($this->getLembagaId());
        $copyObj->setYayasanId($this->getYayasanId());
        $copyObj->setLaId($this->getLaId());
        $copyObj->setDudiId($this->getDudiId());
        $copyObj->setLastUpdate($this->getLastUpdate());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setLastSync($this->getLastSync());
        $copyObj->setUpdaterId($this->getUpdaterId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getLogPenggunas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLogPengguna($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSasaranSurveys() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSasaranSurvey($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setPenggunaId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Pengguna Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PenggunaPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PenggunaPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Dudi object.
     *
     * @param             Dudi $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setDudi(Dudi $v = null)
    {
        if ($v === null) {
            $this->setDudiId(NULL);
        } else {
            $this->setDudiId($v->getDudiId());
        }

        $this->aDudi = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Dudi object, it will not be re-added.
        if ($v !== null) {
            $v->addPengguna($this);
        }


        return $this;
    }


    /**
     * Get the associated Dudi object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Dudi The associated Dudi object.
     * @throws PropelException
     */
    public function getDudi(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aDudi === null && (($this->dudi_id !== "" && $this->dudi_id !== null)) && $doQuery) {
            $this->aDudi = DudiQuery::create()->findPk($this->dudi_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aDudi->addPenggunas($this);
             */
        }

        return $this->aDudi;
    }

    /**
     * Declares an association between this object and a LembagaNonSekolah object.
     *
     * @param             LembagaNonSekolah $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLembagaNonSekolah(LembagaNonSekolah $v = null)
    {
        if ($v === null) {
            $this->setLembagaId(NULL);
        } else {
            $this->setLembagaId($v->getLembagaId());
        }

        $this->aLembagaNonSekolah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the LembagaNonSekolah object, it will not be re-added.
        if ($v !== null) {
            $v->addPengguna($this);
        }


        return $this;
    }


    /**
     * Get the associated LembagaNonSekolah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return LembagaNonSekolah The associated LembagaNonSekolah object.
     * @throws PropelException
     */
    public function getLembagaNonSekolah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aLembagaNonSekolah === null && (($this->lembaga_id !== "" && $this->lembaga_id !== null)) && $doQuery) {
            $this->aLembagaNonSekolah = LembagaNonSekolahQuery::create()->findPk($this->lembaga_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLembagaNonSekolah->addPenggunas($this);
             */
        }

        return $this->aLembagaNonSekolah;
    }

    /**
     * Declares an association between this object and a Yayasan object.
     *
     * @param             Yayasan $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setYayasan(Yayasan $v = null)
    {
        if ($v === null) {
            $this->setYayasanId(NULL);
        } else {
            $this->setYayasanId($v->getYayasanId());
        }

        $this->aYayasan = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Yayasan object, it will not be re-added.
        if ($v !== null) {
            $v->addPengguna($this);
        }


        return $this;
    }


    /**
     * Get the associated Yayasan object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Yayasan The associated Yayasan object.
     * @throws PropelException
     */
    public function getYayasan(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aYayasan === null && (($this->yayasan_id !== "" && $this->yayasan_id !== null)) && $doQuery) {
            $this->aYayasan = YayasanQuery::create()->findPk($this->yayasan_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aYayasan->addPenggunas($this);
             */
        }

        return $this->aYayasan;
    }

    /**
     * Declares an association between this object and a LembagaAkreditasi object.
     *
     * @param             LembagaAkreditasi $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLembagaAkreditasi(LembagaAkreditasi $v = null)
    {
        if ($v === null) {
            $this->setLaId(NULL);
        } else {
            $this->setLaId($v->getLaId());
        }

        $this->aLembagaAkreditasi = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the LembagaAkreditasi object, it will not be re-added.
        if ($v !== null) {
            $v->addPengguna($this);
        }


        return $this;
    }


    /**
     * Get the associated LembagaAkreditasi object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return LembagaAkreditasi The associated LembagaAkreditasi object.
     * @throws PropelException
     */
    public function getLembagaAkreditasi(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aLembagaAkreditasi === null && (($this->la_id !== "" && $this->la_id !== null)) && $doQuery) {
            $this->aLembagaAkreditasi = LembagaAkreditasiQuery::create()->findPk($this->la_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLembagaAkreditasi->addPenggunas($this);
             */
        }

        return $this->aLembagaAkreditasi;
    }

    /**
     * Declares an association between this object and a MstWilayah object.
     *
     * @param             MstWilayah $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMstWilayah(MstWilayah $v = null)
    {
        if ($v === null) {
            $this->setKodeWilayah(NULL);
        } else {
            $this->setKodeWilayah($v->getKodeWilayah());
        }

        $this->aMstWilayah = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MstWilayah object, it will not be re-added.
        if ($v !== null) {
            $v->addPengguna($this);
        }


        return $this;
    }


    /**
     * Get the associated MstWilayah object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return MstWilayah The associated MstWilayah object.
     * @throws PropelException
     */
    public function getMstWilayah(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aMstWilayah === null && (($this->kode_wilayah !== "" && $this->kode_wilayah !== null)) && $doQuery) {
            $this->aMstWilayah = MstWilayahQuery::create()->findPk($this->kode_wilayah, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMstWilayah->addPenggunas($this);
             */
        }

        return $this->aMstWilayah;
    }

    /**
     * Declares an association between this object and a Peran object.
     *
     * @param             Peran $v
     * @return Pengguna The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPeran(Peran $v = null)
    {
        if ($v === null) {
            $this->setPeranId(NULL);
        } else {
            $this->setPeranId($v->getPeranId());
        }

        $this->aPeran = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Peran object, it will not be re-added.
        if ($v !== null) {
            $v->addPengguna($this);
        }


        return $this;
    }


    /**
     * Get the associated Peran object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Peran The associated Peran object.
     * @throws PropelException
     */
    public function getPeran(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPeran === null && ($this->peran_id !== null) && $doQuery) {
            $this->aPeran = PeranQuery::create()->findPk($this->peran_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPeran->addPenggunas($this);
             */
        }

        return $this->aPeran;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('LogPengguna' == $relationName) {
            $this->initLogPenggunas();
        }
        if ('SasaranSurvey' == $relationName) {
            $this->initSasaranSurveys();
        }
    }

    /**
     * Clears out the collLogPenggunas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addLogPenggunas()
     */
    public function clearLogPenggunas()
    {
        $this->collLogPenggunas = null; // important to set this to null since that means it is uninitialized
        $this->collLogPenggunasPartial = null;

        return $this;
    }

    /**
     * reset is the collLogPenggunas collection loaded partially
     *
     * @return void
     */
    public function resetPartialLogPenggunas($v = true)
    {
        $this->collLogPenggunasPartial = $v;
    }

    /**
     * Initializes the collLogPenggunas collection.
     *
     * By default this just sets the collLogPenggunas collection to an empty array (like clearcollLogPenggunas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLogPenggunas($overrideExisting = true)
    {
        if (null !== $this->collLogPenggunas && !$overrideExisting) {
            return;
        }
        $this->collLogPenggunas = new PropelObjectCollection();
        $this->collLogPenggunas->setModel('LogPengguna');
    }

    /**
     * Gets an array of LogPengguna objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|LogPengguna[] List of LogPengguna objects
     * @throws PropelException
     */
    public function getLogPenggunas($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collLogPenggunasPartial && !$this->isNew();
        if (null === $this->collLogPenggunas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLogPenggunas) {
                // return empty collection
                $this->initLogPenggunas();
            } else {
                $collLogPenggunas = LogPenggunaQuery::create(null, $criteria)
                    ->filterByPengguna($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collLogPenggunasPartial && count($collLogPenggunas)) {
                      $this->initLogPenggunas(false);

                      foreach($collLogPenggunas as $obj) {
                        if (false == $this->collLogPenggunas->contains($obj)) {
                          $this->collLogPenggunas->append($obj);
                        }
                      }

                      $this->collLogPenggunasPartial = true;
                    }

                    $collLogPenggunas->getInternalIterator()->rewind();
                    return $collLogPenggunas;
                }

                if($partial && $this->collLogPenggunas) {
                    foreach($this->collLogPenggunas as $obj) {
                        if($obj->isNew()) {
                            $collLogPenggunas[] = $obj;
                        }
                    }
                }

                $this->collLogPenggunas = $collLogPenggunas;
                $this->collLogPenggunasPartial = false;
            }
        }

        return $this->collLogPenggunas;
    }

    /**
     * Sets a collection of LogPengguna objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $logPenggunas A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setLogPenggunas(PropelCollection $logPenggunas, PropelPDO $con = null)
    {
        $logPenggunasToDelete = $this->getLogPenggunas(new Criteria(), $con)->diff($logPenggunas);

        $this->logPenggunasScheduledForDeletion = unserialize(serialize($logPenggunasToDelete));

        foreach ($logPenggunasToDelete as $logPenggunaRemoved) {
            $logPenggunaRemoved->setPengguna(null);
        }

        $this->collLogPenggunas = null;
        foreach ($logPenggunas as $logPengguna) {
            $this->addLogPengguna($logPengguna);
        }

        $this->collLogPenggunas = $logPenggunas;
        $this->collLogPenggunasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LogPengguna objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related LogPengguna objects.
     * @throws PropelException
     */
    public function countLogPenggunas(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collLogPenggunasPartial && !$this->isNew();
        if (null === $this->collLogPenggunas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLogPenggunas) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getLogPenggunas());
            }
            $query = LogPenggunaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPengguna($this)
                ->count($con);
        }

        return count($this->collLogPenggunas);
    }

    /**
     * Method called to associate a LogPengguna object to this object
     * through the LogPengguna foreign key attribute.
     *
     * @param    LogPengguna $l LogPengguna
     * @return Pengguna The current object (for fluent API support)
     */
    public function addLogPengguna(LogPengguna $l)
    {
        if ($this->collLogPenggunas === null) {
            $this->initLogPenggunas();
            $this->collLogPenggunasPartial = true;
        }
        if (!in_array($l, $this->collLogPenggunas->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddLogPengguna($l);
        }

        return $this;
    }

    /**
     * @param	LogPengguna $logPengguna The logPengguna object to add.
     */
    protected function doAddLogPengguna($logPengguna)
    {
        $this->collLogPenggunas[]= $logPengguna;
        $logPengguna->setPengguna($this);
    }

    /**
     * @param	LogPengguna $logPengguna The logPengguna object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeLogPengguna($logPengguna)
    {
        if ($this->getLogPenggunas()->contains($logPengguna)) {
            $this->collLogPenggunas->remove($this->collLogPenggunas->search($logPengguna));
            if (null === $this->logPenggunasScheduledForDeletion) {
                $this->logPenggunasScheduledForDeletion = clone $this->collLogPenggunas;
                $this->logPenggunasScheduledForDeletion->clear();
            }
            $this->logPenggunasScheduledForDeletion[]= clone $logPengguna;
            $logPengguna->setPengguna(null);
        }

        return $this;
    }

    /**
     * Clears out the collSasaranSurveys collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Pengguna The current object (for fluent API support)
     * @see        addSasaranSurveys()
     */
    public function clearSasaranSurveys()
    {
        $this->collSasaranSurveys = null; // important to set this to null since that means it is uninitialized
        $this->collSasaranSurveysPartial = null;

        return $this;
    }

    /**
     * reset is the collSasaranSurveys collection loaded partially
     *
     * @return void
     */
    public function resetPartialSasaranSurveys($v = true)
    {
        $this->collSasaranSurveysPartial = $v;
    }

    /**
     * Initializes the collSasaranSurveys collection.
     *
     * By default this just sets the collSasaranSurveys collection to an empty array (like clearcollSasaranSurveys());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSasaranSurveys($overrideExisting = true)
    {
        if (null !== $this->collSasaranSurveys && !$overrideExisting) {
            return;
        }
        $this->collSasaranSurveys = new PropelObjectCollection();
        $this->collSasaranSurveys->setModel('SasaranSurvey');
    }

    /**
     * Gets an array of SasaranSurvey objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Pengguna is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|SasaranSurvey[] List of SasaranSurvey objects
     * @throws PropelException
     */
    public function getSasaranSurveys($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collSasaranSurveysPartial && !$this->isNew();
        if (null === $this->collSasaranSurveys || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSasaranSurveys) {
                // return empty collection
                $this->initSasaranSurveys();
            } else {
                $collSasaranSurveys = SasaranSurveyQuery::create(null, $criteria)
                    ->filterByPengguna($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collSasaranSurveysPartial && count($collSasaranSurveys)) {
                      $this->initSasaranSurveys(false);

                      foreach($collSasaranSurveys as $obj) {
                        if (false == $this->collSasaranSurveys->contains($obj)) {
                          $this->collSasaranSurveys->append($obj);
                        }
                      }

                      $this->collSasaranSurveysPartial = true;
                    }

                    $collSasaranSurveys->getInternalIterator()->rewind();
                    return $collSasaranSurveys;
                }

                if($partial && $this->collSasaranSurveys) {
                    foreach($this->collSasaranSurveys as $obj) {
                        if($obj->isNew()) {
                            $collSasaranSurveys[] = $obj;
                        }
                    }
                }

                $this->collSasaranSurveys = $collSasaranSurveys;
                $this->collSasaranSurveysPartial = false;
            }
        }

        return $this->collSasaranSurveys;
    }

    /**
     * Sets a collection of SasaranSurvey objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $sasaranSurveys A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Pengguna The current object (for fluent API support)
     */
    public function setSasaranSurveys(PropelCollection $sasaranSurveys, PropelPDO $con = null)
    {
        $sasaranSurveysToDelete = $this->getSasaranSurveys(new Criteria(), $con)->diff($sasaranSurveys);

        $this->sasaranSurveysScheduledForDeletion = unserialize(serialize($sasaranSurveysToDelete));

        foreach ($sasaranSurveysToDelete as $sasaranSurveyRemoved) {
            $sasaranSurveyRemoved->setPengguna(null);
        }

        $this->collSasaranSurveys = null;
        foreach ($sasaranSurveys as $sasaranSurvey) {
            $this->addSasaranSurvey($sasaranSurvey);
        }

        $this->collSasaranSurveys = $sasaranSurveys;
        $this->collSasaranSurveysPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SasaranSurvey objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related SasaranSurvey objects.
     * @throws PropelException
     */
    public function countSasaranSurveys(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collSasaranSurveysPartial && !$this->isNew();
        if (null === $this->collSasaranSurveys || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSasaranSurveys) {
                return 0;
            }

            if($partial && !$criteria) {
                return count($this->getSasaranSurveys());
            }
            $query = SasaranSurveyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPengguna($this)
                ->count($con);
        }

        return count($this->collSasaranSurveys);
    }

    /**
     * Method called to associate a SasaranSurvey object to this object
     * through the SasaranSurvey foreign key attribute.
     *
     * @param    SasaranSurvey $l SasaranSurvey
     * @return Pengguna The current object (for fluent API support)
     */
    public function addSasaranSurvey(SasaranSurvey $l)
    {
        if ($this->collSasaranSurveys === null) {
            $this->initSasaranSurveys();
            $this->collSasaranSurveysPartial = true;
        }
        if (!in_array($l, $this->collSasaranSurveys->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddSasaranSurvey($l);
        }

        return $this;
    }

    /**
     * @param	SasaranSurvey $sasaranSurvey The sasaranSurvey object to add.
     */
    protected function doAddSasaranSurvey($sasaranSurvey)
    {
        $this->collSasaranSurveys[]= $sasaranSurvey;
        $sasaranSurvey->setPengguna($this);
    }

    /**
     * @param	SasaranSurvey $sasaranSurvey The sasaranSurvey object to remove.
     * @return Pengguna The current object (for fluent API support)
     */
    public function removeSasaranSurvey($sasaranSurvey)
    {
        if ($this->getSasaranSurveys()->contains($sasaranSurvey)) {
            $this->collSasaranSurveys->remove($this->collSasaranSurveys->search($sasaranSurvey));
            if (null === $this->sasaranSurveysScheduledForDeletion) {
                $this->sasaranSurveysScheduledForDeletion = clone $this->collSasaranSurveys;
                $this->sasaranSurveysScheduledForDeletion->clear();
            }
            $this->sasaranSurveysScheduledForDeletion[]= clone $sasaranSurvey;
            $sasaranSurvey->setPengguna(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pengguna is new, it will return
     * an empty collection; or if this Pengguna has previously
     * been saved, it will retrieve related SasaranSurveys from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pengguna.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|SasaranSurvey[] List of SasaranSurvey objects
     */
    public function getSasaranSurveysJoinSekolah($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = SasaranSurveyQuery::create(null, $criteria);
        $query->joinWith('Sekolah', $join_behavior);

        return $this->getSasaranSurveys($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->pengguna_id = null;
        $this->username = null;
        $this->password = null;
        $this->nama = null;
        $this->nip_nim = null;
        $this->jabatan_lembaga = null;
        $this->ym = null;
        $this->skype = null;
        $this->alamat = null;
        $this->kode_wilayah = null;
        $this->no_telepon = null;
        $this->no_hp = null;
        $this->aktif = null;
        $this->ptk_id = null;
        $this->peran_id = null;
        $this->sekolah_id = null;
        $this->lembaga_id = null;
        $this->yayasan_id = null;
        $this->la_id = null;
        $this->dudi_id = null;
        $this->last_update = null;
        $this->soft_delete = null;
        $this->last_sync = null;
        $this->updater_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collLogPenggunas) {
                foreach ($this->collLogPenggunas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSasaranSurveys) {
                foreach ($this->collSasaranSurveys as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aDudi instanceof Persistent) {
              $this->aDudi->clearAllReferences($deep);
            }
            if ($this->aLembagaNonSekolah instanceof Persistent) {
              $this->aLembagaNonSekolah->clearAllReferences($deep);
            }
            if ($this->aYayasan instanceof Persistent) {
              $this->aYayasan->clearAllReferences($deep);
            }
            if ($this->aLembagaAkreditasi instanceof Persistent) {
              $this->aLembagaAkreditasi->clearAllReferences($deep);
            }
            if ($this->aMstWilayah instanceof Persistent) {
              $this->aMstWilayah->clearAllReferences($deep);
            }
            if ($this->aPeran instanceof Persistent) {
              $this->aPeran->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collLogPenggunas instanceof PropelCollection) {
            $this->collLogPenggunas->clearIterator();
        }
        $this->collLogPenggunas = null;
        if ($this->collSasaranSurveys instanceof PropelCollection) {
            $this->collSasaranSurveys->clearIterator();
        }
        $this->collSasaranSurveys = null;
        $this->aDudi = null;
        $this->aLembagaNonSekolah = null;
        $this->aYayasan = null;
        $this->aLembagaAkreditasi = null;
        $this->aMstWilayah = null;
        $this->aPeran = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PenggunaPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
