<?php

namespace Admin\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use Admin\Model\PrasaranaLongitudinal;
use Admin\Model\PrasaranaLongitudinalPeer;
use Admin\Model\PrasaranaPeer;
use Admin\Model\SemesterPeer;
use Admin\Model\map\PrasaranaLongitudinalTableMap;

/**
 * Base static class for performing query and update operations on the 'prasarana_longitudinal' table.
 *
 *
 *
 * @package propel.generator.Admin.Model.om
 */
abstract class BasePrasaranaLongitudinalPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'Dapodik_Paudni';

    /** the table name for this class */
    const TABLE_NAME = 'prasarana_longitudinal';

    /** the related Propel class for this table */
    const OM_CLASS = 'Admin\\Model\\PrasaranaLongitudinal';

    /** the related TableMap class for this table */
    const TM_CLASS = 'PrasaranaLongitudinalTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 54;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 54;

    /** the column name for the prasarana_id field */
    const PRASARANA_ID = 'prasarana_longitudinal.prasarana_id';

    /** the column name for the semester_id field */
    const SEMESTER_ID = 'prasarana_longitudinal.semester_id';

    /** the column name for the rusak_pondasi field */
    const RUSAK_PONDASI = 'prasarana_longitudinal.rusak_pondasi';

    /** the column name for the ket_pondasi field */
    const KET_PONDASI = 'prasarana_longitudinal.ket_pondasi';

    /** the column name for the rusak_sloop_kolom_balok field */
    const RUSAK_SLOOP_KOLOM_BALOK = 'prasarana_longitudinal.rusak_sloop_kolom_balok';

    /** the column name for the ket_sloop_kolom_balok field */
    const KET_SLOOP_KOLOM_BALOK = 'prasarana_longitudinal.ket_sloop_kolom_balok';

    /** the column name for the rusak_plester_struktur field */
    const RUSAK_PLESTER_STRUKTUR = 'prasarana_longitudinal.rusak_plester_struktur';

    /** the column name for the ket_plester_struktur field */
    const KET_PLESTER_STRUKTUR = 'prasarana_longitudinal.ket_plester_struktur';

    /** the column name for the rusak_kudakuda_atap field */
    const RUSAK_KUDAKUDA_ATAP = 'prasarana_longitudinal.rusak_kudakuda_atap';

    /** the column name for the ket_kudakuda_atap field */
    const KET_KUDAKUDA_ATAP = 'prasarana_longitudinal.ket_kudakuda_atap';

    /** the column name for the rusak_kaso_atap field */
    const RUSAK_KASO_ATAP = 'prasarana_longitudinal.rusak_kaso_atap';

    /** the column name for the ket_kaso_atap field */
    const KET_KASO_ATAP = 'prasarana_longitudinal.ket_kaso_atap';

    /** the column name for the rusak_reng_atap field */
    const RUSAK_RENG_ATAP = 'prasarana_longitudinal.rusak_reng_atap';

    /** the column name for the ket_reng_atap field */
    const KET_RENG_ATAP = 'prasarana_longitudinal.ket_reng_atap';

    /** the column name for the rusak_tutup_atap field */
    const RUSAK_TUTUP_ATAP = 'prasarana_longitudinal.rusak_tutup_atap';

    /** the column name for the ket_tutup_atap field */
    const KET_TUTUP_ATAP = 'prasarana_longitudinal.ket_tutup_atap';

    /** the column name for the rusak_lisplang_talang field */
    const RUSAK_LISPLANG_TALANG = 'prasarana_longitudinal.rusak_lisplang_talang';

    /** the column name for the ket_lisplang_talang field */
    const KET_LISPLANG_TALANG = 'prasarana_longitudinal.ket_lisplang_talang';

    /** the column name for the rusak_rangka_plafon field */
    const RUSAK_RANGKA_PLAFON = 'prasarana_longitudinal.rusak_rangka_plafon';

    /** the column name for the ket_rangka_plafon field */
    const KET_RANGKA_PLAFON = 'prasarana_longitudinal.ket_rangka_plafon';

    /** the column name for the rusak_tutup_plafon field */
    const RUSAK_TUTUP_PLAFON = 'prasarana_longitudinal.rusak_tutup_plafon';

    /** the column name for the ket_tutup_plafon field */
    const KET_TUTUP_PLAFON = 'prasarana_longitudinal.ket_tutup_plafon';

    /** the column name for the rusak_bata_dinding field */
    const RUSAK_BATA_DINDING = 'prasarana_longitudinal.rusak_bata_dinding';

    /** the column name for the ket_bata_dinding field */
    const KET_BATA_DINDING = 'prasarana_longitudinal.ket_bata_dinding';

    /** the column name for the rusak_plester_dinding field */
    const RUSAK_PLESTER_DINDING = 'prasarana_longitudinal.rusak_plester_dinding';

    /** the column name for the ket_plester_dinding field */
    const KET_PLESTER_DINDING = 'prasarana_longitudinal.ket_plester_dinding';

    /** the column name for the rusak_daun_jendela field */
    const RUSAK_DAUN_JENDELA = 'prasarana_longitudinal.rusak_daun_jendela';

    /** the column name for the ket_daun_jendela field */
    const KET_DAUN_JENDELA = 'prasarana_longitudinal.ket_daun_jendela';

    /** the column name for the rusak_daun_pintu field */
    const RUSAK_DAUN_PINTU = 'prasarana_longitudinal.rusak_daun_pintu';

    /** the column name for the ket_daun_pintu field */
    const KET_DAUN_PINTU = 'prasarana_longitudinal.ket_daun_pintu';

    /** the column name for the rusak_kusen field */
    const RUSAK_KUSEN = 'prasarana_longitudinal.rusak_kusen';

    /** the column name for the ket_kusen field */
    const KET_KUSEN = 'prasarana_longitudinal.ket_kusen';

    /** the column name for the rusak_tutup_lantai field */
    const RUSAK_TUTUP_LANTAI = 'prasarana_longitudinal.rusak_tutup_lantai';

    /** the column name for the ket_penutup_lantai field */
    const KET_PENUTUP_LANTAI = 'prasarana_longitudinal.ket_penutup_lantai';

    /** the column name for the rusak_inst_listrik field */
    const RUSAK_INST_LISTRIK = 'prasarana_longitudinal.rusak_inst_listrik';

    /** the column name for the ket_inst_listrik field */
    const KET_INST_LISTRIK = 'prasarana_longitudinal.ket_inst_listrik';

    /** the column name for the rusak_inst_air field */
    const RUSAK_INST_AIR = 'prasarana_longitudinal.rusak_inst_air';

    /** the column name for the ket_inst_air field */
    const KET_INST_AIR = 'prasarana_longitudinal.ket_inst_air';

    /** the column name for the rusak_drainase field */
    const RUSAK_DRAINASE = 'prasarana_longitudinal.rusak_drainase';

    /** the column name for the ket_drainase field */
    const KET_DRAINASE = 'prasarana_longitudinal.ket_drainase';

    /** the column name for the rusak_finish_struktur field */
    const RUSAK_FINISH_STRUKTUR = 'prasarana_longitudinal.rusak_finish_struktur';

    /** the column name for the ket_finish_struktur field */
    const KET_FINISH_STRUKTUR = 'prasarana_longitudinal.ket_finish_struktur';

    /** the column name for the rusak_finish_plafon field */
    const RUSAK_FINISH_PLAFON = 'prasarana_longitudinal.rusak_finish_plafon';

    /** the column name for the ket_finish_plafon field */
    const KET_FINISH_PLAFON = 'prasarana_longitudinal.ket_finish_plafon';

    /** the column name for the rusak_finish_dinding field */
    const RUSAK_FINISH_DINDING = 'prasarana_longitudinal.rusak_finish_dinding';

    /** the column name for the ket_finish_dinding field */
    const KET_FINISH_DINDING = 'prasarana_longitudinal.ket_finish_dinding';

    /** the column name for the rusak_finish_kpj field */
    const RUSAK_FINISH_KPJ = 'prasarana_longitudinal.rusak_finish_kpj';

    /** the column name for the ket_finish_kpj field */
    const KET_FINISH_KPJ = 'prasarana_longitudinal.ket_finish_kpj';

    /** the column name for the berfungsi field */
    const BERFUNGSI = 'prasarana_longitudinal.berfungsi';

    /** the column name for the blob_id field */
    const BLOB_ID = 'prasarana_longitudinal.blob_id';

    /** the column name for the Last_update field */
    const LAST_UPDATE = 'prasarana_longitudinal.Last_update';

    /** the column name for the Soft_delete field */
    const SOFT_DELETE = 'prasarana_longitudinal.Soft_delete';

    /** the column name for the last_sync field */
    const LAST_SYNC = 'prasarana_longitudinal.last_sync';

    /** the column name for the Updater_ID field */
    const UPDATER_ID = 'prasarana_longitudinal.Updater_ID';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of PrasaranaLongitudinal objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array PrasaranaLongitudinal[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. PrasaranaLongitudinalPeer::$fieldNames[PrasaranaLongitudinalPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('PrasaranaId', 'SemesterId', 'RusakPondasi', 'KetPondasi', 'RusakSloopKolomBalok', 'KetSloopKolomBalok', 'RusakPlesterStruktur', 'KetPlesterStruktur', 'RusakKudakudaAtap', 'KetKudakudaAtap', 'RusakKasoAtap', 'KetKasoAtap', 'RusakRengAtap', 'KetRengAtap', 'RusakTutupAtap', 'KetTutupAtap', 'RusakLisplangTalang', 'KetLisplangTalang', 'RusakRangkaPlafon', 'KetRangkaPlafon', 'RusakTutupPlafon', 'KetTutupPlafon', 'RusakBataDinding', 'KetBataDinding', 'RusakPlesterDinding', 'KetPlesterDinding', 'RusakDaunJendela', 'KetDaunJendela', 'RusakDaunPintu', 'KetDaunPintu', 'RusakKusen', 'KetKusen', 'RusakTutupLantai', 'KetPenutupLantai', 'RusakInstListrik', 'KetInstListrik', 'RusakInstAir', 'KetInstAir', 'RusakDrainase', 'KetDrainase', 'RusakFinishStruktur', 'KetFinishStruktur', 'RusakFinishPlafon', 'KetFinishPlafon', 'RusakFinishDinding', 'KetFinishDinding', 'RusakFinishKpj', 'KetFinishKpj', 'Berfungsi', 'BlobId', 'LastUpdate', 'SoftDelete', 'LastSync', 'UpdaterId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('prasaranaId', 'semesterId', 'rusakPondasi', 'ketPondasi', 'rusakSloopKolomBalok', 'ketSloopKolomBalok', 'rusakPlesterStruktur', 'ketPlesterStruktur', 'rusakKudakudaAtap', 'ketKudakudaAtap', 'rusakKasoAtap', 'ketKasoAtap', 'rusakRengAtap', 'ketRengAtap', 'rusakTutupAtap', 'ketTutupAtap', 'rusakLisplangTalang', 'ketLisplangTalang', 'rusakRangkaPlafon', 'ketRangkaPlafon', 'rusakTutupPlafon', 'ketTutupPlafon', 'rusakBataDinding', 'ketBataDinding', 'rusakPlesterDinding', 'ketPlesterDinding', 'rusakDaunJendela', 'ketDaunJendela', 'rusakDaunPintu', 'ketDaunPintu', 'rusakKusen', 'ketKusen', 'rusakTutupLantai', 'ketPenutupLantai', 'rusakInstListrik', 'ketInstListrik', 'rusakInstAir', 'ketInstAir', 'rusakDrainase', 'ketDrainase', 'rusakFinishStruktur', 'ketFinishStruktur', 'rusakFinishPlafon', 'ketFinishPlafon', 'rusakFinishDinding', 'ketFinishDinding', 'rusakFinishKpj', 'ketFinishKpj', 'berfungsi', 'blobId', 'lastUpdate', 'softDelete', 'lastSync', 'updaterId', ),
        BasePeer::TYPE_COLNAME => array (PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaLongitudinalPeer::SEMESTER_ID, PrasaranaLongitudinalPeer::RUSAK_PONDASI, PrasaranaLongitudinalPeer::KET_PONDASI, PrasaranaLongitudinalPeer::RUSAK_SLOOP_KOLOM_BALOK, PrasaranaLongitudinalPeer::KET_SLOOP_KOLOM_BALOK, PrasaranaLongitudinalPeer::RUSAK_PLESTER_STRUKTUR, PrasaranaLongitudinalPeer::KET_PLESTER_STRUKTUR, PrasaranaLongitudinalPeer::RUSAK_KUDAKUDA_ATAP, PrasaranaLongitudinalPeer::KET_KUDAKUDA_ATAP, PrasaranaLongitudinalPeer::RUSAK_KASO_ATAP, PrasaranaLongitudinalPeer::KET_KASO_ATAP, PrasaranaLongitudinalPeer::RUSAK_RENG_ATAP, PrasaranaLongitudinalPeer::KET_RENG_ATAP, PrasaranaLongitudinalPeer::RUSAK_TUTUP_ATAP, PrasaranaLongitudinalPeer::KET_TUTUP_ATAP, PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG, PrasaranaLongitudinalPeer::KET_LISPLANG_TALANG, PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON, PrasaranaLongitudinalPeer::KET_RANGKA_PLAFON, PrasaranaLongitudinalPeer::RUSAK_TUTUP_PLAFON, PrasaranaLongitudinalPeer::KET_TUTUP_PLAFON, PrasaranaLongitudinalPeer::RUSAK_BATA_DINDING, PrasaranaLongitudinalPeer::KET_BATA_DINDING, PrasaranaLongitudinalPeer::RUSAK_PLESTER_DINDING, PrasaranaLongitudinalPeer::KET_PLESTER_DINDING, PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA, PrasaranaLongitudinalPeer::KET_DAUN_JENDELA, PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU, PrasaranaLongitudinalPeer::KET_DAUN_PINTU, PrasaranaLongitudinalPeer::RUSAK_KUSEN, PrasaranaLongitudinalPeer::KET_KUSEN, PrasaranaLongitudinalPeer::RUSAK_TUTUP_LANTAI, PrasaranaLongitudinalPeer::KET_PENUTUP_LANTAI, PrasaranaLongitudinalPeer::RUSAK_INST_LISTRIK, PrasaranaLongitudinalPeer::KET_INST_LISTRIK, PrasaranaLongitudinalPeer::RUSAK_INST_AIR, PrasaranaLongitudinalPeer::KET_INST_AIR, PrasaranaLongitudinalPeer::RUSAK_DRAINASE, PrasaranaLongitudinalPeer::KET_DRAINASE, PrasaranaLongitudinalPeer::RUSAK_FINISH_STRUKTUR, PrasaranaLongitudinalPeer::KET_FINISH_STRUKTUR, PrasaranaLongitudinalPeer::RUSAK_FINISH_PLAFON, PrasaranaLongitudinalPeer::KET_FINISH_PLAFON, PrasaranaLongitudinalPeer::RUSAK_FINISH_DINDING, PrasaranaLongitudinalPeer::KET_FINISH_DINDING, PrasaranaLongitudinalPeer::RUSAK_FINISH_KPJ, PrasaranaLongitudinalPeer::KET_FINISH_KPJ, PrasaranaLongitudinalPeer::BERFUNGSI, PrasaranaLongitudinalPeer::BLOB_ID, PrasaranaLongitudinalPeer::LAST_UPDATE, PrasaranaLongitudinalPeer::SOFT_DELETE, PrasaranaLongitudinalPeer::LAST_SYNC, PrasaranaLongitudinalPeer::UPDATER_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('PRASARANA_ID', 'SEMESTER_ID', 'RUSAK_PONDASI', 'KET_PONDASI', 'RUSAK_SLOOP_KOLOM_BALOK', 'KET_SLOOP_KOLOM_BALOK', 'RUSAK_PLESTER_STRUKTUR', 'KET_PLESTER_STRUKTUR', 'RUSAK_KUDAKUDA_ATAP', 'KET_KUDAKUDA_ATAP', 'RUSAK_KASO_ATAP', 'KET_KASO_ATAP', 'RUSAK_RENG_ATAP', 'KET_RENG_ATAP', 'RUSAK_TUTUP_ATAP', 'KET_TUTUP_ATAP', 'RUSAK_LISPLANG_TALANG', 'KET_LISPLANG_TALANG', 'RUSAK_RANGKA_PLAFON', 'KET_RANGKA_PLAFON', 'RUSAK_TUTUP_PLAFON', 'KET_TUTUP_PLAFON', 'RUSAK_BATA_DINDING', 'KET_BATA_DINDING', 'RUSAK_PLESTER_DINDING', 'KET_PLESTER_DINDING', 'RUSAK_DAUN_JENDELA', 'KET_DAUN_JENDELA', 'RUSAK_DAUN_PINTU', 'KET_DAUN_PINTU', 'RUSAK_KUSEN', 'KET_KUSEN', 'RUSAK_TUTUP_LANTAI', 'KET_PENUTUP_LANTAI', 'RUSAK_INST_LISTRIK', 'KET_INST_LISTRIK', 'RUSAK_INST_AIR', 'KET_INST_AIR', 'RUSAK_DRAINASE', 'KET_DRAINASE', 'RUSAK_FINISH_STRUKTUR', 'KET_FINISH_STRUKTUR', 'RUSAK_FINISH_PLAFON', 'KET_FINISH_PLAFON', 'RUSAK_FINISH_DINDING', 'KET_FINISH_DINDING', 'RUSAK_FINISH_KPJ', 'KET_FINISH_KPJ', 'BERFUNGSI', 'BLOB_ID', 'LAST_UPDATE', 'SOFT_DELETE', 'LAST_SYNC', 'UPDATER_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('prasarana_id', 'semester_id', 'rusak_pondasi', 'ket_pondasi', 'rusak_sloop_kolom_balok', 'ket_sloop_kolom_balok', 'rusak_plester_struktur', 'ket_plester_struktur', 'rusak_kudakuda_atap', 'ket_kudakuda_atap', 'rusak_kaso_atap', 'ket_kaso_atap', 'rusak_reng_atap', 'ket_reng_atap', 'rusak_tutup_atap', 'ket_tutup_atap', 'rusak_lisplang_talang', 'ket_lisplang_talang', 'rusak_rangka_plafon', 'ket_rangka_plafon', 'rusak_tutup_plafon', 'ket_tutup_plafon', 'rusak_bata_dinding', 'ket_bata_dinding', 'rusak_plester_dinding', 'ket_plester_dinding', 'rusak_daun_jendela', 'ket_daun_jendela', 'rusak_daun_pintu', 'ket_daun_pintu', 'rusak_kusen', 'ket_kusen', 'rusak_tutup_lantai', 'ket_penutup_lantai', 'rusak_inst_listrik', 'ket_inst_listrik', 'rusak_inst_air', 'ket_inst_air', 'rusak_drainase', 'ket_drainase', 'rusak_finish_struktur', 'ket_finish_struktur', 'rusak_finish_plafon', 'ket_finish_plafon', 'rusak_finish_dinding', 'ket_finish_dinding', 'rusak_finish_kpj', 'ket_finish_kpj', 'berfungsi', 'blob_id', 'Last_update', 'Soft_delete', 'last_sync', 'Updater_ID', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. PrasaranaLongitudinalPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('PrasaranaId' => 0, 'SemesterId' => 1, 'RusakPondasi' => 2, 'KetPondasi' => 3, 'RusakSloopKolomBalok' => 4, 'KetSloopKolomBalok' => 5, 'RusakPlesterStruktur' => 6, 'KetPlesterStruktur' => 7, 'RusakKudakudaAtap' => 8, 'KetKudakudaAtap' => 9, 'RusakKasoAtap' => 10, 'KetKasoAtap' => 11, 'RusakRengAtap' => 12, 'KetRengAtap' => 13, 'RusakTutupAtap' => 14, 'KetTutupAtap' => 15, 'RusakLisplangTalang' => 16, 'KetLisplangTalang' => 17, 'RusakRangkaPlafon' => 18, 'KetRangkaPlafon' => 19, 'RusakTutupPlafon' => 20, 'KetTutupPlafon' => 21, 'RusakBataDinding' => 22, 'KetBataDinding' => 23, 'RusakPlesterDinding' => 24, 'KetPlesterDinding' => 25, 'RusakDaunJendela' => 26, 'KetDaunJendela' => 27, 'RusakDaunPintu' => 28, 'KetDaunPintu' => 29, 'RusakKusen' => 30, 'KetKusen' => 31, 'RusakTutupLantai' => 32, 'KetPenutupLantai' => 33, 'RusakInstListrik' => 34, 'KetInstListrik' => 35, 'RusakInstAir' => 36, 'KetInstAir' => 37, 'RusakDrainase' => 38, 'KetDrainase' => 39, 'RusakFinishStruktur' => 40, 'KetFinishStruktur' => 41, 'RusakFinishPlafon' => 42, 'KetFinishPlafon' => 43, 'RusakFinishDinding' => 44, 'KetFinishDinding' => 45, 'RusakFinishKpj' => 46, 'KetFinishKpj' => 47, 'Berfungsi' => 48, 'BlobId' => 49, 'LastUpdate' => 50, 'SoftDelete' => 51, 'LastSync' => 52, 'UpdaterId' => 53, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('prasaranaId' => 0, 'semesterId' => 1, 'rusakPondasi' => 2, 'ketPondasi' => 3, 'rusakSloopKolomBalok' => 4, 'ketSloopKolomBalok' => 5, 'rusakPlesterStruktur' => 6, 'ketPlesterStruktur' => 7, 'rusakKudakudaAtap' => 8, 'ketKudakudaAtap' => 9, 'rusakKasoAtap' => 10, 'ketKasoAtap' => 11, 'rusakRengAtap' => 12, 'ketRengAtap' => 13, 'rusakTutupAtap' => 14, 'ketTutupAtap' => 15, 'rusakLisplangTalang' => 16, 'ketLisplangTalang' => 17, 'rusakRangkaPlafon' => 18, 'ketRangkaPlafon' => 19, 'rusakTutupPlafon' => 20, 'ketTutupPlafon' => 21, 'rusakBataDinding' => 22, 'ketBataDinding' => 23, 'rusakPlesterDinding' => 24, 'ketPlesterDinding' => 25, 'rusakDaunJendela' => 26, 'ketDaunJendela' => 27, 'rusakDaunPintu' => 28, 'ketDaunPintu' => 29, 'rusakKusen' => 30, 'ketKusen' => 31, 'rusakTutupLantai' => 32, 'ketPenutupLantai' => 33, 'rusakInstListrik' => 34, 'ketInstListrik' => 35, 'rusakInstAir' => 36, 'ketInstAir' => 37, 'rusakDrainase' => 38, 'ketDrainase' => 39, 'rusakFinishStruktur' => 40, 'ketFinishStruktur' => 41, 'rusakFinishPlafon' => 42, 'ketFinishPlafon' => 43, 'rusakFinishDinding' => 44, 'ketFinishDinding' => 45, 'rusakFinishKpj' => 46, 'ketFinishKpj' => 47, 'berfungsi' => 48, 'blobId' => 49, 'lastUpdate' => 50, 'softDelete' => 51, 'lastSync' => 52, 'updaterId' => 53, ),
        BasePeer::TYPE_COLNAME => array (PrasaranaLongitudinalPeer::PRASARANA_ID => 0, PrasaranaLongitudinalPeer::SEMESTER_ID => 1, PrasaranaLongitudinalPeer::RUSAK_PONDASI => 2, PrasaranaLongitudinalPeer::KET_PONDASI => 3, PrasaranaLongitudinalPeer::RUSAK_SLOOP_KOLOM_BALOK => 4, PrasaranaLongitudinalPeer::KET_SLOOP_KOLOM_BALOK => 5, PrasaranaLongitudinalPeer::RUSAK_PLESTER_STRUKTUR => 6, PrasaranaLongitudinalPeer::KET_PLESTER_STRUKTUR => 7, PrasaranaLongitudinalPeer::RUSAK_KUDAKUDA_ATAP => 8, PrasaranaLongitudinalPeer::KET_KUDAKUDA_ATAP => 9, PrasaranaLongitudinalPeer::RUSAK_KASO_ATAP => 10, PrasaranaLongitudinalPeer::KET_KASO_ATAP => 11, PrasaranaLongitudinalPeer::RUSAK_RENG_ATAP => 12, PrasaranaLongitudinalPeer::KET_RENG_ATAP => 13, PrasaranaLongitudinalPeer::RUSAK_TUTUP_ATAP => 14, PrasaranaLongitudinalPeer::KET_TUTUP_ATAP => 15, PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG => 16, PrasaranaLongitudinalPeer::KET_LISPLANG_TALANG => 17, PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON => 18, PrasaranaLongitudinalPeer::KET_RANGKA_PLAFON => 19, PrasaranaLongitudinalPeer::RUSAK_TUTUP_PLAFON => 20, PrasaranaLongitudinalPeer::KET_TUTUP_PLAFON => 21, PrasaranaLongitudinalPeer::RUSAK_BATA_DINDING => 22, PrasaranaLongitudinalPeer::KET_BATA_DINDING => 23, PrasaranaLongitudinalPeer::RUSAK_PLESTER_DINDING => 24, PrasaranaLongitudinalPeer::KET_PLESTER_DINDING => 25, PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA => 26, PrasaranaLongitudinalPeer::KET_DAUN_JENDELA => 27, PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU => 28, PrasaranaLongitudinalPeer::KET_DAUN_PINTU => 29, PrasaranaLongitudinalPeer::RUSAK_KUSEN => 30, PrasaranaLongitudinalPeer::KET_KUSEN => 31, PrasaranaLongitudinalPeer::RUSAK_TUTUP_LANTAI => 32, PrasaranaLongitudinalPeer::KET_PENUTUP_LANTAI => 33, PrasaranaLongitudinalPeer::RUSAK_INST_LISTRIK => 34, PrasaranaLongitudinalPeer::KET_INST_LISTRIK => 35, PrasaranaLongitudinalPeer::RUSAK_INST_AIR => 36, PrasaranaLongitudinalPeer::KET_INST_AIR => 37, PrasaranaLongitudinalPeer::RUSAK_DRAINASE => 38, PrasaranaLongitudinalPeer::KET_DRAINASE => 39, PrasaranaLongitudinalPeer::RUSAK_FINISH_STRUKTUR => 40, PrasaranaLongitudinalPeer::KET_FINISH_STRUKTUR => 41, PrasaranaLongitudinalPeer::RUSAK_FINISH_PLAFON => 42, PrasaranaLongitudinalPeer::KET_FINISH_PLAFON => 43, PrasaranaLongitudinalPeer::RUSAK_FINISH_DINDING => 44, PrasaranaLongitudinalPeer::KET_FINISH_DINDING => 45, PrasaranaLongitudinalPeer::RUSAK_FINISH_KPJ => 46, PrasaranaLongitudinalPeer::KET_FINISH_KPJ => 47, PrasaranaLongitudinalPeer::BERFUNGSI => 48, PrasaranaLongitudinalPeer::BLOB_ID => 49, PrasaranaLongitudinalPeer::LAST_UPDATE => 50, PrasaranaLongitudinalPeer::SOFT_DELETE => 51, PrasaranaLongitudinalPeer::LAST_SYNC => 52, PrasaranaLongitudinalPeer::UPDATER_ID => 53, ),
        BasePeer::TYPE_RAW_COLNAME => array ('PRASARANA_ID' => 0, 'SEMESTER_ID' => 1, 'RUSAK_PONDASI' => 2, 'KET_PONDASI' => 3, 'RUSAK_SLOOP_KOLOM_BALOK' => 4, 'KET_SLOOP_KOLOM_BALOK' => 5, 'RUSAK_PLESTER_STRUKTUR' => 6, 'KET_PLESTER_STRUKTUR' => 7, 'RUSAK_KUDAKUDA_ATAP' => 8, 'KET_KUDAKUDA_ATAP' => 9, 'RUSAK_KASO_ATAP' => 10, 'KET_KASO_ATAP' => 11, 'RUSAK_RENG_ATAP' => 12, 'KET_RENG_ATAP' => 13, 'RUSAK_TUTUP_ATAP' => 14, 'KET_TUTUP_ATAP' => 15, 'RUSAK_LISPLANG_TALANG' => 16, 'KET_LISPLANG_TALANG' => 17, 'RUSAK_RANGKA_PLAFON' => 18, 'KET_RANGKA_PLAFON' => 19, 'RUSAK_TUTUP_PLAFON' => 20, 'KET_TUTUP_PLAFON' => 21, 'RUSAK_BATA_DINDING' => 22, 'KET_BATA_DINDING' => 23, 'RUSAK_PLESTER_DINDING' => 24, 'KET_PLESTER_DINDING' => 25, 'RUSAK_DAUN_JENDELA' => 26, 'KET_DAUN_JENDELA' => 27, 'RUSAK_DAUN_PINTU' => 28, 'KET_DAUN_PINTU' => 29, 'RUSAK_KUSEN' => 30, 'KET_KUSEN' => 31, 'RUSAK_TUTUP_LANTAI' => 32, 'KET_PENUTUP_LANTAI' => 33, 'RUSAK_INST_LISTRIK' => 34, 'KET_INST_LISTRIK' => 35, 'RUSAK_INST_AIR' => 36, 'KET_INST_AIR' => 37, 'RUSAK_DRAINASE' => 38, 'KET_DRAINASE' => 39, 'RUSAK_FINISH_STRUKTUR' => 40, 'KET_FINISH_STRUKTUR' => 41, 'RUSAK_FINISH_PLAFON' => 42, 'KET_FINISH_PLAFON' => 43, 'RUSAK_FINISH_DINDING' => 44, 'KET_FINISH_DINDING' => 45, 'RUSAK_FINISH_KPJ' => 46, 'KET_FINISH_KPJ' => 47, 'BERFUNGSI' => 48, 'BLOB_ID' => 49, 'LAST_UPDATE' => 50, 'SOFT_DELETE' => 51, 'LAST_SYNC' => 52, 'UPDATER_ID' => 53, ),
        BasePeer::TYPE_FIELDNAME => array ('prasarana_id' => 0, 'semester_id' => 1, 'rusak_pondasi' => 2, 'ket_pondasi' => 3, 'rusak_sloop_kolom_balok' => 4, 'ket_sloop_kolom_balok' => 5, 'rusak_plester_struktur' => 6, 'ket_plester_struktur' => 7, 'rusak_kudakuda_atap' => 8, 'ket_kudakuda_atap' => 9, 'rusak_kaso_atap' => 10, 'ket_kaso_atap' => 11, 'rusak_reng_atap' => 12, 'ket_reng_atap' => 13, 'rusak_tutup_atap' => 14, 'ket_tutup_atap' => 15, 'rusak_lisplang_talang' => 16, 'ket_lisplang_talang' => 17, 'rusak_rangka_plafon' => 18, 'ket_rangka_plafon' => 19, 'rusak_tutup_plafon' => 20, 'ket_tutup_plafon' => 21, 'rusak_bata_dinding' => 22, 'ket_bata_dinding' => 23, 'rusak_plester_dinding' => 24, 'ket_plester_dinding' => 25, 'rusak_daun_jendela' => 26, 'ket_daun_jendela' => 27, 'rusak_daun_pintu' => 28, 'ket_daun_pintu' => 29, 'rusak_kusen' => 30, 'ket_kusen' => 31, 'rusak_tutup_lantai' => 32, 'ket_penutup_lantai' => 33, 'rusak_inst_listrik' => 34, 'ket_inst_listrik' => 35, 'rusak_inst_air' => 36, 'ket_inst_air' => 37, 'rusak_drainase' => 38, 'ket_drainase' => 39, 'rusak_finish_struktur' => 40, 'ket_finish_struktur' => 41, 'rusak_finish_plafon' => 42, 'ket_finish_plafon' => 43, 'rusak_finish_dinding' => 44, 'ket_finish_dinding' => 45, 'rusak_finish_kpj' => 46, 'ket_finish_kpj' => 47, 'berfungsi' => 48, 'blob_id' => 49, 'Last_update' => 50, 'Soft_delete' => 51, 'last_sync' => 52, 'Updater_ID' => 53, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = PrasaranaLongitudinalPeer::getFieldNames($toType);
        $key = isset(PrasaranaLongitudinalPeer::$fieldKeys[$fromType][$name]) ? PrasaranaLongitudinalPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(PrasaranaLongitudinalPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, PrasaranaLongitudinalPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return PrasaranaLongitudinalPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. PrasaranaLongitudinalPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(PrasaranaLongitudinalPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::PRASARANA_ID);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::SEMESTER_ID);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_PONDASI);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_PONDASI);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_SLOOP_KOLOM_BALOK);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_SLOOP_KOLOM_BALOK);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_PLESTER_STRUKTUR);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_PLESTER_STRUKTUR);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_KUDAKUDA_ATAP);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_KUDAKUDA_ATAP);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_KASO_ATAP);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_KASO_ATAP);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_RENG_ATAP);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_RENG_ATAP);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_TUTUP_ATAP);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_TUTUP_ATAP);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_LISPLANG_TALANG);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_LISPLANG_TALANG);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_RANGKA_PLAFON);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_RANGKA_PLAFON);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_TUTUP_PLAFON);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_TUTUP_PLAFON);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_BATA_DINDING);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_BATA_DINDING);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_PLESTER_DINDING);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_PLESTER_DINDING);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_DAUN_JENDELA);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_DAUN_JENDELA);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_DAUN_PINTU);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_DAUN_PINTU);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_KUSEN);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_KUSEN);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_TUTUP_LANTAI);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_PENUTUP_LANTAI);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_INST_LISTRIK);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_INST_LISTRIK);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_INST_AIR);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_INST_AIR);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_DRAINASE);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_DRAINASE);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_FINISH_STRUKTUR);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_FINISH_STRUKTUR);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_FINISH_PLAFON);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_FINISH_PLAFON);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_FINISH_DINDING);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_FINISH_DINDING);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::RUSAK_FINISH_KPJ);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::KET_FINISH_KPJ);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::BERFUNGSI);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::BLOB_ID);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::LAST_UPDATE);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::SOFT_DELETE);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::LAST_SYNC);
            $criteria->addSelectColumn(PrasaranaLongitudinalPeer::UPDATER_ID);
        } else {
            $criteria->addSelectColumn($alias . '.prasarana_id');
            $criteria->addSelectColumn($alias . '.semester_id');
            $criteria->addSelectColumn($alias . '.rusak_pondasi');
            $criteria->addSelectColumn($alias . '.ket_pondasi');
            $criteria->addSelectColumn($alias . '.rusak_sloop_kolom_balok');
            $criteria->addSelectColumn($alias . '.ket_sloop_kolom_balok');
            $criteria->addSelectColumn($alias . '.rusak_plester_struktur');
            $criteria->addSelectColumn($alias . '.ket_plester_struktur');
            $criteria->addSelectColumn($alias . '.rusak_kudakuda_atap');
            $criteria->addSelectColumn($alias . '.ket_kudakuda_atap');
            $criteria->addSelectColumn($alias . '.rusak_kaso_atap');
            $criteria->addSelectColumn($alias . '.ket_kaso_atap');
            $criteria->addSelectColumn($alias . '.rusak_reng_atap');
            $criteria->addSelectColumn($alias . '.ket_reng_atap');
            $criteria->addSelectColumn($alias . '.rusak_tutup_atap');
            $criteria->addSelectColumn($alias . '.ket_tutup_atap');
            $criteria->addSelectColumn($alias . '.rusak_lisplang_talang');
            $criteria->addSelectColumn($alias . '.ket_lisplang_talang');
            $criteria->addSelectColumn($alias . '.rusak_rangka_plafon');
            $criteria->addSelectColumn($alias . '.ket_rangka_plafon');
            $criteria->addSelectColumn($alias . '.rusak_tutup_plafon');
            $criteria->addSelectColumn($alias . '.ket_tutup_plafon');
            $criteria->addSelectColumn($alias . '.rusak_bata_dinding');
            $criteria->addSelectColumn($alias . '.ket_bata_dinding');
            $criteria->addSelectColumn($alias . '.rusak_plester_dinding');
            $criteria->addSelectColumn($alias . '.ket_plester_dinding');
            $criteria->addSelectColumn($alias . '.rusak_daun_jendela');
            $criteria->addSelectColumn($alias . '.ket_daun_jendela');
            $criteria->addSelectColumn($alias . '.rusak_daun_pintu');
            $criteria->addSelectColumn($alias . '.ket_daun_pintu');
            $criteria->addSelectColumn($alias . '.rusak_kusen');
            $criteria->addSelectColumn($alias . '.ket_kusen');
            $criteria->addSelectColumn($alias . '.rusak_tutup_lantai');
            $criteria->addSelectColumn($alias . '.ket_penutup_lantai');
            $criteria->addSelectColumn($alias . '.rusak_inst_listrik');
            $criteria->addSelectColumn($alias . '.ket_inst_listrik');
            $criteria->addSelectColumn($alias . '.rusak_inst_air');
            $criteria->addSelectColumn($alias . '.ket_inst_air');
            $criteria->addSelectColumn($alias . '.rusak_drainase');
            $criteria->addSelectColumn($alias . '.ket_drainase');
            $criteria->addSelectColumn($alias . '.rusak_finish_struktur');
            $criteria->addSelectColumn($alias . '.ket_finish_struktur');
            $criteria->addSelectColumn($alias . '.rusak_finish_plafon');
            $criteria->addSelectColumn($alias . '.ket_finish_plafon');
            $criteria->addSelectColumn($alias . '.rusak_finish_dinding');
            $criteria->addSelectColumn($alias . '.ket_finish_dinding');
            $criteria->addSelectColumn($alias . '.rusak_finish_kpj');
            $criteria->addSelectColumn($alias . '.ket_finish_kpj');
            $criteria->addSelectColumn($alias . '.berfungsi');
            $criteria->addSelectColumn($alias . '.blob_id');
            $criteria->addSelectColumn($alias . '.Last_update');
            $criteria->addSelectColumn($alias . '.Soft_delete');
            $criteria->addSelectColumn($alias . '.last_sync');
            $criteria->addSelectColumn($alias . '.Updater_ID');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 PrasaranaLongitudinal
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = PrasaranaLongitudinalPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return PrasaranaLongitudinalPeer::populateObjects(PrasaranaLongitudinalPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      PrasaranaLongitudinal $obj A PrasaranaLongitudinal object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = serialize(array((string) $obj->getPrasaranaId(), (string) $obj->getSemesterId()));
            } // if key === null
            PrasaranaLongitudinalPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A PrasaranaLongitudinal object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof PrasaranaLongitudinal) {
                $key = serialize(array((string) $value->getPrasaranaId(), (string) $value->getSemesterId()));
            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key
                $key = serialize(array((string) $value[0], (string) $value[1]));
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or PrasaranaLongitudinal object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(PrasaranaLongitudinalPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   PrasaranaLongitudinal Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(PrasaranaLongitudinalPeer::$instances[$key])) {
                return PrasaranaLongitudinalPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (PrasaranaLongitudinalPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        PrasaranaLongitudinalPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to prasarana_longitudinal
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null && $row[$startcol + 1] === null) {
            return null;
        }

        return serialize(array((string) $row[$startcol], (string) $row[$startcol + 1]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return array((string) $row[$startcol], (string) $row[$startcol + 1]);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = PrasaranaLongitudinalPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = PrasaranaLongitudinalPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PrasaranaLongitudinalPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (PrasaranaLongitudinal object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = PrasaranaLongitudinalPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PrasaranaLongitudinalPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            PrasaranaLongitudinalPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related Prasarana table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinPrasarana(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Semester table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSemester(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with their Prasarana objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinPrasarana(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;
        PrasaranaPeer::addSelectColumns($criteria);

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to $obj2 (Prasarana)
                $obj2->addPrasaranaLongitudinal($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with their Semester objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSemester(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;
        SemesterPeer::addSelectColumns($criteria);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SemesterPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SemesterPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SemesterPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to $obj2 (Semester)
                $obj2->addPrasaranaLongitudinal($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SemesterPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SemesterPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Prasarana rows

            $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj2 (Prasarana)
                $obj2->addPrasaranaLongitudinal($obj1);
            } // if joined row not null

            // Add objects for joined Semester rows

            $key3 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = SemesterPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = SemesterPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SemesterPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj3 (Semester)
                $obj3->addPrasaranaLongitudinal($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Prasarana table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptPrasarana(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Semester table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSemester(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with all related objects except Prasarana.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptPrasarana(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        SemesterPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SemesterPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaLongitudinalPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Semester rows

                $key2 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SemesterPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SemesterPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SemesterPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj2 (Semester)
                $obj2->addPrasaranaLongitudinal($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of PrasaranaLongitudinal objects pre-filled with all related objects except Semester.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of PrasaranaLongitudinal objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSemester(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);
        }

        PrasaranaLongitudinalPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaLongitudinalPeer::NUM_HYDRATE_COLUMNS;

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaLongitudinalPeer::PRASARANA_ID, PrasaranaPeer::PRASARANA_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaLongitudinalPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaLongitudinalPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaLongitudinalPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaLongitudinalPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Prasarana rows

                $key2 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = PrasaranaPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = PrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    PrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (PrasaranaLongitudinal) to the collection in $obj2 (Prasarana)
                $obj2->addPrasaranaLongitudinal($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(PrasaranaLongitudinalPeer::DATABASE_NAME)->getTable(PrasaranaLongitudinalPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BasePrasaranaLongitudinalPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BasePrasaranaLongitudinalPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new PrasaranaLongitudinalTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return PrasaranaLongitudinalPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a PrasaranaLongitudinal or Criteria object.
     *
     * @param      mixed $values Criteria or PrasaranaLongitudinal object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from PrasaranaLongitudinal object
        }


        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a PrasaranaLongitudinal or Criteria object.
     *
     * @param      mixed $values Criteria or PrasaranaLongitudinal object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(PrasaranaLongitudinalPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(PrasaranaLongitudinalPeer::PRASARANA_ID);
            $value = $criteria->remove(PrasaranaLongitudinalPeer::PRASARANA_ID);
            if ($value) {
                $selectCriteria->add(PrasaranaLongitudinalPeer::PRASARANA_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(PrasaranaLongitudinalPeer::SEMESTER_ID);
            $value = $criteria->remove(PrasaranaLongitudinalPeer::SEMESTER_ID);
            if ($value) {
                $selectCriteria->add(PrasaranaLongitudinalPeer::SEMESTER_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(PrasaranaLongitudinalPeer::TABLE_NAME);
            }

        } else { // $values is PrasaranaLongitudinal object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the prasarana_longitudinal table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(PrasaranaLongitudinalPeer::TABLE_NAME, $con, PrasaranaLongitudinalPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PrasaranaLongitudinalPeer::clearInstancePool();
            PrasaranaLongitudinalPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a PrasaranaLongitudinal or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or PrasaranaLongitudinal object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            PrasaranaLongitudinalPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof PrasaranaLongitudinal) { // it's a model object
            // invalidate the cache for this single object
            PrasaranaLongitudinalPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PrasaranaLongitudinalPeer::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(PrasaranaLongitudinalPeer::PRASARANA_ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(PrasaranaLongitudinalPeer::SEMESTER_ID, $value[1]));
                $criteria->addOr($criterion);
                // we can invalidate the cache for this single PK
                PrasaranaLongitudinalPeer::removeInstanceFromPool($value);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(PrasaranaLongitudinalPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            PrasaranaLongitudinalPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given PrasaranaLongitudinal object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      PrasaranaLongitudinal $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(PrasaranaLongitudinalPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(PrasaranaLongitudinalPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(PrasaranaLongitudinalPeer::DATABASE_NAME, PrasaranaLongitudinalPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve object using using composite pkey values.
     * @param   string $prasarana_id
     * @param   string $semester_id
     * @param      PropelPDO $con
     * @return   PrasaranaLongitudinal
     */
    public static function retrieveByPK($prasarana_id, $semester_id, PropelPDO $con = null) {
        $_instancePoolKey = serialize(array((string) $prasarana_id, (string) $semester_id));
         if (null !== ($obj = PrasaranaLongitudinalPeer::getInstanceFromPool($_instancePoolKey))) {
             return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaLongitudinalPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $criteria = new Criteria(PrasaranaLongitudinalPeer::DATABASE_NAME);
        $criteria->add(PrasaranaLongitudinalPeer::PRASARANA_ID, $prasarana_id);
        $criteria->add(PrasaranaLongitudinalPeer::SEMESTER_ID, $semester_id);
        $v = PrasaranaLongitudinalPeer::doSelect($criteria, $con);

        return !empty($v) ? $v[0] : null;
    }
} // BasePrasaranaLongitudinalPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BasePrasaranaLongitudinalPeer::buildTableMap();

