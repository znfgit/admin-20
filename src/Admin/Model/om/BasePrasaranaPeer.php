<?php

namespace Admin\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use Admin\Model\JenisHapusBukuPeer;
use Admin\Model\JenisPrasaranaPeer;
use Admin\Model\Prasarana;
use Admin\Model\PrasaranaPeer;
use Admin\Model\SekolahPeer;
use Admin\Model\StatusKepemilikanSarprasPeer;
use Admin\Model\map\PrasaranaTableMap;

/**
 * Base static class for performing query and update operations on the 'prasarana' table.
 *
 *
 *
 * @package propel.generator.Admin.Model.om
 */
abstract class BasePrasaranaPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'Dapodik_Paudni';

    /** the table name for this class */
    const TABLE_NAME = 'prasarana';

    /** the related Propel class for this table */
    const OM_CLASS = 'Admin\\Model\\Prasarana';

    /** the related TableMap class for this table */
    const TM_CLASS = 'PrasaranaTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 40;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 40;

    /** the column name for the prasarana_id field */
    const PRASARANA_ID = 'prasarana.prasarana_id';

    /** the column name for the jenis_prasarana_id field */
    const JENIS_PRASARANA_ID = 'prasarana.jenis_prasarana_id';

    /** the column name for the sekolah_id field */
    const SEKOLAH_ID = 'prasarana.sekolah_id';

    /** the column name for the id_induk_prasarana field */
    const ID_INDUK_PRASARANA = 'prasarana.id_induk_prasarana';

    /** the column name for the kepemilikan_sarpras_id field */
    const KEPEMILIKAN_SARPRAS_ID = 'prasarana.kepemilikan_sarpras_id';

    /** the column name for the nama field */
    const NAMA = 'prasarana.nama';

    /** the column name for the panjang field */
    const PANJANG = 'prasarana.panjang';

    /** the column name for the lebar field */
    const LEBAR = 'prasarana.lebar';

    /** the column name for the reg_pras field */
    const REG_PRAS = 'prasarana.reg_pras';

    /** the column name for the ket_prasarana field */
    const KET_PRASARANA = 'prasarana.ket_prasarana';

    /** the column name for the vol_pondasi_m3 field */
    const VOL_PONDASI_M3 = 'prasarana.vol_pondasi_m3';

    /** the column name for the vol_sloop_kolom_balok_m3 field */
    const VOL_SLOOP_KOLOM_BALOK_M3 = 'prasarana.vol_sloop_kolom_balok_m3';

    /** the column name for the luas_plester_m2 field */
    const LUAS_PLESTER_M2 = 'prasarana.luas_plester_m2';

    /** the column name for the panj_kudakuda_m field */
    const PANJ_KUDAKUDA_M = 'prasarana.panj_kudakuda_m';

    /** the column name for the vol_kudakuda_m3 field */
    const VOL_KUDAKUDA_M3 = 'prasarana.vol_kudakuda_m3';

    /** the column name for the panj_kaso_m field */
    const PANJ_KASO_M = 'prasarana.panj_kaso_m';

    /** the column name for the panj_reng_m field */
    const PANJ_RENG_M = 'prasarana.panj_reng_m';

    /** the column name for the luas_tutup_atap_m2 field */
    const LUAS_TUTUP_ATAP_M2 = 'prasarana.luas_tutup_atap_m2';

    /** the column name for the luas_plafon_m2 field */
    const LUAS_PLAFON_M2 = 'prasarana.luas_plafon_m2';

    /** the column name for the luas_dinding_m2 field */
    const LUAS_DINDING_M2 = 'prasarana.luas_dinding_m2';

    /** the column name for the luas_daun_jendela_m2 field */
    const LUAS_DAUN_JENDELA_M2 = 'prasarana.luas_daun_jendela_m2';

    /** the column name for the luas_daun_pintu_m2 field */
    const LUAS_DAUN_PINTU_M2 = 'prasarana.luas_daun_pintu_m2';

    /** the column name for the panj_kusen_m field */
    const PANJ_KUSEN_M = 'prasarana.panj_kusen_m';

    /** the column name for the luas_tutup_lantai_m2 field */
    const LUAS_TUTUP_LANTAI_M2 = 'prasarana.luas_tutup_lantai_m2';

    /** the column name for the panj_inst_listrik_m field */
    const PANJ_INST_LISTRIK_M = 'prasarana.panj_inst_listrik_m';

    /** the column name for the jml_inst_listrik field */
    const JML_INST_LISTRIK = 'prasarana.jml_inst_listrik';

    /** the column name for the panj_inst_air_m field */
    const PANJ_INST_AIR_M = 'prasarana.panj_inst_air_m';

    /** the column name for the jml_inst_air field */
    const JML_INST_AIR = 'prasarana.jml_inst_air';

    /** the column name for the panj_drainase_m field */
    const PANJ_DRAINASE_M = 'prasarana.panj_drainase_m';

    /** the column name for the luas_finish_struktur_m2 field */
    const LUAS_FINISH_STRUKTUR_M2 = 'prasarana.luas_finish_struktur_m2';

    /** the column name for the luas_finish_plafon_m2 field */
    const LUAS_FINISH_PLAFON_M2 = 'prasarana.luas_finish_plafon_m2';

    /** the column name for the luas_finish_dinding_m2 field */
    const LUAS_FINISH_DINDING_M2 = 'prasarana.luas_finish_dinding_m2';

    /** the column name for the luas_finish_kpj_m2 field */
    const LUAS_FINISH_KPJ_M2 = 'prasarana.luas_finish_kpj_m2';

    /** the column name for the id_hapus_buku field */
    const ID_HAPUS_BUKU = 'prasarana.id_hapus_buku';

    /** the column name for the tgl_hapus_buku field */
    const TGL_HAPUS_BUKU = 'prasarana.tgl_hapus_buku';

    /** the column name for the asal_data field */
    const ASAL_DATA = 'prasarana.asal_data';

    /** the column name for the Last_update field */
    const LAST_UPDATE = 'prasarana.Last_update';

    /** the column name for the Soft_delete field */
    const SOFT_DELETE = 'prasarana.Soft_delete';

    /** the column name for the last_sync field */
    const LAST_SYNC = 'prasarana.last_sync';

    /** the column name for the Updater_ID field */
    const UPDATER_ID = 'prasarana.Updater_ID';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of Prasarana objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Prasarana[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. PrasaranaPeer::$fieldNames[PrasaranaPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('PrasaranaId', 'JenisPrasaranaId', 'SekolahId', 'IdIndukPrasarana', 'KepemilikanSarprasId', 'Nama', 'Panjang', 'Lebar', 'RegPras', 'KetPrasarana', 'VolPondasiM3', 'VolSloopKolomBalokM3', 'LuasPlesterM2', 'PanjKudakudaM', 'VolKudakudaM3', 'PanjKasoM', 'PanjRengM', 'LuasTutupAtapM2', 'LuasPlafonM2', 'LuasDindingM2', 'LuasDaunJendelaM2', 'LuasDaunPintuM2', 'PanjKusenM', 'LuasTutupLantaiM2', 'PanjInstListrikM', 'JmlInstListrik', 'PanjInstAirM', 'JmlInstAir', 'PanjDrainaseM', 'LuasFinishStrukturM2', 'LuasFinishPlafonM2', 'LuasFinishDindingM2', 'LuasFinishKpjM2', 'IdHapusBuku', 'TglHapusBuku', 'AsalData', 'LastUpdate', 'SoftDelete', 'LastSync', 'UpdaterId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('prasaranaId', 'jenisPrasaranaId', 'sekolahId', 'idIndukPrasarana', 'kepemilikanSarprasId', 'nama', 'panjang', 'lebar', 'regPras', 'ketPrasarana', 'volPondasiM3', 'volSloopKolomBalokM3', 'luasPlesterM2', 'panjKudakudaM', 'volKudakudaM3', 'panjKasoM', 'panjRengM', 'luasTutupAtapM2', 'luasPlafonM2', 'luasDindingM2', 'luasDaunJendelaM2', 'luasDaunPintuM2', 'panjKusenM', 'luasTutupLantaiM2', 'panjInstListrikM', 'jmlInstListrik', 'panjInstAirM', 'jmlInstAir', 'panjDrainaseM', 'luasFinishStrukturM2', 'luasFinishPlafonM2', 'luasFinishDindingM2', 'luasFinishKpjM2', 'idHapusBuku', 'tglHapusBuku', 'asalData', 'lastUpdate', 'softDelete', 'lastSync', 'updaterId', ),
        BasePeer::TYPE_COLNAME => array (PrasaranaPeer::PRASARANA_ID, PrasaranaPeer::JENIS_PRASARANA_ID, PrasaranaPeer::SEKOLAH_ID, PrasaranaPeer::ID_INDUK_PRASARANA, PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, PrasaranaPeer::NAMA, PrasaranaPeer::PANJANG, PrasaranaPeer::LEBAR, PrasaranaPeer::REG_PRAS, PrasaranaPeer::KET_PRASARANA, PrasaranaPeer::VOL_PONDASI_M3, PrasaranaPeer::VOL_SLOOP_KOLOM_BALOK_M3, PrasaranaPeer::LUAS_PLESTER_M2, PrasaranaPeer::PANJ_KUDAKUDA_M, PrasaranaPeer::VOL_KUDAKUDA_M3, PrasaranaPeer::PANJ_KASO_M, PrasaranaPeer::PANJ_RENG_M, PrasaranaPeer::LUAS_TUTUP_ATAP_M2, PrasaranaPeer::LUAS_PLAFON_M2, PrasaranaPeer::LUAS_DINDING_M2, PrasaranaPeer::LUAS_DAUN_JENDELA_M2, PrasaranaPeer::LUAS_DAUN_PINTU_M2, PrasaranaPeer::PANJ_KUSEN_M, PrasaranaPeer::LUAS_TUTUP_LANTAI_M2, PrasaranaPeer::PANJ_INST_LISTRIK_M, PrasaranaPeer::JML_INST_LISTRIK, PrasaranaPeer::PANJ_INST_AIR_M, PrasaranaPeer::JML_INST_AIR, PrasaranaPeer::PANJ_DRAINASE_M, PrasaranaPeer::LUAS_FINISH_STRUKTUR_M2, PrasaranaPeer::LUAS_FINISH_PLAFON_M2, PrasaranaPeer::LUAS_FINISH_DINDING_M2, PrasaranaPeer::LUAS_FINISH_KPJ_M2, PrasaranaPeer::ID_HAPUS_BUKU, PrasaranaPeer::TGL_HAPUS_BUKU, PrasaranaPeer::ASAL_DATA, PrasaranaPeer::LAST_UPDATE, PrasaranaPeer::SOFT_DELETE, PrasaranaPeer::LAST_SYNC, PrasaranaPeer::UPDATER_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('PRASARANA_ID', 'JENIS_PRASARANA_ID', 'SEKOLAH_ID', 'ID_INDUK_PRASARANA', 'KEPEMILIKAN_SARPRAS_ID', 'NAMA', 'PANJANG', 'LEBAR', 'REG_PRAS', 'KET_PRASARANA', 'VOL_PONDASI_M3', 'VOL_SLOOP_KOLOM_BALOK_M3', 'LUAS_PLESTER_M2', 'PANJ_KUDAKUDA_M', 'VOL_KUDAKUDA_M3', 'PANJ_KASO_M', 'PANJ_RENG_M', 'LUAS_TUTUP_ATAP_M2', 'LUAS_PLAFON_M2', 'LUAS_DINDING_M2', 'LUAS_DAUN_JENDELA_M2', 'LUAS_DAUN_PINTU_M2', 'PANJ_KUSEN_M', 'LUAS_TUTUP_LANTAI_M2', 'PANJ_INST_LISTRIK_M', 'JML_INST_LISTRIK', 'PANJ_INST_AIR_M', 'JML_INST_AIR', 'PANJ_DRAINASE_M', 'LUAS_FINISH_STRUKTUR_M2', 'LUAS_FINISH_PLAFON_M2', 'LUAS_FINISH_DINDING_M2', 'LUAS_FINISH_KPJ_M2', 'ID_HAPUS_BUKU', 'TGL_HAPUS_BUKU', 'ASAL_DATA', 'LAST_UPDATE', 'SOFT_DELETE', 'LAST_SYNC', 'UPDATER_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('prasarana_id', 'jenis_prasarana_id', 'sekolah_id', 'id_induk_prasarana', 'kepemilikan_sarpras_id', 'nama', 'panjang', 'lebar', 'reg_pras', 'ket_prasarana', 'vol_pondasi_m3', 'vol_sloop_kolom_balok_m3', 'luas_plester_m2', 'panj_kudakuda_m', 'vol_kudakuda_m3', 'panj_kaso_m', 'panj_reng_m', 'luas_tutup_atap_m2', 'luas_plafon_m2', 'luas_dinding_m2', 'luas_daun_jendela_m2', 'luas_daun_pintu_m2', 'panj_kusen_m', 'luas_tutup_lantai_m2', 'panj_inst_listrik_m', 'jml_inst_listrik', 'panj_inst_air_m', 'jml_inst_air', 'panj_drainase_m', 'luas_finish_struktur_m2', 'luas_finish_plafon_m2', 'luas_finish_dinding_m2', 'luas_finish_kpj_m2', 'id_hapus_buku', 'tgl_hapus_buku', 'asal_data', 'Last_update', 'Soft_delete', 'last_sync', 'Updater_ID', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. PrasaranaPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('PrasaranaId' => 0, 'JenisPrasaranaId' => 1, 'SekolahId' => 2, 'IdIndukPrasarana' => 3, 'KepemilikanSarprasId' => 4, 'Nama' => 5, 'Panjang' => 6, 'Lebar' => 7, 'RegPras' => 8, 'KetPrasarana' => 9, 'VolPondasiM3' => 10, 'VolSloopKolomBalokM3' => 11, 'LuasPlesterM2' => 12, 'PanjKudakudaM' => 13, 'VolKudakudaM3' => 14, 'PanjKasoM' => 15, 'PanjRengM' => 16, 'LuasTutupAtapM2' => 17, 'LuasPlafonM2' => 18, 'LuasDindingM2' => 19, 'LuasDaunJendelaM2' => 20, 'LuasDaunPintuM2' => 21, 'PanjKusenM' => 22, 'LuasTutupLantaiM2' => 23, 'PanjInstListrikM' => 24, 'JmlInstListrik' => 25, 'PanjInstAirM' => 26, 'JmlInstAir' => 27, 'PanjDrainaseM' => 28, 'LuasFinishStrukturM2' => 29, 'LuasFinishPlafonM2' => 30, 'LuasFinishDindingM2' => 31, 'LuasFinishKpjM2' => 32, 'IdHapusBuku' => 33, 'TglHapusBuku' => 34, 'AsalData' => 35, 'LastUpdate' => 36, 'SoftDelete' => 37, 'LastSync' => 38, 'UpdaterId' => 39, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('prasaranaId' => 0, 'jenisPrasaranaId' => 1, 'sekolahId' => 2, 'idIndukPrasarana' => 3, 'kepemilikanSarprasId' => 4, 'nama' => 5, 'panjang' => 6, 'lebar' => 7, 'regPras' => 8, 'ketPrasarana' => 9, 'volPondasiM3' => 10, 'volSloopKolomBalokM3' => 11, 'luasPlesterM2' => 12, 'panjKudakudaM' => 13, 'volKudakudaM3' => 14, 'panjKasoM' => 15, 'panjRengM' => 16, 'luasTutupAtapM2' => 17, 'luasPlafonM2' => 18, 'luasDindingM2' => 19, 'luasDaunJendelaM2' => 20, 'luasDaunPintuM2' => 21, 'panjKusenM' => 22, 'luasTutupLantaiM2' => 23, 'panjInstListrikM' => 24, 'jmlInstListrik' => 25, 'panjInstAirM' => 26, 'jmlInstAir' => 27, 'panjDrainaseM' => 28, 'luasFinishStrukturM2' => 29, 'luasFinishPlafonM2' => 30, 'luasFinishDindingM2' => 31, 'luasFinishKpjM2' => 32, 'idHapusBuku' => 33, 'tglHapusBuku' => 34, 'asalData' => 35, 'lastUpdate' => 36, 'softDelete' => 37, 'lastSync' => 38, 'updaterId' => 39, ),
        BasePeer::TYPE_COLNAME => array (PrasaranaPeer::PRASARANA_ID => 0, PrasaranaPeer::JENIS_PRASARANA_ID => 1, PrasaranaPeer::SEKOLAH_ID => 2, PrasaranaPeer::ID_INDUK_PRASARANA => 3, PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID => 4, PrasaranaPeer::NAMA => 5, PrasaranaPeer::PANJANG => 6, PrasaranaPeer::LEBAR => 7, PrasaranaPeer::REG_PRAS => 8, PrasaranaPeer::KET_PRASARANA => 9, PrasaranaPeer::VOL_PONDASI_M3 => 10, PrasaranaPeer::VOL_SLOOP_KOLOM_BALOK_M3 => 11, PrasaranaPeer::LUAS_PLESTER_M2 => 12, PrasaranaPeer::PANJ_KUDAKUDA_M => 13, PrasaranaPeer::VOL_KUDAKUDA_M3 => 14, PrasaranaPeer::PANJ_KASO_M => 15, PrasaranaPeer::PANJ_RENG_M => 16, PrasaranaPeer::LUAS_TUTUP_ATAP_M2 => 17, PrasaranaPeer::LUAS_PLAFON_M2 => 18, PrasaranaPeer::LUAS_DINDING_M2 => 19, PrasaranaPeer::LUAS_DAUN_JENDELA_M2 => 20, PrasaranaPeer::LUAS_DAUN_PINTU_M2 => 21, PrasaranaPeer::PANJ_KUSEN_M => 22, PrasaranaPeer::LUAS_TUTUP_LANTAI_M2 => 23, PrasaranaPeer::PANJ_INST_LISTRIK_M => 24, PrasaranaPeer::JML_INST_LISTRIK => 25, PrasaranaPeer::PANJ_INST_AIR_M => 26, PrasaranaPeer::JML_INST_AIR => 27, PrasaranaPeer::PANJ_DRAINASE_M => 28, PrasaranaPeer::LUAS_FINISH_STRUKTUR_M2 => 29, PrasaranaPeer::LUAS_FINISH_PLAFON_M2 => 30, PrasaranaPeer::LUAS_FINISH_DINDING_M2 => 31, PrasaranaPeer::LUAS_FINISH_KPJ_M2 => 32, PrasaranaPeer::ID_HAPUS_BUKU => 33, PrasaranaPeer::TGL_HAPUS_BUKU => 34, PrasaranaPeer::ASAL_DATA => 35, PrasaranaPeer::LAST_UPDATE => 36, PrasaranaPeer::SOFT_DELETE => 37, PrasaranaPeer::LAST_SYNC => 38, PrasaranaPeer::UPDATER_ID => 39, ),
        BasePeer::TYPE_RAW_COLNAME => array ('PRASARANA_ID' => 0, 'JENIS_PRASARANA_ID' => 1, 'SEKOLAH_ID' => 2, 'ID_INDUK_PRASARANA' => 3, 'KEPEMILIKAN_SARPRAS_ID' => 4, 'NAMA' => 5, 'PANJANG' => 6, 'LEBAR' => 7, 'REG_PRAS' => 8, 'KET_PRASARANA' => 9, 'VOL_PONDASI_M3' => 10, 'VOL_SLOOP_KOLOM_BALOK_M3' => 11, 'LUAS_PLESTER_M2' => 12, 'PANJ_KUDAKUDA_M' => 13, 'VOL_KUDAKUDA_M3' => 14, 'PANJ_KASO_M' => 15, 'PANJ_RENG_M' => 16, 'LUAS_TUTUP_ATAP_M2' => 17, 'LUAS_PLAFON_M2' => 18, 'LUAS_DINDING_M2' => 19, 'LUAS_DAUN_JENDELA_M2' => 20, 'LUAS_DAUN_PINTU_M2' => 21, 'PANJ_KUSEN_M' => 22, 'LUAS_TUTUP_LANTAI_M2' => 23, 'PANJ_INST_LISTRIK_M' => 24, 'JML_INST_LISTRIK' => 25, 'PANJ_INST_AIR_M' => 26, 'JML_INST_AIR' => 27, 'PANJ_DRAINASE_M' => 28, 'LUAS_FINISH_STRUKTUR_M2' => 29, 'LUAS_FINISH_PLAFON_M2' => 30, 'LUAS_FINISH_DINDING_M2' => 31, 'LUAS_FINISH_KPJ_M2' => 32, 'ID_HAPUS_BUKU' => 33, 'TGL_HAPUS_BUKU' => 34, 'ASAL_DATA' => 35, 'LAST_UPDATE' => 36, 'SOFT_DELETE' => 37, 'LAST_SYNC' => 38, 'UPDATER_ID' => 39, ),
        BasePeer::TYPE_FIELDNAME => array ('prasarana_id' => 0, 'jenis_prasarana_id' => 1, 'sekolah_id' => 2, 'id_induk_prasarana' => 3, 'kepemilikan_sarpras_id' => 4, 'nama' => 5, 'panjang' => 6, 'lebar' => 7, 'reg_pras' => 8, 'ket_prasarana' => 9, 'vol_pondasi_m3' => 10, 'vol_sloop_kolom_balok_m3' => 11, 'luas_plester_m2' => 12, 'panj_kudakuda_m' => 13, 'vol_kudakuda_m3' => 14, 'panj_kaso_m' => 15, 'panj_reng_m' => 16, 'luas_tutup_atap_m2' => 17, 'luas_plafon_m2' => 18, 'luas_dinding_m2' => 19, 'luas_daun_jendela_m2' => 20, 'luas_daun_pintu_m2' => 21, 'panj_kusen_m' => 22, 'luas_tutup_lantai_m2' => 23, 'panj_inst_listrik_m' => 24, 'jml_inst_listrik' => 25, 'panj_inst_air_m' => 26, 'jml_inst_air' => 27, 'panj_drainase_m' => 28, 'luas_finish_struktur_m2' => 29, 'luas_finish_plafon_m2' => 30, 'luas_finish_dinding_m2' => 31, 'luas_finish_kpj_m2' => 32, 'id_hapus_buku' => 33, 'tgl_hapus_buku' => 34, 'asal_data' => 35, 'Last_update' => 36, 'Soft_delete' => 37, 'last_sync' => 38, 'Updater_ID' => 39, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = PrasaranaPeer::getFieldNames($toType);
        $key = isset(PrasaranaPeer::$fieldKeys[$fromType][$name]) ? PrasaranaPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(PrasaranaPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, PrasaranaPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return PrasaranaPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. PrasaranaPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(PrasaranaPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PrasaranaPeer::PRASARANA_ID);
            $criteria->addSelectColumn(PrasaranaPeer::JENIS_PRASARANA_ID);
            $criteria->addSelectColumn(PrasaranaPeer::SEKOLAH_ID);
            $criteria->addSelectColumn(PrasaranaPeer::ID_INDUK_PRASARANA);
            $criteria->addSelectColumn(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID);
            $criteria->addSelectColumn(PrasaranaPeer::NAMA);
            $criteria->addSelectColumn(PrasaranaPeer::PANJANG);
            $criteria->addSelectColumn(PrasaranaPeer::LEBAR);
            $criteria->addSelectColumn(PrasaranaPeer::REG_PRAS);
            $criteria->addSelectColumn(PrasaranaPeer::KET_PRASARANA);
            $criteria->addSelectColumn(PrasaranaPeer::VOL_PONDASI_M3);
            $criteria->addSelectColumn(PrasaranaPeer::VOL_SLOOP_KOLOM_BALOK_M3);
            $criteria->addSelectColumn(PrasaranaPeer::LUAS_PLESTER_M2);
            $criteria->addSelectColumn(PrasaranaPeer::PANJ_KUDAKUDA_M);
            $criteria->addSelectColumn(PrasaranaPeer::VOL_KUDAKUDA_M3);
            $criteria->addSelectColumn(PrasaranaPeer::PANJ_KASO_M);
            $criteria->addSelectColumn(PrasaranaPeer::PANJ_RENG_M);
            $criteria->addSelectColumn(PrasaranaPeer::LUAS_TUTUP_ATAP_M2);
            $criteria->addSelectColumn(PrasaranaPeer::LUAS_PLAFON_M2);
            $criteria->addSelectColumn(PrasaranaPeer::LUAS_DINDING_M2);
            $criteria->addSelectColumn(PrasaranaPeer::LUAS_DAUN_JENDELA_M2);
            $criteria->addSelectColumn(PrasaranaPeer::LUAS_DAUN_PINTU_M2);
            $criteria->addSelectColumn(PrasaranaPeer::PANJ_KUSEN_M);
            $criteria->addSelectColumn(PrasaranaPeer::LUAS_TUTUP_LANTAI_M2);
            $criteria->addSelectColumn(PrasaranaPeer::PANJ_INST_LISTRIK_M);
            $criteria->addSelectColumn(PrasaranaPeer::JML_INST_LISTRIK);
            $criteria->addSelectColumn(PrasaranaPeer::PANJ_INST_AIR_M);
            $criteria->addSelectColumn(PrasaranaPeer::JML_INST_AIR);
            $criteria->addSelectColumn(PrasaranaPeer::PANJ_DRAINASE_M);
            $criteria->addSelectColumn(PrasaranaPeer::LUAS_FINISH_STRUKTUR_M2);
            $criteria->addSelectColumn(PrasaranaPeer::LUAS_FINISH_PLAFON_M2);
            $criteria->addSelectColumn(PrasaranaPeer::LUAS_FINISH_DINDING_M2);
            $criteria->addSelectColumn(PrasaranaPeer::LUAS_FINISH_KPJ_M2);
            $criteria->addSelectColumn(PrasaranaPeer::ID_HAPUS_BUKU);
            $criteria->addSelectColumn(PrasaranaPeer::TGL_HAPUS_BUKU);
            $criteria->addSelectColumn(PrasaranaPeer::ASAL_DATA);
            $criteria->addSelectColumn(PrasaranaPeer::LAST_UPDATE);
            $criteria->addSelectColumn(PrasaranaPeer::SOFT_DELETE);
            $criteria->addSelectColumn(PrasaranaPeer::LAST_SYNC);
            $criteria->addSelectColumn(PrasaranaPeer::UPDATER_ID);
        } else {
            $criteria->addSelectColumn($alias . '.prasarana_id');
            $criteria->addSelectColumn($alias . '.jenis_prasarana_id');
            $criteria->addSelectColumn($alias . '.sekolah_id');
            $criteria->addSelectColumn($alias . '.id_induk_prasarana');
            $criteria->addSelectColumn($alias . '.kepemilikan_sarpras_id');
            $criteria->addSelectColumn($alias . '.nama');
            $criteria->addSelectColumn($alias . '.panjang');
            $criteria->addSelectColumn($alias . '.lebar');
            $criteria->addSelectColumn($alias . '.reg_pras');
            $criteria->addSelectColumn($alias . '.ket_prasarana');
            $criteria->addSelectColumn($alias . '.vol_pondasi_m3');
            $criteria->addSelectColumn($alias . '.vol_sloop_kolom_balok_m3');
            $criteria->addSelectColumn($alias . '.luas_plester_m2');
            $criteria->addSelectColumn($alias . '.panj_kudakuda_m');
            $criteria->addSelectColumn($alias . '.vol_kudakuda_m3');
            $criteria->addSelectColumn($alias . '.panj_kaso_m');
            $criteria->addSelectColumn($alias . '.panj_reng_m');
            $criteria->addSelectColumn($alias . '.luas_tutup_atap_m2');
            $criteria->addSelectColumn($alias . '.luas_plafon_m2');
            $criteria->addSelectColumn($alias . '.luas_dinding_m2');
            $criteria->addSelectColumn($alias . '.luas_daun_jendela_m2');
            $criteria->addSelectColumn($alias . '.luas_daun_pintu_m2');
            $criteria->addSelectColumn($alias . '.panj_kusen_m');
            $criteria->addSelectColumn($alias . '.luas_tutup_lantai_m2');
            $criteria->addSelectColumn($alias . '.panj_inst_listrik_m');
            $criteria->addSelectColumn($alias . '.jml_inst_listrik');
            $criteria->addSelectColumn($alias . '.panj_inst_air_m');
            $criteria->addSelectColumn($alias . '.jml_inst_air');
            $criteria->addSelectColumn($alias . '.panj_drainase_m');
            $criteria->addSelectColumn($alias . '.luas_finish_struktur_m2');
            $criteria->addSelectColumn($alias . '.luas_finish_plafon_m2');
            $criteria->addSelectColumn($alias . '.luas_finish_dinding_m2');
            $criteria->addSelectColumn($alias . '.luas_finish_kpj_m2');
            $criteria->addSelectColumn($alias . '.id_hapus_buku');
            $criteria->addSelectColumn($alias . '.tgl_hapus_buku');
            $criteria->addSelectColumn($alias . '.asal_data');
            $criteria->addSelectColumn($alias . '.Last_update');
            $criteria->addSelectColumn($alias . '.Soft_delete');
            $criteria->addSelectColumn($alias . '.last_sync');
            $criteria->addSelectColumn($alias . '.Updater_ID');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 Prasarana
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = PrasaranaPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return PrasaranaPeer::populateObjects(PrasaranaPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            PrasaranaPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      Prasarana $obj A Prasarana object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getPrasaranaId();
            } // if key === null
            PrasaranaPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Prasarana object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Prasarana) {
                $key = (string) $value->getPrasaranaId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Prasarana object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(PrasaranaPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   Prasarana Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(PrasaranaPeer::$instances[$key])) {
                return PrasaranaPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (PrasaranaPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        PrasaranaPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to prasarana
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (string) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = PrasaranaPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = PrasaranaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = PrasaranaPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PrasaranaPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Prasarana object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = PrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = PrasaranaPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + PrasaranaPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PrasaranaPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            PrasaranaPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related Sekolah table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSekolah(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related JenisHapusBuku table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinJenisHapusBuku(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaPeer::ID_HAPUS_BUKU, JenisHapusBukuPeer::ID_HAPUS_BUKU, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related JenisPrasarana table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinJenisPrasarana(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaPeer::JENIS_PRASARANA_ID, JenisPrasaranaPeer::JENIS_PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related StatusKepemilikanSarpras table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinStatusKepemilikanSarpras(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of Prasarana objects pre-filled with their Sekolah objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Prasarana objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSekolah(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);
        }

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol = PrasaranaPeer::NUM_HYDRATE_COLUMNS;
        SekolahPeer::addSelectColumns($criteria);

        $criteria->addJoin(PrasaranaPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PrasaranaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SekolahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Prasarana) to $obj2 (Sekolah)
                $obj2->addPrasarana($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Prasarana objects pre-filled with their JenisHapusBuku objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Prasarana objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinJenisHapusBuku(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);
        }

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol = PrasaranaPeer::NUM_HYDRATE_COLUMNS;
        JenisHapusBukuPeer::addSelectColumns($criteria);

        $criteria->addJoin(PrasaranaPeer::ID_HAPUS_BUKU, JenisHapusBukuPeer::ID_HAPUS_BUKU, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PrasaranaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = JenisHapusBukuPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = JenisHapusBukuPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = JenisHapusBukuPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    JenisHapusBukuPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Prasarana) to $obj2 (JenisHapusBuku)
                $obj2->addPrasarana($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Prasarana objects pre-filled with their JenisPrasarana objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Prasarana objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinJenisPrasarana(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);
        }

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol = PrasaranaPeer::NUM_HYDRATE_COLUMNS;
        JenisPrasaranaPeer::addSelectColumns($criteria);

        $criteria->addJoin(PrasaranaPeer::JENIS_PRASARANA_ID, JenisPrasaranaPeer::JENIS_PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PrasaranaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = JenisPrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = JenisPrasaranaPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = JenisPrasaranaPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    JenisPrasaranaPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Prasarana) to $obj2 (JenisPrasarana)
                $obj2->addPrasarana($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Prasarana objects pre-filled with their StatusKepemilikanSarpras objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Prasarana objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinStatusKepemilikanSarpras(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);
        }

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol = PrasaranaPeer::NUM_HYDRATE_COLUMNS;
        StatusKepemilikanSarprasPeer::addSelectColumns($criteria);

        $criteria->addJoin(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = PrasaranaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = StatusKepemilikanSarprasPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = StatusKepemilikanSarprasPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = StatusKepemilikanSarprasPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    StatusKepemilikanSarprasPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Prasarana) to $obj2 (StatusKepemilikanSarpras)
                $obj2->addPrasarana($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::ID_HAPUS_BUKU, JenisHapusBukuPeer::ID_HAPUS_BUKU, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::JENIS_PRASARANA_ID, JenisPrasaranaPeer::JENIS_PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of Prasarana objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Prasarana objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);
        }

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        JenisHapusBukuPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + JenisHapusBukuPeer::NUM_HYDRATE_COLUMNS;

        JenisPrasaranaPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + JenisPrasaranaPeer::NUM_HYDRATE_COLUMNS;

        StatusKepemilikanSarprasPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + StatusKepemilikanSarprasPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::ID_HAPUS_BUKU, JenisHapusBukuPeer::ID_HAPUS_BUKU, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::JENIS_PRASARANA_ID, JenisPrasaranaPeer::JENIS_PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Sekolah rows

            $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = SekolahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (Prasarana) to the collection in $obj2 (Sekolah)
                $obj2->addPrasarana($obj1);
            } // if joined row not null

            // Add objects for joined JenisHapusBuku rows

            $key3 = JenisHapusBukuPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = JenisHapusBukuPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = JenisHapusBukuPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    JenisHapusBukuPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (Prasarana) to the collection in $obj3 (JenisHapusBuku)
                $obj3->addPrasarana($obj1);
            } // if joined row not null

            // Add objects for joined JenisPrasarana rows

            $key4 = JenisPrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = JenisPrasaranaPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = JenisPrasaranaPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    JenisPrasaranaPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (Prasarana) to the collection in $obj4 (JenisPrasarana)
                $obj4->addPrasarana($obj1);
            } // if joined row not null

            // Add objects for joined StatusKepemilikanSarpras rows

            $key5 = StatusKepemilikanSarprasPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = StatusKepemilikanSarprasPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = StatusKepemilikanSarprasPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    StatusKepemilikanSarprasPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (Prasarana) to the collection in $obj5 (StatusKepemilikanSarpras)
                $obj5->addPrasarana($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related PrasaranaRelatedByIdIndukPrasarana table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptPrasaranaRelatedByIdIndukPrasarana(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::ID_HAPUS_BUKU, JenisHapusBukuPeer::ID_HAPUS_BUKU, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::JENIS_PRASARANA_ID, JenisPrasaranaPeer::JENIS_PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Sekolah table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSekolah(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaPeer::ID_HAPUS_BUKU, JenisHapusBukuPeer::ID_HAPUS_BUKU, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::JENIS_PRASARANA_ID, JenisPrasaranaPeer::JENIS_PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related JenisHapusBuku table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptJenisHapusBuku(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::JENIS_PRASARANA_ID, JenisPrasaranaPeer::JENIS_PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related JenisPrasarana table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptJenisPrasarana(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::ID_HAPUS_BUKU, JenisHapusBukuPeer::ID_HAPUS_BUKU, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related StatusKepemilikanSarpras table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptStatusKepemilikanSarpras(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(PrasaranaPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            PrasaranaPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(PrasaranaPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::ID_HAPUS_BUKU, JenisHapusBukuPeer::ID_HAPUS_BUKU, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::JENIS_PRASARANA_ID, JenisPrasaranaPeer::JENIS_PRASARANA_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of Prasarana objects pre-filled with all related objects except PrasaranaRelatedByIdIndukPrasarana.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Prasarana objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptPrasaranaRelatedByIdIndukPrasarana(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);
        }

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        JenisHapusBukuPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + JenisHapusBukuPeer::NUM_HYDRATE_COLUMNS;

        JenisPrasaranaPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + JenisPrasaranaPeer::NUM_HYDRATE_COLUMNS;

        StatusKepemilikanSarprasPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + StatusKepemilikanSarprasPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::ID_HAPUS_BUKU, JenisHapusBukuPeer::ID_HAPUS_BUKU, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::JENIS_PRASARANA_ID, JenisPrasaranaPeer::JENIS_PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj2 (Sekolah)
                $obj2->addPrasarana($obj1);

            } // if joined row is not null

                // Add objects for joined JenisHapusBuku rows

                $key3 = JenisHapusBukuPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = JenisHapusBukuPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = JenisHapusBukuPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    JenisHapusBukuPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj3 (JenisHapusBuku)
                $obj3->addPrasarana($obj1);

            } // if joined row is not null

                // Add objects for joined JenisPrasarana rows

                $key4 = JenisPrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = JenisPrasaranaPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = JenisPrasaranaPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    JenisPrasaranaPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj4 (JenisPrasarana)
                $obj4->addPrasarana($obj1);

            } // if joined row is not null

                // Add objects for joined StatusKepemilikanSarpras rows

                $key5 = StatusKepemilikanSarprasPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = StatusKepemilikanSarprasPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = StatusKepemilikanSarprasPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    StatusKepemilikanSarprasPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj5 (StatusKepemilikanSarpras)
                $obj5->addPrasarana($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Prasarana objects pre-filled with all related objects except Sekolah.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Prasarana objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSekolah(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);
        }

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        JenisHapusBukuPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + JenisHapusBukuPeer::NUM_HYDRATE_COLUMNS;

        JenisPrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + JenisPrasaranaPeer::NUM_HYDRATE_COLUMNS;

        StatusKepemilikanSarprasPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + StatusKepemilikanSarprasPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaPeer::ID_HAPUS_BUKU, JenisHapusBukuPeer::ID_HAPUS_BUKU, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::JENIS_PRASARANA_ID, JenisPrasaranaPeer::JENIS_PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined JenisHapusBuku rows

                $key2 = JenisHapusBukuPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = JenisHapusBukuPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = JenisHapusBukuPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    JenisHapusBukuPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj2 (JenisHapusBuku)
                $obj2->addPrasarana($obj1);

            } // if joined row is not null

                // Add objects for joined JenisPrasarana rows

                $key3 = JenisPrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = JenisPrasaranaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = JenisPrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    JenisPrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj3 (JenisPrasarana)
                $obj3->addPrasarana($obj1);

            } // if joined row is not null

                // Add objects for joined StatusKepemilikanSarpras rows

                $key4 = StatusKepemilikanSarprasPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = StatusKepemilikanSarprasPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = StatusKepemilikanSarprasPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    StatusKepemilikanSarprasPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj4 (StatusKepemilikanSarpras)
                $obj4->addPrasarana($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Prasarana objects pre-filled with all related objects except JenisHapusBuku.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Prasarana objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptJenisHapusBuku(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);
        }

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        JenisPrasaranaPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + JenisPrasaranaPeer::NUM_HYDRATE_COLUMNS;

        StatusKepemilikanSarprasPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + StatusKepemilikanSarprasPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::JENIS_PRASARANA_ID, JenisPrasaranaPeer::JENIS_PRASARANA_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj2 (Sekolah)
                $obj2->addPrasarana($obj1);

            } // if joined row is not null

                // Add objects for joined JenisPrasarana rows

                $key3 = JenisPrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = JenisPrasaranaPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = JenisPrasaranaPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    JenisPrasaranaPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj3 (JenisPrasarana)
                $obj3->addPrasarana($obj1);

            } // if joined row is not null

                // Add objects for joined StatusKepemilikanSarpras rows

                $key4 = StatusKepemilikanSarprasPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = StatusKepemilikanSarprasPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = StatusKepemilikanSarprasPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    StatusKepemilikanSarprasPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj4 (StatusKepemilikanSarpras)
                $obj4->addPrasarana($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Prasarana objects pre-filled with all related objects except JenisPrasarana.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Prasarana objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptJenisPrasarana(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);
        }

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        JenisHapusBukuPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + JenisHapusBukuPeer::NUM_HYDRATE_COLUMNS;

        StatusKepemilikanSarprasPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + StatusKepemilikanSarprasPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::ID_HAPUS_BUKU, JenisHapusBukuPeer::ID_HAPUS_BUKU, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::KEPEMILIKAN_SARPRAS_ID, StatusKepemilikanSarprasPeer::KEPEMILIKAN_SARPRAS_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj2 (Sekolah)
                $obj2->addPrasarana($obj1);

            } // if joined row is not null

                // Add objects for joined JenisHapusBuku rows

                $key3 = JenisHapusBukuPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = JenisHapusBukuPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = JenisHapusBukuPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    JenisHapusBukuPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj3 (JenisHapusBuku)
                $obj3->addPrasarana($obj1);

            } // if joined row is not null

                // Add objects for joined StatusKepemilikanSarpras rows

                $key4 = StatusKepemilikanSarprasPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = StatusKepemilikanSarprasPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = StatusKepemilikanSarprasPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    StatusKepemilikanSarprasPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj4 (StatusKepemilikanSarpras)
                $obj4->addPrasarana($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Prasarana objects pre-filled with all related objects except StatusKepemilikanSarpras.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Prasarana objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptStatusKepemilikanSarpras(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);
        }

        PrasaranaPeer::addSelectColumns($criteria);
        $startcol2 = PrasaranaPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        JenisHapusBukuPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + JenisHapusBukuPeer::NUM_HYDRATE_COLUMNS;

        JenisPrasaranaPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + JenisPrasaranaPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(PrasaranaPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::ID_HAPUS_BUKU, JenisHapusBukuPeer::ID_HAPUS_BUKU, $join_behavior);

        $criteria->addJoin(PrasaranaPeer::JENIS_PRASARANA_ID, JenisPrasaranaPeer::JENIS_PRASARANA_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = PrasaranaPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = PrasaranaPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = PrasaranaPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                PrasaranaPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj2 (Sekolah)
                $obj2->addPrasarana($obj1);

            } // if joined row is not null

                // Add objects for joined JenisHapusBuku rows

                $key3 = JenisHapusBukuPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = JenisHapusBukuPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = JenisHapusBukuPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    JenisHapusBukuPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj3 (JenisHapusBuku)
                $obj3->addPrasarana($obj1);

            } // if joined row is not null

                // Add objects for joined JenisPrasarana rows

                $key4 = JenisPrasaranaPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = JenisPrasaranaPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = JenisPrasaranaPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    JenisPrasaranaPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (Prasarana) to the collection in $obj4 (JenisPrasarana)
                $obj4->addPrasarana($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(PrasaranaPeer::DATABASE_NAME)->getTable(PrasaranaPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BasePrasaranaPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BasePrasaranaPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new PrasaranaTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return PrasaranaPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Prasarana or Criteria object.
     *
     * @param      mixed $values Criteria or Prasarana object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Prasarana object
        }


        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Prasarana or Criteria object.
     *
     * @param      mixed $values Criteria or Prasarana object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(PrasaranaPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(PrasaranaPeer::PRASARANA_ID);
            $value = $criteria->remove(PrasaranaPeer::PRASARANA_ID);
            if ($value) {
                $selectCriteria->add(PrasaranaPeer::PRASARANA_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(PrasaranaPeer::TABLE_NAME);
            }

        } else { // $values is Prasarana object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the prasarana table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(PrasaranaPeer::TABLE_NAME, $con, PrasaranaPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PrasaranaPeer::clearInstancePool();
            PrasaranaPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Prasarana or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Prasarana object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            PrasaranaPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Prasarana) { // it's a model object
            // invalidate the cache for this single object
            PrasaranaPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PrasaranaPeer::DATABASE_NAME);
            $criteria->add(PrasaranaPeer::PRASARANA_ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                PrasaranaPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(PrasaranaPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            PrasaranaPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Prasarana object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      Prasarana $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(PrasaranaPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(PrasaranaPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(PrasaranaPeer::DATABASE_NAME, PrasaranaPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      string $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Prasarana
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = PrasaranaPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(PrasaranaPeer::DATABASE_NAME);
        $criteria->add(PrasaranaPeer::PRASARANA_ID, $pk);

        $v = PrasaranaPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Prasarana[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(PrasaranaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(PrasaranaPeer::DATABASE_NAME);
            $criteria->add(PrasaranaPeer::PRASARANA_ID, $pks, Criteria::IN);
            $objs = PrasaranaPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BasePrasaranaPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BasePrasaranaPeer::buildTableMap();

