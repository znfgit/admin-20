<?php

namespace Admin\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use Admin\Model\Sanitasi;
use Admin\Model\SanitasiPeer;
use Admin\Model\SekolahPeer;
use Admin\Model\SemesterPeer;
use Admin\Model\SumberAirPeer;
use Admin\Model\map\SanitasiTableMap;

/**
 * Base static class for performing query and update operations on the 'sanitasi' table.
 *
 *
 *
 * @package propel.generator.Admin.Model.om
 */
abstract class BaseSanitasiPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'Dapodik_Paudni';

    /** the table name for this class */
    const TABLE_NAME = 'sanitasi';

    /** the related Propel class for this table */
    const OM_CLASS = 'Admin\\Model\\Sanitasi';

    /** the related TableMap class for this table */
    const TM_CLASS = 'SanitasiTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 25;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 25;

    /** the column name for the sekolah_id field */
    const SEKOLAH_ID = 'sanitasi.sekolah_id';

    /** the column name for the semester_id field */
    const SEMESTER_ID = 'sanitasi.semester_id';

    /** the column name for the sumber_air_id field */
    const SUMBER_AIR_ID = 'sanitasi.sumber_air_id';

    /** the column name for the ketersediaan_air field */
    const KETERSEDIAAN_AIR = 'sanitasi.ketersediaan_air';

    /** the column name for the kecukupan_air field */
    const KECUKUPAN_AIR = 'sanitasi.kecukupan_air';

    /** the column name for the minum_siswa field */
    const MINUM_SISWA = 'sanitasi.minum_siswa';

    /** the column name for the memproses_air field */
    const MEMPROSES_AIR = 'sanitasi.memproses_air';

    /** the column name for the siswa_bawa_air field */
    const SISWA_BAWA_AIR = 'sanitasi.siswa_bawa_air';

    /** the column name for the toilet_siswa_laki field */
    const TOILET_SISWA_LAKI = 'sanitasi.toilet_siswa_laki';

    /** the column name for the toilet_siswa_perempuan field */
    const TOILET_SISWA_PEREMPUAN = 'sanitasi.toilet_siswa_perempuan';

    /** the column name for the toilet_siswa_kk field */
    const TOILET_SISWA_KK = 'sanitasi.toilet_siswa_kk';

    /** the column name for the toilet_siswa_kecil field */
    const TOILET_SISWA_KECIL = 'sanitasi.toilet_siswa_kecil';

    /** the column name for the tipe_jamban field */
    const TIPE_JAMBAN = 'sanitasi.tipe_jamban';

    /** the column name for the jml_jamban_l_g field */
    const JML_JAMBAN_L_G = 'sanitasi.jml_jamban_l_g';

    /** the column name for the jml_jamban_l_tg field */
    const JML_JAMBAN_L_TG = 'sanitasi.jml_jamban_l_tg';

    /** the column name for the jml_jamban_p_g field */
    const JML_JAMBAN_P_G = 'sanitasi.jml_jamban_p_g';

    /** the column name for the jml_jamban_p_tg field */
    const JML_JAMBAN_P_TG = 'sanitasi.jml_jamban_p_tg';

    /** the column name for the jml_jamban_lp_g field */
    const JML_JAMBAN_LP_G = 'sanitasi.jml_jamban_lp_g';

    /** the column name for the jml_jamban_lp_tg field */
    const JML_JAMBAN_LP_TG = 'sanitasi.jml_jamban_lp_tg';

    /** the column name for the tempat_cuci_tangan field */
    const TEMPAT_CUCI_TANGAN = 'sanitasi.tempat_cuci_tangan';

    /** the column name for the a_sabun_air_mengalir field */
    const A_SABUN_AIR_MENGALIR = 'sanitasi.a_sabun_air_mengalir';

    /** the column name for the Last_update field */
    const LAST_UPDATE = 'sanitasi.Last_update';

    /** the column name for the Soft_delete field */
    const SOFT_DELETE = 'sanitasi.Soft_delete';

    /** the column name for the last_sync field */
    const LAST_SYNC = 'sanitasi.last_sync';

    /** the column name for the Updater_ID field */
    const UPDATER_ID = 'sanitasi.Updater_ID';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of Sanitasi objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Sanitasi[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. SanitasiPeer::$fieldNames[SanitasiPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('SekolahId', 'SemesterId', 'SumberAirId', 'KetersediaanAir', 'KecukupanAir', 'MinumSiswa', 'MemprosesAir', 'SiswaBawaAir', 'ToiletSiswaLaki', 'ToiletSiswaPerempuan', 'ToiletSiswaKk', 'ToiletSiswaKecil', 'TipeJamban', 'JmlJambanLG', 'JmlJambanLTg', 'JmlJambanPG', 'JmlJambanPTg', 'JmlJambanLpG', 'JmlJambanLpTg', 'TempatCuciTangan', 'ASabunAirMengalir', 'LastUpdate', 'SoftDelete', 'LastSync', 'UpdaterId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('sekolahId', 'semesterId', 'sumberAirId', 'ketersediaanAir', 'kecukupanAir', 'minumSiswa', 'memprosesAir', 'siswaBawaAir', 'toiletSiswaLaki', 'toiletSiswaPerempuan', 'toiletSiswaKk', 'toiletSiswaKecil', 'tipeJamban', 'jmlJambanLG', 'jmlJambanLTg', 'jmlJambanPG', 'jmlJambanPTg', 'jmlJambanLpG', 'jmlJambanLpTg', 'tempatCuciTangan', 'aSabunAirMengalir', 'lastUpdate', 'softDelete', 'lastSync', 'updaterId', ),
        BasePeer::TYPE_COLNAME => array (SanitasiPeer::SEKOLAH_ID, SanitasiPeer::SEMESTER_ID, SanitasiPeer::SUMBER_AIR_ID, SanitasiPeer::KETERSEDIAAN_AIR, SanitasiPeer::KECUKUPAN_AIR, SanitasiPeer::MINUM_SISWA, SanitasiPeer::MEMPROSES_AIR, SanitasiPeer::SISWA_BAWA_AIR, SanitasiPeer::TOILET_SISWA_LAKI, SanitasiPeer::TOILET_SISWA_PEREMPUAN, SanitasiPeer::TOILET_SISWA_KK, SanitasiPeer::TOILET_SISWA_KECIL, SanitasiPeer::TIPE_JAMBAN, SanitasiPeer::JML_JAMBAN_L_G, SanitasiPeer::JML_JAMBAN_L_TG, SanitasiPeer::JML_JAMBAN_P_G, SanitasiPeer::JML_JAMBAN_P_TG, SanitasiPeer::JML_JAMBAN_LP_G, SanitasiPeer::JML_JAMBAN_LP_TG, SanitasiPeer::TEMPAT_CUCI_TANGAN, SanitasiPeer::A_SABUN_AIR_MENGALIR, SanitasiPeer::LAST_UPDATE, SanitasiPeer::SOFT_DELETE, SanitasiPeer::LAST_SYNC, SanitasiPeer::UPDATER_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('SEKOLAH_ID', 'SEMESTER_ID', 'SUMBER_AIR_ID', 'KETERSEDIAAN_AIR', 'KECUKUPAN_AIR', 'MINUM_SISWA', 'MEMPROSES_AIR', 'SISWA_BAWA_AIR', 'TOILET_SISWA_LAKI', 'TOILET_SISWA_PEREMPUAN', 'TOILET_SISWA_KK', 'TOILET_SISWA_KECIL', 'TIPE_JAMBAN', 'JML_JAMBAN_L_G', 'JML_JAMBAN_L_TG', 'JML_JAMBAN_P_G', 'JML_JAMBAN_P_TG', 'JML_JAMBAN_LP_G', 'JML_JAMBAN_LP_TG', 'TEMPAT_CUCI_TANGAN', 'A_SABUN_AIR_MENGALIR', 'LAST_UPDATE', 'SOFT_DELETE', 'LAST_SYNC', 'UPDATER_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('sekolah_id', 'semester_id', 'sumber_air_id', 'ketersediaan_air', 'kecukupan_air', 'minum_siswa', 'memproses_air', 'siswa_bawa_air', 'toilet_siswa_laki', 'toilet_siswa_perempuan', 'toilet_siswa_kk', 'toilet_siswa_kecil', 'tipe_jamban', 'jml_jamban_l_g', 'jml_jamban_l_tg', 'jml_jamban_p_g', 'jml_jamban_p_tg', 'jml_jamban_lp_g', 'jml_jamban_lp_tg', 'tempat_cuci_tangan', 'a_sabun_air_mengalir', 'Last_update', 'Soft_delete', 'last_sync', 'Updater_ID', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. SanitasiPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('SekolahId' => 0, 'SemesterId' => 1, 'SumberAirId' => 2, 'KetersediaanAir' => 3, 'KecukupanAir' => 4, 'MinumSiswa' => 5, 'MemprosesAir' => 6, 'SiswaBawaAir' => 7, 'ToiletSiswaLaki' => 8, 'ToiletSiswaPerempuan' => 9, 'ToiletSiswaKk' => 10, 'ToiletSiswaKecil' => 11, 'TipeJamban' => 12, 'JmlJambanLG' => 13, 'JmlJambanLTg' => 14, 'JmlJambanPG' => 15, 'JmlJambanPTg' => 16, 'JmlJambanLpG' => 17, 'JmlJambanLpTg' => 18, 'TempatCuciTangan' => 19, 'ASabunAirMengalir' => 20, 'LastUpdate' => 21, 'SoftDelete' => 22, 'LastSync' => 23, 'UpdaterId' => 24, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('sekolahId' => 0, 'semesterId' => 1, 'sumberAirId' => 2, 'ketersediaanAir' => 3, 'kecukupanAir' => 4, 'minumSiswa' => 5, 'memprosesAir' => 6, 'siswaBawaAir' => 7, 'toiletSiswaLaki' => 8, 'toiletSiswaPerempuan' => 9, 'toiletSiswaKk' => 10, 'toiletSiswaKecil' => 11, 'tipeJamban' => 12, 'jmlJambanLG' => 13, 'jmlJambanLTg' => 14, 'jmlJambanPG' => 15, 'jmlJambanPTg' => 16, 'jmlJambanLpG' => 17, 'jmlJambanLpTg' => 18, 'tempatCuciTangan' => 19, 'aSabunAirMengalir' => 20, 'lastUpdate' => 21, 'softDelete' => 22, 'lastSync' => 23, 'updaterId' => 24, ),
        BasePeer::TYPE_COLNAME => array (SanitasiPeer::SEKOLAH_ID => 0, SanitasiPeer::SEMESTER_ID => 1, SanitasiPeer::SUMBER_AIR_ID => 2, SanitasiPeer::KETERSEDIAAN_AIR => 3, SanitasiPeer::KECUKUPAN_AIR => 4, SanitasiPeer::MINUM_SISWA => 5, SanitasiPeer::MEMPROSES_AIR => 6, SanitasiPeer::SISWA_BAWA_AIR => 7, SanitasiPeer::TOILET_SISWA_LAKI => 8, SanitasiPeer::TOILET_SISWA_PEREMPUAN => 9, SanitasiPeer::TOILET_SISWA_KK => 10, SanitasiPeer::TOILET_SISWA_KECIL => 11, SanitasiPeer::TIPE_JAMBAN => 12, SanitasiPeer::JML_JAMBAN_L_G => 13, SanitasiPeer::JML_JAMBAN_L_TG => 14, SanitasiPeer::JML_JAMBAN_P_G => 15, SanitasiPeer::JML_JAMBAN_P_TG => 16, SanitasiPeer::JML_JAMBAN_LP_G => 17, SanitasiPeer::JML_JAMBAN_LP_TG => 18, SanitasiPeer::TEMPAT_CUCI_TANGAN => 19, SanitasiPeer::A_SABUN_AIR_MENGALIR => 20, SanitasiPeer::LAST_UPDATE => 21, SanitasiPeer::SOFT_DELETE => 22, SanitasiPeer::LAST_SYNC => 23, SanitasiPeer::UPDATER_ID => 24, ),
        BasePeer::TYPE_RAW_COLNAME => array ('SEKOLAH_ID' => 0, 'SEMESTER_ID' => 1, 'SUMBER_AIR_ID' => 2, 'KETERSEDIAAN_AIR' => 3, 'KECUKUPAN_AIR' => 4, 'MINUM_SISWA' => 5, 'MEMPROSES_AIR' => 6, 'SISWA_BAWA_AIR' => 7, 'TOILET_SISWA_LAKI' => 8, 'TOILET_SISWA_PEREMPUAN' => 9, 'TOILET_SISWA_KK' => 10, 'TOILET_SISWA_KECIL' => 11, 'TIPE_JAMBAN' => 12, 'JML_JAMBAN_L_G' => 13, 'JML_JAMBAN_L_TG' => 14, 'JML_JAMBAN_P_G' => 15, 'JML_JAMBAN_P_TG' => 16, 'JML_JAMBAN_LP_G' => 17, 'JML_JAMBAN_LP_TG' => 18, 'TEMPAT_CUCI_TANGAN' => 19, 'A_SABUN_AIR_MENGALIR' => 20, 'LAST_UPDATE' => 21, 'SOFT_DELETE' => 22, 'LAST_SYNC' => 23, 'UPDATER_ID' => 24, ),
        BasePeer::TYPE_FIELDNAME => array ('sekolah_id' => 0, 'semester_id' => 1, 'sumber_air_id' => 2, 'ketersediaan_air' => 3, 'kecukupan_air' => 4, 'minum_siswa' => 5, 'memproses_air' => 6, 'siswa_bawa_air' => 7, 'toilet_siswa_laki' => 8, 'toilet_siswa_perempuan' => 9, 'toilet_siswa_kk' => 10, 'toilet_siswa_kecil' => 11, 'tipe_jamban' => 12, 'jml_jamban_l_g' => 13, 'jml_jamban_l_tg' => 14, 'jml_jamban_p_g' => 15, 'jml_jamban_p_tg' => 16, 'jml_jamban_lp_g' => 17, 'jml_jamban_lp_tg' => 18, 'tempat_cuci_tangan' => 19, 'a_sabun_air_mengalir' => 20, 'Last_update' => 21, 'Soft_delete' => 22, 'last_sync' => 23, 'Updater_ID' => 24, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = SanitasiPeer::getFieldNames($toType);
        $key = isset(SanitasiPeer::$fieldKeys[$fromType][$name]) ? SanitasiPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(SanitasiPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, SanitasiPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return SanitasiPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. SanitasiPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(SanitasiPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SanitasiPeer::SEKOLAH_ID);
            $criteria->addSelectColumn(SanitasiPeer::SEMESTER_ID);
            $criteria->addSelectColumn(SanitasiPeer::SUMBER_AIR_ID);
            $criteria->addSelectColumn(SanitasiPeer::KETERSEDIAAN_AIR);
            $criteria->addSelectColumn(SanitasiPeer::KECUKUPAN_AIR);
            $criteria->addSelectColumn(SanitasiPeer::MINUM_SISWA);
            $criteria->addSelectColumn(SanitasiPeer::MEMPROSES_AIR);
            $criteria->addSelectColumn(SanitasiPeer::SISWA_BAWA_AIR);
            $criteria->addSelectColumn(SanitasiPeer::TOILET_SISWA_LAKI);
            $criteria->addSelectColumn(SanitasiPeer::TOILET_SISWA_PEREMPUAN);
            $criteria->addSelectColumn(SanitasiPeer::TOILET_SISWA_KK);
            $criteria->addSelectColumn(SanitasiPeer::TOILET_SISWA_KECIL);
            $criteria->addSelectColumn(SanitasiPeer::TIPE_JAMBAN);
            $criteria->addSelectColumn(SanitasiPeer::JML_JAMBAN_L_G);
            $criteria->addSelectColumn(SanitasiPeer::JML_JAMBAN_L_TG);
            $criteria->addSelectColumn(SanitasiPeer::JML_JAMBAN_P_G);
            $criteria->addSelectColumn(SanitasiPeer::JML_JAMBAN_P_TG);
            $criteria->addSelectColumn(SanitasiPeer::JML_JAMBAN_LP_G);
            $criteria->addSelectColumn(SanitasiPeer::JML_JAMBAN_LP_TG);
            $criteria->addSelectColumn(SanitasiPeer::TEMPAT_CUCI_TANGAN);
            $criteria->addSelectColumn(SanitasiPeer::A_SABUN_AIR_MENGALIR);
            $criteria->addSelectColumn(SanitasiPeer::LAST_UPDATE);
            $criteria->addSelectColumn(SanitasiPeer::SOFT_DELETE);
            $criteria->addSelectColumn(SanitasiPeer::LAST_SYNC);
            $criteria->addSelectColumn(SanitasiPeer::UPDATER_ID);
        } else {
            $criteria->addSelectColumn($alias . '.sekolah_id');
            $criteria->addSelectColumn($alias . '.semester_id');
            $criteria->addSelectColumn($alias . '.sumber_air_id');
            $criteria->addSelectColumn($alias . '.ketersediaan_air');
            $criteria->addSelectColumn($alias . '.kecukupan_air');
            $criteria->addSelectColumn($alias . '.minum_siswa');
            $criteria->addSelectColumn($alias . '.memproses_air');
            $criteria->addSelectColumn($alias . '.siswa_bawa_air');
            $criteria->addSelectColumn($alias . '.toilet_siswa_laki');
            $criteria->addSelectColumn($alias . '.toilet_siswa_perempuan');
            $criteria->addSelectColumn($alias . '.toilet_siswa_kk');
            $criteria->addSelectColumn($alias . '.toilet_siswa_kecil');
            $criteria->addSelectColumn($alias . '.tipe_jamban');
            $criteria->addSelectColumn($alias . '.jml_jamban_l_g');
            $criteria->addSelectColumn($alias . '.jml_jamban_l_tg');
            $criteria->addSelectColumn($alias . '.jml_jamban_p_g');
            $criteria->addSelectColumn($alias . '.jml_jamban_p_tg');
            $criteria->addSelectColumn($alias . '.jml_jamban_lp_g');
            $criteria->addSelectColumn($alias . '.jml_jamban_lp_tg');
            $criteria->addSelectColumn($alias . '.tempat_cuci_tangan');
            $criteria->addSelectColumn($alias . '.a_sabun_air_mengalir');
            $criteria->addSelectColumn($alias . '.Last_update');
            $criteria->addSelectColumn($alias . '.Soft_delete');
            $criteria->addSelectColumn($alias . '.last_sync');
            $criteria->addSelectColumn($alias . '.Updater_ID');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SanitasiPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SanitasiPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(SanitasiPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 Sanitasi
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = SanitasiPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return SanitasiPeer::populateObjects(SanitasiPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            SanitasiPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(SanitasiPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      Sanitasi $obj A Sanitasi object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = serialize(array((string) $obj->getSekolahId(), (string) $obj->getSemesterId()));
            } // if key === null
            SanitasiPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Sanitasi object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Sanitasi) {
                $key = serialize(array((string) $value->getSekolahId(), (string) $value->getSemesterId()));
            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key
                $key = serialize(array((string) $value[0], (string) $value[1]));
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Sanitasi object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(SanitasiPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   Sanitasi Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(SanitasiPeer::$instances[$key])) {
                return SanitasiPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (SanitasiPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        SanitasiPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to sanitasi
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null && $row[$startcol + 1] === null) {
            return null;
        }

        return serialize(array((string) $row[$startcol], (string) $row[$startcol + 1]));
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return array((string) $row[$startcol], (string) $row[$startcol + 1]);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = SanitasiPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = SanitasiPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = SanitasiPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SanitasiPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Sanitasi object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = SanitasiPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = SanitasiPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + SanitasiPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SanitasiPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            SanitasiPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related Sekolah table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSekolah(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SanitasiPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SanitasiPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SanitasiPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SanitasiPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Semester table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSemester(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SanitasiPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SanitasiPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SanitasiPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SanitasiPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SumberAir table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinSumberAir(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SanitasiPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SanitasiPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SanitasiPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SanitasiPeer::SUMBER_AIR_ID, SumberAirPeer::SUMBER_AIR_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of Sanitasi objects pre-filled with their Sekolah objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Sanitasi objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSekolah(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SanitasiPeer::DATABASE_NAME);
        }

        SanitasiPeer::addSelectColumns($criteria);
        $startcol = SanitasiPeer::NUM_HYDRATE_COLUMNS;
        SekolahPeer::addSelectColumns($criteria);

        $criteria->addJoin(SanitasiPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SanitasiPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SanitasiPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SanitasiPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SanitasiPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SekolahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Sanitasi) to $obj2 (Sekolah)
                $obj2->addSanitasi($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Sanitasi objects pre-filled with their Semester objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Sanitasi objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSemester(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SanitasiPeer::DATABASE_NAME);
        }

        SanitasiPeer::addSelectColumns($criteria);
        $startcol = SanitasiPeer::NUM_HYDRATE_COLUMNS;
        SemesterPeer::addSelectColumns($criteria);

        $criteria->addJoin(SanitasiPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SanitasiPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SanitasiPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SanitasiPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SanitasiPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SemesterPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SemesterPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SemesterPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Sanitasi) to $obj2 (Semester)
                $obj2->addSanitasi($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Sanitasi objects pre-filled with their SumberAir objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Sanitasi objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinSumberAir(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SanitasiPeer::DATABASE_NAME);
        }

        SanitasiPeer::addSelectColumns($criteria);
        $startcol = SanitasiPeer::NUM_HYDRATE_COLUMNS;
        SumberAirPeer::addSelectColumns($criteria);

        $criteria->addJoin(SanitasiPeer::SUMBER_AIR_ID, SumberAirPeer::SUMBER_AIR_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SanitasiPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SanitasiPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = SanitasiPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SanitasiPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = SumberAirPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = SumberAirPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SumberAirPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    SumberAirPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Sanitasi) to $obj2 (SumberAir)
                $obj2->addSanitasi($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SanitasiPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SanitasiPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(SanitasiPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SanitasiPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(SanitasiPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $criteria->addJoin(SanitasiPeer::SUMBER_AIR_ID, SumberAirPeer::SUMBER_AIR_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of Sanitasi objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Sanitasi objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SanitasiPeer::DATABASE_NAME);
        }

        SanitasiPeer::addSelectColumns($criteria);
        $startcol2 = SanitasiPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SemesterPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SemesterPeer::NUM_HYDRATE_COLUMNS;

        SumberAirPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + SumberAirPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SanitasiPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(SanitasiPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $criteria->addJoin(SanitasiPeer::SUMBER_AIR_ID, SumberAirPeer::SUMBER_AIR_ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SanitasiPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SanitasiPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SanitasiPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SanitasiPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Sekolah rows

            $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = SekolahPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (Sanitasi) to the collection in $obj2 (Sekolah)
                $obj2->addSanitasi($obj1);
            } // if joined row not null

            // Add objects for joined Semester rows

            $key3 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = SemesterPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = SemesterPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SemesterPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (Sanitasi) to the collection in $obj3 (Semester)
                $obj3->addSanitasi($obj1);
            } // if joined row not null

            // Add objects for joined SumberAir rows

            $key4 = SumberAirPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = SumberAirPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = SumberAirPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    SumberAirPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (Sanitasi) to the collection in $obj4 (SumberAir)
                $obj4->addSanitasi($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Sekolah table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSekolah(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SanitasiPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SanitasiPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SanitasiPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SanitasiPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $criteria->addJoin(SanitasiPeer::SUMBER_AIR_ID, SumberAirPeer::SUMBER_AIR_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Semester table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSemester(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SanitasiPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SanitasiPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SanitasiPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SanitasiPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(SanitasiPeer::SUMBER_AIR_ID, SumberAirPeer::SUMBER_AIR_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related SumberAir table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptSumberAir(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(SanitasiPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            SanitasiPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(SanitasiPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(SanitasiPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(SanitasiPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of Sanitasi objects pre-filled with all related objects except Sekolah.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Sanitasi objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSekolah(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SanitasiPeer::DATABASE_NAME);
        }

        SanitasiPeer::addSelectColumns($criteria);
        $startcol2 = SanitasiPeer::NUM_HYDRATE_COLUMNS;

        SemesterPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SemesterPeer::NUM_HYDRATE_COLUMNS;

        SumberAirPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SumberAirPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SanitasiPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);

        $criteria->addJoin(SanitasiPeer::SUMBER_AIR_ID, SumberAirPeer::SUMBER_AIR_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SanitasiPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SanitasiPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SanitasiPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SanitasiPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Semester rows

                $key2 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SemesterPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SemesterPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SemesterPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Sanitasi) to the collection in $obj2 (Semester)
                $obj2->addSanitasi($obj1);

            } // if joined row is not null

                // Add objects for joined SumberAir rows

                $key3 = SumberAirPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SumberAirPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = SumberAirPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SumberAirPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Sanitasi) to the collection in $obj3 (SumberAir)
                $obj3->addSanitasi($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Sanitasi objects pre-filled with all related objects except Semester.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Sanitasi objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSemester(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SanitasiPeer::DATABASE_NAME);
        }

        SanitasiPeer::addSelectColumns($criteria);
        $startcol2 = SanitasiPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SumberAirPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SumberAirPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SanitasiPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(SanitasiPeer::SUMBER_AIR_ID, SumberAirPeer::SUMBER_AIR_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SanitasiPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SanitasiPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SanitasiPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SanitasiPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Sanitasi) to the collection in $obj2 (Sekolah)
                $obj2->addSanitasi($obj1);

            } // if joined row is not null

                // Add objects for joined SumberAir rows

                $key3 = SumberAirPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SumberAirPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = SumberAirPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SumberAirPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Sanitasi) to the collection in $obj3 (SumberAir)
                $obj3->addSanitasi($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Sanitasi objects pre-filled with all related objects except SumberAir.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Sanitasi objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptSumberAir(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(SanitasiPeer::DATABASE_NAME);
        }

        SanitasiPeer::addSelectColumns($criteria);
        $startcol2 = SanitasiPeer::NUM_HYDRATE_COLUMNS;

        SekolahPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + SekolahPeer::NUM_HYDRATE_COLUMNS;

        SemesterPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + SemesterPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(SanitasiPeer::SEKOLAH_ID, SekolahPeer::SEKOLAH_ID, $join_behavior);

        $criteria->addJoin(SanitasiPeer::SEMESTER_ID, SemesterPeer::SEMESTER_ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = SanitasiPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = SanitasiPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = SanitasiPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                SanitasiPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Sekolah rows

                $key2 = SekolahPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = SekolahPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = SekolahPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    SekolahPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Sanitasi) to the collection in $obj2 (Sekolah)
                $obj2->addSanitasi($obj1);

            } // if joined row is not null

                // Add objects for joined Semester rows

                $key3 = SemesterPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = SemesterPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = SemesterPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    SemesterPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Sanitasi) to the collection in $obj3 (Semester)
                $obj3->addSanitasi($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(SanitasiPeer::DATABASE_NAME)->getTable(SanitasiPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseSanitasiPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseSanitasiPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new SanitasiTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return SanitasiPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Sanitasi or Criteria object.
     *
     * @param      mixed $values Criteria or Sanitasi object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Sanitasi object
        }


        // Set the correct dbName
        $criteria->setDbName(SanitasiPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Sanitasi or Criteria object.
     *
     * @param      mixed $values Criteria or Sanitasi object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(SanitasiPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(SanitasiPeer::SEKOLAH_ID);
            $value = $criteria->remove(SanitasiPeer::SEKOLAH_ID);
            if ($value) {
                $selectCriteria->add(SanitasiPeer::SEKOLAH_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(SanitasiPeer::TABLE_NAME);
            }

            $comparison = $criteria->getComparison(SanitasiPeer::SEMESTER_ID);
            $value = $criteria->remove(SanitasiPeer::SEMESTER_ID);
            if ($value) {
                $selectCriteria->add(SanitasiPeer::SEMESTER_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(SanitasiPeer::TABLE_NAME);
            }

        } else { // $values is Sanitasi object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(SanitasiPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the sanitasi table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(SanitasiPeer::TABLE_NAME, $con, SanitasiPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SanitasiPeer::clearInstancePool();
            SanitasiPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Sanitasi or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Sanitasi object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            SanitasiPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Sanitasi) { // it's a model object
            // invalidate the cache for this single object
            SanitasiPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SanitasiPeer::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(SanitasiPeer::SEKOLAH_ID, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(SanitasiPeer::SEMESTER_ID, $value[1]));
                $criteria->addOr($criterion);
                // we can invalidate the cache for this single PK
                SanitasiPeer::removeInstanceFromPool($value);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(SanitasiPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            SanitasiPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Sanitasi object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      Sanitasi $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(SanitasiPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(SanitasiPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(SanitasiPeer::DATABASE_NAME, SanitasiPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve object using using composite pkey values.
     * @param   string $sekolah_id
     * @param   string $semester_id
     * @param      PropelPDO $con
     * @return   Sanitasi
     */
    public static function retrieveByPK($sekolah_id, $semester_id, PropelPDO $con = null) {
        $_instancePoolKey = serialize(array((string) $sekolah_id, (string) $semester_id));
         if (null !== ($obj = SanitasiPeer::getInstanceFromPool($_instancePoolKey))) {
             return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(SanitasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $criteria = new Criteria(SanitasiPeer::DATABASE_NAME);
        $criteria->add(SanitasiPeer::SEKOLAH_ID, $sekolah_id);
        $criteria->add(SanitasiPeer::SEMESTER_ID, $semester_id);
        $v = SanitasiPeer::doSelect($criteria, $con);

        return !empty($v) ? $v[0] : null;
    }
} // BaseSanitasiPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseSanitasiPeer::buildTableMap();

