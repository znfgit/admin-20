<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Admin\Preface;

use Admin\Model\SekolahPeer;
use Admin\Model\MstWilayahPeer;
use Admin\Model\PrasaranaPeer;
use Admin\Model\PrasaranaLongitudinalPeer;

class Sarpras
{
	function tesSarpras(Request $request, Application $app){
		return "koe sarpras";
	}

	static function Prasarana(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);
		$arrAll = array();
		$count = 0;

		$c = new \Criteria();
		$c->addJoin(PrasaranaPeer::PRASARANA_ID, PrasaranaLongitudinalPeer::PRASARANA_ID, \Criteria::INNER_JOIN);
		$c->add(PrasaranaPeer::SOFT_DELETE,0);
		$c->add(PrasaranaLongitudinalPeer::SOFT_DELETE,0);
		$c->add(PrasaranaLongitudinalPeer::SEMESTER_ID, $requesta['semester_id']);
		// $c->getJoins();

		if($requesta['sekolah_id']){
			$c->add(PrasaranaPeer::SEKOLAH_ID, $requesta['sekolah_id']);
		}

		// print_r($c->getJoins());die;

		$prasaranas = PrasaranaPeer::doSelect($c);

		foreach ($prasaranas as $p) {
			$arr = $p->toArray(\BasePeer::TYPE_FIELDNAME);

			$d = new \Criteria();
			$d->add(PrasaranaLongitudinalPeer::PRASARANA_ID, $p->getPrasaranaId());
			$d->add(PrasaranaLongitudinalPeer::SOFT_DELETE,0);
			$d->add(PrasaranaLongitudinalPeer::SEMESTER_ID, $requesta['semester_id']);

			$prl = PrasaranaLongitudinalPeer::doSelect($d);

			// print_r($prl);

			foreach ($prl as $pr) {
				// $arr[$key]= $value;
				// array_push($arr, $key);
				// foreach ($value as $val) {
					// print_r($val);
					// $arr[$val] = $vals;
				// }

				$arr['rusak_penutup_atap'] = $pr->getRusakTutupAtap();
				$arr['rusak_rangka_atap'] = $pr->getRusakRangkaPlafon();
				$arr['rusak_rangka_plafon'] = $pr->getRusakRangkaPlafon();
				$arr['rusak_kolom_ringbalok'] = $pr->getRusakKudakudaAtap();
				$arr['rusak_bata_dindingpengisi'] = $pr->getRusakBataDinding();
				$arr['rusak_kusen'] = $pr->getRusakKusen();
				$arr['rusak_daun_pintu'] = $pr->getRusakDaunPintu();
				$arr['rusak_daun_jendela'] = $pr->getRusakDaunJendela();
				$arr['rusak_penutup_lantai'] = $pr->getRusakTutupLantai();
				$arr['rusak_pondasi'] = $pr->getRusakPondasi();
			}

			// $arr['rusak_penutup_atap'] = $p->getPrasaranaLongitudinal()->getSemesterId();

			array_push($arrAll, $arr);
			$count++;
		}

		$return = array();

		$return['success'] = true;
		$return['results'] = $count;
		$return['id'] = 'prasarana_id';
		$return['limit'] = $requesta['limit'];
		$return['rows'] = $arrAll;

		return json_encode($return);
	}

	static function TingkatKerusakan(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case 0:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 1:
				$col_wilayah = "w2.nama";
				$col_kode = "w2.kode_wilayah";
				$col_id_level = "w2.id_level_wilayah";
				$col_mst_kode = "w2.mst_kode_wilayah";
				$col_mst_kode_induk = 'w3.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w3.mst_kode_wilayah,';
				$params_wilayah = " and w2.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 2:
				$col_wilayah = "w1.nama";
				$col_kode = "w1.kode_wilayah";
				$col_id_level = "w1.id_level_wilayah";
				$col_mst_kode = "w1.mst_kode_wilayah";
				$col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
				$params_wilayah = " and w1.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			default:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
		}

		if($requesta['keyword']){
			$param_keyword = " AND {$col_wilayah} like '%".$requesta['keyword']."%'";
		}else{
			$param_keyword = '';
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 's.');

		// $rumus_kerusakan = "((rusak_penutup_atap * 0.1056)+
		// 				   (rusak_rangka_atap   * 0.1162)+
		// 				   (rusak_lisplang_talang     * 0.0206 ) +
		// 				   (rusak_rangka_plafon * 0.0467 ) +
		// 				   (rusak_penutup_listplafon  * 0.0506 ) +
		// 				   (rusak_cat_plafon    * 0.0141 ) +
		// 				   (rusak_kolom_ringbalok     * 0.0966 ) +
		// 				   (rusak_bata_dindingpengisi * 0.1368 ) +
		// 				   (rusak_cat_dinding   * 0.0165 ) +
		// 				   (rusak_kusen  * 0.027 ) +
		// 				   (rusak_daun_pintu    * 0.0247 ) +
		// 				   (rusak_daun_jendela  * 0.0515 ) +
		// 				   (rusak_struktur_bawah      * 0.0289 ) +
		// 				   (rusak_penutup_lantai      * 0.0896 ) +
		// 				   (rusak_pondasi       * 0.115 ) +
		// 				   (rusak_sloof  * 0.033 ) +
		// 				   (rusak_listrik       * 0.0179 ) +
		// 				   (rusak_airhujan_rabatan    * 0.0122 ))";

		$rumus_kerusakan = "round( (
                        (rusak_pondasi * 0.12) +
                        (rusak_sloop_kolom_balok * 0.25) +
                        (rusak_plester_struktur * 0.02) +
                        (rusak_kudakuda_atap * 0.05) +
                        (rusak_kaso_atap * 0.032) +
                        (rusak_reng_atap * 0.015) +
                        ((rusak_tutup_atap) * 0.025) +
                        (rusak_rangka_plafon * 0.03) +
                        (rusak_tutup_plafon * 0.04) +
                        (rusak_bata_dinding * 0.07) +
                        (rusak_plester_dinding * 0.022) +
                        (rusak_daun_jendela * 0.016) +
                        (rusak_daun_pintu * 0.0125) +
                        (rusak_kusen * 0.026) +
                        (rusak_tutup_lantai * 0.12) +
                        (rusak_inst_listrik * 0.0475) +
                        (rusak_inst_air * 0.0245) +
                        (rusak_drainase * 0.015) +
                        (rusak_finish_struktur * 0.01) +
                        (rusak_finish_plafon * 0.012) +
                        (rusak_finish_dinding * 0.025) +
                        (rusak_finish_kpj * 0.0175)
                    ) , 2)";

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$col_wilayah}) as 'no',
					{$col_wilayah} as nama,
					{$col_kode} as kode_wilayah,
					{$col_id_level} as id_level_wilayah,
					{$col_mst_kode} as mst_kode_wilayah,
					{$col_mst_kode_induk}
					SUM(CASE WHEN pr.jenis_prasarana_id = 1 and {$rumus_kerusakan} = 0 then 1 else 0 end) as r_kelas_baik,
					SUM(CASE WHEN pr.jenis_prasarana_id = 1 and {$rumus_kerusakan} > 0 AND {$rumus_kerusakan} <= 30 then 1 else 0 end) as r_kelas_rusak_ringan,
					SUM(CASE WHEN pr.jenis_prasarana_id = 1 and {$rumus_kerusakan} > 30 AND {$rumus_kerusakan} <= 50 then 1 else 0 end) as r_kelas_rusak_sedang,
					SUM(CASE WHEN pr.jenis_prasarana_id = 1 and {$rumus_kerusakan} > 50 then 1 else 0 end) as r_kelas_rusak_berat,
					SUM(CASE WHEN pr.jenis_prasarana_id = 16 and {$rumus_kerusakan} = 0 then 1 else 0 end) as bengkel_baik,
					SUM(CASE WHEN pr.jenis_prasarana_id = 16 and {$rumus_kerusakan} > 0 AND {$rumus_kerusakan} <= 30 then 1 else 0 end) as bengkel_rusak_ringan,
					SUM(CASE WHEN pr.jenis_prasarana_id = 16 and {$rumus_kerusakan} > 30 AND {$rumus_kerusakan} <= 50 then 1 else 0 end) as bengkel_rusak_sedang,
					SUM(CASE WHEN pr.jenis_prasarana_id = 16 and {$rumus_kerusakan} > 50 then 1 else 0 end) as bengkel_rusak_berat,
					SUM(CASE WHEN pr.jenis_prasarana_id = 10 and {$rumus_kerusakan} = 0 then 1 else 0 end) as perpustakaan_baik,
					SUM(CASE WHEN pr.jenis_prasarana_id = 10 and {$rumus_kerusakan} > 0 AND {$rumus_kerusakan} <= 30 then 1 else 0 end) as perpustakaan_rusak_ringan,
					SUM(CASE WHEN pr.jenis_prasarana_id = 10 and {$rumus_kerusakan} > 30 AND {$rumus_kerusakan} <= 50 then 1 else 0 end) as perpustakaan_rusak_sedang,
					SUM(CASE WHEN pr.jenis_prasarana_id = 10 and {$rumus_kerusakan} > 50 then 1 else 0 end) as perpustakaan_rusak_berat
				FROM
					prasarana pr
				JOIN prasarana_longitudinal prl ON prl.prasarana_id = pr.prasarana_id
				JOIN sekolah s ON pr.sekolah_id = s.sekolah_id
				JOIN ref.mst_wilayah w1 ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
				JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
				JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
				WHERE
					pr.Soft_delete = 0
				AND prl.Soft_delete = 0
				AND s.Soft_delete = 0
				AND prl.semester_id = {$requesta['semester_id']}
				{$params_wilayah}
				{$params_bp}
				{$param_keyword}
				GROUP BY
					{$col_wilayah},
					{$col_kode},
					{$col_id_level},
					{$col_mst_kode_induk_group}
					{$col_mst_kode}
				ORDER BY
					{$col_wilayah} ASC";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);

	}

	static function TingkatKerusakanSp (Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case 0:
				$params_wilayah = "";
				break;
			case 1:
				$params_wilayah = "AND w3.kode_wilayah = '{$requesta['kode_wilayah']}'";
				break;
			case 2:
				$params_wilayah = "AND w2.kode_wilayah = '{$requesta['kode_wilayah']}'";
				break;
			case 3:
				$params_wilayah = "AND w1.kode_wilayah = '{$requesta['kode_wilayah']}'";
				break;
			default:
				$params_wilayah = "";
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 's.');

		if($requesta['keyword']){
			$param_keyword = " AND (s.nama like '%".$requesta['keyword']."%' OR s.npsn like '%".$requesta['keyword']."%')";
		}else{
			$param_keyword = "";
		}

		// $rumus_kerusakan = "((rusak_penutup_atap * 0.1056)+
		// 				   (rusak_rangka_atap   * 0.1162)+
		// 				   (rusak_lisplang_talang     * 0.0206 ) +
		// 				   (rusak_rangka_plafon * 0.0467 ) +
		// 				   (rusak_penutup_listplafon  * 0.0506 ) +
		// 				   (rusak_cat_plafon    * 0.0141 ) +
		// 				   (rusak_kolom_ringbalok     * 0.0966 ) +
		// 				   (rusak_bata_dindingpengisi * 0.1368 ) +
		// 				   (rusak_cat_dinding   * 0.0165 ) +
		// 				   (rusak_kusen  * 0.027 ) +
		// 				   (rusak_daun_pintu    * 0.0247 ) +
		// 				   (rusak_daun_jendela  * 0.0515 ) +
		// 				   (rusak_struktur_bawah      * 0.0289 ) +
		// 				   (rusak_penutup_lantai      * 0.0896 ) +
		// 				   (rusak_pondasi       * 0.115 ) +
		// 				   (rusak_sloof  * 0.033 ) +
		// 				   (rusak_listrik       * 0.0179 ) +
		// 				   (rusak_airhujan_rabatan    * 0.0122 ))";
		$rumus_kerusakan = "round( (
                        (rusak_pondasi * 0.12) +
                        (rusak_sloop_kolom_balok * 0.25) +
                        (rusak_plester_struktur * 0.02) +
                        (rusak_kudakuda_atap * 0.05) +
                        (rusak_kaso_atap * 0.032) +
                        (rusak_reng_atap * 0.015) +
                        ((rusak_tutup_atap) * 0.025) +
                        (rusak_rangka_plafon * 0.03) +
                        (rusak_tutup_plafon * 0.04) +
                        (rusak_bata_dinding * 0.07) +
                        (rusak_plester_dinding * 0.022) +
                        (rusak_daun_jendela * 0.016) +
                        (rusak_daun_pintu * 0.0125) +
                        (rusak_kusen * 0.026) +
                        (rusak_tutup_lantai * 0.12) +
                        (rusak_inst_listrik * 0.0475) +
                        (rusak_inst_air * 0.0245) +
                        (rusak_drainase * 0.015) +
                        (rusak_finish_struktur * 0.01) +
                        (rusak_finish_plafon * 0.012) +
                        (rusak_finish_dinding * 0.025) +
                        (rusak_finish_kpj * 0.0175)
                    ) , 2)";

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY s.nama) as 'no',
					s.sekolah_id,
					s.nama,
					s.npsn,
                    s.status_sekolah,
                    CASE s.status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    s.bentuk_pendidikan_id,
                    CASE 
                        WHEN s.bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN s.bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN s.bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN s.bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN s.bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN s.bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN s.bentuk_pendidikan_id IN (7, 8, 29) THEN 'SDLB'
                        WHEN s.bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN s.bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
					w1.nama as kecamatan,
					w2.nama as kabupaten,
					w3.nama as propinsi,
					SUM(CASE WHEN pr.jenis_prasarana_id = 1 and {$rumus_kerusakan} = 0 then 1 else 0 end) as r_kelas_baik,
					SUM(CASE WHEN pr.jenis_prasarana_id = 1 and {$rumus_kerusakan} > 0 AND {$rumus_kerusakan} <= 30 then 1 else 0 end) as r_kelas_rusak_ringan,
					SUM(CASE WHEN pr.jenis_prasarana_id = 1 and {$rumus_kerusakan} > 30 AND {$rumus_kerusakan} <= 50 then 1 else 0 end) as r_kelas_rusak_sedang,
					SUM(CASE WHEN pr.jenis_prasarana_id = 1 and {$rumus_kerusakan} > 50 then 1 else 0 end) as r_kelas_rusak_berat,
					SUM(CASE WHEN pr.jenis_prasarana_id = 16 and {$rumus_kerusakan} = 0 then 1 else 0 end) as bengkel_baik,
					SUM(CASE WHEN pr.jenis_prasarana_id = 16 and {$rumus_kerusakan} > 0 AND {$rumus_kerusakan} <= 30 then 1 else 0 end) as bengkel_rusak_ringan,
					SUM(CASE WHEN pr.jenis_prasarana_id = 16 and {$rumus_kerusakan} > 30 AND {$rumus_kerusakan} <= 50 then 1 else 0 end) as bengkel_rusak_sedang,
					SUM(CASE WHEN pr.jenis_prasarana_id = 16 and {$rumus_kerusakan} > 50 then 1 else 0 end) as bengkel_rusak_berat,
					SUM(CASE WHEN pr.jenis_prasarana_id = 10 and {$rumus_kerusakan} = 0 then 1 else 0 end) as perpustakaan_baik,
					SUM(CASE WHEN pr.jenis_prasarana_id = 10 and {$rumus_kerusakan} > 0 AND {$rumus_kerusakan} <= 30 then 1 else 0 end) as perpustakaan_rusak_ringan,
					SUM(CASE WHEN pr.jenis_prasarana_id = 10 and {$rumus_kerusakan} > 30 AND {$rumus_kerusakan} <= 50 then 1 else 0 end) as perpustakaan_rusak_sedang,
					SUM(CASE WHEN pr.jenis_prasarana_id = 10 and {$rumus_kerusakan} > 50 then 1 else 0 end) as perpustakaan_rusak_berat
				FROM
					prasarana pr
				JOIN prasarana_longitudinal prl ON prl.prasarana_id = pr.prasarana_id
				JOIN sekolah s ON pr.sekolah_id = s.sekolah_id
				JOIN ref.mst_wilayah w1 ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
				JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
				JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
				WHERE
					pr.Soft_delete = 0
				AND prl.Soft_delete = 0
				AND s.Soft_delete = 0
				AND prl.semester_id = {$requesta['semester_id']}
				{$params_wilayah}
				{$params_bp}
				{$param_keyword}
				GROUP BY
                    s.sekolah_id,
                    s.nama, 
                    s.npsn,
                    s.status_sekolah,
                    s.bentuk_pendidikan_id,
					w1.nama,
					w2.nama,
					w3.nama
				ORDER BY
					s.nama ASC";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql);

		$sql .= " OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";


		$final = array();
		$final['results'] = sizeof($return);

		$return = Preface::getDataByQuery($sql);

		$final['rows'] = $return;

		return json_encode($final);

	}

	static function SarprasKerusakan(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case 0:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 1:
				$col_wilayah = "w2.nama";
				$col_kode = "w2.kode_wilayah";
				$col_id_level = "w2.id_level_wilayah";
				$col_mst_kode = "w2.mst_kode_wilayah";
				$col_mst_kode_induk = 'w3.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w3.mst_kode_wilayah,';
				$params_wilayah = " and w2.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 2:
				$col_wilayah = "w1.nama";
				$col_kode = "w1.kode_wilayah";
				$col_id_level = "w1.id_level_wilayah";
				$col_mst_kode = "w1.mst_kode_wilayah";
				$col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
				$params_wilayah = " and w1.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			default:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 's.');

		if($requesta['sekolah_id']){
			$params_sekolah = " AND s.sekolah_id = '".$requesta['sekolah_id']."'";
		}else{
			$params_sekolah = "";
		}

		$rumus_kerusakan = "round( (
		                        (rusak_pondasi * 0.12) +
		                        (rusak_sloop_kolom_balok * 0.25) +
		                        (rusak_plester_struktur * 0.02) +
		                        (rusak_kudakuda_atap * 0.05) +
		                        (rusak_kaso_atap * 0.032) +
		                        (rusak_reng_atap * 0.015) +
		                        ((rusak_tutup_atap) * 0.025) +
		                        (rusak_rangka_plafon * 0.03) +
		                        (rusak_tutup_plafon * 0.04) +
		                        (rusak_bata_dinding * 0.07) +
		                        (rusak_plester_dinding * 0.022) +
		                        (rusak_daun_jendela * 0.016) +
		                        (rusak_daun_pintu * 0.0125) +
		                        (rusak_kusen * 0.026) +
		                        (rusak_tutup_lantai * 0.12) +
		                        (rusak_inst_listrik * 0.0475) +
		                        (rusak_inst_air * 0.0245) +
		                        (rusak_drainase * 0.015) +
		                        (rusak_finish_struktur * 0.01) +
		                        (rusak_finish_plafon * 0.012) +
		                        (rusak_finish_dinding * 0.025) +
		                        (rusak_finish_kpj * 0.0175)
		                    ) , 2)";

		$sql = "SELECT
					(CASE WHEN {$rumus_kerusakan} = 0 THEN 'Baik'
						WHEN {$rumus_kerusakan} > 0 AND {$rumus_kerusakan} <= 30 THEN 'Rusak Ringan'
						WHEN {$rumus_kerusakan} > 30 AND {$rumus_kerusakan} <= 50 THEN 'Rusak Sedang'
						WHEN {$rumus_kerusakan} > 50 THEN 'Rusak Berat'
						ELSE 'Rusak Berat' END) as desk,
						SUM(1) as jumlah
				FROM
					prasarana pr
				JOIN prasarana_longitudinal prl ON prl.prasarana_id = pr.prasarana_id
				JOIN sekolah s ON pr.sekolah_id = s.sekolah_id
				JOIN ref.mst_wilayah w1 ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
				JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
				JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
				WHERE
					pr.Soft_delete = 0
				AND prl.Soft_delete = 0
				AND s.Soft_delete = 0
				AND prl.semester_id = '{$requesta['semester_id']}'
				{$params_wilayah}
				{$params_sekolah}
				{$params_bp}
				AND pr.jenis_prasarana_id = 1
				GROUP BY (CASE WHEN {$rumus_kerusakan} = 0 THEN 'Baik'
						WHEN {$rumus_kerusakan} > 0 AND {$rumus_kerusakan} <= 30 THEN 'Rusak Ringan'
						WHEN {$rumus_kerusakan} > 30 AND {$rumus_kerusakan} <= 50 THEN 'Rusak Sedang'
						WHEN {$rumus_kerusakan} > 50 THEN 'Rusak Berat'
						ELSE 'Rusak Berat' END)";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);

	}

	static function Rkb(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_provinsi}
					{$add_kode_wilayah_induk_provinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					sum( case when status_sekolah = 1 then pd else 0 end ) as pd_negeri,
					sum( case when status_sekolah = 2 then pd else 0 end ) as pd_swasta,
					sum( case when status_sekolah in (1,2) then pd else 0 end ) as pd_total,
					sum(case when status_sekolah = 1 then (r_kelas_baik+r_kelas_rusak_ringan+r_kelas_rusak_sedang+r_kelas_rusak_berat) else 0 end) as jumlah_ruang_kelas_negeri,
					sum(case when status_sekolah = 1 then ceiling((pd / CONVERT(decimal(4,2), (case when bentuk_pendidikan_id in (5,6) then 20 when bentuk_pendidikan_id in (13,15) then 36 else 15 end)))) else 0 end) as jumlah_ruang_kelas_negeri_ideal,
					sum(case when status_sekolah = 2 then (r_kelas_baik+r_kelas_rusak_ringan+r_kelas_rusak_sedang+r_kelas_rusak_berat) else 0 end) as jumlah_ruang_kelas_swasta,
					sum(case when status_sekolah in (1,2) then (r_kelas_baik+r_kelas_rusak_ringan+r_kelas_rusak_sedang+r_kelas_rusak_berat) else 0 end) as jumlah_ruang_kelas_total,
					CAST( sum( case when status_sekolah = 1 then ( case when ceiling((pd / CONVERT(decimal(4,2), (case when bentuk_pendidikan_id in (5,6) then 20 when bentuk_pendidikan_id in (13,15) then 36 else 15 end))) ) - (r_kelas_baik+r_kelas_rusak_ringan+r_kelas_rusak_sedang+r_kelas_rusak_berat) > 0 then ceiling((pd / CONVERT(decimal(4,2), (case when bentuk_pendidikan_id in (5,6) then 20 when bentuk_pendidikan_id in (13,15) then 36 else 15 end))) ) - (r_kelas_baik+r_kelas_rusak_ringan+r_kelas_rusak_sedang+r_kelas_rusak_berat) else 0 end ) else 0 end ) AS INT) as rkb_negeri,
					CAST( sum( case when status_sekolah = 2 then ( case when ceiling((pd / CONVERT(decimal(4,2), (case when bentuk_pendidikan_id in (5,6) then 20 when bentuk_pendidikan_id in (13,15) then 36 else 15 end))) ) - (r_kelas_baik+r_kelas_rusak_ringan+r_kelas_rusak_sedang+r_kelas_rusak_berat) > 0 then ceiling((pd / CONVERT(decimal(4,2), (case when bentuk_pendidikan_id in (5,6) then 20 when bentuk_pendidikan_id in (13,15) then 36 else 15 end))) ) - (r_kelas_baik+r_kelas_rusak_ringan+r_kelas_rusak_sedang+r_kelas_rusak_berat) else 0 end ) else 0 end ) AS INT) as rkb_swasta,
					CAST( sum( case when status_sekolah in (1,2) then ( case when ceiling((pd / CONVERT(decimal(4,2), (case when bentuk_pendidikan_id in (5,6) then 20 when bentuk_pendidikan_id in (13,15) then 36 else 15 end))) ) - (r_kelas_baik+r_kelas_rusak_ringan+r_kelas_rusak_sedang+r_kelas_rusak_berat) > 0 then ceiling((pd / CONVERT(decimal(4,2), (case when bentuk_pendidikan_id in (5,6) then 20 when bentuk_pendidikan_id in (13,15) then 36 else 15 end))) ) - (r_kelas_baik+r_kelas_rusak_ringan+r_kelas_rusak_sedang+r_kelas_rusak_berat) else 0 end ) else 0 end ) AS INT) as rkb_total
				FROM
					rekap_sekolah
				LEFT OUTER JOIN rekap_sekolah_sarpras rekap_sarpras on rekap_sarpras.sekolah_id = rekap_sekolah.sekolah_id and rekap_sarpras.semester_id = '".$requesta['semester_id']."'
				WHERE
					rekap_sekolah.semester_id = '".$requesta['semester_id']."'
				AND rekap_sekolah.tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				{$param_kode_wilayah}
				{$params_bp}
				AND soft_delete = 0
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_provinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_provinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
	}

	static function RkbSp(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY nama) as 'no',
					rekap_sekolah.sekolah_id,
					nama,
					npsn,
                    status_sekolah,
                    CASE status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bentuk_pendidikan_id,
                    CASE 
                        WHEN bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SDLB'
                        WHEN bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
					kecamatan,
					pd,
					(r_kelas_baik+r_kelas_rusak_ringan+r_kelas_rusak_sedang+r_kelas_rusak_berat) as jumlah_ruang_kelas,
					ceiling(
						( pd / CONVERT(decimal(4,2), CONVERT(decimal(4,2), (case when bentuk_pendidikan_id in (5,6) then 20 when bentuk_pendidikan_id in (13,15) then 36 else 15 end))) ) 
					) as jumlah_ruang_kelas_ideal,
					(
						case when ceiling( (pd / CONVERT(decimal(4,2), (case when bentuk_pendidikan_id in (5,6) then 20 when bentuk_pendidikan_id in (13,15) then 36 else 15 end)) ) ) - isnull((r_kelas_baik+r_kelas_rusak_ringan+r_kelas_rusak_sedang+r_kelas_rusak_berat),0) > 0 
						then ceiling( (pd / CONVERT(decimal(4,2), (case when bentuk_pendidikan_id in (5,6) then 20 when bentuk_pendidikan_id in (13,15) then 36 else 15 end)) ) ) - isnull((r_kelas_baik+r_kelas_rusak_ringan+r_kelas_rusak_sedang+r_kelas_rusak_berat),0) 
						else 0 end
					) as rkb
				FROM
					rekap_sekolah
				LEFT OUTER JOIN rekap_sekolah_sarpras rekap_sarpras on rekap_sarpras.sekolah_id = rekap_sekolah.sekolah_id and rekap_sarpras.semester_id = '".$requesta['semester_id']."'
				WHERE
					rekap_sekolah.semester_id = '".$requesta['semester_id']."'
				AND rekap_sekolah.tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				AND kode_wilayah_kecamatan = '".$requesta['kode_wilayah']."'
				{$params_bp}
				AND soft_delete = 0
				ORDER BY
					nama";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		$sql .= " OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";


		$final = array();
		$final['results'] = sizeof($return);

		$return = Preface::getDataByQuery($sql, 'rekap');

		$final['rows'] = $return;

		return json_encode($final);
	}
}