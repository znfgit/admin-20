<?php
namespace Admin;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use
 Admin\Preface;

use Admin\Model\SekolahPeer;
use Admin\Model\MstWilayahPeer;

class SatuanPendidikan
{

	// I Started from above so that conflict can be avoided
	static function getRekapSekolah(Request $request, Application $app){
		$sekolah_id = $request->get('sekolah_id');
		$semester_id = $request->get('semester_id') ? $request->get('semester_id') : $app['session']->get('semester_id');

		$sql = "select * from rekap_sekolah where sekolah_id = '".$sekolah_id."' and semester_id = '".$semester_id."'";

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
	}

	static function SatuanPendidikan(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);
		$count = 0;

		$sql_awal		= " SELECT";
		$sql_tengah 	= " sum(1) as count";
		$sql_akhir 		= " FROM
								sekolah s
							JOIN ref.mst_wilayah w1 ON LEFT(s.kode_wilayah,6) = w1.kode_wilayah
							JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
							JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
							LEFT OUTER JOIN ref.bentuk_pendidikan bp on bp.bentuk_pendidikan_id = s.bentuk_pendidikan_id
							LEFT OUTER JOIN ref.status_kepemilikan sk on sk.status_kepemilikan_id = s.status_kepemilikan_id
							WHERE
								s.Soft_delete = 0";
		$sql_params 	= "";

		// additional param starts here

		if($requesta['keyword'] && $requesta['keyword'] != 'null'){
			$sql_params .= " AND (s.nama like '%{$requesta['keyword']}%' OR s.npsn like '%{$requesta['keyword']}%')";
		}

		if($requesta['propinsi'] && $requesta['propinsi'] != 'null'){
			$sql_params .= " AND w3.kode_wilayah = '{$requesta['propinsi']}'";
		}

		if($requesta['kabupaten'] && $requesta['kabupaten'] != 'null'){
			$sql_params .= " AND w2.kode_wilayah = '{$requesta['kabupaten']}'";
		}

		if($requesta['kecamatan'] && $requesta['kecamatan'] != 'null'){
			$sql_params .= " AND LEFT(s.kode_wilayah,6) = '{$requesta['kecamatan']}'";
		}

		if($requesta['bentuk_pendidikan_id'] && $requesta['bentuk_pendidikan_id'] != 'null'){
			$sql_params .= " AND s.bentuk_pendidikan_id = '{$requesta['bentuk_pendidikan_id']}'";
		}

		if($requesta['status_sekolah'] && $requesta['status_sekolah'] != 'null'){

			if($requesta['status_sekolah'] != '999'){
				$sql_params .= " AND s.status_sekolah = '{$requesta['status_sekolah']}'";
			}

		}

		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMA'){
				$sql_params .= " AND s.bentuk_pendidikan_id = 13";
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$sql_params .= " AND s.bentuk_pendidikan_id = 15";
			}else if(BENTUKPENDIDIKAN == 'SMASMK'){
				$sql_params .= " AND s.bentuk_pendidikan_id IN (13,15)";
			}else{
				$sql_params .= " AND s.bentuk_pendidikan_id IN (13,15)";
			}

		}
		else if(TINGKAT == 'DIKDAS'){
			$sql_params .= " AND s.bentuk_pendidikan_id IN (5,6,14,29)";
		}
		else{
			$sql_params .= " AND s.bentuk_pendidikan_id IN (1,2,3,4,5,6,14,29, 13,15)";

		}

		$sql = $sql_awal . $sql_tengah . $sql_akhir .  $sql_params;

		$fetch = getDataBySql($sql);

		$count = $fetch[0]['count'];

		$sql_tengah = " s.*,
						w1.nama AS kecamatan,
						w2.nama AS kabupaten,
						w3.nama AS propinsi,
						w1.kode_wilayah AS kode_kecamatan,
						w2.kode_wilayah AS kode_kab,
						w3.kode_wilayah AS kode_prop,
						bp.nama as bentuk_pendidikan_id_str,
						sk.nama as status_kepemilikan_id_str";

		$sql = $sql_awal . $sql_tengah . $sql_akhir.  $sql_params;

		// return $sql;die;

		// paging
		$sql .= " ORDER BY s.nama OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

		$return = array();
		$arrAll = array();

		$fetch = getDataBySql($sql);

		foreach ($fetch as $f) {


			foreach ($f as $fc => $v) {
				$arr[$fc] = $v;

			}

			$arr['koreggen'] = strtoupper(base_convert($arr['kode_registrasi'], 10, 32));

			array_push($arrAll, $arr);
		}

		$return['success'] = true;
		$return['results'] = $count;
		$return['id'] = 'sekolah_id';
		$return['limit'] = $requesta['limit'];
		$return['rows'] = $arrAll;

		return json_encode($return);
	}

	function getSumberDanaSatuanPendidikan(Request $request, Application $app){

		$request = Preface::Inisiasi($request);

        switch ($request['id_level_wilayah']) {
            case "0":
                $param_nama = 'provinsi';
                $param_kode_nama = '';
                $param_wilayah = 'provinsi';
                $param_kode_wilayah = '';

                $add_induk_provinsi = "null as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "1":
                $param_nama = 'kabupaten';
                $param_kode_nama = '';
                $param_wilayah = 'kabupaten';
                $param_kode_wilayah = "AND kode_wilayah_provinsi = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "2":
                $param_nama = 'kecamatan';
                $param_kode_nama = '';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "3":
                $param_nama = 'nama';
                $param_kode_nama = 'sekolah_id,';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kecamatan = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "kecamatan as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten,";
                $add_group_induk_kecamatan = "kecamatan,";
                $add_group_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan";
                break;
            default:
                break;
        }

		$sql = "SELECT
				  {$param_nama} as nama,
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah} as kode_wilayah,
				  id_level_wilayah_{$param_wilayah} as id_level_wilayah,
				  mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
				  {$add_induk_provinsi}
				  {$add_kode_wilayah_induk_provinsi}
				  {$add_induk_kabupaten}
				  {$add_kode_wilayah_induk_kabupaten}
				  {$add_induk_kecamatan}
				  {$add_kode_wilayah_induk_kecamatan}
				  SUM(CASE WHEN sumber_dana_sekolah_id = 1 AND bentuk_pendidikan_id = 1 then 1 else 0 END) as tk_iuran_orang_tua,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 1 AND bentuk_pendidikan_id = 2 then 1 else 0 END) as kb_iuran_orang_tua,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 1 AND bentuk_pendidikan_id = 3 then 1 else 0 END) as tpa_iuran_orang_tua,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 1 AND bentuk_pendidikan_id = 4 then 1 else 0 END) as sps_iuran_orang_tua,

				  SUM(CASE WHEN sumber_dana_sekolah_id = 2 AND bentuk_pendidikan_id = 1 then 1 else 0 END) as tk_donatur_tetap,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 2 AND bentuk_pendidikan_id = 2 then 1 else 0 END) as kb_donatur_tetap,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 2 AND bentuk_pendidikan_id = 3 then 1 else 0 END) as tpa_donatur_tetap,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 2 AND bentuk_pendidikan_id = 4 then 1 else 0 END) as sps_donatur_tetap,

				  SUM(CASE WHEN sumber_dana_sekolah_id = 3 AND bentuk_pendidikan_id = 1 then 1 else 0 END) as tk_yayasan,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 3 AND bentuk_pendidikan_id = 2 then 1 else 0 END) as kb_yayasan,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 3 AND bentuk_pendidikan_id = 3 then 1 else 0 END) as tpa_yayasan,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 3 AND bentuk_pendidikan_id = 4 then 1 else 0 END) as sps_yayasan,

				  SUM(CASE WHEN sumber_dana_sekolah_id = 4 AND bentuk_pendidikan_id = 1 then 1 else 0 END) as tk_perusahaan,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 4 AND bentuk_pendidikan_id = 2 then 1 else 0 END) as kb_perusahaan,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 4 AND bentuk_pendidikan_id = 3 then 1 else 0 END) as tpa_perusahaan,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 4 AND bentuk_pendidikan_id = 4 then 1 else 0 END) as sps_perusahaan,

				  SUM(CASE WHEN sumber_dana_sekolah_id = 5 AND bentuk_pendidikan_id = 1 then 1 else 0 END) as tk_desa,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 5 AND bentuk_pendidikan_id = 2 then 1 else 0 END) as kb_desa,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 5 AND bentuk_pendidikan_id = 3 then 1 else 0 END) as tpa_desa,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 5 AND bentuk_pendidikan_id = 4 then 1 else 0 END) as sps_desa,

				  SUM(CASE WHEN sumber_dana_sekolah_id = 6 AND bentuk_pendidikan_id = 1 then 1 else 0 END) as tk_apbd_kab,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 6 AND bentuk_pendidikan_id = 2 then 1 else 0 END) as kb_apbd_kab,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 6 AND bentuk_pendidikan_id = 3 then 1 else 0 END) as tpa_apbd_kab,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 6 AND bentuk_pendidikan_id = 4 then 1 else 0 END) as sps_apbd_kab,

				  SUM(CASE WHEN sumber_dana_sekolah_id = 7 AND bentuk_pendidikan_id = 1 then 1 else 0 END) as tk_apbd_prop,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 7 AND bentuk_pendidikan_id = 2 then 1 else 0 END) as kb_apbd_prop,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 7 AND bentuk_pendidikan_id = 3 then 1 else 0 END) as tpa_apbd_prop,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 7 AND bentuk_pendidikan_id = 4 then 1 else 0 END) as sps_apbd_prop,

				  SUM(CASE WHEN sumber_dana_sekolah_id = 8 AND bentuk_pendidikan_id = 1 then 1 else 0 END) as tk_apbn,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 8 AND bentuk_pendidikan_id = 2 then 1 else 0 END) as kb_apbn,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 8 AND bentuk_pendidikan_id = 3 then 1 else 0 END) as tpa_apbn,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 8 AND bentuk_pendidikan_id = 4 then 1 else 0 END) as sps_apbn,

				  SUM(CASE WHEN sumber_dana_sekolah_id = 9 AND bentuk_pendidikan_id = 1 then 1 else 0 END) as tk_lainnya,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 9 AND bentuk_pendidikan_id = 2 then 1 else 0 END) as kb_lainnya,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 9 AND bentuk_pendidikan_id = 3 then 1 else 0 END) as tpa_lainnya,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 9 AND bentuk_pendidikan_id = 4 then 1 else 0 END) as sps_lainnya,

				  SUM(CASE WHEN sumber_dana_sekolah_id = 0 AND bentuk_pendidikan_id = 1 then 1 else 0 END) as tk_belum_isi,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 0 AND bentuk_pendidikan_id = 2 then 1 else 0 END) as kb_belum_isi,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 0 AND bentuk_pendidikan_id = 3 then 1 else 0 END) as tpa_belum_isi,
				  SUM(CASE WHEN sumber_dana_sekolah_id = 0 AND bentuk_pendidikan_id = 4 then 1 else 0 END) as sps_belum_isi
				FROM
				  dbo.rekap_sekolah
				WHERE
					semester_id is null
				AND tahun_ajaran_id = '".$request['tahun_ajaran_id']."'
				AND bentuk_pendidikan_id IN (1, 2, 3, 4)
				{$param_kode_wilayah}
				GROUP BY
				  {$param_nama},
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah},
				  id_level_wilayah_{$param_wilayah},
				  mst_kode_wilayah_{$param_wilayah},
				  {$add_group_induk_provinsi}
				  {$add_group_kode_wilayah_induk_provinsi}
				  {$add_group_induk_kabupaten}
				  {$add_group_kode_wilayah_induk_kabupaten}
				  {$add_group_induk_kecamatan}
				  {$add_group_kode_wilayah_induk_kecamatan}
				ORDER BY
				  CASE WHEN {$param_nama} = 'Luar Negeri' THEN 2 ELSE 1 END, {$param_nama}";

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);

	}

	// end of I Started ...

	function tesSatuanPendidikan(Request $request, Application $app){
		return "koe sp";
	}

	static function SatuanPendidikanWaktuPenyelenggaraanSp(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case 0:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 1:
				$col_wilayah = "w2.nama";
				$col_kode = "w2.kode_wilayah";
				$col_id_level = "w2.id_level_wilayah";
				$col_mst_kode = "w2.mst_kode_wilayah";
				$col_mst_kode_induk = 'w3.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w3.mst_kode_wilayah,';
				$params_wilayah = " and w2.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 2:
				$col_wilayah = "w1.nama";
				$col_kode = "w1.kode_wilayah";
				$col_id_level = "w1.id_level_wilayah";
				$col_mst_kode = "w1.mst_kode_wilayah";
				$col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
				$params_wilayah = " and w1.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 3:
				$col_wilayah = "w1.nama";
				$col_kode = "w1.kode_wilayah";
				$col_id_level = "w1.id_level_wilayah";
				$col_mst_kode = "w1.mst_kode_wilayah";
				$col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
				$params_wilayah = " and w1.kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			default:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 'sekolah.');

		$sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY sekolah.nama) as 'no',
					sekolah.sekolah_id,
					sekolah.nama,
					sekolah.npsn,
					sekolah.status_sekolah,
                    CASE sekolah.status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bp.nama as bentuk_pendidikan_id,
                    bp.nama as bentuk,
					w1.nama as kecamatan,
					w2.nama as kabupaten,
					w3.nama as propinsi,
					getdate() as tanggal_rekap_terakhir,
					isnull((
						SELECT
							wp.nama
						FROM
							sekolah_longitudinal
						LEFT OUTER JOIN ref.waktu_penyelenggaraan wp on wp.waktu_penyelenggaraan_id = sekolah_longitudinal.waktu_penyelenggaraan_id
						WHERE
							soft_delete = 0
						AND semester_id = 20161
						AND sekolah_id = sekolah.sekolah_id
					),'Tidak diisi') as waktu_penyelenggaraan_id_str
				FROM
					sekolah
				INNER JOIN ref.mst_wilayah w1 WITH (nolock) ON LEFT (sekolah.kode_wilayah, 6) = w1.kode_wilayah
				INNER JOIN ref.mst_wilayah w2 WITH (nolock) ON w1.mst_kode_wilayah = w2.kode_wilayah
				INNER JOIN ref.mst_wilayah w3 WITH (nolock) ON w2.mst_kode_wilayah = w3.kode_wilayah
				INNER JOIN ref.bentuk_pendidikan bp WITH (nolock) ON bp.bentuk_pendidikan_id = sekolah.bentuk_pendidikan_id
				WHERE
					sekolah.soft_delete = 0
				{$params_bp}
				{$params_wilayah}
				ORDER BY sekolah.nama";

        // return $sql;die;

        $return = Preface::getDataByQuery($sql);


        $sql .= " OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";


		$final = array();
		$final['results'] = sizeof($return);

		$return = Preface::getDataByQuery($sql);

		$final['rows'] = $return;

		return json_encode($final);

	}

	static function SatuanPendidikanWaktuPenyelenggaraan(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case 0:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 1:
				$col_wilayah = "w2.nama";
				$col_kode = "w2.kode_wilayah";
				$col_id_level = "w2.id_level_wilayah";
				$col_mst_kode = "w2.mst_kode_wilayah";
				$col_mst_kode_induk = 'w3.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w3.mst_kode_wilayah,';
				$params_wilayah = " and w2.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 2:
				$col_wilayah = "w1.nama";
				$col_kode = "w1.kode_wilayah";
				$col_id_level = "w1.id_level_wilayah";
				$col_mst_kode = "w1.mst_kode_wilayah";
				$col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
				$params_wilayah = " and w1.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			default:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 'sekolah.');

		$sql = "SELECT
					ROW_NUMBER() OVER (ORDER BY {$col_wilayah}) as 'no',
					{$col_wilayah} as nama,
					{$col_kode} as kode_wilayah,
					{$col_id_level} as id_level_wilayah,
					{$col_mst_kode} as mst_kode_wilayah,
					{$col_mst_kode_induk}
					sum(case when slong.waktu_penyelenggaraan_id = 1 and status_sekolah = 1 then 1 else 0 end) as negeri_pagi,
					sum(case when slong.waktu_penyelenggaraan_id = 1 and status_sekolah = 2 then 1 else 0 end) as swasta_pagi,
					sum(case when slong.waktu_penyelenggaraan_id = 2 and status_sekolah = 1 then 1 else 0 end) as negeri_siang,
					sum(case when slong.waktu_penyelenggaraan_id = 2 and status_sekolah = 2 then 1 else 0 end) as swasta_siang,
					sum(case when slong.waktu_penyelenggaraan_id = 4 and status_sekolah = 1 then 1 else 0 end) as negeri_sore,
					sum(case when slong.waktu_penyelenggaraan_id = 4 and status_sekolah = 2 then 1 else 0 end) as swasta_sore,
					sum(case when slong.waktu_penyelenggaraan_id = 5 and status_sekolah = 1 then 1 else 0 end) as negeri_malam,
					sum(case when slong.waktu_penyelenggaraan_id = 5 and status_sekolah = 2 then 1 else 0 end) as swasta_malam,
					sum(case when slong.waktu_penyelenggaraan_id = 3 and status_sekolah = 1 then 1 else 0 end) as negeri_kombinasi,
					sum(case when slong.waktu_penyelenggaraan_id = 3 and status_sekolah = 2 then 1 else 0 end) as swasta_kombinasi,
					sum(case when slong.waktu_penyelenggaraan_id = 6 and status_sekolah = 1 then 1 else 0 end) as negeri_sehari_penuh_5,
					sum(case when slong.waktu_penyelenggaraan_id = 6 and status_sekolah = 2 then 1 else 0 end) as swasta_sehari_penuh_5,
					sum(case when slong.waktu_penyelenggaraan_id = 7 and status_sekolah = 1 then 1 else 0 end) as negeri_sehari_penuh_6,
					sum(case when slong.waktu_penyelenggaraan_id = 7 and status_sekolah = 2 then 1 else 0 end) as swasta_sehari_penuh_6,
					sum(case when slong.waktu_penyelenggaraan_id = 9 and status_sekolah = 1 then 1 else 0 end) as negeri_lainnya,
					sum(case when slong.waktu_penyelenggaraan_id = 9 and status_sekolah = 2 then 1 else 0 end) as swasta_lainnya,
					sum(case when slong.waktu_penyelenggaraan_id is null and status_sekolah = 1 then 1 else 0 end) as negeri_belum_isi,
					sum(case when slong.waktu_penyelenggaraan_id is null and status_sekolah = 2 then 1 else 0 end) as swasta_belum_isi,
					sum(1) as jumlah_sekolah,
					getdate() as tanggal_rekap_terakhir
				FROM
					sekolah WITH (nolock)
				LEFT OUTER JOIN sekolah_longitudinal slong WITH (nolock) on sekolah.sekolah_id = slong.sekolah_id
				INNER JOIN ref.mst_wilayah w1 WITH (nolock) ON LEFT (sekolah.kode_wilayah, 6) = w1.kode_wilayah
				INNER JOIN ref.mst_wilayah w2 WITH (nolock) ON w1.mst_kode_wilayah = w2.kode_wilayah
				INNER JOIN ref.mst_wilayah w3 WITH (nolock) ON w2.mst_kode_wilayah = w3.kode_wilayah

				WHERE
					slong.soft_delete = 0
				AND sekolah.soft_delete = 0
				AND slong.semester_id = {$requesta['semester_id']}
				{$params_wilayah}
				{$params_bp}
				GROUP BY
					{$col_wilayah},
					{$col_kode},
					{$col_id_level},
					{$col_mst_kode_induk_group}
					{$col_mst_kode}
				ORDER BY
					{$col_wilayah} ASC";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

	static function SpBentukPendidikan(Request $request, Application $app){
		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case 0:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 1:
				$col_wilayah = "w2.nama";
				$col_kode = "w2.kode_wilayah";
				$col_id_level = "w2.id_level_wilayah";
				$col_mst_kode = "w2.mst_kode_wilayah";
				$col_mst_kode_induk = 'w3.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w3.mst_kode_wilayah,';
				$params_wilayah = " and w2.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			case 2:
				$col_wilayah = "w1.nama";
				$col_kode = "w1.kode_wilayah";
				$col_id_level = "w1.id_level_wilayah";
				$col_mst_kode = "w1.mst_kode_wilayah";
				$col_mst_kode_induk = 'w2.mst_kode_wilayah as mst_kode_wilayah_induk,';
				$col_mst_kode_induk_group = 'w2.mst_kode_wilayah,';
				$params_wilayah = " and w1.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
			default:
				$col_wilayah = "w3.nama";
				$col_kode = "w3.kode_wilayah";
				$col_id_level = "w3.id_level_wilayah";
				$col_mst_kode = "w3.mst_kode_wilayah";
				$col_mst_kode_induk = '';
				$col_mst_kode_induk_group = '';
				$params_wilayah = " and w3.mst_kode_wilayah = '".$requesta['kode_wilayah']."'";
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], 'sekolah.');

		$sql = "SELECT
					(CASE WHEN sekolah.status_sekolah = 1 then bp.nama + ' Negeri' ELSE bp.nama + ' Swasta' END) as desk,
					SUM(1) as jumlah
				FROM
					sekolah
					LEFT JOIN ref.bentuk_pendidikan bp on sekolah.bentuk_pendidikan_id = bp.bentuk_pendidikan_id
					INNER JOIN ref.mst_wilayah w1 WITH (nolock) ON LEFT (sekolah.kode_wilayah, 6) = w1.kode_wilayah
					INNER JOIN ref.mst_wilayah w2 WITH (nolock) ON w1.mst_kode_wilayah = w2.kode_wilayah
					INNER JOIN ref.mst_wilayah w3 WITH (nolock) ON w2.mst_kode_wilayah = w3.kode_wilayah
				WHERE
					sekolah.Soft_delete = 0
				{$params_bp}
				{$params_wilayah}
				GROUP BY
					bp.nama, sekolah.status_sekolah";

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

	static function SatuanPendidikanRingkasan(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_provinsi}
					{$add_kode_wilayah_induk_provinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					sum( case when status_sekolah = 1 then 1 else 0 end ) as negeri,
					sum( case when status_sekolah = 2 then 1 else 0 end ) as swasta,
					sum( case when status_sekolah = 1 then pd else 0 end ) as pd_negeri,
					sum( case when status_sekolah = 2 then pd else 0 end ) as pd_swasta,
					sum( case when status_sekolah = 1 then guru else 0 end ) as guru_negeri,
					sum( case when status_sekolah = 2 then guru else 0 end ) as guru_swasta,
					sum( case when status_sekolah = 1 then pegawai else 0 end ) as pegawai_negeri,
					sum( case when status_sekolah = 2 then pegawai else 0 end ) as pegawai_swasta,
					sum( case when status_sekolah = 1 then rombel else 0 end ) as rombel_negeri,
					sum( case when status_sekolah = 2 then rombel else 0 end ) as rombel_swasta,
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				{$param_kode_wilayah}
				{$params_bp}
				AND soft_delete = 0
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_provinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_provinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
	}

	static function SatuanPendidikanRingkasanDetail(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);
		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				//nothing to do
				$params_wilayah = null;
				break;
			case "1":
				$params_wilayah = 'kode_wilayah_propinsi';
				break;
			case "2":
				$params_wilayah = 'kode_wilayah_kabupaten';
				break;
			case "3":
				$params_wilayah = 'kode_wilayah_kecamatan';
				break;
			default:
				break;
		}

		$sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY nama) as 'no',
					sekolah_id,
					nama,
					npsn,
                    status_sekolah,
                    CASE status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bentuk_pendidikan_id,
                    CASE 
                        WHEN bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
					kecamatan,
				    kode_wilayah_kecamatan,
				    mst_kode_wilayah_kecamatan,
				    id_level_wilayah_kecamatan,
					(case when status_sekolah = 1 or status_sekolah = 2 then pd else 0 end ) as pd,
					(case when status_sekolah = 1 or status_sekolah = 2 then guru else 0 end ) as guru,
					(case when status_sekolah = 1 or status_sekolah = 2 then pegawai else 0 end ) as pegawai,
					(case when status_sekolah = 1 or status_sekolah = 2 then rombel else 0 end ) as rombel,
					tanggal as tanggal_rekap_terakhir
				FROM
					rekap_sekolah WITH(NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
					AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
					AND {$params_wilayah} = '".$requesta['kode_wilayah']."'
					AND soft_delete = 0
					{$params_bp}";

		$return = Preface::getDataByQuery($sql, 'rekap');

		$sql .= " ORDER BY nama OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

		// return $sql;die;

		$final = array();
		$final['results'] = sizeof($return);

		$return = Preface::getDataByQuery($sql, 'rekap');

		$final['rows'] = $return;

		return json_encode($final);
	}

	static function RekapTotal(Request $request, Application $app){

		$request = Preface::Inisiasi($request);

		switch ($request['id_level_wilayah']) {
			case "0":
				$params = 'provinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "provinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$request['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$request['kode_wilayah']."'";

				$add_induk_provinsi = "provinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "provinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		if(TINGKAT == 'DIKMEN'){

			if(BENTUKPENDIDIKAN == 'SMASMK'){
				$params_bp = 'AND bentuk_pendidikan_id IN (13,15)';
			}else if(BENTUKPENDIDIKAN == 'SMA'){
				$params_bp = 'AND bentuk_pendidikan_id = 13';
			}else if(BENTUKPENDIDIKAN == 'SMK'){
				$params_bp = 'AND bentuk_pendidikan_id = 15';
			}else{
				$params_bp = 'AND bentuk_pendidikan_id IN (13,15)';
			}

		}else if(TINGKAT == 'DIKDAS'){

			$params_bp = 'AND bentuk_pendidikan_id IN (5,6,14,29)';

		}else if(TINGKAT == 'DIKDASMEN'){

			$params_bp = 'AND bentuk_pendidikan_id IN (5,6,29,13,15)';

		}else if(TINGKAT == 'DIKDASMEN-PAUD'){
			
			$params_bp = 'AND bentuk_pendidikan_id IN (1,2,3,4,5,6,29,13,15)';

		}

		// if($request['bentuk_pendidikan_id']){
		// 	$params_bp = 'AND bentuk_pendidikan_id = '.$request['bentuk_pendidikan_id'];
		// }else{
		// 	$params_bp = '';
		// }

		$sql = "SELECT
					convert(int, bentuk_pendidikan_id) as bentuk_pendidikan_id,
					(case 
						when bentuk_pendidikan_id = 1 then 'TK' 
						when bentuk_pendidikan_id = 2 then 'KB' 
						when bentuk_pendidikan_id = 3 then 'TPA' 
						when bentuk_pendidikan_id = 4 then 'SPS' 
						when bentuk_pendidikan_id = 5 then 'SD' 
						when bentuk_pendidikan_id = 6 then 'SMP' 
						when bentuk_pendidikan_id in (14,29) then 'SLB' 
						when bentuk_pendidikan_id = 13 then 'SMA' 
						when bentuk_pendidikan_id = 15 then 'SMK' 
						else '-' 
					end) as nama,
					sum( case when status_sekolah = 1 then 1 else 0 end ) as negeri,
					sum( case when status_sekolah = 2 then 1 else 0 end ) as swasta,
					sum( case when status_sekolah = 1 then pd else 0 end ) as pd_negeri,
					sum( case when status_sekolah = 2 then pd else 0 end ) as pd_swasta,
					sum( case when status_sekolah = 1 then guru else 0 end ) as guru_negeri,
					sum( case when status_sekolah = 2 then guru else 0 end ) as guru_swasta,
					sum( case when status_sekolah = 1 then pegawai else 0 end ) as pegawai_negeri,
					sum( case when status_sekolah = 2 then pegawai else 0 end ) as pegawai_swasta,
					sum( case when status_sekolah = 1 then rombel else 0 end ) as rombel_negeri,
					sum( case when status_sekolah = 2 then rombel else 0 end ) as rombel_swasta
				FROM
					rekap_sekolah
				WHERE
					semester_id = '".$request['semester_id']."'
				AND tahun_ajaran_id = '".$request['tahun_ajaran_id']."'
				{$param_kode_wilayah}
				{$params_bp}
				AND soft_delete = 0
				GROUP BY
					bentuk_pendidikan_id";

		if(BENTUKPENDIDIKAN != 'SMA'){
			$sql .= " UNION
					SELECT
						'999' as bentuk_pendidikan_id,
						'Total' as nama,
						sum( case when status_sekolah = 1 then 1 else 0 end ) as negeri,
						sum( case when status_sekolah = 2 then 1 else 0 end ) as swasta,
						sum( case when status_sekolah = 1 then pd else 0 end ) as pd_negeri,
						sum( case when status_sekolah = 2 then pd else 0 end ) as pd_swasta,
						sum( case when status_sekolah = 1 then guru else 0 end ) as guru_negeri,
						sum( case when status_sekolah = 2 then guru else 0 end ) as guru_swasta,
						sum( case when status_sekolah = 1 then pegawai else 0 end ) as pegawai_negeri,
						sum( case when status_sekolah = 2 then pegawai else 0 end ) as pegawai_swasta,
						sum( case when status_sekolah = 1 then rombel else 0 end ) as rombel_negeri,
						sum( case when status_sekolah = 2 then rombel else 0 end ) as rombel_swasta
					FROM
						rekap_sekolah
					WHERE
						semester_id = '".$request['semester_id']."'
					AND tahun_ajaran_id = '".$request['tahun_ajaran_id']."'
					{$param_kode_wilayah}
					{$params_bp}
					AND soft_delete = 0
					ORDER BY bentuk_pendidikan_id";
		}			



		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
	}

	function getDaftarSatuanPendidikan(Request $requests, Application $app){
		$con = Preface::initdb();
		$request = Preface::Inisiasi($requests);

		if($app['session']->get('ta_berjalan')){
			$request['tahun_ajaran_id'] = $app['session']->get('ta_berjalan');
			$request['semester_id'] = $app['session']->get('semester_berjalan');
		}

		// return $request['sekolah_id'];die;

		if($request['kode_wilayah'] != null && $request['kode_wilayah'] != '000000'){
			if($request['id_level_wilayah'] == 3){

				$return = Rekap::getDaftarSatuanPendidikan($requests, $app);

				return $return;
			}

		}else{

			if($request['sekolah_id']){
				$params = "AND sekolah.sekolah_id = '".$request['sekolah_id']."'";
			}

			if($request['nama_npsn']){
				$params_nama = "AND (sekolah.nama like '%".$request['nama_npsn']."%' or sekolah.npsn like '%".$request['nama_npsn']."%')";
			}

			if($request['propinsi'] && $request['propinsi'] != 'null'){
				$params_propinsi = "AND w3.kode_wilayah = '".$request['propinsi']."'";
			}

			if($request['kabupaten'] && $request['kabupaten'] != 'null'){
				$params_kabupaten = "AND w2.kode_wilayah = '".$request['kabupaten']."'";
			}

			if($request['kecamatan'] && $request['kecamatan'] != 'null' && $request['kecamatan'] != 999){
				$params_kecamatan = "AND w1.kode_wilayah = '".$request['kecamatan']."'";
			}

			if($request['bentuk_pendidikan_id'] && $request['bentuk_pendidikan_id'] != 'null' && $request['bentuk_pendidikan_id'] != 999){
				$params_bentuk_pendidikan_id = "AND sekolah.bentuk_pendidikan_id = '".$request['bentuk_pendidikan_id']."'";
			}

			if($request['status_sekolah'] && $request['status_sekolah'] != 'null' && $request['status_sekolah'] != 999){
				$params_status_sekolah = "AND sekolah.status_sekolah = '".$request['status_sekolah']."'";
			}

			$sql = "select
						sekolah.*,
						w1.nama as kecamatan,
						w1.kode_wilayah as kode_wilayah_kecamatan,
						w2.nama as kabupaten,
						w2.kode_wilayah as kode_wilayah_kabupaten,
						w3.nama as propinsi,
						w3.kode_wilayah as kode_wilayah_propinsi,
						bp.nama as bentuk_pendidikan_id_str
					from
						sekolah
					INNER JOIN ref.mst_wilayah w1 ON LEFT (sekolah.kode_wilayah, 6) = w1.kode_wilayah
					INNER JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
					INNER JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
					LEFT OUTER JOIN ref.bentuk_pendidikan bp on bp.bentuk_pendidikan_id = sekolah.bentuk_pendidikan_id
					where sekolah.Soft_delete = 0
					{$params}
					{$params_nama}
					{$params_propinsi}
					{$params_kabupaten}
					{$params_kecamatan}
					{$params_bentuk_pendidikan_id}
					{$params_status_sekolah}
					ORDER BY sekolah.nama";

			// return $sql;die;

			$return = Preface::getDataByQuery($sql);

			return json_encode($return);

		}


	}

	function getKoreg(){
		$koreg = rAND(10000000,99999999);

		$sql = "select * from sekolah where kode_registrasi = '".$koreg."'";

		$return = Preface::getDataByQuery($sql);

		if(sizeof($return) > 0){
			SatuanPendidikan::getKoreg();
		}else{
			return $koreg;
		}

	}

	function regenKoreg(Request $request,Application $app){
		$con = Preface::initdb();
		$requests = Preface::Inisiasi($request);
		$koreg = SatuanPendidikan::getKoreg();

		$sql = "update sekolah set kode_registrasi = '".$koreg."' where sekolah_id = '".$requests['sekolah_id']."'";

		$return = Preface::execQuery($sql);

		return json_encode($return);

	}

	function unduhExcelKoreg(Request $request,Application $app){

		$return  = SatuanPendidikan::getDaftarSatuanPendidikan($request,$app);

		// return $return;

		$data = json_decode($return);

		$objPHPExcel = new \PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Khalid Saifuddin")
									 ->setLastModifiedBy("Khalid Saifuddin")
									 ->setTitle("Kode Registrasi")
									 ->setSubject("Kode Registrasi")
									 ->setDescription("daftar kode registrasi per satuan pendidikan")
									 ->setKeywords("office 2007 openxml php")
									 ->setCategory("result file");
		$i = 2;

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Nama SP');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', 'NPSN');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', 'Bentuk Pendidikan');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', 'Status');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', 'Kecamatan');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', 'Kabupaten');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', 'Propinsi');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', 'Kode Registrasi');

		$styleArray = array(
		  'borders' => array(
		    'allborders' => array(
		      'style' => \PHPExcel_Style_Border::BORDER_THIN
		    )
		  )
		);

		$objPHPExcel->getActiveSheet()->getStyle('A1:H'.(sizeof($data)+1) )->applyFromArray($styleArray);
		unset($styleArray);

		$objPHPExcel->getActiveSheet()
		    ->getStyle('A1:H1')
		    ->getAlignment()
		    ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objPHPExcel->getActiveSheet()
		    ->getStyle('C1:C'.(sizeof($data)+1) )
		    ->getAlignment()
		    ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


		$objPHPExcel->getActiveSheet()
		    ->getStyle('H1:H'.(sizeof($data)+1) )
		    ->getAlignment()
		    ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


		$objPHPExcel->getActiveSheet()
		    ->getStyle('A1:H1')
		    ->getFont()->setBold(true);

		$styleArray = array(
	        'fill' => array(
	            'type' => \PHPExcel_Style_Fill::FILL_SOLID,
	            'color' => array('rgb' => 'CCCCCC')
	        )
	    );

	    $objPHPExcel->getActiveSheet()
		    ->getStyle('A1:H1')
		    ->applyFromArray($styleArray);
		unset($styleArray);

		foreach ($data as $d) {

			$kabupaten = $d->{'kabupaten'};
			$propinsi = $d->{'propinsi'};


			switch ($d->{'status_sekolah'}) {
				case '1':
					$status = 'Negeri';
					break;
				case '2':
					$status = 'Swasta';
					break;
				default:
					$status = 'Negeri';
					break;
			}

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $d->{'nama'});
		    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);

		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$i, $d->{'npsn'});
		    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);

		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$i, $d->{'bentuk_pendidikan_id_str'});
		    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);

		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, $status);
		    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);

		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$i, $d->{'kecamatan'});
		    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);

		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, $d->{'kabupaten'});
		    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);

		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$i, $d->{'propinsi'});
		    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);

		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, $d->{'kode_registrasi'});
		    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);

		    $i++;

		}

		$objPHPExcel->getActiveSheet()->setTitle('Daftar Kode Registrasi');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);


		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		// header('Content-Disposition: attachment;filename="Daftar Kode Registrasi - '.date('Y-m-d H:i:s').'.xlsx"');
		header('Content-Disposition: attachment;filename="Daftar Kode Registrasi - '.date('Y-m-d H:i:s').' - '.$kabupaten.' - '.$propinsi.'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');

		return true;

	}

	function getDaftarPengiriman(Request $request, Application $app){

		$requests = Preface::Inisiasi($request);

		$start = $requests['start'] ? $requests['start'] : 0;
		$limit = $requests['limit'] ? $requests['limit'] : 20;
		// $tanggal = $request->get('tanggal');

		$rentang = $request->get('rentang');
		$nama_npsn = $request->get('nama_npsn');
		$status = $request->get('status');

		if($rentang){
			$rentangs = explode(" - ", $rentang);

			// return json_encode($rentangs);die;

			$awal = explode("/", $rentangs[0]);
			$batas_awal = $awal[2]."-".$awal[0]."-".$awal[1];

			$akhir = explode("/", $rentangs[1]);
			$batas_akhir = $akhir[2]."-".$akhir[0]."-".$akhir[1];

			$where_waktu = "WHERE tgl_kirim >= '".$batas_awal." 00:00:00' AND tgl_kirim <=  '".$batas_akhir." 23:59:59'";
		}else{
			$where_waktu = '';
		}


		if($nama_npsn){
			$where_nama = " AND (sekolah.nama like '%".$nama_npsn."%' OR sekolah.npsn = '".$nama_npsn."')";
		}else{
			$where_nama = '';
		}

		if($status){

			if($status != 999){

				if($status == 2 || $status == 3){
					$where_status = " AND status in (2,3)";
				}else{
					$where_status = " AND status = '".$status."'";
				}

			}else{
				$where_status = "";
			}

		}

		if($requests['propinsi'] && $requests['propinsi'] != 'null'){
			$params_propinsi = "AND prop.kode_wilayah = '".$requests['propinsi']."'";
		}

		if($requests['kabupaten'] && $requests['kabupaten'] != 'null'){
			$params_kabupaten = "AND kab.kode_wilayah = '".$requests['kabupaten']."'";
		}

		if($requests['kecamatan'] && $requests['kecamatan'] != 'null' && $requests['kecamatan'] != 999){
			$params_kecamatan = "AND kec.kode_wilayah = '".$requests['kecamatan']."'";
		}

		$sql = "SELECT
					left(cast(tgl_kirim as varchar(100)),12) as tanggal,
					sekolah.sekolah_id,
					sekolah.nama,
					sekolah.npsn,
					bp.nama as bentuk_pendidikan_id_str,
					sekolah.status_sekolah,
					kec.nama as kecamatan,
					kab.nama as kabupaten,
					prop.nama as propinsi,
					(case when sekolah.status_sekolah = 1 then 'Negeri' else 'Swasta' end) as status_sekolah_str,
					tgl_kirim,
					cara_kirim,
					(case when cara_kirim = 1 then 'Online' else 'Offline' end) as cara_kirim_str,
					status,
					log,
					register_pengiriman_id,
					(case when status = 1 then 'Diterima' when status in (2,3) then 'Diproses' when status = 4 then 'Selesai' when status = 5 then 'Gagal Diproses' else 'Diproses' end) as status_str
				FROM
					send.register_pengiriman kirim
				JOIN sekolah ON sekolah.sekolah_id = kirim.sekolah_id
				JOIN ref.bentuk_pendidikan bp on bp.bentuk_pendidikan_id = sekolah.bentuk_pendidikan_id
				JOIN ref.mst_wilayah kec on kec.kode_wilayah = left(sekolah.kode_wilayah,6)
				JOIN ref.mst_wilayah kab on kab.kode_wilayah = kec.mst_kode_wilayah
				JOIN ref.mst_wilayah prop on prop.kode_wilayah = kab.mst_kode_wilayah
				{$where_waktu}
				{$where_nama}
				{$where_status}
				{$params_propinsi}
				{$params_kabupaten}
				{$params_kecamatan}
				ORDER BY
					tgl_kirim DESC OFFSET {$start} ROWS FETCH NEXT {$limit} ROWS ONLY";

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

	function getTanggalPengiriman(Request $request, Application $app){
		$rentang = $request->get('rentang');
		$nama_npsn = $request->get('nama_npsn');
		$status = $request->get('status');

		$requests = Preface::Inisiasi($request);

		$start = $requests['start'] ? $requests['start'] : 0;
		$limit = $requests['limit'] ? $requests['limit'] : 20;

		if($rentang){
			$rentangs = explode(" - ", $rentang);

			// return json_encode($rentangs);die;

			$awal = explode("/", $rentangs[0]);
			$batas_awal = $awal[2]."-".$awal[0]."-".$awal[1];

			$akhir = explode("/", $rentangs[1]);
			$batas_akhir = $akhir[2]."-".$akhir[0]."-".$akhir[1];

			$where_waktu = "WHERE tgl_kirim >= '".$batas_awal." 00:00:00' AND tgl_kirim <=  '".$batas_akhir." 23:59:59'";
		}else{
			$where_waktu = '';
		}

		if($nama_npsn){
			$where_nama = " AND (sekolah.nama like '%".$nama_npsn."%' OR sekolah.npsn = '".$nama_npsn."')";
		}else{
			$where_nama = '';
		}

		if($requests['propinsi'] && $requests['propinsi'] != 'null'){
			$params_propinsi = "AND prop.kode_wilayah = '".$requests['propinsi']."'";
		}

		if($requests['kabupaten'] && $requests['kabupaten'] != 'null'){
			$params_kabupaten = "AND kab.kode_wilayah = '".$requests['kabupaten']."'";
		}

		if($requests['kecamatan'] && $requests['kecamatan'] != 'null' && $requests['kecamatan'] != 999){
			$params_kecamatan = "AND kec.kode_wilayah = '".$requests['kecamatan']."'";
		}

		$status = $request->get('status');
		if($status){

			if($status != 999){

				if($status == 2 || $status == 3){
					$where_status = " AND status in (2,3)";
				}else{
					$where_status = " AND status = '".$status."'";
				}

			}else{
				$where_status = "";
			}

		}

		$sql = "SELECT
					left(cast(tgl_kirim as varchar(100)),12) as tanggal
				FROM
					send.register_pengiriman kirim
				JOIN sekolah ON sekolah.sekolah_id = kirim.sekolah_id
				JOIN ref.mst_wilayah kec on kec.kode_wilayah = left(sekolah.kode_wilayah,6)
				JOIN ref.mst_wilayah kab on kab.kode_wilayah = kec.mst_kode_wilayah
				JOIN ref.mst_wilayah prop on prop.kode_wilayah = kab.mst_kode_wilayah
				{$where_waktu}
				{$where_nama}
				{$where_status}
				{$params_propinsi}
				{$params_kabupaten}
				{$params_kecamatan}
				GROUP BY left(cast(tgl_kirim as varchar(100)),12)
				ORDER BY
					left(cast(tgl_kirim as varchar(100)),12) DESC";

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

	function getRekapSekolahStatus(){
		$sql = "SELECT
					SUM (case when status_sekolah =  1 then 1 else 0 end) as negeri,
					SUM (case when status_sekolah =  2 then 1 else 0 end) as swasta
				FROM
					sekolah
				where soft_delete = 0";

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

	function getRekapSekolahBentukPendidikan(){
		$sql = "SELECT
					SUM (case when bentuk_pendidikan_id =  1 then 1 else 0 end) as tk,
					SUM (case when bentuk_pendidikan_id =  2 then 1 else 0 end) as kb,
					SUM (case when bentuk_pendidikan_id =  3 then 1 else 0 end) as tpa,
					SUM (case when bentuk_pendidikan_id =  4 then 1 else 0 end) as sps
				FROM
					sekolah
				where soft_delete = 0";

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

	function getSekolahLongitudinal(Request $request, Application $app){
		$requests = Preface::Inisiasi($request);

		$sql = "SELECT
					wp.nama as waktu_penyelenggaraan_id_str,
					listrik.nama sumber_listrik_id_str,
					iso.nama as sertifikasi_iso_id_str,
					internet.nama as akses_internet_id_str,
					internet2.nama as akses_internet_2_id_str,
					sem.nama as semester_id_str,
					sl.*
				FROM
					sekolah_longitudinal sl
				LEFT OUTER JOIN ref.waktu_penyelenggaraan wp on wp.waktu_penyelenggaraan_id = sl.waktu_penyelenggaraan_id
				LEFT OUTER JOIN ref.sumber_listrik listrik on listrik.sumber_listrik_id = sl.sumber_listrik_id
				LEFT OUTER JOIN ref.sertifikasi_iso iso on iso.sertifikasi_iso_id = sl.sertifikasi_iso_id
				LEFT OUTER JOIN ref.akses_internet internet on internet.akses_internet_id = sl.akses_internet_id
				LEFT OUTER JOIN ref.akses_internet internet2 on internet2.akses_internet_id = sl.akses_internet_2_id
				LEFT OUTER JOIN ref.semester sem on sem.semester_id = sl.semester_id
				WHERE
					sl.soft_delete = 0
				AND sl.sekolah_id = '".$requests['sekolah_id']."'
				AND sl.semester_id = '".$requests['semester_id']."'";

		$return = Preface::getDataByQuery($sql);

		return json_encode($return);
	}

    function getLayananSatuanPendidikan(Request $request, Application $app) {
        $con = Preface::initdb('rekap');
        $request = Preface::Inisiasi($request);

        switch ($request['id_level_wilayah']) {
            case "0":
                $param_nama = 'provinsi';
                $param_kode_nama = '';
                $param_wilayah = 'provinsi';
                $param_kode_wilayah = '';

                $add_induk_provinsi = "null as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "1":
                $param_nama = 'kabupaten';
                $param_kode_nama = '';
                $param_wilayah = 'kabupaten';
                $param_kode_wilayah = "AND kode_wilayah_provinsi = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "2":
                $param_nama = 'kecamatan';
                $param_kode_nama = '';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "3":
                $param_nama = 'nama';
                $param_kode_nama = 'sekolah_id,';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kecamatan = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "kecamatan as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten,";
                $add_group_induk_kecamatan = "kecamatan,";
                $add_group_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan";
                break;
            default:
                break;
        }

        $sql = "SELECT
				  {$param_nama} as nama,
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah} as kode_wilayah,
				  id_level_wilayah_{$param_wilayah} as id_level_wilayah,
				  mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
				  {$add_induk_provinsi}
				  {$add_kode_wilayah_induk_provinsi}
				  {$add_induk_kabupaten}
				  {$add_kode_wilayah_induk_kabupaten}
				  {$add_induk_kecamatan}
				  {$add_kode_wilayah_induk_kecamatan}
				  SUM(CASE WHEN fasilitas_layanan_id = 1 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_studi_banding,
				  SUM(CASE WHEN fasilitas_layanan_id = 2 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_magang,
				  SUM(CASE WHEN fasilitas_layanan_id = 3 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_pelatihan,
				  SUM(CASE WHEN fasilitas_layanan_id in (1,2,3) AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_total,

				  SUM(CASE WHEN fasilitas_layanan_id = 1 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_studi_banding,
				  SUM(CASE WHEN fasilitas_layanan_id = 2 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_magang,
				  SUM(CASE WHEN fasilitas_layanan_id = 3 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_pelatihan,
				  SUM(CASE WHEN fasilitas_layanan_id in (1,2,3) AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_total,

				  SUM(CASE WHEN fasilitas_layanan_id = 1 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_studi_banding,
				  SUM(CASE WHEN fasilitas_layanan_id = 2 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_magang,
				  SUM(CASE WHEN fasilitas_layanan_id = 3 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_pelatihan,
				  SUM(CASE WHEN fasilitas_layanan_id in (1,2,3) AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_total,

				  SUM(CASE WHEN fasilitas_layanan_id = 1 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_studi_banding,
				  SUM(CASE WHEN fasilitas_layanan_id = 2 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_magang,
				  SUM(CASE WHEN fasilitas_layanan_id = 3 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_pelatihan,
				  SUM(CASE WHEN fasilitas_layanan_id in (1,2,3) AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_total,

				  SUM(CASE WHEN fasilitas_layanan_id in (1,2,3) AND bentuk_pendidikan_id in (1,2,3,4) then 1 else 0 end ) as total_layanan
				FROM
				  dbo.rekap_sekolah
				WHERE
					semester_id is null
				AND tahun_ajaran_id = '".$request['tahun_ajaran_id']."'
				AND bentuk_pendidikan_id IN (1, 2, 3, 4)
				{$param_kode_wilayah}
				GROUP BY
				  {$param_nama},
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah},
				  id_level_wilayah_{$param_wilayah},
				  mst_kode_wilayah_{$param_wilayah},
				  {$add_group_induk_provinsi}
				  {$add_group_kode_wilayah_induk_provinsi}
				  {$add_group_induk_kabupaten}
				  {$add_group_kode_wilayah_induk_kabupaten}
				  {$add_group_induk_kecamatan}
				  {$add_group_kode_wilayah_induk_kecamatan}
				ORDER BY
				  CASE WHEN {$param_nama} = 'Luar Negeri' THEN 2 ELSE 1 END, {$param_nama}";

        $return = Preface::getDataByQuery($sql, 'rekap');

       return json_encode($return);
        // return $sql;
    }

    function getJadwalKesehatanSatuanPendidikan(Request $request, Application $app) {
        $con = Preface::initdb('rekap');
        $request = Preface::Inisiasi($request);

        switch ($request['id_level_wilayah']) {
            case "0":
                $param_nama = 'provinsi';
                $param_kode_nama = '';
                $param_wilayah = 'provinsi';
                $param_kode_wilayah = '';

                $add_induk_provinsi = "null as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "1":
                $param_nama = 'kabupaten';
                $param_kode_nama = '';
                $param_wilayah = 'kabupaten';
                $param_kode_wilayah = "AND kode_wilayah_provinsi = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "2":
                $param_nama = 'kecamatan';
                $param_kode_nama = '';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "3":
                $param_nama = 'nama';
                $param_kode_nama = 'sekolah_id,';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kecamatan = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "kecamatan as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten,";
                $add_group_induk_kecamatan = "kecamatan,";
                $add_group_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan";
                break;
            default:
                break;
        }

		$sql = "SELECT
				  {$param_nama} as nama,
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah} as kode_wilayah,
				  id_level_wilayah_{$param_wilayah} as id_level_wilayah,
				  mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
				  {$add_induk_provinsi}
				  {$add_kode_wilayah_induk_provinsi}
				  {$add_induk_kabupaten}
				  {$add_kode_wilayah_induk_kabupaten}
				  {$add_induk_kecamatan}
				  {$add_kode_wilayah_induk_kecamatan}
				  sum(case when jadwal_kesehatan_id = 3 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_satu_bulan,
                  sum(case when jadwal_kesehatan_id = 4 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_tiga_bulan,
                  sum(case when jadwal_kesehatan_id = 5 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_enam_bulan,
                  sum(case when jadwal_kesehatan_id = 6 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_satu_tahun,
                  sum(case when jadwal_kesehatan_id = 99 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_belum_ada,
                  sum(case when jadwal_kesehatan_id = 0 and bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_belum_isi,

                  sum(case when jadwal_kesehatan_id = 3 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_satu_bulan,
                  sum(case when jadwal_kesehatan_id = 4 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_tiga_bulan,
                  sum(case when jadwal_kesehatan_id = 5 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_enam_bulan,
                  sum(case when jadwal_kesehatan_id = 6 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_satu_tahun,
                  sum(case when jadwal_kesehatan_id = 99 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_belum_ada,
                  sum(case when jadwal_kesehatan_id = 0 and bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_belum_isi,

                  sum(case when jadwal_kesehatan_id = 3 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_satu_bulan,
                  sum(case when jadwal_kesehatan_id = 4 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_tiga_bulan,
                  sum(case when jadwal_kesehatan_id = 5 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_enam_bulan,
                  sum(case when jadwal_kesehatan_id = 6 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_satu_tahun,
                  sum(case when jadwal_kesehatan_id = 99 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_belum_ada,
                  sum(case when jadwal_kesehatan_id = 0 and bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_belum_isi,

                  sum(case when jadwal_kesehatan_id = 3 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_satu_bulan,
                  sum(case when jadwal_kesehatan_id = 4 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_tiga_bulan,
                  sum(case when jadwal_kesehatan_id = 5 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_enam_bulan,
                  sum(case when jadwal_kesehatan_id = 6 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_satu_tahun,
                  sum(case when jadwal_kesehatan_id = 99 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_belum_ada,
                  sum(case when jadwal_kesehatan_id = 0 and bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_belum_isi

				FROM
				  dbo.rekap_sekolah
				WHERE
					semester_id is null
				AND tahun_ajaran_id = '".$request['tahun_ajaran_id']."'
				AND bentuk_pendidikan_id IN (1, 2, 3, 4)
				{$param_kode_wilayah}
				GROUP BY
				  {$param_nama},
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah},
				  id_level_wilayah_{$param_wilayah},
				  mst_kode_wilayah_{$param_wilayah},
				  {$add_group_induk_provinsi}
				  {$add_group_kode_wilayah_induk_provinsi}
				  {$add_group_induk_kabupaten}
				  {$add_group_kode_wilayah_induk_kabupaten}
				  {$add_group_induk_kecamatan}
				  {$add_group_kode_wilayah_induk_kecamatan}
				ORDER BY
				  CASE WHEN {$param_nama} = 'Luar Negeri' THEN 2 ELSE 1 END, {$param_nama}";

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
//		return $sql;
	}

    function getJadwalPmtasSatuanPendidikan(Request $request, Application $app) {
        $con = Preface::initdb('rekap');
        $request = Preface::Inisiasi($request);

        switch ($request['id_level_wilayah']) {
            case "0":
                $param_nama = 'provinsi';
                $param_kode_nama = '';
                $param_wilayah = 'provinsi';
                $param_kode_wilayah = '';

                $add_induk_provinsi = "null as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "1":
                $param_nama = 'kabupaten';
                $param_kode_nama = '';
                $param_wilayah = 'kabupaten';
                $param_kode_wilayah = "AND kode_wilayah_provinsi = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "2":
                $param_nama = 'kecamatan';
                $param_kode_nama = '';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "3":
                $param_nama = 'nama';
                $param_kode_nama = 'sekolah_id,';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kecamatan = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "kecamatan as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten,";
                $add_group_induk_kecamatan = "kecamatan,";
                $add_group_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan";
                break;
            default:
                break;
        }

        $sql = "SELECT
				  {$param_nama} as nama,
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah} as kode_wilayah,
				  id_level_wilayah_{$param_wilayah} as id_level_wilayah,
				  mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
				  {$add_induk_provinsi}
				  {$add_kode_wilayah_induk_provinsi}
				  {$add_induk_kabupaten}
				  {$add_kode_wilayah_induk_kabupaten}
				  {$add_induk_kecamatan}
				  {$add_kode_wilayah_induk_kecamatan}

				  sum(case when jadwal_pmtas_id = 3 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_satu_bulan,
                  sum(case when jadwal_pmtas_id = 4 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_tiga_bulan,
                  sum(case when jadwal_pmtas_id = 5 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_enam_bulan,
                  sum(case when jadwal_pmtas_id = 6 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_satu_tahun,
                  sum(case when jadwal_pmtas_id = 99 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_belum_ada,
                  sum(case when jadwal_pmtas_id = 0 and bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_belum_isi,

                  sum(case when jadwal_pmtas_id = 3 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_satu_bulan,
                  sum(case when jadwal_pmtas_id = 4 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_tiga_bulan,
                  sum(case when jadwal_pmtas_id = 5 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_enam_bulan,
                  sum(case when jadwal_pmtas_id = 6 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_satu_tahun,
                  sum(case when jadwal_pmtas_id = 99 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_belum_ada,
                  sum(case when jadwal_pmtas_id = 0 and bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_belum_isi,

                  sum(case when jadwal_pmtas_id = 3 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_satu_bulan,
                  sum(case when jadwal_pmtas_id = 4 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_tiga_bulan,
                  sum(case when jadwal_pmtas_id = 5 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_enam_bulan,
                  sum(case when jadwal_pmtas_id = 6 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_satu_tahun,
                  sum(case when jadwal_pmtas_id = 99 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_belum_ada,
                  sum(case when jadwal_pmtas_id = 0 and bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_belum_isi,

                  sum(case when jadwal_pmtas_id = 3 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_satu_bulan,
                  sum(case when jadwal_pmtas_id = 4 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_tiga_bulan,
                  sum(case when jadwal_pmtas_id = 5 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_enam_bulan,
                  sum(case when jadwal_pmtas_id = 6 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_satu_tahun,
                  sum(case when jadwal_pmtas_id = 99 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_belum_ada,
                  sum(case when jadwal_pmtas_id = 0 and bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_belum_isi

				FROM
				  dbo.rekap_sekolah
				WHERE
					semester_id is null
				AND tahun_ajaran_id = '".$request['tahun_ajaran_id']."'
				AND bentuk_pendidikan_id IN (1, 2, 3, 4)
				{$param_kode_wilayah}
				GROUP BY
				  {$param_nama},
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah},
				  id_level_wilayah_{$param_wilayah},
				  mst_kode_wilayah_{$param_wilayah},
				  {$add_group_induk_provinsi}
				  {$add_group_kode_wilayah_induk_provinsi}
				  {$add_group_induk_kabupaten}
				  {$add_group_kode_wilayah_induk_kabupaten}
				  {$add_group_induk_kecamatan}
				  {$add_group_kode_wilayah_induk_kecamatan}
				ORDER BY
				  CASE WHEN {$param_nama} = 'Luar Negeri' THEN 2 ELSE 1 END, {$param_nama}";

        $return = Preface::getDataByQuery($sql, 'rekap');

        return json_encode($return);
//		return $sql;
    }

    function getJadwalDdtkSatuanPendidikan(Request $request, Application $app) {
        $con = Preface::initdb('rekap');
        $request = Preface::Inisiasi($request);

        switch ($request['id_level_wilayah']) {
            case "0":
                $param_nama = 'provinsi';
                $param_kode_nama = '';
                $param_wilayah = 'provinsi';
                $param_kode_wilayah = '';

                $add_induk_provinsi = "null as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "1":
                $param_nama = 'kabupaten';
                $param_kode_nama = '';
                $param_wilayah = 'kabupaten';
                $param_kode_wilayah = "AND kode_wilayah_provinsi = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "2":
                $param_nama = 'kecamatan';
                $param_kode_nama = '';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "3":
                $param_nama = 'nama';
                $param_kode_nama = 'sekolah_id,';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kecamatan = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "kecamatan as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten,";
                $add_group_induk_kecamatan = "kecamatan,";
                $add_group_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan";
                break;
            default:
                break;
        }

        $sql = "SELECT
				  {$param_nama} as nama,
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah} as kode_wilayah,
				  id_level_wilayah_{$param_wilayah} as id_level_wilayah,
				  mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
				  {$add_induk_provinsi}
				  {$add_kode_wilayah_induk_provinsi}
				  {$add_induk_kabupaten}
				  {$add_kode_wilayah_induk_kabupaten}
				  {$add_induk_kecamatan}
				  {$add_kode_wilayah_induk_kecamatan}
				  sum(case when jadwal_ddtk_id = 3 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_satu_bulan,
                  sum(case when jadwal_ddtk_id = 4 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_tiga_bulan,
                  sum(case when jadwal_ddtk_id = 5 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_enam_bulan,
                  sum(case when jadwal_ddtk_id = 6 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_satu_tahun,
                  sum(case when jadwal_ddtk_id = 99 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_belum_ada,
                  sum(case when jadwal_ddtk_id = 0 and bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_belum_isi,

                  sum(case when jadwal_ddtk_id = 3 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_satu_bulan,
                  sum(case when jadwal_ddtk_id = 4 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_tiga_bulan,
                  sum(case when jadwal_ddtk_id = 5 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_enam_bulan,
                  sum(case when jadwal_ddtk_id = 6 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_satu_tahun,
                  sum(case when jadwal_ddtk_id = 99 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_belum_ada,
                  sum(case when jadwal_ddtk_id = 0 and bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_belum_isi,

                  sum(case when jadwal_ddtk_id = 3 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_satu_bulan,
                  sum(case when jadwal_ddtk_id = 4 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_tiga_bulan,
                  sum(case when jadwal_ddtk_id = 5 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_enam_bulan,
                  sum(case when jadwal_ddtk_id = 6 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_satu_tahun,
                  sum(case when jadwal_ddtk_id = 99 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_belum_ada,
                  sum(case when jadwal_ddtk_id = 0 and bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_belum_isi,

                  sum(case when jadwal_ddtk_id = 3 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_satu_bulan,
                  sum(case when jadwal_ddtk_id = 4 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_tiga_bulan,
                  sum(case when jadwal_ddtk_id = 5 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_enam_bulan,
                  sum(case when jadwal_ddtk_id = 6 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_satu_tahun,
                  sum(case when jadwal_ddtk_id = 99 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_belum_ada,
                  sum(case when jadwal_ddtk_id = 0 and bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_belum_isi

				FROM
				  dbo.rekap_sekolah
				WHERE
					semester_id is null
				AND tahun_ajaran_id = '".$request['tahun_ajaran_id']."'
				AND bentuk_pendidikan_id IN (1, 2, 3, 4)
				{$param_kode_wilayah}
				GROUP BY
				  {$param_nama},
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah},
				  id_level_wilayah_{$param_wilayah},
				  mst_kode_wilayah_{$param_wilayah},
				  {$add_group_induk_provinsi}
				  {$add_group_kode_wilayah_induk_provinsi}
				  {$add_group_induk_kabupaten}
				  {$add_group_kode_wilayah_induk_kabupaten}
				  {$add_group_induk_kecamatan}
				  {$add_group_kode_wilayah_induk_kecamatan}
				ORDER BY
				  CASE WHEN {$param_nama} = 'Luar Negeri' THEN 2 ELSE 1 END, {$param_nama}";

        $return = Preface::getDataByQuery($sql, 'rekap');

        return json_encode($return);
//		return $sql;
    }

    function getPencatatanDdtkSatuanPendidikan(Request $request, Application $app) {
        $con = Preface::initdb('rekap');
        $request = Preface::Inisiasi($request);

        switch ($request['id_level_wilayah']) {
            case "0":
                $param_nama = 'provinsi';
                $param_kode_nama = '';
                $param_wilayah = 'provinsi';
                $param_kode_wilayah = '';

                $add_induk_provinsi = "null as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "1":
                $param_nama = 'kabupaten';
                $param_kode_nama = '';
                $param_wilayah = 'kabupaten';
                $param_kode_wilayah = "AND kode_wilayah_provinsi = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "2":
                $param_nama = 'kecamatan';
                $param_kode_nama = '';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "3":
                $param_nama = 'nama';
                $param_kode_nama = 'sekolah_id,';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kecamatan = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "kecamatan as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten,";
                $add_group_induk_kecamatan = "kecamatan,";
                $add_group_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan";
                break;
            default:
                break;
        }

        $sql = "SELECT
				  {$param_nama} as nama,
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah} as kode_wilayah,
				  id_level_wilayah_{$param_wilayah} as id_level_wilayah,
				  mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
				  {$add_induk_provinsi}
				  {$add_kode_wilayah_induk_provinsi}
				  {$add_induk_kabupaten}
				  {$add_kode_wilayah_induk_kabupaten}
				  {$add_induk_kecamatan}
				  {$add_kode_wilayah_induk_kecamatan}

				  sum(case when pencatatan_ddtk = 1 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_ada,
                  sum(case when pencatatan_ddtk = 2 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_tidak_ada,

                  sum(case when pencatatan_ddtk = 1 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_ada,
                  sum(case when pencatatan_ddtk = 2 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_tidak_ada,

                  sum(case when pencatatan_ddtk = 1 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_ada,
                  sum(case when pencatatan_ddtk = 2 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_tidak_ada,

                  sum(case when pencatatan_ddtk = 1 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_ada,
                  sum(case when pencatatan_ddtk = 2 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_tidak_ada

				FROM
				  dbo.rekap_sekolah
				WHERE
					semester_id is null
				AND tahun_ajaran_id = '".$request['tahun_ajaran_id']."'
				AND bentuk_pendidikan_id IN (1, 2, 3, 4)
				{$param_kode_wilayah}
				GROUP BY
				  {$param_nama},
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah},
				  id_level_wilayah_{$param_wilayah},
				  mst_kode_wilayah_{$param_wilayah},
				  {$add_group_induk_provinsi}
				  {$add_group_kode_wilayah_induk_provinsi}
				  {$add_group_induk_kabupaten}
				  {$add_group_kode_wilayah_induk_kabupaten}
				  {$add_group_induk_kecamatan}
				  {$add_group_kode_wilayah_induk_kecamatan}
				ORDER BY
				  CASE WHEN {$param_nama} = 'Luar Negeri' THEN 2 ELSE 1 END, {$param_nama}";

        $return = Preface::getDataByQuery($sql, 'rekap');

        return json_encode($return);
//		return $sql;
    }

    function getRujukanDdtkSatuanPendidikan(Request $request, Application $app) {
        $con = Preface::initdb('rekap');
        $request = Preface::Inisiasi($request);

        switch ($request['id_level_wilayah']) {
            case "0":
                $param_nama = 'provinsi';
                $param_kode_nama = '';
                $param_wilayah = 'provinsi';
                $param_kode_wilayah = '';

                $add_induk_provinsi = "null as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "1":
                $param_nama = 'kabupaten';
                $param_kode_nama = '';
                $param_wilayah = 'kabupaten';
                $param_kode_wilayah = "AND kode_wilayah_provinsi = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "2":
                $param_nama = 'kecamatan';
                $param_kode_nama = '';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "3":
                $param_nama = 'nama';
                $param_kode_nama = 'sekolah_id,';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kecamatan = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "kecamatan as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten,";
                $add_group_induk_kecamatan = "kecamatan,";
                $add_group_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan";
                break;
            default:
                break;
        }

        $sql = "SELECT
				  {$param_nama} as nama,
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah} as kode_wilayah,
				  id_level_wilayah_{$param_wilayah} as id_level_wilayah,
				  mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
				  {$add_induk_provinsi}
				  {$add_kode_wilayah_induk_provinsi}
				  {$add_induk_kabupaten}
				  {$add_kode_wilayah_induk_kabupaten}
				  {$add_induk_kecamatan}
				  {$add_kode_wilayah_induk_kecamatan}

				  sum(case when rujukan_ddtk = 1 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_ada,
                  sum(case when rujukan_ddtk = 2 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_tidak_ada,

                  sum(case when rujukan_ddtk = 1 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_ada,
                  sum(case when rujukan_ddtk = 2 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_tidak_ada,

                  sum(case when rujukan_ddtk = 1 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_ada,
                  sum(case when rujukan_ddtk = 2 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_tidak_ada,

                  sum(case when rujukan_ddtk = 1 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_ada,
                  sum(case when rujukan_ddtk = 2 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_tidak_ada

				FROM
				  dbo.rekap_sekolah
				WHERE
					semester_id is null
				AND tahun_ajaran_id = '".$request['tahun_ajaran_id']."'
				AND bentuk_pendidikan_id IN (1, 2, 3, 4)
				{$param_kode_wilayah}
				GROUP BY
				  {$param_nama},
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah},
				  id_level_wilayah_{$param_wilayah},
				  mst_kode_wilayah_{$param_wilayah},
				  {$add_group_induk_provinsi}
				  {$add_group_kode_wilayah_induk_provinsi}
				  {$add_group_induk_kabupaten}
				  {$add_group_kode_wilayah_induk_kabupaten}
				  {$add_group_induk_kecamatan}
				  {$add_group_kode_wilayah_induk_kecamatan}
				ORDER BY
				  CASE WHEN {$param_nama} = 'Luar Negeri' THEN 2 ELSE 1 END, {$param_nama}";

        $return = Preface::getDataByQuery($sql, 'rekap');

        return json_encode($return);
//		return $sql;
    }

    function getPelaksanaanParentingSatuanPendidikan(Request $request, Application $app) {
        $con = Preface::initdb('rekap');
        $request = Preface::Inisiasi($request);

        switch ($request['id_level_wilayah']) {
            case "0":
                $param_nama = 'provinsi';
                $param_kode_nama = '';
                $param_wilayah = 'provinsi';
                $param_kode_wilayah = '';

                $add_induk_provinsi = "null as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "1":
                $param_nama = 'kabupaten';
                $param_kode_nama = '';
                $param_wilayah = 'kabupaten';
                $param_kode_wilayah = "AND kode_wilayah_provinsi = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "null as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
                $add_group_induk_kabupaten = "";
                $add_group_kode_wilayah_induk_kabupaten = "";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "2":
                $param_nama = 'kecamatan';
                $param_kode_nama = '';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "null as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
                $add_group_induk_kecamatan = "";
                $add_group_kode_wilayah_induk_kecamatan = "";
                break;
            case "3":
                $param_nama = 'nama';
                $param_kode_nama = 'sekolah_id,';
                $param_wilayah = 'kecamatan';
                $param_kode_wilayah = "AND kode_wilayah_kecamatan = '".$request['kode_wilayah']."'";

                $add_induk_provinsi = "provinsi as induk_propinsi,";
                $add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
                $add_induk_kabupaten = "kabupaten as induk_kabupaten,";
                $add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
                $add_induk_kecamatan = "kecamatan as induk_kecamatan,";
                $add_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan as kode_wilayah_induk_kecamatan,";

                $add_group_induk_provinsi = "provinsi,";
                $add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
                $add_group_induk_kabupaten = "kabupaten,";
                $add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten,";
                $add_group_induk_kecamatan = "kecamatan,";
                $add_group_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan";
                break;
            default:
                break;
        }

        $sql = "SELECT
				  {$param_nama} as nama,
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah} as kode_wilayah,
				  id_level_wilayah_{$param_wilayah} as id_level_wilayah,
				  mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
				  {$add_induk_provinsi}
				  {$add_kode_wilayah_induk_provinsi}
				  {$add_induk_kabupaten}
				  {$add_kode_wilayah_induk_kabupaten}
				  {$add_induk_kecamatan}
				  {$add_kode_wilayah_induk_kecamatan}

				  sum(case when pelaksanaan_parenting = 1 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_ada,
                  sum(case when pelaksanaan_parenting = 2 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_tidak_ada,

                  sum(case when pelaksanaan_parenting = 1 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_ada,
                  sum(case when pelaksanaan_parenting = 2 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_tidak_ada,

                  sum(case when pelaksanaan_parenting = 1 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_ada,
                  sum(case when pelaksanaan_parenting = 2 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_tidak_ada,

                  sum(case when pelaksanaan_parenting = 1 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_ada,
                  sum(case when pelaksanaan_parenting = 2 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_tidak_ada

				FROM
				  dbo.rekap_sekolah
				WHERE
					semester_id is null
				AND tahun_ajaran_id = '".$request['tahun_ajaran_id']."'
				AND bentuk_pendidikan_id IN (1, 2, 3, 4)
				{$param_kode_wilayah}
				GROUP BY
				  {$param_nama},
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah},
				  id_level_wilayah_{$param_wilayah},
				  mst_kode_wilayah_{$param_wilayah},
				  {$add_group_induk_provinsi}
				  {$add_group_kode_wilayah_induk_provinsi}
				  {$add_group_induk_kabupaten}
				  {$add_group_kode_wilayah_induk_kabupaten}
				  {$add_group_induk_kecamatan}
				  {$add_group_kode_wilayah_induk_kecamatan}
				ORDER BY
				  CASE WHEN {$param_nama} = 'Luar Negeri' THEN 2 ELSE 1 END, {$param_nama}";

        $return = Preface::getDataByQuery($sql, 'rekap');

        return json_encode($return);
//		return $sql;
    }

	function getJadwalParentingSatuanPendidikan(Request $request, Application $app) {
		$con = Preface::initdb('rekap');
		$request = Preface::Inisiasi($request);

		switch ($request['id_level_wilayah']) {
			case "0":
				$param_nama = 'provinsi';
				$param_kode_nama = '';
				$param_wilayah = 'provinsi';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
				$add_induk_kecamatan = "null as induk_kecamatan,";
				$add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

				$add_group_induk_provinsi = "provinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				$add_group_induk_kecamatan = "";
				$add_group_kode_wilayah_induk_kecamatan = "";
				break;
			case "1":
				$param_nama = 'kabupaten';
				$param_kode_nama = '';
				$param_wilayah = 'kabupaten';
				$param_kode_wilayah = "AND kode_wilayah_provinsi = '".$request['kode_wilayah']."'";

				$add_induk_provinsi = "provinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
				$add_induk_kecamatan = "null as induk_kecamatan,";
				$add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

				$add_group_induk_provinsi = "provinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				$add_group_induk_kecamatan = "";
				$add_group_kode_wilayah_induk_kecamatan = "";
				break;
			case "2":
				$param_nama = 'kecamatan';
				$param_kode_nama = '';
				$param_wilayah = 'kecamatan';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$request['kode_wilayah']."'";

				$add_induk_provinsi = "provinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
				$add_induk_kecamatan = "null as induk_kecamatan,";
				$add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

				$add_group_induk_provinsi = "provinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				$add_group_induk_kecamatan = "";
				$add_group_kode_wilayah_induk_kecamatan = "";
				break;
			case "3":
				$param_nama = 'nama';
				$param_kode_nama = 'sekolah_id,';
				$param_wilayah = 'kecamatan';
				$param_kode_wilayah = "AND kode_wilayah_kecamatan = '".$request['kode_wilayah']."'";

				$add_induk_provinsi = "provinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
				$add_induk_kecamatan = "kecamatan as induk_kecamatan,";
				$add_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan as kode_wilayah_induk_kecamatan,";

				$add_group_induk_provinsi = "provinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten,";
				$add_group_induk_kecamatan = "kecamatan,";
				$add_group_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan";
				break;
			default:
				break;
		}

		$sql = "SELECT
				  {$param_nama} as nama,
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah} as kode_wilayah,
				  id_level_wilayah_{$param_wilayah} as id_level_wilayah,
				  mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
				  {$add_induk_provinsi}
				  {$add_kode_wilayah_induk_provinsi}
				  {$add_induk_kabupaten}
				  {$add_kode_wilayah_induk_kabupaten}
				  {$add_induk_kecamatan}
				  {$add_kode_wilayah_induk_kecamatan}
				  sum(case when freq_parenting = 3 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_satu_bulan,
                  sum(case when freq_parenting = 7 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_dua_tiga_bulan,
                  sum(case when freq_parenting = 8 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_diatas_tiga_bulan,
                  sum(case when freq_parenting = 99 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_belum_ada,
                  sum(case when freq_parenting = 0 and bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_belum_isi,

                  sum(case when freq_parenting = 3 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_satu_bulan,
                  sum(case when freq_parenting = 7 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_dua_tiga_bulan,
                  sum(case when freq_parenting = 8 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_diatas_tiga_bulan,
                  sum(case when freq_parenting = 99 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_belum_ada,
                  sum(case when freq_parenting = 0 and bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_belum_isi,

                  sum(case when freq_parenting = 3 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_satu_bulan,
                  sum(case when freq_parenting = 7 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_dua_tiga_bulan,
                  sum(case when freq_parenting = 8 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_diatas_tiga_bulan,
                  sum(case when freq_parenting = 99 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_belum_ada,
                  sum(case when freq_parenting = 0 and bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_belum_isi,

                  sum(case when freq_parenting = 3 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_satu_bulan,
                  sum(case when freq_parenting = 7 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_dua_tiga_bulan,
                  sum(case when freq_parenting = 8 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_diatas_tiga_bulan,
                  sum(case when freq_parenting = 99 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_belum_ada,
                  sum(case when freq_parenting = 0 and bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_belum_isi

				FROM
				  dbo.rekap_sekolah
				WHERE
					semester_id is null
				AND tahun_ajaran_id = '".$request['tahun_ajaran_id']."'
				AND bentuk_pendidikan_id IN (1, 2, 3, 4)
				{$param_kode_wilayah}
				GROUP BY
				  {$param_nama},
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah},
				  id_level_wilayah_{$param_wilayah},
				  mst_kode_wilayah_{$param_wilayah},
				  {$add_group_induk_provinsi}
				  {$add_group_kode_wilayah_induk_provinsi}
				  {$add_group_induk_kabupaten}
				  {$add_group_kode_wilayah_induk_kabupaten}
				  {$add_group_induk_kecamatan}
				  {$add_group_kode_wilayah_induk_kecamatan}
				ORDER BY
				  CASE WHEN {$param_nama} = 'Luar Negeri' THEN 2 ELSE 1 END, {$param_nama}";

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
//		return $sql;
	}

	function getBentukParentingSatuanPendidikan(Request $request, Application $app) {
		$con = Preface::initdb('rekap');
		$request = Preface::Inisiasi($request);

		switch ($request['id_level_wilayah']) {
			case "0":
				$param_nama = 'provinsi';
				$param_kode_nama = '';
				$param_wilayah = 'provinsi';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
				$add_induk_kecamatan = "null as induk_kecamatan,";
				$add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

				$add_group_induk_provinsi = "provinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				$add_group_induk_kecamatan = "";
				$add_group_kode_wilayah_induk_kecamatan = "";
				break;
			case "1":
				$param_nama = 'kabupaten';
				$param_kode_nama = '';
				$param_wilayah = 'kabupaten';
				$param_kode_wilayah = "AND kode_wilayah_provinsi = '".$request['kode_wilayah']."'";

				$add_induk_provinsi = "provinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";
				$add_induk_kecamatan = "null as induk_kecamatan,";
				$add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

				$add_group_induk_provinsi = "provinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				$add_group_induk_kecamatan = "";
				$add_group_kode_wilayah_induk_kecamatan = "";
				break;
			case "2":
				$param_nama = 'kecamatan';
				$param_kode_nama = '';
				$param_wilayah = 'kecamatan';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$request['kode_wilayah']."'";

				$add_induk_provinsi = "provinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
				$add_induk_kecamatan = "null as induk_kecamatan,";
				$add_kode_wilayah_induk_kecamatan = "null as kode_wilayah_induk_kecamatan,";

				$add_group_induk_provinsi = "provinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				$add_group_induk_kecamatan = "";
				$add_group_kode_wilayah_induk_kecamatan = "";
				break;
			case "3":
				$param_nama = 'nama';
				$param_kode_nama = 'sekolah_id,';
				$param_wilayah = 'kecamatan';
				$param_kode_wilayah = "AND kode_wilayah_kecamatan = '".$request['kode_wilayah']."'";

				$add_induk_provinsi = "provinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";
				$add_induk_kecamatan = "kecamatan as induk_kecamatan,";
				$add_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan as kode_wilayah_induk_kecamatan,";

				$add_group_induk_provinsi = "provinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_provinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten,";
				$add_group_induk_kecamatan = "kecamatan,";
				$add_group_kode_wilayah_induk_kecamatan = "kode_wilayah_kecamatan";
				break;
			default:
				break;
		}

		$sql = "SELECT
				  {$param_nama} as nama,
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah} as kode_wilayah,
				  id_level_wilayah_{$param_wilayah} as id_level_wilayah,
				  mst_kode_wilayah_{$param_wilayah} as mst_kode_wilayah,
				  {$add_induk_provinsi}
				  {$add_kode_wilayah_induk_provinsi}
				  {$add_induk_kabupaten}
				  {$add_kode_wilayah_induk_kabupaten}
				  {$add_induk_kecamatan}
				  {$add_kode_wilayah_induk_kecamatan}

				  sum(case when parenting_kpo = 1 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_parenting_kpo,
                  sum(case when parenting_kelas = 1 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_parenting_kelas,
                  sum(case when parenting_kegiatan = 1 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_parenting_kegiatan,
                  sum(case when parenting_konsultasi = 1 AND bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_parenting_konsultasi,
                  sum(case when parenting_kunjungan = 1 and bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_parenting_kunjungan,
                  sum(case when parenting_lainnya = 1 and bentuk_pendidikan_id = 1 then 1 else 0 end ) as tk_parenting_lainnya,

                  sum(case when parenting_kpo = 1 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_parenting_kpo,
                  sum(case when parenting_kelas = 1 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_parenting_kelas,
                  sum(case when parenting_kegiatan = 1 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_parenting_kegiatan,
                  sum(case when parenting_konsultasi = 1 AND bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_parenting_konsultasi,
                  sum(case when parenting_kunjungan = 1 and bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_parenting_kunjungan,
                  sum(case when parenting_lainnya = 1 and bentuk_pendidikan_id = 2 then 1 else 0 end ) as kb_parenting_lainnya,

                  sum(case when parenting_kpo = 1 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_parenting_kpo,
                  sum(case when parenting_kelas = 1 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_parenting_kelas,
                  sum(case when parenting_kegiatan = 1 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_parenting_kegiatan,
                  sum(case when parenting_konsultasi = 1 AND bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_parenting_konsultasi,
                  sum(case when parenting_kunjungan = 1 and bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_parenting_kunjungan,
                  sum(case when parenting_lainnya = 1 and bentuk_pendidikan_id = 3 then 1 else 0 end ) as tpa_parenting_lainnya,

                  sum(case when parenting_kpo = 1 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_parenting_kpo,
                  sum(case when parenting_kelas = 1 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_parenting_kelas,
                  sum(case when parenting_kegiatan = 1 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_parenting_kegiatan,
                  sum(case when parenting_konsultasi = 1 AND bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_parenting_konsultasi,
                  sum(case when parenting_kunjungan = 1 and bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_parenting_kunjungan,
                  sum(case when parenting_lainnya = 1 and bentuk_pendidikan_id = 4 then 1 else 0 end ) as sps_parenting_lainnya

				FROM
				  dbo.rekap_sekolah
				WHERE
					semester_id is null
				AND tahun_ajaran_id = '".$request['tahun_ajaran_id']."'
				AND bentuk_pendidikan_id IN (1, 2, 3, 4)
				{$param_kode_wilayah}
				GROUP BY
				  {$param_nama},
				  {$param_kode_nama}
				  kode_wilayah_{$param_wilayah},
				  id_level_wilayah_{$param_wilayah},
				  mst_kode_wilayah_{$param_wilayah},
				  {$add_group_induk_provinsi}
				  {$add_group_kode_wilayah_induk_provinsi}
				  {$add_group_induk_kabupaten}
				  {$add_group_kode_wilayah_induk_kabupaten}
				  {$add_group_induk_kecamatan}
				  {$add_group_kode_wilayah_induk_kecamatan}
				ORDER BY
				  CASE WHEN {$param_nama} = 'Luar Negeri' THEN 2 ELSE 1 END, {$param_nama}";

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
//		return $sql;
	}

	static function SatuanPendidikanKurikulum(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				$params = 'propinsi';
				$mst_kode_wilayah_induk = '';
				$mst_kode_wilayah_induk_group = '';
				$param_kode_wilayah = '';

				$add_induk_provinsi = "null as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "null as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "1":
				$params = 'kabupaten';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_propinsi as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_propinsi,';
				$param_kode_wilayah = "AND kode_wilayah_propinsi = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "null as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "null as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi";
				$add_group_induk_kabupaten = "";
				$add_group_kode_wilayah_induk_kabupaten = "";
				break;
			case "2":
				$params = 'kecamatan';
				$mst_kode_wilayah_induk = 'mst_kode_wilayah_kabupaten as mst_kode_wilayah_induk,';
				$mst_kode_wilayah_induk_group = 'mst_kode_wilayah_kabupaten,';
				$param_kode_wilayah = "AND kode_wilayah_kabupaten = '".$requesta['kode_wilayah']."'";

				$add_induk_provinsi = "propinsi as induk_propinsi,";
				$add_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi as kode_wilayah_induk_propinsi,";
				$add_induk_kabupaten = "kabupaten as induk_kabupaten,";
				$add_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten as kode_wilayah_induk_kabupaten,";

				$add_group_induk_provinsi = "propinsi,";
				$add_group_kode_wilayah_induk_provinsi = "kode_wilayah_propinsi,";
				$add_group_induk_kabupaten = "kabupaten,";
				$add_group_kode_wilayah_induk_kabupaten = "kode_wilayah_kabupaten";
				break;
			default:
				break;
		}

		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		$sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY {$params}) as 'no',
					{$params} as nama,
					kode_wilayah_{$params} as kode_wilayah,
					mst_kode_wilayah_{$params} as mst_kode_wilayah,
					{$mst_kode_wilayah_induk}
					id_level_wilayah_{$params} as id_level_wilayah,
					{$add_induk_provinsi}
					{$add_kode_wilayah_induk_provinsi}
					{$add_induk_kabupaten}
					{$add_kode_wilayah_induk_kabupaten}
					sum( case when status_sekolah = 1 and kurikulum = 'K13' then 1 else 0 end ) as k13_negeri,
					sum( case when status_sekolah = 2 and kurikulum = 'K13' then 1 else 0 end ) as k13_swasta,
					sum( case when status_sekolah in (1,2) and kurikulum = 'K13' then 1 else 0 end ) as k13_total,
					sum( case when status_sekolah = 1 and kurikulum = 'KTSP' then 1 else 0 end ) as ktsp_negeri,
					sum( case when status_sekolah = 2 and kurikulum = 'KTSP' then 1 else 0 end ) as ktsp_swasta,
					sum( case when status_sekolah in (1,2) and kurikulum = 'KTSP' then 1 else 0 end ) as ktsp_total,
					min(tanggal) as tanggal_rekap_terakhir
				FROM
					rekap_sekolah
				WHERE
					semester_id = '".$requesta['semester_id']."'
				AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
				{$param_kode_wilayah}
				{$params_bp}
				AND soft_delete = 0
				GROUP BY
					{$params},
					kode_wilayah_{$params},
					mst_kode_wilayah_{$params},
					id_level_wilayah_{$params},
					{$add_group_induk_provinsi}
					{$mst_kode_wilayah_induk_group}
					{$add_group_kode_wilayah_induk_provinsi}
					{$add_group_induk_kabupaten}
					{$add_group_kode_wilayah_induk_kabupaten}
				ORDER BY
					{$params}";

		// return $sql;die;

		$return = Preface::getDataByQuery($sql, 'rekap');

		return json_encode($return);
	}

	static function SatuanPendidikanKurikulumSp(Request $request, Application $app){

		$requesta = Preface::Inisiasi($request);
		$params_bp = Preface::parameterBentukPendidikan($requesta['bentuk_pendidikan_id'], null);

		switch ($requesta['id_level_wilayah']) {
			case "0":
				//nothing to do
				$params_wilayah = null;
				break;
			case "1":
				$params_wilayah = 'kode_wilayah_propinsi';
				break;
			case "2":
				$params_wilayah = 'kode_wilayah_kabupaten';
				break;
			case "3":
				$params_wilayah = 'kode_wilayah_kecamatan';
				break;
			default:
				break;
		}

		$sql = "SELECT
                    ROW_NUMBER() OVER (ORDER BY nama) as 'no',
					sekolah_id,
					nama,
					npsn,
                    status_sekolah,
                    CASE status_sekolah
                        WHEN 1 THEN 'Negeri'
                        WHEN 2 THEN 'Swasta'
                    ELSE '-' END AS status,
                    bentuk_pendidikan_id,
                    CASE 
                        WHEN bentuk_pendidikan_id = 1 THEN 'TK'
                        WHEN bentuk_pendidikan_id = 2 THEN 'KB'
                        WHEN bentuk_pendidikan_id = 3 THEN 'TPA'
                        WHEN bentuk_pendidikan_id = 4 THEN 'SPS'
                        WHEN bentuk_pendidikan_id = 5 THEN 'SD'
                        WHEN bentuk_pendidikan_id = 6 THEN 'SMP'
                        WHEN bentuk_pendidikan_id IN (7, 8, 14, 29) THEN 'SLB'
                        WHEN bentuk_pendidikan_id = 13 THEN 'SMA'
                        WHEN bentuk_pendidikan_id = 15 THEN 'SMK'
                    ELSE '-' END AS bentuk,
					kecamatan,
				    kode_wilayah_kecamatan,
				    mst_kode_wilayah_kecamatan,
				    id_level_wilayah_kecamatan,
					kurikulum,
					tanggal as tanggal_rekap_terakhir
				FROM
					rekap_sekolah WITH(NOLOCK)
				WHERE
					semester_id = '".$requesta['semester_id']."'
					AND tahun_ajaran_id = '".$requesta['tahun_ajaran_id']."'
					AND {$params_wilayah} = '".$requesta['kode_wilayah']."'
					AND soft_delete = 0
					{$params_bp}";

		$return = Preface::getDataByQuery($sql, 'rekap');

		$sql .= " ORDER BY nama OFFSET {$requesta['start']} ROWS FETCH NEXT {$requesta['limit']} ROWS ONLY";

		// return $sql;die;

		$final = array();
		$final['results'] = sizeof($return);

		$return = Preface::getDataByQuery($sql, 'rekap');

		$final['rows'] = $return;

		return json_encode($final);
	}

}