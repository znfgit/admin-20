<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for SaranaLongitudinal Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class SaranaLongitudinalTableInfo extends base\BaseSaranaLongitudinalTableInfo
{
    const CLASS_NAME = 'Admin.Info.SaranaLongitudinalTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}