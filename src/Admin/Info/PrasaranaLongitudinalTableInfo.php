<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for PrasaranaLongitudinal Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class PrasaranaLongitudinalTableInfo extends base\BasePrasaranaLongitudinalTableInfo
{
    const CLASS_NAME = 'Admin.Info.PrasaranaLongitudinalTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}