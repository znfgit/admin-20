<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for PtkTerdaftar Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class PtkTerdaftarTableInfo extends base\BasePtkTerdaftarTableInfo
{
    const CLASS_NAME = 'Admin.Info.PtkTerdaftarTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}