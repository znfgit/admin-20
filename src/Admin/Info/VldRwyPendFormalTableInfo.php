<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for VldRwyPendFormal Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class VldRwyPendFormalTableInfo extends base\BaseVldRwyPendFormalTableInfo
{
    const CLASS_NAME = 'Admin.Info.VldRwyPendFormalTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}