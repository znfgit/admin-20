<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for KebutuhanKhusus Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class KebutuhanKhususTableInfo extends base\BaseKebutuhanKhususTableInfo
{
    const CLASS_NAME = 'Admin.Info.KebutuhanKhususTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}