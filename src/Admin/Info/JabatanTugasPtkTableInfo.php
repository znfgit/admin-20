<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for JabatanTugasPtk Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class JabatanTugasPtkTableInfo extends base\BaseJabatanTugasPtkTableInfo
{
    const CLASS_NAME = 'Admin.Info.JabatanTugasPtkTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}