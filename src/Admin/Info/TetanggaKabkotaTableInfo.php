<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for TetanggaKabkota Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class TetanggaKabkotaTableInfo extends base\BaseTetanggaKabkotaTableInfo
{
    const CLASS_NAME = 'Admin.Info.TetanggaKabkotaTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}