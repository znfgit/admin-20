<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for BeasiswaPtk Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class BeasiswaPtkTableInfo extends base\BaseBeasiswaPtkTableInfo
{
    const CLASS_NAME = 'Admin.Info.BeasiswaPtkTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}