<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for TingkatPenghargaan Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class TingkatPenghargaanTableInfo extends base\BaseTingkatPenghargaanTableInfo
{
    const CLASS_NAME = 'Admin.Info.TingkatPenghargaanTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}