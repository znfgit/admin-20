<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for KeahlianLaboratorium Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class KeahlianLaboratoriumTableInfo extends base\BaseKeahlianLaboratoriumTableInfo
{
    const CLASS_NAME = 'Admin.Info.KeahlianLaboratoriumTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}