<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for TingkatPrestasi Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class TingkatPrestasiTableInfo extends base\BaseTingkatPrestasiTableInfo
{
    const CLASS_NAME = 'Admin.Info.TingkatPrestasiTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}