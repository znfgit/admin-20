<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for PenghasilanOrangtuaWali Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class PenghasilanOrangtuaWaliTableInfo extends base\BasePenghasilanOrangtuaWaliTableInfo
{
    const CLASS_NAME = 'Admin.Info.PenghasilanOrangtuaWaliTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}