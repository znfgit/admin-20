<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for PesertaDidikBaru Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class PesertaDidikBaruTableInfo extends base\BasePesertaDidikBaruTableInfo
{
    const CLASS_NAME = 'Admin.Info.PesertaDidikBaruTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}