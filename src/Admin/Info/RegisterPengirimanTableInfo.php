<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for RegisterPengiriman Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class RegisterPengirimanTableInfo extends base\BaseRegisterPengirimanTableInfo
{
    const CLASS_NAME = 'Admin.Info.RegisterPengirimanTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}