<?php
namespace Admin\Info\base;
use Xond\Info\TableInfo;

/**
 * The Base TableInfo for MapBidangMataPelajaran Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class BaseMapBidangMataPelajaranTableInfo extends TableInfo
{
    const CLASS_NAME = 'Admin.Info.MapBidangMataPelajaranTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function initialize()
    {
        $this->setName('map_bidang_mata_pelajaran');
        $this->setPhpName('MapBidangMataPelajaran');
        $this->setClassname('Admin\\Model\\MapBidangMataPelajaran');
        $this->setPackage('Admin.Model');
    }
    
    public function setVariables() {
        
        $this->setPkName(            'map_bidang_mata_pelajaran_id');
        $this->setIsData(           1);   
        $this->setCreateGrid(       1);
        $this->setCreateForm(       1);
        $this->setRecordCount(      0);
        $this->setIsRef(            0);
        $this->setIsStaticRef(      1);
        $this->setIsBigRef(         0);
        $this->setIsSmallRef(       1);
        $this->setIsCompositePk(    1);
        $this->setDisplayField(     'mata_pelajaran_id');
        $this->setRendererString(   'map_bidang_mata_pelajaran_id_str');
        $this->setHeader(           'Map Bidang Mata Pelajaran');
        $this->setLabel(            'Map bidang mata pelajaran');
        $this->setCreateCombobox(   1);
        $this->setCreateRadiogroup( 1);
        $this->setCreateList(       1);
        $this->setCreateModel(      1);
        $this->setXtypeCombo(       'mapbidangmatapelajarancombo');
        $this->setXtypeRadio(       'mapbidangmatapelajaranradio');
        $this->setXtypeList(        'mapbidangmatapelajaranlist');
        $this->setInfoBeforeDelete( '');
        $this->setHasMany(array(    ""));
        $this->setIsSplitEntity(    0);
        $this->setHasSplitEntity(   0);
        $this->setSplitEntityName(  '');
        $this->setRelatingColumns(array(    ""));
        $this->setFormDefaultLabelWidth(140);

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'map_bidang_mata_pelajaran_id');
        $cvar->setColumnPhpName(    'MapBidangMataPelajaranId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '1');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           ' ');
        $cvar->setLabel(            ' ');
        $cvar->setInputLength(      100);
        $cvar->setFieldWidth(       68);
        $cvar->setColumnWidth(      204);
        $cvar->setHideColumn(       1);
        $cvar->setXtype(            'hidden');
        $cvar->setComboXtype(       'mapbidangmatapelajaranid');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       0);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'mata_pelajaran_id');
        $cvar->setColumnPhpName(    'MataPelajaranId');
        $cvar->setType(             'int');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '1');
        $cvar->setIsFk(             '1');
        $cvar->setFkTableName(      'mata_pelajaran'); 
        $cvar->setMin(              0);
        $cvar->setMax(              9999);
        $cvar->setHeader(           'Id');
        $cvar->setLabel(            'Id');
        $cvar->setInputLength(      4);
        $cvar->setFieldWidth(       48);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       1);
        $cvar->setXtype(            'radiogroup');
        $cvar->setComboXtype(       'matapelajarancombo');
        $cvar->setRadiogroupXtype(  'matapelajaranradiogroup');
        $cvar->setDisplayField(     'nama');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'bidang_studi_id');
        $cvar->setColumnPhpName(    'BidangStudiId');
        $cvar->setType(             'int');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '1');
        $cvar->setIsFk(             '1');
        $cvar->setFkTableName(      'bidang_studi'); 
        $cvar->setMin(              0);
        $cvar->setMax(              9999);
        $cvar->setHeader(           'Id');
        $cvar->setLabel(            'Id');
        $cvar->setInputLength(      4);
        $cvar->setFieldWidth(       48);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       1);
        $cvar->setXtype(            'combobox');
        $cvar->setComboXtype(       'bidangstudicombo');
        $cvar->setRadiogroupXtype(  'bidangstudiradiogroup');
        $cvar->setDisplayField(     'bidang_studi');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'tahun_sertifikasi');
        $cvar->setColumnPhpName(    'TahunSertifikasi');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '1');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              999999);
        $cvar->setHeader(           'Id');
        $cvar->setLabel(            'Id');
        $cvar->setInputLength(      6);
        $cvar->setFieldWidth(       72);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       1);
        $cvar->setXtype(            'hidden');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'sesuai');
        $cvar->setColumnPhpName(    'Sesuai');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              999);
        $cvar->setHeader(           'Sesuai');
        $cvar->setLabel(            'Sesuai');
        $cvar->setInputLength(      3);
        $cvar->setFieldWidth(       36);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'numberfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'create_date');
        $cvar->setColumnPhpName(    'CreateDate');
        $cvar->setType(             'timestamp');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Create Date');
        $cvar->setLabel(            'Create date');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'datefield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'last_update');
        $cvar->setColumnPhpName(    'LastUpdate');
        $cvar->setType(             'timestamp');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Last Update');
        $cvar->setLabel(            'Last update');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'datefield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'expired_date');
        $cvar->setColumnPhpName(    'ExpiredDate');
        $cvar->setType(             'timestamp');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Expired Date');
        $cvar->setLabel(            'Expired date');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'datefield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       0);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'last_sync');
        $cvar->setColumnPhpName(    'LastSync');
        $cvar->setType(             'timestamp');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Last Sync');
        $cvar->setLabel(            'Last sync');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'datefield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        
        $this->setColumns($columns);        
        /*
        $this->setTitle( "Fieldset Name" );
        $this->setGroupId( 1 );
        $this->setParentGroupId( 0 );
        $this->setGroupingMethod( 'fieldset' );
        */
        
    }
    
}