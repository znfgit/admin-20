<?php
namespace Admin\Info\base;
use Xond\Info\TableInfo;

/**
 * The Base TableInfo for NilaiTest Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class BaseNilaiTestTableInfo extends TableInfo
{
    const CLASS_NAME = 'Admin.Info.NilaiTestTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function initialize()
    {
        $this->setName('nilai_test');
        $this->setPhpName('NilaiTest');
        $this->setClassname('Admin\\Model\\NilaiTest');
        $this->setPackage('Admin.Model');
    }
    
    public function setVariables() {
        
        $this->setPkName(            'nilai_test_id');
        $this->setIsData(           1);   
        $this->setCreateGrid(       1);
        $this->setCreateForm(       1);
        $this->setRecordCount(      2385);
        $this->setIsRef(            0);
        $this->setIsStaticRef(      0);
        $this->setIsBigRef(         1);
        $this->setIsSmallRef(       0);
        $this->setIsCompositePk(    0);
        $this->setDisplayField(     'nama');
        $this->setRendererString(   'nilai_test_id_str');
        $this->setHeader(           'Nilai Test');
        $this->setLabel(            'Nilai test');
        $this->setCreateCombobox(   1);
        $this->setCreateRadiogroup( 0);
        $this->setCreateList(       1);
        $this->setCreateModel(      1);
        $this->setXtypeCombo(       'nilaitestcombo');
        $this->setXtypeRadio(       'nilaitestradio');
        $this->setXtypeList(        'nilaitestlist');
        $this->setInfoBeforeDelete( '');
        $this->setHasMany(array(    "vld_nilai_test"));
        $this->setBelongsTo(array(  "Ptk"));
        $this->setIsSplitEntity(    0);
        $this->setHasSplitEntity(   0);
        $this->setSplitEntityName(  '');
        $this->setRelatingColumns(array(    "vld_nilai_test"));
        $this->setFormDefaultLabelWidth(140);

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'nilai_test_id');
        $cvar->setColumnPhpName(    'NilaiTestId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '1');
		$cvar->setIsPk(             '1');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Id');
        $cvar->setLabel(            'Id');
        $cvar->setInputLength(      36);
        $cvar->setFieldWidth(       432);
        $cvar->setColumnWidth(      144);
        $cvar->setHideColumn(       1);
        $cvar->setXtype(            'hidden');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'jenis_test_id');
        $cvar->setColumnPhpName(    'JenisTestId');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '1');
        $cvar->setFkTableName(      'jenis_test'); 
        $cvar->setMin(              0);
        $cvar->setMax(              99999);
        $cvar->setHeader(           'Jenis Test');
        $cvar->setLabel(            'Jenis test');
        $cvar->setInputLength(      5);
        $cvar->setFieldWidth(       60);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'combobox');
        $cvar->setComboXtype(       'jenistestcombo');
        $cvar->setRadiogroupXtype(  'jenistestradiogroup');
        $cvar->setDisplayField(     'jenis_test');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'ptk_id');
        $cvar->setColumnPhpName(    'PtkId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '1');
        $cvar->setFkTableName(      'ptk'); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Ptk');
        $cvar->setLabel(            'Ptk');
        $cvar->setInputLength(      36);
        $cvar->setFieldWidth(       432);
        $cvar->setColumnWidth(      144);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'combobox');
        $cvar->setComboXtype(       'ptkcombo');
        $cvar->setRadiogroupXtype(  'ptkradiogroup');
        $cvar->setDisplayField(     'nama');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'nama');
        $cvar->setColumnPhpName(    'Nama');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Nama');
        $cvar->setLabel(            'Nama');
        $cvar->setInputLength(      50);
        $cvar->setFieldWidth(       600);
        $cvar->setColumnWidth(      200);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'textfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'penyelenggara');
        $cvar->setColumnPhpName(    'Penyelenggara');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Penyelenggara');
        $cvar->setLabel(            'Penyelenggara');
        $cvar->setInputLength(      80);
        $cvar->setFieldWidth(       960);
        $cvar->setColumnWidth(      320);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'textfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'tahun');
        $cvar->setColumnPhpName(    'Tahun');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              999999);
        $cvar->setHeader(           'Tahun');
        $cvar->setLabel(            'Tahun');
        $cvar->setInputLength(      6);
        $cvar->setFieldWidth(       72);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'numberfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'skor');
        $cvar->setColumnPhpName(    'Skor');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              99999999);
        $cvar->setHeader(           'Skor');
        $cvar->setLabel(            'Skor');
        $cvar->setInputLength(      8);
        $cvar->setFieldWidth(       96);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'numberfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'Last_update');
        $cvar->setColumnPhpName(    'LastUpdate');
        $cvar->setType(             'timestamp');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Last Update');
        $cvar->setLabel(            'Last update');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'datefield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'Soft_delete');
        $cvar->setColumnPhpName(    'SoftDelete');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              999);
        $cvar->setHeader(           'Soft Delete');
        $cvar->setLabel(            'Soft delete');
        $cvar->setInputLength(      3);
        $cvar->setFieldWidth(       36);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'numberfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'last_sync');
        $cvar->setColumnPhpName(    'LastSync');
        $cvar->setType(             'timestamp');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Last Sync');
        $cvar->setLabel(            'Last sync');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'datefield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'Updater_ID');
        $cvar->setColumnPhpName(    'UpdaterId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Updater Id');
        $cvar->setLabel(            'Updater id');
        $cvar->setInputLength(      36);
        $cvar->setFieldWidth(       432);
        $cvar->setColumnWidth(      144);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'textfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        
        $this->setColumns($columns);        
        /*
        $this->setTitle( "Fieldset Name" );
        $this->setGroupId( 1 );
        $this->setParentGroupId( 0 );
        $this->setGroupingMethod( 'fieldset' );
        */
        
    }
    
}