<?php
namespace Admin\Info\base;
use Xond\Info\TableInfo;

/**
 * The Base TableInfo for JenisSoal Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class BaseJenisSoalTableInfo extends TableInfo
{
    const CLASS_NAME = 'Admin.Info.JenisSoalTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function initialize()
    {
        $this->setName('jenis_soal');
        $this->setPhpName('JenisSoal');
        $this->setClassname('Admin\\Model\\JenisSoal');
        $this->setPackage('Admin.Model');
    }
    
    public function setVariables() {
        
        $this->setPkName(            'jenis_soal_id');
        $this->setIsData(           0);   
        $this->setCreateGrid(       0);
        $this->setCreateForm(       0);
        $this->setRecordCount(      2);
        $this->setIsRef(            1);
        $this->setIsStaticRef(      1);
        $this->setIsBigRef(         0);
        $this->setIsSmallRef(       1);
        $this->setIsCompositePk(    0);
        $this->setDisplayField(     'nama');
        $this->setRendererString(   'jenis_soal_id_str');
        $this->setHeader(           'Jenis Soal');
        $this->setLabel(            'Jenis soal');
        $this->setCreateCombobox(   1);
        $this->setCreateRadiogroup( 1);
        $this->setCreateList(       1);
        $this->setCreateModel(      1);
        $this->setXtypeCombo(       'jenissoalcombo');
        $this->setXtypeRadio(       'jenissoalradio');
        $this->setXtypeList(        'jenissoallist');
        $this->setInfoBeforeDelete( '');
        $this->setHasMany(array(    "soal"));
        $this->setIsSplitEntity(    0);
        $this->setHasSplitEntity(   0);
        $this->setSplitEntityName(  '');
        $this->setRelatingColumns(array(    "soal"));
        $this->setFormDefaultLabelWidth(140);

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'jenis_soal_id');
        $cvar->setColumnPhpName(    'JenisSoalId');
        $cvar->setType(             'int');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '1');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              9999);
        $cvar->setHeader(           'Id');
        $cvar->setLabel(            'Id');
        $cvar->setInputLength(      4);
        $cvar->setFieldWidth(       48);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       1);
        $cvar->setXtype(            'hidden');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'nama');
        $cvar->setColumnPhpName(    'Nama');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Nama');
        $cvar->setLabel(            'Nama');
        $cvar->setInputLength(      50);
        $cvar->setFieldWidth(       600);
        $cvar->setColumnWidth(      200);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'textfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       0);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'expired_date');
        $cvar->setColumnPhpName(    'ExpiredDate');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Expired Date');
        $cvar->setLabel(            'Expired date');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'textfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       0);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'last_update');
        $cvar->setColumnPhpName(    'LastUpdate');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Last Update');
        $cvar->setLabel(            'Last update');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'textfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        
        $this->setColumns($columns);        
        /*
        $this->setTitle( "Fieldset Name" );
        $this->setGroupId( 1 );
        $this->setParentGroupId( 0 );
        $this->setGroupingMethod( 'fieldset' );
        */
        
    }
    
}