<?php
namespace Admin\Info\base;
use Xond\Info\TableInfo;

/**
 * The Base TableInfo for SaranaLongitudinal Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class BaseSaranaLongitudinalTableInfo extends TableInfo
{
    const CLASS_NAME = 'Admin.Info.SaranaLongitudinalTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function initialize()
    {
        $this->setName('sarana_longitudinal');
        $this->setPhpName('SaranaLongitudinal');
        $this->setClassname('Admin\\Model\\SaranaLongitudinal');
        $this->setPackage('Admin.Model');
    }
    
    public function setVariables() {
        
        $this->setPkName(            'sarana_longitudinal_id');
        $this->setIsData(           1);   
        $this->setCreateGrid(       1);
        $this->setCreateForm(       1);
        $this->setRecordCount(      1364231);
        $this->setIsRef(            0);
        $this->setIsStaticRef(      0);
        $this->setIsBigRef(         1);
        $this->setIsSmallRef(       0);
        $this->setIsCompositePk(    1);
        $this->setDisplayField(     'sarana_id');
        $this->setRendererString(   'sarana_longitudinal_id_str');
        $this->setHeader(           'Sarana Longitudinal');
        $this->setLabel(            'Sarana longitudinal');
        $this->setCreateCombobox(   1);
        $this->setCreateRadiogroup( 0);
        $this->setCreateList(       1);
        $this->setCreateModel(      1);
        $this->setXtypeCombo(       'saranalongitudinalcombo');
        $this->setXtypeRadio(       'saranalongitudinalradio');
        $this->setXtypeList(        'saranalongitudinallist');
        $this->setInfoBeforeDelete( '');
        $this->setHasMany(array(    ""));
        $this->setBelongsTo(array(  "Sarana"));
        $this->setIsSplitEntity(    0);
        $this->setHasSplitEntity(   0);
        $this->setSplitEntityName(  '');
        $this->setRelatingColumns(array(    ""));
        $this->setFormDefaultLabelWidth(140);

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'sarana_longitudinal_id');
        $cvar->setColumnPhpName(    'SaranaLongitudinalId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '1');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           ' ');
        $cvar->setLabel(            ' ');
        $cvar->setInputLength(      100);
        $cvar->setFieldWidth(       172);
        $cvar->setColumnWidth(      516);
        $cvar->setHideColumn(       1);
        $cvar->setXtype(            'hidden');
        $cvar->setComboXtype(       'saranalongitudinalid');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       0);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'sarana_id');
        $cvar->setColumnPhpName(    'SaranaId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '1');
		$cvar->setIsPk(             '1');
        $cvar->setIsFk(             '1');
        $cvar->setFkTableName(      'sarana'); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Id');
        $cvar->setLabel(            'Id');
        $cvar->setInputLength(      36);
        $cvar->setFieldWidth(       432);
        $cvar->setColumnWidth(      144);
        $cvar->setHideColumn(       1);
        $cvar->setXtype(            'combobox');
        $cvar->setComboXtype(       'saranacombo');
        $cvar->setRadiogroupXtype(  'saranaradiogroup');
        $cvar->setDisplayField(     'nama');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'semester_id');
        $cvar->setColumnPhpName(    'SemesterId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '1');
        $cvar->setIsFk(             '1');
        $cvar->setFkTableName(      'semester'); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Id');
        $cvar->setLabel(            'Id');
        $cvar->setInputLength(      5);
        $cvar->setFieldWidth(       60);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       1);
        $cvar->setXtype(            'combobox');
        $cvar->setComboXtype(       'semestercombo');
        $cvar->setRadiogroupXtype(  'semesterradiogroup');
        $cvar->setDisplayField(     'nama');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'jumlah');
        $cvar->setColumnPhpName(    'Jumlah');
        $cvar->setType(             'int');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              9999);
        $cvar->setHeader(           'Jumlah');
        $cvar->setLabel(            'Jumlah');
        $cvar->setInputLength(      4);
        $cvar->setFieldWidth(       48);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'numberfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'status_kelaikan');
        $cvar->setColumnPhpName(    'StatusKelaikan');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              999);
        $cvar->setHeader(           'Status Kelaikan');
        $cvar->setLabel(            'Status kelaikan');
        $cvar->setInputLength(      3);
        $cvar->setFieldWidth(       36);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'numberfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'Last_update');
        $cvar->setColumnPhpName(    'LastUpdate');
        $cvar->setType(             'timestamp');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Last Update');
        $cvar->setLabel(            'Last update');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'datefield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'Soft_delete');
        $cvar->setColumnPhpName(    'SoftDelete');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              999);
        $cvar->setHeader(           'Soft Delete');
        $cvar->setLabel(            'Soft delete');
        $cvar->setInputLength(      3);
        $cvar->setFieldWidth(       36);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'numberfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'last_sync');
        $cvar->setColumnPhpName(    'LastSync');
        $cvar->setType(             'timestamp');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Last Sync');
        $cvar->setLabel(            'Last sync');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'datefield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'Updater_ID');
        $cvar->setColumnPhpName(    'UpdaterId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Updater Id');
        $cvar->setLabel(            'Updater id');
        $cvar->setInputLength(      36);
        $cvar->setFieldWidth(       432);
        $cvar->setColumnWidth(      144);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'textfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        
        $this->setColumns($columns);        
        /*
        $this->setTitle( "Fieldset Name" );
        $this->setGroupId( 1 );
        $this->setParentGroupId( 0 );
        $this->setGroupingMethod( 'fieldset' );
        */
        
    }
    
}