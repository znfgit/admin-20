<?php
namespace Admin\Info\base;
use Xond\Info\TableInfo;

/**
 * The Base TableInfo for AnggotaRombel Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class BaseAnggotaRombelTableInfo extends TableInfo
{
    const CLASS_NAME = 'Admin.Info.AnggotaRombelTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function initialize()
    {
        $this->setName('anggota_rombel');
        $this->setPhpName('AnggotaRombel');
        $this->setClassname('Admin\\Model\\AnggotaRombel');
        $this->setPackage('Admin.Model');
    }
    
    public function setVariables() {
        
        $this->setPkName(            'anggota_rombel_id');
        $this->setIsData(           1);   
        $this->setCreateGrid(       1);
        $this->setCreateForm(       1);
        $this->setRecordCount(      2352700);
        $this->setIsRef(            0);
        $this->setIsStaticRef(      0);
        $this->setIsBigRef(         1);
        $this->setIsSmallRef(       0);
        $this->setIsCompositePk(    0);
        $this->setDisplayField(     'anggota_rombel_id');
        $this->setRendererString(   'anggota_rombel_id_str');
        $this->setHeader(           'Anggota Rombel');
        $this->setLabel(            'Anggota rombel');
        $this->setCreateCombobox(   1);
        $this->setCreateRadiogroup( 0);
        $this->setCreateList(       1);
        $this->setCreateModel(      1);
        $this->setXtypeCombo(       'anggotarombelcombo');
        $this->setXtypeRadio(       'anggotarombelradio');
        $this->setXtypeList(        'anggotarombellist');
        $this->setInfoBeforeDelete( '');
        $this->setHasMany(array(    ""));
        $this->setBelongsTo(array(  "RombonganBelajar","PesertaDidik"));
        $this->setIsSplitEntity(    0);
        $this->setHasSplitEntity(   0);
        $this->setSplitEntityName(  '');
        $this->setRelatingColumns(array(    ""));
        $this->setFormDefaultLabelWidth(140);

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'anggota_rombel_id');
        $cvar->setColumnPhpName(    'AnggotaRombelId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '1');
		$cvar->setIsPk(             '1');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Id');
        $cvar->setLabel(            'Id');
        $cvar->setInputLength(      36);
        $cvar->setFieldWidth(       432);
        $cvar->setColumnWidth(      144);
        $cvar->setHideColumn(       1);
        $cvar->setXtype(            'hidden');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'rombongan_belajar_id');
        $cvar->setColumnPhpName(    'RombonganBelajarId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '1');
        $cvar->setFkTableName(      'rombongan_belajar'); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Rombongan Belajar');
        $cvar->setLabel(            'Rombongan belajar');
        $cvar->setInputLength(      36);
        $cvar->setFieldWidth(       432);
        $cvar->setColumnWidth(      144);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'combobox');
        $cvar->setComboXtype(       'rombonganbelajarcombo');
        $cvar->setRadiogroupXtype(  'rombonganbelajarradiogroup');
        $cvar->setDisplayField(     'nama');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'peserta_didik_id');
        $cvar->setColumnPhpName(    'PesertaDidikId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '1');
        $cvar->setFkTableName(      'peserta_didik'); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Peserta Didik');
        $cvar->setLabel(            'Peserta didik');
        $cvar->setInputLength(      36);
        $cvar->setFieldWidth(       432);
        $cvar->setColumnWidth(      144);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'combobox');
        $cvar->setComboXtype(       'pesertadidikcombo');
        $cvar->setRadiogroupXtype(  'pesertadidikradiogroup');
        $cvar->setDisplayField(     'nama');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'jenis_pendaftaran_id');
        $cvar->setColumnPhpName(    'JenisPendaftaranId');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '1');
        $cvar->setFkTableName(      'jenis_pendaftaran'); 
        $cvar->setMin(              0);
        $cvar->setMax(              999);
        $cvar->setHeader(           'Jenis Pendaftaran');
        $cvar->setLabel(            'Jenis pendaftaran');
        $cvar->setInputLength(      3);
        $cvar->setFieldWidth(       36);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'combobox');
        $cvar->setComboXtype(       'jenispendaftarancombo');
        $cvar->setRadiogroupXtype(  'jenispendaftaranradiogroup');
        $cvar->setDisplayField(     'nama');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'Last_update');
        $cvar->setColumnPhpName(    'LastUpdate');
        $cvar->setType(             'timestamp');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Last Update');
        $cvar->setLabel(            'Last update');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'datefield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'Soft_delete');
        $cvar->setColumnPhpName(    'SoftDelete');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              999);
        $cvar->setHeader(           'Soft Delete');
        $cvar->setLabel(            'Soft delete');
        $cvar->setInputLength(      3);
        $cvar->setFieldWidth(       36);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'numberfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'last_sync');
        $cvar->setColumnPhpName(    'LastSync');
        $cvar->setType(             'timestamp');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Last Sync');
        $cvar->setLabel(            'Last sync');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'datefield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'Updater_ID');
        $cvar->setColumnPhpName(    'UpdaterId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Updater Id');
        $cvar->setLabel(            'Updater id');
        $cvar->setInputLength(      36);
        $cvar->setFieldWidth(       432);
        $cvar->setColumnWidth(      144);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'textfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        
        $this->setColumns($columns);        
        /*
        $this->setTitle( "Fieldset Name" );
        $this->setGroupId( 1 );
        $this->setParentGroupId( 0 );
        $this->setGroupingMethod( 'fieldset' );
        */
        
    }
    
}