<?php
namespace Admin\Info\base;
use Xond\Info\TableInfo;

/**
 * The Base TableInfo for Prasarana Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class BasePrasaranaTableInfo extends TableInfo
{
    const CLASS_NAME = 'Admin.Info.PrasaranaTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function initialize()
    {
        $this->setName('prasarana');
        $this->setPhpName('Prasarana');
        $this->setClassname('Admin\\Model\\Prasarana');
        $this->setPackage('Admin.Model');
    }
    
    public function setVariables() {
        
        $this->setPkName(            'prasarana_id');
        $this->setIsData(           1);   
        $this->setCreateGrid(       1);
        $this->setCreateForm(       1);
        $this->setRecordCount(      393027);
        $this->setIsRef(            0);
        $this->setIsStaticRef(      0);
        $this->setIsBigRef(         1);
        $this->setIsSmallRef(       0);
        $this->setIsCompositePk(    0);
        $this->setDisplayField(     'nama');
        $this->setRendererString(   'prasarana_id_str');
        $this->setHeader(           'Prasarana');
        $this->setLabel(            'Prasarana');
        $this->setCreateCombobox(   1);
        $this->setCreateRadiogroup( 0);
        $this->setCreateList(       1);
        $this->setCreateModel(      1);
        $this->setXtypeCombo(       'prasaranacombo');
        $this->setXtypeRadio(       'prasaranaradio');
        $this->setXtypeList(        'prasaranalist');
        $this->setInfoBeforeDelete( '');
        $this->setHasMany(array(    "rombongan_belajar,vld_prasarana,sarana,prasarana_longitudinal,buku_alat"));
        $this->setBelongsTo(array(  "Sekolah"));
        $this->setIsSplitEntity(    0);
        $this->setHasSplitEntity(   0);
        $this->setSplitEntityName(  '');
        $this->setRelatingColumns(array(    "rombongan_belajar,vld_prasarana,sarana,prasarana_longitudinal,buku_alat"));
        $this->setFormDefaultLabelWidth(140);

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'prasarana_id');
        $cvar->setColumnPhpName(    'PrasaranaId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '1');
		$cvar->setIsPk(             '1');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Id');
        $cvar->setLabel(            'Id');
        $cvar->setInputLength(      36);
        $cvar->setFieldWidth(       432);
        $cvar->setColumnWidth(      144);
        $cvar->setHideColumn(       1);
        $cvar->setXtype(            'hidden');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'jenis_prasarana_id');
        $cvar->setColumnPhpName(    'JenisPrasaranaId');
        $cvar->setType(             'int');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '1');
        $cvar->setFkTableName(      'jenis_prasarana'); 
        $cvar->setMin(              0);
        $cvar->setMax(              9999);
        $cvar->setHeader(           'Jenis Prasarana');
        $cvar->setLabel(            'Jenis prasarana');
        $cvar->setInputLength(      4);
        $cvar->setFieldWidth(       48);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'combobox');
        $cvar->setComboXtype(       'jenisprasaranacombo');
        $cvar->setRadiogroupXtype(  'jenisprasaranaradiogroup');
        $cvar->setDisplayField(     'nama');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'sekolah_id');
        $cvar->setColumnPhpName(    'SekolahId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '1');
        $cvar->setFkTableName(      'sekolah'); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Sekolah');
        $cvar->setLabel(            'Sekolah');
        $cvar->setInputLength(      36);
        $cvar->setFieldWidth(       432);
        $cvar->setColumnWidth(      144);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'combobox');
        $cvar->setComboXtype(       'sekolahcombo');
        $cvar->setRadiogroupXtype(  'sekolahradiogroup');
        $cvar->setDisplayField(     'nama');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'kepemilikan_sarpras_id');
        $cvar->setColumnPhpName(    'KepemilikanSarprasId');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '1');
        $cvar->setFkTableName(      'status_kepemilikan_sarpras'); 
        $cvar->setMin(              0);
        $cvar->setMax(              999);
        $cvar->setHeader(           'Kepemilikan Sarpras');
        $cvar->setLabel(            'Kepemilikan sarpras');
        $cvar->setInputLength(      3);
        $cvar->setFieldWidth(       36);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'combobox');
        $cvar->setComboXtype(       'statuskepemilikansarprascombo');
        $cvar->setRadiogroupXtype(  'statuskepemilikansarprasradiogroup');
        $cvar->setDisplayField(     'nama');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'nama');
        $cvar->setColumnPhpName(    'Nama');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Nama');
        $cvar->setLabel(            'Nama');
        $cvar->setInputLength(      30);
        $cvar->setFieldWidth(       360);
        $cvar->setColumnWidth(      120);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'textfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'panjang');
        $cvar->setColumnPhpName(    'Panjang');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              99999999);
        $cvar->setHeader(           'Panjang');
        $cvar->setLabel(            'Panjang');
        $cvar->setInputLength(      8);
        $cvar->setFieldWidth(       96);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'numberfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       0);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'lebar');
        $cvar->setColumnPhpName(    'Lebar');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              99999999);
        $cvar->setHeader(           'Lebar');
        $cvar->setLabel(            'Lebar');
        $cvar->setInputLength(      8);
        $cvar->setFieldWidth(       96);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'numberfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       0);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'Last_update');
        $cvar->setColumnPhpName(    'LastUpdate');
        $cvar->setType(             'timestamp');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Last Update');
        $cvar->setLabel(            'Last update');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'datefield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'Soft_delete');
        $cvar->setColumnPhpName(    'SoftDelete');
        $cvar->setType(             'float');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              999);
        $cvar->setHeader(           'Soft Delete');
        $cvar->setLabel(            'Soft delete');
        $cvar->setInputLength(      3);
        $cvar->setFieldWidth(       36);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'numberfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'last_sync');
        $cvar->setColumnPhpName(    'LastSync');
        $cvar->setType(             'timestamp');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Last Sync');
        $cvar->setLabel(            'Last sync');
        $cvar->setInputLength(      16);
        $cvar->setFieldWidth(       192);
        $cvar->setColumnWidth(      80);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'datefield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        $cvar = new \Xond\Info\ColumnInfo();
        $cvar->setColumnName(       'Updater_ID');
        $cvar->setColumnPhpName(    'UpdaterId');
        $cvar->setType(             'string');
        $cvar->setIsPkUuid(         '0');
		$cvar->setIsPk(             '0');
        $cvar->setIsFk(             '0');
        $cvar->setFkTableName(      ''); 
        $cvar->setMin(              0);
        $cvar->setMax(              0);
        $cvar->setHeader(           'Updater Id');
        $cvar->setLabel(            'Updater id');
        $cvar->setInputLength(      36);
        $cvar->setFieldWidth(       432);
        $cvar->setColumnWidth(      144);
        $cvar->setHideColumn(       0);
        $cvar->setXtype(            'textfield');
        $cvar->setComboXtype(       '');
        $cvar->setRadiogroupXtype(  '');
        $cvar->setDisplayField(     '');
        $cvar->setValidation(       );
        $cvar->setAllowEmpty(       1);
        $cvar->setDescription(      '');
        $columns[] = $cvar;

        
        $this->setColumns($columns);        
        /*
        $this->setTitle( "Fieldset Name" );
        $this->setGroupId( 1 );
        $this->setParentGroupId( 0 );
        $this->setGroupingMethod( 'fieldset' );
        */
        
    }
    
}