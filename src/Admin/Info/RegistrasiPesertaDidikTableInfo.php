<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for RegistrasiPesertaDidik Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class RegistrasiPesertaDidikTableInfo extends base\BaseRegistrasiPesertaDidikTableInfo
{
    const CLASS_NAME = 'Admin.Info.RegistrasiPesertaDidikTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}