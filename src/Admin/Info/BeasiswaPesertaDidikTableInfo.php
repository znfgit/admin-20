<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for BeasiswaPesertaDidik Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class BeasiswaPesertaDidikTableInfo extends base\BaseBeasiswaPesertaDidikTableInfo
{
    const CLASS_NAME = 'Admin.Info.BeasiswaPesertaDidikTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}