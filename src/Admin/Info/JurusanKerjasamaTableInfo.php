<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for JurusanKerjasama Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class JurusanKerjasamaTableInfo extends base\BaseJurusanKerjasamaTableInfo
{
    const CLASS_NAME = 'Admin.Info.JurusanKerjasamaTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}