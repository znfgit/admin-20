<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for MataPelajaranKurikulum Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class MataPelajaranKurikulumTableInfo extends base\BaseMataPelajaranKurikulumTableInfo
{
    const CLASS_NAME = 'Admin.Info.MataPelajaranKurikulumTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}