<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for StatusKepegawaian Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class StatusKepegawaianTableInfo extends base\BaseStatusKepegawaianTableInfo
{
    const CLASS_NAME = 'Admin.Info.StatusKepegawaianTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}