<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for PengawasTerdaftar Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class PengawasTerdaftarTableInfo extends base\BasePengawasTerdaftarTableInfo
{
    const CLASS_NAME = 'Admin.Info.PengawasTerdaftarTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}