<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for UnitUsahaKerjasama Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class UnitUsahaKerjasamaTableInfo extends base\BaseUnitUsahaKerjasamaTableInfo
{
    const CLASS_NAME = 'Admin.Info.UnitUsahaKerjasamaTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}