<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for PesertaDidikLongitudinal Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class PesertaDidikLongitudinalTableInfo extends base\BasePesertaDidikLongitudinalTableInfo
{
    const CLASS_NAME = 'Admin.Info.PesertaDidikLongitudinalTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}