<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for RiwayatGajiBerkala Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class RiwayatGajiBerkalaTableInfo extends base\BaseRiwayatGajiBerkalaTableInfo
{
    const CLASS_NAME = 'Admin.Info.RiwayatGajiBerkalaTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}