<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for SasaranPengawasan Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class SasaranPengawasanTableInfo extends base\BaseSasaranPengawasanTableInfo
{
    const CLASS_NAME = 'Admin.Info.SasaranPengawasanTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}