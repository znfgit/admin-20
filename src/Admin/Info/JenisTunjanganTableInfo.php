<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for JenisTunjangan Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class JenisTunjanganTableInfo extends base\BaseJenisTunjanganTableInfo
{
    const CLASS_NAME = 'Admin.Info.JenisTunjanganTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}