<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for WaktuPenyelenggaraan Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class WaktuPenyelenggaraanTableInfo extends base\BaseWaktuPenyelenggaraanTableInfo
{
    const CLASS_NAME = 'Admin.Info.WaktuPenyelenggaraanTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}