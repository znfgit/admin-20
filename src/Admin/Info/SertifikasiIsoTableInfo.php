<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for SertifikasiIso Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class SertifikasiIsoTableInfo extends base\BaseSertifikasiIsoTableInfo
{
    const CLASS_NAME = 'Admin.Info.SertifikasiIsoTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}