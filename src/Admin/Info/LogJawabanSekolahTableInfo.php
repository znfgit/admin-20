<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for LogJawabanSekolah Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class LogJawabanSekolahTableInfo extends base\BaseLogJawabanSekolahTableInfo
{
    const CLASS_NAME = 'Admin.Info.LogJawabanSekolahTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}