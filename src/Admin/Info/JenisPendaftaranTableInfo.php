<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for JenisPendaftaran Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class JenisPendaftaranTableInfo extends base\BaseJenisPendaftaranTableInfo
{
    const CLASS_NAME = 'Admin.Info.JenisPendaftaranTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}