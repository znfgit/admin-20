<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for StatusKepemilikanSarpras Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class StatusKepemilikanSarprasTableInfo extends base\BaseStatusKepemilikanSarprasTableInfo
{
    const CLASS_NAME = 'Admin.Info.StatusKepemilikanSarprasTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}