<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for StatusKepemilikan Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class StatusKepemilikanTableInfo extends base\BaseStatusKepemilikanTableInfo
{
    const CLASS_NAME = 'Admin.Info.StatusKepemilikanTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}