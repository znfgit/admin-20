<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for BatasWaktuRapor Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class BatasWaktuRaporTableInfo extends base\BaseBatasWaktuRaporTableInfo
{
    const CLASS_NAME = 'Admin.Info.BatasWaktuRaporTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}