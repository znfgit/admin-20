<?php
namespace Admin\Info;
use Xond\Info\TableInfo;
use Admin\Info\base;

/**
 * The TableInfo for Jurusan Table
 * 
 * @author Donny Fauzan <donny.fauzan@gmail.com>
 * @version $version$
 */
class JurusanTableInfo extends base\BaseJurusanTableInfo
{
    const CLASS_NAME = 'Admin.Info.JurusanTableInfo';

    public function __construct(){        
        parent::__construct();        
    }
    
    public function setVariables() {
        parent::setVariables();
        
        // Override below here!
    }
    
}