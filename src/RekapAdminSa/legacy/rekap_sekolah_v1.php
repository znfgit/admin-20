<?php
include "startup.php";

$z = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $z->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, 2016);
$z->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($z);

foreach ($semesters as $semester) {
	# code...
	echo $br."[".date('H:i:s')."] Mengambil data tabel semester: ".$semester->getNama();

	$x = new Criteria();
	$recordCountX = Admin\Model\SekolahPeer::doCount($x);

	echo $br."[".date('H:i:s')."] Jumlah Record: ".$recordCountX;

	$total = $recordCountX; 
		
	$data_proses = 0;

	for ($i=0; $i < $recordCountX; $i++) { 

		if($i % 1000 == 0){

			echo $br."[".date('H:i:s')."] Mengambil data tabel sekolah dari record ".($i+1)." sampai ".($i+1000).": OK";

			$obj = array();

			$sql = "SELECT
						newid() as rekap_sekolah_id,
						s.sekolah_id,
						s.nama,
						s.npsn,
						s.bentuk_pendidikan_id,
						s.status_sekolah,
						s.soft_delete,
						'".$semester->getSemesterId()."' as semester_id,
						'".$semester->getTahunAjaranId()."' as tahun_ajaran_id,
						getdate() as tanggal,
						kecamatan.nama as kecamatan,
						kabupaten.nama as kabupaten,
						propinsi.nama as propinsi,
						kecamatan.kode_wilayah as kode_wilayah_kecamatan,
						kabupaten.kode_wilayah as kode_wilayah_kabupaten,
						propinsi.kode_wilayah as kode_wilayah_propinsi,
						kecamatan.id_level_wilayah as id_level_wilayah_kecamatan,
						kabupaten.id_level_wilayah as id_level_wilayah_kabupaten,
						propinsi.id_level_wilayah as id_level_wilayah_propinsi,
						kecamatan.mst_kode_wilayah as mst_kode_wilayah_kecamatan,
						kabupaten.mst_kode_wilayah as mst_kode_wilayah_kabupaten,
						propinsi.mst_kode_wilayah as mst_kode_wilayah_propinsi,
						(
							SELECT
								COUNT (1)
							FROM
								ptk ptk WITH (nolock)
							JOIN ptk_terdaftar ptkd WITH (nolock) ON ptk.ptk_id = ptkd.ptk_id
							JOIN ref.tahun_ajaran ta WITH (nolock) ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
							WHERE
								ptkd.sekolah_id = s.sekolah_id
							AND ptk.Soft_delete = 0
							AND ptkd.Soft_delete = 0
							AND ptkd.ptk_induk = 1
							AND ptkd.tahun_ajaran_id = LEFT(".$semester->getSemesterId().", 4)
							AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
							AND (
								ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								OR ptkd.jenis_keluar_id IS NULL
							)
						) AS guru,
						(
							SELECT
								COUNT (1)
							FROM
								ptk ptk WITH (nolock) 
							JOIN ptk_terdaftar ptkd WITH (nolock) ON ptk.ptk_id = ptkd.ptk_id
							JOIN ref.tahun_ajaran ta WITH (nolock) ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
							WHERE
								ptkd.sekolah_id = s.sekolah_id
							AND ptk.Soft_delete = 0
							AND ptkd.Soft_delete = 0
							AND ptkd.ptk_induk != 1
							AND ptkd.tahun_ajaran_id = LEFT(".$semester->getSemesterId().", 4)
							AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
							AND (
								ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								OR ptkd.jenis_keluar_id IS NULL
							)
						) AS guru_non_induk,
						(
							SELECT
								COUNT (1)
							FROM
								ptk ptk 
							JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
							JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
							WHERE
								ptkd.sekolah_id = s.sekolah_id
							AND ptk.Soft_delete = 0
							AND ptk.jenis_kelamin = 'L'
							AND ptkd.Soft_delete = 0
							AND ptkd.ptk_induk = 1
							AND ptkd.tahun_ajaran_id = LEFT(".$semester->getSemesterId().", 4)
							AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
							AND (
								ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								OR ptkd.jenis_keluar_id IS NULL
							)
						) AS guru_laki,
						(
							SELECT
								COUNT (1)
							FROM
								ptk ptk
							JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
							JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
							WHERE
								ptkd.sekolah_id = s.sekolah_id
							AND ptk.Soft_delete = 0
							AND ptk.jenis_kelamin = 'P'
							AND ptkd.Soft_delete = 0
							AND ptkd.ptk_induk = 1
							AND ptkd.tahun_ajaran_id = LEFT(".$semester->getSemesterId().", 4)
							AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
							AND (
								ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								OR ptkd.jenis_keluar_id IS NULL
							)
						) AS guru_perempuan,
						(
							SELECT
								COUNT (1)
							FROM
								ptk ptk
							JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
							JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
							WHERE
								ptkd.sekolah_id = s.sekolah_id
							AND ptk.Soft_delete = 0
							AND ptkd.Soft_delete = 0
							AND ptkd.ptk_induk = 1
							AND ptkd.tahun_ajaran_id = LEFT(".$semester->getSemesterId().", 4)
							AND ptk.jenis_ptk_id IN (11, 99, 30, 40)
							AND (
								ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								OR ptkd.jenis_keluar_id IS NULL
							)
						) AS pegawai,
						(
							SELECT
								COUNT (1)
							FROM
								ptk ptk
							JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
							JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
							WHERE
								ptkd.sekolah_id = s.sekolah_id
							AND ptk.Soft_delete = 0
							AND ptkd.Soft_delete = 0
							AND ptkd.ptk_induk != 1
							AND ptkd.tahun_ajaran_id = LEFT(".$semester->getSemesterId().", 4)
							AND ptk.jenis_ptk_id IN (11, 99, 30, 40)
							AND (
								ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								OR ptkd.jenis_keluar_id IS NULL
							)
						) AS pegawai_non_induk,
						(
							SELECT
								COUNT (1)
							FROM
								ptk ptk
							JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
							JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
							WHERE
								ptkd.sekolah_id = s.sekolah_id
							AND ptk.Soft_delete = 0
							AND ptk.jenis_kelamin = 'L'
							AND ptkd.Soft_delete = 0
							AND ptkd.ptk_induk = 1
							AND ptkd.tahun_ajaran_id = LEFT(".$semester->getSemesterId().", 4)
							AND ptk.jenis_ptk_id IN (11, 99, 30, 40)
							AND (
								ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								OR ptkd.jenis_keluar_id IS NULL
							)
						) AS pegawai_laki,
						(
							SELECT
								COUNT (1)
							FROM
								ptk ptk
							JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
							JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
							WHERE
								ptkd.sekolah_id = s.sekolah_id
							AND ptk.Soft_delete = 0
							AND ptk.jenis_kelamin = 'P'
							AND ptkd.Soft_delete = 0
							AND ptkd.ptk_induk = 1
							AND ptkd.tahun_ajaran_id = LEFT(".$semester->getSemesterId().", 4)
							AND ptk.jenis_ptk_id IN (11, 99, 30, 40)
							AND (
								ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								OR ptkd.jenis_keluar_id IS NULL
							)
						) AS pegawai_perempuan,
						(
							SELECT
								COUNT (1) AS jumlah
							FROM
								rombongan_belajar
							WHERE
								sekolah_id = s.sekolah_id
							AND soft_delete = 0
							AND semester_id = ".$semester->getSemesterId()."
							AND jenis_rombel = 1
						) AS rombel,
						(
							SELECT
								COUNT (DISTINCT(pd.peserta_didik_id))
							FROM
								peserta_didik pd
							JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
							JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
							JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
							JOIN ref.semester sm ON sm.semester_id = rb.semester_id
							WHERE
								rpd.Soft_delete = 0
							AND pd.Soft_delete = 0
							AND (rpd.jenis_keluar_id is null
								OR rpd.tanggal_keluar > sm.tanggal_selesai
								OR rpd.jenis_keluar_id = '1')
							AND sm.tahun_ajaran_id like LEFT(".$semester->getSemesterId().",4)+'%'
							AND s.sekolah_id = rpd.sekolah_id
							AND rb.soft_delete = 0
							AND ar.soft_delete = 0
							AND rb.jenis_rombel = 1
						) as pd,
						(
							SELECT
								COUNT (DISTINCT(pd.peserta_didik_id))
							FROM
								peserta_didik pd
							JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
							JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
							JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
							JOIN ref.semester sm ON sm.semester_id = rb.semester_id
							WHERE
								rpd.Soft_delete = 0
							AND pd.Soft_delete = 0
							AND pd.jenis_kelamin = 'L'
							AND (rpd.jenis_keluar_id is null
								OR rpd.tanggal_keluar > sm.tanggal_selesai
								OR rpd.jenis_keluar_id = '1')
							AND sm.tahun_ajaran_id like LEFT(".$semester->getSemesterId().",4)+'%'
							AND s.sekolah_id = rpd.sekolah_id
							AND rb.soft_delete = 0
							AND ar.soft_delete = 0
							AND rb.jenis_rombel = 1
						) as pd_laki,
						(
							SELECT
								COUNT (DISTINCT(pd.peserta_didik_id))
							FROM
								peserta_didik pd
							JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
							JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
							JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
							JOIN ref.semester sm ON sm.semester_id = rb.semester_id
							WHERE
								rpd.Soft_delete = 0
							AND pd.Soft_delete = 0
							AND pd.jenis_kelamin = 'P'
							AND (rpd.jenis_keluar_id is null
								OR rpd.tanggal_keluar > sm.tanggal_selesai
								OR rpd.jenis_keluar_id = '1')
							AND sm.tahun_ajaran_id like LEFT(".$semester->getSemesterId().",4)+'%'
							AND s.sekolah_id = rpd.sekolah_id
							AND rb.soft_delete = 0
							AND ar.soft_delete = 0
							AND rb.jenis_rombel = 1
						) as pd_perempuan
					FROM
						sekolah s WITH (nolock)
					JOIN ref.mst_wilayah kecamatan WITH (nolock) on left(s.kode_wilayah,6) = kecamatan.kode_wilayah
					JOIN ref.mst_wilayah kabupaten WITH (nolock) on kecamatan.mst_kode_wilayah = kabupaten.kode_wilayah
					JOIN ref.mst_wilayah propinsi WITH (nolock) on kabupaten.mst_kode_wilayah = propinsi.kode_wilayah
					ORDER BY
						s.nama OFFSET ".$i." ROWS FETCH NEXT 1000 ROWS ONLY";

			$fetch = getDataBySql($sql);

			$count = 1;
			$objCount = 1000;

			for ($j=0; $j < sizeof($fetch); $j++) {

				$data_proses++;

				$arr = $fetch[$j];

				$checkExistSql = "select sekolah_id from rekap_sekolah where sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

				$dataExist = getDataByQuery($checkExistSql, 'rekap');

				if(sizeof($dataExist) > 0){
					//update

					$set = "";

					foreach ($fetch[$j] as $key => $value) {

						if($key != 'sekolah_id'){

							if($value != null){
								$set .= ", ".$key." = '".trim($value)."'";
							}else{
								$set .= ", ".$key." = null";
							}

						}
					}

					$set = substr($set, 1, strlen($set));

					$sql_insert = "Update rekap_sekolah set ".$set." where sekolah_id = '".$fetch[$j]['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

					// echo $br.$sql_insert;continue;

					$result = execQuery($sql_insert);

					if($result['success']){
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap data sekolah ".$fetch[$j]['sekolah_id'];
					}else{
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [UPDATE] GAGAL merekap data sekolah ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
					}

				}else{
					$str_key = "";
					$str_value = "";
					//insert
					foreach ($fetch[$j] as $key => $value) {
						$str_key .= ",".$key;

						if($value != null){
							$str_value .= ",'".trim($value)."'";
						}else{
							$str_value .= ",null";
						}

					}

					$str_key = substr($str_key, 1, strlen($str_key));
					$str_value = substr($str_value, 1, strlen($str_value));

					$sql_insert = "insert into rekap_sekolah(".$str_key.") values(".$str_value.")";
		
					// echo $br."[".date('H:i:s')."] ".$sql_insert;
					$result = execQuery($sql_insert);

					if($result['success']){
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [INSERT] BERHASIL merekap data sekolah ".$fetch[$j]['sekolah_id'];
					}else{
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [INSERT] GAGAL merekap data sekolah ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
					}
				}

			}
		}

	}

}






?>