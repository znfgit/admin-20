<?php

include "startup.php";

$z = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $z->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, 2016);
$z->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($z);

foreach ($semesters as $semester) {
	# code...
	echo $br."[".date('H:i:s')."] Mengambil data tabel semester: ".$semester->getNama();

	$sql_count = "select sum(1) as total FROM
					rekap_kualitas_data_sekolah
				where semester_id = ".$semester->getSemesterId();

	$fetch_count = getDataByQuery($sql_count, 'rekap');

	$recordCountX = $fetch_count[0]['total'];

	echo $br."[".date('H:i:s')."] Jumlah Record: ".$recordCountX;

	$total = $recordCountX; 
		
	$data_proses = 0;

	for ($i=0; $i < $recordCountX; $i++) { 

		if($i % 10 == 0){

			echo $br."[".date('H:i:s')."] Mengambil data kualitas sekolah SUM dari record ".($i+1)." sampai ".($i+10).": OK";

			$sql = "SELECT 
						isnull(
							round(
								(
									(
										CAST (
											(
												npsn_valid + sk_pendirian_sekolah_valid + tanggal_sk_pendirian_valid + sk_izin_operasional_valid + tanggal_sk_izin_operasional_valid + no_rekening_valid + nama_bank_valid + cabang_kcp_unit_valid + rekening_atas_nama_valid + no_hp_kepsek_valid + nama_kepsek_valid + email_kepsek_valid + daya_listrik_valid + partisipasi_bos_valid + waktu_penyelenggaraan_id_valid + sumber_listrik_id_valid + sertifikasi_iso_id_valid + akses_internet_id_valid
											) AS FLOAT (24)
										) / 18
									) * 100
								),
								2
							),
							0
						) AS identitas_valid,
						isnull((
							SELECT
								round(
									(
										SUM (
											isnull(
												round(
													(
														(
															CAST (
																(
																	tgl_cpns_valid + sk_cpns_valid + tmt_pns_valid + pangkat_golongan_id_valid + nama_suami_istri_valid + pekerjaan_suami_istri_valid + sk_pengangkatan_valid + tmt_pengangkatan_valid + nama_ibu_kandung_valid + nik_valid + nuptk_valid + desa_kelurahan_valid + no_hp_valid + email_valid + npwp_valid
																) AS FLOAT (24)
															) / 15
														) * 100
													),
													2
												),
												0
											)
										) / isnull(SUM(1), 1)
									),
									2
								) AS ptk_valid
							FROM
								rekap_kualitas_data_ptk
							WHERE
								soft_delete = 0
							AND sekolah_id = rekap_kualitas_data_sekolah.sekolah_id
							AND semester_id = rekap_kualitas_data_sekolah.semester_id
						),0) AS ptk_valid,
						isnull((
							SELECT
								round(
									(
										SUM (
											isnull(
												round(
													(
														(
															CAST (
																(
																	panjang_valid + lebar_valid + kerusakan_valid + sarana_valid + buku_alat_valid
																) AS FLOAT (24)
															) / 5
														) * 100
													),
													2
												),
												0
											)
										) / isnull(SUM(1), 1)
									),
									2
								) AS prasarana_valid
							FROM
								rekap_kualitas_data_prasarana
							WHERE
								soft_delete = 0
							AND sekolah_id = rekap_kualitas_data_sekolah.sekolah_id
							AND semester_id = rekap_kualitas_data_sekolah.semester_id
						),0) AS prasarana_valid,
						isnull((
							SELECT
								round(
									(
										SUM (
											isnull(
												round(
													(
														(
															CAST (
																(
																	nisn_valid + nik_valid + desa_kelurahan_valid + kode_pos_valid + jenis_tinggal_valid + alat_transportasi_id_valid + nomor_telepon_seluler_valid + email_valid + no_kps_valid + nama_ayah_valid + tahun_lahir_ayah_valid + jenjang_pendidikan_ayah_valid + pekerjaan_id_ayah_valid + penghasilan_id_ayah_valid + nama_ibu_kandung_valid + tahun_lahir_ibu_valid + jenjang_pendidikan_ibu_valid + penghasilan_id_ibu_valid + pekerjaan_id_ibu_valid + nipd_valid + no_skhun_valid
																) AS FLOAT (24)
															) / 21
														) * 100
													),
													2
												),
												0
											)
										) / SUM (1)
									),
									2
								) AS pd_valid
							FROM
								rekap_kualitas_data_pd
							WHERE
								soft_delete = 0
							AND sekolah_id = rekap_kualitas_data_sekolah.sekolah_id
							AND semester_id = rekap_kualitas_data_sekolah.semester_id
						),0) AS pd_valid,
						sekolah_id,
						semester_id,
						tahun_ajaran_id,
						nama,
						npsn,
						bentuk_pendidikan_id,
						status_sekolah,
						kecamatan,
						kabupaten,
						propinsi,
						kode_wilayah_kecamatan,
						kode_wilayah_kabupaten,
						kode_wilayah_propinsi,
						mst_kode_wilayah_kecamatan,
						mst_kode_wilayah_kabupaten,
						mst_kode_wilayah_propinsi,
						id_level_wilayah_kecamatan,
						id_level_wilayah_kabupaten,
						id_level_wilayah_propinsi,
						getdate() AS tanggal,
						newid() rekap_kualitas_data_sekolah_sum_id,
						soft_delete
					FROM
						rekap_kualitas_data_sekolah
					where semester_id = ".$semester->getSemesterId()."
					-- and sekolah_id = '44CAABD5-C624-4D74-BFA5-EDD5C729F9E3'
					ORDER BY
					 	sekolah_id desc OFFSET ".$i." ROWS FETCH NEXT 10 ROWS ONLY";

			// echo $sql;die;

			try {
				$fetch = getDataByQuery($sql, 'rekap');

				// print_r(sqlsrv_errors());
				// echo $sql;

				$count = 1;
				$objCount = 10;

				for ($j=0; $j < sizeof($fetch); $j++) {

					$data_proses++;

					$arr = $fetch[$j];
					
					$arr['rata_rata_valid'] = round((($arr['identitas_valid']+$arr['ptk_valid']+$arr['pd_valid']+$arr['prasarana_valid'])/4),2);

					$checkExistSql = "select sekolah_id from rekap_kualitas_data_sekolah_sum where sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

					$dataExist = getDataByQuery($checkExistSql, 'rekap');

					if(sizeof($dataExist) > 0){
						//update

						$set = "";

						foreach ($arr as $key => $value) {

							if($key != 'sekolah_id'){

								// if($value != null){
									$set .= ", ".$key." = '".trim(str_replace("'","''",$value))."'";
								// }else{
									// $set .= ", ".$key." = null";
								// }

							}
						}

						$set = substr($set, 1, strlen($set));

						$sql_insert = "Update rekap_kualitas_data_sekolah_sum set ".$set." where sekolah_id = '".$arr['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

						// echo $br.$sql_insert;continue;

						$result = execQuery($sql_insert);

						if($result['success']){
							echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap kualitas data sekolah ".$arr['sekolah_id'];
						}else{
							echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [UPDATE] GAGAL merekap kualitas data sekolah ".$arr['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
						}

					}else{
						$str_key = "";
						$str_value = "";
						//insert
						foreach ($arr as $key => $value) {
							$str_key .= ",".$key;

							// if($value != null){
								$str_value .= ",'".trim(str_replace("'","''",$value))."'";
							// }else{
							// 	$str_value .= ",null";
							// }

						}

						$str_key = substr($str_key, 1, strlen($str_key));
						$str_value = substr($str_value, 1, strlen($str_value));

						$sql_insert = "insert into rekap_kualitas_data_sekolah_sum(".$str_key.") values(".$str_value.")";

						// echo $sql_insert;
			
						// echo $br."[".date('H:i:s')."] ".$sql_insert;
						$result = execQuery($sql_insert);

						if($result['success']){
							echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [INSERT] BERHASIL merekap kualitas data sekolah ".$arr['sekolah_id'];
						}else{
							echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [INSERT] GAGAL merekap kualitas data sekolah ".$arr['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
						}
					}
				}

			} catch (Exception $e) {
				print_r(sqlsrv_errors());
			}
		}
	}
}



?>