<?php
include "startup.php";

$z = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

$z->add(Admin\Model\TahunAjaranPeer::TAHUN_AJARAN_ID, 2016);

$tas = Admin\Model\TahunAjaranPeer::doSelect($z);

foreach ($tas as $ta) {
	# code...
	echo $br."[".date('H:i:s')."] Mengambil data tabel tahun ajaran: ".$ta->getNama();

	$x = new Criteria();
	$recordCountX = Admin\Model\SekolahPeer::doCount($x);

	echo $br."[".date('H:i:s')."] Jumlah Record: ".$recordCountX;

	$total = $recordCountX; 
		
	$data_proses = 0;

	for ($i=0; $i < $recordCountX; $i++) { 

		if($i % 1000 == 0){

			echo $br."[".date('H:i:s')."] Mengambil data tabel sekolah dari record ".($i+1)." sampai ".($i+1000).": OK";

			$obj = array();

			$sql = "SELECT
						newid() as rekap_sekolah_id,
						s.sekolah_id,
						s.nama,
						s.npsn,
						s.bentuk_pendidikan_id,
						s.status_sekolah,
						s.soft_delete,
						null as semester_id,
						'".$ta->getTahunAjaranId()."' as tahun_ajaran_id,
						getdate() as tanggal,
						kec.nama as kecamatan,
						kab.nama as kabupaten,
						prop.nama as propinsi,
						kec.kode_wilayah as kode_wilayah_kecamatan,
						kab.kode_wilayah as kode_wilayah_kabupaten,
						prop.kode_wilayah as kode_wilayah_propinsi,
						kec.id_level_wilayah as id_level_wilayah_kecamatan,
						kab.id_level_wilayah as id_level_wilayah_kabupaten,
						prop.id_level_wilayah as id_level_wilayah_propinsi,
						kec.mst_kode_wilayah as mst_kode_wilayah_kecamatan,
						kab.mst_kode_wilayah as mst_kode_wilayah_kabupaten,
						prop.mst_kode_wilayah as mst_kode_wilayah_propinsi,
						(SELECT TOP 1
							ak.akreditasi_id
						FROM
							akreditasi_sp WITH (nolock)
						JOIN ref.akreditasi ak WITH (nolock) ON ak.akreditasi_id = akreditasi_sp.akreditasi_id
						WHERE
							akreditasi_sp.sekolah_id = s.sekolah_id
						AND akreditasi_sp.soft_delete = 0
						-- AND (
						--	akreditasi_sp.akred_sp_tst IS NULL
						--	OR akreditasi_sp.akred_sp_tst > getdate()
						--)
						ORDER BY akred_sp_tmt desc) as akreditasi_id,
						(SELECT TOP 1
							akreditasi_sp.akred_sp_tmt
						FROM
							akreditasi_sp WITH (nolock)
						JOIN ref.akreditasi ak WITH (nolock) ON ak.akreditasi_id = akreditasi_sp.akreditasi_id
						WHERE
							akreditasi_sp.sekolah_id = s.sekolah_id
						AND akreditasi_sp.soft_delete = 0
						-- AND (
						--	akreditasi_sp.akred_sp_tst IS NULL
						--	OR akreditasi_sp.akred_sp_tst > getdate()
						--)
						ORDER BY akred_sp_tmt desc) as tmt_akreditasi,
						(SELECT TOP 1
							akreditasi_sp.akred_sp_tst
						FROM
							akreditasi_sp WITH (nolock)
						JOIN ref.akreditasi ak WITH (nolock) ON ak.akreditasi_id = akreditasi_sp.akreditasi_id
						WHERE
							akreditasi_sp.sekolah_id = s.sekolah_id
						AND akreditasi_sp.soft_delete = 0
						-- AND (
						--	akreditasi_sp.akred_sp_tst IS NULL
						--	OR akreditasi_sp.akred_sp_tst > getdate()
						--)
						ORDER BY akred_sp_tmt desc) as tst_akreditasi,
						(SELECT TOP 1
							akreditasi_sp.akred_sp_sk
						FROM
							akreditasi_sp WITH (nolock)
						JOIN ref.akreditasi ak WITH (nolock) ON ak.akreditasi_id = akreditasi_sp.akreditasi_id
						WHERE
							akreditasi_sp.sekolah_id = s.sekolah_id
						AND akreditasi_sp.soft_delete = 0
						-- AND (
						--	akreditasi_sp.akred_sp_tst IS NULL
						--	OR akreditasi_sp.akred_sp_tst > getdate()
						--)
						ORDER BY akred_sp_tmt desc) as sk_akreditasi,
						(
						SELECT
							slong.waktu_penyelenggaraan_id AS waktu_penyelenggaraan_id
						FROM
							sekolah_longitudinal slong WITH (nolock)
						JOIN ref.waktu_penyelenggaraan waktu WITH (nolock) ON waktu.waktu_penyelenggaraan_id = slong.waktu_penyelenggaraan_id
						WHERE
							slong.sekolah_id = s.sekolah_id
						AND slong.soft_delete = 0
						AND slong.semester_id = '".$ta->getTahunAjaranId()."1'
						) as waktu_penyelenggaraan_id
					FROM
						sekolah s WITH (nolock)
					JOIN ref.mst_wilayah kec WITH (nolock) ON kec.kode_wilayah = LEFT(s.kode_wilayah, 6)
					JOIN ref.mst_wilayah kab WITH (nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
					JOIN ref.mst_wilayah prop WITH (nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
					where s.soft_delete = 0
					ORDER BY
						s.nama OFFSET ".$i." ROWS FETCH NEXT 1000 ROWS ONLY";

			$fetch = getDataBySql($sql);

			$count = 1;
			$objCount = 1000;

			for ($j=0; $j < sizeof($fetch); $j++) {

				$data_proses++;

				$arr = $fetch[$j];

				$checkExistSql = "select sekolah_id from rekap_sekolah where sekolah_id = '".$arr['sekolah_id']."' and semester_id is null and tahun_ajaran_id = '".$ta->getTahunAjaranId()."'"; 

				$dataExist = getDataByQuery($checkExistSql, 'rekap');

				if(sizeof($dataExist) > 0){
					//update

					$set = "";

					foreach ($fetch[$j] as $key => $value) {

						if($key != 'sekolah_id' && $key != 'rekap_sekolah_id'){

							if($value != null){
								$set .= ", ".$key." = '".trim($value)."'";
							}else{
								$set .= ", ".$key." = null";
							}

						}
					}

					$set = substr($set, 1, strlen($set));

					$sql_insert = "Update rekap_sekolah set ".$set." where sekolah_id = '".$fetch[$j]['sekolah_id']."' and semester_id is null and tahun_ajaran_id = ".$ta->getTahunAjaranId();	

					// echo $br.$sql_insert;continue;

					$result = execQuery($sql_insert);

					if($result['success']){
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap data akreditasi sekolah ta [".$fetch[$j]['akreditasi_id']."] ".$fetch[$j]['sekolah_id'];
					}else{
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [UPDATE] GAGAL merekap data akreditasi sekolah ta ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
					}

				}else{
					$str_key = "";
					$str_value = "";
					//insert
					foreach ($fetch[$j] as $key => $value) {
						$str_key .= ",".$key;

						if($value != null){
							$str_value .= ",'".trim($value)."'";
						}else{
							$str_value .= ",null";
						}

					}

					$str_key = substr($str_key, 1, strlen($str_key));
					$str_value = substr($str_value, 1, strlen($str_value));

					$sql_insert = "insert into rekap_sekolah(".$str_key.") values(".$str_value.")";
		
					// echo $br."[".date('H:i:s')."] ".$sql_insert;
					$result = execQuery($sql_insert);

					if($result['success']){
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [INSERT] BERHASIL merekap data akreditasi sekolah ta ".$fetch[$j]['sekolah_id'];
					}else{
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [INSERT] GAGAL merekap data akreditasi sekolah ta ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
					}
				}

			}
		}

	}

}






?>