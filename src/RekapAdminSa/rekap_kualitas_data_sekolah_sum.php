<?php

$limit_var = 100;

include "startup.php";

$semesterCriteria = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $semesterCriteria->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, ".$semester->getTahunAjaranId().");
$semesterCriteria->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($semesterCriteria);

foreach ($semesters as $semester) {

	$z = new Criteria();

	echo $br."[".date('H:i:s')."] Mengambil data propinsi: OK";

	$z->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
	$z->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 1);

	$propinsis = Admin\Model\MstWilayahPeer::doSelect($z);
	$propinsiCount = Admin\Model\MstWilayahPeer::doCount($z);

	$data_proses_propinsi = 0;

	foreach ($propinsis as $propinsi) {

		$data_proses_propinsi++;

		echo $br."[".date('H:i:s')."] |__ [".$data_proses_propinsi."/".$propinsiCount."] Mengambil kabupaten dari propinsi: ".$propinsi->getNama()." - ".trim($propinsi->getKodeWilayah());

		echo $br."[".date('H:i:s')."] |__ delete record lama [".$semester->getNama()."] ...";

		$checkExistSql = "delete from rekap_kualitas_data_sekolah_sum where kode_wilayah_propinsi = '".trim($propinsi->getKodeWilayah())."' and semester_id = '".$semester->getSemesterId()."'"; 

		// echo $checkExistSql;die;

		$dataExist = execQuery($checkExistSql, 'rekap');

		if($dataExist){
			echo $br."[".date('H:i:s')."] |__ delete record lama [".$semester->getNama()."] [OK]";
		}else{
			echo $br."[".date('H:i:s')."] |__ delete record lama [".$semester->getNama()."] [GAGAL]";
		}

		$y = new Criteria();

		$y->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
		$y->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 2);
		$y->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($propinsi->getKodeWilayah()));

		$kabupatenCount = Admin\Model\MstWilayahPeer::doCount($y);
		$kabupatens = Admin\Model\MstWilayahPeer::doSelect($y);

		$data_proses_kabupaten = 0;

		foreach ($kabupatens as $kabupaten) {
			$data_proses_kabupaten++;

			echo $br."[".date('H:i:s')."] |  |__ [".$data_proses_kabupaten."/".$kabupatenCount."] Mengambil kecamatan dari kabupaten: ".$kabupaten->getNama()." - ".trim($kabupaten->getKodeWilayah());

			$x = new Criteria();

			$x->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
			$x->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 3);
			$x->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($kabupaten->getKodeWilayah()));

			$kecamatanCount = Admin\Model\MstWilayahPeer::doCount($x);
			$kecamatans = Admin\Model\MstWilayahPeer::doSelect($x);

			$data_proses_kecamatan = 0;

			foreach ($kecamatans as $kecamatan) {
				$data_proses_kecamatan++;

				echo $br."[".date('H:i:s')."] |  |  |__ [".$data_proses_kecamatan."/".$kecamatanCount."] Mengambil data sekolah dari kecamatan: ".$kecamatan->getNama()." - ".trim($kecamatan->getKodeWilayah());
			
				$sql_count = "select sum(1) as total FROM
							sekolah WITH (nolock)
						JOIN ref.mst_wilayah kec WITH (nolock) ON kec.kode_wilayah = LEFT (sekolah.kode_wilayah, 6)
						JOIN ref.mst_wilayah kab WITH (nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
						JOIN ref.mst_wilayah prop WITH (nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
						where 
						--soft_delete = 0
						--and 
						bentuk_pendidikan_id = 13
						and kec.kode_wilayah = '".trim($kecamatan->getKodeWilayah())."'
						";

				$fetch_count = getDataByQuery($sql_count);

				$recordCountX = $fetch_count[0]['total'];

				echo $br."[".date('H:i:s')."] |  |  |  |__ Jumlah Record: ".$recordCountX;

				$total = $recordCountX; 
			
				$data_proses = 0;
			
				for ($i=0; $i < $recordCountX; $i++) {
					if($i % $limit_var == 0){
						echo $br."[".date('H:i:s')."] |  |  |  |__ Mengambil data kualitas sekolah SUM dari record ".($i+1)." sampai ".($i+$limit_var).": OK";

						$sql = "SELECT
							newid() AS rekap_kualitas_data_sekolah_sum_id,
							sekolah.sekolah_id,
							sekolah.nama AS nama,
							sekolah.npsn,
							sekolah.bentuk_pendidikan_id,
							sekolah.status_sekolah,
							kec.nama AS kecamatan,
							kab.nama AS kabupaten,
							prop.nama AS propinsi,
							'".$semester->getSemesterId()."' AS semester_id,
							'".$semester->getTahunAjaranId()."' AS tahun_ajaran_id,
							getdate() AS tanggal,
							kec.kode_wilayah AS kode_wilayah_kecamatan,
							kab.kode_wilayah AS kode_wilayah_kabupaten,
							prop.kode_wilayah AS kode_wilayah_propinsi,
							kec.mst_kode_wilayah AS mst_kode_wilayah_kecamatan,
							kab.mst_kode_wilayah AS mst_kode_wilayah_kabupaten,
							prop.mst_kode_wilayah AS mst_kode_wilayah_propinsi,
							kec.id_level_wilayah AS id_level_wilayah_kecamatan,
							kab.id_level_wilayah AS id_level_wilayah_kabupaten,
							prop.id_level_wilayah AS id_level_wilayah_propinsi,
							soft_delete,
							isnull( (round( (cast ((
								(
									CASE
									WHEN npsn IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN sk_pendirian_sekolah IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN tanggal_sk_pendirian IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN sk_izin_operasional IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN tanggal_sk_izin_operasional IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN no_rekening IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN nama_bank IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN cabang_kcp_unit IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN rekening_atas_nama IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN (
										SELECT
											TOP 1 ptk.nama AS nama_kepsek
										FROM
											tugas_tambahan WITH (nolock)
										JOIN ptk WITH (nolock) ON tugas_tambahan.ptk_id = ptk.ptk_id
										JOIN ptk_terdaftar WITH (nolock) ON ptk_terdaftar.ptk_id = ptk.ptk_id
										WHERE
											ptk.soft_delete = 0
										AND tugas_tambahan.Soft_delete = 0
										AND ptk_terdaftar.Soft_delete = 0
										AND ptk_terdaftar.sekolah_id = sekolah.sekolah_id
										AND tugas_tambahan.jabatan_ptk_id = 2
										AND ptk_terdaftar.tahun_ajaran_id = ".$semester->getTahunAjaranId()."
										ORDER BY
											tugas_tambahan.tmt_tambahan DESC
									) IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN (
										SELECT
											TOP 1 ptk.no_hp
										FROM
											tugas_tambahan WITH (nolock)
										JOIN ptk WITH (nolock) ON tugas_tambahan.ptk_id = ptk.ptk_id
										JOIN ptk_terdaftar WITH (nolock) ON ptk_terdaftar.ptk_id = ptk.ptk_id
										WHERE
											ptk.soft_delete = 0
										AND tugas_tambahan.Soft_delete = 0
										AND ptk_terdaftar.Soft_delete = 0
										AND ptk_terdaftar.sekolah_id = sekolah.sekolah_id
										AND tugas_tambahan.jabatan_ptk_id = 2
										AND ptk_terdaftar.tahun_ajaran_id = ".$semester->getTahunAjaranId()."
										ORDER BY
											tugas_tambahan.tmt_tambahan DESC
									) IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN (
										SELECT
											TOP 1 ptk.email
										FROM
											tugas_tambahan WITH (nolock)
										JOIN ptk WITH (nolock) ON tugas_tambahan.ptk_id = ptk.ptk_id
										JOIN ptk_terdaftar WITH (nolock) ON ptk_terdaftar.ptk_id = ptk.ptk_id
										WHERE
											ptk.soft_delete = 0
										AND tugas_tambahan.Soft_delete = 0
										AND ptk_terdaftar.Soft_delete = 0
										AND ptk_terdaftar.sekolah_id = sekolah.sekolah_id
										AND tugas_tambahan.jabatan_ptk_id = 2
										AND ptk_terdaftar.tahun_ajaran_id = ".$semester->getTahunAjaranId()."
										ORDER BY
											tugas_tambahan.tmt_tambahan DESC
									) IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN (
										SELECT
											daya_listrik
										FROM
											sekolah_longitudinal WITH (nolock)
										WHERE
											sekolah_id = sekolah.sekolah_id
										AND sekolah_longitudinal.soft_delete = 0
										AND sekolah_longitudinal.semester_id = ".$semester->getSemesterId()."
									) IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN (
										SELECT
											partisipasi_bos
										FROM
											sekolah_longitudinal WITH (nolock)
										WHERE
											sekolah_id = sekolah.sekolah_id
										AND sekolah_longitudinal.soft_delete = 0
										AND sekolah_longitudinal.semester_id = ".$semester->getSemesterId()."
									) IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN (
										SELECT
											waktu_penyelenggaraan_id
										FROM
											sekolah_longitudinal WITH (nolock)
										WHERE
											sekolah_id = sekolah.sekolah_id
										AND sekolah_longitudinal.soft_delete = 0
										AND sekolah_longitudinal.semester_id = ".$semester->getSemesterId()."
									) IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN (
										SELECT
											sumber_listrik_id
										FROM
											sekolah_longitudinal WITH (nolock)
										WHERE
											sekolah_id = sekolah.sekolah_id
										AND sekolah_longitudinal.soft_delete = 0
										AND sekolah_longitudinal.semester_id = ".$semester->getSemesterId()."
									) IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN (
										SELECT
											sertifikasi_iso_id
										FROM
											sekolah_longitudinal WITH (nolock)
										WHERE
											sekolah_id = sekolah.sekolah_id
										AND sekolah_longitudinal.soft_delete = 0
										AND sekolah_longitudinal.semester_id = ".$semester->getSemesterId()."
									) IS NULL THEN
										0
									ELSE
										1
									END
								) + (
									CASE
									WHEN (
										SELECT
											akses_internet_id
										FROM
											sekolah_longitudinal WITH (nolock)
										WHERE
											sekolah_id = sekolah.sekolah_id
										AND sekolah_longitudinal.soft_delete = 0
										AND sekolah_longitudinal.semester_id = ".$semester->getSemesterId()."
									) IS NULL THEN
										0
									ELSE
										1
									END
								)
							) as float(24) ) / 18 ),2) * 100 ) ,0) AS identitas_valid,
							isnull( (select 
								round( ( sum( (round( (cast( (
									(case when pd.nisn is null or pd.nisn = '-' then 0 else 1 end)+
									(case when pd.nik is null or pd.nik = '-' then 0 else 1 end)+
									(case when pd.desa_kelurahan is null or pd.desa_kelurahan = '-' then 0 else 1 end)+
									(case when pd.kode_pos is null or pd.kode_pos = '-' then 0 else 1 end)+
									(case when pd.jenis_tinggal_id is null then 0 else 1 end)+
									(case when pd.alat_transportasi_id is null then 0 else 1 end)+
									(case when pd.nomor_telepon_seluler is null then 0 else 1 end)+
									(case when pd.email is null then 0 else 1 end)+
									(case when pd.penerima_kps = 1 then 
										(case when pd.no_kps is null then 0 else 1 end)
										else
										1
										end)+
									(case when pd.nama_ayah is null or pd.nama_ayah = '-' then 0 else 1 end)+
									(case when pd.tahun_lahir_ayah is null or pd.tahun_lahir_ayah = 0 then 0 else 1 end)+
									(case when pd.jenjang_pendidikan_ayah is null then 0 else 1 end)+
									(case when pd.pekerjaan_id_ayah is null then 0 else 1 end)+
									(case when pd.penghasilan_id_ayah is null then 0 else 1 end)+
									(case when pd.nama_ibu_kandung is null or pd.nama_ibu_kandung = '-' then 0 else 1 end)+
									(case when pd.tahun_lahir_ibu is null or pd.tahun_lahir_ibu = 0 then 0 else 1 end)+
									(case when pd.jenjang_pendidikan_ibu is null then 0 else 1 end)+
									(case when pd.penghasilan_id_ibu is null then 0 else 1 end)+
									(case when pd.pekerjaan_id_ibu is null then 0 else 1 end)+
									(case when rpd.nipd is null then 0 else 1 end)+
									(case when rpd.no_skhun is null then 0 else 1 end) 
								) as float(24) ) / 21 ) , 2 ) * 100) ) / sum(1) ) , 2 ) as pd_valid
							FROM
								registrasi_peserta_didik rpd WITH (nolock)
							JOIN peserta_didik pd WITH (nolock) ON rpd.peserta_didik_id = pd.peserta_didik_id
							JOIN anggota_rombel ar WITH (nolock) ON ar.peserta_didik_id = pd.peserta_didik_id
							JOIN rombongan_belajar rb WITH (nolock) ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
							JOIN sekolah s WITH (nolock) ON s.sekolah_id = rb.sekolah_id
							JOIN ref.mst_wilayah kec WITH (nolock) on kec.kode_wilayah = left(s.kode_wilayah,6)
							JOIN ref.mst_wilayah kab WITH (nolock) on kab.kode_wilayah = kec.mst_kode_wilayah
							JOIN ref.mst_wilayah prop WITH (nolock) on prop.kode_wilayah = kab.mst_kode_wilayah
							JOIN ref.semester sm WITH (nolock) ON sm.semester_id = rb.semester_id
							WHERE
								rpd.Soft_delete = 0
							AND s.soft_delete = 0
							AND pd.Soft_delete = 0
							AND (
								rpd.jenis_keluar_id IS NULL OR
								rpd.tanggal_keluar > sm.tanggal_selesai
							)
							AND rb.semester_id = '".$semester->getSemesterId()."'
							AND rb.soft_delete = 0
							AND ar.soft_delete = 0
							AND rb.jenis_rombel = 1
							and rpd.sekolah_id = sekolah.sekolah_id
							) ,0) as pd_valid,
							isnull( (select 
								round( ( sum( (round( (cast( ((case when ptk.status_kepegawaian_id in(1,2,3,10) then 
									(case when ptk.sk_cpns is null then 0 else 1 end)
									else
									1
									end)+
								(case when ptk.status_kepegawaian_id in(1,2,3,10) then 
									(case when ptk.tgl_cpns is null then 0 else 1 end)
									else
									1
									end)+
								(case when ptk.status_kepegawaian_id in(1,2,3,10) then 
									(case when ptk.tmt_pns is null then 0 else 1 end)
									else
									1
									end)+
								(case when ptk.status_kepegawaian_id in(1,2,3,10) then 
									(case when ptk.pangkat_golongan_id is null then 0 else 1 end)
									else
									1
									end)+
								(case when ptk.status_perkawinan = 1 then 
									(case when ptk.nama_suami_istri is null then 0 else 1 end)
									else
									1
									end)+
								(case when ptk.status_perkawinan = 1 then 
									(case when ptk.pekerjaan_suami_istri is null then 0 else 1 end)
									else
									1
									end)+
								(case when ptk.sk_pengangkatan is null or ptk.sk_pengangkatan = '-' then 0 else 1 end)+
								(case when ptk.tmt_pengangkatan is null then 0 else 1 end)+
								(case when ptk.nama_ibu_kandung is null or ptk.nama_ibu_kandung = '-' then 0 else 1 end)+
								(case when ptk.nik is null or ptk.nik = '-' then 0 else 1 end)+
								(case when ptk.nuptk is null or ptk.nuptk = '-' then 0 else 1 end)+
								(case when ptk.desa_kelurahan is null or ptk.desa_kelurahan = '-' then 0 else 1 end)+
								(case when ptk.no_hp is null or ptk.no_hp = '-' then 0 else 1 end)+
								(case when ptk.email is null or ptk.email = '-' then 0 else 1 end)+
								(case when ptk.npwp is null or ptk.npwp = '-' then 0 else 1 end)) as float(24) ) / 15 ) , 2 ) * 100) ) / sum(1) ) , 2 )
							FROM
								ptk ptk with(nolock)
							JOIN ptk_terdaftar ptkd with(nolock) ON ptk.ptk_id = ptkd.ptk_id
							JOIN ref.tahun_ajaran ta with(nolock) ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
							JOIN sekolah s with(nolock) on s.sekolah_id = ptkd.sekolah_id
							JOIN ref.mst_wilayah kec with(nolock) on kec.kode_wilayah = left(s.kode_wilayah,6)
							JOIN ref.mst_wilayah kab with(nolock) on kab.kode_wilayah = kec.mst_kode_wilayah
							JOIN ref.mst_wilayah prop with(nolock) on prop.kode_wilayah = kab.mst_kode_wilayah
							WHERE
								ptkd.Soft_delete = 0
							AND ptk.Soft_delete = 0
							AND s.Soft_delete = 0
							AND ptkd.ptk_induk = 1
							AND ptkd.tahun_ajaran_id = ".$semester->getTahunAjaranId()."
							AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
							AND (
								ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								OR ptkd.jenis_keluar_id IS NULL
							)
							AND ptkd.sekolah_id = sekolah.sekolah_id
							) ,0) as ptk_valid,
							isnull( (select
								round( ( sum( (round( (cast( ((
										CASE
										WHEN pras.panjang IS NULL
										OR pras.panjang = 0 THEN
											0
										ELSE
											1
										END
									)+
									(
										CASE
										WHEN pras.lebar IS NULL
										OR pras.lebar = 0 THEN
											0
										ELSE
											1
										END
									)+
									(
										CASE
										WHEN praslong.prasarana_id IS NULL THEN
											0
										ELSE
											1
										END
									)+
									(
										CASE
										WHEN sarana.jumlah > 0 THEN
											1
										ELSE
											0
										END
									)+
									(
										CASE
										WHEN buku_alat.jumlah > 0 THEN
											1
										ELSE
											0
										END
									)) as float(24) ) / 5 ) , 2 ) * 100) ) / sum(1) ) , 2 )
								FROM
									prasarana pras with(nolock)
								JOIN sekolah s with(nolock) ON s.sekolah_id = pras.sekolah_id
								JOIN ref.mst_wilayah kec with(nolock) ON kec.kode_wilayah = LEFT (s.kode_wilayah, 6)
								JOIN ref.mst_wilayah kab with(nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
								JOIN ref.mst_wilayah prop with(nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
								LEFT OUTER JOIN prasarana_longitudinal praslong with(nolock) ON praslong.prasarana_id = pras.prasarana_id
								AND praslong.semester_id = ".$semester->getSemesterId()."
								LEFT OUTER JOIN (
									SELECT
										buku_alat.prasarana_id,
										SUM (1) as jumlah
									FROM
										buku_alat
									WHERE buku_alat.soft_delete = 0
									group by buku_alat.prasarana_id
								) buku_alat on buku_alat.prasarana_id = pras.prasarana_id
								LEFT OUTER JOIN (
									SELECT
										sarana.prasarana_id,
										SUM (1) as jumlah
									FROM
										sarana
									WHERE sarana.soft_delete = 0
									group by sarana.prasarana_id
								) sarana on sarana.prasarana_id = pras.prasarana_id
								where pras.sekolah_id = sekolah.sekolah_id
								and pras.soft_delete = 0
								and praslong.soft_delete = 0) ,0) as prasarana_valid
							FROM
								sekolah WITH (nolock)
							JOIN ref.mst_wilayah kec WITH (nolock) ON kec.kode_wilayah = LEFT (sekolah.kode_wilayah, 6)
							JOIN ref.mst_wilayah kab WITH (nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
							JOIN ref.mst_wilayah prop WITH (nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
							where 
							--soft_delete = 0
							--and 
							bentuk_pendidikan_id = 13
							--and sekolah.sekolah_id = 'F3FB19D3-DC27-47B8-88D3-3B6A5EB36EC3'
							and kec.kode_wilayah = '".trim($kecamatan->getKodeWilayah())."'
							ORDER BY
								prop.kode_wilayah desc OFFSET ".$i." ROWS FETCH NEXT ".$limit_var." ROWS ONLY";

						try {
							$fetch = getDataByQuery($sql);

							$count = 1;
							$objCount = 10;

							for ($j=0; $j < sizeof($fetch); $j++) {
								$data_proses++;

								$arr = $fetch[$j];
								
								$arr['rata_rata_valid'] = round((($arr['identitas_valid']+$arr['ptk_valid']+$arr['pd_valid']+$arr['prasarana_valid'])/4),2);

								$str_key = "";
								$str_value = "";
								//insert
								foreach ($arr as $key => $value) {
									$str_key .= ",".$key;

									// if($value != null){
										$str_value .= ",'".trim(str_replace("'","''",$value))."'";
									// }else{
									// 	$str_value .= ",null";
									// }

								}

								$str_key = substr($str_key, 1, strlen($str_key));
								$str_value = substr($str_value, 1, strlen($str_value));

								$sql_insert = "insert into rekap_kualitas_data_sekolah_sum(".$str_key.") values(".$str_value.")";

								// echo $sql_insert;
					
								// echo $br."[".date('H:i:s')."] ".$sql_insert;
								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [INSERT] BERHASIL merekap kualitas data sekolah ".$arr['sekolah_id']. " - ".$arr['kode_wilayah_propinsi'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [INSERT] GAGAL merekap kualitas data sekolah ".$arr['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}
							}

						} catch (Exception $e) {
							print_r(sqlsrv_errors());
						}

					}

				}
			}
		}

	}

}

?>