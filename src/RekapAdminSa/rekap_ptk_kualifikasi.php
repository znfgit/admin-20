<?php

$limit_var = 100;

include "startup.php";

$semesterCriteria = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $semesterCriteria->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, ".$semester->getTahunAjaranId().");
$semesterCriteria->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($semesterCriteria);

foreach ($semesters as $semester) {

	$z = new Criteria();

	echo $br."[".date('H:i:s')."] Mengambil data propinsi: OK";

	$z->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
	$z->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 1);

	$propinsis = Admin\Model\MstWilayahPeer::doSelect($z);
	$propinsiCount = Admin\Model\MstWilayahPeer::doCount($z);

	$data_proses_propinsi = 0;

	foreach ($propinsis as $propinsi) {

		$data_proses_propinsi++;

		echo $br."[".date('H:i:s')."] |__ [".$data_proses_propinsi."/".$propinsiCount."] Mengambil kabupaten dari propinsi: ".$propinsi->getNama()." - ".trim($propinsi->getKodeWilayah());

		$y = new Criteria();

		$y->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
		$y->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 2);
		$y->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($propinsi->getKodeWilayah()));

		$kabupatenCount = Admin\Model\MstWilayahPeer::doCount($y);
		$kabupatens = Admin\Model\MstWilayahPeer::doSelect($y);

		$data_proses_kabupaten = 0;

		foreach ($kabupatens as $kabupaten) {
			$data_proses_kabupaten++;

			echo $br."[".date('H:i:s')."] |  |__ [".$data_proses_kabupaten."/".$kabupatenCount."] Mengambil kecamatan dari kabupaten: ".$kabupaten->getNama()." - ".trim($kabupaten->getKodeWilayah());

			$x = new Criteria();

			$x->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
			$x->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 3);
			$x->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($kabupaten->getKodeWilayah()));

			$kecamatanCount = Admin\Model\MstWilayahPeer::doCount($x);
			$kecamatans = Admin\Model\MstWilayahPeer::doSelect($x);

			$data_proses_kecamatan = 0;

			foreach ($kecamatans as $kecamatan) {
				$data_proses_kecamatan++;

				echo $br."[".date('H:i:s')."] |  |  |__ [".$data_proses_kecamatan."/".$kecamatanCount."] Mengambil data sekolah dari kecamatan: ".$kecamatan->getNama()." - ".trim($kecamatan->getKodeWilayah());

								$sql_count = "SELECT
								sum(1) as total
							FROM
								sekolah WITH (nolock)
							JOIN ref.mst_wilayah kec WITH (nolock) on kec.kode_wilayah = left(sekolah.kode_wilayah,6)
							JOIN ref.mst_wilayah kab WITH (nolock) on kab.kode_wilayah = kec.mst_kode_wilayah
							JOIN ref.mst_wilayah prop WITH (nolock) on prop.kode_wilayah = kab.mst_kode_wilayah
							where kec.kode_wilayah = '".$kecamatan->getKodeWilayah()."'";

				$fetch_count = getDataByQuery($sql_count);

				$recordCountX = $fetch_count[0]['total'];

				echo $br."[".date('H:i:s')."] |  |  |  |__ Jumlah Record: ".$recordCountX;

				$total = $recordCountX; 			

				$data_proses = 0;
			
				for ($i=0; $i < $recordCountX; $i++) {
					if($i % $limit_var == 0){
						echo $br."[".date('H:i:s')."] |  |  |  |__ Mengambil data kualitas sekolah dari record ".($i+1)." sampai ".($i+$limit_var).": OK";

						$sql = "SELECT
									newid() as rekap_sekolah_id,
									s.sekolah_id,
									s.nama,
									s.npsn,
									s.bentuk_pendidikan_id,
									s.status_sekolah,
									s.soft_delete,
									'".$semester->getSemesterId()."' as semester_id,
									'".$semester->getTahunAjaranId()."' as tahun_ajaran_id,
									-- getdate() as tanggal,
									kec.nama as kecamatan,
									kab.nama as kabupaten,
									prop.nama as propinsi,
									kec.kode_wilayah as kode_wilayah_kecamatan,
									kab.kode_wilayah as kode_wilayah_kabupaten,
									prop.kode_wilayah as kode_wilayah_propinsi,
									kec.id_level_wilayah as id_level_wilayah_kecamatan,
									kab.id_level_wilayah as id_level_wilayah_kabupaten,
									prop.id_level_wilayah as id_level_wilayah_propinsi,
									kec.mst_kode_wilayah as mst_kode_wilayah_kecamatan,
									kab.mst_kode_wilayah as mst_kode_wilayah_kabupaten,
									prop.mst_kode_wilayah as mst_kode_wilayah_propinsi
								FROM
									sekolah s WITH (nolock)
								JOIN ref.mst_wilayah kec WITH (nolock) ON kec.kode_wilayah = LEFT(s.kode_wilayah, 6)
								JOIN ref.mst_wilayah kab WITH (nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
								JOIN ref.mst_wilayah prop WITH (nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
								where s.soft_delete = 0
								and kec.kode_wilayah = '".$kecamatan->getKodeWilayah()."'
								ORDER BY
									s.nama OFFSET ".$i." ROWS FETCH NEXT ".$limit_var." ROWS ONLY";

						$fetch = getDataBySql($sql);

						$count = 1;
						$objCount = 1000;

						for ($j=0; $j < sizeof($fetch); $j++) {

							$data_proses++;

							$arr = $fetch[$j];

							//ambil data kualifikasi ptk
							$sql_ptk = "SELECT
											-- ptkd.sekolah_id
											SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 22 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_d3_laki_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 23 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_d4_laki_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 30 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_s1_laki_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 35 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_s2_laki_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 40 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_s3_laki_kependidikan

											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 22 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_d3_laki_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 23 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_d4_laki_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 30 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_s1_laki_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 35 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_s2_laki_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 40 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_s3_laki_non_kependidikan

											-- --
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 22 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_d3_perempuan_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 23 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_d4_perempuan_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 30 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_s1_perempuan_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 35 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_s2_perempuan_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 40 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_s3_perempuan_kependidikan

											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 22 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_d3_perempuan_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 23 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_d4_perempuan_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 30 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_s1_perempuan_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 35 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_s2_perempuan_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 40 and ptk.jenis_ptk_id in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS guru_s3_perempuan_non_kependidikan

											----
											----
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 22 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_d3_laki_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 23 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_d4_laki_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 30 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_s1_laki_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 35 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_s2_laki_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 40 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_s3_laki_kependidikan

											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 22 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_d3_laki_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 23 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_d4_laki_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 30 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_s1_laki_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 35 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_s2_laki_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 40 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'L' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_s3_laki_non_kependidikan

											-- --
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 22 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_d3_perempuan_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 23 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_d4_perempuan_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 30 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_s1_perempuan_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 35 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_s2_perempuan_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan.jenjang_pendidikan_id > jenjang_pendidikan_non.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan.jenjang_pendidikan_id = 40 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_s3_perempuan_kependidikan

											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 22 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_d3_perempuan_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 23 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_d4_perempuan_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 30 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_s1_perempuan_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 35 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_s2_perempuan_non_kependidikan
											,SUM (
												CASE
												WHEN jenjang_pendidikan_non.jenjang_pendidikan_id > jenjang_pendidikan.jenjang_pendidikan_id THEN
													(case when jenjang_pendidikan_non.jenjang_pendidikan_id = 40 and ptk.jenis_ptk_id not in (3, 4, 5, 6, 12, 13, 14) and ptk.jenis_kelamin = 'P' then 1 else 0 end)
												ELSE
													0
												END
											) AS pegawai_s3_perempuan_non_kependidikan
										FROM
											ptk ptk WITH (nolock)
										LEFT OUTER JOIN (
											SELECT
												formal.ptk_id AS ptk_id,
												MAX (
													formal.jenjang_pendidikan_id
												) AS jenjang_pendidikan_id
											FROM
												rwy_pend_formal formal WITH (nolock)
											WHERE
												formal.soft_delete = 0
											AND formal.jenjang_pendidikan_id < 90
											AND formal.kependidikan = 1
											GROUP BY
												ptk_id
										) AS jenjang_pendidikan ON jenjang_pendidikan.ptk_id = ptk.ptk_id
										LEFT OUTER JOIN (
											SELECT
												formal.ptk_id AS ptk_id,
												MAX (
													formal.jenjang_pendidikan_id
												) AS jenjang_pendidikan_id
											FROM
												rwy_pend_formal formal WITH (nolock)
											WHERE
												formal.soft_delete = 0
											AND formal.jenjang_pendidikan_id < 90
											AND formal.kependidikan = 0
											GROUP BY
												ptk_id
										) AS jenjang_pendidikan_non ON jenjang_pendidikan_non.ptk_id = ptk.ptk_id
										JOIN ptk_terdaftar ptkd WITH (nolock) ON ptk.ptk_id = ptkd.ptk_id
										JOIN ref.tahun_ajaran ta WITH (nolock) ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
										JOIN sekolah  WITH (nolock) ON sekolah.sekolah_id = ptkd.sekolah_id
										WHERE
											ptk.Soft_delete = 0
										AND ptkd.Soft_delete = 0
										AND ptkd.ptk_induk = 1
										AND ptkd.tahun_ajaran_id = LEFT (".$semester->getSemesterId().", 4) -- AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
										AND sekolah.soft_delete = 0
										AND (
											ptkd.tgl_ptk_keluar > ta.tanggal_selesai
											OR ptkd.jenis_keluar_id IS NULL
										)
										AND sekolah.sekolah_id = '".$arr['sekolah_id']."'
										GROUP BY
											ptkd.sekolah_id";

											// echo $sql_ptk;die;

							$fetch_ptk = getDataByQuery($sql_ptk);

							if($fetch_ptk){
								foreach ($fetch_ptk[0] as $key => $value) {
									if($value == 0){
										$value = '0';
									}

									$fetch[$j][$key] = $value;
								}
							}


							// print_r($arr);die;

							//end of ambil data kualifikasi ptk

							$checkExistSql = "select sekolah_id from rekap_sekolah where sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

							$dataExist = getDataByQuery($checkExistSql, 'rekap');

							if(sizeof($dataExist) > 0){
								//update

								$set = "";

								foreach ($fetch[$j] as $key => $value) {

									if($key != 'sekolah_id' && $key != 'rekap_sekolah_id'){

										if($value != null){
											$set .= ", ".$key." = '".str_replace("'", "''", trim($value))."'";
										}else{
											$set .= ", ".$key." = null";
										}

									}
								}

								$set = substr($set, 1, strlen($set));

								$sql_insert = "Update rekap_sekolah set ".$set." where sekolah_id = '".$fetch[$j]['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

								// echo $br.$sql_insert;continue;

								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap data kualifikasi PTK ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [UPDATE] GAGAL merekap data kualifikasi PTK ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}

							}else{
								$str_key = "";
								$str_value = "";
								//insert
								foreach ($fetch[$j] as $key => $value) {
									$str_key .= ",".$key;

									if($value != null){
										$str_value .= ",'".trim($value)."'";
									}else{
										$str_value .= ",null";
									}

								}

								$str_key = substr($str_key, 1, strlen($str_key));
								$str_value = substr($str_value, 1, strlen($str_value));

								$sql_insert = "insert into rekap_sekolah(".$str_key.") values(".$str_value.")";
					
								// echo $br."[".date('H:i:s')."] ".$sql_insert;
								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [INSERT] BERHASIL merekap data kualifikasi PTK ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [INSERT] GAGAL merekap data kualifikasi PTK ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}
							}

						}
					}
				}

			}
		}
	}
}
?>