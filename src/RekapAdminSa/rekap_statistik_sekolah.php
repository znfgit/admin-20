<?php

$limit_var = 100;

include "startup.php";

$semesterCriteria = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $semesterCriteria->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, ".$semester->getTahunAjaranId().");
$semesterCriteria->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($semesterCriteria);

foreach ($semesters as $semester) {

	$z = new Criteria();

	echo $br."[".date('H:i:s')."] Mengambil data propinsi: OK";

	$z->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
	$z->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 1);
	$z->addDescendingOrderByColumn(Admin\Model\MstWilayahPeer::KODE_WILAYAH);

	$propinsis = Admin\Model\MstWilayahPeer::doSelect($z);
	$propinsiCount = Admin\Model\MstWilayahPeer::doCount($z);

	$data_proses_propinsi = 0;

	foreach ($propinsis as $propinsi) {

		$data_proses_propinsi++;

		echo $br."[".date('H:i:s')."] |__ [".$data_proses_propinsi."/".$propinsiCount."] Mengambil kabupaten dari propinsi: ".$propinsi->getNama()." - ".trim($propinsi->getKodeWilayah());

		$y = new Criteria();

		$y->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
		$y->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 2);
		$y->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($propinsi->getKodeWilayah()));

		$kabupatenCount = Admin\Model\MstWilayahPeer::doCount($y);
		$kabupatens = Admin\Model\MstWilayahPeer::doSelect($y);

		$data_proses_kabupaten = 0;

		foreach ($kabupatens as $kabupaten) {
			$data_proses_kabupaten++;

			echo $br."[".date('H:i:s')."] |  |__ [".$data_proses_kabupaten."/".$kabupatenCount."] Mengambil kecamatan dari kabupaten: ".$kabupaten->getNama()." - ".trim($kabupaten->getKodeWilayah());

			$x = new Criteria();

			$x->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
			$x->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 3);
			$x->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($kabupaten->getKodeWilayah()));

			$kecamatanCount = Admin\Model\MstWilayahPeer::doCount($x);
			$kecamatans = Admin\Model\MstWilayahPeer::doSelect($x);

			$data_proses_kecamatan = 0;

			foreach ($kecamatans as $kecamatan) {
				$data_proses_kecamatan++;

				echo $br."[".date('H:i:s')."] |  |  |__ [".$data_proses_kecamatan."/".$kecamatanCount."] Mengambil data sekolah dari kecamatan: ".$kecamatan->getNama()." - ".trim($kecamatan->getKodeWilayah());

				$sql = "SELECT
							sekolah_id,
							kurikulum,
							nama_operator,
							email_operator,
							hp_operator,
							kepala_sekolah as nama_kepsek,
							jenis_kelamin as jenis_kelamin_kepsek,
							jenjang_pendidikan_terakhir as pendidikan_kepsek,
							no_hp as hp_kepsek
						FROM
							rpt.statistiksekolah WITH (nolock)
						WHERE
							kode_kec = '".$kecamatan->getKodeWilayah()."'
						AND periode_aktif = '".$semester->getSemesterId()."'
						and bentuk in ('SMA')";

				// echo $sql;die;

				$fetch = getDataByQuery($sql, 'statistik');

				for ($j=0; $j < sizeof($fetch); $j++) {

					// echo var_dump($fetch[$j]);die;

					$checkExistSql = "select sekolah_id from rekap_sekolah where sekolah_id = '".$fetch[$j]['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'";

					$dataExist = getDataByQuery($checkExistSql, 'rekap');

					if(sizeof($dataExist) > 0){
						$set = "";

						foreach ($fetch[$j] as $key => $value) {

							if($key != 'sekolah_id'){

								if($value != null){
									$set .= ", ".$key." = '".trim(str_replace("'", "''", $value))."'";
								}else{
									$set .= ", ".$key." = null";
								}

							}
						}

						$set = substr($set, 1, strlen($set));

						$sql_insert = "Update rekap_sekolah set ".$set." where sekolah_id = '".$fetch[$j]['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

						// echo $br.$sql_insert;continue;

						$result = execQuery($sql_insert);

						if($result['success']){
							echo $br."[".date('H:i:s')."] |  |  |  |__ [".$j."/".sizeof($fetch)."] [UPDATE] BERHASIL merekap data statistik sekolah ".$fetch[$j]['sekolah_id'];
						}else{
							echo $br."[".date('H:i:s')."] |  |  |  |__ [".$j."/".sizeof($fetch)."] [UPDATE] GAGAL merekap data statistik sekolah ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
						}
					}else{
						// do nothing. let the record is added by another cron job		
					}
				}
			}
		}
	}
}

?>