@echo OFF
php -f rekap_data_pokok_sekolah.php %*

php -f rekap_sekolah.php %*
php -f rekap_sekolah_akreditasi.php %*
php -f rekap_sekolah_rombel.php %*

php -f rekap_pd_tingkat.php %*
php -f rekap_pd_agama.php %*
php -f rekap_pd_ap.php %*
php -f rekap_pd_am.php %*
php -f rekap_pd_lulus.php %*
php -f rekap_pd_do.php %*

php -f rekap_ptk_kualifikasi.php %*

php -f rekap_pengiriman.php %*

php -f rekap_statistik_sekolah.php %*
