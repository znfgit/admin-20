<?php

$limit_var = 100;

include "startup.php";

$semesterCriteria = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $semesterCriteria->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, ".$semester->getTahunAjaranId().");
$semesterCriteria->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($semesterCriteria);

foreach ($semesters as $semester) {

	$z = new Criteria();

	echo $br."[".date('H:i:s')."] Mengambil data propinsi: OK";

	$z->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
	$z->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 1);

	$propinsis = Admin\Model\MstWilayahPeer::doSelect($z);
	$propinsiCount = Admin\Model\MstWilayahPeer::doCount($z);

	$data_proses_propinsi = 0;

	foreach ($propinsis as $propinsi) {

		$data_proses_propinsi++;

		echo $br."[".date('H:i:s')."] |__ [".$data_proses_propinsi."/".$propinsiCount."] Mengambil kabupaten dari propinsi: ".$propinsi->getNama()." - ".trim($propinsi->getKodeWilayah());

		$y = new Criteria();

		$y->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
		$y->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 2);
		$y->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($propinsi->getKodeWilayah()));

		$kabupatenCount = Admin\Model\MstWilayahPeer::doCount($y);
		$kabupatens = Admin\Model\MstWilayahPeer::doSelect($y);

		$data_proses_kabupaten = 0;

		foreach ($kabupatens as $kabupaten) {
			$data_proses_kabupaten++;

			echo $br."[".date('H:i:s')."] |  |__ [".$data_proses_kabupaten."/".$kabupatenCount."] Mengambil kecamatan dari kabupaten: ".$kabupaten->getNama()." - ".trim($kabupaten->getKodeWilayah());

			$x = new Criteria();

			$x->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
			$x->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 3);
			$x->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($kabupaten->getKodeWilayah()));

			$kecamatanCount = Admin\Model\MstWilayahPeer::doCount($x);
			$kecamatans = Admin\Model\MstWilayahPeer::doSelect($x);

			$data_proses_kecamatan = 0;

			foreach ($kecamatans as $kecamatan) {
				$data_proses_kecamatan++;

				echo $br."[".date('H:i:s')."] |  |  |__ [".$data_proses_kecamatan."/".$kecamatanCount."] Mengambil data sekolah dari kecamatan: ".$kecamatan->getNama()." - ".trim($kecamatan->getKodeWilayah());

								$sql_count = "SELECT
								sum(1) as total
							FROM
								sekolah WITH (nolock)
							JOIN ref.mst_wilayah kec WITH (nolock) on kec.kode_wilayah = left(sekolah.kode_wilayah,6)
							JOIN ref.mst_wilayah kab WITH (nolock) on kab.kode_wilayah = kec.mst_kode_wilayah
							JOIN ref.mst_wilayah prop WITH (nolock) on prop.kode_wilayah = kab.mst_kode_wilayah
							where kec.kode_wilayah = '".$kecamatan->getKodeWilayah()."'";

				$fetch_count = getDataByQuery($sql_count);

				$recordCountX = $fetch_count[0]['total'];

				echo $br."[".date('H:i:s')."] |  |  |  |__ Jumlah Record: ".$recordCountX;

				$total = $recordCountX; 			

				$data_proses = 0;
			
				for ($i=0; $i < $recordCountX; $i++) {
					if($i % $limit_var == 0){
						echo $br."[".date('H:i:s')."] |  |  |  |__ Mengambil data kualitas sekolah dari record ".($i+1)." sampai ".($i+$limit_var).": OK";

						$sql = "SELECT
									newid() as rekap_sekolah_id,
									s.sekolah_id,
									s.nama,
									s.npsn,
									s.bentuk_pendidikan_id,
									s.status_sekolah,
									s.soft_delete,
									'".$semester->getSemesterId()."' as semester_id,
									'".$semester->getTahunAjaranId()."' as tahun_ajaran_id,
									-- getdate() as tanggal,
									kec.nama as kecamatan,
									kab.nama as kabupaten,
									prop.nama as propinsi,
									kec.kode_wilayah as kode_wilayah_kecamatan,
									kab.kode_wilayah as kode_wilayah_kabupaten,
									prop.kode_wilayah as kode_wilayah_propinsi,
									kec.id_level_wilayah as id_level_wilayah_kecamatan,
									kab.id_level_wilayah as id_level_wilayah_kabupaten,
									prop.id_level_wilayah as id_level_wilayah_propinsi,
									kec.mst_kode_wilayah as mst_kode_wilayah_kecamatan,
									kab.mst_kode_wilayah as mst_kode_wilayah_kabupaten,
									prop.mst_kode_wilayah as mst_kode_wilayah_propinsi,
									(SELECT TOP 1
										ak.akreditasi_id
									FROM
										akreditasi_sp WITH (nolock)
									JOIN ref.akreditasi ak WITH (nolock) ON ak.akreditasi_id = akreditasi_sp.akreditasi_id
									WHERE
										akreditasi_sp.sekolah_id = s.sekolah_id
									AND akreditasi_sp.soft_delete = 0
									-- AND (
									--	akreditasi_sp.akred_sp_tst IS NULL
									--	OR akreditasi_sp.akred_sp_tst > getdate()
									--)
									ORDER BY akred_sp_tmt desc) as akreditasi_id,
									(SELECT TOP 1
										ak.nama
									FROM
										akreditasi_sp WITH (nolock)
									JOIN ref.akreditasi ak WITH (nolock) ON ak.akreditasi_id = akreditasi_sp.akreditasi_id
									WHERE
										akreditasi_sp.sekolah_id = s.sekolah_id
									AND akreditasi_sp.soft_delete = 0
									-- AND (
									--	akreditasi_sp.akred_sp_tst IS NULL
									--	OR akreditasi_sp.akred_sp_tst > getdate()
									--)
									ORDER BY akred_sp_tmt desc) as akreditasi_id_str,
									(SELECT TOP 1
										akreditasi_sp.akred_sp_tmt
									FROM
										akreditasi_sp WITH (nolock)
									JOIN ref.akreditasi ak WITH (nolock) ON ak.akreditasi_id = akreditasi_sp.akreditasi_id
									WHERE
										akreditasi_sp.sekolah_id = s.sekolah_id
									AND akreditasi_sp.soft_delete = 0
									-- AND (
									--	akreditasi_sp.akred_sp_tst IS NULL
									--	OR akreditasi_sp.akred_sp_tst > getdate()
									--)
									ORDER BY akred_sp_tmt desc) as tmt_akreditasi,
									(SELECT TOP 1
										akreditasi_sp.akred_sp_tst
									FROM
										akreditasi_sp WITH (nolock)
									JOIN ref.akreditasi ak WITH (nolock) ON ak.akreditasi_id = akreditasi_sp.akreditasi_id
									WHERE
										akreditasi_sp.sekolah_id = s.sekolah_id
									AND akreditasi_sp.soft_delete = 0
									-- AND (
									--	akreditasi_sp.akred_sp_tst IS NULL
									--	OR akreditasi_sp.akred_sp_tst > getdate()
									--)
									ORDER BY akred_sp_tmt desc) as tst_akreditasi,
									(SELECT TOP 1
										akreditasi_sp.akred_sp_sk
									FROM
										akreditasi_sp WITH (nolock)
									JOIN ref.akreditasi ak WITH (nolock) ON ak.akreditasi_id = akreditasi_sp.akreditasi_id
									WHERE
										akreditasi_sp.sekolah_id = s.sekolah_id
									AND akreditasi_sp.soft_delete = 0
									-- AND (
									--	akreditasi_sp.akred_sp_tst IS NULL
									--	OR akreditasi_sp.akred_sp_tst > getdate()
									--)
									ORDER BY akred_sp_tmt desc) as sk_akreditasi,
									(
									SELECT
										slong.waktu_penyelenggaraan_id AS waktu_penyelenggaraan_id
									FROM
										sekolah_longitudinal slong WITH (nolock)
									JOIN ref.waktu_penyelenggaraan waktu WITH (nolock) ON waktu.waktu_penyelenggaraan_id = slong.waktu_penyelenggaraan_id
									WHERE
										slong.sekolah_id = s.sekolah_id
									AND slong.soft_delete = 0
									AND slong.semester_id = '".$semester->getSemesterId()."'
									) as waktu_penyelenggaraan_id,
									(
									SELECT
										waktu.nama AS waktu_penyelenggaraan_id_str
									FROM
										sekolah_longitudinal slong WITH (nolock)
									JOIN ref.waktu_penyelenggaraan waktu WITH (nolock) ON waktu.waktu_penyelenggaraan_id = slong.waktu_penyelenggaraan_id
									WHERE
										slong.sekolah_id = s.sekolah_id
									AND slong.soft_delete = 0
									AND slong.semester_id = '".$semester->getSemesterId()."'
									) as waktu_penyelenggaraan_id_str,
									(
									SELECT
										listrik.sumber_listrik_id AS sumber_listrik_id
									FROM
										sekolah_longitudinal slong WITH (nolock)
									JOIN ref.sumber_listrik listrik WITH (nolock) ON listrik.sumber_listrik_id = slong.sumber_listrik_id
									WHERE
										slong.sekolah_id = s.sekolah_id
									AND slong.soft_delete = 0
									AND slong.semester_id = '".$semester->getSemesterId()."'
									) as sumber_listrik_id,
									(
									SELECT
										listrik.nama AS sumber_listrik_id_str
									FROM
										sekolah_longitudinal slong WITH (nolock)
									JOIN ref.sumber_listrik listrik WITH (nolock) ON listrik.sumber_listrik_id = slong.sumber_listrik_id
									WHERE
										slong.sekolah_id = s.sekolah_id
									AND slong.soft_delete = 0
									AND slong.semester_id = '".$semester->getSemesterId()."'
									) as sumber_listrik_id_str,
									(
									SELECT
										internet.akses_internet_id AS akses_internet_id
									FROM
										sekolah_longitudinal slong WITH (nolock)
									JOIN ref.akses_internet internet WITH (nolock) ON internet.akses_internet_id = slong.akses_internet_id
									WHERE
										slong.sekolah_id = s.sekolah_id
									AND slong.soft_delete = 0
									AND slong.semester_id = '".$semester->getSemesterId()."'
									) as akses_internet_id,
									(
									SELECT
										internet.nama AS akses_internet_id
									FROM
										sekolah_longitudinal slong WITH (nolock)
									JOIN ref.akses_internet internet WITH (nolock) ON internet.akses_internet_id = slong.akses_internet_id
									WHERE
										slong.sekolah_id = s.sekolah_id
									AND slong.soft_delete = 0
									AND slong.semester_id = '".$semester->getSemesterId()."'
									) as akses_internet_id_str
								FROM
									sekolah s WITH (nolock)
								JOIN ref.mst_wilayah kec WITH (nolock) ON kec.kode_wilayah = LEFT(s.kode_wilayah, 6)
								JOIN ref.mst_wilayah kab WITH (nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
								JOIN ref.mst_wilayah prop WITH (nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
								where s.soft_delete = 0
								and kec.kode_wilayah = '".$kecamatan->getKodeWilayah()."'
								ORDER BY
									s.nama OFFSET ".$i." ROWS FETCH NEXT ".$limit_var." ROWS ONLY";

						$fetch = getDataBySql($sql);

						$count = 1;
						$objCount = 1000;

						for ($j=0; $j < sizeof($fetch); $j++) {

							$data_proses++;

							$arr = $fetch[$j];

							$checkExistSql = "select sekolah_id from rekap_sekolah where sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

							$dataExist = getDataByQuery($checkExistSql, 'rekap');

							if(sizeof($dataExist) > 0){
								//update

								$set = "";

								foreach ($fetch[$j] as $key => $value) {

									if($key != 'sekolah_id' && $key != 'rekap_sekolah_id'){

										if($value != null){
											$set .= ", ".$key." = '".str_replace("'", "''", trim($value))."'";
										}else{
											$set .= ", ".$key." = null";
										}

									}
								}

								$set = substr($set, 1, strlen($set));

								$sql_insert = "Update rekap_sekolah set ".$set." where sekolah_id = '".$fetch[$j]['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

								// echo $br.$sql_insert;continue;

								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap data akreditasi sekolah [".$fetch[$j]['akreditasi_id']."] ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [UPDATE] GAGAL merekap data akreditasi sekolah ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}

							}else{
								$str_key = "";
								$str_value = "";
								//insert
								foreach ($fetch[$j] as $key => $value) {
									$str_key .= ",".$key;

									if($value != null){
										$str_value .= ",'".trim($value)."'";
									}else{
										$str_value .= ",null";
									}

								}

								$str_key = substr($str_key, 1, strlen($str_key));
								$str_value = substr($str_value, 1, strlen($str_value));

								$sql_insert = "insert into rekap_sekolah(".$str_key.") values(".$str_value.")";
					
								// echo $br."[".date('H:i:s')."] ".$sql_insert;
								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [INSERT] BERHASIL merekap data akreditasi sekolah ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [INSERT] GAGAL merekap data akreditasi sekolah ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}
							}

						}
					}
				}

			}
		}
	}
}
?>