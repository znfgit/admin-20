<?php

$limit_var = 100;

include "startup.php";

$semesterCriteria = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $semesterCriteria->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, ".$semester->getTahunAjaranId().");
$semesterCriteria->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($semesterCriteria);

foreach ($semesters as $semester) {

	$z = new Criteria();

	echo $br."[".date('H:i:s')."] Mengambil data propinsi: OK";

	$z->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
	$z->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 1);
	$z->addDescendingOrderByColumn(Admin\Model\MstWilayahPeer::KODE_WILAYAH);

	$propinsis = Admin\Model\MstWilayahPeer::doSelect($z);
	$propinsiCount = Admin\Model\MstWilayahPeer::doCount($z);

	$data_proses_propinsi = 0;

	foreach ($propinsis as $propinsi) {

		$data_proses_propinsi++;

		echo $br."[".date('H:i:s')."] |__ [".$data_proses_propinsi."/".$propinsiCount."] Mengambil kabupaten dari propinsi: ".$propinsi->getNama()." - ".trim($propinsi->getKodeWilayah());

		$y = new Criteria();

		$y->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
		$y->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 2);
		$y->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($propinsi->getKodeWilayah()));

		$kabupatenCount = Admin\Model\MstWilayahPeer::doCount($y);
		$kabupatens = Admin\Model\MstWilayahPeer::doSelect($y);

		$data_proses_kabupaten = 0;

		foreach ($kabupatens as $kabupaten) {
			$data_proses_kabupaten++;

			echo $br."[".date('H:i:s')."] |  |__ [".$data_proses_kabupaten."/".$kabupatenCount."] Mengambil data sekolah dari kabupaten: ".$kabupaten->getNama()." - ".trim($kabupaten->getKodeWilayah());

			// $x = new Criteria();

			// $x->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
			// $x->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 3);
			// $x->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($kabupaten->getKodeWilayah()));

			// $kecamatanCount = Admin\Model\MstWilayahPeer::doCount($x);
			// $kecamatans = Admin\Model\MstWilayahPeer::doSelect($x);

			// $data_proses_kecamatan = 0;

			// foreach ($kecamatans as $kecamatan) {
			// 	$data_proses_kecamatan++;

			// 	echo $br."[".date('H:i:s')."] |  |  |__ [".$data_proses_kecamatan."/".$kecamatanCount."] Mengambil data sekolah dari kecamatan: ".$kecamatan->getNama()." - ".trim($kecamatan->getKodeWilayah());

				$sql_count = "SELECT
									count(distinct(s.sekolah_id)) as total
								FROM
									peserta_didik pd WITH (nolock)
								JOIN (
									SELECT
										ar.peserta_didik_id,
										max(rb.tingkat_pendidikan_id) as tingkat_pendidikan_id
										FROM
											anggota_rombel ar WITH (nolock)
										JOIN rombongan_belajar rb WITH (nolock) ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
									WHERE
										ar.soft_delete = 0
									AND rb.soft_delete = 0
									AND rb.jenis_rombel = 1
									AND rb.semester_id IN (".$semester->getTahunAjaranId()."2, ".$semester->getTahunAjaranId()."1)
									GROUP BY
										ar.peserta_didik_id
								) arb ON arb.peserta_didik_id = pd.peserta_didik_id
								JOIN registrasi_peserta_didik rpd WITH (nolock) ON pd.peserta_didik_id = rpd.peserta_didik_id
								JOIN sekolah s on s.sekolah_id = rpd.sekolah_id
								JOIN ref.mst_wilayah kec WITH (nolock) ON kec.kode_wilayah = LEFT(s.kode_wilayah, 6)
								JOIN ref.mst_wilayah kab WITH (nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
								JOIN ref.mst_wilayah prop WITH (nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
								WHERE
									rpd.Soft_delete = 0
								AND pd.Soft_delete = 0
								AND (
									rpd.jenis_keluar_id IS NULL
									OR rpd.jenis_keluar_id = '1'
								)
								and kab.kode_wilayah = '".$kabupaten->getKodeWilayah()."'
								and s.soft_delete = 0";

				$fetch_count = getDataByQuery($sql_count);

				$recordCountX = $fetch_count[0]['total'];

				echo $br."[".date('H:i:s')."] |  |  |  |__ Jumlah Record: ".$recordCountX;

				$total = $recordCountX; 			

				$data_proses = 0;
			
				for ($i=0; $i < $recordCountX; $i++) {
					if($i % $limit_var == 0){
						echo $br."[".date('H:i:s')."] |  |  |  |__ Mengambil data kualitas sekolah dari record ".($i+1)." sampai ".($i+$limit_var).": OK";

						$sql = "SELECT
									s.sekolah_id as sekolah_id,
									SUM (1) as pd,
									sum (case when pd.jenis_kelamin = 'L' then 1 else 0 end) as pd_laki,
									sum (case when pd.jenis_kelamin = 'P' then 1 else 0 end) as pd_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 1 then 1 else 0 end)as pd_kelas_1,
									sum (case when arb.tingkat_pendidikan_id = 1 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_1_laki,
									sum (case when arb.tingkat_pendidikan_id = 1 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_1_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 2 then 1 else 0 end)as pd_kelas_2,
									sum (case when arb.tingkat_pendidikan_id = 2 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_2_laki,
									sum (case when arb.tingkat_pendidikan_id = 2 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_2_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 3 then 1 else 0 end)as pd_kelas_3,
									sum (case when arb.tingkat_pendidikan_id = 3 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_3_laki,
									sum (case when arb.tingkat_pendidikan_id = 3 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_3_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 4 then 1 else 0 end)as pd_kelas_4,
									sum (case when arb.tingkat_pendidikan_id = 4 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_4_laki,
									sum (case when arb.tingkat_pendidikan_id = 4 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_4_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 5 then 1 else 0 end)as pd_kelas_5,
									sum (case when arb.tingkat_pendidikan_id = 5 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_5_laki,
									sum (case when arb.tingkat_pendidikan_id = 5 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_5_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 6 then 1 else 0 end)as pd_kelas_6,
									sum (case when arb.tingkat_pendidikan_id = 6 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_6_laki,
									sum (case when arb.tingkat_pendidikan_id = 6 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_6_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 7 then 1 else 0 end)as pd_kelas_7,
									sum (case when arb.tingkat_pendidikan_id = 7 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_7_laki,
									sum (case when arb.tingkat_pendidikan_id = 7 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_7_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 8 then 1 else 0 end)as pd_kelas_8,
									sum (case when arb.tingkat_pendidikan_id = 8 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_8_laki,
									sum (case when arb.tingkat_pendidikan_id = 8 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_8_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 9 then 1 else 0 end)as pd_kelas_9,
									sum (case when arb.tingkat_pendidikan_id = 9 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_9_laki,
									sum (case when arb.tingkat_pendidikan_id = 9 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_9_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 10 then 1 else 0 end)as pd_kelas_10,
									sum (case when arb.tingkat_pendidikan_id = 10 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_10_laki,
									sum (case when arb.tingkat_pendidikan_id = 10 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_10_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 11 then 1 else 0 end)as pd_kelas_11,
									sum (case when arb.tingkat_pendidikan_id = 11 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_11_laki,
									sum (case when arb.tingkat_pendidikan_id = 11 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_11_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 12 then 1 else 0 end)as pd_kelas_12,
									sum (case when arb.tingkat_pendidikan_id = 12 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_12_laki,
									sum (case when arb.tingkat_pendidikan_id = 12 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_12_perempuan,
									sum (case when arb.tingkat_pendidikan_id = 13 then 1 else 0 end)as pd_kelas_13,
									sum (case when arb.tingkat_pendidikan_id = 13 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_13_laki,
									sum (case when arb.tingkat_pendidikan_id = 13 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_13_perempuan,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 0 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_0_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 0 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_0_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 1 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_1_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 1 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_1_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 2 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_2_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 2 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_2_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 3 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_3_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 3 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_3_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 4 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_4_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 4 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_4_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 5 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_5_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 5 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_5_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 6 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_6_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 6 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_6_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 7 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_7_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 7 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_7_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 8 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_8_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 8 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_8_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 9 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_9_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 9 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_9_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 10 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_10_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 10 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_10_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 11 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_11_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 11 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_11_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 12 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_12_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 12 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_12_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 13 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_13_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 13 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_13_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 14 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_14_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 14 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_14_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 15 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_15_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 15 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_15_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 16 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_16_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 16 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_16_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 17 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_17_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 17 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_17_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 18 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_18_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 18 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_18_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 19 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_19_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 19 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_19_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 20 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_20_tahun,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 20 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_20_tahun,												
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 20 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_20_tahun_lebih,
									sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 20 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_20_tahun_lebih,
								
									sum (case when pd.jenis_kelamin = 'L' and pd.penerima_KIP = 1 then 1 else 0 end) as pd_penerima_kip_laki,
									sum (case when pd.jenis_kelamin = 'P' and pd.penerima_KIP = 1 then 1 else 0 end) as pd_penerima_kip_perempuan,
									sum (case when pd.jenis_kelamin = 'L' and pd.layak_PIP = 1 then 1 else 0 end) as pd_layak_pip_laki,
									sum (case when pd.jenis_kelamin = 'P' and pd.layak_PIP = 1 then 1 else 0 end) as pd_layak_pip_perempuan
								FROM
									peserta_didik pd WITH (nolock)
								JOIN (
									SELECT
										ar.peserta_didik_id,
										max(rb.tingkat_pendidikan_id) as tingkat_pendidikan_id
										FROM
											anggota_rombel ar WITH (nolock)
										JOIN rombongan_belajar rb WITH (nolock) ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
									WHERE
										ar.soft_delete = 0
									AND rb.soft_delete = 0
									AND rb.jenis_rombel = 1
									AND rb.semester_id IN (".$semester->getTahunAjaranId()."2, ".$semester->getTahunAjaranId()."1)
									GROUP BY
										ar.peserta_didik_id
								) arb ON arb.peserta_didik_id = pd.peserta_didik_id
								JOIN registrasi_peserta_didik rpd WITH (nolock) ON pd.peserta_didik_id = rpd.peserta_didik_id
								JOIN sekolah s on s.sekolah_id = rpd.sekolah_id
								JOIN ref.mst_wilayah kec WITH (nolock) ON kec.kode_wilayah = LEFT(s.kode_wilayah, 6)
								JOIN ref.mst_wilayah kab WITH (nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
								JOIN ref.mst_wilayah prop WITH (nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
								WHERE
									rpd.Soft_delete = 0
								AND pd.Soft_delete = 0
								AND (
									rpd.jenis_keluar_id IS NULL
									OR rpd.jenis_keluar_id = '1'
								)
								and kab.kode_wilayah = '".$kabupaten->getKodeWilayah()."'
								and s.soft_delete = 0
								group by s.sekolah_id
								ORDER BY
									s.sekolah_id OFFSET ".$i." ROWS FETCH NEXT ".$limit_var." ROWS ONLY";

						$fetch = getDataBySql($sql);

						$count = 1;
						$objCount = 1000;

						for ($j=0; $j < sizeof($fetch); $j++) {

							$data_proses++;

							$arr = $fetch[$j];

							// //ambil data peserta didik
							// try {
								
							// 	$sql_pd = "SELECT
							// 					SUM (1) as pd,
							// 					sum (case when pd.jenis_kelamin = 'L' then 1 else 0 end) as pd_laki,
							// 					sum (case when pd.jenis_kelamin = 'P' then 1 else 0 end) as pd_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 1 then 1 else 0 end)as pd_kelas_1,
							// 					sum (case when arb.tingkat_pendidikan_id = 1 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_1_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 1 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_1_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 2 then 1 else 0 end)as pd_kelas_2,
							// 					sum (case when arb.tingkat_pendidikan_id = 2 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_2_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 2 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_2_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 3 then 1 else 0 end)as pd_kelas_3,
							// 					sum (case when arb.tingkat_pendidikan_id = 3 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_3_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 3 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_3_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 4 then 1 else 0 end)as pd_kelas_4,
							// 					sum (case when arb.tingkat_pendidikan_id = 4 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_4_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 4 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_4_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 5 then 1 else 0 end)as pd_kelas_5,
							// 					sum (case when arb.tingkat_pendidikan_id = 5 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_5_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 5 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_5_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 6 then 1 else 0 end)as pd_kelas_6,
							// 					sum (case when arb.tingkat_pendidikan_id = 6 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_6_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 6 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_6_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 7 then 1 else 0 end)as pd_kelas_7,
							// 					sum (case when arb.tingkat_pendidikan_id = 7 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_7_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 7 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_7_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 8 then 1 else 0 end)as pd_kelas_8,
							// 					sum (case when arb.tingkat_pendidikan_id = 8 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_8_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 8 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_8_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 9 then 1 else 0 end)as pd_kelas_9,
							// 					sum (case when arb.tingkat_pendidikan_id = 9 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_9_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 9 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_9_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 10 then 1 else 0 end)as pd_kelas_10,
							// 					sum (case when arb.tingkat_pendidikan_id = 10 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_10_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 10 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_10_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 11 then 1 else 0 end)as pd_kelas_11,
							// 					sum (case when arb.tingkat_pendidikan_id = 11 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_11_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 11 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_11_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 12 then 1 else 0 end)as pd_kelas_12,
							// 					sum (case when arb.tingkat_pendidikan_id = 12 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_12_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 12 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_12_perempuan,
							// 					sum (case when arb.tingkat_pendidikan_id = 13 then 1 else 0 end)as pd_kelas_13,
							// 					sum (case when arb.tingkat_pendidikan_id = 13 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_kelas_13_laki,
							// 					sum (case when arb.tingkat_pendidikan_id = 13 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_kelas_13_perempuan,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 0 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_0_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 0 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_0_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 1 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_1_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 1 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_1_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 2 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_2_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 2 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_2_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 3 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_3_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 3 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_3_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 4 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_4_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 4 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_4_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 5 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_5_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 5 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_5_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 6 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_6_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 6 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_6_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 7 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_7_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 7 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_7_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 8 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_8_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 8 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_8_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 9 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_9_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 9 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_9_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 10 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_10_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 10 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_10_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 11 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_11_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 11 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_11_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 12 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_12_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 12 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_12_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 13 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_13_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 13 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_13_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 14 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_14_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 14 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_14_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 15 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_15_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 15 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_15_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 16 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_16_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 16 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_16_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 17 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_17_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 17 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_17_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 18 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_18_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 18 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_18_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 19 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_19_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 19 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_19_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 20 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_20_tahun,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) = 20 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_20_tahun,												
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 20 and pd.jenis_kelamin = 'L' then 1 else 0 end)as pd_laki_20_tahun_lebih,
							// 					sum (case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 20 and pd.jenis_kelamin = 'P' then 1 else 0 end)as pd_perempuan_20_tahun_lebih
							// 				FROM
							// 					peserta_didik pd WITH (nolock)
							// 				JOIN (
							// 					SELECT
							// 						ar.peserta_didik_id,
							// 						max(rb.tingkat_pendidikan_id) as tingkat_pendidikan_id
							// 						FROM
							// 							anggota_rombel ar WITH (nolock)
							// 						JOIN rombongan_belajar rb WITH (nolock) ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
							// 					WHERE
							// 						ar.soft_delete = 0
							// 					AND rb.soft_delete = 0
							// 					AND rb.jenis_rombel = 1
							// 					AND rb.semester_id IN (20162, 20161)
							// 					GROUP BY
							// 						ar.peserta_didik_id
							// 				) arb ON arb.peserta_didik_id = pd.peserta_didik_id
							// 				JOIN registrasi_peserta_didik rpd WITH (nolock) ON pd.peserta_didik_id = rpd.peserta_didik_id
							// 				WHERE
							// 					rpd.Soft_delete = 0
							// 				AND pd.Soft_delete = 0
							// 				AND (
							// 					rpd.jenis_keluar_id IS NULL
							// 					OR rpd.jenis_keluar_id = '1'
							// 				)
							// 				AND rpd.sekolah_id = '".$arr['sekolah_id']."'";

							// 	// echo $sql_pd;die;

							// 	$fetch_pd = getDataByQuery($sql_pd);

							// 	if($fetch_pd){
							// 		foreach ($fetch_pd[0] as $key => $value) {
							// 			if($value == 0){
							// 				$value = '0';
							// 			}

							// 			$fetch[$j][$key] = $value;
							// 		}
							// 	}
							// } catch (Exception $e) {
										
							// }


							// // print_r($arr);die;

							// //end of ambil data tingkat peserta didik

							$checkExistSql = "select sekolah_id from rekap_sekolah where sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

							$dataExist = getDataByQuery($checkExistSql, 'rekap');

							if(sizeof($dataExist) > 0){
								//update

								$set = "";

								foreach ($fetch[$j] as $key => $value) {

									if($key != 'sekolah_id' && $key != 'rekap_sekolah_id'){

										if($value != null){
											$set .= ", ".$key." = '".str_replace("'", "''", trim($value))."'";
										}else{
											$set .= ", ".$key." = null";
										}

									}
								}

								$set = substr($set, 1, strlen($set));

								$sql_insert = "Update rekap_sekolah set ".$set." where sekolah_id = '".$fetch[$j]['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

								// echo $br.$sql_insert;continue;

								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap data PD ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [UPDATE] GAGAL merekap data PD ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}

							}else{
								$str_key = "";
								$str_value = "";
								//insert
								foreach ($fetch[$j] as $key => $value) {
									$str_key .= ",".$key;

									if($value != null){
										$str_value .= ",'".trim($value)."'";
									}else{
										$str_value .= ",null";
									}

								}

								$str_key = substr($str_key, 1, strlen($str_key));
								$str_value = substr($str_value, 1, strlen($str_value));

								$sql_insert = "insert into rekap_sekolah(".$str_key.") values(".$str_value.")";
					
								// echo $br."[".date('H:i:s')."] ".$sql_insert;
								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [INSERT] BERHASIL merekap data PD ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [INSERT] GAGAL merekap data PD ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}
							}

						}
					}
				}

			// }
		}
	}
}
?>