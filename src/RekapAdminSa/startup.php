<?php

date_default_timezone_set('Asia/Jakarta');

$start = new DateTime(date('Y-m-d H:i:s'));

$br = PHP_EOL."[INF] ";

require_once __DIR__."/../../vendor/propel/propel1/runtime/lib/Propel.php";
require_once __DIR__."/../../app/config.php";

require_once __DIR__."/../Admin/Model/map/SekolahTableMap.php";
require_once __DIR__."/../Admin/Model/om/BaseSekolah.php";
require_once __DIR__."/../Admin/Model/om/BaseSekolahPeer.php";
require_once __DIR__."/../Admin/Model/Sekolah.php";
require_once __DIR__."/../Admin/Model/SekolahPeer.php";

require_once __DIR__."/../Admin/Model/map/SemesterTableMap.php";
require_once __DIR__."/../Admin/Model/om/BaseSemester.php";
require_once __DIR__."/../Admin/Model/om/BaseSemesterPeer.php";
require_once __DIR__."/../Admin/Model/Semester.php";
require_once __DIR__."/../Admin/Model/SemesterPeer.php";

require_once __DIR__."/../Admin/Model/map/TahunAjaranTableMap.php";
require_once __DIR__."/../Admin/Model/om/BaseTahunAjaran.php";
require_once __DIR__."/../Admin/Model/om/BaseTahunAjaranPeer.php";
require_once __DIR__."/../Admin/Model/TahunAjaran.php";
require_once __DIR__."/../Admin/Model/TahunAjaranPeer.php";

require_once __DIR__."/../Admin/Model/map/MstWilayahTableMap.php";
require_once __DIR__."/../Admin/Model/om/BaseMstWilayah.php";
require_once __DIR__."/../Admin/Model/om/BaseMstWilayahPeer.php";
require_once __DIR__."/../Admin/Model/MstWilayah.php";
require_once __DIR__."/../Admin/Model/MstWilayahPeer.php";

function initdb($tipe = 'utama'){

	if($tipe == 'utama'){
		$serverName = DATABASEHOST; //serverName\instanceName
    	$connectionInfo = array( "Database"=>DATABASENAME, "UID"=>DATABASEUSER, "PWD"=>DATABASEPASSWORD,'ReturnDatesAsStrings'=>true);
	}else if($tipe == 'statistik'){
		$serverName = DATABASEHOST_STATISTIK; //serverName\instanceName
    	$connectionInfo = array( "Database"=>DATABASENAME_STATISTIK, "UID"=>DATABASEUSER_STATISTIK, "PWD"=>DATABASEPASSWORD_STATISTIK,'ReturnDatesAsStrings'=>true);
	}else{
		$serverName = DATABASEHOST_REKAP; //serverName\instanceName
    	$connectionInfo = array( "Database"=>DATABASENAME_REKAP, "UID"=>DATABASEUSER_REKAP, "PWD"=>DATABASEPASSWORD_REKAP,'ReturnDatesAsStrings'=>true);
	}

    $conn = sqlsrv_connect( $serverName, $connectionInfo);

    return $conn;
}

function getDataByQuery($sql, $tipe = 'utama'){
	$con = initdb($tipe);
	$return = array();

	$stmt = sqlsrv_query( $con, $sql );
	while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
		array_push($return, $row);
	}

	return $return;
}

function execQuery($sql, $tipe = 'rekap'){
	$con = initdb($tipe);
	$return = array();

	$stmt = sqlsrv_query( $con, $sql );

	if($stmt){
		$return['success'] = true;
	}else{
		$return['success'] = false;
	}

	return $return;
}

try {
	$con = initdb('utama');

	if($con){
		echo $br."[".date('H:i:s')."] Koneksi database dapodik: OK";
	}else{
		echo $br."[".date('H:i:s')."] Koneksi database dapodik: ERROR";
	}
} catch (Exception $e) {
	echo $br."[".date('H:i:s')."] Koneksi database dapodik: ERROR";	
}

try {
	$con = initdb('rekap');

	if($con){
		echo $br."[".date('H:i:s')."] Koneksi database rekap: OK";
	}else{
		echo $br."[".date('H:i:s')."] Koneksi database rekap: ERROR";
	}
} catch (Exception $e) {
	echo $br."[".date('H:i:s')."] Koneksi database rekap: ERROR";	
}

function getDataBySql($sql="", $remove_keys=FALSE, $dbName="Dapodik_Paudni") {
    
    $con = Propel::getConnection($dbName);
    $stmt = $con->prepare($sql); 
    $stmt->execute(); 
    if ($remove_keys) {
        $result = $stmt->setFetchMode(PDO::FETCH_NUM);      
        $res = array();
        while ($row = $stmt->fetch()) {
            $res[] = $row;
        }       
    } else {        
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);        
        $res = $stmt->fetchAll();       
    }
    
    return $res;
}


try {
	Propel::init(__DIR__."/../../app/config/conf/admin-conf.php");
	
	echo $br."[".date('H:i:s')."] Koneksi PROPEL: OK";
} catch (Exception $e) {
	die($e->getMessage());
}




?>