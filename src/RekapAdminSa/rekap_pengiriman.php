<?php

$limit_var = 100;

include "startup.php";

$semesterCriteria = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $semesterCriteria->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, ".$semester->getTahunAjaranId().");
$semesterCriteria->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($semesterCriteria);

foreach ($semesters as $semester) {

	$z = new Criteria();

	echo $br."[".date('H:i:s')."] Mengambil data propinsi: OK";

	$z->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
	$z->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 1);
	$z->addDescendingOrderByColumn(Admin\Model\MstWilayahPeer::KODE_WILAYAH);

	$propinsis = Admin\Model\MstWilayahPeer::doSelect($z);
	$propinsiCount = Admin\Model\MstWilayahPeer::doCount($z);

	$data_proses_propinsi = 0;

	foreach ($propinsis as $propinsi) {

		$data_proses_propinsi++;

		echo $br."[".date('H:i:s')."] |__ [".$data_proses_propinsi."/".$propinsiCount."] Mengambil kabupaten dari propinsi: ".$propinsi->getNama()." - ".trim($propinsi->getKodeWilayah());

		$y = new Criteria();

		$y->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
		$y->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 2);
		$y->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($propinsi->getKodeWilayah()));

		$kabupatenCount = Admin\Model\MstWilayahPeer::doCount($y);
		$kabupatens = Admin\Model\MstWilayahPeer::doSelect($y);

		$data_proses_kabupaten = 0;

		foreach ($kabupatens as $kabupaten) {
			$data_proses_kabupaten++;

			echo $br."[".date('H:i:s')."] |  |__ [".$data_proses_kabupaten."/".$kabupatenCount."] Mengambil kecamatan dari kabupaten: ".$kabupaten->getNama()." - ".trim($kabupaten->getKodeWilayah());

			$x = new Criteria();

			$x->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
			$x->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 3);
			$x->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($kabupaten->getKodeWilayah()));

			$kecamatanCount = Admin\Model\MstWilayahPeer::doCount($x);
			$kecamatans = Admin\Model\MstWilayahPeer::doSelect($x);

			$data_proses_kecamatan = 0;

			foreach ($kecamatans as $kecamatan) {
				$data_proses_kecamatan++;

				echo $br."[".date('H:i:s')."] |  |  |__ [".$data_proses_kecamatan."/".$kecamatanCount."] Mengambil data sekolah dari kecamatan: ".$kecamatan->getNama()." - ".trim($kecamatan->getKodeWilayah());

				$url = "http://dapo.dikdasmen.kemdikbud.go.id/rekap/progresSP?id_level_wilayah=3&kode_wilayah=".trim($kecamatan->getKodeWilayah())."&semester_id=".$semester->getSemesterId();

				$json = file_get_contents($url);
		
				$arrJson = json_decode($json);

				$data_proses = 0;

				foreach ($arrJson as $arr) {
					$data_proses++;

					echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".sizeof($arrJson)."] Rekap Pengiriman ".$arr->{'nama'};

					$checkExistSql = "select sekolah_id from rekap_pengiriman where sekolah_id = '".$arr->{'sekolah_id'}."' and semester_id = '".$semester->getSemesterId()."'"; 

					$dataExist = getDataByQuery($checkExistSql, 'rekap');

					if(sizeof($dataExist) > 0){
						
						$sql_insert = "Update rekap_pengiriman set 
											nama = '".$arr->{'nama'}."',
											npsn = '".$arr->{'npsn'}."',
											jumlah_kirim = '".$arr->{'jumlah_kirim'}."',
											sinkron_terakhir = '".$arr->{'sinkron_terakhir'}."',
											sekolah_id_enkrip = '".$arr->{'sekolah_id_enkrip'}."'
										where sekolah_id = '".$arr->{'sekolah_id'}."' and semester_id = ".$semester->getSemesterId();	

						// echo $sql_insert;	

						$method = "UPDATE";					

					}else{

						$sql_insert = "insert into rekap_pengiriman(
											rekap_pengiriman_id,
											sekolah_id,
											sekolah_id_enkrip,
											semester_id,
											nama,
											npsn,
											jumlah_kirim,
											sinkron_terakhir
										) values(
											newid(),
											'".$arr->{'sekolah_id'}."',
											'".$arr->{'sekolah_id_enkrip'}."',
											'".$semester->getSemesterId()."',
											'".$arr->{'nama'}."',
											'".$arr->{'npsn'}."',
											'".$arr->{'jumlah_kirim'}."',
											'".$arr->{'sinkron_terakhir'}."'
										)";

						$method = "INSERT";

					}

					$result = execQuery($sql_insert);

					if($result['success']){
						echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".sizeof($arrJson)."] [".$method."] BERHASIL merekap data sekolah ".$arr->{'sekolah_id'};
					}else{
						echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".sizeof($arrJson)."] [".$method."] GAGAL merekap data sekolah ".$arr->{'sekolah_id'}." Penyebab: ".print_r(sqlsrv_errors(), true);	
					}
					
				}

			}
		}
	}
}

?>