<?php

$limit_var = 100;

include "startup.php";

$semesterCriteria = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $semesterCriteria->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, ".$semester->getTahunAjaranId().");
$semesterCriteria->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($semesterCriteria);

foreach ($semesters as $semester) {

	$z = new Criteria();

	echo $br."[".date('H:i:s')."] Mengambil data propinsi: OK";

	$z->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
	$z->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 1);
	$z->addDescendingOrderByColumn(Admin\Model\MstWilayahPeer::KODE_WILAYAH);

	$propinsis = Admin\Model\MstWilayahPeer::doSelect($z);
	$propinsiCount = Admin\Model\MstWilayahPeer::doCount($z);

	$data_proses_propinsi = 0;

	foreach ($propinsis as $propinsi) {

		$data_proses_propinsi++;

		echo $br."[".date('H:i:s')."] |__ [".$data_proses_propinsi."/".$propinsiCount."] Mengambil kabupaten dari propinsi: ".$propinsi->getNama()." - ".trim($propinsi->getKodeWilayah());

		$y = new Criteria();

		$y->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
		$y->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 2);
		$y->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($propinsi->getKodeWilayah()));

		$kabupatenCount = Admin\Model\MstWilayahPeer::doCount($y);
		$kabupatens = Admin\Model\MstWilayahPeer::doSelect($y);

		$data_proses_kabupaten = 0;

		foreach ($kabupatens as $kabupaten) {
			$data_proses_kabupaten++;

			echo $br."[".date('H:i:s')."] |  |__ [".$data_proses_kabupaten."/".$kabupatenCount."] Mengambil data sekolah dari kabupaten: ".$kabupaten->getNama()." - ".trim($kabupaten->getKodeWilayah());

			// $x = new Criteria();

			// $x->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
			// $x->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 3);
			// $x->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($kabupaten->getKodeWilayah()));

			// $kecamatanCount = Admin\Model\MstWilayahPeer::doCount($x);
			// $kecamatans = Admin\Model\MstWilayahPeer::doSelect($x);

			// $data_proses_kecamatan = 0;

			// foreach ($kecamatans as $kecamatan) {
			// 	$data_proses_kecamatan++;

			// 	echo $br."[".date('H:i:s')."] |  |  |__ [".$data_proses_kecamatan."/".$kecamatanCount."] Mengambil data sekolah dari kecamatan: ".$kecamatan->getNama()." - ".trim($kecamatan->getKodeWilayah());

				$sql_count = "SELECT
									count(distinct(s.sekolah_id)) as total
								FROM
									peserta_didik pd WITH (nolock)
								JOIN (
									SELECT
										ar.peserta_didik_id,
										max(rb.tingkat_pendidikan_id) as tingkat_pendidikan_id
										FROM
											anggota_rombel ar WITH (nolock)
										JOIN rombongan_belajar rb WITH (nolock) ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
									WHERE
										ar.soft_delete = 0
									AND rb.soft_delete = 0
									AND rb.jenis_rombel = 1
									AND rb.semester_id IN (".$semester->getTahunAjaranId()."2, ".$semester->getTahunAjaranId()."1)
									GROUP BY
										ar.peserta_didik_id
								) arb ON arb.peserta_didik_id = pd.peserta_didik_id
								JOIN registrasi_peserta_didik rpd WITH (nolock) ON pd.peserta_didik_id = rpd.peserta_didik_id
								JOIN sekolah s on s.sekolah_id = rpd.sekolah_id
								JOIN ref.mst_wilayah kec WITH (nolock) ON kec.kode_wilayah = LEFT(s.kode_wilayah, 6)
								JOIN ref.mst_wilayah kab WITH (nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
								JOIN ref.mst_wilayah prop WITH (nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
								WHERE
									rpd.Soft_delete = 0
								AND pd.Soft_delete = 0
								AND (
									rpd.jenis_keluar_id = '3'
								)
								and kab.kode_wilayah = '".$kabupaten->getKodeWilayah()."'
								and s.soft_delete = 0";

				$fetch_count = getDataByQuery($sql_count);

				$recordCountX = $fetch_count[0]['total'];

				echo $br."[".date('H:i:s')."] |  |  |  |__ Jumlah Record: ".$recordCountX;

				$total = $recordCountX; 			

				$data_proses = 0;
			
				for ($i=0; $i < $recordCountX; $i++) {
					if($i % $limit_var == 0){
						echo $br."[".date('H:i:s')."] |  |  |  |__ Mengambil data pd DO sekolah dari record ".($i+1)." sampai ".($i+$limit_var).": OK";

						$sql = "SELECT
									s.sekolah_id as sekolah_id,
									SUM (case when tingkat_pendidikan_id = 1 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_1_laki,
									SUM (case when tingkat_pendidikan_id = 2 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_2_laki,
									SUM (case when tingkat_pendidikan_id = 3 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_3_laki,
									SUM (case when tingkat_pendidikan_id = 4 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_4_laki,
									SUM (case when tingkat_pendidikan_id = 5 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_5_laki,
									SUM (case when tingkat_pendidikan_id = 6 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_6_laki,
									SUM (case when tingkat_pendidikan_id = 7 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_7_laki,
									SUM (case when tingkat_pendidikan_id = 8 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_8_laki,
									SUM (case when tingkat_pendidikan_id = 9 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_9_laki,
									SUM (case when tingkat_pendidikan_id = 10 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_10_laki,
									SUM (case when tingkat_pendidikan_id = 11 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_11_laki,
									SUM (case when tingkat_pendidikan_id = 12 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_12_laki,
									SUM (case when tingkat_pendidikan_id = 13 and jenis_kelamin = 'L' then 1 else 0 end) AS pd_do_kelas_13_laki,
									SUM (case when tingkat_pendidikan_id = 1 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_1_perempuan,
									SUM (case when tingkat_pendidikan_id = 2 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_2_perempuan,
									SUM (case when tingkat_pendidikan_id = 3 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_3_perempuan,
									SUM (case when tingkat_pendidikan_id = 4 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_4_perempuan,
									SUM (case when tingkat_pendidikan_id = 5 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_5_perempuan,
									SUM (case when tingkat_pendidikan_id = 6 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_6_perempuan,
									SUM (case when tingkat_pendidikan_id = 7 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_7_perempuan,
									SUM (case when tingkat_pendidikan_id = 8 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_8_perempuan,
									SUM (case when tingkat_pendidikan_id = 9 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_9_perempuan,
									SUM (case when tingkat_pendidikan_id = 10 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_10_perempuan,
									SUM (case when tingkat_pendidikan_id = 11 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_11_perempuan,
									SUM (case when tingkat_pendidikan_id = 12 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_12_perempuan,
									SUM (case when tingkat_pendidikan_id = 13 and jenis_kelamin = 'P' then 1 else 0 end) AS pd_do_kelas_13_perempuan
								FROM
									peserta_didik pd WITH (nolock)
								JOIN (
									SELECT
										ar.peserta_didik_id,
										max(rb.tingkat_pendidikan_id) as tingkat_pendidikan_id
										FROM
											anggota_rombel ar WITH (nolock)
										JOIN rombongan_belajar rb WITH (nolock) ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
									WHERE
										ar.soft_delete = 0
									AND rb.soft_delete = 0
									AND rb.jenis_rombel = 1
									AND rb.semester_id IN (".$semester->getTahunAjaranId()."2, ".$semester->getTahunAjaranId()."1)
									GROUP BY
										ar.peserta_didik_id
								) arb ON arb.peserta_didik_id = pd.peserta_didik_id
								JOIN registrasi_peserta_didik rpd WITH (nolock) ON pd.peserta_didik_id = rpd.peserta_didik_id
								JOIN sekolah s on s.sekolah_id = rpd.sekolah_id
								JOIN ref.mst_wilayah kec WITH (nolock) ON kec.kode_wilayah = LEFT(s.kode_wilayah, 6)
								JOIN ref.mst_wilayah kab WITH (nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
								JOIN ref.mst_wilayah prop WITH (nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
								WHERE
									rpd.Soft_delete = 0
								AND pd.Soft_delete = 0
								AND (
									rpd.jenis_keluar_id = '3'
								)
								and kab.kode_wilayah = '".$kabupaten->getKodeWilayah()."'
								and s.soft_delete = 0
								group by s.sekolah_id
								ORDER BY
									s.sekolah_id OFFSET ".$i." ROWS FETCH NEXT ".$limit_var." ROWS ONLY";

						$fetch = getDataBySql($sql);

						$count = 1;
						$objCount = 1000;

						for ($j=0; $j < sizeof($fetch); $j++) {

							$data_proses++;

							$arr = $fetch[$j];

							// //ambil data peserta didik
							

							// //end of ambil data tingkat peserta didik

							$checkExistSql = "select sekolah_id from rekap_sekolah where sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

							$dataExist = getDataByQuery($checkExistSql, 'rekap');

							if(sizeof($dataExist) > 0){
								//update

								$set = "";

								foreach ($fetch[$j] as $key => $value) {

									if($key != 'sekolah_id' && $key != 'rekap_sekolah_id'){

										if($value != null){
											$set .= ", ".$key." = '".str_replace("'", "''", trim($value))."'";
										}else{
											$set .= ", ".$key." = null";
										}

									}
								}

								$set = substr($set, 1, strlen($set));

								$sql_insert = "Update rekap_sekolah set ".$set." where sekolah_id = '".$fetch[$j]['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

								// echo $br.$sql_insert;continue;

								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap data PD ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [UPDATE] GAGAL merekap data PD ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}

							}else{
								$str_key = "";
								$str_value = "";
								//insert
								foreach ($fetch[$j] as $key => $value) {
									$str_key .= ",".$key;

									if($value != null){
										$str_value .= ",'".trim($value)."'";
									}else{
										$str_value .= ",null";
									}

								}

								$str_key = substr($str_key, 1, strlen($str_key));
								$str_value = substr($str_value, 1, strlen($str_value));

								$sql_insert = "insert into rekap_sekolah(".$str_key.") values(".$str_value.")";
					
								// echo $br."[".date('H:i:s')."] ".$sql_insert;
								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [INSERT] BERHASIL merekap data PD ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__  [".$data_proses."/".$total."] [INSERT] GAGAL merekap data PD ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}
							}

						}
					}
				}

			// }
		}
	}
}
?>