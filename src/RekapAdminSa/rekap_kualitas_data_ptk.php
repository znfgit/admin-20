<?php

include "startup.php";

$z = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $z->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, 2016);
$z->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($z);

foreach ($semesters as $semester) {
	# code...
	echo $br."[".date('H:i:s')."] Mengambil data tabel semester: ".$semester->getNama();


	$sql_count = "select sum(1) as total FROM
						ptk ptk
					JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
					JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
					JOIN sekolah s on s.sekolah_id = ptkd.sekolah_id
					JOIN ref.mst_wilayah kec on kec.kode_wilayah = left(s.kode_wilayah,6)
					JOIN ref.mst_wilayah kab on kab.kode_wilayah = kec.mst_kode_wilayah
					JOIN ref.mst_wilayah prop on prop.kode_wilayah = kab.mst_kode_wilayah
					WHERE
					 	ptkd.Soft_delete = 0
					-- AND ptk.Soft_delete = 0
					AND s.Soft_delete = 0
					-- AND ptkd.ptk_induk = 1
					AND ptkd.tahun_ajaran_id = ".$semester->getTahunAjaranId()."
					AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
					AND (
						ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						OR ptkd.jenis_keluar_id IS NULL
					)";

	$fetch_count = getDataBySql($sql_count);

	$recordCountX = $fetch_count[0]['total'];

	echo $br."[".date('H:i:s')."] Jumlah Record: ".$recordCountX;

	$total = $recordCountX; 
		
	$data_proses = 0;

	// for ($i=0; $i < 1; $i++) { 
	for ($i=0; $i < $recordCountX; $i++) { 

		if($i % 10000 == 0){

			echo $br."[".date('H:i:s')."] Mengambil data kualitas PTK dari record ".($i+1)." sampai ".($i+10000).": OK";

			$sql = "SELECT
						newid() as rekap_kualitas_data_ptk_id,
						ptk.ptk_id,
						ptk.nama,
						ptkd.sekolah_id,
						'".$semester->getSemesterId()."' as semester_id,
						'".$semester->getTahunAjaranId()."' as tahun_ajaran_id,
						getdate() as tanggal,
						s.nama as nama_sekolah,
						s.npsn,
						s.bentuk_pendidikan_id,
						s.status_sekolah,
						kec.nama as kecamatan,
						kab.nama as kabupaten,
						prop.nama as propinsi,
						kec.kode_wilayah as kode_wilayah_kecamatan,
						kab.kode_wilayah as kode_wilayah_kabupaten,
						prop.kode_wilayah as kode_wilayah_propinsi,
						kec.mst_kode_wilayah as mst_kode_wilayah_kecamatan,
						kab.mst_kode_wilayah as mst_kode_wilayah_kabupaten,
						prop.mst_kode_wilayah as mst_kode_wilayah_propinsi,
						kec.id_level_wilayah as id_level_wilayah_kecamatan,
						kab.id_level_wilayah as id_level_wilayah_kabupaten,
						prop.id_level_wilayah as id_level_wilayah_propinsi,
						(case when ptk.status_kepegawaian_id in(1,2,3,10) then 
							(case when ptk.sk_cpns is null then 0 else 1 end)
							else
							1
							end) as sk_cpns_valid,
						(case when ptk.status_kepegawaian_id in(1,2,3,10) then 
							(case when ptk.tgl_cpns is null then 0 else 1 end)
							else
							1
							end) as tgl_cpns_valid,
						(case when ptk.status_kepegawaian_id in(1,2,3,10) then 
							(case when ptk.tmt_pns is null then 0 else 1 end)
							else
							1
							end) as tmt_pns_valid,
						(case when ptk.status_kepegawaian_id in(1,2,3,10) then 
							(case when ptk.pangkat_golongan_id is null then 0 else 1 end)
							else
							1
							end) as pangkat_golongan_id_valid,
						(case when ptk.status_perkawinan = 1 then 
							(case when ptk.nama_suami_istri is null then 0 else 1 end)
							else
							1
							end) as nama_suami_istri_valid,
						(case when ptk.status_perkawinan = 1 then 
							(case when ptk.pekerjaan_suami_istri is null then 0 else 1 end)
							else
							1
							end) as pekerjaan_suami_istri_valid,
						(case when ptk.sk_pengangkatan is null or ptk.sk_pengangkatan = '-' then 0 else 1 end) as sk_pengangkatan_valid,
						(case when ptk.tmt_pengangkatan is null then 0 else 1 end) as tmt_pengangkatan_valid,
						(case when ptk.nama_ibu_kandung is null or ptk.nama_ibu_kandung = '-' then 0 else 1 end) as nama_ibu_kandung_valid,
						(case when ptk.nik is null or ptk.nik = '-' then 0 else 1 end) as nik_valid,
						(case when ptk.nuptk is null or ptk.nuptk = '-' then 0 else 1 end) as nuptk_valid,
						(case when ptk.desa_kelurahan is null or ptk.desa_kelurahan = '-' then 0 else 1 end) as desa_kelurahan_valid,
						(case when ptk.no_hp is null or ptk.no_hp = '-' then 0 else 1 end) as no_hp_valid,
						(case when ptk.email is null or ptk.email = '-' then 0 else 1 end) as email_valid,
						(case when ptk.npwp is null or ptk.npwp = '-' then 0 else 1 end) as npwp_valid,
						ptk.soft_delete
					FROM
						ptk ptk
					JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
					JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
					JOIN sekolah s on s.sekolah_id = ptkd.sekolah_id
					JOIN ref.mst_wilayah kec on kec.kode_wilayah = left(s.kode_wilayah,6)
					JOIN ref.mst_wilayah kab on kab.kode_wilayah = kec.mst_kode_wilayah
					JOIN ref.mst_wilayah prop on prop.kode_wilayah = kab.mst_kode_wilayah
					WHERE
						ptkd.Soft_delete = 0
					-- AND ptk.Soft_delete = 0
					AND s.Soft_delete = 0
					-- AND ptkd.ptk_induk = 1
					AND ptkd.tahun_ajaran_id = ".$semester->getTahunAjaranId()."
					AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
					AND (
						ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						OR ptkd.jenis_keluar_id IS NULL
					)
					ORDER BY
						ptk.ptk_id OFFSET ".$i." ROWS FETCH NEXT 10000 ROWS ONLY";

			// echo $sql;die;			

			$fetch = getDataBySql($sql);

			$count = 1;
			$objCount = 10000;

			for ($j=0; $j < sizeof($fetch); $j++) {

				$data_proses++;

				$arr = $fetch[$j];

				$checkExistSql = "select ptk_id from rekap_kualitas_data_ptk where ptk_id = '".$arr['ptk_id']."' and sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

				// echo $checkExistSql;die;

				$dataExist = getDataByQuery($checkExistSql, 'rekap');

				if(sizeof($dataExist) > 0){
					//update

					$set = "";

					foreach ($arr as $key => $value) {

						if($key != 'sekolah_id'){

							if($value != null){
								$set .= ", ".$key." = '".trim(str_replace("'","''",$value))."'";
							}else{
								$set .= ", ".$key." = null";
							}

						}
					}

					$set = substr($set, 1, strlen($set));

					$sql_insert = "Update rekap_kualitas_data_ptk set ".$set." where ptk_id = '".$arr['ptk_id']."' and sekolah_id = '".$arr['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

					// echo $br.$sql_insert;continue;

					$result = execQuery($sql_insert);

					if($result['success']){
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap kualitas data PTK ".$arr['sekolah_id'];
					}else{
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [UPDATE] GAGAL merekap kualitas data PTK ".$arr['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
					}

				}else{
					$str_key = "";
					$str_value = "";
					//insert
					foreach ($arr as $key => $value) {
						$str_key .= ",".$key;

						if($value != null){
							$str_value .= ",'".trim(str_replace("'","''",$value))."'";
						}else{
							$str_value .= ",null";
						}

					}

					$str_key = substr($str_key, 1, strlen($str_key));
					$str_value = substr($str_value, 1, strlen($str_value));

					$sql_insert = "insert into rekap_kualitas_data_ptk(".$str_key.") values(".$str_value.")";
		
					// echo $br."[".date('H:i:s')."] ".$sql_insert;
					$result = execQuery($sql_insert);

					if($result['success']){
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [INSERT] BERHASIL merekap kualitas data PTK ".$arr['sekolah_id'];
					}else{
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [INSERT] GAGAL merekap kualitas data PTK ".$arr['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
					}
				}
			}

		}
	}
}



?>