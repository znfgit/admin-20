<?php

$limit_var = 100;

include "startup.php";

$semesterCriteria = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $semesterCriteria->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, ".$semester->getTahunAjaranId().");
$semesterCriteria->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($semesterCriteria);

foreach ($semesters as $semester) {

	$z = new Criteria();

	echo $br."[".date('H:i:s')."] Mengambil data propinsi: OK";

	$z->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
	$z->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 1);

	$propinsis = Admin\Model\MstWilayahPeer::doSelect($z);
	$propinsiCount = Admin\Model\MstWilayahPeer::doCount($z);

	$data_proses_propinsi = 0;

	foreach ($propinsis as $propinsi) {

		$data_proses_propinsi++;

		echo $br."[".date('H:i:s')."] |__ [".$data_proses_propinsi."/".$propinsiCount."] Mengambil kabupaten dari propinsi: ".$propinsi->getNama()." - ".trim($propinsi->getKodeWilayah());

		$y = new Criteria();

		$y->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
		$y->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 2);
		$y->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($propinsi->getKodeWilayah()));

		$kabupatenCount = Admin\Model\MstWilayahPeer::doCount($y);
		$kabupatens = Admin\Model\MstWilayahPeer::doSelect($y);

		$data_proses_kabupaten = 0;

		foreach ($kabupatens as $kabupaten) {
			$data_proses_kabupaten++;

			echo $br."[".date('H:i:s')."] |  |__ [".$data_proses_kabupaten."/".$kabupatenCount."] Mengambil kecamatan dari kabupaten: ".$kabupaten->getNama()." - ".trim($kabupaten->getKodeWilayah());

			$x = new Criteria();

			$x->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
			$x->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 3);
			$x->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($kabupaten->getKodeWilayah()));

			$kecamatanCount = Admin\Model\MstWilayahPeer::doCount($x);
			$kecamatans = Admin\Model\MstWilayahPeer::doSelect($x);

			$data_proses_kecamatan = 0;

			foreach ($kecamatans as $kecamatan) {
				$data_proses_kecamatan++;

				echo $br."[".date('H:i:s')."] |  |  |__ [".$data_proses_kecamatan."/".$kecamatanCount."] Mengambil data sekolah dari kecamatan: ".$kecamatan->getNama()." - ".trim($kecamatan->getKodeWilayah());

				$sql_count = "SELECT
									sum(1) as total
								FROM
									rekap_kualitas_data_sekolah_sum sekolah
								WHERE
									semester_id = ".$semester->getSemesterId()."
								and kode_wilayah_kecamatan = '".$kecamatan->getKodeWilayah()."'";

				// echo $sql_count;die;

				$fetch_count = getDataByQuery($sql_count,'rekap');

				$recordCountX = $fetch_count[0]['total'];

				echo $br."[".date('H:i:s')."] |  |  |  |__ Jumlah Record: ".$recordCountX;

				$total = $recordCountX; 			

				$data_proses = 0;
			
				for ($i=0; $i < $recordCountX; $i++) {
					if($i % $limit_var == 0){
				
						$sql = "SELECT
									datepart(mm, tanggal) AS bulan,
									*
								FROM
									rekap_kualitas_data_sekolah_sum sekolah
								WHERE
									semester_id = ".$semester->getSemesterId()."
								and kode_wilayah_kecamatan = '".$kecamatan->getKodeWilayah()."'
								ORDER BY
									sekolah.sekolah_id OFFSET ".$i." ROWS FETCH NEXT ".$limit_var." ROWS ONLY";

									// echo $sql;die;

						$fetch = getDataByQuery($sql,'rekap');

						for ($j=0; $j < sizeof($fetch); $j++) {

							$data_proses++;

							$arr = $fetch[$j];

							$checkExistSql = "select sekolah_id from rekap_kualitas_data_periodik where sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

							$dataExist = getDataByQuery($checkExistSql, 'rekap');

							if(sizeof($dataExist) > 0){

								$sql_insert = "Update rekap_kualitas_data_periodik set 
													tanggal = getdate(),
													identitas_valid_bulan_".$fetch[$j]['bulan']." = '".$fetch[$j]['identitas_valid']."',
													ptk_valid_bulan_".$fetch[$j]['bulan']." = '".$fetch[$j]['ptk_valid']."',
													pd_valid_bulan_".$fetch[$j]['bulan']." = '".$fetch[$j]['pd_valid']."',
													prasarana_valid_bulan_".$fetch[$j]['bulan']." = '".$fetch[$j]['prasarana_valid']."',
													rata_rata_valid_bulan_".$fetch[$j]['bulan']." = '".$fetch[$j]['rata_rata_valid']."'
												where 
													sekolah_id = '".$fetch[$j]['sekolah_id']."' 
												and semester_id = ".$semester->getSemesterId();	

								// echo $br.$sql_insert;continue;

								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap data sekolah ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [UPDATE] GAGAL merekap data sekolah ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}


							}else{

								$sql_insert = "insert into rekap_kualitas_data_periodik(
										rekap_kualitas_data_periodik_id,
										sekolah_id,
										semester_id,
										tahun_ajaran_id,
										soft_delete,
										nama,
										npsn,
										bentuk_pendidikan_id,
										status_sekolah,
										kecamatan,
										kabupaten,
										propinsi,
										kode_wilayah_kecamatan,
										kode_wilayah_kabupaten,
										kode_wilayah_propinsi,
										mst_kode_wilayah_kecamatan,
										mst_kode_wilayah_kabupaten,
										mst_kode_wilayah_propinsi,
										id_level_wilayah_kecamatan,
										id_level_wilayah_kabupaten,
										id_level_wilayah_propinsi,
										tanggal,
										identitas_valid_bulan_".$fetch[$j]['bulan'].",
										ptk_valid_bulan_".$fetch[$j]['bulan'].",
										pd_valid_bulan_".$fetch[$j]['bulan'].",
										prasarana_valid_bulan_".$fetch[$j]['bulan'].",
										rata_rata_valid_bulan_".$fetch[$j]['bulan']."
									) values(
										newid(),
										'".$fetch[$j]['sekolah_id']."',
										'".$fetch[$j]['semester_id']."',
										'".$fetch[$j]['tahun_ajaran_id']."',
										'".$fetch[$j]['soft_delete']."',
										'".$fetch[$j]['nama']."',
										'".$fetch[$j]['npsn']."',
										'".$fetch[$j]['bentuk_pendidikan_id']."',
										'".$fetch[$j]['status_sekolah']."',
										'".$fetch[$j]['kecamatan']."',
										'".$fetch[$j]['kabupaten']."',
										'".$fetch[$j]['propinsi']."',
										'".$fetch[$j]['kode_wilayah_kecamatan']."',
										'".$fetch[$j]['kode_wilayah_kabupaten']."',
										'".$fetch[$j]['kode_wilayah_propinsi']."',
										'".$fetch[$j]['mst_kode_wilayah_kecamatan']."',
										'".$fetch[$j]['mst_kode_wilayah_kabupaten']."',
										'".$fetch[$j]['mst_kode_wilayah_propinsi']."',
										'".$fetch[$j]['id_level_wilayah_kecamatan']."',
										'".$fetch[$j]['id_level_wilayah_kabupaten']."',
										'".$fetch[$j]['id_level_wilayah_propinsi']."',
										getdate(),
										'".$fetch[$j]['identitas_valid']."',
										'".$fetch[$j]['ptk_valid']."',
										'".$fetch[$j]['pd_valid']."',
										'".$fetch[$j]['prasarana_valid']."',
										'".$fetch[$j]['rata_rata_valid']."'
									)";
					
								// echo $br."[".date('H:i:s')."] ".$sql_insert;
								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [INSERT] BERHASIL merekap data sekolah ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [INSERT] GAGAL merekap data sekolah ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}

							}
							
						}

					}
				}
			}
		}


	}
}