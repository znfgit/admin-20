<?php

$limit_var = 100;

include "startup.php";

$semesterCriteria = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $semesterCriteria->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, ".$semester->getTahunAjaranId().");
$semesterCriteria->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($semesterCriteria);

foreach ($semesters as $semester) {

	$z = new Criteria();

	echo $br."[".date('H:i:s')."] Mengambil data propinsi: OK";

	$z->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
	$z->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 1);
	$z->addDescendingOrderByColumn(Admin\Model\MstWilayahPeer::KODE_WILAYAH);

	$propinsis = Admin\Model\MstWilayahPeer::doSelect($z);
	$propinsiCount = Admin\Model\MstWilayahPeer::doCount($z);

	$data_proses_propinsi = 0;

	foreach ($propinsis as $propinsi) {

		$data_proses_propinsi++;

		echo $br."[".date('H:i:s')."] |__ [".$data_proses_propinsi."/".$propinsiCount."] Mengambil kabupaten dari propinsi: ".$propinsi->getNama()." - ".trim($propinsi->getKodeWilayah());

		$y = new Criteria();

		$y->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
		$y->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 2);
		$y->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($propinsi->getKodeWilayah()));

		$kabupatenCount = Admin\Model\MstWilayahPeer::doCount($y);
		$kabupatens = Admin\Model\MstWilayahPeer::doSelect($y);

		$data_proses_kabupaten = 0;

		foreach ($kabupatens as $kabupaten) {
			$data_proses_kabupaten++;

			echo $br."[".date('H:i:s')."] |  |__ [".$data_proses_kabupaten."/".$kabupatenCount."] Mengambil kecamatan dari kabupaten: ".$kabupaten->getNama()." - ".trim($kabupaten->getKodeWilayah());

			$x = new Criteria();

			$x->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
			$x->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 3);
			$x->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($kabupaten->getKodeWilayah()));

			$kecamatanCount = Admin\Model\MstWilayahPeer::doCount($x);
			$kecamatans = Admin\Model\MstWilayahPeer::doSelect($x);

			$data_proses_kecamatan = 0;

			foreach ($kecamatans as $kecamatan) {
				$data_proses_kecamatan++;

				echo $br."[".date('H:i:s')."] |  |  |__ [".$data_proses_kecamatan."/".$kecamatanCount."] Mengambil data sekolah dari kecamatan: ".$kecamatan->getNama()." - ".trim($kecamatan->getKodeWilayah());

				$sql = "SELECT
							s.*,
							bp.nama as bentuk_pendidikan,
							kecamatan.nama as kecamatan,
							kabupaten.nama as kabupaten,
							propinsi.nama as propinsi,
							milik.nama as status_kepemilikan
						FROM
							sekolah s WITH (nolock)
						JOIN ref.mst_wilayah kecamatan WITH (nolock) on left(s.kode_wilayah,6) = kecamatan.kode_wilayah
						JOIN ref.mst_wilayah kabupaten WITH (nolock) on kecamatan.mst_kode_wilayah = kabupaten.kode_wilayah
						JOIN ref.mst_wilayah propinsi WITH (nolock) on kabupaten.mst_kode_wilayah = propinsi.kode_wilayah
						LEFT OUTER JOIN ref.bentuk_pendidikan bp with (nolock) on bp.bentuk_pendidikan_id = s.bentuk_pendidikan_id
						LEFT OUTER JOIN ref.status_kepemilikan milik with (nolock) on milik.status_kepemilikan_id = s.status_kepemilikan_id
						where kecamatan.kode_wilayah = '".trim($kecamatan->getKodeWilayah())."'
						ORDER BY
							s.nama asc";

				$fetch = getDataBySql($sql);

				$data_proses = 0;

				for ($j=0; $j < sizeof($fetch); $j++) {

					$data_proses++;

					$arr = $fetch[$j];

					$checkExistSql = "select sekolah_id from sekolah where sekolah_id = '".$arr['sekolah_id']."'"; 

					$dataExist = getDataByQuery($checkExistSql, 'rekap');

					if(sizeof($dataExist) > 0){
						$set = "";

						foreach ($fetch[$j] as $key => $value) {

							if($key != 'sekolah_id'){

								if($value != null){
									$set .= ", ".$key." = '".trim(str_replace("'", "''", $value))."'";
								}else{
									$set .= ", ".$key." = null";
								}

							}
						}

						$set = substr($set, 1, strlen($set));

						$sql_insert = "Update sekolah set ".$set." where sekolah_id = '".$fetch[$j]['sekolah_id']."'";

						$method = 'UPDATE';
					}else{
						$str_key = "";
						$str_value = "";
						//insert
						foreach ($fetch[$j] as $key => $value) {
							$str_key .= ",".$key;

							if($value != null){
								$str_value .= ",'".trim(str_replace("'", "''", $value))."'";
							}else{
								$str_value .= ",null";
							}

						}

						$str_key = substr($str_key, 1, strlen($str_key));
						$str_value = substr($str_value, 1, strlen($str_value));

						$sql_insert = "insert into sekolah(".$str_key.") values(".$str_value.")";

						$method = 'INSERT';
					}
					
					$result = execQuery($sql_insert);	

					if($result['success']){
						echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".sizeof($fetch)."] [".$method."] BERHASIL merekap data sekolah ".$fetch[$j]['sekolah_id'];
					}else{
						echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".sizeof($fetch)."] [".$method."] GAGAL merekap data sekolah ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
					}			
				}
				

			}
		}
	}
}

?>