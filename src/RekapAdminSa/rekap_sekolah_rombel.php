<?php

$limit_var = 100;

include "startup.php";

$semesterCriteria = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $semesterCriteria->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, ".$semester->getTahunAjaranId().");
$semesterCriteria->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($semesterCriteria);

foreach ($semesters as $semester) {

	$z = new Criteria();

	echo $br."[".date('H:i:s')."] Mengambil data propinsi: OK";

	$z->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
	$z->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 1);
	$z->addDescendingOrderByColumn(Admin\Model\MstWilayahPeer::KODE_WILAYAH);

	$propinsis = Admin\Model\MstWilayahPeer::doSelect($z);
	$propinsiCount = Admin\Model\MstWilayahPeer::doCount($z);

	$data_proses_propinsi = 0;

	foreach ($propinsis as $propinsi) {

		$data_proses_propinsi++;

		echo $br."[".date('H:i:s')."] |__ [".$data_proses_propinsi."/".$propinsiCount."] Mengambil kabupaten dari propinsi: ".$propinsi->getNama()." - ".trim($propinsi->getKodeWilayah());

		$y = new Criteria();

		$y->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
		$y->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 2);
		$y->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($propinsi->getKodeWilayah()));

		$kabupatenCount = Admin\Model\MstWilayahPeer::doCount($y);
		$kabupatens = Admin\Model\MstWilayahPeer::doSelect($y);

		$data_proses_kabupaten = 0;

		foreach ($kabupatens as $kabupaten) {
			$data_proses_kabupaten++;

			echo $br."[".date('H:i:s')."] |  |__ [".$data_proses_kabupaten."/".$kabupatenCount."] Mengambil kecamatan dari kabupaten: ".$kabupaten->getNama()." - ".trim($kabupaten->getKodeWilayah());

			$x = new Criteria();

			$x->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
			$x->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 3);
			$x->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($kabupaten->getKodeWilayah()));

			$kecamatanCount = Admin\Model\MstWilayahPeer::doCount($x);
			$kecamatans = Admin\Model\MstWilayahPeer::doSelect($x);

			$data_proses_kecamatan = 0;

			foreach ($kecamatans as $kecamatan) {
				$data_proses_kecamatan++;

				echo $br."[".date('H:i:s')."] |  |  |__ [".$data_proses_kecamatan."/".$kecamatanCount."] Mengambil data sekolah dari kecamatan: ".$kecamatan->getNama()." - ".trim($kecamatan->getKodeWilayah());

				$sql_count = "SELECT
								sum(1) as total
							FROM
								sekolah WITH (nolock)
							JOIN ref.mst_wilayah kec WITH (nolock) on kec.kode_wilayah = left(sekolah.kode_wilayah,6)
							JOIN ref.mst_wilayah kab WITH (nolock) on kab.kode_wilayah = kec.mst_kode_wilayah
							JOIN ref.mst_wilayah prop WITH (nolock) on prop.kode_wilayah = kab.mst_kode_wilayah
							where kec.kode_wilayah = '".$kecamatan->getKodeWilayah()."'";

				$fetch_count = getDataByQuery($sql_count);

				$recordCountX = $fetch_count[0]['total'];

				echo $br."[".date('H:i:s')."] |  |  |  |__ Jumlah Record: ".$recordCountX;

				$total = $recordCountX; 			

				$data_proses = 0;
			
				for ($i=0; $i < $recordCountX; $i++) {
					if($i % $limit_var == 0){
						echo $br."[".date('H:i:s')."] |  |  |  |__ Mengambil data sekolah dari record ".($i+1)." sampai ".($i+$limit_var).": OK";

						$sql = "SELECT
									newid() as rekap_sekolah_id,
									s.sekolah_id,
									s.nama,
									s.npsn,
									s.bentuk_pendidikan_id,
									s.status_sekolah,
									s.soft_delete,
									'".$semester->getSemesterId()."' as semester_id,
									'".$semester->getTahunAjaranId()."' as tahun_ajaran_id,
									getdate() as tanggal,
									kecamatan.nama as kecamatan,
									kabupaten.nama as kabupaten,
									propinsi.nama as propinsi,
									kecamatan.kode_wilayah as kode_wilayah_kecamatan,
									kabupaten.kode_wilayah as kode_wilayah_kabupaten,
									propinsi.kode_wilayah as kode_wilayah_propinsi,
									kecamatan.id_level_wilayah as id_level_wilayah_kecamatan,
									kabupaten.id_level_wilayah as id_level_wilayah_kabupaten,
									propinsi.id_level_wilayah as id_level_wilayah_propinsi,
									kecamatan.mst_kode_wilayah as mst_kode_wilayah_kecamatan,
									kabupaten.mst_kode_wilayah as mst_kode_wilayah_kabupaten,
									propinsi.mst_kode_wilayah as mst_kode_wilayah_propinsi,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 1
									) AS rombel_kelas_1,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 2
									) AS rombel_kelas_2,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 3
									) AS rombel_kelas_3,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 4
									) AS rombel_kelas_4,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 5
									) AS rombel_kelas_5,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 6
									) AS rombel_kelas_6,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 7
									) AS rombel_kelas_7,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 8
									) AS rombel_kelas_8,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 9
									) AS rombel_kelas_9,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 10
									) AS rombel_kelas_10,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 11
										AND kurikulum_id in (23,24,33)
									) AS rombel_kelas_11_ipa,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 12
										AND kurikulum_id in (23,24,33)
									) AS rombel_kelas_12_ipa,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 11
										AND kurikulum_id in (22,25,34)
									) AS rombel_kelas_11_ips,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 12
										AND kurikulum_id in (22,25,34)
									) AS rombel_kelas_12_ips,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 11
										AND kurikulum_id in (21,26,32)
									) AS rombel_kelas_11_bhs,
									(
										SELECT
											COUNT (1) AS jumlah
										FROM
											rombongan_belajar WITH (nolock)
										WHERE
											sekolah_id = s.sekolah_id
										AND soft_delete = 0
										AND semester_id = ".$semester->getSemesterId()."
										AND jenis_rombel = 1
										AND tingkat_pendidikan_id = 12
										AND kurikulum_id in (21,26,32)
									) AS rombel_kelas_12_bhs
								FROM
									sekolah s WITH (nolock)
								JOIN ref.mst_wilayah kecamatan WITH (nolock) on left(s.kode_wilayah,6) = kecamatan.kode_wilayah
								JOIN ref.mst_wilayah kabupaten WITH (nolock) on kecamatan.mst_kode_wilayah = kabupaten.kode_wilayah
								JOIN ref.mst_wilayah propinsi WITH (nolock) on kabupaten.mst_kode_wilayah = propinsi.kode_wilayah
								where kecamatan.kode_wilayah = '".trim($kecamatan->getKodeWilayah())."'
								ORDER BY
									s.nama OFFSET ".$i." ROWS FETCH NEXT ".$limit_var." ROWS ONLY";

						$fetch = getDataBySql($sql);

						for ($j=0; $j < sizeof($fetch); $j++) {

							$data_proses++;

							$arr = $fetch[$j];

							$checkExistSql = "select sekolah_id from rekap_sekolah where sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

							$dataExist = getDataByQuery($checkExistSql, 'rekap');

							if(sizeof($dataExist) > 0){
								//update

								$set = "";

								foreach ($fetch[$j] as $key => $value) {

									if($key != 'sekolah_id'){

										if($value != null){
											$set .= ", ".$key." = '".trim(str_replace("'", "''", $value))."'";
										}else{
											$set .= ", ".$key." = null";
										}

									}
								}

								$set = substr($set, 1, strlen($set));

								$sql_insert = "Update rekap_sekolah set ".$set." where sekolah_id = '".$fetch[$j]['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

								// echo $br.$sql_insert;continue;

								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap data sekolah ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [UPDATE] GAGAL merekap data sekolah ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}

							}else{
								$str_key = "";
								$str_value = "";
								//insert
								foreach ($fetch[$j] as $key => $value) {
									$str_key .= ",".$key;

									if($value != null){
										$str_value .= ",'".trim(str_replace("'", "''", $value))."'";
									}else{
										$str_value .= ",null";
									}

								}

								$str_key = substr($str_key, 1, strlen($str_key));
								$str_value = substr($str_value, 1, strlen($str_value));

								$sql_insert = "insert into rekap_sekolah(".$str_key.") values(".$str_value.")";
					
								// echo $br."[".date('H:i:s')."] ".$sql_insert;
								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [INSERT] BERHASIL merekap data sekolah ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [INSERT] GAGAL merekap data sekolah ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}
							}

						}
					}
				}

			}
		}
	}
}

?>