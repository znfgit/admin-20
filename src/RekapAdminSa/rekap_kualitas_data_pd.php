<?php

include "startup.php";

$z = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $z->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, 2016);
$z->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($z);

foreach ($semesters as $semester) {
	# code...
	echo $br."[".date('H:i:s')."] Mengambil data tabel semester: ".$semester->getNama();


	$sql_count = "select sum(1) as total FROM
					registrasi_peserta_didik rpd
				JOIN peserta_didik pd ON rpd.peserta_didik_id = pd.peserta_didik_id
				JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
				JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
				JOIN sekolah s ON s.sekolah_id = rb.sekolah_id
				JOIN ref.mst_wilayah kec on kec.kode_wilayah = left(s.kode_wilayah,6)
				JOIN ref.mst_wilayah kab on kab.kode_wilayah = kec.mst_kode_wilayah
				JOIN ref.mst_wilayah prop on prop.kode_wilayah = kab.mst_kode_wilayah
				JOIN ref.semester sm ON sm.semester_id = rb.semester_id
				WHERE
					rpd.Soft_delete = 0
				AND s.soft_delete = 0
				-- AND pd.Soft_delete = 0
				AND (
					rpd.jenis_keluar_id IS NULL OR
					rpd.tanggal_keluar > sm.tanggal_selesai
				)
				AND rb.semester_id = '".$semester->getSemesterId()."'
				AND rb.soft_delete = 0
				AND ar.soft_delete = 0
				AND rb.jenis_rombel = 1";

	$fetch_count = getDataBySql($sql_count);

	$recordCountX = $fetch_count[0]['total'];

	echo $br."[".date('H:i:s')."] Jumlah Record: ".$recordCountX;

	$total = $recordCountX; 
		
	$data_proses = 0;

	// die;

	// for ($i=0; $i < 1; $i++) { 
	for ($i=0; $i < $recordCountX; $i++) { 

		if($i % 10000 == 0){

			echo $br."[".date('H:i:s')."] Mengambil data kualitas PD dari record ".($i+1)." sampai ".($i+10000).": OK";

			$sql = "SELECT
						newid() as rekap_kualitas_data_pd_id,
						'".$semester->getSemesterId()."' as semester_id,
						'".$semester->getTahunAjaranId()."' as tahun_ajaran_id,
						getdate() as tanggal,
						pd.peserta_didik_id,
						pd.nama,
						pd.jenis_kelamin,
						rb.tingkat_pendidikan_id,
						rb.nama as nama_rombel,
						s.sekolah_id,
						s.nama as nama_sekolah,
						s.npsn,
						s.bentuk_pendidikan_id,
						s.status_sekolah,
						kec.nama as kecamatan,
						kab.nama as kabupaten,
						prop.nama as propinsi,
						kec.kode_wilayah as kode_wilayah_kecamatan,
						kab.kode_wilayah as kode_wilayah_kabupaten,
						prop.kode_wilayah as kode_wilayah_propinsi,
						kec.mst_kode_wilayah as mst_kode_wilayah_kecamatan,
						kab.mst_kode_wilayah as mst_kode_wilayah_kabupaten,
						prop.mst_kode_wilayah as mst_kode_wilayah_propinsi,
						kec.id_level_wilayah as id_level_wilayah_kecamatan,
						kab.id_level_wilayah as id_level_wilayah_kabupaten,
						prop.id_level_wilayah as id_level_wilayah_propinsi,
						(case when pd.nisn is null or pd.nisn = '-' then 0 else 1 end) as nisn_valid,
						(case when pd.nik is null or pd.nik = '-' then 0 else 1 end) as nik_valid,
						(case when pd.desa_kelurahan is null or pd.desa_kelurahan = '-' then 0 else 1 end) as desa_kelurahan_valid,
						(case when pd.kode_pos is null or pd.kode_pos = '-' then 0 else 1 end) as kode_pos_valid,
						(case when pd.jenis_tinggal_id is null then 0 else 1 end) as jenis_tinggal_valid,
						(case when pd.alat_transportasi_id is null then 0 else 1 end) as alat_transportasi_id_valid,
						(case when pd.nomor_telepon_seluler is null then 0 else 1 end) as nomor_telepon_seluler_valid,
						(case when pd.email is null then 0 else 1 end) as email_valid,
						(case when pd.penerima_kps = 1 then 
							(case when pd.no_kps is null then 0 else 1 end)
							else
							1
							end) as no_kps_valid,
						(case when pd.nama_ayah is null or pd.nama_ayah = '-' then 0 else 1 end) as nama_ayah_valid,
						(case when pd.tahun_lahir_ayah is null or pd.tahun_lahir_ayah = 0 then 0 else 1 end) as tahun_lahir_ayah_valid,
						(case when pd.jenjang_pendidikan_ayah is null then 0 else 1 end) as jenjang_pendidikan_ayah_valid,
						(case when pd.pekerjaan_id_ayah is null then 0 else 1 end) as pekerjaan_id_ayah_valid,
						(case when pd.penghasilan_id_ayah is null then 0 else 1 end) as penghasilan_id_ayah_valid,
						(case when pd.nama_ibu_kandung is null or pd.nama_ibu_kandung = '-' then 0 else 1 end) as nama_ibu_kandung_valid,
						(case when pd.tahun_lahir_ibu is null or pd.tahun_lahir_ibu = 0 then 0 else 1 end) as tahun_lahir_ibu_valid,
						(case when pd.jenjang_pendidikan_ibu is null then 0 else 1 end) as jenjang_pendidikan_ibu_valid,
						(case when pd.penghasilan_id_ibu is null then 0 else 1 end) as penghasilan_id_ibu_valid,
						(case when pd.pekerjaan_id_ibu is null then 0 else 1 end) as pekerjaan_id_ibu_valid,
						(case when rpd.nipd is null then 0 else 1 end) as nipd_valid,
						(case when rpd.no_skhun is null then 0 else 1 end) as no_skhun_valid,
						pd.soft_delete
					FROM
						registrasi_peserta_didik rpd WITH (nolock)
					JOIN peserta_didik pd WITH (nolock) ON rpd.peserta_didik_id = pd.peserta_didik_id
					JOIN anggota_rombel ar WITH (nolock) ON ar.peserta_didik_id = pd.peserta_didik_id
					JOIN rombongan_belajar rb WITH (nolock) ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
					JOIN sekolah s WITH (nolock) ON s.sekolah_id = rb.sekolah_id
					JOIN ref.mst_wilayah kec WITH (nolock) on kec.kode_wilayah = left(s.kode_wilayah,6)
					JOIN ref.mst_wilayah kab WITH (nolock) on kab.kode_wilayah = kec.mst_kode_wilayah
					JOIN ref.mst_wilayah prop WITH (nolock) on prop.kode_wilayah = kab.mst_kode_wilayah
					JOIN ref.semester sm WITH (nolock) ON sm.semester_id = rb.semester_id
					WHERE
						rpd.Soft_delete = 0
					AND s.soft_delete = 0
					-- AND pd.Soft_delete = 0
					AND (
						rpd.jenis_keluar_id IS NULL OR
						rpd.tanggal_keluar > sm.tanggal_selesai
					)
					AND rb.semester_id = '".$semester->getSemesterId()."'
					AND rb.soft_delete = 0
					AND ar.soft_delete = 0
					AND rb.jenis_rombel = 1
					ORDER BY
						pd.peserta_didik_id OFFSET ".$i." ROWS FETCH NEXT 10000 ROWS ONLY";

			// echo $sql;die;			

			$fetch = getDataBySql($sql);

			$count = 1;
			$objCount = 10000;

			for ($j=0; $j < sizeof($fetch); $j++) {

				$data_proses++;

				$arr = $fetch[$j];

				$checkExistSql = "select peserta_didik_id from rekap_kualitas_data_pd where peserta_didik_id = '".$arr['peserta_didik_id']."' and sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

				// echo $checkExistSql;die;

				$dataExist = getDataByQuery($checkExistSql, 'rekap');

				if(sizeof($dataExist) > 0){
					//update

					$set = "";

					foreach ($arr as $key => $value) {

						if($key != 'sekolah_id'){

							if($value != null){
								$set .= ", ".$key." = '".trim(str_replace("'","''",$value))."'";
							}else{
								$set .= ", ".$key." = null";
							}

						}
					}

					$set = substr($set, 1, strlen($set));

					$sql_insert = "Update rekap_kualitas_data_pd set ".$set." where peserta_didik_id = '".$arr['peserta_didik_id']."' and sekolah_id = '".$arr['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

					// echo $br.$sql_insert;continue;

					$result = execQuery($sql_insert);

					if($result['success']){
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap kualitas data PD ".$arr['sekolah_id'];
					}else{
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [UPDATE] GAGAL merekap kualitas data PD ".$arr['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
					}

				}else{
					$str_key = "";
					$str_value = "";
					//insert
					foreach ($arr as $key => $value) {
						$str_key .= ",".$key;

						if($value != null){
							$str_value .= ",'".trim(str_replace("'","''",$value))."'";
						}else{
							$str_value .= ",null";
						}

					}

					$str_key = substr($str_key, 1, strlen($str_key));
					$str_value = substr($str_value, 1, strlen($str_value));

					$sql_insert = "insert into rekap_kualitas_data_pd(".$str_key.") values(".$str_value.")";
		
					// echo $br."[".date('H:i:s')."] ".$sql_insert;
					$result = execQuery($sql_insert);

					if($result['success']){
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [INSERT] BERHASIL merekap kualitas data PD ".$arr['sekolah_id'];
					}else{
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [INSERT] GAGAL merekap kualitas data PD ".$arr['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
					}
				}
			}

		}
	}
}



?>