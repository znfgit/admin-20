<?php

$limit_var = 100;

include "startup.php";

$semesterCriteria = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $semesterCriteria->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, ".$semester->getTahunAjaranId().");
$semesterCriteria->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($semesterCriteria);

foreach ($semesters as $semester) {

	$z = new Criteria();

	echo $br."[".date('H:i:s')."] Mengambil data propinsi: OK";

	$z->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
	$z->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 1);

	$propinsis = Admin\Model\MstWilayahPeer::doSelect($z);
	$propinsiCount = Admin\Model\MstWilayahPeer::doCount($z);

	$data_proses_propinsi = 0;

	foreach ($propinsis as $propinsi) {

		$data_proses_propinsi++;

		echo $br."[".date('H:i:s')."] |__ [".$data_proses_propinsi."/".$propinsiCount."] Mengambil kabupaten dari propinsi: ".$propinsi->getNama()." - ".trim($propinsi->getKodeWilayah());

		echo $br."[".date('H:i:s')."] |__ delete record lama [".$semester->getNama()."] ...";

		$checkExistSql = "delete from rekap_kualitas_data_sekolah where kode_wilayah_propinsi = '".trim($propinsi->getKodeWilayah())."' and semester_id = '".$semester->getSemesterId()."'"; 

		// echo $checkExistSql;die;

		$dataExist = execQuery($checkExistSql, 'rekap');

		if($dataExist){
			echo $br."[".date('H:i:s')."] |__ delete record lama [".$semester->getNama()."] [OK]";
		}else{
			echo $br."[".date('H:i:s')."] |__ delete record lama [".$semester->getNama()."] [GAGAL]";
		}

		$y = new Criteria();

		$y->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
		$y->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 2);
		$y->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($propinsi->getKodeWilayah()));

		$kabupatenCount = Admin\Model\MstWilayahPeer::doCount($y);
		$kabupatens = Admin\Model\MstWilayahPeer::doSelect($y);

		$data_proses_kabupaten = 0;

		foreach ($kabupatens as $kabupaten) {
			$data_proses_kabupaten++;

			echo $br."[".date('H:i:s')."] |  |__ [".$data_proses_kabupaten."/".$kabupatenCount."] Mengambil kecamatan dari kabupaten: ".$kabupaten->getNama()." - ".trim($kabupaten->getKodeWilayah());

			$x = new Criteria();

			$x->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
			$x->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 3);
			$x->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($kabupaten->getKodeWilayah()));

			$kecamatanCount = Admin\Model\MstWilayahPeer::doCount($x);
			$kecamatans = Admin\Model\MstWilayahPeer::doSelect($x);

			$data_proses_kecamatan = 0;

			foreach ($kecamatans as $kecamatan) {
				$data_proses_kecamatan++;

				echo $br."[".date('H:i:s')."] |  |  |__ [".$data_proses_kecamatan."/".$kecamatanCount."] Mengambil data sekolah dari kecamatan: ".$kecamatan->getNama()." - ".trim($kecamatan->getKodeWilayah());
			
				$sql_count = "SELECT
								sum(1) as total
							FROM
								sekolah WITH (nolock)
							JOIN ref.mst_wilayah kec WITH (nolock) on kec.kode_wilayah = left(sekolah.kode_wilayah,6)
							JOIN ref.mst_wilayah kab WITH (nolock) on kab.kode_wilayah = kec.mst_kode_wilayah
							JOIN ref.mst_wilayah prop WITH (nolock) on prop.kode_wilayah = kab.mst_kode_wilayah
							where kec.kode_wilayah = '".$kecamatan->getKodeWilayah()."'";

				$fetch_count = getDataByQuery($sql_count);

				$recordCountX = $fetch_count[0]['total'];

				echo $br."[".date('H:i:s')."] |  |  |  |__ Jumlah Record: ".$recordCountX;

				$total = $recordCountX; 
			
				$data_proses = 0;
			
				for ($i=0; $i < $recordCountX; $i++) {
					if($i % $limit_var == 0){
						echo $br."[".date('H:i:s')."] |  |  |  |__ Mengambil data kualitas sekolah dari record ".($i+1)." sampai ".($i+$limit_var).": OK";

						$sql = "SELECT
								newid() as rekap_kualitas_data_sekolah_id,
							 	sekolah.sekolah_id,
							 	sekolah.nama as nama,
							 	sekolah.npsn,
							 	sekolah.bentuk_pendidikan_id,
				 				sekolah.status_sekolah,
								kec.nama as kecamatan,
								kab.nama as kabupaten,
								prop.nama as propinsi,
								'".$semester->getSemesterId()."' as semester_id,
								'".$semester->getTahunAjaranId()."' as tahun_ajaran_id,
								getdate() as tanggal,
								kec.kode_wilayah as kode_wilayah_kecamatan,
								kab.kode_wilayah as kode_wilayah_kabupaten,
								prop.kode_wilayah as kode_wilayah_propinsi,
								kec.mst_kode_wilayah as mst_kode_wilayah_kecamatan,
								kab.mst_kode_wilayah as mst_kode_wilayah_kabupaten,
								prop.mst_kode_wilayah as mst_kode_wilayah_propinsi,
								kec.id_level_wilayah as id_level_wilayah_kecamatan,
								kab.id_level_wilayah as id_level_wilayah_kabupaten,
								prop.id_level_wilayah as id_level_wilayah_propinsi,
							 	(case when npsn is null then 0 else 1 end) as npsn_valid,
							 	(case when sk_pendirian_sekolah is null then 0 else 1 end) as sk_pendirian_sekolah_valid,
							 	(case when tanggal_sk_pendirian is null then 0 else 1 end) as tanggal_sk_pendirian_valid,
							 	(case when sk_izin_operasional is null then 0 else 1 end) as sk_izin_operasional_valid,
							 	(case when tanggal_sk_izin_operasional is null then 0 else 1 end) as tanggal_sk_izin_operasional_valid,
							 	(case when no_rekening is null then 0 else 1 end) as no_rekening_valid,
							 	(case when nama_bank is null then 0 else 1 end) as nama_bank_valid,
							 	(case when cabang_kcp_unit is null then 0 else 1 end) as cabang_kcp_unit_valid,
							 	(case when rekening_atas_nama is null then 0 else 1 end) as rekening_atas_nama_valid,
								soft_delete
							FROM
								sekolah WITH (nolock)
							JOIN ref.mst_wilayah kec WITH (nolock) on kec.kode_wilayah = left(sekolah.kode_wilayah,6)
							JOIN ref.mst_wilayah kab WITH (nolock) on kab.kode_wilayah = kec.mst_kode_wilayah
							JOIN ref.mst_wilayah prop WITH (nolock) on prop.kode_wilayah = kab.mst_kode_wilayah
							where kec.kode_wilayah = '".$kecamatan->getKodeWilayah()."'
							ORDER BY
								sekolah.nama OFFSET ".$i." ROWS FETCH NEXT ".$limit_var." ROWS ONLY";

						try {
							$fetch = getDataByQuery($sql);

							$count = 1;
							$objCount = 10;

							for ($j=0; $j < sizeof($fetch); $j++) {
								$data_proses++;

								$arr = $fetch[$j];

								$sql_kepsek = "SELECT top 1
							 						ptk.no_hp,
							 						ptk.nama as nama_kepsek,
							 						ptk.email as email_kepsek
							 					FROM
							 						tugas_tambahan WITH (nolock)
							 					JOIN ptk WITH (nolock) ON tugas_tambahan.ptk_id = ptk.ptk_id
							 					JOIN ptk_terdaftar WITH (nolock) ON ptk_terdaftar.ptk_id = ptk.ptk_id
							 					WHERE
							 						ptk.soft_delete = 0
							 					AND tugas_tambahan.Soft_delete = 0
							 					AND ptk_terdaftar.Soft_delete = 0
							 					AND ptk_terdaftar.sekolah_id = '".$fetch[$j]['sekolah_id']."'
							 					AND tugas_tambahan.jabatan_ptk_id = 2
							 					AND ptk_terdaftar.tahun_ajaran_id = ".$semester->getTahunAjaranId()."
							 					ORDER BY tugas_tambahan.tmt_tambahan desc";

								$fetch_kepsek = getDataBySql($sql_kepsek);

								if(sizeof($fetch_kepsek) > 0){
									if($fetch_kepsek[0]['no_hp'] != null){
										$arr['no_hp_kepsek_valid'] = 1;
									}else{
										$arr['no_hp_kepsek_valid'] = 0;
									}

									if($fetch_kepsek[0]['nama_kepsek'] != null){
										$arr['nama_kepsek_valid'] = 1;
									}else{
										$arr['nama_kepsek_valid'] = 0;
									}

									if($fetch_kepsek[0]['email_kepsek'] != null){
										$arr['email_kepsek_valid'] = 1;
									}else{
										$arr['email_kepsek_valid'] = 0;
									}
								}else{
									$arr['no_hp_kepsek_valid'] = 0;
									$arr['nama_kepsek_valid'] = 0;
									$arr['email_kepsek_valid'] = 0;
									
								}			 	


								$sql_long = "SELECT
												daya_listrik,
												partisipasi_bos,
												waktu_penyelenggaraan_id,
												sumber_listrik_id,
												sertifikasi_iso_id,
												akses_internet_id
											FROM
												sekolah_longitudinal WITH (nolock)
											WHERE
												sekolah_id = '".$fetch[$j]['sekolah_id']."'
											AND	sekolah_longitudinal.soft_delete = 0
											AND sekolah_longitudinal.semester_id = ".$semester->getSemesterId();

								$fetch_long = getDataBySql($sql_long);
								
								if(sizeof($fetch_long) > 0){

									if($fetch_long[0]['daya_listrik'] != null){
										$arr['daya_listrik_valid'] = 1;
									}else{
										$arr['daya_listrik_valid'] = 0;
									}

									if($fetch_long[0]['partisipasi_bos'] != null){
										$arr['partisipasi_bos_valid'] = 1;
									}else{
										$arr['partisipasi_bos_valid'] = 0;
									}

									if($fetch_long[0]['waktu_penyelenggaraan_id'] != null){
										$arr['waktu_penyelenggaraan_id_valid'] = 1;
									}else{
										$arr['waktu_penyelenggaraan_id_valid'] = 0;
									}

									if($fetch_long[0]['sumber_listrik_id'] != null){
										$arr['sumber_listrik_id_valid'] = 1;
									}else{
										$arr['sumber_listrik_id_valid'] = 0;
									}

									if($fetch_long[0]['sertifikasi_iso_id'] != null){
										$arr['sertifikasi_iso_id_valid'] = 1;
									}else{
										$arr['sertifikasi_iso_id_valid'] = 0;
									}

									if($fetch_long[0]['akses_internet_id'] != null){
										$arr['akses_internet_id_valid'] = 1;
									}else{
										$arr['akses_internet_id_valid'] = 0;
									}

								}else{
									$arr['daya_listrik_valid'] = 0;
									$arr['partisipasi_bos_valid'] = 0;
									$arr['waktu_penyelenggaraan_id_valid'] = 0;
									$arr['sumber_listrik_id_valid'] = 0;
									$arr['sertifikasi_iso_id_valid'] = 0;
									$arr['akses_internet_id_valid'] = 0;
									
								}

								$checkExistSql = "select sekolah_id from rekap_kualitas_data_sekolah where sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

								$dataExist = getDataByQuery($checkExistSql, 'rekap');

								if(sizeof($dataExist) > 0){
									//update

									$set = "";

									foreach ($arr as $key => $value) {

										if($key != 'sekolah_id'){

											if($value != null){
												$set .= ", ".$key." = '".trim(str_replace("'","''",$value))."'";
											}else{
												$set .= ", ".$key." = null";
											}

										}
									}

									$set = substr($set, 1, strlen($set));

									$sql_insert = "Update rekap_kualitas_data_sekolah set ".$set." where sekolah_id = '".$arr['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

									// echo $br.$sql_insert;continue;

									$result = execQuery($sql_insert);

									if($result['success']){
										echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap kualitas data sekolah ".$arr['sekolah_id'];
									}else{
										echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [UPDATE] GAGAL merekap kualitas data sekolah ".$arr['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
									}

								}else{
									$str_key = "";
									$str_value = "";
									//insert
									foreach ($arr as $key => $value) {
										$str_key .= ",".$key;

										if($value != null){
											$str_value .= ",'".trim(str_replace("'","''",$value))."'";
										}else{
											$str_value .= ",null";
										}

									}

									$str_key = substr($str_key, 1, strlen($str_key));
									$str_value = substr($str_value, 1, strlen($str_value));

									$sql_insert = "insert into rekap_kualitas_data_sekolah(".$str_key.") values(".$str_value.")";
						
									// echo $br."[".date('H:i:s')."] ".$sql_insert;
									$result = execQuery($sql_insert);

									if($result['success']){
										echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [INSERT] BERHASIL merekap kualitas data sekolah ".$arr['sekolah_id'];
									}else{
										echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [INSERT] GAGAL merekap kualitas data sekolah ".$arr['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
									}
								}
							}

						} catch (Exception $e) {
							print_r(sqlsrv_errors());
						}

					}

				}
			}
		}

	}

}

?>