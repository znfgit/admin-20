<?php

include "startup.php";

$z = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

$z->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, 2016);
$z->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($z);

foreach ($semesters as $semester) {
	# code...
	echo $br."[".date('H:i:s')."] Mengambil data tabel semester: ".$semester->getNama();


	$sql_count = "SELECT
					sum(1) as total
				FROM
					prasarana pras with(nolock)
				JOIN sekolah s with(nolock) ON s.sekolah_id = pras.sekolah_id
				JOIN ref.mst_wilayah kec with(nolock) ON kec.kode_wilayah = LEFT (s.kode_wilayah, 6)
				JOIN ref.mst_wilayah kab with(nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
				JOIN ref.mst_wilayah prop with(nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
				LEFT OUTER JOIN prasarana_longitudinal praslong with(nolock) ON praslong.prasarana_id = pras.prasarana_id
				AND praslong.semester_id = ".$semester->getSemesterId();

	$fetch_count = getDataBySql($sql_count);

	$recordCountX = $fetch_count[0]['total'];

	echo $br."[".date('H:i:s')."] Jumlah Record: ".$recordCountX;

	$total = $recordCountX; 
		
	$data_proses = 0;

	// die;

	// for ($i=0; $i < 1; $i++) { 
	for ($i=0; $i < $recordCountX; $i++) { 

		if($i % 10000 == 0){

			echo $br."[".date('H:i:s')."] Mengambil data kualitas Prasarana dari record ".($i+1)." sampai ".($i+10000).": OK";

			$sql = "SELECT
						newid() AS rekap_kualitas_data_prasarana_id,
						".$semester->getSemesterId()." AS semester_id,
						".$semester->getTahunAjaranId()." AS tahun_ajaran_id,
						getdate() as tanggal,
						pras.prasarana_id,
						pras.nama,
						s.sekolah_id,
						s.nama AS nama_sekolah,
						s.npsn,
						s.bentuk_pendidikan_id,
						s.status_sekolah,
						kec.nama AS kecamatan,
						kab.nama AS kabupaten,
						prop.nama AS propinsi,
						kec.kode_wilayah AS kode_wilayah_kecamatan,
						kab.kode_wilayah AS kode_wilayah_kabupaten,
						prop.kode_wilayah AS kode_wilayah_propinsi,
						kec.mst_kode_wilayah AS mst_kode_wilayah_kecamatan,
						kab.mst_kode_wilayah AS mst_kode_wilayah_kabupaten,
						prop.mst_kode_wilayah AS mst_kode_wilayah_propinsi,
						kec.id_level_wilayah AS id_level_wilayah_kecamatan,
						kab.id_level_wilayah AS id_level_wilayah_kabupaten,
						prop.id_level_wilayah AS id_level_wilayah_propinsi,
						(
							CASE
							WHEN pras.panjang IS NULL
							OR pras.panjang = 0 THEN
								0
							ELSE
								1
							END
						) AS panjang_valid,
						(
							CASE
							WHEN pras.lebar IS NULL
							OR pras.lebar = 0 THEN
								0
							ELSE
								1
							END
						) AS lebar_valid,
						(
							CASE
							WHEN praslong.prasarana_id IS NULL THEN
								0
							ELSE
								1
							END
						) AS kerusakan_valid,
						(
							CASE
							WHEN (
								SELECT
									SUM (1)
								FROM
									sarana
								WHERE
									sarana.prasarana_id = pras.prasarana_id
								AND sarana.soft_delete = 0
							) > 0 THEN
								1
							ELSE
								0
							END
						) AS sarana_valid,
						(
							CASE
							WHEN (
								SELECT
									SUM (1)
								FROM
									buku_alat
								WHERE
									buku_alat.prasarana_id = pras.prasarana_id
								AND buku_alat.soft_delete = 0
							) > 0 THEN
								1
							ELSE
								0
							END
						) AS buku_alat_valid,
						pras.soft_delete
					FROM
						prasarana pras with(nolock)
					JOIN sekolah s with(nolock) ON s.sekolah_id = pras.sekolah_id
					JOIN ref.mst_wilayah kec with(nolock) ON kec.kode_wilayah = LEFT (s.kode_wilayah, 6)
					JOIN ref.mst_wilayah kab with(nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
					JOIN ref.mst_wilayah prop with(nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
					LEFT OUTER JOIN prasarana_longitudinal praslong with(nolock) ON praslong.prasarana_id = pras.prasarana_id
					AND praslong.semester_id = ".$semester->getSemesterId()."
					ORDER BY
						pras.prasarana_id OFFSET ".$i." ROWS FETCH NEXT 10000 ROWS ONLY";

			// echo $sql;die;			

			$fetch = getDataBySql($sql);

			$count = 1;
			$objCount = 10000;

			for ($j=0; $j < sizeof($fetch); $j++) {

				$data_proses++;

				$arr = $fetch[$j];

				$checkExistSql = "select prasarana_id from rekap_kualitas_data_prasarana where prasarana_id = '".$arr['prasarana_id']."' and sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

				// echo $checkExistSql;die;

				$dataExist = getDataByQuery($checkExistSql, 'rekap');

				if(sizeof($dataExist) > 0){
					//update

					$set = "";

					foreach ($arr as $key => $value) {

						if($key != 'sekolah_id'){

							if($value != null){
								$set .= ", ".$key." = '".trim(str_replace("'","''",$value))."'";
							}else{
								$set .= ", ".$key." = null";
							}

						}
					}

					$set = substr($set, 1, strlen($set));

					$sql_insert = "Update rekap_kualitas_data_prasarana set ".$set." where prasarana_id = '".$arr['prasarana_id']."' and sekolah_id = '".$arr['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

					// echo $br.$sql_insert;continue;

					$result = execQuery($sql_insert);

					if($result['success']){
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap kualitas data Prasarana ".$arr['sekolah_id'];
					}else{
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [UPDATE] GAGAL merekap kualitas data Prasarana ".$arr['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
					}

				}else{
					$str_key = "";
					$str_value = "";
					//insert
					foreach ($arr as $key => $value) {
						$str_key .= ",".$key;

						if($value != null){
							$str_value .= ",'".trim(str_replace("'","''",$value))."'";
						}else{
							$str_value .= ",null";
						}

					}

					$str_key = substr($str_key, 1, strlen($str_key));
					$str_value = substr($str_value, 1, strlen($str_value));

					$sql_insert = "insert into rekap_kualitas_data_prasarana(".$str_key.") values(".$str_value.")";
		
					// echo $br."[".date('H:i:s')."] ".$sql_insert;
					$result = execQuery($sql_insert);

					if($result['success']){
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [INSERT] BERHASIL merekap kualitas data Prasarana ".$arr['sekolah_id'];
					}else{
						echo $br."[".date('H:i:s')."] [".$data_proses."/".$total."] [INSERT] GAGAL merekap kualitas data Prasarana ".$arr['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
					}
				}
			}

		}
	}
}



?>