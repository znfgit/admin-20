<?php

$limit_var = 100;

include "startup.php";

$semesterCriteria = new Criteria();

echo $br."[".date('H:i:s')."] Mengambil data tabel semester: OK";

// $semesterCriteria->add(Admin\Model\SemesterPeer::TAHUN_AJARAN_ID, ".$semester->getTahunAjaranId().");
$semesterCriteria->add(Admin\Model\SemesterPeer::SEMESTER_ID, 20162);

$semesters = Admin\Model\SemesterPeer::doSelect($semesterCriteria);

foreach ($semesters as $semester) {

	$z = new Criteria();

	echo $br."[".date('H:i:s')."] Mengambil data propinsi: OK";

	$z->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
	$z->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 1);
	$z->addDescendingOrderByColumn(Admin\Model\MstWilayahPeer::KODE_WILAYAH);

	$propinsis = Admin\Model\MstWilayahPeer::doSelect($z);
	$propinsiCount = Admin\Model\MstWilayahPeer::doCount($z);

	$data_proses_propinsi = 0;

	foreach ($propinsis as $propinsi) {

		$data_proses_propinsi++;

		echo $br."[".date('H:i:s')."] |__ [".$data_proses_propinsi."/".$propinsiCount."] Mengambil kabupaten dari propinsi: ".$propinsi->getNama()." - ".trim($propinsi->getKodeWilayah());

		$y = new Criteria();

		$y->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
		$y->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 2);
		$y->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($propinsi->getKodeWilayah()));

		$kabupatenCount = Admin\Model\MstWilayahPeer::doCount($y);
		$kabupatens = Admin\Model\MstWilayahPeer::doSelect($y);

		$data_proses_kabupaten = 0;

		foreach ($kabupatens as $kabupaten) {
			$data_proses_kabupaten++;

			echo $br."[".date('H:i:s')."] |  |__ [".$data_proses_kabupaten."/".$kabupatenCount."] Mengambil kecamatan dari kabupaten: ".$kabupaten->getNama()." - ".trim($kabupaten->getKodeWilayah());

			$x = new Criteria();

			$x->add(Admin\Model\MstWilayahPeer::EXPIRED_DATE, null, \Criteria::ISNULL);
			$x->add(Admin\Model\MstWilayahPeer::ID_LEVEL_WILAYAH, 3);
			$x->add(Admin\Model\MstWilayahPeer::MST_KODE_WILAYAH, trim($kabupaten->getKodeWilayah()));

			$kecamatanCount = Admin\Model\MstWilayahPeer::doCount($x);
			$kecamatans = Admin\Model\MstWilayahPeer::doSelect($x);

			$data_proses_kecamatan = 0;

			foreach ($kecamatans as $kecamatan) {
				$data_proses_kecamatan++;

				echo $br."[".date('H:i:s')."] |  |  |__ [".$data_proses_kecamatan."/".$kecamatanCount."] Mengambil data sekolah dari kecamatan: ".$kecamatan->getNama()." - ".trim($kecamatan->getKodeWilayah());

				$sql_count = "SELECT
									COUNT (distinct(s.sekolah_id)) AS total
								FROM
									prasarana pr
								JOIN prasarana_longitudinal prl ON prl.prasarana_id = pr.prasarana_id
								JOIN sekolah s ON pr.sekolah_id = s.sekolah_id
								JOIN ref.mst_wilayah w1 ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
								JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
								JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
								WHERE
									pr.Soft_delete = 0
								AND prl.Soft_delete = 0
								AND s.Soft_delete = 0
								AND prl.semester_id = ".$semester->getSemesterId()."
								AND w1.kode_wilayah = '".$kecamatan->getKodeWilayah()."'
								AND s.bentuk_pendidikan_id IN (13, 15)";

				$fetch_count = getDataByQuery($sql_count);

				$recordCountX = $fetch_count[0]['total'];

				echo $br."[".date('H:i:s')."] |  |  |  |__ Jumlah Record: ".$recordCountX;

				$total = $recordCountX; 			

				$data_proses = 0;
			
				for ($i=0; $i < $recordCountX; $i++) {
					if($i % $limit_var == 0){
						echo $br."[".date('H:i:s')."] |  |  |  |__ Mengambil data sarpras sekolah dari record ".($i+1)." sampai ".($i+$limit_var).": OK";

						$sql = "SELECT
									newid() as rekap_sekolah_sarpras_id,
									s.sekolah_id,
									".$semester->getSemesterId()." as semester_id,
									".$semester->getTahunAjaranId()." as tahun_ajaran_id,
									SUM (
										CASE
										WHEN pr.jenis_prasarana_id = 1
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) = 0 THEN
											1
										ELSE
											0
										END
									) AS r_kelas_baik,
									 SUM (
										CASE
										WHEN pr.jenis_prasarana_id = 1
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) > 0
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) <= 30 THEN
											1
										ELSE
											0
										END
									) AS r_kelas_rusak_ringan,
									 SUM (
										CASE
										WHEN pr.jenis_prasarana_id = 1
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) > 30
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) <= 50 THEN
											1
										ELSE
											0
										END
									) AS r_kelas_rusak_sedang,
									 SUM (
										CASE
										WHEN pr.jenis_prasarana_id = 1
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) > 50 THEN
											1
										ELSE
											0
										END
									) AS r_kelas_rusak_berat,
									 SUM (
										CASE
										WHEN pr.jenis_prasarana_id = 16
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) = 0 THEN
											1
										ELSE
											0
										END
									) AS bengkel_baik,
									 SUM (
										CASE
										WHEN pr.jenis_prasarana_id = 16
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) > 0
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) <= 30 THEN
											1
										ELSE
											0
										END
									) AS bengkel_rusak_ringan,
									 SUM (
										CASE
										WHEN pr.jenis_prasarana_id = 16
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) > 30
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) <= 50 THEN
											1
										ELSE
											0
										END
									) AS bengkel_rusak_sedang,
									 SUM (
										CASE
										WHEN pr.jenis_prasarana_id = 16
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) > 50 THEN
											1
										ELSE
											0
										END
									) AS bengkel_rusak_berat,
									 SUM (
										CASE
										WHEN pr.jenis_prasarana_id = 10
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) = 0 THEN
											1
										ELSE
											0
										END
									) AS perpustakaan_baik,
									 SUM (
										CASE
										WHEN pr.jenis_prasarana_id = 10
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) > 0
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) <= 30 THEN
											1
										ELSE
											0
										END
									) AS perpustakaan_rusak_ringan,
									 SUM (
										CASE
										WHEN pr.jenis_prasarana_id = 10
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) > 30
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) <= 50 THEN
											1
										ELSE
											0
										END
									) AS perpustakaan_rusak_sedang,
									 SUM (
										CASE
										WHEN pr.jenis_prasarana_id = 10
										AND round(
											(
												(rusak_pondasi * 0.12) + (
													rusak_sloop_kolom_balok * 0.25
												) + (
													rusak_plester_struktur * 0.02
												) + (rusak_kudakuda_atap * 0.05) + (rusak_kaso_atap * 0.032) + (rusak_reng_atap * 0.015) + ((rusak_tutup_atap) * 0.025) + (rusak_rangka_plafon * 0.03) + (rusak_tutup_plafon * 0.04) + (rusak_bata_dinding * 0.07) + (
													rusak_plester_dinding * 0.022
												) + (rusak_daun_jendela * 0.016) + (rusak_daun_pintu * 0.0125) + (rusak_kusen * 0.026) + (rusak_tutup_lantai * 0.12) + (rusak_inst_listrik * 0.0475) + (rusak_inst_air * 0.0245) + (rusak_drainase * 0.015) + (rusak_finish_struktur * 0.01) + (rusak_finish_plafon * 0.012) + (rusak_finish_dinding * 0.025) + (rusak_finish_kpj * 0.0175)
											),
											2
										) > 50 THEN
											1
										ELSE
											0
										END
									) AS perpustakaan_rusak_berat
									FROM
										prasarana pr
									JOIN prasarana_longitudinal prl ON prl.prasarana_id = pr.prasarana_id
									JOIN sekolah s ON pr.sekolah_id = s.sekolah_id
									JOIN ref.mst_wilayah w1 ON LEFT (s.kode_wilayah, 6) = w1.kode_wilayah
									JOIN ref.mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
									JOIN ref.mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
									WHERE
										pr.Soft_delete = 0
									AND prl.Soft_delete = 0
									AND s.Soft_delete = 0
									AND prl.semester_id = ".$semester->getSemesterId()."
									AND w1.kode_wilayah = '".$kecamatan->getKodeWilayah()."'
									AND s.bentuk_pendidikan_id IN (13, 15)
									GROUP BY
										s.sekolah_id,
										s.nama,
										s.npsn,
										s.status_sekolah,
										s.bentuk_pendidikan_id,
										w1.nama,
										w2.nama,
										w3.nama
									ORDER BY
										s.nama OFFSET ".$i." ROWS FETCH NEXT ".$limit_var." ROWS ONLY";

						$fetch = getDataBySql($sql);

						for ($j=0; $j < sizeof($fetch); $j++) {

							$data_proses++;

							$arr = $fetch[$j];

							$checkExistSql = "select sekolah_id from rekap_sekolah_sarpras where sekolah_id = '".$arr['sekolah_id']."' and semester_id = '".$semester->getSemesterId()."'"; 

							$dataExist = getDataByQuery($checkExistSql, 'rekap');

							if(sizeof($dataExist) > 0){
								//update

								$set = "";

								foreach ($fetch[$j] as $key => $value) {

									if($key != 'sekolah_id'){

										if($value != null){
											$set .= ", ".$key." = '".trim(str_replace("'", "''", $value))."'";
										}else{
											$set .= ", ".$key." = null";
										}

									}
								}

								$set = substr($set, 1, strlen($set));

								$sql_insert = "Update rekap_sekolah_sarpras set ".$set." where sekolah_id = '".$fetch[$j]['sekolah_id']."' and semester_id = ".$semester->getSemesterId();	

								// echo $br.$sql_insert;continue;

								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [UPDATE] BERHASIL merekap sarpras sekolah ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [UPDATE] GAGAL merekap sarpras sekolah ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}

							}else{
								$str_key = "";
								$str_value = "";
								//insert
								foreach ($fetch[$j] as $key => $value) {
									$str_key .= ",".$key;

									if($value != null){
										$str_value .= ",'".trim(str_replace("'", "''", $value))."'";
									}else{
										$str_value .= ",null";
									}

								}

								$str_key = substr($str_key, 1, strlen($str_key));
								$str_value = substr($str_value, 1, strlen($str_value));

								$sql_insert = "insert into rekap_sekolah_sarpras(".$str_key.") values(".$str_value.")";
					
								// echo $br."[".date('H:i:s')."] ".$sql_insert;
								$result = execQuery($sql_insert);

								if($result['success']){
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [INSERT] BERHASIL merekap sarpras sekolah ".$fetch[$j]['sekolah_id'];
								}else{
									echo $br."[".date('H:i:s')."] |  |  |  |__ [".$data_proses."/".$total."] [INSERT] GAGAL merekap sarpras sekolah ".$fetch[$j]['sekolah_id']." Penyebab: ".print_r(sqlsrv_errors(), true);	
								}
							}

						}
					}
				}

			}
		}
	}
}

?>