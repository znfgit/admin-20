SELECT
	sekolah_id,
	
	pd_tkt_1_l AS pd_kelas_1_laki
	,pd_tkt_1_p AS pd_kelas_1_perempuan

	,pd_tkt_2_l AS pd_kelas_2_laki
	,pd_tkt_2_p AS pd_kelas_2_perempuan

	,pd_tkt_3_l AS pd_kelas_3_laki
	,pd_tkt_3_p AS pd_kelas_3_perempuan

	,pd_tkt_4_l AS pd_kelas_4_laki
	,pd_tkt_4_p AS pd_kelas_4_perempuan

	,pd_tkt_5_l AS pd_kelas_5_laki
	,pd_tkt_5_p AS pd_kelas_5_perempuan

	,pd_tkt_6_l AS pd_kelas_6_laki
	,pd_tkt_6_p AS pd_kelas_6_perempuan

	,pd_tkt_7_l AS pd_kelas_7_laki
	,pd_tkt_7_p AS pd_kelas_7_perempuan

	,pd_tkt_8_l AS pd_kelas_8_laki
	,pd_tkt_8_p AS pd_kelas_8_perempuan

	,pd_tkt_9_l AS pd_kelas_9_laki
	,pd_tkt_9_p AS pd_kelas_9_perempuan

	,pd_tkt_10_l AS pd_kelas_10_laki
	,pd_tkt_10_p AS pd_kelas_10_perempuan

	,pd_tkt_11_l AS pd_kelas_11_laki
	,pd_tkt_11_p AS pd_kelas_11_perempuan

	,pd_tkt_12_l AS pd_kelas_12_laki
	,pd_tkt_12_p AS pd_kelas_12_perempuan

	,pd_tkt_13_l AS pd_kelas_13_laki
	,pd_tkt_13_p AS pd_kelas_13_perempuan

FROM
	rpt.statistiksekolah
