DECLARE @rekap_sekolah TABLE(
	[rekap_sekolah_id] uniqueidentifier NOT NULL ,
	[sekolah_id] varchar(100) NOT NULL ,
	[tanggal] date NULL ,
	[semester_id] varchar(10) NULL ,
	[tahun_ajaran_id] varchar(10) NULL ,
	[nama] varchar(100) NULL ,
	[npsn] varchar(100) NULL ,
	[bentuk_pendidikan_id] varchar(10) NULL ,
	[status_sekolah] varchar(5) NULL ,
	[kode_wilayah_kecamatan] varchar(20) NULL ,
	[kecamatan] varchar(100) NULL ,
	[mst_kode_wilayah_kecamatan] varchar(20) NULL ,
	[id_level_wilayah_kecamatan] int NULL ,
	[kode_wilayah_kabupaten] varchar(20) NULL ,
	[kabupaten] varchar(100) NULL ,
	[mst_kode_wilayah_kabupaten] varchar(20) NULL ,
	[id_level_wilayah_kabupaten] int NULL ,
	[kode_wilayah_propinsi] varchar(20) NULL ,
	[propinsi] varchar(100) NULL ,
	[mst_kode_wilayah_propinsi] varchar(20) NULL ,
	[id_level_wilayah_propinsi] int NULL ,
	[guru] int NULL ,
	[guru_non_induk] int NULL ,
	[pegawai] int NULL ,
	[pegawai_non_induk] int NULL,
	[pd] int NULL ,
	[rombel] int NULL ,
	[guru_laki] int NULL ,
	[guru_perempuan] int NULL ,
	[pegawai_laki] int NULL ,
	[pegawai_perempuan] int NULL ,
	[pd_laki] int NULL ,
	[pd_perempuan] int NULL ,
	[jumlah_kirim] int NULL ,
	[akreditasi_id] int NULL ,
	[akreditasi_id_str] varchar(20) NULL ,
	[pd_laki_0_tahun] int NULL ,
	[pd_laki_1_tahun] int NULL ,
	[pd_laki_2_tahun] int NULL ,
	[pd_laki_3_tahun] int NULL ,
	[pd_laki_4_tahun] int NULL ,
	[pd_laki_5_tahun] int NULL ,
	[pd_laki_6_tahun] int NULL ,
	[pd_laki_7_tahun] int NULL ,
	[pd_laki_8_tahun] int NULL ,
	[pd_laki_9_tahun] int NULL ,
	[pd_laki_10_tahun] int NULL ,
	[pd_laki_11_tahun] int NULL ,
	[pd_laki_12_tahun] int NULL ,
	[pd_laki_13_tahun] int NULL ,
	[pd_laki_14_tahun] int NULL ,
	[pd_laki_15_tahun] int NULL ,
	[pd_laki_16_tahun] int NULL ,
	[pd_laki_17_tahun] int NULL ,
	[pd_laki_18_tahun] int NULL ,
	[pd_laki_19_tahun] int NULL ,
	[pd_laki_20_tahun] int NULL ,
	[pd_laki_20_tahun_lebih] int NULL ,
	[pd_perempuan_0_tahun] int NULL ,
	[pd_perempuan_1_tahun] int NULL ,
	[pd_perempuan_2_tahun] int NULL ,
	[pd_perempuan_3_tahun] int NULL ,
	[pd_perempuan_4_tahun] int NULL ,
	[pd_perempuan_5_tahun] int NULL ,
	[pd_perempuan_6_tahun] int NULL ,
	[pd_perempuan_7_tahun] int NULL ,
	[pd_perempuan_8_tahun] int NULL ,
	[pd_perempuan_9_tahun] int NULL ,
	[pd_perempuan_10_tahun] int NULL ,
	[pd_perempuan_11_tahun] int NULL ,
	[pd_perempuan_12_tahun] int NULL ,
	[pd_perempuan_13_tahun] int NULL ,
	[pd_perempuan_14_tahun] int NULL ,
	[pd_perempuan_15_tahun] int NULL ,
	[pd_perempuan_16_tahun] int NULL ,
	[pd_perempuan_17_tahun] int NULL ,
	[pd_perempuan_18_tahun] int NULL ,
	[pd_perempuan_19_tahun] int NULL ,
	[pd_perempuan_20_tahun_lebih] int NULL ,
	[pd_laki_islam] int NULL ,
	[pd_laki_kristen] int NULL ,
	[pd_laki_katholik] int NULL ,
	[pd_laki_hindu] int NULL ,
	[pd_laki_budha] int NULL ,
	[pd_laki_konghucu] int NULL ,
	[pd_laki_lainnya] int NULL ,
	[pd_perempuan_islam] int NULL ,
	[pd_perempuan_kristen] int NULL ,
	[pd_perempuan_katholik] int NULL ,
	[pd_perempuan_hindu] int NULL ,
	[pd_perempuan_budha] int NULL ,
	[pd_perempuan_konghucu] int NULL ,
	[pd_perempuan_lainnya] int NULL ,
	[pd_laki_agama_tidak_diisi] int NULL ,
	[pd_perempuan_agama_tidak_diisi] int NULL,
	[soft_delete] INT
)
DECLARE semester CURSOR FOR SELECT semester_id FROM ref.semester where expired_date is null and tahun_ajaran_id in (2016)
DECLARE @semester_id VARCHAR(256)

OPEN semester
FETCH NEXT FROM semester INTO @semester_id
WHILE @@FETCH_STATUS = 0  
BEGIN  
-- 			
 			DECLARE @c_sekolah CURSOR
			SET @c_sekolah = CURSOR FOR 
			
			SELECT
				s.sekolah_id,
				s.nama,
				s.npsn,
				s.bentuk_pendidikan_id,
				s.status_sekolah,
				kec.nama as kecamatan,
				kec.kode_wilayah as kode_wilayah_kecamatan,
				kec.id_level_wilayah as id_level_wilayah_kecamatan,
				kec.mst_kode_wilayah as mst_kode_wilayah_kecamatan,
				kab.nama as kabupaten,
				kab.kode_wilayah as kode_wilayah_kabupaten,
				kab.id_level_wilayah as id_level_wilayah_kabupaten,
				kab.mst_kode_wilayah as mst_kode_wilayah_kabupaten,
				prop.nama as propinsi,
				prop.kode_wilayah as kode_wilayah_propinsi,
				prop.id_level_wilayah as id_level_wilayah_propinsi,
				prop.mst_kode_wilayah as mst_kode_wilayah_propinsi,
				(
						SELECT
							COUNT (1)
						FROM
							ptk ptk WITH (nolock)
						JOIN ptk_terdaftar ptkd WITH (nolock) ON ptk.ptk_id = ptkd.ptk_id
						JOIN ref.tahun_ajaran ta WITH (nolock) ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						WHERE
							ptkd.sekolah_id = s.sekolah_id
						AND ptk.Soft_delete = 0
						AND ptkd.Soft_delete = 0
						AND ptkd.ptk_induk = 1
						AND ptkd.tahun_ajaran_id = LEFT(@semester_id, 4)
						AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
						AND (
							ptkd.tgl_ptk_keluar > ta.tanggal_selesai
							OR ptkd.jenis_keluar_id IS NULL
						)
					) AS guru,
					(
						SELECT
							COUNT (1)
						FROM
							ptk ptk WITH (nolock) 
						JOIN ptk_terdaftar ptkd WITH (nolock) ON ptk.ptk_id = ptkd.ptk_id
						JOIN ref.tahun_ajaran ta WITH (nolock) ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						WHERE
							ptkd.sekolah_id = s.sekolah_id
						AND ptk.Soft_delete = 0
						AND ptkd.Soft_delete = 0
						AND ptkd.ptk_induk != 1
						AND ptkd.tahun_ajaran_id = LEFT(@semester_id, 4)
						AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
						AND (
							ptkd.tgl_ptk_keluar > ta.tanggal_selesai
							OR ptkd.jenis_keluar_id IS NULL
						)
					) AS guru_non_induk,
					(
						SELECT
							COUNT (1)
						FROM
							ptk ptk 
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						WHERE
							ptkd.sekolah_id = s.sekolah_id
						AND ptk.Soft_delete = 0
						AND ptk.jenis_kelamin = 'L'
						AND ptkd.Soft_delete = 0
						AND ptkd.ptk_induk = 1
						AND ptkd.tahun_ajaran_id = LEFT(@semester_id, 4)
						AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
						AND (
							ptkd.tgl_ptk_keluar > ta.tanggal_selesai
							OR ptkd.jenis_keluar_id IS NULL
						)
					) AS guru_laki,
					(
						SELECT
							COUNT (1)
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						WHERE
							ptkd.sekolah_id = s.sekolah_id
						AND ptk.Soft_delete = 0
						AND ptk.jenis_kelamin = 'P'
						AND ptkd.Soft_delete = 0
						AND ptkd.ptk_induk = 1
						AND ptkd.tahun_ajaran_id = LEFT(@semester_id, 4)
						AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
						AND (
							ptkd.tgl_ptk_keluar > ta.tanggal_selesai
							OR ptkd.jenis_keluar_id IS NULL
						)
					) AS guru_perempuan,
					(
						SELECT
							COUNT (1)
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						WHERE
							ptkd.sekolah_id = s.sekolah_id
						AND ptk.Soft_delete = 0
						AND ptkd.Soft_delete = 0
						AND ptkd.ptk_induk = 1
						AND ptkd.tahun_ajaran_id = LEFT(@semester_id, 4)
						AND ptk.jenis_ptk_id IN (11, 99, 30, 40)
						AND (
							ptkd.tgl_ptk_keluar > ta.tanggal_selesai
							OR ptkd.jenis_keluar_id IS NULL
						)
					) AS pegawai,
					(
						SELECT
							COUNT (1)
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						WHERE
							ptkd.sekolah_id = s.sekolah_id
						AND ptk.Soft_delete = 0
						AND ptkd.Soft_delete = 0
						AND ptkd.ptk_induk != 1
						AND ptkd.tahun_ajaran_id = LEFT(@semester_id, 4)
						AND ptk.jenis_ptk_id IN (11, 99, 30, 40)
						AND (
							ptkd.tgl_ptk_keluar > ta.tanggal_selesai
							OR ptkd.jenis_keluar_id IS NULL
						)
					) AS pegawai_non_induk,
					(
						SELECT
							COUNT (1)
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						WHERE
							ptkd.sekolah_id = s.sekolah_id
						AND ptk.Soft_delete = 0
						AND ptk.jenis_kelamin = 'L'
						AND ptkd.Soft_delete = 0
						AND ptkd.ptk_induk = 1
						AND ptkd.tahun_ajaran_id = LEFT(@semester_id, 4)
						AND ptk.jenis_ptk_id IN (11, 99, 30, 40)
						AND (
							ptkd.tgl_ptk_keluar > ta.tanggal_selesai
							OR ptkd.jenis_keluar_id IS NULL
						)
					) AS pegawai_laki,
					(
						SELECT
							COUNT (1)
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						WHERE
							ptkd.sekolah_id = s.sekolah_id
						AND ptk.Soft_delete = 0
						AND ptk.jenis_kelamin = 'P'
						AND ptkd.Soft_delete = 0
						AND ptkd.ptk_induk = 1
						AND ptkd.tahun_ajaran_id = LEFT(@semester_id, 4)
						AND ptk.jenis_ptk_id IN (11, 99, 30, 40)
						AND (
							ptkd.tgl_ptk_keluar > ta.tanggal_selesai
							OR ptkd.jenis_keluar_id IS NULL
						)
					) AS pegawai_perempuan,
					(
						SELECT
							COUNT (1) AS jumlah
						FROM
							rombongan_belajar
						WHERE
							sekolah_id = s.sekolah_id
						AND soft_delete = 0
						AND semester_id = @semester_id
						AND jenis_rombel = 1
					) AS rombel,
					(
						SELECT
							COUNT (DISTINCT(pd.peserta_didik_id))
						FROM
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
						JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
						JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN ref.semester sm ON sm.semester_id = rb.semester_id
						WHERE
							rpd.Soft_delete = 0
						AND pd.Soft_delete = 0
						AND (rpd.jenis_keluar_id is null
							OR rpd.tanggal_keluar > sm.tanggal_selesai
							OR rpd.jenis_keluar_id = '1')
						AND sm.tahun_ajaran_id like LEFT(@semester_id,4)+'%'
						AND s.sekolah_id = rpd.sekolah_id
						AND rb.soft_delete = 0
						AND ar.soft_delete = 0
						AND rb.jenis_rombel = 1
					) as pd,
					(
						SELECT
							COUNT (DISTINCT(pd.peserta_didik_id))
						FROM
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
						JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
						JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN ref.semester sm ON sm.semester_id = rb.semester_id
						WHERE
							rpd.Soft_delete = 0
						AND pd.Soft_delete = 0
						AND pd.jenis_kelamin = 'L'
						AND (rpd.jenis_keluar_id is null
							OR rpd.tanggal_keluar > sm.tanggal_selesai
							OR rpd.jenis_keluar_id = '1')
						AND sm.tahun_ajaran_id like LEFT(@semester_id,4)+'%'
						AND s.sekolah_id = rpd.sekolah_id
						AND rb.soft_delete = 0
						AND ar.soft_delete = 0
						AND rb.jenis_rombel = 1
					) as pd_laki,
					(
						SELECT
							COUNT (DISTINCT(pd.peserta_didik_id))
						FROM
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
						JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
						JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN ref.semester sm ON sm.semester_id = rb.semester_id
						WHERE
							rpd.Soft_delete = 0
						AND pd.Soft_delete = 0
						AND pd.jenis_kelamin = 'P'
						AND (rpd.jenis_keluar_id is null
							OR rpd.tanggal_keluar > sm.tanggal_selesai
							OR rpd.jenis_keluar_id = '1')
						AND sm.tahun_ajaran_id like LEFT(@semester_id,4)+'%'
						AND s.sekolah_id = rpd.sekolah_id
						AND rb.soft_delete = 0
						AND ar.soft_delete = 0
						AND rb.jenis_rombel = 1
					) as pd_perempuan,
					soft_delete
			FROM
				sekolah s WITH (nolock)
			JOIN ref.mst_wilayah kec WITH (nolock) ON kec.kode_wilayah = LEFT(s.kode_wilayah, 6)
			JOIN ref.mst_wilayah kab WITH (nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
			JOIN ref.mst_wilayah prop WITH (nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
 			
			DECLARE @sekolah_id uniqueidentifier
			DECLARE
				@semester_id_rw VARCHAR (6),
				@nama VARCHAR (100),
				@npsn VARCHAR (50),
				@bentuk_pendidikan_id VARCHAR (6),
				@status_sekolah VARCHAR (1),
				@kecamatan VARCHAR (100),
				@kode_wilayah_kecamatan VARCHAR (100),
				@id_level_wilayah_kecamatan VARCHAR (1),
				@mst_kode_wilayah_kecamatan VARCHAR (100),
				@kabupaten as VARCHAR (100),
				@kode_wilayah_kabupaten VARCHAR (100),
				@id_level_wilayah_kabupaten VARCHAR (1),
				@mst_kode_wilayah_kabupaten VARCHAR (100),
				@propinsi VARCHAR (100),
				@kode_wilayah_propinsi VARCHAR (100),
				@id_level_wilayah_propinsi VARCHAR (1),
				@mst_kode_wilayah_propinsi VARCHAR (100),
				@guru int,
				@guru_non_induk int,
				@guru_laki int,
				@guru_perempuan int,
				@pegawai int,
				@pegawai_non_induk int,
				@pegawai_laki int,
				@pegawai_perempuan int,
				@rombel int,
				@pd int,
				@pd_laki int,
				@pd_perempuan int,
				@soft_delete int
 	
 			SET @semester_id_rw = @semester_id
 			
 			OPEN @c_sekolah
			
			FETCH NEXT
			FROM
				@c_sekolah 
			INTO 
				@sekolah_id,
				@nama,
				@npsn,
				@bentuk_pendidikan_id,
				@status_sekolah,
				@kecamatan,
				@kode_wilayah_kecamatan,
				@id_level_wilayah_kecamatan,
				@mst_kode_wilayah_kecamatan,
				@kabupaten,
				@kode_wilayah_kabupaten,
				@id_level_wilayah_kabupaten,
				@mst_kode_wilayah_kabupaten,
				@propinsi,
				@kode_wilayah_propinsi,
				@id_level_wilayah_propinsi,
				@mst_kode_wilayah_propinsi,
				@guru,
				@guru_non_induk,
				@guru_laki,
				@guru_perempuan,
				@pegawai,
				@pegawai_non_induk,
				@pegawai_laki,
				@pegawai_perempuan,
				@rombel,
				@pd,
				@pd_laki,
				@pd_perempuan,
				@soft_delete

			WHILE @@FETCH_STATUS = 0
			BEGIN
				
				INSERT INTO @rekap_sekolah (
					rekap_sekolah_id,
					tanggal,
					sekolah_id,
					semester_id,
					tahun_ajaran_id,
					nama,
					npsn,
					bentuk_pendidikan_id,
					status_sekolah,
					kecamatan,					
					kode_wilayah_kecamatan,
					id_level_wilayah_kecamatan,
					mst_kode_wilayah_kecamatan,
					kabupaten,
					kode_wilayah_kabupaten,
					id_level_wilayah_kabupaten,
					mst_kode_wilayah_kabupaten,
					propinsi,
					kode_wilayah_propinsi,
					id_level_wilayah_propinsi,
					mst_kode_wilayah_propinsi,
					guru,
					guru_non_induk,
					guru_laki,
					guru_perempuan,
					pegawai,
					pegawai_non_induk,
					pegawai_laki,
					pegawai_perempuan,
					rombel,
					pd,
					pd_laki,
					pd_perempuan,
					soft_delete
				)
				VALUES
				(
					NEWID(),
					GETDATE(),
					@sekolah_id,
					@semester_id_rw,
					LEFT(@semester_id_rw,4),
					@nama,
					@npsn,
					@bentuk_pendidikan_id,
					@status_sekolah,
					@kecamatan,
					@kode_wilayah_kecamatan,
					@id_level_wilayah_kecamatan,
					@mst_kode_wilayah_kecamatan,
					@kabupaten,
					@kode_wilayah_kabupaten,
					@id_level_wilayah_kabupaten,
					@mst_kode_wilayah_kabupaten,
					@propinsi,
					@kode_wilayah_propinsi,
					@id_level_wilayah_propinsi,
					@mst_kode_wilayah_propinsi,
					@guru,
					@guru_non_induk,
					@guru_laki,
					@guru_perempuan,
					@pegawai,
					@pegawai_non_induk,
					@pegawai_laki,
					@pegawai_perempuan,
					@rombel,
					@pd,	
					@pd_laki,
					@pd_perempuan,
					@soft_delete
				)
				
				FETCH NEXT
				FROM
					@c_sekolah 
				INTO 
					@sekolah_id,
					@nama,
					@npsn,
					@bentuk_pendidikan_id,
					@status_sekolah,
					@kecamatan,
					@kode_wilayah_kecamatan,
					@id_level_wilayah_kecamatan,
					@mst_kode_wilayah_kecamatan,
					@kabupaten,
					@kode_wilayah_kabupaten,
					@id_level_wilayah_kabupaten,
					@mst_kode_wilayah_kabupaten,
					@propinsi,
					@kode_wilayah_propinsi,
					@id_level_wilayah_propinsi,
					@mst_kode_wilayah_propinsi,
					@guru,					
					@guru_non_induk,
					@guru_laki,
					@guru_perempuan,
					@pegawai,
					@pegawai_non_induk,
					@pegawai_laki,
					@pegawai_perempuan,
					@rombel,
					@pd,
					@pd_laki,
					@pd_perempuan,
					@soft_delete

			END
			
			CLOSE @c_sekolah
			DEALLOCATE @c_sekolah

      FETCH NEXT FROM semester INTO @semester_id
END
CLOSE semester
DEALLOCATE semester

SELECT * from @rekap_sekolah