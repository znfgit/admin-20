DECLARE @rekap_sekolah TABLE(
	[rekap_sekolah_id] uniqueidentifier NOT NULL ,
	[sekolah_id] varchar(100) NOT NULL ,
	[semester_id] varchar(10) NULL ,
	[tahun_ajaran_id] varchar(10) NULL ,

	[guru_d3_laki_kependidikan] INT NULL ,
	[guru_d3_laki_non_kependidikan] INT NULL ,
	[guru_d3_perempuan_kependidikan] INT NULL ,
	[guru_d3_perempuan_non_kependidikan] INT NULL ,
	
	[guru_d4_laki_kependidikan] INT NULL ,
	[guru_d4_laki_non_kependidikan] INT NULL ,
	[guru_d4_perempuan_kependidikan] INT NULL ,
	[guru_d4_perempuan_non_kependidikan] INT NULL ,

	[guru_s1_laki_kependidikan] INT NULL ,
	[guru_s1_laki_non_kependidikan] INT NULL ,
	[guru_s1_perempuan_kependidikan] INT NULL ,
	[guru_s1_perempuan_non_kependidikan] INT NULL ,

	[guru_s2_laki_kependidikan] INT NULL ,
	[guru_s2_laki_non_kependidikan] INT NULL ,
	[guru_s2_perempuan_kependidikan] INT NULL ,
	[guru_s2_perempuan_non_kependidikan] INT NULL ,

	[guru_s3_laki_kependidikan] INT NULL ,
	[guru_s3_laki_non_kependidikan] INT NULL ,
	[guru_s3_perempuan_kependidikan] INT NULL ,
	[guru_s3_perempuan_non_kependidikan] INT NULL 

)

DECLARE semester CURSOR FOR SELECT semester_id FROM ref.semester where expired_date is null and tahun_ajaran_id in (2016)
DECLARE @semester_id VARCHAR(256),
		@guru_d3_laki_kependidikan INT ,
		@guru_d3_laki_non_kependidikan INT ,
		@guru_d3_perempuan_kependidikan INT ,
		@guru_d3_perempuan_non_kependidikan INT ,
		@guru_d4_laki_kependidikan INT ,
		@guru_d4_laki_non_kependidikan INT ,
		@guru_d4_perempuan_kependidikan INT ,
		@guru_d4_perempuan_non_kependidikan INT ,
		@guru_s1_laki_kependidikan INT ,
		@guru_s1_laki_non_kependidikan INT ,
		@guru_s1_perempuan_kependidikan INT ,
		@guru_s1_perempuan_non_kependidikan INT ,
		@guru_s2_laki_kependidikan INT ,
		@guru_s2_laki_non_kependidikan INT ,
		@guru_s2_perempuan_kependidikan INT ,
		@guru_s2_perempuan_non_kependidikan INT ,
		@guru_s3_laki_kependidikan INT ,
		@guru_s3_laki_non_kependidikan INT ,
		@guru_s3_perempuan_kependidikan INT ,
		@guru_s3_perempuan_non_kependidikan INT 

OPEN semester
FETCH NEXT FROM semester INTO @semester_id
WHILE @@FETCH_STATUS = 0  
BEGIN  
-- 			
 			DECLARE @c_sekolah CURSOR
			SET @c_sekolah = CURSOR FOR 
			
			SELECT
				s.sekolah_id,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'L'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 1
					ORDER by jenjang_pendidikan_id DESC
					) = 22
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_d3_laki_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'L'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 0
					ORDER by jenjang_pendidikan_id DESC
					) = 22
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_d3_laki_non_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'P'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 1
					ORDER by jenjang_pendidikan_id DESC
					) = 22
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_d3_perempuan_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'P'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 0
					ORDER by jenjang_pendidikan_id DESC
					) = 22
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_d3_perempuan_non_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'L'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 1
					ORDER by jenjang_pendidikan_id DESC
					) = 23
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_d4_laki_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'L'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 0
					ORDER by jenjang_pendidikan_id DESC
					) = 23
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_d4_laki_non_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'P'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 1
					ORDER by jenjang_pendidikan_id DESC
					) = 23
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_d4_perempuan_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'P'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 0
					ORDER by jenjang_pendidikan_id DESC
					) = 23
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_d4_perempuan_non_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'L'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 1
					ORDER by jenjang_pendidikan_id DESC
					) = 30
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_s1_laki_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'L'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 0
					ORDER by jenjang_pendidikan_id DESC
					) = 30
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_s1_laki_non_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'P'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 1 
					ORDER by jenjang_pendidikan_id DESC
					) = 30
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_s1_perempuan_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'P'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 0 
					ORDER by jenjang_pendidikan_id DESC
					) = 30
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_s1_perempuan_non_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'L'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 1
					ORDER by jenjang_pendidikan_id DESC
					) = 35
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_s2_laki_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'L'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 0
					ORDER by jenjang_pendidikan_id DESC
					) = 35
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_s2_laki_non_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'P'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 1
					ORDER by jenjang_pendidikan_id DESC
					) = 35
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_s2_perempuan_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'P'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 0
					ORDER by jenjang_pendidikan_id DESC
					) = 35
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_s2_perempuan_non_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'L'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 1
					ORDER by jenjang_pendidikan_id DESC
					) = 40
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_s3_laki_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'L'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 0
					ORDER by jenjang_pendidikan_id DESC
					) = 40
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_s3_laki_non_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'P'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 1
					ORDER by jenjang_pendidikan_id DESC
					) = 40
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_s3_perempuan_kependidikan,
				(
				SELECT
					 sum(1)
				FROM
					ptk ptk
				JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
				JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
				WHERE
					ptk.Soft_delete = 0
				AND ptkd.Soft_delete = 0
				AND ptkd.ptk_induk = 1
				AND ptkd.tahun_ajaran_id = LEFT(@semester_id,4)
				AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
				AND (
					ptkd.tgl_ptk_keluar > ta.tanggal_selesai
					OR ptkd.jenis_keluar_id IS NULL
				)
				AND ptk.jenis_kelamin = 'P'
				AND (
					SELECT TOP 1
						jenjang_pendidikan_id
					FROM
						rwy_pend_formal formal
					WHERE
						formal.soft_delete = 0
					AND formal.jenjang_pendidikan_id < 90
					AND formal.ptk_id = ptk.ptk_id
					AND formal.kependidikan = 0
					ORDER by jenjang_pendidikan_id DESC
					) = 40
					AND ptkd.sekolah_id = s.sekolah_id
				) as guru_s3_perempuan_non_kependidikan
			FROM
				sekolah s WITH (nolock)
			JOIN ref.mst_wilayah kec WITH (nolock) ON kec.kode_wilayah = LEFT(s.kode_wilayah, 6)
			JOIN ref.mst_wilayah kab WITH (nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
			JOIN ref.mst_wilayah prop WITH (nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
			WHERE
				s.Soft_delete = 0
 			
			DECLARE @sekolah_id uniqueidentifier
			DECLARE
				@semester_id_rw VARCHAR (6)
 	
 			SET @semester_id_rw = @semester_id
 			
 			OPEN @c_sekolah
			
			FETCH NEXT
			FROM
				@c_sekolah 
			INTO 
				@sekolah_id,
				@guru_d3_laki_kependidikan ,
				@guru_d3_laki_non_kependidikan ,
				@guru_d3_perempuan_kependidikan ,
				@guru_d3_perempuan_non_kependidikan ,
				@guru_d4_laki_kependidikan ,
				@guru_d4_laki_non_kependidikan ,
				@guru_d4_perempuan_kependidikan ,
				@guru_d4_perempuan_non_kependidikan ,
				@guru_s1_laki_kependidikan ,
				@guru_s1_laki_non_kependidikan ,
				@guru_s1_perempuan_kependidikan ,
				@guru_s1_perempuan_non_kependidikan ,
				@guru_s2_laki_kependidikan ,
				@guru_s2_laki_non_kependidikan ,
				@guru_s2_perempuan_kependidikan ,
				@guru_s2_perempuan_non_kependidikan ,
				@guru_s3_laki_kependidikan ,
				@guru_s3_laki_non_kependidikan ,
				@guru_s3_perempuan_kependidikan ,
				@guru_s3_perempuan_non_kependidikan 

			WHILE @@FETCH_STATUS = 0
			BEGIN
				
				INSERT INTO @rekap_sekolah (
					rekap_sekolah_id,
					sekolah_id,
					semester_id,
					tahun_ajaran_id,
					guru_d3_laki_kependidikan, 
					guru_d3_laki_non_kependidikan, 
					guru_d3_perempuan_kependidikan, 
					guru_d3_perempuan_non_kependidikan, 
					guru_d4_laki_kependidikan, 
					guru_d4_laki_non_kependidikan, 
					guru_d4_perempuan_kependidikan, 
					guru_d4_perempuan_non_kependidikan, 
					guru_s1_laki_kependidikan, 
					guru_s1_laki_non_kependidikan, 
					guru_s1_perempuan_kependidikan, 
					guru_s1_perempuan_non_kependidikan, 
					guru_s2_laki_kependidikan, 
					guru_s2_laki_non_kependidikan, 
					guru_s2_perempuan_kependidikan, 
					guru_s2_perempuan_non_kependidikan, 
					guru_s3_laki_kependidikan, 
					guru_s3_laki_non_kependidikan, 
					guru_s3_perempuan_kependidikan, 
					guru_s3_perempuan_non_kependidikan 
				)
				VALUES
				(
					NEWID(),
					@sekolah_id,
					@semester_id_rw,
					LEFT(@semester_id_rw,4),
					@guru_d3_laki_kependidikan,
					@guru_d3_laki_non_kependidikan,
					@guru_d3_perempuan_kependidikan,
					@guru_d3_perempuan_non_kependidikan,
					@guru_d4_laki_kependidikan,
					@guru_d4_laki_non_kependidikan,
					@guru_d4_perempuan_kependidikan,
					@guru_d4_perempuan_non_kependidikan,
					@guru_s1_laki_kependidikan,
					@guru_s1_laki_non_kependidikan,
					@guru_s1_perempuan_kependidikan,
					@guru_s1_perempuan_non_kependidikan,
					@guru_s2_laki_kependidikan,
					@guru_s2_laki_non_kependidikan,
					@guru_s2_perempuan_kependidikan,
					@guru_s2_perempuan_non_kependidikan,
					@guru_s3_laki_kependidikan,
					@guru_s3_laki_non_kependidikan,
					@guru_s3_perempuan_kependidikan,
					@guru_s3_perempuan_non_kependidikan
				)
				
				FETCH NEXT
				FROM
					@c_sekolah 
				INTO 
					@sekolah_id,
					@guru_d3_laki_kependidikan,
					@guru_d3_laki_non_kependidikan,
					@guru_d3_perempuan_kependidikan,
					@guru_d3_perempuan_non_kependidikan,
					@guru_d4_laki_kependidikan,
					@guru_d4_laki_non_kependidikan,
					@guru_d4_perempuan_kependidikan,
					@guru_d4_perempuan_non_kependidikan,
					@guru_s1_laki_kependidikan,
					@guru_s1_laki_non_kependidikan,
					@guru_s1_perempuan_kependidikan,
					@guru_s1_perempuan_non_kependidikan,
					@guru_s2_laki_kependidikan,
					@guru_s2_laki_non_kependidikan,
					@guru_s2_perempuan_kependidikan,
					@guru_s2_perempuan_non_kependidikan,
					@guru_s3_laki_kependidikan,
					@guru_s3_laki_non_kependidikan,
					@guru_s3_perempuan_kependidikan,
					@guru_s3_perempuan_non_kependidikan

			END
			
			CLOSE @c_sekolah
			DEALLOCATE @c_sekolah

      FETCH NEXT FROM semester INTO @semester_id
END
CLOSE semester
DEALLOCATE semester

SELECT * from @rekap_sekolah