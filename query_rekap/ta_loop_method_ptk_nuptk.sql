DECLARE @rekap_sekolah TABLE(
	[rekap_sekolah_id] uniqueidentifier NOT NULL ,
	[sekolah_id] varchar(100) NOT NULL ,
	[semester_id] varchar(10) NULL ,
	[tahun_ajaran_id] varchar(10) NULL ,
	[guru_nuptk_ada_laki] INT,
	[guru_nuptk_tidak_ada_laki] INT,
	[guru_nuptk_ada_perempuan] INT,
	[guru_nuptk_tidak_ada_perempuan] INT,
	[pegawai_nuptk_ada_laki] INT,
	[pegawai_nuptk_tidak_ada_laki] INT,
	[pegawai_nuptk_ada_perempuan] INT,
	[pegawai_nuptk_tidak_ada_perempuan] INT
)

DECLARE tahun_ajaran CURSOR FOR SELECT tahun_ajaran_id FROM ref.tahun_ajaran where expired_date is null and tahun_ajaran_id in (2016)
DECLARE @tahun_ajaran_id VARCHAR(256),
		@guru_nuptk_ada_laki INT,
		@guru_nuptk_tidak_ada_laki INT,
		@guru_nuptk_ada_perempuan INT,
		@guru_nuptk_tidak_ada_perempuan INT,
		@pegawai_nuptk_ada_laki INT,
		@pegawai_nuptk_tidak_ada_laki INT,
		@pegawai_nuptk_ada_perempuan INT,
		@pegawai_nuptk_tidak_ada_perempuan INT

OPEN tahun_ajaran
FETCH NEXT FROM tahun_ajaran INTO @tahun_ajaran_id
WHILE @@FETCH_STATUS = 0  
BEGIN  
-- 			
 			DECLARE @c_sekolah CURSOR
			SET @c_sekolah = CURSOR FOR 
			
			SELECT
				s.sekolah_id,
				(
					SELECT
						COUNT (1)
					FROM
						ptk ptk
					JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
					JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
					WHERE
						ptkd.sekolah_id = s.sekolah_id
					AND ptk.Soft_delete = 0
					AND ptkd.Soft_delete = 0
					AND ptkd.ptk_induk = 1
					AND ptkd.tahun_ajaran_id = LEFT(@tahun_ajaran_id, 4)
					AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
					AND ptk.jenis_kelamin = 'L'
					AND ISNUMERIC(ptk.nuptk) = 1
					AND (
						ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						OR ptkd.jenis_keluar_id IS NULL
					)
				) as guru_nuptk_ada_laki,
				(
					SELECT
						COUNT (1)
					FROM
						ptk ptk
					JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
					JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
					WHERE
						ptkd.sekolah_id = s.sekolah_id
					AND ptk.Soft_delete = 0
					AND ptkd.Soft_delete = 0
					AND ptkd.ptk_induk = 1
					AND ptkd.tahun_ajaran_id = LEFT(@tahun_ajaran_id, 4)
					AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
					AND ptk.jenis_kelamin = 'L'
					AND ISNUMERIC(ptk.nuptk) = 0
					AND (
						ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						OR ptkd.jenis_keluar_id IS NULL
					)
				) as guru_nuptk_tidak_ada_laki,
				(
					SELECT
						COUNT (1)
					FROM
						ptk ptk
					JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
					JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
					WHERE
						ptkd.sekolah_id = s.sekolah_id
					AND ptk.Soft_delete = 0
					AND ptkd.Soft_delete = 0
					AND ptkd.ptk_induk = 1
					AND ptkd.tahun_ajaran_id = LEFT(@tahun_ajaran_id, 4)
					AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
					AND ptk.jenis_kelamin = 'P'
					AND ISNUMERIC(ptk.nuptk) = 1
					AND (
						ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						OR ptkd.jenis_keluar_id IS NULL
					)
				) as guru_nuptk_ada_perempuan,
				(
					SELECT
						COUNT (1)
					FROM
						ptk ptk
					JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
					JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
					WHERE
						ptkd.sekolah_id = s.sekolah_id
					AND ptk.Soft_delete = 0
					AND ptkd.Soft_delete = 0
					AND ptkd.ptk_induk = 1
					AND ptkd.tahun_ajaran_id = LEFT(@tahun_ajaran_id, 4)
					AND ptk.jenis_ptk_id IN (3, 4, 5, 6, 12, 13, 14)
					AND ptk.jenis_kelamin = 'P'
					AND ISNUMERIC(ptk.nuptk) = 0
					AND (
						ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						OR ptkd.jenis_keluar_id IS NULL
					)
				) as guru_nuptk_tidak_ada_perempuan,
				(
					SELECT
						COUNT (1)
					FROM
						ptk ptk
					JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
					JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
					WHERE
						ptkd.sekolah_id = s.sekolah_id
					AND ptk.Soft_delete = 0
					AND ptkd.Soft_delete = 0
					AND ptkd.ptk_induk = 1
					AND ptkd.tahun_ajaran_id = LEFT(@tahun_ajaran_id, 4)
					AND ptk.jenis_ptk_id IN (11, 99, 30, 40)
					AND ptk.jenis_kelamin = 'L'
					AND ISNUMERIC(ptk.nuptk) = 1
					AND (
						ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						OR ptkd.jenis_keluar_id IS NULL
					)
				) as pegawai_nuptk_ada_laki,
				(
					SELECT
						COUNT (1)
					FROM
						ptk ptk
					JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
					JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
					WHERE
						ptkd.sekolah_id = s.sekolah_id
					AND ptk.Soft_delete = 0
					AND ptkd.Soft_delete = 0
					AND ptkd.ptk_induk = 1
					AND ptkd.tahun_ajaran_id = LEFT(@tahun_ajaran_id, 4)
					AND ptk.jenis_ptk_id IN (11, 99, 30, 40)
					AND ptk.jenis_kelamin = 'L'
					AND ISNUMERIC(ptk.nuptk) = 0
					AND (
						ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						OR ptkd.jenis_keluar_id IS NULL
					)
				) as pegawai_nuptk_tidak_ada_laki,
				(
					SELECT
						COUNT (1)
					FROM
						ptk ptk
					JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
					JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
					WHERE
						ptkd.sekolah_id = s.sekolah_id
					AND ptk.Soft_delete = 0
					AND ptkd.Soft_delete = 0
					AND ptkd.ptk_induk = 1
					AND ptkd.tahun_ajaran_id = LEFT(@tahun_ajaran_id, 4)
					AND ptk.jenis_ptk_id IN (11, 99, 30, 40)
					AND ptk.jenis_kelamin = 'P'
					AND ISNUMERIC(ptk.nuptk) = 1
					AND (
						ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						OR ptkd.jenis_keluar_id IS NULL
					)
				) as pegawai_nuptk_ada_perempuan,
				(
					SELECT
						COUNT (1)
					FROM
						ptk ptk
					JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
					JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
					WHERE
						ptkd.sekolah_id = s.sekolah_id
					AND ptk.Soft_delete = 0
					AND ptkd.Soft_delete = 0
					AND ptkd.ptk_induk = 1
					AND ptkd.tahun_ajaran_id = LEFT(@tahun_ajaran_id, 4)
					AND ptk.jenis_ptk_id IN (11, 99, 30, 40)
					AND ptk.jenis_kelamin = 'P'
					AND ISNUMERIC(ptk.nuptk) = 0
					AND (
						ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						OR ptkd.jenis_keluar_id IS NULL
					)
				) as pegawai_nuptk_tidak_ada_perempuan
			FROM
				sekolah s WITH (nolock)
			JOIN ref.mst_wilayah kec WITH (nolock) ON kec.kode_wilayah = LEFT(s.kode_wilayah, 6)
			JOIN ref.mst_wilayah kab WITH (nolock) ON kab.kode_wilayah = kec.mst_kode_wilayah
			JOIN ref.mst_wilayah prop WITH (nolock) ON prop.kode_wilayah = kab.mst_kode_wilayah
			WHERE
				s.Soft_delete = 0
 			
			DECLARE @sekolah_id uniqueidentifier
			DECLARE
				@tahun_ajaran_id_rw VARCHAR (6)
 	
 			SET @tahun_ajaran_id_rw = @tahun_ajaran_id
 			
 			OPEN @c_sekolah
			
			FETCH NEXT
			FROM
				@c_sekolah 
			INTO 
				@sekolah_id,
				@guru_nuptk_ada_laki,
				@guru_nuptk_tidak_ada_laki,
				@guru_nuptk_ada_perempuan,
				@guru_nuptk_tidak_ada_perempuan,
				@pegawai_nuptk_ada_laki,
				@pegawai_nuptk_tidak_ada_laki,
				@pegawai_nuptk_ada_perempuan,
				@pegawai_nuptk_tidak_ada_perempuan

			WHILE @@FETCH_STATUS = 0
			BEGIN
				
				INSERT INTO @rekap_sekolah (
					rekap_sekolah_id,
					sekolah_id,
					semester_id,
					tahun_ajaran_id,
					guru_nuptk_ada_laki,
					guru_nuptk_tidak_ada_laki,
					guru_nuptk_ada_perempuan,
					guru_nuptk_tidak_ada_perempuan,
					pegawai_nuptk_ada_laki,
					pegawai_nuptk_tidak_ada_laki,
					pegawai_nuptk_ada_perempuan,
					pegawai_nuptk_tidak_ada_perempuan
				)
				VALUES
				(
					NEWID(),
					@sekolah_id,
					NULL,
					LEFT(@tahun_ajaran_id_rw,4),
					@guru_nuptk_ada_laki,
					@guru_nuptk_tidak_ada_laki,
					@guru_nuptk_ada_perempuan,
					@guru_nuptk_tidak_ada_perempuan,
					@pegawai_nuptk_ada_laki,
					@pegawai_nuptk_tidak_ada_laki,
					@pegawai_nuptk_ada_perempuan,
					@pegawai_nuptk_tidak_ada_perempuan
				)
				
				FETCH NEXT
				FROM
					@c_sekolah 
				INTO 
					@sekolah_id,
					@guru_nuptk_ada_laki,
					@guru_nuptk_tidak_ada_laki,
					@guru_nuptk_ada_perempuan,
					@guru_nuptk_tidak_ada_perempuan,
					@pegawai_nuptk_ada_laki,
					@pegawai_nuptk_tidak_ada_laki,
					@pegawai_nuptk_ada_perempuan,
					@pegawai_nuptk_tidak_ada_perempuan

			END
			
			CLOSE @c_sekolah
			DEALLOCATE @c_sekolah

      FETCH NEXT FROM tahun_ajaran INTO @tahun_ajaran_id
END
CLOSE tahun_ajaran
DEALLOCATE tahun_ajaran

SELECT * from @rekap_sekolah